﻿ Shader "StrangeLight/Sky" {
    Properties {
     _MainTex ("color_map", 2D) = "white" {}
     _Tint ("Tint", Color) = (1,1,1,1)
    }
    
 SubShader { 
 Pass { 
 Tags { "Queue"="Background" "RenderType"="Opaque" "IgnoreProjector"="True"} 
 CULL FRONT
 ZWrite Off
 Fog { Mode Off }
 Lighting Off Blend SrcAlpha OneMinusSrcAlpha 
 CGPROGRAM
 #pragma vertex vert_img
 #pragma fragment frag
 #include "UnityCG.cginc"
 uniform sampler2D _MainTex;
 uniform fixed4 _Tint;
 void frag(v2f_img i, out fixed4 col:COLOR, out float depth:DEPTH)
	 { fixed2 uv = i.uv;
	 // uv.x += _Time.x; 
	 col = tex2D(_MainTex, uv) * _Tint;
	 depth = 1; }
 	ENDCG
 	}
 	}
 }
 