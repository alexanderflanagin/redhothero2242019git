﻿Shader "StrangeLight/DiffuseThresholdSurface" {
		Properties {
	      _MainTex ("Texture", 2D) = "white" {}
//		  _DiffuseThreshold ( "_DiffuseThreshold", float ) = 0.25
//		  _Diffusion ( "Diffusion", float ) = 0.69
	    }
	    SubShader {
	      Tags { "RenderType" = "Opaque" }
//	      	Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" }
//	Blend SrcAlpha OneMinusSrcAlpha
//	AlphaTest Greater .01
//	ColorMask RGB
//	Cull Off Lighting Off ZWrite Off
	      CGPROGRAM
	      #pragma surface surf Ramp addshadow 
	      
	      
	      fixed _DiffuseThreshold;
	      fixed _Diffusion;
//	      fixed3 _lightDir;
//	      fixed _atten;
	      half diffusionCutoffTEST;
	      half4 c;
	      
//	      half NdotLTESTING;
	      
//          inline half3 Cacafuti(in half3x3 dirBasis, fixed4 color, fixed4 scale, half3 normal, bool surfFuncWritesNormal, out half3 scalePerBasisVector)
//			{
//				half3 lm = DecodeLightmap (color);
//				
//				// will be compiled out (and so will the texture sample providing the value)
//				// if it's not used in the lighting function, like in LightingLambert
//				scalePerBasisVector = DecodeLightmap (scale);
//
//				// will be compiled out when surface function does not write into o.Normal
//				if (surfFuncWritesNormal)
//				{
//					half3 normalInRnmBasis = saturate (mul (dirBasis, normal));
//					lm *= dot (normalInRnmBasis, scalePerBasisVector);
//				}
//
//				return lm;
//			}

//			
//			half4 LightingRamp_SingleLightmap (SurfaceOutput s, fixed4 color, half3 lightDir, half atten){
//				half NdotLLM = dot ( s.Normal, lightdir );
//				half diffusionCutodffLM = saturate ( ( max ( _DiffuseThreshold, NdotLLM ) - _DiffuseThreshold ) * pow ( ( 2 - _Diffusion ), 10 ) );
//				half4 lm = DecodeLightmap (color);
//				lm.rgb = s.Albedo * _LightColor0.rgb * diffusionCutoff * ( atten *3 );
//				lm.a = s.Alpha;
//				
//				return lm;
//			}

	      half4 LightingRamp (SurfaceOutput s, half3 lightDir, half atten) {
//	        	_lightDir = lightDir;
//	        	_atten = atten;
	        	
	          	half NdotL = dot (s.Normal, lightDir);
//	          	NdotLTESTING = NdotL;
	          	half diffusionCutoff = saturate ( ( max ( 0, NdotL ) - 0 )  * pow ( ( 2 - 0 ), 10 ) );
//	          	diffusionCutoffTEST = diffusionCutoff;
//	          	half4 c;
	          	c.rgb = s.Albedo * _LightColor0.rgb * diffusionCutoff * ( atten * 0.5 ); 
	          	c.a = s.Alpha;
	          	return c;
	          
	      }
	      
	      half4 LightingRamp_SingleLightmap (SurfaceOutput s, fixed4 color){
					
//				half NdotLLM = dot ( s.Normal, _lightDir );
//				half diffusionCutoffLM = saturate ( ( max ( _DiffuseThreshold, NdotLTESTING ) - _DiffuseThreshold ) * pow ( ( 2- _Diffusion ), 10 ));
				
				half3 lm = DecodeLightmap (color);
//				
//				lm = lm * diffusionCutoffTEST;	
//				lm *= c.rgb;
				return half4 ( lm, 0);
		  }
	      
//		  inline half4 LightingRamp_DualLightmap (SurfaceOutput s, fixed4 color, fixed4 indirectOnlyColor, half indirectFade) {
//				UNITY_DIRBASIS
//				half3 scalePerBasisVector;
//				
//				half3 lm = Cacafuti (unity_DirBasis, color, scale, s.Normal, surfFuncWritesNormal, scalePerBasisVector);
//				
//				return half4(lm + 0.2, 0);
//		  }

//			inline half4 LightingRamp_DirLightmap (SurfaceOutput s, fixed4 color, fixed4 scale, bool surfFuncWritesNormal)
//			{
//				UNITY_DIRBASIS
//				half3 scalePerBasisVector;
//				
//				half3 lm = Cacafuti (unity_DirBasis, color, scale, s.Normal, surfFuncWritesNormal, scalePerBasisVector);
//				
//				return half4(lm * 70, 0);
//			}

	      struct Input {
	          float2 uv_MainTex;
	      };
	      sampler2D _MainTex;
	      void surf (Input IN, inout SurfaceOutput o) {
	          o.Albedo = tex2D (_MainTex, IN.uv_MainTex ).rgb;
	          o.Alpha = tex2D (_MainTex, IN.uv_MainTex ).a;
	      }
	      ENDCG
	    }
	   Fallback Off
  }