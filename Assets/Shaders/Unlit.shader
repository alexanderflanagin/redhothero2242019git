﻿ Shader "StrangeLight/Unlit" {
    Properties {
      _MainTex ("Texture", 2D) = "white" {}
      _Color ("Main Color", Color) = (0.0,0.0,0.0,0.0)
    }
    
    SubShader {
      Tags { "RenderType" = "Opaque" }
      Fog {Mode Off}
      
//       LOD 400
      CGPROGRAM
      #pragma surface surf NoLighting

      fixed4 LightingNoLighting(SurfaceOutput s, fixed3 lightDir, fixed atten){
        fixed4 c;
        c.rgb = s.Albedo; 
        c.a = s.Alpha;
        return c;
      }
      struct Input {
          float2 uv_MainTex;
      };
         half4 _Color;
         sampler2D _MainTex;
         
      void surf (Input IN, inout SurfaceOutput o) {
      	half3 a = tex2D(_MainTex, IN.uv_MainTex).rgb * _Color.rgb;
        o.Albedo = a;
      }
      ENDCG
    }
    Fallback "Diffuse"
  }