﻿Shader "Surface/Colored Specular Bumped with Illumination" {
	Properties {
	  _MainTex ("MainTex", 2D) = "white" {}
	  _Shadow ( "Shadow", 2D ) = "white" {}
	  _SpecMap ("SpecMap SpecMap(RGB) Illum(A)", 2D) = "white" {}
	  //_BumpMap ("Normalmap", 2D) = "bump" {}
	  _Shininess ("Shininess", Range (0.01, 5)) = 0.078125
	  _Color ("Color", Color) = (1.0,1.0,1.0,1.0)
	  _ShadowColor ("Shadow Color", Color) = (1.0,1.0,1.0,1.0)
	  _GlossColor ( "Gloss Color", Color ) = (1,1,1,1)
	  _Alpha ( "Alpha", Range (0.1, 1.0) ) = 0.5
	}
	
Subshader {
Tags { "RenderType" = "Transparent" }
Blend SrcAlpha OneMinusSrcAlpha
ColorMask RGB 

	    
	CGPROGRAM
	#pragma surface surf ColoredSpecular
	
	struct MySurfaceOutput {
	    half3 Albedo;
	    half3 Normal;
	    half3 Emission;
	    half Specular;
	    half3 GlossColor;
	    half Alpha;
	};

	inline half4 LightingColoredSpecular (MySurfaceOutput s, half3 lightDir, half3 viewDir, half atten)
	{
	  half3 h = normalize (lightDir + viewDir);
	  half diff = max (0, dot (s.Normal, lightDir));
	  float nh = max (0, dot (s.Normal, h));
	  float spec = pow (nh, 32.0 * s.Specular);
	  half3 specCol = spec * s.GlossColor;
	  half4 c;
	  c.rgb = (s.Albedo * _LightColor0.rgb * diff + _LightColor0.rgb * specCol) * (atten * 2);
	  c.a = s.Alpha;
	  return c;
	}
	inline half4 LightingColoredSpecular_PrePass (MySurfaceOutput s, half4 light)
	{
	    half3 spec = light.a * s.GlossColor;
	    half4 c;
	    c.rgb = (s.Albedo * light.rgb + light.rgb * spec);
	    c.a = s.Alpha + spec * _SpecColor.a;
	    return c;
	}
	struct Input {
	  float2 uv_MainTex;
	  float2 uv_SpecMap;
	  float2 uv_Shadow;
	  //float2 uv_BumpMap;
	};

	sampler2D _MainTex;
	sampler2D _SpecMap;
	sampler2D _Shadow;
	//sampler2D _BumpMap;
	float4 _Color;
	float4 _ShadowColor;
	half _Shininess;
	half4 _GlossColor;
	float _Alpha;
	 
	
	void surf (Input IN, inout MySurfaceOutput o)
	
	{
	
	  half3 c = tex2D (_MainTex, IN.uv_MainTex).rgb * _Color.rgb;
	  half3 shadow = tex2D( _Shadow,IN.uv_Shadow ).rgb * _Color.rgb ;
	  o.Albedo = c.rgb + shadow.rgb;
	  half4 spec = tex2D (_SpecMap, IN.uv_SpecMap);
	  o.GlossColor = spec.rgb;
	  o.Emission = c * spec.a;
	  half4 caca = 
	  o.Specular = _Shininess * _GlossColor ;
	  //o.Normal = UnpackNormal(tex2D(_BumpMap, IN.uv_BumpMap));
	  o.Alpha = _Alpha;
	
	}
	
	ENDCG
	
	}
	
	Fallback "Diffuse"

}