﻿ Shader "StrangeLight/Unlit Ambient" {
    Properties {
      _MainTex ("Texture", 2D) = "white" {}
      _Color ("Main Color", Color) = (1,1,1,1)
      _AmbientPower ( "Ambient Strength", Range ( 0.1, 1 ) ) = 1.0
    }
    SubShader {
      Tags { "RenderType" = "Opaque" }
       LOD 400
      CGPROGRAM
      #pragma surface surf NoLighting

      fixed4 LightingNoLighting(SurfaceOutput s, fixed3 lightDir, fixed atten){
        fixed4 c;
        c.rgb = s.Albedo; 
        c.a = s.Alpha;
        return c;
      }
      struct Input {
          float2 uv_MainTex;
      };
         half4 _Color;
         sampler2D _MainTex;
         half _AmbientPower;
         
      void surf (Input IN, inout SurfaceOutput o) {
      	half3 a = tex2D(_MainTex, IN.uv_MainTex).rgb * _Color.rgb * (UNITY_LIGHTMODEL_AMBIENT / _AmbientPower);
        o.Albedo = a;
      }
      ENDCG
    }
    Fallback "Diffuse"
  }