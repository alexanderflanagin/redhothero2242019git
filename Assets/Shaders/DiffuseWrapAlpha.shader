﻿ Shader "StrangeLight/Cutout Alpha Gradient" {
    Properties {
      _MainTex ("Texture", 2D) = "white" {}
      _Alpha ( "Alpha", Range (0.0, 1.0) ) = 0.5
      _Color ( "Color", Color ) = (1,1,1,1)
    }
    SubShader {
      Tags { "RenderType"="Opaque" }
      ZWRITE ON
      Cull off
      CGPROGRAM
// Upgrade NOTE: excluded shader from OpenGL ES 2.0 because it does not contain a surface program or both vertex and fragment programs.
#pragma exclude_renderers gles
      #pragma surface surf WrapLambert alpha
      #pragma vertex vert
      

      half4 LightingWrapLambert (SurfaceOutput s, half3 lightDir, half atten) {
          half NdotL = dot (s.Normal, lightDir);
          half diff = NdotL * 0.5 + 0.5;
          half4 c;
          c.rgb = s.Albedo * _LightColor0.rgb * (diff * atten * 2);
          c.a = s.Alpha;
          return c;
      }
      
      sampler2D _MainTex;
      float4 _Color;
      float _Alpha;
      
      struct Input {
          float2 uv_MainTex;
          fixed4 color;
      };
      
      void vert ( inout appdata_full v, out Input o ){
      	UNITY_INITIALIZE_OUTPUT( Input, o );
      	o.color = v.color;
      }
      
      void surf (Input IN, inout SurfaceOutput o) {
      	  fixed4 c = tex2D( _MainTex, IN.uv_MainTex ) * IN.color;
      	  
          o.Albedo = c.rgb;
          o.Alpha = c.a;
          o.Gloss = 0.0;
      }
      ENDCG
    }
    Fallback "Diffuse"
  }
