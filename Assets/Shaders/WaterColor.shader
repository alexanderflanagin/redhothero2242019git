﻿Shader "StrangeLight/WaterTEST" {
	Properties {
	      _MainTex ("Main Texture", 2D) = "white" {}
	      _Color ("MainColor", Color ) = ( 0,0,0,0 )
	      _Mask ("Mask", 2D ) = "white" {}
	      _MaskThreshold ("Mask Threshold", Float ) = 0

	    }
	    SubShader {
	      Tags { "QUEUE"="Transparent" "IGNOREPROJECTOR"="true" "RenderType"="Transparent" }
	      Blend SrcAlpha One
 	      Fog {Mode Off}
	      CGPROGRAM
	      #pragma surface surf Lambert addshadow alpha
	      #pragma debug

	      half4 c;
	      
	      half4 _Color;
	      half  _MaskThreshold;
	      half4 _WaveSpeed;
	      half4 _WaveScale;
	      half2 _direction;
//	      half _Fresnel;
//	      half _FresnelThreshold;


	      half4 LightingRamp (SurfaceOutput s, half3 lightDir, half atten) {
	        	
	          	half NdotL = dot (s.Normal, lightDir);
	          	half diffusionCutoff = saturate ( ( max ( 0, NdotL ) - 0 )  * pow ( ( 2 - 0 ), 10 ) );
	          	c.rgb = s.Albedo * _LightColor0.rgb * diffusionCutoff * ( atten * 0.5 ); 
	          	c.a = s.Alpha ;
	          	return c;
	      }

	      struct Input {
	          float2 uv_MainTex;
	          float2 uv_BumpMap;
	          float2 uv_Mask;
	      };
	      
	      sampler2D _MainTex;
//	      sampler2D _BumpMap;
	      sampler2D _Mask;
	      void surf (Input IN, inout SurfaceOutput o) {

//	      UnpackNormal(tex2D(_BumpMap, _WaveScale.x * IN.uv_BumpMap + flowMap.rg * _Time.x * _WaveSpeed.x));
	      	 // float3 NormalMap = UnpackNormal(tex2D(_BumpMap, IN.uv_BumpMap * _Time.x));
//	      	float3 normal1 = UnpackNormal ( tex2D ( _BumpMap, _WaveScale.x * IN.uv_BumpMap + _direction.xy * _Time.x * _WaveSpeed.x ) );
//	      	float3 normal2 = UnpackNormal ( tex2D ( _BumpMap, _WaveScale.y * IN.uv_BumpMap + _direction.xy * _Time.x * _WaveSpeed.y ) );
//	      	
//	      	fixed3 bumpNormal = normalize(normal1 + normal2);
////			
//			float2 screenUVOffset = bumpNormal.xy * 1 / 100;
////			
//			o.Normal.xyz = normalize(bumpNormal);
	      	
	      	
//			  o.Normal = normalize ( Normal1 + Normal2 );
//				half Thresh = saturate ( (1 - _Fresnel ) * pow ( ( 2-_FresnelThreshold ), 10 ) );
  			  o.Albedo = tex2D (_MainTex, IN.uv_MainTex ).rgb * .8 * _Color;
  			  o.Alpha = tex2D (_Mask, IN.uv_Mask ).a + _MaskThreshold;
//				o.Alpha = Thresh;
	          
	      }
	      
	      ENDCG
	    }
	   Fallback Off
  }