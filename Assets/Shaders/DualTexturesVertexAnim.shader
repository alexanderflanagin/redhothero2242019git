﻿Shader "StrangeLight/DualTexture wsVertexAnimation" {
	Properties {
	      _MainTex ("Main Texture [_MainTex]", 2D) = "white" {}
	      _SecTex ( "Secondary Texture [_SecTex]", 2D ) = "white" {}
	      _Color ("MainColor", Color ) = ( 0,0,0,0 )
	      _Mask ("Mask [_Mask]", 2D ) = "white" {}
	      _MaskThreshold ("Mask Threshold", Float ) = 0
	      _MaskColor ("MaskIntensity", Color) = (0,0,0,0)
	      
	      _AnimFreqX ("Freq X", float) = 0
	      _AnimFreqZ ("Freq Z", float) = 0
	      
	      _AnimAmpX ("Amp X", float) = 0
	      _AnimAmpZ ("Amp Z", float) = 0
	      
	      _AnimSpeed ("Speed", float) = 0
	      _Freq ("Freq", float) = 0

	    }
	    SubShader {
	      Tags { "QUEUE"="Transparent" "IGNOREPROJECTOR"="true" "RenderType"="Transparent" }
	      Blend SrcAlpha One
 	      Fog {Mode Off}
	      CGPROGRAM
	      #pragma surface surf Lambert addshadow alpha vertex:vert
	      #pragma debug

	      half4 c;
	      
	      half4 _Color;
	      half  _MaskThreshold;
	      half4 _MaskColor;
	      half4 _WaveSpeed;
	      half4 _WaveScale;
	      half2 _direction;
	      float _AnimFreqX;
	      float _AnimFreqZ;
	      float _AnimAmpX;
	      float _AnimAmpZ;
	      float _AnimSpeed;
	      float _Freq;

	      half4 LightingRamp (SurfaceOutput s, half3 lightDir, half atten) {
	        	
	          	half NdotL = dot (s.Normal, lightDir);
	          	half diffusionCutoff = saturate ( ( max ( 0, NdotL ) - 0 )  * pow ( ( 2 - 0 ), 10 ) );
	          	c.rgb = s.Albedo * _LightColor0.rgb * diffusionCutoff * ( atten * 0.5 ); 
	          	c.a = s.Alpha ;
	          	return c;
	      }

		  void vert ( inout appdata_full v ){
		  		float3 animFreq = float3 (_AnimFreqX, 0, _AnimFreqZ) * v.vertex.xyz;
		  		float3 animAmp = float3 (_AnimAmpX, 0, _AnimAmpZ);
		  		float3 newPos = v.vertex.xyz;
//		  		v.vertex.xyz += ( sin ( _Time.x * animFreq.xyz ) * animAmp.xyz);
				v.vertex.xyz += ( sin ( _Time.x * _AnimSpeed +( animFreq.x + animFreq.y + animFreq.z ) * _Freq) ) * animAmp ;
		  }
	
	      struct Input {
	          float2 uv_MainTex;
	          float2 uv_SecTex;
//	          float2 uv_BumpMap;
	          float2 uv_Mask;
	      };
	      
	      sampler2D _MainTex;
	      sampler2D _SecTex;
//	      sampler2D _BumpMap;
	      sampler2D _Mask;
	      void surf (Input IN, inout SurfaceOutput o) {
	      	  half3 addedTextures = tex2D ( _SecTex, IN.uv_SecTex ).rgb + tex2D (_MainTex, IN.uv_MainTex ).rgb;
  			  o.Albedo = addedTextures * _Color;
  			  o.Alpha = tex2D (_Mask, IN.uv_Mask ).a * _Color.a ;
	      }
	      
	      ENDCG
	    }
	   Fallback Off
  }