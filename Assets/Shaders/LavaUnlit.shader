﻿ Shader "StrangeLight/Lava Ambient" {
    Properties {
      _MainTex ("Texture", 2D) = "white" {}
      _Color ("Main Color", Color) = (1,1,1,1)
      _Bump ("Normal Map", 2D) = "bump" {}
      _AmbientPower ( "Ambient Strength", Range ( 0.1, 1 ) ) = 1.0
    }
    SubShader {
      Tags { "RenderType" = "Opaque" }
       LOD 400
      CGPROGRAM
      #pragma surface surf Lambert

      fixed4 LightingNoLighting(SurfaceOutput s, fixed3 lightDir, fixed atten){
        fixed4 c;
        c.rgb = s.Albedo; 
        c.a = s.Alpha;
        return c;
      }
      struct Input {
          float2 uv_MainTex;
          float2 uv_Bump;
      };
         half4 _Color;
         sampler2D _MainTex;
         sampler2D _Bump;
         half _AmbientPower;
         
      void surf (Input IN, inout SurfaceOutput o) {
      	half3 a = tex2D(_MainTex, IN.uv_MainTex).rgb * _Color.rgb * (UNITY_LIGHTMODEL_AMBIENT / _AmbientPower);
        o.Albedo = a;
        o.Normal = UnpackNormal ( tex2D( _Bump, IN.uv_Bump ) );
      }
      ENDCG
    }
    Fallback "Diffuse"
  }