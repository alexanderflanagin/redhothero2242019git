﻿Shader "StrangeLight/Unlit Ambient Cutout" {
    Properties {
      _MainTex ("Texture", 2D) = "white" {}
      _Color ("Main Color", Color) = (1,1,1,1)
      _AmbientPower ( "Ambient Strength", Range ( 0.1, 1 ) ) = 1.0
      _Cutoffenland ("Alpha cutoff", Range(0,1)) = 0.5

    }
    SubShader {
	Tags {"Queue"="AlphaTest" "IgnoreProjector"="True" "RenderType"="Transparent"}
	Cull Off
	LOD 200

//	Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" }
//	Blend SrcAlpha OneMinusSrcAlpha
//	AlphaTest Greater .01
//	ColorMask RGB
//	Cull Off Lighting Off ZWrite Off
	
	//Lighting Off
      CGPROGRAM
#pragma surface surf Lambert alphatest:_Cutoffenland

      fixed4 LightingNoLighting(SurfaceOutput s, fixed3 lightDir, fixed atten){
        fixed4 c;
        c.rgb = s.Albedo; 
        c.a = s.Alpha;
        return c;
      }
      struct Input {
          float2 uv_MainTex;
      };
         half4 _Color;
         sampler2D _MainTex;
         half _AmbientPower;
         
      void surf (Input IN, inout SurfaceOutput o) {
      	half4 tex = tex2D(_MainTex, IN.uv_MainTex) * _Color * UNITY_LIGHTMODEL_AMBIENT * 2;
        o.Albedo = tex.rgb;
        o.Alpha = tex.a;
      }
      ENDCG
    }
    Fallback "Diffuse"
  }