﻿Shader "StrangeLight/ParticlesThreshold" {
		Properties {
	      _MainTex ("Texture", 2D) = "white" {}
			_Color ( "Color", Color ) = (1.0,1.0,1.0,1.0)
	    }
	    SubShader {

      Tags { "Queue"="transparent" "RenderType"="Opaque" }
      BlendOp Min
      CGPROGRAM
      #pragma surface surf Lambert alpha
      
	      
	      
	      fixed _DiffuseThreshold;
	      fixed _Diffusion;
	      fixed4 _Color;

	      half diffusionCutoffTEST;
	      half4 c;

	      half4 LightingRamp (SurfaceOutput s, half3 lightDir, half atten) {

	          	half NdotL = dot (s.Normal, lightDir);
	          	half diffusionCutoff = saturate ( ( max ( 0, NdotL ) - 0 )  * pow ( ( 2 - 0 ), 10 ) );
	          	diffusionCutoffTEST = diffusionCutoff;
	          	c.rgb = s.Albedo * _LightColor0.rgb * diffusionCutoffTEST * ( atten * 0.5 ); 
	          	c.a = s.Alpha;
	          	return c;
	          
	      }
	      
	      half4 LightingRamp_SingleLightmap (SurfaceOutput s, fixed4 color){
				half3 lm = DecodeLightmap (color);
				return half4 ( lm + 0.2  , 0);
		  }

	      struct Input {
	          float2 uv_MainTex;
	          fixed4 _Color;
	      };
	      
	      sampler2D _MainTex;
	      void surf (Input IN, inout SurfaceOutput o) {
	          o.Albedo = tex2D (_MainTex, IN.uv_MainTex ).rgb;
	          o.Alpha = _Color.a;
	      }
	      ENDCG
	    }
	   Fallback Off
  }