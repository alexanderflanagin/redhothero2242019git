﻿Shader "StrangeLight/Diffuse Specular Rim" {
	Properties {
		_MainTex ("Texture", 2D) = "white" {}
		_RimPower ( "Rim Power", Range( 0.5, 8.0 ) ) = 3.0
		_RimColor ( "Rim Color", Color ) = (0.26,0.19,0.16,0.0)
	}
	SubShader {
		Tags { "RenderType" = "Opaque" }
		CGPROGRAM
		#pragma surface surf DiffuseThresholdSpecular addshadow 

		fixed _DiffuseThreshold;
		fixed _Diffusion;
		fixed diffusionCutoffTEST;
		fixed4 c;

		fixed4 LightingDiffuseThresholdSpecular (SurfaceOutput s, half3 lightDir, half3 viewDir, half atten) {
			fixed3 h = normalize (lightDir + viewDir);
			fixed NdotL = dot (s.Normal, lightDir);
			fixed diffusionCutoff = saturate ( ( max ( 0, NdotL ) - 0 )  * pow ( ( 2 - 0 ), 10 ) );
			fixed nh = max (0, dot (s.Normal, h));
			//pow inversely proportional size of specularity
			fixed spec = pow (nh, 48.0);
			c.rgb = (s.Albedo * _LightColor0.rgb * diffusionCutoff + _LightColor0.rgb * spec) * (atten * 0.7);
			c.a = s.Alpha;
			return c;
		}

		struct Input {
			half2 uv_MainTex;
			fixed3 viewDir;
		};

		sampler2D _MainTex;
		fixed4 _RimColor;
		fixed _RimPower;

		void surf (Input IN, inout SurfaceOutput o) {
			fixed rim = 1.0 - saturate( dot ( normalize( IN.viewDir ), o.Normal ) );
			o.Emission = _RimColor.rgb * pow ( rim, _RimPower );
			o.Albedo = tex2D (_MainTex, IN.uv_MainTex ).rgb;
			o.Alpha = tex2D (_MainTex, IN.uv_MainTex ).a;
		}
		ENDCG
	}

	Fallback Off
}