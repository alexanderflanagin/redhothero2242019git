﻿Shader "StrangeLight/Bumped Diffuse Specular" {
		Properties {
	      _MainTex ("Texture", 2D) = "white" {}
	      _BumpMap ("Bump", 2D) = "Bump" {}
	    }
	    SubShader {
	      Tags { "RenderType" = "Opaque" }

	      CGPROGRAM
	      #pragma surface surf DiffuseThresholdSpecular addshadow 
	      
	      fixed _DiffuseThreshold;
	      fixed _Diffusion;
	      fixed diffusionCutoffTEST;
	      fixed4 c;
	      
	      struct Input {
	          half2 uv_MainTex;
	          half2 uv_BumpMap;
	      };

		fixed4 LightingDiffuseThresholdSpecular (SurfaceOutput s, half3 lightDir, half3 viewDir, half atten) {
			fixed3 h = normalize (lightDir + viewDir);
			fixed NdotL = dot (s.Normal, lightDir);
			fixed diffusionCutoff = saturate ( ( max ( 0, NdotL ) - 0 )  * pow ( ( 2 - 0 ), 0 ) );
			fixed nh = max (0, dot (s.Normal, h));
			fixed spec = pow (nh, 48.0);
			c.rgb = (s.Albedo * _LightColor0.rgb * diffusionCutoff + _LightColor0.rgb * spec) * (atten * 1);
			c.a = s.Alpha;
			return c;
		}
	      
	      sampler2D _MainTex;
	      sampler2D _BumpMap;
	      
	      void surf (Input IN, inout SurfaceOutput o) {
	      		
	          o.Albedo = tex2D (_MainTex, IN.uv_MainTex ).rgb * 1.2f;
	          o.Normal = UnpackNormal (tex2D ( _BumpMap, IN.uv_BumpMap ) );
	          o.Alpha = tex2D (_MainTex, IN.uv_MainTex ).a;
	      }
	      ENDCG
	    }
	    
	   Fallback Off
  }