﻿Shader "StrangeLight/Cutout Diffuse Specular Rim" {
		Properties {
	      _MainTex ("Texture", 2D) = "white" {}
	      _Cutoffenland ( "Cutoff", Range(0,1)) = 0.5
	      _RimPower ( "Rim Power", Range( 0.5, 8.0 ) ) = 3.0
		  _RimColor ( "Rim Color", Color ) = (0.26,0.19,0.16,0.0)

	    }
	    SubShader {
	    Tags { "QUEUE"="AlphaTest" "RenderType"="TransparentCutout"  }
			Cull Off

		      CGPROGRAM
		      #pragma surface surf DiffuseThresholdSpecular addshadow alphatest:_Cutoffenland
		      
		      //FIXED VARIABLES
		      fixed _DiffuseThreshold;
		      fixed diffusionCutoffTEST;
		      fixed4 c;
		      fixed4 _Color;
		      
		fixed4 LightingDiffuseThresholdSpecular (SurfaceOutput s, half3 lightDir, half3 viewDir, half atten) {
			fixed3 h = normalize (lightDir + viewDir);
			fixed NdotL = dot (s.Normal, lightDir);
			fixed diffusionCutoff = saturate ( ( max ( 0, NdotL ) - 0 )  * pow ( ( 2 - 0 ), 10 ) );
			fixed nh = max (0, dot (s.Normal, h));
			fixed spec = pow (nh, 48.0);
			c.rgb = (s.Albedo * _LightColor0.rgb * diffusionCutoff + _LightColor0.rgb * spec) * (atten * 0.7);
			c.a = s.Alpha;
			return c;
		}

		      struct Input {
		          half2 uv_MainTex;
		          half3 viewDir;
		      };
		      sampler2D _MainTex;
		      fixed4 _RimColor;
			  fixed _RimPower;
		      
		      
		      void surf (Input IN, inout SurfaceOutput o) {
		          o.Albedo = tex2D (_MainTex, IN.uv_MainTex ).rgb;
		          o.Alpha = tex2D (_MainTex, IN.uv_MainTex ).a;
		          fixed rim = 1.0 - saturate( dot ( normalize( IN.viewDir ), o.Normal ) );
				  o.Emission = _RimColor.rgb * pow ( rim, _RimPower );
		      }
		      ENDCG
		    }
		   Fallback Off
  }