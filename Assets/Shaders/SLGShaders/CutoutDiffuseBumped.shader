﻿Shader "StrangeLight/Cutout Diffuse Bumped" {
		Properties {
	      _MainTex ("Texture", 2D) = "white" {}
	      _Cutoffenland ( "Cutoff", Range(0,1)) = 0.5
		  _BumpMap ("Bump", 2D) = "Bump" {}
	    }
	    SubShader {
	    Tags { "QUEUE"="AlphaTest" "RenderType"="TransparentCutout"  }
//	      Tags {"Queue"="AlphaTest" "IgnoreProjector"="True" "RenderType"="Transparent"}
			Cull Off
//	      	Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" }
		//	Blend SrcAlpha OneMinusSrcAlpha
		//	AlphaTest Greater .01
		//	ColorMask RGB
		//	Cull Off Lighting Off ZWrite Off
		      CGPROGRAM
		      #pragma surface surf DiffuseThreshold addshadow alphatest:_Cutoffenland
		      
		      //FIXED VARIABLES
		      fixed _DiffuseThreshold;
		      fixed _Diffusion;
	//	      fixed3 _lightDir;
	//	      fixed _atten;
		      fixed diffusionCutoffTEST;
		      fixed4 c;
		      fixed4 _Color;
		      
		fixed4 LightingDiffuseThreshold (SurfaceOutput s, half3 lightDir, half atten) {
			fixed NdotL = dot (s.Normal, lightDir);
			fixed diffusionCutoff = saturate ( ( max ( 0, NdotL ) - 0 )  * pow ( ( 2 - 0 ), 0 ) );
			c.rgb = s.Albedo * _LightColor0.rgb * diffusionCutoff * ( atten * 2 ); 
			c.a = s.Alpha;
			return c;
		}

		      struct Input {
		          half2 uv_MainTex;
		          half2 uv_BumpMap;
		      };
		      sampler2D _MainTex;
		      sampler2D _BumpMap;
		      
		      void surf (Input IN, inout SurfaceOutput o) {
		      	  o.Normal = UnpackNormal ( tex2D ( _BumpMap, IN.uv_BumpMap ));
		          o.Albedo = tex2D (_MainTex, IN.uv_MainTex ).rgb;
		          o.Alpha = tex2D (_MainTex, IN.uv_MainTex ).a;
		      }
		      ENDCG
		   }
		   Fallback Off
  }