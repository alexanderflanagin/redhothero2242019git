﻿Shader "StrangeLight/Cutout Diffuse Specular" {
		Properties {
	      _MainTex ("Texture", 2D) = "white" {}
	      _Cutoffenland ( "Cutoff", Range(0,1)) = 0.5
	    }
	    SubShader {
	    Tags { "QUEUE"="AlphaTest" "RenderType"="TransparentCutout"  }
			Cull Off

		      CGPROGRAM
		      #pragma surface surf DiffuseThresholdSpecular addshadow alphatest:_Cutoffenland
		      
		      //FIXED VARIABLES
		      fixed _DiffuseThreshold;
		      fixed _Diffusion;
		      fixed diffusionCutoffTEST;
		      fixed4 c;
		      fixed4 _Color;
		      
		half4 LightingDiffuseThresholdSpecular (SurfaceOutput s, half3 lightDir, half3 viewDir, half atten) {
			half3 h = normalize (lightDir + viewDir);
			half NdotL = dot (s.Normal, lightDir);
			half diffusionCutoff = saturate ( ( max ( 0, NdotL ) - 0 )  * pow ( ( 2 - 0 ), 10 ) );
			float nh = max (0, dot (s.Normal, h));
			float spec = pow (nh, 8.0);
			c.rgb = (s.Albedo * _LightColor0.rgb * diffusionCutoff + _LightColor0.rgb * spec) * (atten * 0.7);
			c.a = s.Alpha;
			return c;
		}

		      struct Input {
		          float2 uv_MainTex;
		      };
		      sampler2D _MainTex;
		      void surf (Input IN, inout SurfaceOutput o) {
		          o.Albedo = tex2D (_MainTex, IN.uv_MainTex ).rgb;
		          o.Alpha = tex2D (_MainTex, IN.uv_MainTex ).a;
		      }
		      ENDCG
		    }
		   Fallback Off
  }