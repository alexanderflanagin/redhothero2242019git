﻿Shader "StrangeLight/Cutout Diffuse Bumped Specular" {
		Properties {
//		  _Color ("Color", Color) = (0.5,0.5,0.5,0.5)
	      _MainTex ("Texture", 2D) = "white" {}
	      _Cutoffenland ( "Cutoff", Range(0,1)) = 0.5
	      _BumpMap ("Bump", 2D) = "Bump" {}
//		  _DiffuseThreshold ( "_DiffuseThreshold", float ) = 0.25
//		  _Diffusion ( "Diffusion", float ) = 0.69
	    }
	    SubShader {
	    Tags { "QUEUE"="AlphaTest" "RenderType"="TransparentCutout"  }
			Cull Off
		      CGPROGRAM
		      #pragma surface surf DiffuseThresholdSpecular addshadow alphatest:_Cutoffenland
		      
		      //FIXED VARIABLES
		      fixed _DiffuseThreshold;
		      fixed _Diffusion;
		      fixed diffusionCutoffTEST;
		      fixed4 c;
		      fixed4 _Color;
		      
		fixed4 LightingDiffuseThresholdSpecular (SurfaceOutput s, half3 lightDir, half3 viewDir, half atten) {
			fixed3 h = normalize (lightDir + viewDir);
			fixed NdotL = dot (s.Normal, lightDir);
			fixed diffusionCutoff = saturate ( ( max ( 0, NdotL ) - 0 )  * pow ( ( 2 - 0 ), 0 ) );
			fixed nh = max (0, dot (s.Normal, h));
			fixed spec = pow (nh, 48.0);
			c.rgb = (s.Albedo * _LightColor0.rgb * diffusionCutoff + _LightColor0.rgb * spec) * (atten * 2);
			c.a = s.Alpha;
			return c;
		}

		      struct Input {
		          half2 uv_MainTex;
		          half2 uv_BumpMap;
		      };
		      sampler2D _MainTex;
		      sampler2D _BumpMap;
		      void surf (Input IN, inout SurfaceOutput o) {
		      	  o.Normal = UnpackNormal ( tex2D ( _BumpMap, IN.uv_BumpMap ) );
		          o.Albedo = tex2D (_MainTex, IN.uv_MainTex ).rgb;
		          o.Alpha = tex2D (_MainTex, IN.uv_MainTex ).a;
		      }
		      ENDCG
		    }
		   Fallback Off
  }