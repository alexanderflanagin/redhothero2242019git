﻿Shader "StrangeLight/Cutout Diffuse" {
		Properties {
	      _MainTex ("Texture", 2D) = "white" {}
	      _Cutoffenland ( "Cutoff", Range(0,1)) = 0.5
	    }
	    SubShader {
	    Tags { "QUEUE"="AlphaTest" "RenderType"="TransparentCutout"  }
			Cull Off
		      CGPROGRAM
		      #pragma surface surf DiffuseThreshold addshadow alphatest:_Cutoffenland
		      
		      //FIXED VARIABLES
		      fixed _DiffuseThreshold;
		      fixed _Diffusion;
		      fixed diffusionCutoffTEST;
		      fixed4 c;
		      fixed4 _Color;
		      
		fixed4 LightingDiffuseThreshold (SurfaceOutput s, half3 lightDir, half atten) {
			fixed NdotL = dot (s.Normal, lightDir);
			fixed diffusionCutoff = saturate ( ( max ( 0, NdotL ) - 0 )  * pow ( ( 2 - 0 ), 10 ) );
			c.rgb = s.Albedo * _LightColor0.rgb * diffusionCutoff * ( atten * 0.7 ); 
			c.a = s.Alpha;
			return c;
		}

		      struct Input {
		          half2 uv_MainTex;
		      };
		      sampler2D _MainTex;
		      void surf (Input IN, inout SurfaceOutput o) {
		          o.Albedo = tex2D (_MainTex, IN.uv_MainTex ).rgb;
		          o.Alpha = tex2D (_MainTex, IN.uv_MainTex ).a;
		      }
		      ENDCG
		    }
		   Fallback Off
  }