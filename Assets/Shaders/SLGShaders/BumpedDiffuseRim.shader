﻿Shader "StrangeLight/Bumped Diffuse Rim" {
	Properties {
		_MainTex ("Texture", 2D) = "white" {}
		_BumpMap ("Bump", 2D) = "Bump" {}
		_RimPower ( "Rim Power", Range( 0.5, 8.0 ) ) = 3.0
		_RimColor ( "Rim Color", Color ) = (0.26,0.19,0.16,0.0)
	}
	SubShader {
		Tags { "RenderType" = "Opaque" }

		CGPROGRAM
		#pragma surface surf DiffuseThreshold addshadow 

		fixed _DiffuseThreshold;
		fixed _Diffusion;
		fixed diffusionCutoffTEST;
		fixed4 c;

		struct Input {
			float2 uv_MainTex;
			float2 uv_BumpMap;
			float3 viewDir;
		};

		fixed4 LightingDiffuseThreshold (SurfaceOutput s, half3 lightDir, half atten) {
			fixed NdotL = dot (s.Normal, lightDir);
			fixed diffusionCutoff = saturate ( ( max ( 0, NdotL ) - 0 )  * pow ( ( 2 - 0 ), 0 ) );
			c.rgb = s.Albedo * _LightColor0.rgb * diffusionCutoff * ( atten * 1 ); 
			c.a = s.Alpha;
			return c;
		}

		sampler2D _MainTex;
		sampler2D _BumpMap;
		fixed4 _RimColor;
		fixed _RimPower;

		void surf (Input IN, inout SurfaceOutput o) {

		o.Albedo = tex2D (_MainTex, IN.uv_MainTex ).rgb * 1.2f;
			o.Normal = UnpackNormal (tex2D ( _BumpMap, IN.uv_BumpMap ) );
			o.Alpha = tex2D (_MainTex, IN.uv_MainTex ).a;
			fixed rim = 1.0 - saturate( dot ( normalize( IN.viewDir ), o.Normal ) );
			o.Emission = _RimColor.rgb * pow ( rim, _RimPower );
		}
		ENDCG
	}
	Fallback Off
}