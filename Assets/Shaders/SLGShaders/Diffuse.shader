﻿Shader "StrangeLight/Diffuse" {
	Properties {
		_MainTex ("Texture", 2D) = "white" {}
	}
	SubShader {
		Tags { "RenderType" = "Opaque" }
		CGPROGRAM
		#pragma surface surf DiffuseThreshold addshadow 

		fixed4 c;

		half4 LightingDiffuseThreshold (SurfaceOutput s, half3 lightDir, half atten) {
			fixed NdotL = dot (s.Normal, lightDir);
			fixed diffusionCutoff = saturate ( ( max ( 0, NdotL ) - 0 )  * pow ( ( 2 - 0 ), 10 ) );
			c.rgb = s.Albedo * _LightColor0.rgb * diffusionCutoff * ( atten * 0.7 ); 
			c.a = s.Alpha;
			return c;
		}

		struct Input {
			half2 uv_MainTex;
		};
		
		sampler2D _MainTex;
		
		void surf (Input IN, inout SurfaceOutput o) {
			o.Albedo = tex2D (_MainTex, IN.uv_MainTex ).rgb;
			o.Alpha = tex2D (_MainTex, IN.uv_MainTex ).a;
		}
		ENDCG
	}

	Fallback Off
}