﻿ Shader "StrangeLight/CubeTransparent" {
    Properties {
      _MainTex ("Texture", 2D) = "white" {}
      _Alpha ( "Alpha", Range (0.1, 1.0) ) = 0.5
      _Cube ( "Cubemap", Cube ) = "" {}
      _RimPower( "Rim Power", Range( 0.1, 8.0 ) ) = 3.0
      _RimColor( "Rim Colorete", Color ) = ( 0.5, 0.5, 0.5, 0.5 ) 
      _AlphPower ("Alpha Rim Power", Range(0.0,8.0)) = 3.0
_AlphaMin ("Alpha Minimum", Range(0.0,1.0)) = 0.5
    }
    SubShader {
      Tags { "Queue"="transparent" "RenderType"="Opaque" }
      CGPROGRAM
      #pragma surface surf Lambert alpha
     

      half4 LightingWrapLambert (SurfaceOutput s, half3 lightDir, half atten) {
          half NdotL = dot (s.Normal, lightDir);
          half diff = NdotL * 0.5 + 0.5;
          half4 c;
          c.rgb = s.Albedo * _LightColor0.rgb * (diff * atten * 2);
          c.a = s.Alpha;
          return c;
      }

      struct Input {
          float2 uv_MainTex;
          float3 worldRefl;
          float3 viewDir;
          INTERNAL_DATA

      };
      sampler2D _MainTex;
      float4 _RimColor;
      float _RimPower;
      float _Alpha;
      samplerCUBE _Cube;
      	float _AlphPower;
float _AlphaMin;
      void surf (Input IN, inout SurfaceOutput o) {
          o.Albedo = tex2D (_MainTex, IN.uv_MainTex).rgb;
          half rim = 1.0 - saturate( dot ( normalize( IN.viewDir ), o.Normal ) );
          o.Gloss = 0.0;
        //  o.Cube = ( texCUBE (_Cube, IN.worldRefl).rgb );
          
         // o.Emission = _RimColor.rgb * pow (rim, _RimPower) + texCUBE (_Cube, IN.worldRefl).rgb;
          o.Emission = _RimColor.rgb * pow (rim, _RimPower) +  texCUBE (_Cube, IN.worldRefl).rgb;
                  //  o.Alpha = _Alpha;
                    o.Alpha = (pow (rim, _AlphPower)*(1-_AlphaMin))+_AlphaMin ;

      }
      ENDCG
    }
    Fallback "Diffuse"
  }
