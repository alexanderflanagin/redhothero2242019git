using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

[CustomEditor(typeof(StageDot))]
public class StageDotEditor : Editor
{
    EnviroData.EnviroType currentEnviro;
    
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        //GameDataHolder gameData = Resources.Load("Data/EditorConfig") as GameDataHolder;

        //List<string> names = new List<string>();
        //int selected = 0;

        //for (int i = 0; i < gameData.levelsData.Length; i++)
        //{
        //    names.Add(gameData.levelsData[i].sceneName + " - " + gameData.levelsData[i].name);

        //    if (gameData.levelsData[i].sceneName == ((StageDot)this.target).sceneName)
        //    {
        //        selected = i;
        //    }
        //}

        //EditorGUILayout.LabelField(gameData.levelsData[selected].sceneName, EditorStyles.boldLabel);

        //EditorGUILayout.LabelField("Cells needed: " + gameData.levelsData[selected].cellsNeeded);

        //EditorGUILayout.LabelField("Forced minimum of cells: " + gameData.levelsData[selected].forcedMinimumBatteries);

        //EditorGUILayout.LabelField("Forced number of cells: " + gameData.levelsData[selected].forcedBatteries);

        //EditorGUILayout.LabelField("Speedrun time: " + gameData.levelsData[selected].speedrunTime);

        //if (GUI.changed)
        //{
        //    EditorUtility.SetDirty(target);
        //}
    }
}
