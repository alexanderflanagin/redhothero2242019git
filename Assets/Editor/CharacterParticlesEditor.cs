using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor (typeof(CharacterParticles))]
public class CharacterParticlesEditor : Editor {

    
    void OnEnable()
    {
        
    }

    public override void OnInspectorGUI()
    {
        //serializedObject.Update();

        //EditorList.Show(serializedObject.FindProperty("particles"));

        base.OnInspectorGUI();
    }
}

[CustomEditor (typeof(InstantParticleSystemEvent))]
public class InstantParticleSystemEventEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        // LabelField(((InstantParticleSystemEvent)serializedObject).triggerEventStart);
    }
}

public static class EditorList
{
    public static void Show(SerializedProperty list)
    {
        EditorGUILayout.PropertyField(list);
        EditorGUI.indentLevel += 1;
        for (int i = 0; i < list.arraySize; i++)
        {
            EditorGUILayout.LabelField(list.GetArrayElementAtIndex(i).FindPropertyRelative("triggerEventStart").ToString());
            EditorGUILayout.PropertyField(list.GetArrayElementAtIndex(i));
        }
        EditorGUI.indentLevel -= 1;
    }
}
