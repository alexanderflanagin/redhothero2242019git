﻿Shader "Batching/SpecularVC" 
{
	Properties 
	{
		_MainTex ("Base (RGB), Gloss (A)", 2D) = "white" {}
		_SpecColor ("Specular Color", Color) = (0.5, 0.5, 0.5, 1)
		_Shininess ("Shininess", Range (0.01, 1)) = 0.078125
	}
	SubShader 
	{
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf BlinnPhong

		sampler2D _MainTex;
		fixed4 _Color;
		half _Shininess;

		struct Input 
		{
			half2 uv_MainTex;
			half4 color : COLOR;
		};

		void surf (Input IN, inout SurfaceOutput o)
		{
			half4 c = tex2D(_MainTex, IN.uv_MainTex);
		
			o.Albedo = IN.color.rgb * c.rgb;
			o.Gloss = c.a;
			o.Alpha = c.a * IN.color.a;
			o.Specular = _Shininess;
		}
		
		ENDCG
	}
	
	FallBack "Specular"
}
