﻿Shader "Batching/DiffuseVC" 
{
	Properties 
	{
		_MainTex ("Base (RGB), Alpha (A)", 2D) = "white" {}
	}
	SubShader 
	{
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Lambert

		sampler2D _MainTex;

		struct Input 
		{
			half2 uv_MainTex;
			half4 color : COLOR;
		};

		void surf (Input IN, inout SurfaceOutput o)
		{
			half4 c = tex2D(_MainTex, IN.uv_MainTex);
		
			o.Albedo = IN.color.rgb * c.rgb;
			o.Alpha = 1.0;
		}
		
		ENDCG
	}
	
	FallBack "Diffuse"
}
