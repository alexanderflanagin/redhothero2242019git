using UnityEngine;
using UnityEditor;
using System.IO;
using System.Collections.Generic;

public static class LevelMenu
{
    public const string MenuName = "SRHH Editor";
    
    /// <summary>
    /// Find all used psd's in the materials under Assets/Materials
    /// </summary>
    [MenuItem(MenuName + "/Find used psds", false, 10)]
    static void FindUsedPsds()
    {
        string[] lookFor = new string[] { "Assets/Materials" };
        string[] guids2 = AssetDatabase.FindAssets("t:material", lookFor);
        
        foreach (string guid in guids2)
        {
            Material mat = (Material)AssetDatabase.LoadAssetAtPath(AssetDatabase.GUIDToAssetPath(guid), typeof(Material));
            int propertyCount = ShaderUtil.GetPropertyCount(mat.shader);
            List<string> shaderTexturesTypes = new List<string>();
            for(int i = 0; i < propertyCount; i++)
            {
                if(ShaderUtil.GetPropertyType(mat.shader, i) == ShaderUtil.ShaderPropertyType.TexEnv)
                {
                    string name = ShaderUtil.GetPropertyName(mat.shader, i);

                    if (mat.GetTexture(name) != null)
                    {
                        // shaderTextures.Add(AssetDatabase.GetAssetPath(mat.GetTexture(textName)));
                        if (AssetDatabase.GetAssetPath(mat.GetTexture(name)).EndsWith(".psd"))
                        {
                            Debug.Log(AssetDatabase.GUIDToAssetPath(guid) + " utilitza psd's com a textura: " + AssetDatabase.GetAssetPath(mat.GetTexture(name)));
                        }
                    }
                }
            }
        }
    }

    /// <summary>
    /// Find all used tga's in the materials under Assets/Materials
    /// </summary>
    [MenuItem(MenuName + "/Find used tga", false, 10)]
    static void FindUsedTga()
    {
        string[] lookFor = new string[] { "Assets/Materials" };
        string[] guids2 = AssetDatabase.FindAssets("t:material", lookFor);

        foreach (string guid in guids2)
        {
            Material mat = (Material)AssetDatabase.LoadAssetAtPath(AssetDatabase.GUIDToAssetPath(guid), typeof(Material));
            int propertyCount = ShaderUtil.GetPropertyCount(mat.shader);
            List<string> shaderTexturesTypes = new List<string>();
            for (int i = 0; i < propertyCount; i++)
            {
                if (ShaderUtil.GetPropertyType(mat.shader, i) == ShaderUtil.ShaderPropertyType.TexEnv)
                {
                    string name = ShaderUtil.GetPropertyName(mat.shader, i);

                    if (mat.GetTexture(name) != null)
                    {
                        // shaderTextures.Add(AssetDatabase.GetAssetPath(mat.GetTexture(textName)));
                        if (AssetDatabase.GetAssetPath(mat.GetTexture(name)).EndsWith(".tga"))
                        {
                            Debug.Log(AssetDatabase.GUIDToAssetPath(guid) + " utilitza tga's com a textura: " + AssetDatabase.GetAssetPath(mat.GetTexture(name)));
                        }
                    }
                }
            }
        }
    }

    /// <summary>
    /// Check that at least the most basic elements in every stage exists and are well placed
    /// </summary>
    [MenuItem (MenuName + "/Check level elements", false, 0)]
	static void CheckLevelElements()
    {
		string text = "";

        // Gameplay
        GameObject gameplay = GameObject.Find("Gameplay");
        if (!gameplay)
        {
            EditorUtility.DisplayDialog("ERROR", "ERROR\nNo Gameplay Game Object. This stage needs a gameplay Game Object in the root of the scene to works", "ERROR");
            return;
        }

        if(gameplay.transform.parent != null)
        {
            EditorUtility.DisplayDialog("ERROR", "ERROR\nGameplay bad parent. Gameplay Game Object must be in the root of the hierarchy", "ERROR");
            return;
        }

        // Decorations
        GameObject decorations = GameObject.Find("Decorations");
        if (decorations == null)
        {
            // Creem el block de gameplay
            decorations = new GameObject("Decorations");
            decorations.transform.position = Vector3.zero;
            text += "Decorations Game Object created\n";
        }

        Light[] lights = decorations.GetComponentsInChildren<Light>();
        if(lights.Length > 0)
        {
            text += "Lights found inside Decorations GameObject:\n";
            for(int i = 0; i < lights.Length; i++)
            {
                text += lights[i].transform.parent.name + " - " + lights[i].gameObject.name + "\n";
            }
        }

        // 
        if (GameObject.FindWithTag("FinishLine") == null)
        {
            GameObject finishLine = PrefabUtility.InstantiatePrefab(AssetDatabase.LoadAssetAtPath("Assets/Prefabs/VFX/RedHot/RedHotFinishLine.prefab", typeof(GameObject))) as GameObject;
            finishLine.transform.position = new Vector3(30, 0, 0);
            text += "Finish Line created\n";
        }

        // Main Char
        GameObject[] players = GameObject.FindGameObjectsWithTag(Tags.Player);
        GameObject[] spawns = GameObject.FindGameObjectsWithTag("Respawn");
        ArrayUtility.AddRange(ref players, spawns);
        if (players.Length == 0)
        {
            GameObject player = PrefabUtility.InstantiatePrefab(AssetDatabase.LoadAssetAtPath("Assets/Prefabs/Editor/CharacterSpawn.prefab", typeof(GameObject))) as GameObject;
            player.transform.position = new Vector3(13, 0.5f, 0);
            text += "New player created\n";
        }
        else if (players.Length > 1)
        {
            EditorUtility.DisplayDialog("ERROR", "ERROR\nMore than one player Game Object / Spawn", "ERROR");
            return;
        }
        else if(players[0].transform.parent != null && players[0].transform.parent.name != "Gameplay")
        {
            EditorUtility.DisplayDialog("ERROR", "ERROR\nPlayer spawn must be in the root of the hierarchy or inside Gameplay", "ERROR");
            return;
        }

        // Scenario Manager
        GameObject stageManager = GameObject.FindWithTag("StageManager");
        if (stageManager == null)
        {
            GameObject scenarioManager = PrefabUtility.InstantiatePrefab(AssetDatabase.LoadAssetAtPath("Assets/Prefabs/Managers/StandardStageManager.prefab", typeof(GameObject))) as GameObject;
            scenarioManager.transform.position = Vector3.zero;
            text += "scenario Manager created. Check it's values.\n";
        }
        else if(stageManager.transform.parent != null)
        {
            EditorUtility.DisplayDialog("ERROR", "ERROR\nStage Manager must be in the root of the hierarchy", "ERROR");
            return;
        }

        GameObject[] pickeables = GameObject.FindGameObjectsWithTag("Pickeable");
        if (pickeables.Length > 3)
        {
            text += "More than 3 pickeables\n";
        }
        else if (pickeables.Length < 3)
        {
            text += "Less than 3 pickeables\n";
        }

        if (text == "")
        {
            text = "All Ok";
        }

        EditorUtility.DisplayDialog("Scene checked", text, "Ok");
    }

    /// <summary>
    /// Add a quick dummy background to the stage for beta testing purpouse
    /// </summary>
    [MenuItem(MenuName + "/Add dummy background", false, 0)]
    static void AddDummyBackground()
    {
        GameObject gameplay = GameObject.Find("Gameplay");

        if(!gameplay)
        {
            return;
        }

        MeshRenderer[] allMeshes = gameplay.GetComponentsInChildren<MeshRenderer>();
        Bounds bounds = allMeshes[0].bounds;

        for(int i = 1; i < allMeshes.Length; i++)
        {
            bounds.Encapsulate(allMeshes[i].bounds);
        }

        //Debug.Log("Min: " + bounds.min + " - max: " + bounds.max);

        GameObject dummyBackground = GameObject.Find("DummyBackground");
        if(dummyBackground)
        {
            GameObject.DestroyImmediate(dummyBackground);
        }
        
        dummyBackground = new GameObject("DummyBackground");
        dummyBackground.transform.position = Vector3.zero;

        string startPath = "Assets/Prefabs/Editor/DummyBackgroundStart.prefab";
        string partPath = "Assets/Prefabs/Editor/DummyBackgroundPart.prefab";
        string wallPath = "Assets/Prefabs/Editor/DummyBackgroundWall.prefab";
        int backgroundPartHeight = 50;
        int backgroundPartWidth = 100;

        int height = Mathf.CeilToInt(bounds.size.y) > backgroundPartHeight ? Mathf.CeilToInt(bounds.size.y) / backgroundPartHeight + 1 : 1;
        int width = Mathf.CeilToInt(bounds.size.x) > backgroundPartWidth ? Mathf.CeilToInt(bounds.size.x) / backgroundPartWidth + 1 : 1;

        Debug.Log("Size: " + Mathf.CeilToInt(bounds.size.x) + ", " + Mathf.CeilToInt(bounds.size.y) + " num: " + width + ", " + height);

        GameObject start = PrefabUtility.InstantiatePrefab(AssetDatabase.LoadAssetAtPath(startPath, typeof(GameObject))) as GameObject;
        start.transform.SetParent(dummyBackground.transform);
        start.transform.position = new Vector3(-10, 0, 0);

        for(int i = 1; i < width; i++)
        {
            GameObject part = PrefabUtility.InstantiatePrefab(AssetDatabase.LoadAssetAtPath(partPath, typeof(GameObject))) as GameObject;
            part.transform.SetParent(dummyBackground.transform);
            part.transform.position = new Vector3(-10 + backgroundPartWidth * i, 0, 0);
        }

        for(int i = 1; i < height; i++)
        {
            for(int j = 0; j < width; j++)
            {
                GameObject wall = PrefabUtility.InstantiatePrefab(AssetDatabase.LoadAssetAtPath(wallPath, typeof(GameObject))) as GameObject;
                wall.transform.SetParent(dummyBackground.transform);
                wall.transform.position = new Vector3(-10 + backgroundPartWidth * j + backgroundPartWidth / 2, i * backgroundPartHeight + backgroundPartHeight / 2, 25);
            }
        }
    }

    /// <summary>
    /// Show and hidde the mesh renderer of the colliders tagged as EditorColliders.
    /// By default, these are into Gameplay/Colliders
    /// </summary>
    [MenuItem (MenuName + "/Show - Hide Colliders %k", false, 1)]
	static void ShowColliders() {
		// Debug.Log ( "Mostro els colliders" );
		// GameObject colliders = GameObject.Find( "Colliders" );
        foreach (GameObject colliders in GameObject.FindGameObjectsWithTag(Tags.EditorColliders))
        {
            if (colliders.GetComponentInChildren<MeshRenderer>() != null) // Sin� encara �s buit
            {
                bool enabled = colliders.GetComponentInChildren<MeshRenderer>().enabled;
                foreach (Transform t in colliders.GetComponentsInChildren<Transform>())
                {
                    if (t.gameObject.GetComponent<Renderer>() != null)
                    {
                        t.gameObject.GetComponent<Renderer>().enabled = !enabled;

                        if (t.gameObject.GetComponent<DrawPhysics>() == null && enabled)
                        {
                            t.gameObject.AddComponent<DrawPhysics>();
                        }
                        else
                        {
                            if (t.gameObject.GetComponent<DrawPhysics>() != null)
                            {
                                GameObject.DestroyImmediate(t.gameObject.GetComponent<DrawPhysics>());
                            }
                        }
                    }
                }
            }
        }
	}

    /// <summary>
    /// Usefull to find used assets of a certain type
    /// </summary>
    [MenuItem(MenuName + "/Get all assets of type...", false, 10)]
    public static void GetAllAssetsOfType()
    {
        string assetType = ".prefab";

        Object[] objects = GetAssetsOfType<MainMenuManager>(assetType);
        Debug.Log("Start search...");
        foreach (Object obj in objects)
        {
            Debug.Log(obj);
        }

        Debug.Log("Search finished");
    }

    /// <summary>
    /// Used to get assets of a certain type and file extension from entire project
    /// </summary>
    /// <param name="type">The type to retrieve. eg typeof(GameObject).</param>
    /// <param name="fileExtension">The file extention the type uses eg ".prefab".</param>
    /// <returns>An Object array of assets.</returns>
    public static Object[] GetAssetsOfType<T>(string fileExtension)
    {
        List<Object> tempObjects = new List<Object>();
        DirectoryInfo directory = new DirectoryInfo(Application.dataPath);
        FileInfo[] goFileInfo = directory.GetFiles("*" + fileExtension, SearchOption.AllDirectories);

        int i = 0; int goFileInfoLength = goFileInfo.Length;
        FileInfo tempGoFileInfo; string tempFilePath;
        Object tempGO;
        for (; i < goFileInfoLength; i++)
        {
            tempGoFileInfo = goFileInfo[i];
            if (tempGoFileInfo == null)
                continue;

            tempFilePath = tempGoFileInfo.FullName;
            tempFilePath = tempFilePath.Replace(@"\", "/").Replace(Application.dataPath, "Assets");

            //Debug.Log(tempFilePath + "\n" + Application.dataPath);

            tempGO = AssetDatabase.LoadAssetAtPath(tempFilePath, typeof(Object)) as Object;
            if (tempGO == null)
            {
                //Debug.LogWarning("Skipping Null");
                continue;
            }
            else if (tempGO.GetType() == typeof(GameObject) && ((GameObject)tempGO).GetComponent<T>() != null)
            {
                tempObjects.Add(tempGO);
            }
            else
            {
                //Debug.LogWarning("Skipping " + tempGO.GetType().ToString());
            }
        }

        return tempObjects.ToArray();
    }
}
