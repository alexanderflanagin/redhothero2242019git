using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;

/// <summary>
/// Creem un Play propi per a carregar des del menu d'inici per comoditat
/// </summary>
[InitializeOnLoad]
public static class CustomPlayMode
{
    /// <summary>
    /// Al generar la classe, registrem el delegate per a quan fem stop
    /// </summary>
    static CustomPlayMode()
    {
        EditorApplication.playmodeStateChanged += StateChange;
    }

    /// <summary>
    /// Guardem l'escena actual, carreguem el menu i fem play
    /// </summary>
    [MenuItem(LevelMenu.MenuName + "/Play from main menu %0")]
    static void PlayFromMainMenu()
    {
        PlayerPrefs.SetString("old-scene", EditorSceneManager.GetActiveScene().path);

        EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
        EditorSceneManager.OpenScene("Assets/Scenes/GuiScenes/MainMenu.unity");
        EditorApplication.isPlaying = true;
    }

    /// <summary>
    /// Al canviar l'estat del play del editor, mirem si hem de recuperar l'escena que teniem
    /// </summary>
    static void StateChange()
    {
        if (!string.IsNullOrEmpty(PlayerPrefs.GetString("old-scene")) && !EditorApplication.isPlaying)
        {
            EditorApplication.playmodeStateChanged -= StateChange;
            if (!EditorApplication.isPlayingOrWillChangePlaymode)
            {
                //We're in playmode, just about to change playmode to Editor
                Debug.Log("Loading original level: " + PlayerPrefs.GetString("old-scene"));
                EditorSceneManager.OpenScene(PlayerPrefs.GetString("old-scene"));

                PlayerPrefs.DeleteKey("old-scene");
            }
        }
    }
}
