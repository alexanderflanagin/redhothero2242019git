using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

/// <summary>
/// Custom inspectors per a poder editar la info d'escenes mes comodament
/// </summary>
[CustomEditor(typeof(GameDataHolder))]
[CanEditMultipleObjects]
public class GameDataHolderEditor : Editor
{
    /// <summary>
    /// Llista per a mostrar la info d'escenes
    /// </summary>
    private ReorderableList stagesDataList;

    /// <summary>
    /// Property per a la info del gameData
    /// </summary>
    private SerializedProperty gameDataProp;

    /// <summary>
    /// Creem la llista d'escenes i recuperem el gameDataProp
    /// </summary>
    private void OnEnable()
    {
        stagesDataList = new ReorderableList(serializedObject, serializedObject.FindProperty("stagesData"), true, true, true, true);

        stagesDataList.drawHeaderCallback = DrawHeaderCallback;

        stagesDataList.elementHeight = EditorGUIUtility.singleLineHeight * 2 + 4;

        stagesDataList.drawElementCallback =
            (Rect rect, int index, bool isActive, bool isFocused) =>
            {
                DrawElementCallback(stagesDataList, rect, index, isActive, isFocused);
            };
        stagesDataList.onChangedCallback = OnChangedCallback;

        gameDataProp = serializedObject.FindProperty("gameData");
    }

    /// <summary>
    /// Mostrem la info del GameData i la llista d'escenes
    /// </summary>
    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        EditorGUILayout.PropertyField(gameDataProp, true);

        EditorGUILayout.Space();
        EditorGUILayout.Space();
        EditorGUILayout.Space();

        EditorGUILayout.LabelField("Stages data");

        stagesDataList.DoLayoutList();
        serializedObject.ApplyModifiedProperties();
    }

    public static void DrawHeaderCallback(Rect rect)
    {
        EditorGUI.LabelField(
                new Rect(rect.x, rect.y, rect.width * 3 / 5, EditorGUIUtility.singleLineHeight),
                "Scene Name");
        EditorGUI.LabelField(
            new Rect(rect.x + rect.width * 3 / 5, rect.y, rect.width * 1 / 5, EditorGUIUtility.singleLineHeight),
            "Name");
        EditorGUI.LabelField(
            new Rect(rect.x + rect.width * 4 / 5, rect.y, rect.width * 1 / 5, EditorGUIUtility.singleLineHeight),
            "Speedrun");
        EditorGUI.LabelField(
            new Rect(rect.x, rect.y + EditorGUIUtility.singleLineHeight + 2, rect.width / 2, EditorGUIUtility.singleLineHeight),
            "Pickeables Order");
        EditorGUI.LabelField(
            new Rect(rect.x + rect.width / 2, rect.y + EditorGUIUtility.singleLineHeight + 2, rect.width / 2, EditorGUIUtility.singleLineHeight),
            "World Map Image");
    }

    public static void DrawElementCallback(ReorderableList list, Rect rect, int index, bool isActive, bool isFocused)
    {
        var element = list.serializedProperty.GetArrayElementAtIndex(index);
        int themeId = System.Array.IndexOf(GetAllThemesIDs(), element.FindPropertyRelative("theme").intValue);
        if(themeId < 0)
        {
            themeId = 0;
        }

        rect.y += 2;
        EditorGUI.LabelField(new Rect(rect.x, rect.y, 30, EditorGUIUtility.singleLineHeight),
            element.FindPropertyRelative("enviro").intValue + "-" + element.FindPropertyRelative("inEnviroPos").intValue);
        EditorGUI.PropertyField(
            new Rect(rect.x + 30, rect.y, rect.width * 3 / 5 - 30, EditorGUIUtility.singleLineHeight),
            element.FindPropertyRelative("sceneName"), GUIContent.none);
        EditorGUI.PropertyField(
            new Rect(rect.x + rect.width * 3 / 5, rect.y, rect.width * 1 / 5, EditorGUIUtility.singleLineHeight),
            element.FindPropertyRelative("name"), GUIContent.none);
        EditorGUI.PropertyField(
            new Rect(rect.x + rect.width * 4 / 5, rect.y, rect.width * 1 / 5, EditorGUIUtility.singleLineHeight),
            element.FindPropertyRelative("speedrunTime"), GUIContent.none);
        EditorGUI.PropertyField(
            new Rect(rect.x, rect.y + EditorGUIUtility.singleLineHeight + 2, rect.width / 3, EditorGUIUtility.singleLineHeight),
            element.FindPropertyRelative("pickeablesOrder"), GUIContent.none);
        EditorGUI.PropertyField(
            new Rect(rect.x + rect.width / 3, rect.y + EditorGUIUtility.singleLineHeight + 2, rect.width / 3, EditorGUIUtility.singleLineHeight),
            element.FindPropertyRelative("worldMapImage"), GUIContent.none);
        themeId = EditorGUI.Popup(
            new Rect(rect.x + 2 * rect.width / 3, rect.y + EditorGUIUtility.singleLineHeight + 2, rect.width / 3, EditorGUIUtility.singleLineHeight),
            themeId, GetAllThemes());
        element.FindPropertyRelative("theme").intValue = GetAllThemesIDs()[themeId];
    }

    public static string[] GetAllThemes()
    {
        AudioClipsData audioClipsData = AssetDatabase.LoadAssetAtPath(AudioClipsDataWindow.path, typeof(AudioClipsData)) as AudioClipsData;

        string[] themes = new string[audioClipsData.themes.Length];

        for(int i = 0; i < audioClipsData.themes.Length; i++)
        {
            themes[i] = audioClipsData.themes[i].name;
        }

        return themes;
    }

    public static int[] GetAllThemesIDs()
    {
        AudioClipsData audioClipsData = AssetDatabase.LoadAssetAtPath(AudioClipsDataWindow.path, typeof(AudioClipsData)) as AudioClipsData;

        int[] IDs = new int[audioClipsData.themes.Length];

        for (int i = 0; i < audioClipsData.themes.Length; i++)
        {
            IDs[i] = audioClipsData.themes[i].ID;
        }

        return IDs;

    }

    /// <summary>
    /// On change something in the list, save order and enviro/stage info in the DataHolder
    /// </summary>
    /// <param name="list"></param>
    public static void OnChangedCallback(ReorderableList list)
    {
        string path = "Assets/Resources/Data/EditorConfig.asset";
        GameDataHolder gdh = AssetDatabase.LoadAssetAtPath(path, typeof(GameDataHolder)) as GameDataHolder;

        if(list.count > gdh.stagesData.Length)
        {
            // Estic afegint, no faig res
            return;
        }
        
        for (int i = 0; i < gdh.stagesData.Length; i++)
        {
            int enviro = (i / EnviroData.STAGES_PER_ENVIRO);
            int stage = i % EnviroData.STAGES_PER_ENVIRO;

            if (enviro >= EnviroData.ENVIROS)
            {
                gdh.stagesData[i].enviro = 0;
                gdh.stagesData[i].inEnviroPos = 0;
                continue;
            }

            var element = list.serializedProperty.GetArrayElementAtIndex(i);
            string sceneName = element.FindPropertyRelative("sceneName").stringValue;

            gdh.stagesData[i].enviro = enviro + 1;
            gdh.stagesData[i].inEnviroPos = stage + 1;
            gdh.stagesOrdered[enviro].stages[stage] = i;
        }
                

        EditorUtility.SetDirty(gdh);
    }
}
