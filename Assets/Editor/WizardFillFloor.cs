using UnityEngine;
using UnityEditor;
using System.Collections;

public class WizardFillFloor : ScriptableWizard {

    public int elementIndex;

	/// <summary>
	/// El parent del GameObject amb els prefabs dintre
	/// </summary>
	public GameObject parent;

	/// <summary>
	/// El nom del GameObject que es creara (si no es posa, es fara servir un nom per default)
	/// </summary>
	public string gameObjectName;

	/// <summary>
	/// Hem d'afegir pilons?
	/// </summary>
	public bool addPilons;

	[MenuItem(LevelMenu.MenuName + "/Fill floor coast #%h")]
	static void CreateWizard() {
		ScriptableWizard.DisplayWizard<WizardFillFloor>("Fill the floor", "Fill");
	}

	void OnWizardCreate () {
		if( parent == null ) {
			parent = GameObject.Find ("Decorations");
			// EditorUtility.DisplayDialog( "No has afegit un pare", "Es farà servir per defecte el primer GameObject anomenat Decorations", "Ok" );
		}

		if( Selection.activeGameObject == null ) {
			EditorUtility.DisplayDialog( "No tens sel·leccionat cap GameObject", "Si us plau, sel·lecciona el GameObject del qual vulguis crear els terres.", "Ok" );
			return;
		}

		if (Selection.activeGameObject.layer != Layers.Ground && Selection.activeGameObject.layer != Layers.OneWayPlatform)
        {
			EditorUtility.DisplayDialog( "El GameObject sel·leccionat no es un terra", "Bernardo, sel·lecciona un Floor des d'on crear el terra, que sino despres no funciona :P", "Ok" );
			return;
		}

        FillFloor.Fill(parent, gameObjectName, addPilons, elementIndex);
	}

	void OnWizardUpdate () {
		helpString = "Fill a ground collider object with wood planks.";
	}
}
