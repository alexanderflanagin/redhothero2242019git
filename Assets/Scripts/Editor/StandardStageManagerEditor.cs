using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.SceneManagement;

[CustomEditor(typeof(StandardStageManager))]
public class StandardStageManagerEditor : Editor
{
    private int batteriesNeeded;

    private float speedrunTime;

    private int forcedMinimumBatteries;

    private int forcedBatteries;

    // Comentat perque de moment no es fa servir
    //private bool hasFusePart;

    private StandardStageManager.PickeablesOrder pickeablesOrder;

    void OnEnable()
    {
        string path = "Assets/Resources/Data/EditorConfig.asset";
        GameDataHolder gdh = AssetDatabase.LoadAssetAtPath(path, typeof(GameDataHolder)) as GameDataHolder;

        name = SceneManager.GetActiveScene().name;

        if (gdh)
        {
            foreach (LevelData ld in gdh.stagesData)
            {
                if (ld.sceneName == name)
                {
                    this.name = ld.sceneName;
                    this.batteriesNeeded = ld.cellsNeeded;
                    this.speedrunTime = ld.speedrunTime;
                    this.forcedMinimumBatteries = ld.forcedMinimumBatteries;
                    this.forcedBatteries = ld.forcedBatteries;
                    this.pickeablesOrder = ld.pickeablesOrder;
                }
            }
        }
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        if (GUILayout.Button("Get Data"))
        {
            string path = "Assets/Resources/Data/EditorConfig.asset";
            GameDataHolder gdh = AssetDatabase.LoadAssetAtPath(path, typeof(GameDataHolder)) as GameDataHolder;

            string[] scenePath = EditorApplication.currentScene.Split(char.Parse("/"));
            name = scenePath[scenePath.Length - 1].Replace(".unity", "");

            if (!gdh)
            {
                EditorUtility.DisplayDialog("No hi ha l'objecte EditorConfig", "El pots crear a través del menú SRHH Data/Select editor game data", "Ok");
            }
            else
            {
                bool founded = false;

                //Debug.Log("Loaded level name: " + Application.loadedLevelName);

                foreach (LevelData ld in gdh.stagesData)
                {
                    //Debug.Log(ld.sceneName);

                    if (ld.sceneName == name)
                    {
                        founded = true;
                        this.name = ld.sceneName;
                        this.batteriesNeeded = ld.cellsNeeded;
                        this.speedrunTime = ld.speedrunTime;
                        this.forcedMinimumBatteries = ld.forcedMinimumBatteries;
                        this.forcedBatteries = ld.forcedBatteries;
                        this.pickeablesOrder = ld.pickeablesOrder;
                    }
                }

                if (!founded)
                {
                    EditorUtility.DisplayDialog("No tenim dades d'aquesta pantalla", "Quan guardis les dades, es crearà una nova entrada per a aquesta pantalla", "Ok");
                }
            }
        }

        EditorGUILayout.LabelField("Name: " + this.name);

        this.batteriesNeeded = EditorGUILayout.IntSlider("Batteries needed", this.batteriesNeeded, 0, 21);

        this.forcedMinimumBatteries = EditorGUILayout.IntSlider("Forced minimum batteries", this.forcedMinimumBatteries, 0, 21);

        this.forcedBatteries = EditorGUILayout.IntSlider("Forced batteries", this.forcedBatteries, 0, 21);

        this.speedrunTime = EditorGUILayout.FloatField("Speedrun time", this.speedrunTime);

        this.pickeablesOrder = (StandardStageManager.PickeablesOrder)EditorGUILayout.EnumPopup("Pickeables order", this.pickeablesOrder);

        if (GUILayout.Button("Save Data"))
        {
            string path = "Assets/Resources/Data/EditorConfig.asset";
            GameDataHolder gdh = AssetDatabase.LoadAssetAtPath(path, typeof(GameDataHolder)) as GameDataHolder;

            if (!gdh)
            {
                GameData gd = new GameData();
                LevelData[] lsd = new LevelData[0];
                gdh = new GameDataHolder(gd, lsd);
                AssetDatabase.CreateAsset(gdh, path);
                AssetDatabase.SaveAssets();
            }

            bool founded = false;

            foreach (LevelData ld in gdh.stagesData)
            {
                if (ld.sceneName == name)
                {
                    founded = true;
                    ld.cellsNeeded = this.batteriesNeeded;
                    ld.speedrunTime = this.speedrunTime;
                    ld.forcedMinimumBatteries = this.forcedMinimumBatteries;
                    ld.forcedBatteries = this.forcedBatteries;
                    ld.pickeablesOrder = this.pickeablesOrder;
                }
            }

            if (!founded)
            {
                LevelData ld = new LevelData();
                ld.sceneName = name;
                ld.cellsNeeded = this.batteriesNeeded;
                ld.speedrunTime = this.speedrunTime;
                ld.forcedMinimumBatteries = this.forcedMinimumBatteries;
                ld.forcedBatteries = this.forcedBatteries;
                ld.pickeablesOrder = this.pickeablesOrder;

                List<LevelData> list = new List<LevelData>();
                for (int i = 0; i < gdh.stagesData.Length; i++)
                {
                    list.Add(gdh.stagesData[i]);
                }

                list.Add(ld);

                gdh.stagesData = list.ToArray();
            }
        }
    }
}
