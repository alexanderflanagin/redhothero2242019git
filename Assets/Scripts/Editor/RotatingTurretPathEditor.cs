﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(RotatingTurretPath))]
public class RotatingTurretPathEditor : CircularMovingPathEditor
{
    protected override void OnSceneGUI()
    {
        base.OnSceneGUI();
    }
}
