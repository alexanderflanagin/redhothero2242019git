using UnityEngine;
using UnityEditor;
using System;
using System.Reflection;
using System.Collections;

[CustomEditor(typeof(LinearMovingPath))]
public class LinearMovingPathEditor : Editor {

    // public override void 
    float distance = 0;

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        GUILayout.Label("\nTotal distance: " + distance);
        if (GUILayout.Button("Calculate total distance"))
        {
            distance = CalculateDistance();
        }
        if (GUILayout.Button("Auto-calculate distance"))
        {
            LinearMovingPath movingPlatform = this.target as LinearMovingPath;
            distance = CalculateDistance();

            movingPlatform.distance = this.distance / movingPlatform.numElements;
            EditorUtility.SetDirty(movingPlatform);
        }

        GUILayout.Label("\nNavigation points:");
        if (GUILayout.Button("Add point at platform position"))
        {
            AddNewPoint();
        }

        if (GUILayout.Button("Remove last navigation point"))
        {
            RemoveLastPoint();
        }

        GUILayout.Label("\nNavigation Element:");
        if(GUILayout.Button("Instantiate element"))
        {
            // Do something
            LinearMovingPath movingPlatform = this.target as LinearMovingPath;

            if(movingPlatform.gObject != null)
            {
                // Primer eliminem l'antic
                if (movingPlatform.instantiatedGameObject != null)
                {
                    DestroyImmediate(movingPlatform.instantiatedGameObject);
                }

                movingPlatform.instantiatedGameObject = GameObject.Instantiate(movingPlatform.gObject, movingPlatform.platform.transform.position, Quaternion.identity) as GameObject;
                movingPlatform.instantiatedGameObject.name = "Moving Platform - " + movingPlatform.instantiatedGameObject.name;
            }
        }

        if (GUILayout.Button("Remove instantiated element"))
        {
            // Do something
            LinearMovingPath movingPlatform = this.target as LinearMovingPath;

            if (movingPlatform.instantiatedGameObject != null)
            {
                DestroyImmediate(movingPlatform.instantiatedGameObject);
            }
        }
    }

    private void AddNewPoint()
    {
        LinearMovingPath movingPlatform = this.target as LinearMovingPath;

        Vector3 newPoint = Vector3.zero;

        Vector3[] points = movingPlatform.navigationPoints;

        newPoint = movingPlatform.platform.transform.localPosition;

        if (points == null)
        {
            points = new Vector3[1];
        }
        else
        {
            Array.Resize(ref points, points.Length + 1);
        }

        points[points.Length - 1] = newPoint;

        movingPlatform.navigationPoints = points;

        EditorUtility.SetDirty(movingPlatform);
    }

    /// <summary>
    /// Elimina el darrer punt dels navigation points i repinta l'escena
    /// </summary>
    private void RemoveLastPoint()
    {
        LinearMovingPath movingPlatform = this.target as LinearMovingPath;

        if (movingPlatform.navigationPoints.Length > 0)
        {
            Array.Resize(ref movingPlatform.navigationPoints, movingPlatform.navigationPoints.Length - 1);
        }

        SceneView.RepaintAll();
    }

    private float CalculateDistance()
    {
        float distance = 0;

        LinearMovingPath movingPlatform = this.target as LinearMovingPath;

        int i;
        for(i = 0; i < movingPlatform.navigationPoints.Length - 1; i++)
        {
            distance += Vector3.Distance(movingPlatform.navigationPoints[i], movingPlatform.navigationPoints[i + 1]);
        }

        if (movingPlatform.closeCircuit)
        {
            distance += Vector3.Distance(movingPlatform.navigationPoints[i], movingPlatform.navigationPoints[0]);
        }

        return distance;
    }


    /// <summary>
    /// Dibuixa unes axis per a moure elements per a la plataforma i per a cada punt dels navigation points
    /// Actualitza la posici� d'aquests si es fan servir les axis
    /// </summary>
    protected virtual void OnSceneGUI()
    {
        LinearMovingPath movingPlatform = this.target as LinearMovingPath;
        Vector3 newPosition;

        for(int i = 0; i < movingPlatform.navigationPoints.Length; i++)
        {
            EditorGUI.BeginChangeCheck();
            newPosition = Handles.PositionHandle(movingPlatform.navigationPoints[i] + movingPlatform.transform.position, Quaternion.identity) - movingPlatform.transform.position;

            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(movingPlatform, "Move point");
                movingPlatform.navigationPoints[i] = newPosition;
                EditorUtility.SetDirty(movingPlatform);
            }
        }
        
        EditorGUI.BeginChangeCheck();
        newPosition = Handles.PositionHandle(movingPlatform.platform.transform.position, Quaternion.identity);

        if(EditorGUI.EndChangeCheck())
        {
            Undo.RecordObject(movingPlatform, "Move point");
            movingPlatform.platform.transform.position = newPosition;
            EditorUtility.SetDirty(movingPlatform);
        }

        if(Handles.Button(movingPlatform.platform.transform.position + new Vector3(1.5f, 1.5f), Quaternion.identity, 0.5f, 0.5f, Handles.CubeCap))
        {
            if (movingPlatform.navigationPoints == null)
            {
                movingPlatform.navigationPoints = new Vector3[0];
            }

            if (movingPlatform.navigationPoints.Length == 0)
            {
                AddNewPoint();
            }
            else if (movingPlatform.platform.transform.position != movingPlatform.navigationPoints[movingPlatform.navigationPoints.Length - 1])
            {
                AddNewPoint();
            }
        }
    }
}
