using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(AudioClipAttribute))]
public class AudioClipDrawer : PropertyDrawer
{
    AudioClipsData audioClipsData;

    AudioClipAttribute audioClipAttribute
    {
        get
        {
            return ((AudioClipAttribute)attribute);
        }
    }

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        if(audioClipsData == null)
        {
            audioClipsData = AssetDatabase.LoadAssetAtPath(AudioClipsDataWindow.path, typeof(AudioClipsData)) as AudioClipsData;
        }

        List<string> names = new List<string>();

        if(audioClipAttribute.prefix == AudioClipsData.ClipsGroups.Player)
        {
            for (int i = 0; i < audioClipsData.playerClips.Length; i++)
            {
                names.Add(audioClipsData.playerClips[i].name);
            }
        }

        if (audioClipAttribute.prefix == AudioClipsData.ClipsGroups.Enemies)
        {
            for (int i = 0; i < audioClipsData.enemiesClips.Length; i++)
            {
                names.Add(audioClipsData.enemiesClips[i].name);
            }
        }

        if (audioClipAttribute.prefix == AudioClipsData.ClipsGroups.Enviro)
        {
            for (int i = 0; i < audioClipsData.enviroClips.Length; i++)
            {
                names.Add(audioClipsData.enviroClips[i].name);
            }
        }

        // Using BeginProperty / EndProperty on the parent property means that
        // prefab override logic works on the entire property.
        EditorGUI.BeginProperty(position, label, property);

        // Draw label
        position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

        // Don't make child fields be indented
        var indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        int index = ArrayUtility.IndexOf(names.ToArray(), property.stringValue);

        EditorGUI.BeginChangeCheck();
        int id = EditorGUI.Popup(position, index, names.ToArray());
        if (EditorGUI.EndChangeCheck())
        {
            property.stringValue = names[id];
        }

        // Set indent back to what it was
        EditorGUI.indentLevel = indent;

        EditorGUI.EndProperty();
    }
}
