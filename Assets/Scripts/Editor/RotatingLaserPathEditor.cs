﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(RotatingLaserPath))]
public class RotatingLaserPathEditor : CircularMovingPathEditor
{
    protected override void OnSceneGUI()
    {
        base.OnSceneGUI();
    }
}
