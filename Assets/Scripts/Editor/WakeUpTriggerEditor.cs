using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(WakeUpTrigger))]
public class WakeUpTriggerEditor : Editor
{
    /// <summary>
    /// Prefix que li posem a l'string del buffer, per a testejar que es una posicio de trigger
    /// </summary>
    public const string BUFFER_PREFIX = "WAKEUPTRIGGERPOSITION:";

    /// <summary>
    /// Afegim botons de copiar i enganxar els punts del trigger
    /// </summary>
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        if (GUILayout.Button("Randomize Color"))
        {
            ((WakeUpTrigger)this.target).color = new Color(UnityEngine.Random.Range(0f, 1f), UnityEngine.Random.Range(0f, 1f), UnityEngine.Random.Range(0f, 1f));
        }

        GUILayout.Label("Trigger Position");

        if (GUILayout.Button("Copy points"))
        {
            Copy();
        }

        if(GUILayout.Button("Paste points"))
        {
            Paste();
        }
    }

    /// <summary>
    /// Dibuixa uns punts per a agafar i moure el trigger de WakeUp
    /// </summary>
    void OnSceneGUI()
    {
        WakeUpTrigger wakeUpTrigger = target as WakeUpTrigger;

        Vector3 max, min, originalMax, originalMin;
        BoxCollider2D collider = wakeUpTrigger.GetComponent<BoxCollider2D>();

        if (collider != null)
        {
            originalMax = collider.bounds.max;
            originalMin = collider.bounds.min;

            EditorGUI.BeginChangeCheck();
            max = Handles.PositionHandle(originalMax, Quaternion.identity);
            min = Handles.PositionHandle(originalMin, Quaternion.identity);

            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(collider, "Move point");

                if (max != originalMax)
                {
                    collider.offset = collider.offset + (Vector2)(wakeUpTrigger.transform.rotation * ((max - collider.bounds.max) / 2));
                    collider.size = max - min;
                }
                if (min != originalMin)
                {
                    collider.offset = collider.offset + (Vector2)(wakeUpTrigger.transform.rotation * ((min - collider.bounds.min) / 2));
                    collider.size = max - min;
                }

                // Make sure to call SetDirty otherwise the inspector will not know that the script properties were modified
                EditorUtility.SetDirty(collider);
            }
        }
    }

    /// <summary>
    /// Copiem els punts minim i maxim del collider al portapapers
    /// </summary>
    private void Copy()
    {
        BoxCollider2D collider = ((WakeUpTrigger)this.target).GetComponent<BoxCollider2D>();
        EditorGUIUtility.systemCopyBuffer = SerializePosition(collider.bounds.min, collider.bounds.max);
    }

    /// <summary>
    /// Enganxem (si existeixen) els punts minim i maxim del portapapers
    /// </summary>
    private void Paste()
    {
        Vector2 min, max;

        if(DeserializePosition(EditorGUIUtility.systemCopyBuffer, out min, out max))
        {
            BoxCollider2D collider = ((WakeUpTrigger)this.target).GetComponent<BoxCollider2D>();

            if (collider != null)
            {
                Bounds bounds = new Bounds();
                bounds.SetMinMax(min, max);
                collider.size = bounds.size;
                collider.offset = bounds.center - collider.transform.position;
            }
        }
    }

    /// <summary>
    /// Serialitzem dos vectors en el format propi del Wake up trigger
    /// </summary>
    /// <param name="min">Punt minim del collider</param>
    /// <param name="max">Punt maxim del collider</param>
    /// <returns>Posicions serialitzades</returns>
    private string SerializePosition(Vector2 min, Vector2 max)
    {
        return BUFFER_PREFIX + min.x + "," + min.y + "|" + max.x + "," + max.y;
    }

    /// <summary>
    /// De-serialitzem els punts minim i maxim d'un collider segons un string serialitzat
    /// </summary>
    /// <param name="serializedPosition">String serialitzat dels punts</param>
    /// <param name="min">Minim del collider</param>
    /// <param name="max">Maxim del collider</param>
    /// <returns>Cert si la de-serialitzacio ha funcionat</returns>
    private bool DeserializePosition(string serializedPosition, out Vector2 min, out Vector2 max)
    {
        min = Vector2.zero;
        max = Vector2.zero;

        if (!serializedPosition.StartsWith(BUFFER_PREFIX))
        {
            Debug.LogWarning("No points copied");
            return false;
        }

        serializedPosition = serializedPosition.Remove(0, BUFFER_PREFIX.Length);

        string[] vectors = serializedPosition.Split('|');
        string[] minString = vectors[0].Split(',');
        string[] maxString = vectors[1].Split(',');
        min = new Vector2(float.Parse(minString[0]), float.Parse(minString[1]));
        max = new Vector2(float.Parse(maxString[0]), float.Parse(maxString[1]));

        return true;
    }
}
