using UnityEditor;

public class CreateGameDataHolder
{
    [MenuItem("SRHH Data/Edit game data")]
    public static void SelectGlobalDataHolder()
    {
        string path = "Assets/Resources/Data/EditorConfig.asset";
        GameDataHolder gdh = AssetDatabase.LoadAssetAtPath(path, typeof(GameDataHolder)) as GameDataHolder;

        if (!gdh)
        {
            GameData gd = new GameData();
            LevelData[] lsd = new LevelData[0];
            gdh = new GameDataHolder(gd, lsd);
            AssetDatabase.CreateAsset(gdh, path);
            AssetDatabase.SaveAssets();
        }

        EditorUtility.FocusProjectWindow();
        Selection.activeObject = gdh;
    }

    [MenuItem("SRHH Data/Edit player data")]
    public static void CreatePlayerHolder()
    {
        string path = "Assets/Resources/Data/PlayerDataHolder.asset";
        PlayerDataHolder pdh = AssetDatabase.LoadAssetAtPath(path, typeof(PlayerDataHolder)) as PlayerDataHolder;

        if (!pdh)
        {
            pdh = new PlayerDataHolder();
            AssetDatabase.CreateAsset(pdh, path);
            AssetDatabase.SaveAssets();
        }
        
        EditorUtility.FocusProjectWindow();
        Selection.activeObject = pdh;
    }

    [MenuItem("SRHH Data/Edit FillFloor prefabs")]
    public static void CreateFillFloorDataHolder()
    {
        string path = "Assets/Resources/Data/FillFloorDataHolder.asset";
        FillFloorDataHolder ffdh = AssetDatabase.LoadAssetAtPath(path, typeof(FillFloorDataHolder)) as FillFloorDataHolder;

        if (!ffdh)
        {
            ffdh = new FillFloorDataHolder();
            AssetDatabase.CreateAsset(ffdh, path);
            AssetDatabase.SaveAssets();
        }

        EditorUtility.FocusProjectWindow();
        Selection.activeObject = ffdh;
    }

    /// <summary>
    /// Creem/sel.leccionem el data holder amb la relacio entre Tiled i prefabs
    /// </summary>
    [MenuItem("SRHH Data/Edit Tiled prefabs")]
    public static void TiledDataHolder()
    {
        string path = "Assets/Resources/Data/TiledDataHolder.asset";
        TiledData tiledData = AssetDatabase.LoadAssetAtPath(path, typeof(TiledData)) as TiledData;

        if (!tiledData)
        {
            tiledData = new TiledData();
            AssetDatabase.CreateAsset(tiledData, path);
            AssetDatabase.SaveAssets();
        }

        EditorUtility.FocusProjectWindow();
        Selection.activeObject = tiledData;
    }

    [MenuItem("SRHH Data/Open HopProperties script")]
    public static void OpenHopDataScript()
    {
        string path = "Assets/Scripts/GameScripts/Gameplay/Enums/HopProperties.cs";

        var script = AssetDatabase.LoadAssetAtPath(path, typeof(UnityEngine.Object));
        AssetDatabase.OpenAsset(script, 12);
    }

    [MenuItem("SRHH Data/Open JumpProperties script")]
    public static void OpenJumpDataScript()
    {
        string path = "Assets/Scripts/GameScripts/Gameplay/Enums/JumpProperties.cs";

        var script = AssetDatabase.LoadAssetAtPath(path, typeof(UnityEngine.Object));
        AssetDatabase.OpenAsset(script, 12);
    }

    [MenuItem("SRHH Data/Open MovementProperties script")]
    public static void OpenMovementDataScript()
    {
        string path = "Assets/Scripts/GameScripts/Gameplay/Enums/MovementProperties.cs";

        var script = AssetDatabase.LoadAssetAtPath(path, typeof(UnityEngine.Object));
        AssetDatabase.OpenAsset(script, 12);
    }
}
