using UnityEngine;
using UnityEditor;
using Com.LuisPedroFonseca.ProCamera2D;

/// <summary>
/// Serveix per a afegir un botó que centri la càmera del scroll vertical segons les línies de posició
/// </summary>
[CustomEditor(typeof(VerticalCenterLines))]
public class VerticalCenterLinesEditor : Editor {

    /// <summary>
    /// Al fer click a "Center Camera", aquesta es posarà automàticament a on començaria la pantalla
    /// </summary>
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        if (GUILayout.Button("Center Camera"))
        {
            ProCamera2D camera = FindObjectOfType<ProCamera2D>();
            if (camera == null)
            {
                Debug.LogError("Camera not found");
            }
            else
            {
                VerticalCenterLines centerLines = FindObjectOfType<VerticalCenterLines>();

                float distance = (centerLines.lateralDistance * 2 / Camera.main.aspect) * 0.5f / Mathf.Tan(Camera.main.fieldOfView * 0.5f * Mathf.Deg2Rad);

                camera.transform.position = new Vector3(centerLines.center.position.x, 0, -distance);

                ProCamera2DNumericBoundaries numericBoundaries = camera.transform.GetOrAddComponent<ProCamera2DNumericBoundaries>();

                Debug.Log("Tenim numeric boundaries: " + numericBoundaries);

                Debug.Log("Center lines center X: " + centerLines.center.transform.position.x);
                numericBoundaries.LeftBoundary = centerLines.center.transform.position.x - centerLines.lateralDistance;
                Debug.Log("Left Boundary: " + numericBoundaries.LeftBoundary);
                numericBoundaries.RightBoundary = centerLines.center.transform.position.x + centerLines.lateralDistance;
                Debug.Log("Right Boundary: " + numericBoundaries.RightBoundary);

                Selection.activeGameObject = camera.gameObject;
            }
        }
    }
}
