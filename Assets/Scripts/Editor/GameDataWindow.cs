using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

public class GameDataWindow : EditorWindow
{
    static string path = "Assets/Resources/Data/EditorConfig.asset";

    GameDataHolder gameData;

    ReorderableList stagesData;

    SerializedObject serializedObject;

    Vector2 scrollPos;

    [MenuItem("SRHH Data/Stages Order Window")]
    static void Init()
    {
        GameDataWindow window = CreateInstance<GameDataWindow>();
        window.titleContent = new GUIContent("Stages Order");
        window.Show();

        GameDataHolder gameData = AssetDatabase.LoadAssetAtPath(path, typeof(GameDataHolder)) as GameDataHolder;

        if (!gameData)
        {
            GameData gd = new GameData();
            LevelData[] lsd = new LevelData[0];
            gameData = new GameDataHolder(gd, lsd);
            AssetDatabase.CreateAsset(gameData, path);
            AssetDatabase.SaveAssets();
        }
    }

    private void Awake()
    {
        gameData = AssetDatabase.LoadAssetAtPath(path, typeof(GameDataHolder)) as GameDataHolder;
        serializedObject = new SerializedObject(gameData);

        stagesData = new ReorderableList(serializedObject, serializedObject.FindProperty("stagesData"), true, true, true, true);

        stagesData.drawHeaderCallback = GameDataHolderEditor.DrawHeaderCallback;

        stagesData.elementHeight = EditorGUIUtility.singleLineHeight * 2 + 4;

        stagesData.drawElementCallback =
            (Rect rect, int index, bool isActive, bool isFocused) =>
            {
                GameDataHolderEditor.DrawElementCallback(stagesData, rect, index, isActive, isFocused);
            };
        stagesData.onChangedCallback = GameDataHolderEditor.OnChangedCallback;
    }

    private void OnGUI()
    {
        if (serializedObject == null)
        {
            Awake();
        }

        if (serializedObject != null)
        {
            EditorGUILayout.Space();
            EditorGUILayout.Space();
            EditorGUILayout.BeginVertical(EditorStyles.inspectorDefaultMargins);
            scrollPos = EditorGUILayout.BeginScrollView(scrollPos, false, false);
            serializedObject.Update();
            stagesData.DoLayoutList();
            serializedObject.ApplyModifiedProperties();
            EditorGUILayout.EndScrollView();
            EditorGUILayout.EndVertical();
        }
    }
}
