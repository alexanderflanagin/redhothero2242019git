using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(CircularMovingPath))]
public class CircularMovingPathEditor : Editor
{
    float perimeter;

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        GUILayout.Label("\nPerimeter: " + this.perimeter);
        if (GUILayout.Button("Calculate total perimeter"))
        {
            perimeter = CalculateRadius();
        }

        if(GUILayout.Button("Auto-calculate distance"))
        {
            CircularMovingPath movingPathCircular = this.target as CircularMovingPath;
            perimeter = CalculateRadius();

            movingPathCircular.distance = this.perimeter / movingPathCircular.numElements;
            EditorUtility.SetDirty(movingPathCircular);
        }

    }

    private float CalculateRadius()
    {
        CircularMovingPath movingPathCircular = this.target as CircularMovingPath;
        float radius = Mathf.Sqrt(Mathf.Pow((movingPathCircular.radiusLocalPosition).x, 2) + Mathf.Pow((movingPathCircular.radiusLocalPosition).y, 2));
        return radius * 2 * Mathf.PI;
    }

    protected virtual void OnSceneGUI()
    {
        CircularMovingPath movingPlatformCircular = this.target as CircularMovingPath;

        Vector3 newPosition;
        EditorGUI.BeginChangeCheck();
        newPosition = Handles.PositionHandle(movingPlatformCircular.transform.position + movingPlatformCircular.radiusLocalPosition, Quaternion.identity);

        if (EditorGUI.EndChangeCheck())
        {
            Undo.RecordObject(movingPlatformCircular, "Move radius");
            movingPlatformCircular.radiusLocalPosition = newPosition - movingPlatformCircular.transform.position;
            EditorUtility.SetDirty(movingPlatformCircular);
        }
    }
}
