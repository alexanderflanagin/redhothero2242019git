using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

public class AudioClipsDataWindow : EditorWindow
{
    public static string path = "Assets/Resources/Data/AudioData.asset";

    AudioClipsData audioClipsData;

    ReorderableList playerClips;

    ReorderableList enviroClips;

    ReorderableList enemiesClips;

    ReorderableList themes;

    SerializedObject serializedObject;

    Vector2 scrollPos;

    bool showPlayerClips = true;

    bool showEnemiesClips = true;

    bool showEnviroClips = true;

    bool showThemes = true;

    [MenuItem("SRHH Data/Audio data")]
    static void Init()
    {
        AudioClipsDataWindow window = (AudioClipsDataWindow)ScriptableObject.CreateInstance(typeof(AudioClipsDataWindow));
        window.Show();

        AudioClipsData audioClipsData = AssetDatabase.LoadAssetAtPath(path, typeof(AudioClipsData)) as AudioClipsData;

        if (!audioClipsData)
        {
            audioClipsData = new AudioClipsData();
            AssetDatabase.CreateAsset(audioClipsData, path);
            AssetDatabase.SaveAssets();
        }
    }

    private void Awake()
    {
        audioClipsData = AssetDatabase.LoadAssetAtPath(path, typeof(AudioClipsData)) as AudioClipsData;
        serializedObject = new SerializedObject(audioClipsData);

        playerClips = new ReorderableList(serializedObject, serializedObject.FindProperty("playerClips"), true, true, true, true);
        playerClips.drawHeaderCallback = drawHeaderCallback;
        playerClips.elementHeight = EditorGUIUtility.singleLineHeight * 2 + 4;
        playerClips.drawElementCallback =
        (Rect rect, int index, bool isActive, bool isFocused) =>
        {
            var element = playerClips.serializedProperty.GetArrayElementAtIndex(index);
            drawElementCallback(element, rect);
            
        };
        playerClips.onAddCallback = addElementCallback;

        enemiesClips = new ReorderableList(serializedObject, serializedObject.FindProperty("enemiesClips"), true, true, true, true);
        enemiesClips.drawHeaderCallback = drawHeaderCallback;
        enemiesClips.elementHeight = EditorGUIUtility.singleLineHeight * 2 + 4;
        enemiesClips.drawElementCallback =
        (Rect rect, int index, bool isActive, bool isFocused) =>
        {
            var element = enemiesClips.serializedProperty.GetArrayElementAtIndex(index);
            drawElementCallback(element, rect);

        };
        enemiesClips.onAddCallback = addElementCallback;

        enviroClips = new ReorderableList(serializedObject, serializedObject.FindProperty("enviroClips"), true, true, true, true);
        enviroClips.drawHeaderCallback = drawHeaderCallback;
        enviroClips.elementHeight = EditorGUIUtility.singleLineHeight * 2 + 4;
        enviroClips.drawElementCallback =
        (Rect rect, int index, bool isActive, bool isFocused) =>
        {
            var element = enviroClips.serializedProperty.GetArrayElementAtIndex(index);
            drawElementCallback(element, rect);

        };
        enviroClips.onAddCallback = addElementCallback;

        themes = new ReorderableList(serializedObject, serializedObject.FindProperty("themes"), true, true, true, true);
        themes.drawHeaderCallback = drawHeaderCallback;
        themes.elementHeight = EditorGUIUtility.singleLineHeight * 2 + 4;
        themes.drawElementCallback =
        (Rect rect, int index, bool isActive, bool isFocused) =>
        {
            var element = themes.serializedProperty.GetArrayElementAtIndex(index);
            drawElementCallback(element, rect);

        };

        themes.onAddCallback = (ReorderableList list) =>
        {

            addElementCallback(list);

            // Li posem un ID superior al anterior
            serializedObject.FindProperty("themesCount").intValue++;

            int index = list.serializedProperty.arraySize;
            SerializedProperty element = list.serializedProperty.GetArrayElementAtIndex(list.index);

            element.FindPropertyRelative("ID").intValue = audioClipsData.themesCount;
        };
    }

    private void OnGUI()
    {
        Vector2 sizeOfLabel;
        GUIStyle labelStyle = new GUIStyle();
        string playerClipsButtonText = showPlayerClips ? "[^]" : "[v]";
        string enemiesClipsButtonText = showEnemiesClips ? "[^]" : "[v]";
        string enviroClipsButtonText = showEnviroClips ? "[^]" : "[v]";
        string themesButtonText = showThemes ? "[^]" : "[v]";

        if (serializedObject == null)
        {
            Awake();
        }

        if (serializedObject != null)
        {
            EditorGUILayout.BeginVertical();
            scrollPos = EditorGUILayout.BeginScrollView(scrollPos, false, false);
            serializedObject.Update();

            // Player Clips
            EditorGUILayout.BeginHorizontal();
            sizeOfLabel = labelStyle.CalcSize(new GUIContent("Hero clips"));
            EditorGUILayout.LabelField("Hero clips", GUILayout.Width(sizeOfLabel.x + 4));

            if(GUILayout.Button(playerClipsButtonText, GUILayout.Width(40)))
            {
                showPlayerClips = !showPlayerClips;
            }

            EditorGUILayout.EndHorizontal();

            if(showPlayerClips)
            {
                playerClips.DoLayoutList();
            }

            // Enemies Clips
            EditorGUILayout.BeginHorizontal();
            sizeOfLabel = labelStyle.CalcSize(new GUIContent("Enemies clips"));
            EditorGUILayout.LabelField("Enemies clips", GUILayout.Width(sizeOfLabel.x + 4));

            if (GUILayout.Button(enemiesClipsButtonText, GUILayout.Width(40)))
            {
                showEnemiesClips = !showEnemiesClips;
            }

            EditorGUILayout.EndHorizontal();

            if (showEnemiesClips)
            {
                enemiesClips.DoLayoutList();
            }

            // Enviros clips
            EditorGUILayout.BeginHorizontal();
            sizeOfLabel = labelStyle.CalcSize(new GUIContent("Enviros clips"));
            EditorGUILayout.LabelField("Enviros clips", GUILayout.Width(sizeOfLabel.x + 4));

            if (GUILayout.Button(enviroClipsButtonText, GUILayout.Width(40)))
            {
                showEnviroClips = !showEnviroClips;
            }

            EditorGUILayout.EndHorizontal();

            if (showEnviroClips)
            {
                enviroClips.DoLayoutList();
            }

            // Themes
            EditorGUILayout.BeginHorizontal();
            sizeOfLabel = labelStyle.CalcSize(new GUIContent("Themes"));
            EditorGUILayout.LabelField("Themes", GUILayout.Width(sizeOfLabel.x + 4));

            if (GUILayout.Button(themesButtonText, GUILayout.Width(40)))
            {
                showThemes = !showThemes;
            }

            EditorGUILayout.EndHorizontal();

            if (showThemes)
            {
                themes.DoLayoutList();
            }

            // End
            serializedObject.ApplyModifiedProperties();
            EditorGUILayout.EndScrollView();
            EditorGUILayout.EndVertical();
        }
    }

    private void drawHeaderCallback(Rect rect)
    {
        EditorGUI.LabelField(
                new Rect(rect.x, rect.y, rect.width * 1 / 3, EditorGUIUtility.singleLineHeight),
                "Name");
        EditorGUI.LabelField(
            new Rect(rect.x + rect.width * 1 / 3, rect.y, rect.width * 1 / 3, EditorGUIUtility.singleLineHeight),
            "Audio Clip");
        EditorGUI.LabelField(
            new Rect(rect.x + rect.width * 2 / 3, rect.y, rect.width * 1 / 3, EditorGUIUtility.singleLineHeight),
            "Mixer Group");
        EditorGUI.LabelField(
            new Rect(rect.x, rect.y + EditorGUIUtility.singleLineHeight + 2, rect.width / 2, EditorGUIUtility.singleLineHeight),
            "Priority");
        EditorGUI.LabelField(
            new Rect(rect.x + rect.width / 2, rect.y + EditorGUIUtility.singleLineHeight + 2, rect.width / 2, EditorGUIUtility.singleLineHeight),
            "Volume");
    }

    private void drawElementCallback(SerializedProperty element, Rect rect)
    {
        rect.y += 2;
        EditorGUI.PropertyField(
            new Rect(rect.x, rect.y, rect.width * 1 / 3, EditorGUIUtility.singleLineHeight),
            element.FindPropertyRelative("name"), GUIContent.none);
        EditorGUI.PropertyField(
            new Rect(rect.x + rect.width * 1 / 3, rect.y, rect.width * 1 / 3, EditorGUIUtility.singleLineHeight),
            element.FindPropertyRelative("file"), GUIContent.none);
        EditorGUI.PropertyField(
            new Rect(rect.x + rect.width * 2 / 3, rect.y, rect.width * 1 / 3, EditorGUIUtility.singleLineHeight),
            element.FindPropertyRelative("mixerGroup"), GUIContent.none);
        EditorGUI.PropertyField(
            new Rect(rect.x, rect.y + EditorGUIUtility.singleLineHeight + 2, rect.width / 2, EditorGUIUtility.singleLineHeight),
            element.FindPropertyRelative("priority"), GUIContent.none);
        EditorGUI.PropertyField(
            new Rect(rect.x + rect.width / 2, rect.y + EditorGUIUtility.singleLineHeight + 2, rect.width / 2 - 40, EditorGUIUtility.singleLineHeight),
            element.FindPropertyRelative("volume"), GUIContent.none);
        EditorGUI.LabelField(
            new Rect(rect.x + rect.width - 40, rect.y + EditorGUIUtility.singleLineHeight + 2, 40, EditorGUIUtility.singleLineHeight),
            element.FindPropertyRelative("ID").intValue.ToString());
    }

    private void addElementCallback(ReorderableList list)
    {
        int index = list.serializedProperty.arraySize;
        list.serializedProperty.arraySize++;
        list.index = index;
        SerializedProperty element = list.serializedProperty.GetArrayElementAtIndex(index);
        element.FindPropertyRelative("priority").intValue = 128;
        element.FindPropertyRelative("volume").floatValue = 1;
    }
}
