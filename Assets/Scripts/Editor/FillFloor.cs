using UnityEngine;
using UnityEditor;
using System.Collections;

public class FillFloor {

    public enum FillFloorType
    {
        Wood,
        Jungle
    }

	/// <summary>
	/// The size of the small floor.
	/// </summary>
	public static float smallFloorSize = 0.5f;

	/// <summary>
	/// The size of the medium floor.
	/// </summary>
	public static float mediumFloorSize = 1f;

	/// <summary>
	/// The size of the large floor.
	/// </summary>
	public static float largeFloorSize = 2f;

	/// <summary>
	/// The right floor prefab
	/// </summary>
	// public static GameObject rightFloor;

	/// <summary>
	/// The left floor prefab
	/// </summary>
    // public static GameObject leftFloor;

	// Margins dels pilons
	public static float pilonXMargin = 0.3f;
	public static float pilonYMargin = 0.35f;
	public static float pilonZMargin = 0.8f;

	public static float pilonDistance = 5f;

	/// <summary>
	/// Loads the coast props
	/// </summary>
	/// <param name="small">Array of small objects</param>
	/// <param name="medium">Array of medium objects</param>
	/// <param name="large">Array of large objects</param>
    public static void LoadGraphics(EnviroTypeData enviroTypeData, out GameObject[] small, out GameObject[] medium, out GameObject[] large, out GameObject rightFloor, out GameObject leftFloor)
    {
        rightFloor = enviroTypeData.right;

        leftFloor = enviroTypeData.left;

        small = enviroTypeData.small;

        medium = enviroTypeData.medium;

        large = enviroTypeData.large;
	}

    /// <summary>
    /// Agafa el dataHolder del FillFloorDataHolder segons l'index, i revisa que sigui correcte per a treballar-hi
    /// </summary>
    /// <param name="elementIndex">Índex del EnviroTypeData segons el FillFloorDataHolder</param>
    /// <returns>L'objecte EnviroTypeData amb la informació o null si hi ha hagut cap error</returns>
    public static EnviroTypeData getDataHolder(int elementIndex)
    {
        FillFloorDataHolder dataHolder = AssetDatabase.LoadAssetAtPath("Assets/Resources/Data/FillFloorDataHolder.asset", typeof(FillFloorDataHolder)) as FillFloorDataHolder;

        EnviroTypeData enviroTypeData = null;

        if (elementIndex >= dataHolder.enviroTypeFloors.Length)
        {
            EditorUtility.DisplayDialog("Error al preparar els gràfics", "Element index no existeix", "Me cagüen Pere");
            return null;
        }
        else
        {
            enviroTypeData = dataHolder.enviroTypeFloors[elementIndex];

            if (enviroTypeData.small == null || enviroTypeData.medium == null || enviroTypeData.large == null || enviroTypeData.small.Length < 1 || enviroTypeData.medium.Length < 1 || enviroTypeData.large.Length < 1)
            {
                EditorUtility.DisplayDialog("Error al preparar els gràfics", "Algun dels arrays -small, medium o large- no estan assignats i per tant no es pot fer el Fill Floor", "Me cagüen Pere");
                return null;
            }
            else
            {
                for (int i = 0; i < enviroTypeData.small.Length; i++)
                {
                    if (enviroTypeData.small[i] == null)
                    {
                        EditorUtility.DisplayDialog("Error al preparar els gràfics", "Algun dels elements de l'array small no està assignat i per tant no es pot fer el Fill Floor", "Me cagüen Pere");
                        return null;
                    }
                }

                for (int i = 0; i < enviroTypeData.medium.Length; i++)
                {
                    if (enviroTypeData.medium[i] == null)
                    {
                        EditorUtility.DisplayDialog("Error al preparar els gràfics", "Algun dels elements de l'array medium no està assignat i per tant no es pot fer el Fill Floor", "Me cagüen Pere");
                        return null;
                    }
                }

                for (int i = 0; i < enviroTypeData.large.Length; i++)
                {
                    if (enviroTypeData.large[i] == null)
                    {
                        EditorUtility.DisplayDialog("Error al preparar els gràfics", "Algun dels elements de l'array large no està assignat i per tant no es pot fer el Fill Floor", "Me cagüen Pere");
                        return null;
                    }
                }
            }

            if (enviroTypeData.right == null)
            {
                if (enviroTypeData.small != null && enviroTypeData.small.Length > 0)
                {
                    Debug.Log("[FillFloor::Fill] No tenim element right. Fem servir un small");
                    enviroTypeData.right = enviroTypeData.small[0];
                }
                
            }

            if (enviroTypeData.left == null)
            {
                if (enviroTypeData.small != null && enviroTypeData.small.Length > 0)
                {
                    Debug.Log("[FillFloor::Fill] No tenim element right. Fem servir un small");
                    enviroTypeData.left = enviroTypeData.small[0];
                }
            }
        }

        return enviroTypeData;
    }

	/// <summary>
	/// Generate a random value representing small, medium or large but looking if there are free objects of that size
	/// </summary>
	/// <returns>The random value.</returns>
	/// <param name="small">Number of free small objects</param>
	/// <param name="medium">Number of free medium objects</param>
	/// <param name="large">Number of free large objects</param>
    public static int GenerateRandomExistingValue(int small, int medium, int large)
    {
        bool found = false;
        int num = 0;

        while (!found)
        {
            num = Random.Range(0, 3);

            switch (num)
            {
                case 0:
                    found = small > 0;
                    break;
                case 1:
                    found = medium > 0;
                    break;
                case 2:
                    found = large > 0;
                    break;
            }
        }

        return num;
    }

#if UNITY_EDITOR
	/// <summary>
	/// Omple un collider de ground amb taulons de coast
	/// </summary>
	/// <param name="parent">Quin objecte volem omplir (per a agafar mida, alçada, etc.)</param>
	/// <param name="name">Quin nom tindra el GameObject que contindra els prefabs instanciats</param>
	/// <param name="addPilons">If set to <c>true</c> add pilons.</param>
    public static void Fill(GameObject parent, string name, bool addPilons, int elementIndex)
    {
		// Debug.Log ( Selection.activeGameObject );
		GameObject floor = Selection.activeGameObject;
		if( floor.GetComponent<Collider2D>() == null ) {
			Debug.LogWarning( "[FillFloor::Fill] No te collider 2D" );
			return;
		}

		float xPosition = floor.GetComponent<Renderer>().bounds.min.x;
		float endPosition = floor.GetComponent<Renderer>().bounds.max.x;
		float size = endPosition - xPosition;

		size = size - smallFloorSize * 2; // Deixem lloc per a la fusta que obra. La que tanca anira al final i servira per no haver d'afegir fustes

		GameObject[] small, medium, large;
        GameObject rightFloor, leftFloor;

        EnviroTypeData enviroTypeData = FillFloor.getDataHolder(elementIndex);

        if (enviroTypeData == null)
        {
            Debug.LogWarning("[FillFloor::Fill] No s'ha trobat informació gràfica de l'index");
            return;
        }

        LoadGraphics(enviroTypeData, out small, out medium, out large, out rightFloor, out leftFloor);

		// Numero de terres de cada tipus
		int nSmall, nMedium, nLarge;
		nLarge = Mathf.FloorToInt(size / largeFloorSize);
		size = size % largeFloorSize;

		// De l'espai que quedi posem tots els mitjans possibles
		nMedium = Mathf.FloorToInt(size / mediumFloorSize);
		size = size % mediumFloorSize;

		// De l'espai que quedi posem tots els petits possibles
		nSmall = Mathf.FloorToInt (size / smallFloorSize);

		// Debug.Log ( "size: " + ( endPosition - xPosition ) + " - large: " + nLarge + ", medium: " + nMedium + ", small: " + nSmall );
        GameObject insideParent;
        if (string.IsNullOrEmpty(name))
        {
            insideParent = new GameObject("fill" + floor.name + "At" + xPosition);
        }
        else
        {
            insideParent = new GameObject(name);
        }

		GameObject floorInstance;
        floorInstance = Instantiate(leftFloor, new Vector3(xPosition + smallFloorSize / 2, floor.transform.position.y + floor.transform.localScale.y / 2, floor.transform.position.z), insideParent.transform, Vector3.zero);
		xPosition = xPosition + smallFloorSize;

        int total = nSmall + nMedium + nLarge;
        for (int i = 0; i < total; i++)
        {
            int num = GenerateRandomExistingValue(nSmall, nMedium, nLarge);
            // Debug.Log ( i + " - " + num + " - " + ( nSmall + nMedium + nLarge ));
            switch (num)
            {
                case 0:
                    nSmall--;
                    floorInstance = Instantiate(small[Random.Range(0, small.Length)], new Vector3(xPosition + smallFloorSize / 2, floor.transform.position.y + floor.transform.localScale.y / 2, floor.transform.position.z),
                                                insideParent.transform, Vector3.zero);
                    xPosition = xPosition + smallFloorSize;
                    break;
                case 1:
                    nMedium--;
                    floorInstance = Instantiate(medium[Random.Range(0, medium.Length)], new Vector3(xPosition + mediumFloorSize / 2, floor.transform.position.y + floor.transform.localScale.y / 2, floor.transform.position.z),
                                                insideParent.transform, Vector3.zero);
                    xPosition = xPosition + mediumFloorSize;
                    break;
                case 2:
                    nLarge--;
                    floorInstance = Instantiate(large[Random.Range(0, large.Length)], new Vector3(xPosition + largeFloorSize / 2, floor.transform.position.y + floor.transform.localScale.y / 2, floor.transform.position.z),
                                                insideParent.transform, Vector3.zero);
                    xPosition = xPosition + largeFloorSize;
                    break;
            }
        }

        floorInstance = Instantiate(rightFloor, new Vector3(xPosition + smallFloorSize / 2, floor.transform.position.y + floor.transform.localScale.y / 2, floor.transform.position.z), 
		                            insideParent.transform, Vector3.zero );

        if (addPilons)
        {
            AddPilons(floor, insideParent);
        }

		insideParent.transform.parent = parent.transform;
	}

	/// <summary>
	/// Adds the pilons.
	/// </summary>
	/// <param name="floor">A quin terra hi volem afegir els pilons</param>
	/// <param name="parent">A quin parent afegirem els prefabs</param>
	public static void AddPilons( GameObject floor, GameObject parent ) {
		// Start and size del floor al que li posem els pilons
		float start = floor.GetComponent<Renderer>().bounds.min.x;
		float size = floor.GetComponent<Renderer>().bounds.size.x;

		// GameObject del pilon, per a instanciar-lo
		GameObject pilon = AssetDatabase.LoadAssetAtPath( "Assets/Prefabs/Props/WoodObjects/WoodPilons/BeachWoodPilon.prefab", typeof(GameObject) ) as GameObject;

		// El fem servir per a calcular els bounds.
		GameObject pilonMasterInstance = PrefabUtility.InstantiatePrefab( pilon ) as GameObject;

		// Calculem quans pilons posarem
		bool stopDivide = false;
		int numberOfPilons = 0;
		while ( !stopDivide ) {
			numberOfPilons++;
			stopDivide = ( ( size - pilonXMargin * 2 ) / numberOfPilons ) < pilonDistance;
		}

		float distanceBetweenPilons = ( size - pilonXMargin - 0.1f ) / numberOfPilons; // restem tambe la meitat de la mida del pilon final

		// Afegim els pilons que tanquen l'estructura.
		numberOfPilons++; 

		// Preparem la mascara i el hit per a les col·lisions
		int oldLayer = floor.layer;
		floor.layer = Layers.NoCollision;
		LayerMask mask = 1 << Layers.Ground;
		RaycastHit2D hit;

		// Creem un GameObject que ens servira per a fer les instancies
		GameObject pilonInstance;

		// un multiplicador que fem servir per a posar davant i darrera amb un "for" senzill
		int multiplier = 1;

		// Per a tot el numero de pilons
		for( int i = 0; i < numberOfPilons; i++) {

			// Dues vegades, una davant i una darrera
			for ( int k = 0; k < 2; k++ ) {
				// Davant
				Vector3 position = new Vector3( start + distanceBetweenPilons * i + pilonXMargin, floor.transform.position.y + pilonYMargin, floor.transform.position.z - pilonZMargin * multiplier );

				// Busquem fins a on posem el pilon. Si no diu res, el posem fins a -3
				float bottom = -3;
				if( hit = Physics2D.Raycast( position, -Vector2.up, 50f, mask ) ) {
					bottom = hit.point.y;
				}

				// Posem pilons segons la distancia fins al terra. Com a molt els escalem al doble
				float large = (position.y - bottom);
				if ( large > pilonMasterInstance.GetComponent<Renderer>().bounds.size.y * 2 ) {
					float pilonSize;
					float pilonNum;
					DivideLarge( large, pilonMasterInstance.GetComponent<Renderer>().bounds.size.y * 2 , out pilonSize, out pilonNum );
					for( int j = 0; j < pilonNum; j++ ) {
						pilonInstance = Instantiate( pilon, position + new Vector3( 0, -pilonSize * j, 0 ), parent.transform, new Vector3( 180, 0 ) );
						pilonInstance.transform.localScale = new Vector3( 1, pilonSize / pilonMasterInstance.GetComponent<Renderer>().bounds.size.y, 1 );
					}
				} else {
					pilonInstance = Instantiate( pilon, position, parent.transform, new Vector3( 180, 0 ) );
					pilonInstance.transform.localScale = new Vector3( 1, large / pilonMasterInstance.GetComponent<Renderer>().bounds.size.y, 1 );
				}

				multiplier = multiplier * -1;
			}
		}

		// Esborrem el pilon master que hem fet servir per a calcular mides
		GameObject.DestroyImmediate( pilonMasterInstance );

		// Tornem el terra a la seva mida original
		floor.layer = oldLayer;
	}

	/// <summary>
	/// Divideix la llargada de la platafroma segons el maxim que poden fer els pilons i retorna quina mida tindran i quants en farem servir
	/// </summary>
	/// <param name="large">Llargada de la plataforma</param>
	/// <param name="maxSize">Mida maxima dels pilons</param>
	/// <param name="size">(out) mida que tindran els pilons</param>
	/// <param name="num">(out) numero de pilons que tindrem</param>
	public static void DivideLarge( float large, float maxSize, out float size, out float num ) {
		num = 1;
		size = large;
		while( large > maxSize ) {
			large = large / 2;
			num = num * 2;
			size = large;
		}
	}

	/// <summary>
	/// Instantiate the specified obj, at position with parent and euler angles of rotation.
	/// </summary>
	/// <param name="obj">The object to instantiate</param>
	/// <param name="position">Position</param>
	/// <param name="parent">Parent to be attached</param>
	/// <param name="euler">Rotation</param>
	public static GameObject Instantiate( GameObject obj, Vector3 position, Transform parent, Vector3 euler ) {
		GameObject pilonInstance = PrefabUtility.InstantiatePrefab( obj ) as GameObject;
		pilonInstance.transform.position = position;
		pilonInstance.transform.parent = parent.transform;
		pilonInstance.transform.eulerAngles = euler;

		return pilonInstance;
	}

#endif
}
