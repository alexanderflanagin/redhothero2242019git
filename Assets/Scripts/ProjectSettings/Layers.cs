using UnityEngine;
using System.Collections;

public class Layers {

	public static int DebugLayer = 8;

	public static int Player = 9;

	public static int NoReflNoRefr = 10;

	public static int Enemy = 11;

	public static int Ground = 12;

	public static int NoCollision = 13;

    /// <summary>
    /// Factories dels enemics que tenen part solides
    /// </summary>
	public static int EnemyFactory = 14;

	public static int Smoke = 15;

	public static int WaterOwn = 16;

	public static int Background = 17;

    public static int IngameGUI = 18;

	public static int BouncingObject = 19;

    /// <summary>
    /// Layer dels punts d'energia que carreguen
    /// </summary>
	public static int RedHotDot = 20;
	
	public static int OneWayPlatform = 22;

    /// <summary>
    /// Layer dels portals de Red Hot Energy
    /// </summary>
    public static int RedHotDensity = 23;

    /// <summary>
    /// Layer per a les coses de primer línia que reben iluminació especial
    /// </summary>
    public static int FirstLineIlumination = 24;

    /// <summary>
    /// Layer per als projectils i les seves col·lisions
    /// </summary>
    public static int Projectiles = 25;

    /// <summary>
    /// Layer per als elements de BlueSubstance
    /// </summary>
    public static int BlueSubstance = 26;

    /// <summary>
    /// Layer per als enemies que ha d'ignorar el player (perque l'ha tocat)
    /// </summary>
    public static int IgnoredEnemy = 27;

    /// <summary>
    /// Layer per al final boss
    /// </summary>
    public static int FinalBoss = 28;

    public static int Player0 = 28;

    public static int Player1 = 28;

    public static int Player2 = 28;

    public static int Player3 = 28;

    public static LayerMask Enemies = 1 << Enemy | 1 << IgnoredEnemy;

    public static LayerMask Players = 1 << Player;

    /// <summary>
    /// Capes amb les que han de col·lisionar per defecte els enemics.
    /// </summary>
    public static LayerMask EnemiesCollideWith = 1 << Ground | 1 << BouncingObject | 1 << OneWayPlatform | 1 << Enemy | 1 << IgnoredEnemy | 1 << EnemyFactory | 1 << FinalBoss;

    /// <summary>
    /// Màscara de càpes dels Red Hot Dots
    /// </summary>
    public static LayerMask RedHotDots = 1 << RedHotDot;

    /// <summary>
    /// Màscara de càpes de RedHot Portals (creada així per si després ha de tenir alguna capa més).
    /// </summary>
    public static LayerMask RedHotDensities = 1 << RedHotDensity;

    /// <summary>
    /// Mascara de capes que farem servir per saber amb que han de col.lisionar els PowerGloved
    /// </summary>
    public static LayerMask PowerGlovedElements = 1 << Ground | 1 << Enemy | 1 << IgnoredEnemy | 1 << EnemyFactory | 1 << FinalBoss;

    /// <summary>
    /// Mascara amb tots els elements que si son mobils es transformen en MovingPlatforms
    /// </summary>
    public static LayerMask MovingPlatforms = 1 << Ground | 1 << OneWayPlatform | 1 << BouncingObject;

    /// <summary>
    /// Mascara per els elements que ha de moure per defecte una moving platform
    /// </summary>
    public static LayerMask MovingPlatformPassengers = 1 << Player | 1 << Enemy | 1 << IgnoredEnemy;

    /// <summary>
    /// Mira si una capa está dins d'una màscara
    /// </summary>
    /// <param name="mask">La màscara que es vol testejar</param>
    /// <param name="layer">La capa que mirem si és dins</param>
    /// <returns>true si hi és, false altrament</returns>
    public static bool inMask( LayerMask mask, int layer )
    {
        return (mask.value & 1 << layer) != 0;
    }

    /// <summary>
    /// Elimina una capa d'una màscara
    /// </summary>
    /// <param name="mask">La màscara d'on es vol eliminar la capa</param>
    /// <param name="layer">La capa a eliminar</param>
    /// <returns>Retorna la màscara amb la capa eliminada</returns>
    public static LayerMask removeLayer(LayerMask mask, int layer)
    {
        LayerMask invertedOriginal = ~mask;
		return ~(invertedOriginal | (1 << layer));
    }

    /// <summary>
    /// Afegeix una capa a una màscara
    /// </summary>
    /// <param name="mask">La màscara on volem afegir la capa</param>
    /// <param name="layer">La capa a afegir</param>
    /// <returns>La màscara amb la capa afegida</returns>
    public static LayerMask addLayer(LayerMask mask, int layer)
    {
        return mask | (1 << layer);
    }
}
