using UnityEngine;
using System.Collections;

public class Tags : MonoBehaviour {

	public static string Player = "Player";

	public static string LavaWall = "LavaWall";

	public static string FreezeTheWave = "FreezeTheWave";

	public static string CameraTarget = "CameraTarget";

	public static string FinishLine = "FinishLine";

	public static string Trigger = "Trigger";

	public static string Kill = "Kill";

	public static string Whatever = "Whatever";

    public static string Pickeable = "Pickeable";

	public static string BeachFloor = "BeachFloor";

	public static string SecureTriggerTop = "SecureTriggerTop";

    public static string Handler = "Handler";

	public static string RockEnemy = "RockEnemy";

    public static string EditorColliders = "EditorColliders";

    public static string KamikazansWater = "KamikazansWater";

    public static string KamikazansSand = "KamikazansSand";

    public static string KKRobot = "KKRobot";

    /// <summary>
    /// Objectes de RedHotEnergy als que et pots teletransportar
    /// </summary>
    public static string RedHotEnergy = "RedHotEnergy";

    /// <summary>
    /// Punts imantants cap als que apuntarà el player aim (igual per al dispar que per al puny)
    /// </summary>
    public static string MagnetPoint = "MagnetPoint";

	public static string TravellingHero = "TravellingHero";

    /// <summary>
    /// Canvas que mostra els elements de la GUI
    /// </summary>
    public static string GUIHelperCanvas = "GUIHelperCanvas";

    public static string StageManager = "StageManager";
}
