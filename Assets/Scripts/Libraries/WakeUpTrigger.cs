#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using System;
using System.Collections;
using System.Reflection;

[ExecuteInEditMode]
public class WakeUpTrigger : MonoBehaviour
{
    public LayerMask shouldWakeUp = Layers.Players;

    /// <summary>
    /// Objecte que depen d'aquest trigger
    /// </summary>
    private IWakeUpable _dependant;

    /// <summary>
    /// El collider del trigger
    /// </summary>
    private Collider2D _trigger;

    private IEnumerator insideCheck;

    private bool playerIn = false;

#if UNITY_EDITOR

    /// <summary>
    /// Per saber si hem mogut el trigger en les X
    /// </summary>
    private float _lastX = 0;

    /// <summary>
    /// Color del gizmo
    /// </summary>
    public Color color = Color.white;

    /// <summary>
    /// Al start adjudicarem el color d'aquest gizmo (aleatori o per valor del seu pare)
    /// </summary>
    void Start()
    {
        // Mirem si el pare te color, i sino ho fem blanc
        try
        {
            Type dependant = this._dependant.GetType();
            FieldInfo fi = dependant.GetField("gizmosColor");
            Color gizmosColor = (Color)fi.GetValue(this._dependant);
            color = new Color(gizmosColor.r, gizmosColor.g, gizmosColor.b);
        }
        catch (Exception)
        {
            
        }
    }

    /// <summary>
    /// Al fer enable del Game Object afegit l'update al del editor.
    /// </summary>
    void OnEnable()
    {
        EditorApplication.update += EditorUpdate;
        _lastX = transform.position.x;

        _trigger = GetComponent<Collider2D>();

        foreach (var mb in GetComponentsInParent<MonoBehaviour>())
        {
            if (mb is IWakeUpable)
            {
                _dependant = mb as IWakeUpable;
                break;
            }
        }
    }

    /// <summary>
    /// Al fer disable, treiem l'update del editor
    /// </summary>
    void OnDisable()
    {
        EditorApplication.update -= EditorUpdate;
    }

    /// <summary>
    /// Updater durant l'editor. Si movem el trigger, actualitza la distancia.
    /// </summary>
    public void EditorUpdate()
    {
        if (Selection.activeGameObject == this.gameObject)
        {
            if (_dependant.GetTriggerOffset() != _trigger.offset)
            {
                _dependant.SetTriggerOffset(_trigger.offset);
                EditorUtility.SetDirty(((MonoBehaviour)_dependant));
            }

            if (_dependant.GetTriggerSize() != (Vector2)_trigger.bounds.size)
            {
                _dependant.SetTriggerSize((Vector2)_trigger.bounds.size);
                EditorUtility.SetDirty(((MonoBehaviour)_dependant));
            }

            if (_lastX != transform.position.x)
            {
                _dependant.SetTriggerDistance(transform.parent.position.x - transform.position.x);
                EditorUtility.SetDirty(((MonoBehaviour)_dependant));
                _lastX = transform.position.x;
            }
        }
        else if (_dependant != null && Selection.activeObject == ((MonoBehaviour)_dependant).gameObject)
        {
            transform.position = new Vector3(transform.parent.position.x - _dependant.GetTriggerDistance(), transform.position.y);
        }
    }
#endif

#if (!UNITY_EDITOR)
	void Start() {
	    _trigger = GetComponent<Collider2D>();	
    
        foreach( var mb in GetComponentsInParent<MonoBehaviour>() ) {
			if( mb is IWakeUpable ) {
				_dependant = mb as IWakeUpable;
				break;
			}
		}
	}
#endif

    void OnTriggerEnter2D(Collider2D other)
    {
        
        if (!this.playerIn && Layers.inMask(shouldWakeUp, other.gameObject.layer) && !other.transform.IsChildOf(transform.parent))
        {
            _dependant.WakeUp();

            if (this.insideCheck != null)
            {
                StopCoroutine(this.insideCheck);
            }

            this.insideCheck = InsideCheck(other);

            StartCoroutine(this.insideCheck);
        }
    }

    private IEnumerator InsideCheck(Collider2D other)
    {
        this.playerIn = true;

        do
        {
            //Debug.Log(transform.position);
            yield return null;
        } while (other.bounds.max.x > this._trigger.bounds.min.x && other.bounds.min.x < this._trigger.bounds.max.x && other.bounds.max.y > this._trigger.bounds.min.y && other.bounds.min.y < this._trigger.bounds.max.y);

        this.playerIn = false;

        this._dependant.Sleep();
    }

    void OnDrawGizmos()
    {
		#if UNITY_EDITOR        
		Gizmos.color = color;
#endif
        Vector3 topLeft = new Vector3(_trigger.bounds.min.x, _trigger.bounds.max.y);
        Vector3 bottomRight = new Vector3(_trigger.bounds.max.x, _trigger.bounds.min.y);

        Gizmos.DrawLine(_trigger.bounds.min, topLeft);
        Gizmos.DrawLine(topLeft, _trigger.bounds.max);
        Gizmos.DrawLine(_trigger.bounds.max, bottomRight);
        Gizmos.DrawLine(bottomRight, _trigger.bounds.min);

        Gizmos.DrawLine(_trigger.bounds.max, transform.parent.position);
        Gizmos.DrawLine(_trigger.bounds.min, transform.parent.position);
        Gizmos.DrawLine(topLeft, transform.parent.position);
        Gizmos.DrawLine(bottomRight, transform.parent.position);

        Gizmos.DrawLine(_trigger.bounds.max, transform.parent.position);
        Gizmos.DrawLine(_trigger.bounds.min, transform.parent.position);
        Gizmos.DrawLine(bottomRight, transform.parent.position);
        Gizmos.DrawLine(topLeft, transform.parent.position);

        Gizmos.DrawSphere(_trigger.bounds.max, 0.4f);
        Gizmos.DrawSphere(_trigger.bounds.min, 0.4f);
        Gizmos.DrawSphere(bottomRight, 0.4f);
        Gizmos.DrawSphere(topLeft, 0.4f);
    }
}
