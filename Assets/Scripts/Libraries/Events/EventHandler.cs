﻿using UnityEngine;
using System.Collections;

public class EventHandler {

	public delegate void EventAction();
	public event EventAction OnEvent;

	public void Launch() {
		if( OnEvent != null )
			OnEvent();
	}
}
