using UnityEngine;
using System;
using System.Collections;

public class Physics2DController : MonoBehaviour
{
    /// <summary>
    /// Si pot pujar/baixar pendents
    /// </summary>
    public bool canUseSlopes = false;

    /// <summary>
    /// Angle que pot pujar/baixar en pendent
    /// </summary>
    public float maxSlopeAngle = 80;

    /// <summary>
    /// LayerMask dels objectes amb els que col.lisionem
    /// </summary>
    public LayerMask collisionMask;

    /// <summary>
    /// Informacio de col.lisions que te
    /// </summary>
    public CollisionInfo collisions;

    /// <summary>
    /// Input del player. El fem servir per a "baixar" plataformes atravesables
    /// </summary>
    [HideInInspector]
    public Vector2 playerInput;

    /// <summary>
    /// Informacio des d'on crearem els rajos respecte al boxCollider
    /// </summary>
    [HideInInspector]
    public BoxColliderRayInfo rayInfo;

    /// <summary>
    /// Acces directe per a veure si esta bloquejat tenint en compte la direccio que porta
    /// </summary>
    public bool blockedFront
    {
        get
        {
            return (collisions.faceDir == 1 && collisions.right) || (collisions.faceDir == -1 && collisions.left);
        }
    }

    /// <summary>
    /// Event que es llen?a quan es bloqueja per dalt
    /// </summary>
    public event Action BlockedAboveEnter;

    /// <summary>
    /// Event que es llen?a quan toca el terra
    /// </summary>
    public event Action BlockedBelowEnter;

    /// <summary>
    /// Event que es llen?a quan es bloqueja per davant
    /// </summary>
    public event Action BlockedFrontEnter;

    /// <summary>
    /// Es llenca quan una plataforma mobil l'aixafa de forma horitzontal
    /// </summary>
    public event Action HorizontallyCrushed;

    /// <summary>
    /// Es llenca quan una plataforma mobil l'aixafa de forma vertical
    /// </summary>
    public event Action VerticallyCrushed;

    /// <summary>
    /// Hi guardarem tota la informacio dels hits, per a no haver de crear una variable cada frame
    /// </summary>
    private RaycastHit2D[] hits;

    public void Awake()
    {
        if (rayInfo == null)
        {
            rayInfo = new BoxColliderRayInfo();
        }

        hits = new RaycastHit2D[1];
    }

    /// <summary>
    /// Al OnEnable reinicialitzem la info dels rajos (per als enemics que canvien de posicio)
    /// </summary>
    public void OnEnable()
    {
        collisions.faceDir = 1;

        rayInfo.Initialize(this.GetComponent<BoxCollider2D>());
    }

    /// <summary>
    /// Move en el qual l'objecte es mou per ell mateix i no te input/no importa l'input
    /// </summary>
    /// <param name="moveAmount">Velocitats que portem</param>
    public void Move(Vector2 moveAmount)
    {
        Move(moveAmount, Vector2.zero, false, false, Vector2.zero);
    }

    /// <summary>
    /// Move en el qual l'objecte es mou per ell mateix i volem tenir en compte l'input
    /// </summary>
    /// <param name="moveAmount">Velocitats que portem</param>
    /// <param name="input">Entrada de l'usuari</param>
    public void Move(Vector2 moveAmount, Vector2 input)
    {
        Move(moveAmount, input, false, false, Vector2.zero);
    }

    /// <summary>
    /// Move en el qual ens mou un element extern, i ens pot empenyer, arrossegar, etc.
    /// </summary>
    /// <param name="moveAmount">Velocitat que volem moure</param>
    /// <param name="standingOnPlatform">Si estem sobre la plataforma</param>
    /// <param name="movedFromOutside">Si ens estan movent des de fora</param>
    /// <param name="outsideMovement">Quantitat de moviment que rebem des de fora</param>
    public void Move(Vector2 moveAmount, bool standingOnPlatform, bool movedFromOutside, Vector2 outsideMovement)
    {
        Move(moveAmount, Vector2.zero, movedFromOutside, standingOnPlatform, outsideMovement);
    }

    public void Move(Vector2 moveAmount, Vector2 input, bool movedFromOutside, bool standingOnPlatform, Vector2 outsideMovement)
    {
        rayInfo.UpdateRaycastOrigins();

        collisions.Reset();
        collisions.moveAmountOld = moveAmount;
        playerInput = input;

        if (canUseSlopes && moveAmount.y < 0)
        {
            DescendSlope(ref moveAmount);
        }

        if (moveAmount.x != 0)
        {
            collisions.faceDir = (int)Mathf.Sign(moveAmount.x);
        }

        HorizontalCollisions(ref moveAmount);
        if (moveAmount.y != 0)
        {
            VerticalCollisions(ref moveAmount);
        }

        collisions.moveAmountFinal = moveAmount;
        transform.Translate(moveAmount);

        if (standingOnPlatform)
        {
            collisions.below = true;
        }

        if(movedFromOutside)
        {
            if (collisions.right && outsideMovement.x > 0)
            {
                // Ens empenyen cap a la dreta. Mirem si estem bloquejats per l'esquerra i si hem hagut de "disminuir" el moviment que ens intenten aplicar
                //Debug.Log("Crushed Horizontally from left");
                CrushedHorizontally(1, outsideMovement.x);
            }

            if (collisions.left && outsideMovement.x < 0)
            {
                // Ens empenyen cap a l'esquerra. Mirem si estem bloquejats per la dreta i si hem hagut de "disminuir" el moviment que ens intenten aplicar
                //Debug.Log("Crushed Horizontally from right");
                CrushedHorizontally(-1, -outsideMovement.x);
            }

            if (collisions.above && outsideMovement.y > 0)
            {
                // Ens empenyen cap amunt. Mirem si estem bloquejats per sota
                //Debug.Log("Crushed vertically from bottom");
                CrushedVertically(1, outsideMovement.y);
            }

            if (!standingOnPlatform && collisions.below && outsideMovement.y < 0)
            {
                // Ens estan empenyent cap avall però ja estem tocant el terra
                //Debug.Log("Crushed vertically from top");
                CrushedVertically(-1, -outsideMovement.y);
            }
        }

        //Debug.Log(string.Format("Left: {0} - Right: {1} - Above: {2} - Below: {3}", collisions.left, collisions.right, collisions.above, collisions.below));
    }

    void CrushedHorizontally(int directionX, float moveAmountX)
    {
        directionX = -directionX;

        // El raig es tant llarg com la part horizontall del collider sense skin
        float rayLength = rayInfo.raycastOrigins.bottomRight.x - rayInfo.raycastOrigins.bottomLeft.x + moveAmountX + BoxColliderRayInfo.skinWidth;

        for (int i = 0; i < rayInfo.horizontalRayCount; i++)
        {
            Vector2 rayOrigin = (directionX == -1) ? rayInfo.raycastOrigins.bottomRight : rayInfo.raycastOrigins.bottomLeft;
            rayOrigin += Vector2.up * (rayInfo.horizontalRaySpacing * i);

            Debug.DrawRay(rayOrigin, Vector2.right * directionX * rayLength, Color.green);
            Debug.DrawRay(rayOrigin, (Vector2.up + Vector2.left) * .5f, Color.green);

            if (Physics2D.RaycastNonAlloc(rayOrigin, Vector2.right * directionX, hits, rayLength, collisionMask) > 0)
            {
                // Mirem si la layer d'allo que hem tocat es OneWay. Si es així la ignorem
                if (hits[0].transform.gameObject.layer == Layers.OneWayPlatform)
                {
                    continue;
                }

                if(HorizontallyCrushed != null)
                {
                    HorizontallyCrushed();
                }

                return;
            }
        }
    }

    void CrushedVertically(int directionY, float moveAmountY)
    {
        directionY = -directionY;
        
        // El raig es tant llarg com la part vertical del collider sense skin
        float rayLength = rayInfo.raycastOrigins.topRight.y - rayInfo.raycastOrigins.bottomRight.y + moveAmountY + BoxColliderRayInfo.skinWidth;

        for (int i = 0; i < rayInfo.verticalRayCount; i++)
        {
            Vector2 rayOrigin = (directionY == -1) ? rayInfo.raycastOrigins.topLeft : rayInfo.raycastOrigins.bottomLeft;
            rayOrigin += Vector2.right * (rayInfo.verticalRaySpacing * i);

            Debug.DrawRay(rayOrigin, Vector2.up * directionY * rayLength, Color.green);
            Debug.DrawRay(rayOrigin, (Vector2.up + Vector2.left) * .5f, Color.green);

            if (Physics2D.RaycastNonAlloc(rayOrigin, Vector2.up * directionY, hits, rayLength, collisionMask) > 0)
            {
                // Mirem si la layer d'allo que hem tocat es OneWay
                if (hits[0].transform.gameObject.layer == Layers.OneWayPlatform)
                {
                    if (directionY == 1 || hits[0].distance == 0)
                    {
                        continue;
                    }
                }

                if (VerticallyCrushed != null)
                {
                    VerticallyCrushed();
                }

                return;
            }
        }
    }

    void HorizontalCollisions(ref Vector2 moveAmount)
    {
        float directionX = collisions.faceDir;
        float rayLength = Mathf.Abs(moveAmount.x) + BoxColliderRayInfo.skinWidth;

        if (Mathf.Abs(moveAmount.x) < BoxColliderRayInfo.skinWidth)
        {
            rayLength = 2 * BoxColliderRayInfo.skinWidth;
        }

        for (int i = 0; i < rayInfo.horizontalRayCount; i++)
        {
            Vector2 rayOrigin = (directionX == -1) ? rayInfo.raycastOrigins.bottomLeft : rayInfo.raycastOrigins.bottomRight;
            rayOrigin += Vector2.up * (rayInfo.horizontalRaySpacing * i);
            //RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.right * directionX, rayLength, collisionMask);

            Debug.DrawRay(rayOrigin, Vector2.right * directionX, Color.red);

            if (Physics2D.RaycastNonAlloc(rayOrigin, Vector2.right * directionX, hits, rayLength, collisionMask) > 0)
            {
                // Mirem si la layer d'allo que hem tocat es OneWay. Si es així la ignorem
                if (hits[0].transform.gameObject.layer == Layers.OneWayPlatform)
                {
                    continue;
                }

                if (hits[0].distance == 0)
                {
                    continue;
                }

                float slopeAngle = Vector2.Angle(hits[0].normal, Vector2.up);

                if (i == 0 && slopeAngle <= maxSlopeAngle)
                {
                    if (collisions.descendingSlope)
                    {
                        collisions.descendingSlope = false;
                        moveAmount = collisions.moveAmountOld;
                    }
                    float distanceToSlopeStart = 0;
                    if (slopeAngle != collisions.slopeAngleOld)
                    {
                        distanceToSlopeStart = hits[0].distance - BoxColliderRayInfo.skinWidth;
                        moveAmount.x -= distanceToSlopeStart * directionX;
                    }
                    ClimbSlope(ref moveAmount, slopeAngle, hits[0].normal);
                    moveAmount.x += distanceToSlopeStart * directionX;
                }

                if (!collisions.climbingSlope || slopeAngle > maxSlopeAngle)
                {
                    moveAmount.x = (hits[0].distance - BoxColliderRayInfo.skinWidth) * directionX;
                    rayLength = hits[0].distance;

                    if (collisions.climbingSlope)
                    {
                        moveAmount.y = Mathf.Tan(collisions.slopeAngle * Mathf.Deg2Rad) * Mathf.Abs(moveAmount.x);
                    }

                    collisions.left = directionX == -1;
                    collisions.right = directionX == 1;

                    // NEW: Guarda l'objecte amb el que col.lisiona
                    if(collisions.left)
                    {
                        collisions.leftObject = hits[0].transform.gameObject;
                    }

                    if (collisions.right)
                    {
                        collisions.rightObject = hits[0].transform.gameObject;
                    }
                }
            }
        }

        // Ho treiem del for per a que no s'executi varies vegades
        if (collisions.left && !collisions.leftOld && BlockedFrontEnter != null)
        {
            BlockedFrontEnter();
        }

        if (collisions.right && !collisions.rightOld && BlockedFrontEnter != null)
        {
            BlockedFrontEnter();
        }
    }

    void VerticalCollisions(ref Vector2 moveAmount)
    {
        float directionY = Mathf.Sign(moveAmount.y);
        float rayLength = Mathf.Abs(moveAmount.y) + BoxColliderRayInfo.skinWidth;

        for (int i = 0; i < rayInfo.verticalRayCount; i++)
        {

            Vector2 rayOrigin = (directionY == -1) ? rayInfo.raycastOrigins.bottomLeft : rayInfo.raycastOrigins.topLeft;
            rayOrigin += Vector2.right * (rayInfo.verticalRaySpacing * i + moveAmount.x);
            // RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.up * directionY, rayLength, collisionMask);

            Debug.DrawRay(rayOrigin, Vector2.up * directionY, Color.red);

            if (Physics2D.RaycastNonAlloc(rayOrigin, Vector2.up * directionY, hits, rayLength, collisionMask) > 0)
            {
                // Mirem si la layer d'allo que hem tocat es OneWay i la deshabilitem
                if (hits[0].transform.gameObject.layer == Layers.OneWayPlatform)
                {
                    if (directionY == 1 || hits[0].distance == 0)
                    {
                        continue;
                    }
                    if (collisions.fallingThroughPlatform)
                    {
                        continue;
                    }
                    if (playerInput.y == -1)
                    {
                        collisions.fallingThroughPlatform = true;
                        Invoke("ResetFallingThroughPlatform", .5f);
                        continue;
                    }
                }

                if(directionY == 1 && Layers.inMask(Layers.Enemies, gameObject.layer) && Layers.inMask(Layers.Enemies, hits[0].transform.gameObject.layer))
                {
                    // Enemic sobre enemic. No ens bloqueja, el pugem
                    Physics2DController otherController = hits[0].transform.gameObject.GetComponent<Physics2DController>();
                    
                    if (!GetComponent<PowerGloved>() && !otherController.GetComponent<PowerGloved>())
                    {
                        // Si un dels dos esta powergloved si que han de xocar
                        otherController.Move(moveAmount, true, true, moveAmount);
                        continue;
                    }
                }

                moveAmount.y = (hits[0].distance - BoxColliderRayInfo.skinWidth) * directionY;
                rayLength = hits[0].distance;

                if (collisions.climbingSlope)
                {
                    moveAmount.x = moveAmount.y / Mathf.Tan(collisions.slopeAngle * Mathf.Deg2Rad) * Mathf.Sign(moveAmount.x);
                }

                collisions.below = directionY == -1;
                collisions.above = directionY == 1;

                // NEW: Guarda l'objecte amb el que col.lisiona
                if (collisions.below)
                {
                    collisions.belowObject = hits[0].transform.gameObject;
                }

                if (collisions.above)
                {
                    collisions.aboveObject = hits[0].transform.gameObject;
                }
            }
        }

        if (collisions.climbingSlope)
        {
            float directionX = Mathf.Sign(moveAmount.x);
            rayLength = Mathf.Abs(moveAmount.x) + BoxColliderRayInfo.skinWidth;
            Vector2 rayOrigin = ((directionX == -1) ? rayInfo.raycastOrigins.bottomLeft : rayInfo.raycastOrigins.bottomRight) + Vector2.up * moveAmount.y;
            //RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.right * directionX, rayLength, collisionMask);

            if (Physics2D.RaycastNonAlloc(rayOrigin, Vector2.right * directionX, hits, rayLength, collisionMask) > 0)
            {
                float slopeAngle = Vector2.Angle(hits[0].normal, Vector2.up);
                if (slopeAngle != collisions.slopeAngle)
                {
                    moveAmount.x = (hits[0].distance - BoxColliderRayInfo.skinWidth) * directionX;
                    collisions.slopeAngle = slopeAngle;
                    collisions.slopeNormal = hits[0].normal;
                }
            }
        }

        // Ho treiem del for per a que no s'executi varies vegades
        if (collisions.below && !collisions.belowOld && BlockedBelowEnter != null)
        {
            BlockedBelowEnter();
        }

        if (collisions.above && !collisions.aboveOld && BlockedAboveEnter != null)
        {
            BlockedAboveEnter();
        }
    }

    void ClimbSlope(ref Vector2 moveAmount, float slopeAngle, Vector2 slopeNormal)
    {
        float moveDistance = Mathf.Abs(moveAmount.x);
        float climbmoveAmountY = Mathf.Sin(slopeAngle * Mathf.Deg2Rad) * moveDistance;

        if (moveAmount.y <= climbmoveAmountY)
        {
            moveAmount.y = climbmoveAmountY;
            moveAmount.x = Mathf.Cos(slopeAngle * Mathf.Deg2Rad) * moveDistance * Mathf.Sign(moveAmount.x);
            collisions.below = true;
            collisions.climbingSlope = true;
            collisions.slopeAngle = slopeAngle;
            collisions.slopeNormal = slopeNormal;
        }
    }

    void DescendSlope(ref Vector2 moveAmount)
    {

        RaycastHit2D maxSlopeHitLeft = Physics2D.Raycast(rayInfo.raycastOrigins.bottomLeft, Vector2.down, Mathf.Abs(moveAmount.y) + BoxColliderRayInfo.skinWidth, collisionMask);
        RaycastHit2D maxSlopeHitRight = Physics2D.Raycast(rayInfo.raycastOrigins.bottomRight, Vector2.down, Mathf.Abs(moveAmount.y) + BoxColliderRayInfo.skinWidth, collisionMask);
        if (maxSlopeHitLeft ^ maxSlopeHitRight)
        {
            SlideDownMaxSlope(maxSlopeHitLeft, ref moveAmount);
            SlideDownMaxSlope(maxSlopeHitRight, ref moveAmount);
        }

        if (!collisions.slidingDownMaxSlope)
        {
            float directionX = Mathf.Sign(moveAmount.x);
            Vector2 rayOrigin = (directionX == -1) ? rayInfo.raycastOrigins.bottomRight : rayInfo.raycastOrigins.bottomLeft;
            //RaycastHit2D hit = Physics2D.Raycast(rayOrigin, -Vector2.up, Mathf.Infinity, collisionMask);

            if (Physics2D.RaycastNonAlloc(rayOrigin, -Vector2.up, hits, Mathf.Infinity, collisionMask) > 0)
            {
                float slopeAngle = Vector2.Angle(hits[0].normal, Vector2.up);
                if (slopeAngle != 0 && slopeAngle <= maxSlopeAngle)
                {
                    if (Mathf.Sign(hits[0].normal.x) == directionX)
                    {
                        if (hits[0].distance - BoxColliderRayInfo.skinWidth <= Mathf.Tan(slopeAngle * Mathf.Deg2Rad) * Mathf.Abs(moveAmount.x))
                        {
                            float moveDistance = Mathf.Abs(moveAmount.x);
                            float descendmoveAmountY = Mathf.Sin(slopeAngle * Mathf.Deg2Rad) * moveDistance;
                            moveAmount.x = Mathf.Cos(slopeAngle * Mathf.Deg2Rad) * moveDistance * Mathf.Sign(moveAmount.x);
                            moveAmount.y -= descendmoveAmountY;

                            collisions.slopeAngle = slopeAngle;
                            collisions.descendingSlope = true;
                            collisions.below = true;
                            collisions.slopeNormal = hits[0].normal;
                        }
                    }
                }
            }
        }
    }

    void SlideDownMaxSlope(RaycastHit2D hit, ref Vector2 moveAmount)
    {

        if (hit)
        {
            float slopeAngle = Vector2.Angle(hit.normal, Vector2.up);
            if (slopeAngle > maxSlopeAngle)
            {
                moveAmount.x = Mathf.Sign(hit.normal.x) * (Mathf.Abs(moveAmount.y) - hit.distance) / Mathf.Tan(slopeAngle * Mathf.Deg2Rad);

                collisions.slopeAngle = slopeAngle;
                collisions.slidingDownMaxSlope = true;
                collisions.slopeNormal = hit.normal;
            }
        }

    }

    void ResetFallingThroughPlatform()
    {
        collisions.fallingThroughPlatform = false;
    }

    // ============== NEW STUFF ================ //
    /// <summary>
    /// Actualitza la LayerMask del ray info
    /// </summary>
    /// <param name="mask"></param>
    public void UpdateLayerMask(LayerMask mask)
    {
        collisionMask = mask;
    }

    [System.Obsolete("Don't use the face direction in the method, use input or standing platform instead")]
    public void Move(Vector2 moveAmount, int facing)
    {
        Move(moveAmount, Vector2.zero);
    }

    // ============== END NEW STUFF ================ //

    public struct CollisionInfo
    {
        public bool above, below;
        public bool left, right;

        public bool aboveOld, belowOld;
        public bool leftOld, rightOld;

        public GameObject aboveObject;
        public GameObject belowObject;
        public GameObject leftObject;
        public GameObject rightObject;

        public bool climbingSlope;
        public bool descendingSlope;
        public bool slidingDownMaxSlope;

        public float slopeAngle, slopeAngleOld;
        public Vector2 slopeNormal;
        
        /// <summary>
        /// Amount that we moved last frame
        /// </summary>
        public Vector2 moveAmountFinal;
        public Vector2 moveAmountOld;
        public int faceDir;
        public bool fallingThroughPlatform;

        public void Reset()
        {
            aboveOld = above;
            belowOld = below;
            leftOld = left;
            rightOld = right;

            above = below = false;
            left = right = false;

            aboveObject = null;
            belowObject = null;
            leftObject = null;
            rightObject = null;

            climbingSlope = false;
            descendingSlope = false;
            slidingDownMaxSlope = false;
            slopeNormal = Vector2.zero;

            slopeAngleOld = slopeAngle;
            slopeAngle = 0;
        }
    }

}
