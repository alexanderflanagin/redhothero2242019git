using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// Classe base d'on extendre classes que gestionen el moviment de plataformes que poden moure passatgers
/// </summary>
public abstract class Physics2DPlatform : MonoBehaviour
{
    /// <summary>
    /// LayerMask dels elements que ha d'entendre com a passatgers
    /// </summary>
    public LayerMask passengerMask;

    /// <summary>
    /// Guarda la informació dels rajos per al controller
    /// </summary>
    public BoxColliderRayInfo rayInfo;

    /// <summary>
    /// Vector on guardem el moviment de cada frame
    /// </summary>
    protected Vector2 movement;

    /// <summary>
    /// Moviment que aplicarà als passatgers
    /// </summary>
    protected List<PassengerMovement> passengerMovement;

    /// <summary>
    /// Guardem un diccionari de passatgers per a no buscar el Physics2DController cada cop
    /// TODO: Això pot ser global de l'escena
    /// </summary>
    protected Dictionary<Transform, Physics2DController> passengerDictionary = new Dictionary<Transform, Physics2DController>();

    /// <summary>
    /// Mirem si tenim el rayInfo creat i si no el creem
    /// </summary>
    public virtual void Start()
    {
        if(rayInfo == null)
        {
            rayInfo = new BoxColliderRayInfo();
        }

        rayInfo.Initialize(this.GetComponent<BoxCollider2D>());
    }

    /// <summary>
    /// Al habilitar la plataforma, registrem Events per a treure el player dels possibles passatgers quan estigui en travel
    /// </summary>
    void OnEnable()
    {
        StandardStageManager.current.playerControl.events.registerEvent(CharacterEvents.EventName.TravelEnter, RemovePlayerFromLayerMask);
        StandardStageManager.current.playerControl.events.registerEvent(CharacterEvents.EventName.TravelExit, AddPlayerFromLayerMask);
    }

    /// <summary>
    /// Al deshabilitar la plataforma traiem els Events que treien i posaven le player com a possible passatger
    /// </summary>
    void OnDisable()
    {
        StandardStageManager.current.playerControl.events.unregisterEvent(CharacterEvents.EventName.TravelEnter, RemovePlayerFromLayerMask);
        StandardStageManager.current.playerControl.events.unregisterEvent(CharacterEvents.EventName.TravelExit, AddPlayerFromLayerMask);
    }

    /// <summary>
    /// Treu el player de la layer mask de passatgers
    /// </summary>
    void RemovePlayerFromLayerMask()
    {
        passengerMask = Layers.removeLayer(passengerMask, Layers.Player);
    }

    /// <summary>
    /// Posa el player a la layer mask de passatgers
    /// </summary>
    void AddPlayerFromLayerMask()
    {
        passengerMask = Layers.addLayer(passengerMask, Layers.Player);
    }

    /// <summary>
    /// Actualitzem el rayInfo, calculem el moviment, calculem el moviment dels passatgers i ho movem tot (en ordre)
    /// </summary>
    void Update()
    {
        rayInfo.UpdateRaycastOrigins();

        CalculatePlatformMovement();

        // Primer hem de testejar si tenim res a sobre
        CalculatePassengerMovement();

        MovePassengers(true);
        transform.Translate(movement);
        MovePassengers(false);
    }

    /// <summary>
    /// Per a calcular el moviment dels passatgers. Canviara segons el tipus de plataforma
    /// </summary>
    protected abstract void CalculatePassengerMovement();

    /// <summary>
    /// Per a calcular el moviment de la plataforma. Canviara segons la plataforma
    /// </summary>
    protected abstract void CalculatePlatformMovement();

    /// <summary>
    /// Mou els passatgers tenint en compte si es mouen abans o despres de la plataforma
    /// </summary>
    /// <param name="beforeMovePlatform">Si movem els passatgers que es mouen abans que la plataforma o no</param>
    protected void MovePassengers(bool beforeMovePlatform)
    {
        foreach (PassengerMovement passenger in passengerMovement)
        {
            if (!passengerDictionary.ContainsKey(passenger.transform))
            {
                passengerDictionary.Add(passenger.transform, passenger.transform.GetComponent<Physics2DController>());
            }

            if (passenger.moveBeforePlatform == beforeMovePlatform)
            {
                passengerDictionary[passenger.transform].Move(passenger.velocity, passenger.standingOnPlatform, true, movement);
            }
        }
    }
}

/// <summary>
/// Struct per a guardar info dels passetgers als que movem.
/// </summary>
public struct PassengerMovement
{
    public Transform transform;
    public Vector3 velocity;
    public bool standingOnPlatform;
    public bool moveBeforePlatform;

    public PassengerMovement(Transform _transform, Vector3 _velocity, bool _standingOnPlatform, bool _moveBeforePlatform)
    {
        transform = _transform;
        velocity = _velocity;
        standingOnPlatform = _standingOnPlatform;
        moveBeforePlatform = _moveBeforePlatform;
    }
}
