using UnityEngine;
using System.Collections;

/// <summary>
/// Interface per a tots els objectes que poden ser colpejats, siguin animats o no
/// </summary>
public interface IPowerGloveable
{
    /// <summary>
    /// L'objecte que conte la malla, per a la rotacio
    /// </summary>
    GameObject meshObject { get; }

    /// <summary>
    /// Per a saber si es l'objecte colpejat que esta seguint la camera
    /// </summary>
    bool theChosen { get; set; }

    /// <summary>
    /// Funcio que es crida al ser colpejat
    /// </summary>
    /// <param name="lookingAtPoint">La direccio cap a on mira al ser colpejat</param>
    void PowerGloved(Vector3 lookingAtPoint);

    /// <summary>
    /// Es crida abans de ser colpejat. Per a guardar posibles variables que necessitem de l'ojecte
    /// </summary>
    void SaveState();

    /// <summary>
    /// Es crida quan ja s'ha "destruit", per a recuperar els estats que es guardessin al SaveState
    /// </summary>
    void RecoverState();

    /// <summary>
    /// Es crida quan xoca amb algun element
    /// </summary>
    void Collide(GameObject with);

    /// <summary>
    /// Es crida quan es destrueix perque no ha xocat amb res i ja ha passat massa temps
    /// </summary>
    void DestroyByTime();
}
