﻿
public interface IKillable {
    void StartDying(bool byTouch);
}
