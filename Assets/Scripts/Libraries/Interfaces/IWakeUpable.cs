using UnityEngine;
using System.Collections;

public interface IWakeUpable {

	/// <summary>
    /// Accio que es disparara quan es vulgui despertar a l'objecte
	/// </summary>
	void WakeUp();

    /// <summary>
    /// Acció que es dispara quan es vol posar a dormir l'objecte
    /// </summary>
    void Sleep();

	/// <summary>
    /// Per a posar la distancia del trigger des del mateix trigger
	/// </summary>
	/// <param name="distance">Distància a la que està el trigger</param>
	void SetTriggerDistance( float distance );

    void SetTriggerOffset(Vector2 offset);

    void SetTriggerSize(Vector2 size);


	/// <summary>
    /// Per a recollir la distancia del trigger des del mateix trigger
	/// </summary>
	/// <returns>Distància a la que està el trigger</returns>
	float GetTriggerDistance();

    Vector2 GetTriggerOffset();

    Vector2 GetTriggerSize();
}
