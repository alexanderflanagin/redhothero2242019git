﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public interface IDisplacer {

    // List<Physics2DController> displacingControllers { get; set; }
    
    void OnTouching( GameObject displaced );
	
	void OnUntouching( GameObject displaced );
	
	GameObject GetPlatformGameObject();

    // void WillIMoveSomething(Vector3 movement);
}
