
public interface IUserInput
{
    /// <summary>
    /// Handles x direction
    /// </summary>
    InputButton xAxis { get; set; }

    /// <summary>
    /// Handles y direction
    /// </summary>
    InputButton yAxis { get; set; }

    /// <summary>
    /// Roll button
    /// </summary>
    InputButton roll { get; set; }

    /// <summary>
    /// Jump button
    /// </summary>
    InputButton jump { get; set; }

    /// <summary>
    /// Travel button
    /// </summary>
    InputButton travel { get; set; }

    /// <summary>
    /// Shoot button
    /// </summary>
    InputButton shoot { get; set; }

    /// <summary>
    /// Punch button
    /// </summary>
    InputButton punch { get; set; }

    /// <summary>
    /// Brake/anchor button
    /// </summary>
    InputButton brake { get; set; }

    /// <summary>
    /// If user input is freezed
    /// </summary>
    bool freeze { get; }

    /// <summary>
    /// Prepare the user input
    /// </summary>
	void Prepared();

    /// <summary>
    /// Freeze the user input
    /// </summary>
    /// <param name="continueRunning">Sets X axis value</param>
    /// <param name="cancelJump">Cancell jump or let jump finish</param>
	void Freeze(int continueRunning = 1, bool cancelJump = false);

    /// <summary>
    /// Enables the user input again
    /// </summary>
	void Defreeze();

    /// <summary>
    /// Says if player can perform jump action after /time/ seconds
    /// </summary>
    /// <param name="time">Seconds passed to test if still can jump</param>
    /// <returns></returns>
	bool StillCanJump(float time);

    /// <summary>
    /// Sets automatically the xAxis value for /time/ time
    /// </summary>
    /// <param name="direction">Direction of the xAxis</param>
    /// <param name="time">Time that will be forced</param>
    void AutoXAxis(int direction, float time);

    /// <summary>
    /// Freezes the user input except to perform travels
    /// </summary>
    void FreezeExceptTravel();
}
