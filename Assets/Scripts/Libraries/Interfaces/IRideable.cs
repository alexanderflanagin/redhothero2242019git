﻿using UnityEngine;
using System.Collections;

public interface IRideable {

	void OnRide( GameObject rider );

	void OnUnride( GameObject rider);

	GameObject GetPlatformGameObject();
}
