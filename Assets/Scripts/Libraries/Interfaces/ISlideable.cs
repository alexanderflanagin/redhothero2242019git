﻿using UnityEngine;
using System.Collections;

public interface ISlideable {

	/*
	 * Es dispara al entrar un Slide
	 */
	void EnterSlide( float time, float speed );

	/*
	 * Per mirar si pot relliscar o no
	 */
	bool CanSlide();
}
