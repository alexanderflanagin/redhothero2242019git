﻿using UnityEngine;
using System.Collections;

public static class Utils {

	public static Vector3 FloatArrayToVector3( float[] arr ) {
		return new Vector3( arr[0], arr[1], arr[2] );
	}
	
	public static Quaternion FloatArrayToQuaternion( float[] arr ) {
		return new Quaternion( arr[0], arr[1], arr[2], arr[3] );
	}
	
	public static float[] StringToFloatArray( string vector ) {
		vector = vector.Substring( 1, vector.Length - 2 );
		
		string[] exitString = vector.Split( ',' );
		float[] exit = new float[ exitString.Length ];
		
		for( int i = 0; i < exitString.Length; i++ ) {
			exit[i] = float.Parse( exitString[i] );
		}
		
		return exit;
	}
}
