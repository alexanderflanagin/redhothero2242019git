using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class TimeStepController : MonoBehaviour
{
    public float timeStep = 1;
    //public float volume;
    //public GameObject _mainChar;

    void OnGUI()
    {
        timeStep = GUI.HorizontalSlider(new Rect(Screen.width / 2 - 50, Screen.height / 12, 100, 20), timeStep, 0.0f, 1.0f);
        Time.timeScale = timeStep;

        //volume = GUI.HorizontalSlider ( new Rect ( Screen.width/2 - 50, Screen.height/8, 100, 20 ), volume, 0.0f, 1.0f );
        //AudioListener.volume = volume;


        //if (GUI.Button( new Rect ( Screen.width/2 - 50, Screen.height/30, 100, 20 ), "Reset Player" ) && _mainChar ){
        //    if (_mainChar){
        //        _mainChar.transform.position = Vector3.zero;
        //    }
        //}
    }
}
