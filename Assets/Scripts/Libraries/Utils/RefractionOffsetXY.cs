using UnityEngine;

/// <summary>
/// Moves a texture in X Y every frame
/// </summary>
public class RefractionOffsetXY : MonoBehaviour
{
    /// <summary>
    /// Renderer from which we get the material
    /// </summary>
    public new Renderer renderer;

    /// <summary>
    /// Texture speed
    /// </summary>
    public Vector2 textureSpeed;

    /// <summary>
    /// Texture offset
    /// </summary>
    private Vector2 textureOffset;

    /// <summary>
    /// Calculates new offset and assign to the texture
    /// </summary>
    void Update()
    {
        textureOffset += textureSpeed * Time.deltaTime;
        renderer.material.SetTextureOffset("_Refraction", textureOffset);
    }
}
