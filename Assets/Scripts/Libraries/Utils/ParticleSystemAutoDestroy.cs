using UnityEngine;
using System.Collections;

/// <summary>
/// Fa que s'auto-destrueixi un sistema de partícules quan acaba
/// Tret de: http://answers.unity3d.com/questions/219609/auto-destroying-particle-system.html
/// </summary>
public class ParticleSystemAutoDestroy : MonoBehaviour
{
    /// <summary>
    /// El sistema de partícules pare
    /// </summary>
    private ParticleSystem ps;

    /// <summary>
    /// Al Start, agafem el sistema de particules
    /// </summary>
    public void Start()
    {
        ps = GetComponent<ParticleSystem>();
    }

    /// <summary>
    /// Si tenim sistema de partícules i ja no està Alive, el destruim
    /// </summary>
    public void Update()
    {
        if (ps)
        {
            if (!ps.IsAlive())
            {
                Destroy(gameObject);
            }
        }
    }
}
