using UnityEngine;
using UnityEngine.UI;
using System.Collections;

/// <summary>
/// Classe per a gestionar les 4 tecles que fem servir com a shortcut d'accions de la Console
/// </summary>
public class ShortcutKeys
{
    /// <summary>
    /// Nom de la Key 1 per al cInput
    /// </summary>
    public const string KEY_1 = "Key 1";

    /// <summary>
    /// Nom de la Key 2 per al cInput
    /// </summary>
    public const string KEY_2 = "Key 2";

    /// <summary>
    /// Nom de la Key 3 per al cInput
    /// </summary>
    public const string KEY_3 = "Key 3";

    /// <summary>
    /// Nom de la Key 4 per al cInput
    /// </summary>
    public const string KEY_4 = "Key 4";

    /// <summary>
    /// Accions de les keys
    /// </summary>
    private KeyAction[] keysActions;

    /// <summary>
    /// Canvas on tenim el quadre de text on mostrem info de que hem clicat
    /// </summary>
    private GameObject canvas;

    /// <summary>
    /// El camp de text on es mostra l'accio que hem aplicat
    /// </summary>
    private Text textField;

    /// <summary>
    /// Variable que fem servir per saber quan hem d'amagar el canvas (si l'hem mostrat)
    /// </summary>
    private float hiddeTime;

    /// <summary>
    /// Posa les keys al cInput, instancia el Canvas i crea l'array de KeyActions per defecte
    /// </summary>
    public ShortcutKeys()
    {
#if !UNITY_EDITOR_OSX && !UNITY_STANDALONE_OSX
        if (GameManager.Instance.gameData.gameData.controllerCheatsEnabled)
        {
            cInput.SetKey(KEY_1, Keys.C, Keys.Xbox1DPadDown);
            cInput.SetKey(KEY_2, Keys.V, Keys.Xbox1DPadRight);
            cInput.SetKey(KEY_3, Keys.B, Keys.Xbox1DPadUp);
            cInput.SetKey(KEY_4, Keys.N, Keys.Xbox1DPadLeft);
        }
        else
        {
            cInput.SetKey(KEY_1, Keys.C);
            cInput.SetKey(KEY_2, Keys.V);
            cInput.SetKey(KEY_3, Keys.B);
            cInput.SetKey(KEY_4, Keys.N);
        }
#endif

        CreateTextDialog();

        keysActions = new KeyAction[4];
        keysActions[0] = new KeyAction("pacomode", null);
        keysActions[1] = new KeyAction("e", null);
        keysActions[2] = new KeyAction("pacomode", null);
        keysActions[3] = new KeyAction("e", null);
    }

    /// <summary>
    /// Crea el canvas on es mostrara la key que hem premut
    /// </summary>
    private void CreateTextDialog()
    {
        canvas = GameObject.Instantiate(GeneralPurposeObjectManager.Instance.achivementWindow) as GameObject;
        GameObject.DontDestroyOnLoad(canvas);

        textField = canvas.GetComponentInChildren<Text>();

        canvas.SetActive(false);
    }

    /// <summary>
    /// Mira si tenim entrada de teclat i actualitza l'estat visual del canvs
    /// </summary>
    public void Update()
    {
        bool active = false;
#if !UNITY_EDITOR_OSX && !UNITY_STANDALONE_OSX
        if (cInput.GetKeyDown(KEY_1))
        {
            textField.text = ConsoleCommandsRepository.Instance.ExecuteCommand(keysActions[0].action, keysActions[0].args);
            active = true;
        }

        if (cInput.GetKeyDown(KEY_2))
        {
            textField.text = ConsoleCommandsRepository.Instance.ExecuteCommand(keysActions[1].action, keysActions[1].args);
            active = true;
        }

        if (cInput.GetKeyDown(KEY_3))
        {
            textField.text = ConsoleCommandsRepository.Instance.ExecuteCommand(keysActions[2].action, keysActions[2].args);
            active = true;
        }

        if (cInput.GetKeyDown(KEY_4))
        {
            textField.text = ConsoleCommandsRepository.Instance.ExecuteCommand(keysActions[3].action, keysActions[3].args);
            active = true;
        }
#endif
        if(active)
        {
            hiddeTime = 0;
            canvas.SetActive(true);
        }
        else
        {
            hiddeTime += Time.deltaTime;
            if(hiddeTime > 2)
            {
                canvas.SetActive(false);
            }
        }
    }

    /// <summary>
    /// Assigna a una key una nova acci�
    /// </summary>
    /// <param name="key">Posicio en l'array de la key 0..3</param>
    /// <param name="action">Accio que s'executa</param>
    /// <param name="args">Arguments de l'accio</param>
    public void SetKeyAction(int key, string action, string[] args)
    {
        keysActions[key] = new KeyAction(action, args);
    }

    /// <summary>
    /// Struct per a guardar info de les accions de les keys
    /// </summary>
    public struct KeyAction
    {
        public string action;
        public string[] args;

        public KeyAction(string _action, string[] _args)
        {
            action = _action;
            args = _args;
        }
    }
}
