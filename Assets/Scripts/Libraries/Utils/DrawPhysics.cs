using UnityEngine;
using System.Collections;

public class DrawPhysics : MonoBehaviour {

	private Vector2[] points;
	private BoxCollider2D boxCollider;

	private bool Initialize() {
		points = new Vector2[4];
		boxCollider = GetComponent<BoxCollider2D>();

		if( boxCollider == null ) {
            //Debug.LogWarning ( "[DrawPhysics::Initialize] Los colliders no estan correctamente colocados.", this.gameObject );
			return false;
		}

		boxCollider.size = new Vector2( 1, 1 );
		// boxCollider.center = Vector2.zero;

		float x = transform.localScale.x;
		float y = transform.localScale.y;

		Vector2 position = new Vector2( transform.position.x, transform.position.y );

		points[0] = position + new Vector2( - x / 2  + x * boxCollider.offset.x, y / 2 + y * boxCollider.offset.y );
		points[1] = position + new Vector2( x / 2 + x * boxCollider.offset.x, y / 2 + y * boxCollider.offset.y );
		points[2] = position + new Vector2( x / 2 + x * boxCollider.offset.x, -y / 2 + y * boxCollider.offset.y );
		points[3] = position + new Vector2( - x / 2 + x * boxCollider.offset.x, -y / 2 + y * boxCollider.offset.y );

		return true;
	}

    void OnDrawGizmos()
    {
        if (Initialize())
        {
            if (gameObject.layer == Layers.OneWayPlatform)
            {
                Gizmos.color = Color.blue;
            }
            else if (GetComponent<SaggingPlatform>())
            {
                Gizmos.color = Color.red;
            }
            else
            {
                Gizmos.color = Color.green;
            }

            Gizmos.DrawLine(points[0], points[1]);
            Gizmos.DrawLine(points[1], points[2]);
            Gizmos.DrawLine(points[2], points[3]);
            Gizmos.DrawLine(points[3], points[0]);
        }
    }
}
