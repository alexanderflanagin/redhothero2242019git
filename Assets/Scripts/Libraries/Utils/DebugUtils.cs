using UnityEngine;
using System.Collections;

/// <summary>
/// Classe que fare servir amb utilitats per a fer logs més complets
/// </summary>
public static class DebugUtils
{
    public static bool debug = true;

    /// <summary>
    /// Mira si es compleix la condició, i si no fa un log del missatge
    /// </summary>
    /// <param name="condition">Condicio que s'ha de complir</param>
    /// <param name="message">Missatge a mostrar</param>
    public static void Assert(bool condition, object message)
    {
        if (!condition)
        {
            UnityEngine.Debug.Log(message);
        }
    }

    /// <summary>
    /// Mira si es compleix la condició, i si no fa un log del missatge i el context
    /// </summary>
    /// <param name="condition">Condicio que s'ha de complir</param>
    /// <param name="message">Missatge a mostrar</param>
    /// <param name="context">Context del missatge</param>
    public static void Assert(bool condition, object message, UnityEngine.Object context)
    {
        if (!condition)
        {
            UnityEngine.Debug.Log(message, context);
        }
    }

    /// <summary>
    /// Mira si es compleix la condició, i si no fa un warning del missatge
    /// </summary>
    /// <param name="condition">Condicio que s'ha de complir</param>
    /// <param name="message">Missatge a mostrar</param>
    public static void AssertWarning(bool condition, object message)
    {
        if (!condition)
        {
            UnityEngine.Debug.LogWarning(message);
        }
    }

    /// <summary>
    /// Mira si es compleix la condició, i si no fa un warning del missatge i el context
    /// </summary>
    /// <param name="condition">Condicio que s'ha de complir</param>
    /// <param name="message">Missatge a mostrar</param>
    /// <param name="context">Context del missatge</param>
    public static void AssertWarning(bool condition, object message, UnityEngine.Object context)
    {
        if (!condition)
        {
            UnityEngine.Debug.LogWarning(message, context);
        }
    }

    /// <summary>
    /// Mira si es compleix la condició, i si no fa un error del missatge
    /// </summary>
    /// <param name="condition">Condicio que s'ha de complir</param>
    /// <param name="message">Missatge a mostrar</param>
    public static void AssertError(bool condition, object message)
    {
        if (!condition)
        {
            UnityEngine.Debug.LogError(message);
        }
    }

    /// <summary>
    /// Mira si es compleix la condició, i si no fa un error del missatge i el context
    /// </summary>
    /// <param name="condition">Condicio que s'ha de complir</param>
    /// <param name="message">Missatge a mostrar</param>
    /// <param name="context">Context del missatge</param>
    public static void AssertError(bool condition, object message, UnityEngine.Object context)
    {
        if (!condition)
        {
            UnityEngine.Debug.LogError(message, context);
        }
    }

    /// <summary>
    /// Used to draw rectangles
    /// </summary>
    /// <param name="min">X and Y min position</param>
    /// <param name="max">X and Y max position</param>
    public static void DrawRectangle(Vector2 min, Vector2 max)
    {
        DrawRectangle(min, max, Color.white, 0);
    }

    /// <summary>
    /// Draw a rectangle of a certain color
    /// </summary>
    /// <param name="min">X and Y min position</param>
    /// <param name="max">X and Y max position</param>
    /// <param name="color">Color of the rectangle</param>
    public static void DrawRectangle(Vector2 min, Vector2 max, Color color)
    {
        DrawRectangle(min, max, color, 0);
    }

    /// <summary>
    /// Draw a rectangle of a certain color
    /// </summary>
    /// <param name="min">X and Y min position</param>
    /// <param name="max">X and Y max position</param>
    /// <param name="color">Color of the rectangle</param>
    /// <param name="time">How much time should it last</param>
    public static void DrawRectangle(Vector2 min, Vector2 max, Color color, float time)
    {
        Vector3 topLeft = new Vector3(min.x, max.y);
        Vector3 topRight = max;
        Vector3 bottomRight = new Vector3(max.x, min.y);
        Vector3 bottomLeft = min;

        Debug.DrawLine(topLeft, topRight, color, time);
        Debug.DrawLine(topRight, bottomRight, color, time);
        Debug.DrawLine(bottomRight, bottomLeft, color, time);
        Debug.DrawLine(bottomLeft, topLeft, color, time);
    }
}
