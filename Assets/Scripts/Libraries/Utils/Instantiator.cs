using UnityEngine;
using System.Collections;

public class Instantiator : MonoBehaviour {


	public static void LoadParticleSystem ( ref ParticleSystem particleSystem, Vector3 position, Vector3 rotation, GameObject parent ) {
		if ( particleSystem ) {
			particleSystem = Instantiate ( particleSystem, Vector3.zero, Quaternion.Euler ( rotation ) ) as ParticleSystem;
			particleSystem.transform.parent = parent.transform;
			particleSystem.transform.localPosition = position;
		}
	}

    public static void LoadParticleSystem(ref ParticleSystem particleSystem, Vector3 position, Vector3 rotation)
    {
        if (particleSystem)
        {
            particleSystem = Instantiate(particleSystem, position, Quaternion.Euler(rotation)) as ParticleSystem;
        }
    }

    public static void LoadGameObject ( ref GameObject gameObject, Vector3 position, Vector3 rotation, GameObject parent, bool active ) {
		if ( gameObject ) {
			gameObject = Instantiate ( gameObject, Vector3.zero, Quaternion.Euler (rotation) ) as GameObject;
			gameObject.transform.parent = parent.transform;
			gameObject.transform.position = parent.transform.position + position;
			gameObject.SetActive (active);
		}
	}

	public static void LoadGameObject ( ref GameObject gameObject, Vector3 position, Vector3 rotation, GameObject parent ) {
		if ( gameObject ) {
			gameObject = Instantiate ( gameObject, Vector3.zero, Quaternion.Euler (rotation) ) as GameObject;
			gameObject.transform.parent = parent.transform;
			gameObject.transform.position = parent.transform.position + position;
		}
	}
	
	public void LoadGameObject ( ref GameObject gameObject, Vector3 position, Vector3 rotation, bool active ) {
		if ( gameObject ) {
			gameObject = Instantiate ( gameObject, position, Quaternion.Euler (rotation) ) as GameObject;
			gameObject.transform.position = transform.position + position;
			gameObject.SetActive (active);

		}
	}
	public void LoadGameObject ( ref GameObject gameObject, Vector3 position, Vector3 rotation ) {
		if ( gameObject ) {
			gameObject = Instantiate ( gameObject, position, Quaternion.Euler (rotation) ) as GameObject;
			gameObject.transform.position = transform.position + position;
		}
	}

}
