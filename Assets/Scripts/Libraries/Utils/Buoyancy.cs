﻿using UnityEngine;
using System.Collections;

public class Buoyancy : MonoBehaviour {

	/*
	 * El angle que fa entre un punt i l'altre del moviment
	 */
	public float angle;

	/*
	 * Velocitat de moviment
	 */
	public float speed;
	
	/*
	 * Quina inclinacio tenim en aquest moment
	 */
	private float _step = 0;
	
	/*
	 * Rotacio inicial de l'objecte (segons transform.rotation)
	 */
	private Quaternion _startRotation;

	private AnimationCurve _curve;



	// Use this for initialization
	void Start () {
		_startRotation = transform.rotation;

		// Dividim l'angle per dos perque sumarem i restarem la meitat per a mouren's respecte al 0
		angle = angle / 2;

		_curve = AnimationCurve.EaseInOut(0,-angle,speed,angle);
		_curve.postWrapMode = WrapMode.PingPong;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		// Canviem l'inclinacio actual segons el temps i la velocitat
		_step = _curve.Evaluate( Mathf.PingPong( Time.time, (speed - 0.1f) ) );

		// Actualitzem la rotacio afegint la rotacio anterior calculada. Restem l'angle perque volem anar entre a/2 i -a/2 i no entre a i 0.
		transform.rotation = Quaternion.Euler( new Vector3(_step,0, 0) ) * _startRotation;
	}
}
