using UnityEngine;

/// <summary>
/// Classe que farem servir per a controlar els inputs
/// </summary>
public class InputButton
{
    public const string MENU_UP = "Menu Up";
    public const string MENU_DOWN = "Menu Down";
    public const string MENU_HORIZONTAL_AXIS = "Menu Horizontal Axis";
    public const string MENU_LEFT = "Menu Left";
    public const string MENU_RIGHT = "Menu Right";
    public const string MENU_VERTICAL_AXIS = "Menu Vertical Axis";
    public const string MENU_ENTER = "Menu Enter";
    public const string MENU_BACK = "Menu Back";

    public const string LEFT = "Left";
    public const string RIGHT = "Right";
    public const string HORIZONTAL_AXIS = "Horizontal Axis";

    public const string UP = "Up";
    public const string DOWN = "Down";
    public const string VERTICAL_AXIS = "Vertical Axis";

    //public const string AIM_LEFT = "Aim Left";
    //public const string AIM_RIGHT = "Aim Right";
    //public const string AIM_HORIZONTAL_AXIS = "Aim Horizontal Axis";

    //public const string AIM_UP = "Aim Up";
    //public const string AIM_DOWN = "Aim Down";
    //public const string AIM_VERTICAL_AXIS = "Aim Vertical Axis";

    //public const string AIM_DIAGONAL_LEFT = "Aim Diagonal Left";
    //public const string AIM_DIAGONAL_RIGHT = "Aim Diagonal Right";
    //public const string AIM_DIAGONAL_AXIS = "Aim Diagonal Axis";

    public const string JUMP = "Jump";
    public const string SPRINT = "Sprint";
    public const string PUNCH = "Punch";
    public const string SHOOT = "Shoot";
    public const string TRAVEL = "Travel";
    public const string ROLL = "Roll";

    public const string BRAKE = "Brake";

    public const string PAUSE = "Pause";

    public const string RESTART = "Restart";

    public const string ALT = "Alt";

    /// <summary>
    /// El nom del botó/Axis en el input manager
    /// </summary>
    public string name;
        

    /// <summary>
    /// Getter públic del <see cref="_down"/>. Té en compte l'autoValue
    /// </summary>
    public bool down
    {
        get
        {
            if (autoValue)
            {
                return false;
            }
            else
            {
                return !_isAxis && (cInput.GetKeyDown(name) || (this.hasAlt && cInput.GetKeyDown(name + InputButton.ALT)));
            }
        }
    }

    /// <summary>
    /// Getter públic del <see cref="_val"/>. Té en compte l'autoValue
    /// </summary>
    public float val
    {
        get
        {
            if (autoValue)
            {
                return autoValueValue;
            }
            else
            {
                if (_isAxis)
                {
                    return cInput.GetAxis(name);
                }
                else
                {
                    return ((cInput.GetKey(name) || (this.hasAlt && cInput.GetKey(name + InputButton.ALT)))) ? 1 : 0;
                }
            }
        }
    }

    /// <summary>
    /// Valor real del boto/axis, ignorant autoValues
    /// </summary>
    public float realVal
    {
        get
        {
            if (_isAxis)
            {
                return cInput.GetAxis(name);
            }
            else
            {
                return ((cInput.GetKey(name) || (this.hasAlt && cInput.GetKey(name + InputButton.ALT)))) ? 1 : 0;
            }
        }
    }

    /// <summary>
    /// El valor actual del botó en número absolut
    /// </summary>
    public float absoluteVal
    {
        get
        {
            if (autoValue)
            {
                return Mathf.Ceil(Mathf.Abs(autoValueValue)) * Mathf.Sign(autoValueValue);
            }
            else
            {
                if (_isAxis)
                {
                    return Mathf.Ceil(Mathf.Abs(cInput.GetAxis(name))) * Mathf.Sign(cInput.GetAxis(name));
                }
                else
                {
                    return ((cInput.GetKey(name) || (this.hasAlt && cInput.GetKey(name + InputButton.ALT)))) ? 1 : -1; // Això no hauria de ser -1 em sembla, però de moment ho poso així
                }
            }
        }
    }
    
    /// <summary>
    /// Mira si el botó / axis està premut o no
    /// </summary>
    public bool check
    {
        get
        {
            if (autoValue)
            {
                return autoValueValue != 0;
            }
            else
            {
                if (_isAxis)
                {
                    return cInput.GetAxis(name) != 0;
                }
                else
                {
                    return (cInput.GetKey(name) || (this.hasAlt && cInput.GetKey(name + InputButton.ALT)));
                }
            }
        }
    }

    /// <summary>
    /// Getter públic del <see cref="_up"/>. Té en compte l'autoValue
    /// </summary>
    public bool up
    {
        get
        {
            if (autoValue)
            {
                return false;
            }
            else
            {
                return !_isAxis && (cInput.GetKeyUp(name) || (this.hasAlt && cInput.GetKeyUp(name + InputButton.ALT)));
            }
        }
    }

    /// <summary>
    /// Si el valor es posa automàticament segons el valor de autoValueValue
    /// </summary>
    public bool autoValue;

    /// <summary>
    /// El valor que es posarà automàticament
    /// </summary>
    public float autoValueValue;

    /// <summary>
    /// Si és un axis (per a agafar els ups i downs del update)
    /// </summary>
    private bool _isAxis;

    /// <summary>
    /// Si ha estat revisat i existeix
    /// </summary>
    private bool _checked;

    /// <summary>
    /// Si té una configuració de botons alternativa
    /// </summary>
    private bool hasAlt;

    public bool alreadyChecked
    {
        get
        {
            return _checked;
        }
    }

    /// <summary>
    /// Al crear l'objecte, mirem si el nom és correcte. Sinó ja no farem l'update
    /// </summary>
    public bool Prepare()
    {
        _checked = false;

        if (cInput.IsAxisDefined(name))
        {
            _checked = true;
            _isAxis = true;

            if (cInput.IsAxisDefined(this.name + InputButton.ALT))
            {
                this.hasAlt = true;
            }
        }
        else if (cInput.IsKeyDefined(name))
        {
            _checked = true;
            _isAxis = false;

            if (cInput.IsKeyDefined(this.name + InputButton.ALT))
            {
                this.hasAlt = true;
            }
        }

        return _checked;
    }

    /// <summary>
    /// Crea el botó
    /// </summary>
    /// <param name="name">Nom que té el botó a l'InputManager</param>
    public void Create(string name)
    {
        this.name = name;
        Prepare();
    }

    /// <summary>
    /// General el nom alternatiu d'una accio
    /// </summary>
    /// <param name="name">Nom de l'accio</param>
    /// <returns>Nom alternatiu de l'accio</returns>
    public static string AltName(string name)
    {
        return name + ALT;
    }
}
