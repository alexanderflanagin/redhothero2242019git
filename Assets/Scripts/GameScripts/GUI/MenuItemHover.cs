using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MenuItemHover : MonoBehaviour, ISelectHandler
{
    public string itemSelectedClip = "SelectOption";

    private bool used;

	public void OnSelect(BaseEventData eventData)
	{
        if (FindObjectOfType<EventSystem>().firstSelectedGameObject != this.gameObject || this.used)
        {
            AudioManager.Instance.PlayOneShoot(AudioClipsData.ClipsGroups.Enviro, itemSelectedClip);
        }
        else if (FindObjectOfType<EventSystem>().firstSelectedGameObject == this.gameObject)
        {
            this.used = true;
        }
        
	}
}
