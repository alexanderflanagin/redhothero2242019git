using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

public class ConfigureControls : MonoBehaviour
{
    /// <summary>
    /// El farem servir per el sorol del click cada cop que cliquem
    /// </summary>
    public ClickButtonSound clickButtonSound;
    
    /// <summary>
    /// GameObject que mostrem quan volem indicar que s'ha de premer algun boto
    /// </summary>
    public GameObject pressAnyKey;

    /// <summary>
    /// GameObject que mostrem per indicar que no podem repetir els botons de pause i restart
    /// </summary>
    public GameObject pauseAndRestartMustBe;

    /// <summary>
    /// Event system actual. Guardem una referencia per quan el deshabilitem
    /// </summary>
    private EventSystem eventSystem;

    /// <summary>
    /// El boto de mostrar els botons de teclat
    /// </summary>
    public TabButtonInfo keyboardButton;

    /// <summary>
    /// El boto de mostrar els botons del pad
    /// </summary>
    public TabButtonInfo padButton;

    /// <summary>
    /// El boto de tirar enrera
    /// </summary>
    public Button backButton;

    /// <summary>
    /// El boto per a fer reset
    /// </summary>
    public Button resetButton;

    /// <summary>
    /// Una llista amb totes les tecles fetes servir, per a no poder repetir-nos
    /// </summary>
    public List<string> usedKeys;

    /// <summary>
    /// L'esquema de navegacio del boto de back, per a canviar segons si estem mostrant keyboard o pad
    /// </summary>
    private Navigation backNavigation;
    
    /// <summary>
    /// L'esquema de navegacio del boto de reset, per a canviar segons si estem mostrant keyboard o pad
    /// </summary>
    private Navigation resetNavigation;

    /// <summary>
    /// L'esquema de navegacio del boto de keyboard, per a canviar segons si estem mostrant keyboard o pad
    /// </summary>
    private Navigation keyboardNavigation;

    /// <summary>
    /// L'esquema de navegacio del boto de pad, per a canviar segons si estem mostrant keyboard o pad
    /// </summary>
    private Navigation padNavigation;

    /// <summary>
    /// Generem els esquemes de navegacio
    /// </summary>
    void Awake()
    {
        backNavigation = new Navigation();
        backNavigation.mode = Navigation.Mode.Explicit;
        backNavigation.selectOnUp = resetButton;

        resetNavigation = new Navigation();
        resetNavigation.mode = Navigation.Mode.Explicit;
        resetNavigation.selectOnDown = backButton;
        resetNavigation.selectOnUp = padButton.button;

        keyboardNavigation = new Navigation();
        keyboardNavigation.mode = Navigation.Mode.Explicit;
        keyboardNavigation.selectOnRight = padButton.button;

        padNavigation = new Navigation();
        padNavigation.mode = Navigation.Mode.Explicit;
        padNavigation.selectOnLeft = keyboardButton.button;
        padNavigation.selectOnRight = backButton;

        usedKeys = new List<string>();
        usedKeys.Add(cInput.GetText(InputButton.MENU_ENTER));

        // Marquem que ja hem passat per aquí
        MainMenuManager mainMenuManager = FindObjectOfType<MainMenuManager>();

        if(mainMenuManager)
        {
            mainMenuManager.ControlsViewed();
        }
        
    }

    void Update()
    {
        if(!cInput.scanning && (cInput.GetKeyDown(InputButton.MENU_BACK) || cInput.GetKeyDown(InputButton.PUNCH)))
        {
            backButton.Select();
            var pointer = new PointerEventData(EventSystem.current);
            ExecuteEvents.Execute(backButton.gameObject, pointer, ExecuteEvents.submitHandler);
        }
    }

    

    /// <summary>
    /// Actualitzem els esquemes de navegacio segons si estem mostrant el teclat o el pad
    /// </summary>
    /// <param name="keyboard">Cert si estem mostrant el teclat</param>
    public void UpdateNavigationOrder(bool keyboard)
    {
        if (keyboard)
        {
            // Modificacions grafiques
            //keyboardButton.text.color = selectedKeysButtonColor;
            //padButton.text.color = Color.white;
            keyboardButton.underline.enabled = true;
            padButton.underline.enabled = false;

            // Amaguem pestanyes
            padButton.firstInsideButton.transform.parent.gameObject.SetActive(false);
            keyboardButton.firstInsideButton.transform.parent.gameObject.SetActive(true);

            // Actualitzem navegacio
            backNavigation.selectOnDown = keyboardButton.lastInsideButton;
            backNavigation.selectOnLeft = keyboardButton.firstInsideButton;

            resetNavigation.selectOnLeft = keyboardButton.firstInsideButton;

            keyboardNavigation.selectOnDown = keyboardButton.firstInsideButton;
            padNavigation.selectOnDown = keyboardButton.lastInsideButton;
        }
        else
        {
            //keyboardButton.text.color = Color.white;
            //padButton.text.color = selectedKeysButtonColor;
            keyboardButton.underline.enabled = false;
            padButton.underline.enabled = true;

            padButton.firstInsideButton.transform.parent.gameObject.SetActive(true);
            keyboardButton.firstInsideButton.transform.parent.gameObject.SetActive(false);

            // Actualitzem navegacio
            backNavigation.selectOnDown = padButton.lastInsideButton;
            backNavigation.selectOnLeft = padButton.firstInsideButton;

            resetNavigation.selectOnLeft = padButton.firstInsideButton;

            keyboardNavigation.selectOnDown = padButton.firstInsideButton;
            padNavigation.selectOnDown = padButton.lastInsideButton;
        }

        backButton.navigation = backNavigation;
        resetButton.navigation = resetNavigation;

        keyboardButton.button.navigation = keyboardNavigation;
        padButton.button.navigation = padNavigation;
        
        if(clickButtonSound)
        {
            clickButtonSound.OnClickSound();
        }
    }

    /// <summary>
    /// Llegeix un boto i l'assigna al cInput
    /// </summary>
    /// <param name="name">Nom del boto dins del cInput</param>
    /// <param name="keyboard">Cert si es tracta del teclat</param>
    /// <param name="text">L'objecte Text de la UI que mostra el nom de la tecla</param>
    /// <param name="next">Objecte que sel.leccionarem despres</param>
    public void ReadButton(ConfigureControl control, string name, bool keyboard, Text text, GameObject next)
    {
        eventSystem = EventSystem.current;
        eventSystem.enabled = false;
        pressAnyKey.SetActive(true);
        StartCoroutine(WaitReadButton(control, name, (keyboard) ? 1 : 2, text, keyboard, !keyboard, !keyboard, keyboard, next));
    }

    /// <summary>
    /// Torna els controls als estandards i actualitza els texts dels buttons
    /// </summary>
    public void SetDefaultControls()
    {
        cInput.Clear();
        cInput.ResetInputs();

        ConfigureControl[] allControls = GameObject.FindObjectsOfType<ConfigureControl>();

        for (int i = 0; i < allControls.Length; i++)
        {
            allControls[i].ResetName();
        }
    }

    /// <summary>
    /// Llegeix un boto, esperant l'input de l'usuari, mostra avisos de que s'esta fent, valida i mostra missatges de que s'est� treballant
    /// </summary>
    /// <param name="buttonName">El nom de l'accio/boto que volem configurar</param>
    /// <param name="position">La posicio en el cInput (1 teclat, 2 pad)</param>
    /// <param name="textToChange">El text que actualitzarem amb el nom del boto</param>
    /// <param name="mouseButton">Si pot ser un boto del mouse</param>
    /// <param name="joyAx">Si pot ser una axis del joystick</param>
    /// <param name="joyBut">Si pot ser un boto del joystick</param>
    /// <param name="keyb">Si pot ser una tecla del teclat</param>
    /// <param name="nextButton">Boto que s'ha de sel.leccionar quan acabem</param>
    /// <returns></returns>
    private IEnumerator WaitReadButton(ConfigureControl control, string buttonName, int position, Text textToChange, bool mouseButton, bool joyAx, bool joyBut, bool keyb, GameObject nextButton)
    {
        string oldKey = cInput.GetText(buttonName, position);

        cInput.ChangeKey(buttonName, position, false, mouseButton, joyAx, joyBut, keyb);
        while (cInput.scanning)
        {
            yield return null;
        }
        clickButtonSound.OnClickSound();

        // Name of the key, not the action
        string keyName = cInput.GetText(buttonName, position);
        
        // Key already defined and not myself?
        bool keyRepeated = usedKeys.Contains(keyName) && keyName != oldKey;

        // Per comprovar despres quina tecla te assignada
        bool existsRollAlt = cInput.IsKeyDefined(InputButton.AltName(InputButton.ROLL));

        // I'm assigning down or roll?
        bool downOrRoll = buttonName == InputButton.DOWN || buttonName == InputButton.ROLL || buttonName == InputButton.AltName(InputButton.ROLL);

        bool ignoreDownOrRoll = false;

        if(keyRepeated && downOrRoll)
        {
            if(buttonName == InputButton.DOWN)
            {
                ignoreDownOrRoll = (keyName == cInput.GetText(InputButton.ROLL, position) || (existsRollAlt && keyName == cInput.GetText(InputButton.AltName(InputButton.ROLL), position)));
            }
            else if (buttonName == InputButton.ROLL || buttonName == InputButton.AltName(InputButton.ROLL))
            {
                ignoreDownOrRoll = (keyName == cInput.GetText(InputButton.DOWN, position));
            }
        }

        if (keyRepeated && !ignoreDownOrRoll)
        {
            pressAnyKey.SetActive(false);
            pauseAndRestartMustBe.SetActive(true);

            if (position == 1)
            {
                cInput.ChangeKey(buttonName, oldKey, cInput.GetText(buttonName, 2));
            }
            else
            {
                cInput.ChangeKey(buttonName, cInput.GetText(buttonName, 1), oldKey);
            }

            yield return null;
            while (!cInput.anyKeyDown)
            {
                yield return null;
            }

            yield return new WaitForSeconds(0.75f);

            pauseAndRestartMustBe.SetActive(false);

            // Tornem a sel�leccionar el boto que estavem configurant, no avancem
            nextButton = textToChange.gameObject;
        }
        else
        {
            usedKeys.Remove(oldKey);
            usedKeys.Add(cInput.GetText(buttonName, position));

            textToChange.text = cInput.GetText(buttonName, position);
            pressAnyKey.SetActive(false);
        }

        yield return new WaitForSeconds(0.25f);

        eventSystem.enabled = true;
        eventSystem.SetSelectedGameObject(nextButton);
    }

    /// <summary>
    /// Struct per a tenir organitzada tota la info dels botons de tabs
    /// </summary>
    [System.Serializable]
    public struct TabButtonInfo
    {
        /// <summary>
        /// El boto que ho representa
        /// </summary>
        public Button button;

        /// <summary>
        /// El subratllat que marca que esta sel.leccionat
        /// </summary>
        public Image underline;

        /// <summary>
        /// El text del boto
        /// </summary>
        public Text text;

        /// <summary>
        /// El primer Control a configurar d'aquesta tab
        /// </summary>
        public Button firstInsideButton;

        /// <summary>
        /// El darrer Control a configurar d'aquesta tab
        /// </summary>
        public Button lastInsideButton;
    }
}
