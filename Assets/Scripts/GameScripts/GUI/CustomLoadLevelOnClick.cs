﻿using UnityEngine;

// [AddComponentMenu("NGUI/Examples/Load Level On Click")]
public class CustomLoadLevelOnClick : MonoBehaviour
{
	public string levelName;
	
	void OnClick ()
	{
		if (!string.IsNullOrEmpty(levelName))
		{
			GameManager.Instance.LoadLevel(levelName);
		}
	}
}