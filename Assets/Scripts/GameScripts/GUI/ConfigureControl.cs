using UnityEngine;
using UnityEngine.UI;

public class ConfigureControl : MonoBehaviour
{
    /// <summary>
    /// El Configure Controls pare, al que li enviem les accions
    /// </summary>
    public ConfigureControls configureControls;

    /// <summary>
    /// Nom de la tecla, sense Alt
    /// </summary>
    public new string name;

    /// <summary>
    /// Text que representa la tecla
    /// </summary>
    public Text text;

    /// <summary>
    /// True si es del teclat, fals si es del pad
    /// </summary>
    public bool keyboard;

    /// <summary>
    /// Seguent game object que hem de sel.leccionar despres de setejar aquest
    /// </summary>
    public GameObject next;

    /// <summary>
    /// True si es la tecla alternativa, fals altrament
    /// </summary>
    public bool alt;

    /// <summary>
    /// Nom intern de la tecla, amb el Alt aplicat si es necessari
    /// </summary>
    private string internName;

    public void Start()
    {
        DebugUtils.AssertError(cInput.IsKeyDefined(name), "Key not defined: " + name, this.gameObject);

        if (alt)
        {
            internName = InputButton.AltName(name);
        }
        else
        {
            internName = name;
        }

        if(cInput.IsKeyDefined(internName))
        {
            text.text = cInput.GetText(internName, (keyboard) ? 1 : 2);
            configureControls.usedKeys.Add(text.text);
        }
        else
        {
            text.text = "NONE";
        }
    }

    public void ReadButton()
    {
        // Si no esta definida, nomes la definim si la volem crear. Si no ens es igual si no esta definida
        if (!cInput.IsKeyDefined(internName))
        {
            cInput.SetKey(internName, "");
        }

        configureControls.ReadButton(this, internName, keyboard, text, next);
    }

    public void ResetName()
    {
        if (cInput.IsKeyDefined(internName))
        {
            text.text = cInput.GetText(internName, (keyboard) ? 1 : 2);
        }
        else
        {
            text.text = "NONE";
        }

        Debug.Log("Passo per aqui: " + internName + " - " + text.text);
    }
}

