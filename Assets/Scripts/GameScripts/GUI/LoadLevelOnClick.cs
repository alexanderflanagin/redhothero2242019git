using UnityEngine;
using System.Collections;

public class LoadLevelOnClick : MonoBehaviour
{

    /// <summary>
    /// Carrega l'escena /level/ fent servir el GameManager
    /// </summary>
    /// <param name="level">El fitxer scene a carregar</param>
    public void LoadScene(string level)
    {
        GameManager.Instance.LoadLevel(level);
    }

    /// <summary>
    /// Loads worldMap if first level is complete, and first level if any level is complete
    /// </summary>
    public void LoadCorrectScene()
    {
        PlayerStageData pld;
        int firstStage = GameManager.Instance.gameData.stagesOrdered[0].stages[0];
        string firstStageName = GameManager.Instance.gameData.stagesData[0].sceneName;

        if (GameManager.Instance.playerData.levelsData.TryGetValue(firstStageName, out pld) && pld.finished)
        {
            GameManager.Instance.LoadLevel("WorldMap");
        }
        else
        {
            GameManager.Instance.LoadLevel(firstStageName);
        }
    }

    /// <summary>
    /// Si detecta que s'ha acabat l'escena /level/, carrega el world map. Sinó aquesta escena
    /// </summary>
    /// <param name="level">L'escena a testejar si ha estat acabada</param>
    public void LoadWorldMapIfFinished(string level)
    {
        PlayerStageData pld;
        if (GameManager.Instance.playerData.levelsData.TryGetValue(level, out pld) && pld.finished)
        {
            GameManager.Instance.LoadLevel("WorldMap");
        }
        else
        {
            GameManager.Instance.LoadLevel(level);
        }
    }
}
