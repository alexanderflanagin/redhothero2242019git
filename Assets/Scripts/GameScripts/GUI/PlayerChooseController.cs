using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerChooseController : MonoBehaviour {

	public static string DEFAULT_TEXT = "press jump button";
    
    private int _id;

	private string _suffix;

	private ChooseControls _chooseControls;

    public Text text;

	void Start() {

        if (text != null)
        {
            text.text = DEFAULT_TEXT;
        }

		_chooseControls = FindObjectOfType<ChooseControls>();
	}

	public void Activate( string suffix, int id ) {
		_suffix = suffix;
		_id = id;

        // TODO Aquest m�tode no fa res

        //GameObject scrollViewContainer = gameObject.transform.FindChild("SkinSelector").gameObject;

        //scrollViewContainer.SetActive( true );

        //foreach (Image sprite in scrollViewContainer.GetComponentsInChildren<Image>())
        //{
        //    if( _chooseControls.skinsSelected.ContainsKey( sprite.gameObject.name ) ) {
        //        sprite.color = Color.black;
        //    }
        //}

        //StartCoroutine( "CheckChangeSkin" );
	}

	private IEnumerator CheckChangeSkin() {
		CharacterInput input = gameObject.AddComponent<CharacterInput>();
		input.xAxis.name = "Horizontal" + _suffix;
		input.jump.name = "Jump" + _suffix;

		bool justSelected = false;
		bool canSelect = true;

		GameObject selected = null;

        //foreach(UIKeyNavigation kn in GetComponentsInChildren<UIKeyNavigation>()) {
        //    if( kn.startsSelected ) {
        //        selected = kn.gameObject;
        //    }
        //}

		while( true ) {
			if( canSelect && !justSelected && input.xAxis.val == 1 ) {
				// Debug.Log ( "Here" );
                //UICamera.selectedObject = GetComponentInChildren<UIScrollView>().gameObject;
                //GameObject next = selected.GetComponent<UIKeyNavigation>().onRight;

                //selected.GetComponent<UIKeyNavigation>().gameObject.SendMessage("OnKey", KeyCode.RightArrow);
                //selected = next;

                // TODO seleccionar el skin del player
				justSelected = true;
			}

			if( canSelect && !justSelected && input.xAxis.val == -1 ) {
                //UICamera.selectedObject = GetComponentInChildren<UIScrollView>().gameObject;
                //GameObject next = selected.GetComponent<UIKeyNavigation>().onLeft;

                //selected.GetComponent<UIKeyNavigation>().gameObject.SendMessage("OnKey", KeyCode.LeftArrow);
                //selected = next;
                
                // TODO seleccionar el skin del player
				justSelected = true;
			}

			if( input.xAxis.val == 0 ) {
				justSelected = false;
			}

			if( canSelect && input.jump.down ) {
				if( _chooseControls.skinsSelected.ContainsKey( selected.name ) ) {
					// Soroll de que no el pots agafar o algu
				} else {
					// Seleccionem tio
                    transform.Find("SkinSelector").gameObject.GetComponent<Image>().color = Color.red;
					// Debug.Log ( selected.name );
					_chooseControls.skinsSelected.Add( selected.name, true );

					_chooseControls.skinsNames[_id] = selected.name;

                    foreach (Image sprite in GameObject.FindObjectsOfType<Image>())
                    {
						if (  sprite.gameObject.name == selected.name && sprite.gameObject != selected ) {
							sprite.color = Color.black;
						}
					}

					canSelect = false;

					_chooseControls.nPlayers++;
				}
			}

			yield return null;
		}
	}
}
