using UnityEngine;
using UnityEngine.UI;
using System.Collections;

/// <summary>
/// Manager que gestiona el HUD del medidor d'energia
/// </summary>
public class HUDEnergyMeter : MonoBehaviour
{
    /// <summary>
    /// El gameobject de la imatge de la bateria que parpalleja quan et quedes sense energia
    /// </summary>
    public GameObject battery;

    /// <summary>
    /// Sound played when low battery beeps
    /// </summary>
    public string beepClip = "LowBattery";

    /// <summary>
    /// Número de cells que tenim. Té en compte la primera cell
    /// </summary>
    private int nBatteries;

    /// <summary>
    /// Coroutina que fa el parpalleig de la bateria. Per a poder-la parar
    /// </summary>
    private IEnumerator lowBattery;

    // NOVES
    /// <summary>
    /// La hombra de les bateries que estan buides
    /// </summary>
    public Image maskBatteriesShadow;

    /// <summary>
    /// Les bateries buides
    /// </summary>
    public Image maskBatteriesEmpty;

    /// <summary>
    /// L'halo vermell que envolta a les bateries carregades
    /// </summary>
    public Image maskBatteriesRedHalo;

    private float redHaloOffset = 0.011f;

    /// <summary>
    /// Les bateries carregades
    /// </summary>
    public Image maskBatteriesCharged;

    public Image[] maskFusesActive;

    /// <summary>
    /// La imatge que mostra les linies blanques del numero de fuses que tens
    /// </summary>
    public Image fuseMark;

    /// <summary>
    /// Les imatges per a cada un dels fuses que pots tenir
    /// </summary>
    public Sprite[] fuseMarksImages;

    /// <summary>
    /// Al habilitar, amaguem la bateria, registrem l'audio source al manager i registrem els events del energy low
    /// </summary>
    void OnEnable()
    {
        battery.SetActive(false);

        StartCoroutine(RegisterEvents());

        int fusesStartingAtZero = GameManager.Instance.playerData.GetFuses() - 1;
        DebugUtils.AssertError(fusesStartingAtZero >= 0 && fusesStartingAtZero < fuseMarksImages.Length, "El player te mes de 4 fuses o menys de 0", gameObject);

        fuseMark.sprite = fuseMarksImages[fusesStartingAtZero];
    }

    private IEnumerator RegisterEvents()
    {
        yield return null;
        yield return null;

        if (StandardStageManager.current)
        {
            if (StandardStageManager.current.playerControl)
            {
                StandardStageManager.current.playerControl.events.registerEvent(CharacterEvents.EventName.EnergyLowEnter, OnEnergyLowEnter);
                StandardStageManager.current.playerControl.events.registerEvent(CharacterEvents.EventName.EnergyLowExit, OnEnergyLowExit);
                StandardStageManager.current.playerControl.events.registerEvent(CharacterEvents.EventName.DieEnter, OnEnergyLowExit);
            }
        }
    }

    /// <summary>
    /// Al deshabilitar, des-registrem els esdeveniments del energy low
    /// </summary>
    void OnDisable()
    {
        if (StandardStageManager.current)
        {
            if (StandardStageManager.current.playerControl)
            {
                StandardStageManager.current.playerControl.events.unregisterEvent(CharacterEvents.EventName.EnergyLowEnter, OnEnergyLowEnter);
                StandardStageManager.current.playerControl.events.unregisterEvent(CharacterEvents.EventName.EnergyLowExit, OnEnergyLowExit);
                StandardStageManager.current.playerControl.events.unregisterEvent(CharacterEvents.EventName.DieEnter, OnEnergyLowExit);
            }
        }
    }

    /// <summary>
    /// Al entrar un energy low, comencem la coroutina del blink
    /// </summary>
    void OnEnergyLowEnter()
    {
        if(gameObject.activeInHierarchy)
        {
            lowBattery = Blink();
            StartCoroutine(lowBattery);
        }
    }

    /// <summary>
    /// Al sortir del energy low, treiem la coroutina (si est� engegada) i amaguem la pila
    /// </summary>
    void OnEnergyLowExit()
    {
        if (lowBattery != null)
        {
            StopCoroutine(lowBattery);
            battery.SetActive(false);
        }
        
    }

    /// <summary>
    /// Fa brillar la pila tres cops durant 0.1 segons i reprodueix el so
    /// </summary>
    /// <returns></returns>
    private IEnumerator Blink()
    {
        while (true)
        {
            AudioManager.Instance.PlayOneShoot(AudioClipsData.ClipsGroups.Player, beepClip);
            battery.SetActive(true);
            yield return new WaitForSeconds(0.2f);
            battery.SetActive(false);
            yield return new WaitForSeconds(0.2f);
            battery.SetActive(true);
            yield return new WaitForSeconds(0.2f);
            battery.SetActive(false);
            yield return new WaitForSeconds(0.7f);
        }
    }

    public void EnergyGet(int energy)
    {
        //StartCoroutine(EnergyGetBlink(energy));
    }

    //private IEnumerator EnergyGetBlink(int energy)
    //{
    //    this.bigCellAnimator.SetBool("Fill", true);
        
    //    for (int i = 0; i < this.nBatteries - 1; i++)
    //    {
    //        this.smallCellsAnimator[i].SetBool("Fill", true);
    //    }

    //    yield return new WaitForSeconds((float)energy / 100f);

    //    this.bigCellAnimator.SetBool("Fill", false);

    //    for (int i = 0; i < this.nBatteries - 1; i++)
    //    {
    //        this.smallCellsAnimator[i].SetBool("Fill", false);
    //    }
    //}

    /// <summary>
    /// Actualitzem la energia mostrada per el HUD, tenint en compte el número de cells i l'energia que tenim.
    /// Si el número de cells és diferent a l'actual, l'actualitzem
    /// </summary>
    /// <param name="nBatteries">Número de cells que tenim</param>
    /// <param name="energy">Energia que tenim</param>
    public void updateEnergy(int nBatteries, int energy, int nFuses)
    {
        if (nBatteries != this.nBatteries)
        {
            // Actualitzem les cells que es veuen
            this.nBatteries = nBatteries;
            UpdateBatteriesMask();
        }

        UpdateCurrentEnergyMask(energy);

        UpdateCurrentFusesMask(nFuses, energy);
    }

    /// <summary>
    /// Actualitza la mascara de les bateries segons el número de bateries del player
    /// </summary>
    private void UpdateBatteriesMask()
    {
        this.maskBatteriesShadow.fillAmount = (float)this.nBatteries / (float)EnergyManager.MAX_BATTERIES;
        this.maskBatteriesEmpty.fillAmount = (float)this.nBatteries / (float)EnergyManager.MAX_BATTERIES;
    }

    /// <summary>
    /// Actualitza la màscara de l'energia actual segons l'energia que tenim
    /// </summary>
    /// <param name="energy">L'energia actual del player</param>
    private void UpdateCurrentEnergyMask(int energy)
    {
        this.maskBatteriesRedHalo.fillAmount = ((float)energy / (float)EnergyManager.POINTS_PER_BATTERY) / (float)EnergyManager.MAX_BATTERIES + this.redHaloOffset;
        this.maskBatteriesCharged.fillAmount = ((float)energy / (float)EnergyManager.POINTS_PER_BATTERY) / (float)EnergyManager.MAX_BATTERIES;
    }

    /// <summary>
    /// Actualitza la representació dels fuses tenint en compte els que tenim i l'energia que tenim
    /// </summary>
    /// <param name="nFuses">Número de fuses que té el player</param>
    /// <param name="energy">Energía que tenim en aquest moment</param>
    private void UpdateCurrentFusesMask(int nFuses, int energy)
    {
        float fillAmount = 0;

        for (int i = 0; i < nFuses; i++)
        {
            // 1 Fuse: 0.11f
            // 2 Fuses: 0.248f
            // 3 Fuses: 0.385f
            // 4 Fuses: 0.522f
            if((energy / EnergyManager.POINTS_PER_BATTERY) >= EnergyManager.FIRST_FUSE_BATTERIES + EnergyManager.FUSE_BATTERIES * i)
            {
                switch(i + 1)
                {
                    case 1:
                        fillAmount = .11f;
                        break;
                    case 2:
                        fillAmount = .248f;
                        break;
                    case 3:
                        fillAmount = .385f;
                        break;
                    case 4:
                        fillAmount = .522f;
                        break;
                }
                
            }
        }

        for (int i = 0; i < this.maskFusesActive.Length; i++)
        {
            this.maskFusesActive[i].fillAmount = fillAmount;
        }
    }

    //private void changeCellsAnimatorSpeed(bool enable)
    //{
    //    this.bigCellAnimator.SetBool("LowEnergy", enable);

    //    for (int i = 0; i < this.nBatteries - 1; i++)
    //    {
    //        this.smallCellsAnimator[i].SetBool("LowEnergy", enable);
    //    }
    //}
}
