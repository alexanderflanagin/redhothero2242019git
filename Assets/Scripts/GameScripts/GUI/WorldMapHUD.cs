using UnityEngine;
using UnityEngine.UI;
using System.Collections;

/// <summary>
/// Gestiona el que es mostra en la GUI del WorldMap
/// </summary>
public class WorldMapHUD : MonoBehaviour
{
    /// <summary>
    /// Per a agafar el HUD des de qualsevol punt
    /// </summary>
    public static WorldMapHUD current;
    
    /// <summary>
    /// Imatge del enviro Toast Coast
    /// </summary>
    public Sprite toastCoastEnviroImage;

    /// <summary>
    /// Imatge del enviro Humble Jungle
    /// </summary>
    public Sprite humbleJungleEnviroImage;

    /// <summary>
    /// Imatge del enviro Prefab Lab
    /// </summary>
    public Sprite prefabLabEnviroImage;

    /// <summary>
    /// Image on es mostra la imatge del enviro actual
    /// </summary>
    public Image enviroImage;

    /// <summary>
    /// Imatge rodona del stage que tenim sel.leccionat
    /// </summary>
    public Image stageDotThumb;

    /// <summary>
    /// Seal of respect que es mostra si aconseguim tots els pickeables d'una pantalla
    /// </summary>
    public Image sealOfRespect;

    /// <summary>
    /// Array amb els 3 pickeables in-game
    /// </summary>
    public Image[] pickeables;

    /// <summary>
    /// Pickeable del speedRun
    /// </summary>
    public Image pickeableSpeedRun;

    /// <summary>
    /// Nom del stage que tenim sel.leccionat
    /// </summary>
    public Text stageTitle;

    /// <summary>
    /// Text amb el nostre millor temps
    /// </summary>
    public Text speedRunTime;

    /// <summary>
    /// Sprite cell que posarem si tenim una cell
    /// </summary>
    public Sprite cell;

    /// <summary>
    /// Sprite fuse que posarem si tenim un fuse
    /// </summary>
    public Sprite fusePart;

    /// <summary>
    /// Sprite de pickeable que no tenim per posar quan no tenim el pickeable en questio
    /// </summary>
    public Sprite pickeableEmpty;

    /// <summary>
    /// Assignem el current per a tenir-hi acces des de tot arreu
    /// </summary>
    private void Awake()
    {
        if(current != null)
        {
            Debug.LogError("WorldMapHUD already created. Destroying myself");
            Destroy(this);
        }

        current = this;
    }

    /// <summary>
    /// Cridem per a actualitzar la imatge de l'enviro
    /// </summary>
    private void Start()
    {
        UpdateEnviroImage();
    }

    /// <summary>
    /// Actualitzem la imatge del enviro segons a l'enviro on estiguem
    /// </summary>
    public void UpdateEnviroImage()
    {
        switch (GameManager.Instance.currentEnviro)
        {
            case EnviroData.EnviroType.Coast:
                enviroImage.sprite = toastCoastEnviroImage;
                break;
            case EnviroData.EnviroType.Jungle:
                enviroImage.sprite = humbleJungleEnviroImage;
                break;
            case EnviroData.EnviroType.Lab:
                enviroImage.sprite = prefabLabEnviroImage;
                break;
        }
    }

    /// <summary>
    /// Omplim la GUI amb la info que tenim de l'stage
    /// </summary>
    /// <param name="levelData">La informaci� pr�piament de l'stage</param>
    /// <param name="playerLevelData">La informaci� que t� el player de l'stage</param>
    /// <returns></returns>
    public IEnumerator FillGUI(LevelData levelData, PlayerStageData playerLevelData)
    {
        float showTime = 0.1f;

        // Nom
        this.stageTitle.text = levelData.name;

        // Speedrun
        this.speedRunTime.text = "--";

        bool sealOfRespect = true;

        // Reset de les imatges
        this.sealOfRespect.enabled = false;

        // Posem tot a fals i despr�s ja crearem
        for (int i = 0; i < StandardStageManager.PICKEABLES_PER_STAGE; i++)
        {
            pickeables[i].sprite = pickeableEmpty;
        }

        this.pickeableSpeedRun.sprite = pickeableEmpty;

        this.stageDotThumb.sprite = levelData.worldMapImage;
        // --- Fi reset d'imatges. Ens esperem el showTime i comencem a mostrar

        yield return new WaitForSeconds(showTime);

        if (playerLevelData.finished)
        {
            float timeToShow = playerLevelData.bestTime > 999.99f ? 999.99f : playerLevelData.bestTime;
            speedRunTime.text = timeToShow.ToString("0.00");
        }

        // Cells
        for (int i = 0; i < StandardStageManager.PICKEABLES_PER_STAGE; i++)
        {
            sealOfRespect = sealOfRespect && playerLevelData.pickeablesPicked[i];
            if (playerLevelData.pickeablesPicked[i])
            {
                if ((int)levelData.pickeablesOrder == i)
                {
                    // Es fuse part
                    pickeables[i].sprite = this.fusePart;
                }
                else
                {
                    // Es cell
                    pickeables[i].sprite = this.cell;
                }

                yield return new WaitForSeconds(showTime);
            }
        }

        // Speed run cell
        sealOfRespect = sealOfRespect && playerLevelData.speedrunDone;

        if (playerLevelData.speedrunDone)
        {
            if ((int)levelData.pickeablesOrder == StandardStageManager.PICKEABLES_PER_STAGE)
            {
                // Es fuse part
                pickeableSpeedRun.sprite = this.fusePart;
            }
            else
            {
                // Es cell
                pickeableSpeedRun.sprite = this.cell;
            }

            yield return new WaitForSeconds(showTime);
        }

        // Seal of respect
        if (sealOfRespect)
        {
            this.sealOfRespect.enabled = true;
        }
    }
}
