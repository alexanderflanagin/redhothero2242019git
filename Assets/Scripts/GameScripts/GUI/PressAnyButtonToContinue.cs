﻿using UnityEngine;
using System.Collections;

public class PressAnyButtonToContinue : MonoBehaviour {

	public string loadScene;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if( Input.anyKeyDown ) {
			GameManager.Instance.LoadLevel( loadScene );
		}
	}
}
