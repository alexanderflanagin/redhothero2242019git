﻿using UnityEngine;
using System.Collections;

public class PauseMenuElement : MonoBehaviour {

	public enum Button { restart, resume, exit }

	public Button button;

	void OnEnable() {

	}

	public void OnSubmit () {
		switch( button ) {
		case Button.resume:
			HUDManager.PauseExit();
			break;
		case Button.restart:
			HUDManager.PauseExit();
			GameManager.Instance.LoadLevel( "Reload" );
			break;
		case Button.exit:
			HUDManager.PauseExit();
			GameManager.Instance.LoadLevel( "MainMenu" );
			break;
		}
	}
}
