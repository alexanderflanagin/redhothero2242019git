using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StageCompleteHUD : MonoBehaviour
{
    /// <summary>
    /// La cell que es mostra al HUD quan has aconseguit una cell
    /// </summary>
    public GameObject cellHUD;

    /// <summary>
    /// La cell que es mostra al HUD si ja teniem aquella cell
    /// </summary>
    public GameObject cellHUDAlreadyPicked;

    /// <summary>
    /// La fuse part que es mostra al hud quan has aconseguit una fuse part
    /// </summary>
    public GameObject fusePartHUD;

    /// <summary>
    /// La fuse part que es mostra al hud si ja teniem aquella fuse part
    /// </summary>
    public GameObject fusePartHUDAlreadyPicked;

    /// <summary>
    /// Les cells buides al HUD
    /// </summary>
    public GameObject[] emptyCells;

    /// <summary>
    /// Text que conte el speedrun per defecte de la pantalla
    /// </summary>
    public Text speedrunText;

    /// <summary>
    /// Text que conte la millor marca de la panalla
    /// </summary>
    public Text bestRunText;

    /// <summary>
    /// GameObject que mostrar� el temps que hem trigat
    /// </summary>
    public GameObject time;

    /// <summary>
    /// GameObject que cont� el bot� de Next (per a fer focus quan acabem les estad�stiques)
    /// </summary>
    public GameObject next;

    /// <summary>
    /// Animator del Stage Complete
    /// </summary>
    public Animator animator;
    
    public string counterClip = "Counter";

    public string speedrunClip = "SpeedRun";

    public string respectClip = "Respect";

	public string coinsClip = "Coins";

    [Header("Cells and Fuses info")]
    public GameObject cellsUnitlNext;

    public GameObject fusesUntilNext;

    public Text cellsCount;

    public Text fusesCount;

    public GameObject newCell;

    public GameObject newFuse;

	void OnEnable()
    {
        animator.enabled = false;

        cellsUnitlNext.SetActive(false);
        fusesUntilNext.SetActive(false);
        cellsCount.gameObject.SetActive(false);
        fusesCount.gameObject.SetActive(false);
        newCell.SetActive(false);
        newFuse.SetActive(false);
    }

    /// <summary>
    /// Engeguem la coroutina de final d'stage
    /// </summary>
    public void StartEndStage()
    {
        StartCoroutine(EndStage());
    }

    /// <summary>
    /// Mostrarem, bloc per bloc, la informaci� de la pantalla. Coins, temps, etc.
    /// Permet, gaireb� sempre, passar r�pid les animacions prement el bot� de Jump
    /// </summary>
    /// <returns></returns>
    private IEnumerator EndStage()
    {
        // Recuperem informacio del nivell
        LevelData levelData;
        PlayerStageData playerLevelData;
        float speedRunTime;
        string loadedLevelname = SceneManager.GetActiveScene().name;

        if (GameManager.Instance.gameData.sceneNameBasedStagesList.TryGetValue(loadedLevelname, out levelData))
        {
            speedRunTime = levelData.speedrunTime;
        }
        else
        {
            levelData = LevelData.CreateDummy();
            speedRunTime = levelData.speedrunTime;
        }

        if (GameManager.Instance.playerData.levelsData.TryGetValue(loadedLevelname, out playerLevelData))
        {
            // En teoria sempre ha de passar per aquí, ja que això s'executa després del Save()
        }
        else
        {
            playerLevelData = new PlayerStageData();
        }
            
        speedrunText.text += speedRunTime;
        

        animator.enabled = true;
        animator.SetTrigger("Start");

        yield return StartCoroutine(WaitSecondsOrJumpButton(0.5f));

        // Apareixen les 3 primeres coins
        bool[] coinsPicked = StandardStageManager.current.pickeablesPicked;
        GameObject[] cells = new GameObject[emptyCells.Length];
        bool allCoins = true;

        // Preparem els gràfics, però de moment no en mostrem cap
        PreparePickeables(levelData, playerLevelData, speedRunTime, ref cells);

        // Mostrem els gràfics que hem aconseguit o teníem
        for (int i = 0; i < coinsPicked.Length; i++)
        {
            allCoins = allCoins && (coinsPicked[i] || playerLevelData.pickeablesPicked[i]);
            if (coinsPicked[i] || playerLevelData.pickeablesPicked[i])
            {
                emptyCells[i].GetComponent<Image>().enabled = false;
                cells[i].SetActive(true);
                AudioManager.Instance.PlayOneShoot(AudioClipsData.ClipsGroups.Enviro, coinsClip);
                if (i > 0)
                {
                    cells[i].transform.localRotation = cells[0].transform.localRotation;
                    cells[i].transform.Find("RedHotCellMesh").transform.localRotation = cells[0].transform.Find("RedHotCellMesh").transform.localRotation;
                }
            }

            yield return StartCoroutine(WaitSecondsOrJumpButton(0.5f));
        }

        // Mostrem el temps
        animator.Play("StageCompleteHUDTime", 2);
        yield return StartCoroutine(WaitSecondsOrJumpButton(0.5f));

        time.SetActive(true);

        Text timeText = time.GetComponent<Text>();

        bool jumpDownAutoValue = StandardStageManager.current.playerControl.input.jump.autoValue;
        StandardStageManager.current.playerControl.input.jump.autoValue = false;
        for (float f = 0; f < StandardStageManager.current.time; f += 0.11f)
        {
            timeText.text = f.ToString("0.00");
            AudioManager.Instance.PlayOneShoot(AudioClipsData.ClipsGroups.Enviro, counterClip);
            if (StandardStageManager.current.playerControl.input.jump.down)
            {
                break;
            }
            yield return null;
        }

        timeText.text = StandardStageManager.current.time.ToString("0.00");

        StandardStageManager.current.playerControl.input.jump.autoValue = jumpDownAutoValue;

        if (playerLevelData.bestTime != 0)
        {
            bestRunText.text += playerLevelData.bestTime.ToString("0.00");
        }
        else
        {
            bestRunText.text += StandardStageManager.current.time.ToString("0.00");
        }

        // Mirem si hem aconseguit l'speed run
        bool speedRun = StandardStageManager.current.time <= speedRunTime || playerLevelData.bestTime <= speedRunTime;

        if (speedRun)
        {
            if (StandardStageManager.current.time <= speedRunTime)
            {
                animator.Play("StageCompleteHUDSpeedRun", 1);
                AudioManager.Instance.PlayOneShoot(AudioClipsData.ClipsGroups.Enviro, speedrunClip);
            }
            yield return null;

            while (animator.GetCurrentAnimatorStateInfo(1).normalizedTime < 1)
            {
                yield return null;
            }

            emptyCells[3].GetComponent<Image>().enabled = false;
            AudioManager.Instance.PlayOneShoot(AudioClipsData.ClipsGroups.Enviro, coinsClip);
            if (emptyCells[0] || emptyCells[1] || emptyCells[2])
            {
                int i = 0;
                if (!emptyCells[0])
                {
                    if(emptyCells[1])
                    {
                        i = 1;
                    }
                    else
                    {
                        i = 2;
                    }
                }
                cells[3].transform.localRotation = cells[i].transform.localRotation;
                cells[3].transform.Find("RedHotCellMesh").transform.localRotation = cells[i].transform.Find("RedHotCellMesh").transform.localRotation;
            }

            cells[3].SetActive(true);

            yield return StartCoroutine(WaitSecondsOrJumpButton(0.5f));
        }

        if (speedRun && allCoins) // Mirem si hem de fer respect
        {
            AudioManager.Instance.PlayOneShoot(AudioClipsData.ClipsGroups.Enviro, respectClip);
            animator.Play("StageCompleteHUDRespect", 3);
            yield return StartCoroutine(WaitSecondsOrJumpButton(1f));

        }

        // Mostrem les cells i els fuses
        int actualTotalCells = GameManager.Instance.playerData.totalCells;
        int actualTotalFuseParts = GameManager.Instance.playerData.totalFuseParts;

        // Calculem quants son nous
        int getCells = 0, getFuses = 0;
        int fusePosition = (int)levelData.pickeablesOrder;
        List<bool> newPicked = new List<bool>();
        newPicked.AddRange(StandardStageManager.current.pickeablesPicked);
        newPicked.Add(StandardStageManager.current.time <= speedRunTime);
        for(int i = 0; i < newPicked.Count; i++)
        {
            if(i == fusePosition && newPicked[i])
            {
                getFuses++;
            }
            else if (newPicked[i])
            {
                getCells++;
            }
        }

        bool batteryExtra = (actualTotalCells - getCells) / EnergyManager.CELLS_PER_BATTERY < actualTotalCells / 6;
        bool fuseExtra = (actualTotalFuseParts - getFuses) / EnergyManager.FUSE_PARTS_PER_FUSE < actualTotalFuseParts / EnergyManager.FUSE_PARTS_PER_FUSE;

        if (batteryExtra)
        {
            newCell.SetActive(true);
        }
        else
        {
            // Restem per el Until. Si no Until, no cal restar
            cellsCount.text = (EnergyManager.CELLS_PER_BATTERY - (actualTotalCells % EnergyManager.CELLS_PER_BATTERY)) + "/" + EnergyManager.CELLS_PER_BATTERY;
            cellsUnitlNext.SetActive(true);
            cellsCount.gameObject.SetActive(true);
        }

        if (fuseExtra)
        {
            newFuse.SetActive(true);
        }
        else
        {
            // Restem per el Until. Si no Until, no cal restar
            fusesCount.text = (EnergyManager.FUSE_PARTS_PER_FUSE - (actualTotalFuseParts % EnergyManager.FUSE_PARTS_PER_FUSE)) + "/" + EnergyManager.FUSE_PARTS_PER_FUSE;
            fusesUntilNext.SetActive(true);
            fusesCount.gameObject.SetActive(true);
        }

        // Wait for input tu end
        while (!cInput.GetKeyDown("Jump"))
        {
            yield return null;
        }

        LoadNextlevel();
    }

    /// <summary>
    /// Prepara els game objects que es mostraran si s'han agafat els pickeables. Té en compte si ja els tenia, els ha tornat a agafar, etc.
    /// </summary>
    /// <param name="levelData">La info del nivell</param>
    /// <param name="playerLevelData">La info del nivell que té el player</param>
    /// <param name="speedRunTime">El temps de speed run calculat anteriorment</param>
    /// <param name="cells">Les cells que instància</param>
    private void PreparePickeables(LevelData levelData, PlayerStageData playerLevelData, float speedRunTime, ref GameObject[] cells)
    {
        GameObject toInstantiate;

        for (int i = 0; i < emptyCells.Length; i++)
        {
            if (i == (int)levelData.pickeablesOrder)
            {
                toInstantiate = this.fusePartHUD;
            }
            else
            {
                toInstantiate = this.cellHUD;
            }

            if (i < StandardStageManager.current.pickeablesPicked.Length)
            {
                // No speed run
                if (!StandardStageManager.current.pickeablesPicked[i] && playerLevelData.pickeablesPicked[i])
                {
                    // Ja la teníem i no l'hem tornat a agafar
                    if (i == (int)levelData.pickeablesOrder)
                    {
                        toInstantiate = this.fusePartHUDAlreadyPicked;
                    }
                    else
                    {
                        toInstantiate = this.cellHUDAlreadyPicked;
                    }
                }
            }
            else
            {
                // La del speed run
                if (StandardStageManager.current.time > speedRunTime && playerLevelData.bestTime <= speedRunTime)
                {
                    if (i == (int)levelData.pickeablesOrder)
                    {
                        toInstantiate = this.fusePartHUDAlreadyPicked;
                    }
                    else
                    {
                        toInstantiate = this.cellHUDAlreadyPicked;
                    }
                }
            }

            cells[i] = Instantiate(toInstantiate, emptyCells[i].transform.position + new Vector3(0, 0, -1), Quaternion.identity) as GameObject;
            cells[i].transform.SetParent(transform);

            cells[i].SetActive(false);
        }
    }

    private IEnumerator WaitSecondsOrJumpButton(float time)
    {
        float startTime = Time.time;

        do
        {
            if (StandardStageManager.current.playerControl.input.jump.down)
            {
                break;
            }
            yield return null;
        } while (startTime + time < Time.time);
    }


    /// <summary>
    /// Mètode per a que tingui el botó next i que es guardi amb el prefab
    /// </summary>
    public void LoadNextlevel()
    {
        StandardStageManager.current.LoadNextLevel();
    }
}
