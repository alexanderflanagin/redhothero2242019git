using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;

public class ChooseControls : MonoBehaviour
{
    public int nPlayers = 0;

    public GameObject pressStart;

    public PlayerChooseController[] players;

    public GameObject[] lights;

    private bool[] _suffixSelected;

    public Dictionary<string, bool> skinsSelected;

    public string[] skinsNames;

    // #if NO
    // Use this for initialization
    void Start()
    {
        if (GameManager.Instance.gameData.gameData.develControllersSuffix == null)
        {
            GameManager.Instance.gameData.gameData.develControllersSuffix = new string[2];
        }

        pressStart.SetActive(false);

        for (int i = 0; i < lights.Length; i++)
        {
            lights[i].SetActive(false);
        }

        StartCoroutine("Choose");
        // StartCoroutine ( "ChooseControl", 1 );
    }

    private IEnumerator Choose()
    {

        _suffixSelected = new bool[5] { false, false, false, false, false };

        skinsNames = new string[4] { "", "", "", "" };

        skinsSelected = new Dictionary<string, bool>();

        yield return StartCoroutine(ChooseControl(1));
        yield return StartCoroutine(ChooseControl(2));
        StartCoroutine(StartPlaying());
        yield return StartCoroutine(ChooseControl(3));
        yield return StartCoroutine(ChooseControl(4));

        /* GameManager.Instance.currentGameMode = GameManager.GameMode.MultiplayerCampaign;
        GameManager.Instance.LoadLevel( "PereDevelop0" ); */
    }

    private IEnumerator ChooseControl(int number)
    {
        bool selected = false;
        string name = "";
        string suffix = "";
        Event clickEvent;

        while (!selected)
        {
            if (Event.current != null)
            {
                clickEvent = Event.current;
                Debug.Log(clickEvent.isKey);
            }

            if (!_suffixSelected[0] && cInput.GetButtonDown(InputButton.JUMP + "1"))
            {
                // Player 1 = joystick 1
                name = Input.GetJoystickNames()[0];
                suffix = "1";
                selected = true;
                _suffixSelected[0] = true;
            }
            else if (!_suffixSelected[1] && cInput.GetButtonDown(InputButton.JUMP + "2"))
            {
                // Player 1 = joystick 2
                if (Input.GetJoystickNames().Length > 1)
                    name = Input.GetJoystickNames()[1];
                suffix = "2";
                selected = true;
                _suffixSelected[1] = true;
            }
            else if (!_suffixSelected[2] && cInput.GetButtonDown(InputButton.JUMP + "K"))
            {
                // Player 1 = keyboard
                name = "Keyboard";
                suffix = "K";
                selected = true;
                _suffixSelected[2] = true;
            }
            else if (!_suffixSelected[3] && cInput.GetButtonDown(InputButton.JUMP + "3"))
            {
                // Player 1 = joystick 3
                if (Input.GetJoystickNames().Length > 2)
                    name = Input.GetJoystickNames()[2];
                suffix = "3";
                selected = true;
                _suffixSelected[3] = true;
            }
            else if (!_suffixSelected[3] && cInput.GetButtonDown(InputButton.JUMP + "4"))
            {
                // Player 1 = joystick 3
                if (Input.GetJoystickNames().Length > 3)
                    name = Input.GetJoystickNames()[3];
                suffix = "4";
                selected = true;
                _suffixSelected[4] = true;
            }
            yield return null;
        }

        players[number-1].text.text = name;
        lights[number - 1].SetActive(true);
        GameManager.Instance.gameData.gameData.develControllersSuffix[number - 1] = suffix;

        players[number - 1].Activate(suffix, number - 1);

        nPlayers++;
    }

    private IEnumerator StartPlaying()
    {
        pressStart.SetActive(true);
        // bool active = true;
        while (!cInput.GetButton(InputButton.PAUSE))
        {
            // pressStart.SetActive( active );
            // active = !active;
            yield return null;
        }

        //GameManager.Instance.SetGameMode(GameManager.GameMode.MultiplayerFight);
        //GameManager.Instance.nPlayers = nPlayers;

        SkinManager.Skins[] skins = new SkinManager.Skins[nPlayers];

        for (int i = 0; i < nPlayers; i++)
        {
            skins[i] = (SkinManager.Skins)i;
        }

        //GameManager.Instance.skins = skins;

        GameManager.Instance.LoadLevel("SelectMultiplayerStage");
    }
    // #endif

}
