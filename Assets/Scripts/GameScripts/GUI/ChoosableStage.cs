using UnityEngine;
using System.Collections;

public class ChoosableStage : MonoBehaviour {

    public string levelName;

    public void OnClick()
    {
        FindObjectOfType<ChooseStage>().LoadStage(levelName);
    }
}
