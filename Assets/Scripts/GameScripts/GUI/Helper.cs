using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Helper : MonoBehaviour
{
    /// <summary>
    /// Imatge que es mostrar� en la GUI al tocar el helper. Si es defineix tamb� una animaci�, aquesta no es mostra
    /// </summary>
    public Sprite image;

    /// <summary>
    /// Animacio que es mostrara en la GUI al tocar el helper
    /// </summary>
    public new GameObject animation;

    /// <summary>
    /// Canvas que genera el helper. Si ja n'hi ha un en pantalla, no en crea un de nou
    /// </summary>
    public Canvas myCanvas;

    /// <summary>
    /// Offset de l'element gr�fic que surt en relaci� al centre del collider
    /// </summary>
    public Vector3 offset = Vector3.zero;

    /// <summary>
    /// Esdeveniments del player que amaguen el helper
    /// </summary>
    public CharacterEvents.EventName[] shutDownEvents;

    /// <summary>
    /// Si s'ha de tornar a mostrar el helper despr�s d'haver desaparegut
    /// </summary>
    public bool shouldRelaunch;

    /// <summary>
    /// GameObject que generem al tocar el trigger
    /// </summary>
    private GameObject generatedGameObject;

    /// <summary>
    /// Control del player (que recollim quan ens toca)
    /// </summary>
    private CharacterControl characterControl;

    /// <summary>
    /// Si ja hem estat llen�ats un cop
    /// </summary>
    private bool launched;

    void OnEnable()
    {
        HUDManager.PauseEnter += OnPauseEnter;
        HUDManager.PauseExit += OnPauseExit;
    }

    void Start()
    {
        GenerateCanvas();

        this.launched = false;
    }

    void OnDisable()
    {
        HUDManager.PauseEnter += OnPauseEnter;
        HUDManager.PauseExit += OnPauseExit;
    }

    /// <summary>
    /// Al posar pausa amaguem el canvas
    /// </summary>
    public void OnPauseEnter()
    {
        if(myCanvas != null)
        {
            myCanvas.gameObject.SetActive(false);
        }
    }

    /// <summary>
    /// Al treure pausa mostrem el canvas de nou
    /// </summary>
    public void OnPauseExit()
    {
        if (myCanvas != null)
        {
            myCanvas.gameObject.SetActive(true);
        }
    }

    void GenerateCanvas()
    {
        GameObject[] allCanvas = GameObject.FindGameObjectsWithTag(Tags.GUIHelperCanvas);

        if (allCanvas.Length == 0)
        {
            this.myCanvas = Instantiate(this.myCanvas) as Canvas;

            // Afegim una c�mera de GUI Helper a l'escena (que no hauria d'existir)
            if (Camera.main.transform.Find("IngameGUICamera"))
            {
                Debug.LogWarning("Ja existia una c�mera per a la GUI de helpers per� no el canvas");
            }
            else
            {
                GameObject ingameGUICamera = new GameObject("IngameGUICamera");
                Camera newCamera = ingameGUICamera.AddComponent<Camera>();

                newCamera.clearFlags = CameraClearFlags.Nothing;
                newCamera.cullingMask = 1 << Layers.IngameGUI;
                newCamera.orthographic = false;
                newCamera.fieldOfView = Camera.main.fieldOfView;
                newCamera.nearClipPlane = 1;
                newCamera.farClipPlane = 50;
                newCamera.depth = 1;
                newCamera.renderingPath = RenderingPath.Forward;

                ingameGUICamera.transform.SetParent(Camera.main.transform);
                ingameGUICamera.transform.localPosition = Vector3.zero;
                ingameGUICamera.transform.localRotation = Quaternion.identity;
            }
            
        }
        else
        {
            this.myCanvas = allCanvas[0].GetComponent<Canvas>();
        }

        if (allCanvas.Length > 1)
        {
            Debug.LogWarning("Hi ha " + allCanvas.Length + " Canvas per als helpers");
        }
        

        // this.myCanvas.worldCamera = HUDManager.current.GUICamera;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if ((!this.launched || (this.shouldRelaunch && !this.generatedGameObject)) && other.CompareTag(Tags.Player))
        {
            this.launched = true;

            this.characterControl = other.GetComponent<CharacterControl>();

            if (this.characterControl)
            {
                foreach (CharacterEvents.EventName charEvent in this.shutDownEvents)
                {
                    this.characterControl.events.registerEvent(charEvent, ShutDown);
                }
            }

            if (this.image != null)
            {
                this.generatedGameObject = new GameObject("Helper");
                this.generatedGameObject.layer = Layers.IngameGUI;
                this.generatedGameObject.transform.SetParent(myCanvas.transform);
                this.generatedGameObject.transform.localScale = new Vector3(0.01f, 0.01f, 0.01f);
                this.generatedGameObject.transform.position = this.transform.position + this.offset;

                this.generatedGameObject.AddComponent<CanvasRenderer>();

                Image imageTmp = this.generatedGameObject.AddComponent<Image>();

                imageTmp.sprite = this.image;
            }

            if (this.animation != null)
            {
                this.generatedGameObject = Instantiate(this.animation);

                this.generatedGameObject.transform.SetParent(this.myCanvas.transform);
                this.generatedGameObject.layer = Layers.IngameGUI;
                this.generatedGameObject.transform.position = this.transform.position + this.offset;
            }
        }
    }

    public void ShutDown()
    {
        foreach (CharacterEvents.EventName charEvent in this.shutDownEvents)
        {
            this.characterControl.events.unregisterEvent(charEvent, ShutDown);
        }

        Destroy(this.generatedGameObject);
    }

    void OnDrawGizmos()
    {
        Gizmos.DrawLine(this.transform.position, this.transform.position + this.offset);
        Gizmos.DrawSphere(this.transform.position + this.offset, 0.5f);
    }
}
