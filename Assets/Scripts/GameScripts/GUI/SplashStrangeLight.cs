using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SplashStrangeLight : MonoBehaviour
{
    public Image splashImage;
    
    void Start()
    {
        Cursor.visible = false;
        
        //Invoke("GotoMainMenu", 5);
        StartCoroutine(FadeInOut());
    }

    private IEnumerator FadeInOut()
    {
        Color color = splashImage.color;
        color.a = 0;

        splashImage.color = color;

        color.a = 1;

        while (splashImage.color.a < 0.99f)
        {

            splashImage.color = Color.Lerp(splashImage.color, color, Time.deltaTime * 1.4f);
            // splashImage.color = color;
            yield return null;
        }

        yield return new WaitForSeconds(0.6f);

        color.a = 0;

        while (splashImage.color.a > 0.01f)
        {
            splashImage.color = Color.Lerp(splashImage.color, color, Time.deltaTime * 1.4f);
            yield return null;
        }
        
        GameManager.Instance.LoadLevel("MainMenu");
    }
}
