using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using System.Collections;

/// <summary>
/// M�nager que s'encarrega de les funcionalitats del men� de pausa
/// </summary>

[RequireComponent (typeof (AudioSource) ) ]
public class HUDPauseManager : MonoBehaviour
{
    public HUDLeafletManager hudLeafletManager;

    public GameObject hudLeafletManagerGameObject;
   
	/// <summary>
    /// Al habilitar el GameObject, posem com a actiu el bot� Resume
    /// </summary>
    void OnEnable()
    {
		StartCoroutine(SelectFirst());

        //if (StageManager.current.playerData.totalLeaflets == 0)
        //{
        //    this.transform.Find("Abilities").gameObject.GetComponent<Button>().interactable = false;
        //}
        //else
        //{
        //    this.transform.Find("Abilities").gameObject.GetComponent<Button>().interactable = true;
        //}
    }
	

    /// <summary>
    /// Sel�lecciona i remarca el Resume sempre que es fa pausa
    /// </summary>
    /// <returns></returns>
    private IEnumerator SelectFirst()
    {
        yield return null;
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(this.transform.Find("Resume").gameObject);
    }

    /// <summary>
    /// Funcionalitat del bot� Resume
    /// </summary>
    public void Resume()
    {
        HUDManager.PauseExit();
    }

    /// <summary>
    /// Funcionalitat del bot� Restart
    /// </summary>
    public void Restart()
    {
        HUDManager.PauseExit();
        GameManager.Instance.checkPointProperties = null;
        GameManager.Instance.LoadLevel(SceneManager.GetActiveScene().name);
    }

    /// <summary>
    /// Funcionalitat del bot� Exit
    /// </summary>
    public void Exit()
    {
        HUDManager.PauseExit();
        GameManager.Instance.checkPointProperties = null;
        GameManager.Instance.LoadLevel("MainMenu");
    }

    public void ToWorldMap()
    {
        HUDManager.PauseExit();
        GameManager.Instance.checkPointProperties = null;
        GameManager.Instance.LoadLevel("WorldMap");
    }
}
