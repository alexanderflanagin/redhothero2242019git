using UnityEngine;
using System.Collections;

public class EndDemo : MonoBehaviour {

	// Use this for initialization
	void Start () {
        Invoke("ReloadMenu", 5);
	}

    void ReloadMenu()
    {
        GameManager.Instance.LoadLevel("MainMenu");
    }
}
