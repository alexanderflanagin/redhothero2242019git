using UnityEngine;
using System.Collections;

namespace UnityEngine.EventSystems
{
    public class CustomInputModule : BaseInputModule
    {
        private float lastActionTime;

        public float timeBetweenActions;

        public float repeatTime;

        private int lastHorizontalEvent = 0; // 0 = button, 1 = up, -1 = down

        private int lastVerticalEvent = 0; // 0 = none, 1 = up, -1 = down

        public override bool ShouldActivateModule()
        {
            if (!base.ShouldActivateModule())
            {
                return false;
            }

            bool shouldActivate = cInput.GetButtonDown(InputButton.JUMP);
            shouldActivate = cInput.GetButtonDown(InputButton.MENU_ENTER);
            shouldActivate |= cInput.GetButtonDown(InputButton.PUNCH);
            shouldActivate = cInput.GetButtonDown(InputButton.MENU_BACK);
            shouldActivate |= !Mathf.Approximately(cInput.GetAxis(InputButton.HORIZONTAL_AXIS), 0.0f);
            shouldActivate |= !Mathf.Approximately(cInput.GetAxis(InputButton.MENU_HORIZONTAL_AXIS), 0.0f);
            shouldActivate |= !Mathf.Approximately(cInput.GetAxis(InputButton.VERTICAL_AXIS), 0.0f);
            shouldActivate |= !Mathf.Approximately(cInput.GetAxis(InputButton.MENU_VERTICAL_AXIS), 0.0f);

            return shouldActivate;
        }

        public override void Process()
        {
            if(eventSystem.currentSelectedGameObject == null)
            {
                // Si no tenim cap objecte sel.leccionat, sel.leccionem el primer
                eventSystem.SetSelectedGameObject(eventSystem.firstSelectedGameObject);
            }
            else if (!Mathf.Approximately(cInput.GetAxisRaw(InputButton.VERTICAL_AXIS), 0f) || !Mathf.Approximately(cInput.GetAxisRaw(InputButton.MENU_VERTICAL_AXIS), 0f))
            {
                float verticalSign = Mathf.Approximately(cInput.GetAxisRaw(InputButton.VERTICAL_AXIS), 0f) ? Mathf.Sign(cInput.GetAxisRaw(InputButton.MENU_VERTICAL_AXIS)) : Mathf.Sign(cInput.GetAxisRaw(InputButton.VERTICAL_AXIS));
                if (verticalSign == this.lastVerticalEvent && Time.unscaledTime > this.lastActionTime + this.repeatTime)
                {
                    ExecuteEvents.Execute(eventSystem.currentSelectedGameObject, GetAxisEventData(0, verticalSign, 0.6f), ExecuteEvents.moveHandler);

                    this.lastVerticalEvent = (int)verticalSign;
                    this.lastActionTime = Time.unscaledTime;
                }
                else if (Time.unscaledTime > this.lastActionTime + this.timeBetweenActions)
                {
                    ExecuteEvents.Execute(eventSystem.currentSelectedGameObject, GetAxisEventData(0, verticalSign, 0.6f), ExecuteEvents.moveHandler);
                    this.lastVerticalEvent = (int)verticalSign;
                    this.lastActionTime = Time.unscaledTime;
                }

                this.lastVerticalEvent = 0;
            }
            else if (!Mathf.Approximately(cInput.GetAxisRaw(InputButton.HORIZONTAL_AXIS), 0f) || !Mathf.Approximately(cInput.GetAxisRaw(InputButton.MENU_HORIZONTAL_AXIS), 0f))
            {
                float horizonatlSign = Mathf.Approximately(cInput.GetAxisRaw(InputButton.HORIZONTAL_AXIS), 0f) ? Mathf.Sign(cInput.GetAxisRaw(InputButton.MENU_HORIZONTAL_AXIS)) : Mathf.Sign(cInput.GetAxisRaw(InputButton.HORIZONTAL_AXIS));
                if (horizonatlSign == this.lastHorizontalEvent && Time.unscaledTime > this.lastActionTime + this.repeatTime)
                {
                    ExecuteEvents.Execute(eventSystem.currentSelectedGameObject, GetAxisEventData(horizonatlSign, 0, 0.6f), ExecuteEvents.moveHandler);
                    this.lastHorizontalEvent = (int)horizonatlSign;
                    this.lastActionTime = Time.unscaledTime;
                }
                else if (Time.unscaledTime > this.lastActionTime + this.timeBetweenActions)
                {
                    ExecuteEvents.Execute(eventSystem.currentSelectedGameObject, GetAxisEventData(horizonatlSign, 0, 0.6f), ExecuteEvents.moveHandler);
                    this.lastHorizontalEvent = (int)horizonatlSign;
                    this.lastActionTime = Time.unscaledTime;
                }

                this.lastHorizontalEvent = 0;
            }
            else
            {
                SendSubmitEventToSelectedObject();
            }
        }

        /// <summary>
        /// Process submit keys.
        /// </summary>
        private bool SendSubmitEventToSelectedObject()
        {
            if (eventSystem.currentSelectedGameObject == null)
                return false;

            var data = GetBaseEventData();
            if (cInput.GetButtonDown(InputButton.JUMP) || cInput.GetButtonDown(InputButton.MENU_ENTER))
            {
                ExecuteEvents.Execute(eventSystem.currentSelectedGameObject, data, ExecuteEvents.submitHandler);
                this.lastActionTime = Time.unscaledTime;
                this.lastHorizontalEvent = 0;
                this.lastVerticalEvent = 0;
            }

            if (cInput.GetButtonDown(InputButton.PUNCH) || cInput.GetButtonDown(InputButton.MENU_BACK))
            {
                ExecuteEvents.Execute(eventSystem.currentSelectedGameObject, data, ExecuteEvents.cancelHandler);
                this.lastActionTime = Time.unscaledTime;
                this.lastHorizontalEvent = 0;
                this.lastVerticalEvent = 0;
            }
            
            return data.used;
        }
    }
}
