using UnityEngine;
using System.Collections;
using System.IO;

public class EraseData : MonoBehaviour {

    public void Erase()
    {
        if (File.Exists(Application.persistentDataPath + "/redHotHero.srhh"))
        {
            File.Delete(Application.persistentDataPath + "/redHotHero.srhh");

            GameManager.Instance.LoadPlayerData();
        }
    }
}
