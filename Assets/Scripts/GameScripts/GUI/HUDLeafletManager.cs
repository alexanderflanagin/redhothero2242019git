using UnityEngine;
using System.Linq;
using System.Collections;

public class HUDLeafletManager : MonoBehaviour
{
	/// <summary>
	/// Array que guarda de totes les octavilles que es monstren
	/// </summary>
	private GameObject[] _elements;
	/// <summary>
	/// Inicialitzo la variable en 0 per dirli que comenci sempre per la primera octavilla.
	/// </summary>
	private int _activeElement = 0;

    //public int totalLeaflets = 0;

    /// <summary>
    /// Setter públic del active element. Per a poder obrir les "octavilles" amb una directament sel·leccionada.
    /// </summary>
    public int activeElement
    {
        set
        {
            if (_elements != null && value < _elements.Length && value > 0)
            {
                _elements[_activeElement].SetActive(false);
                _activeElement = value;
                _elements[_activeElement].SetActive(true);
            }
        }
    }

    void OnEnable()
    {
        HUDManager.PauseExit += OnPauseExit;

        //totalLeaflets = StageManager.current.playerData.totalLeaflets;
    }

    void OnDisable()
    {
        HUDManager.PauseExit -= OnPauseExit;
    }

	// Use this for initialization
	void Start () {
		//Defineixo la longitud de l'array
		_elements = new GameObject[this.transform.Find("Leaflets").childCount];

		int i = 0;
		//Recorro el gameObject per omplir l'array amb els elements que conté
        foreach (Transform t in this.transform.Find("Leaflets"))
        {
			_elements[i] = t.gameObject;
			if(i == _activeElement) 
			{
				t.gameObject.SetActive(true);
			}
			else
			{
				t.gameObject.SetActive(false);
			}

			i++;
		}

        //_elements = _elements.OrderBy(x => x.name).ToArray();
	}

	// Update is called once per frame
	void Update () {
		// Tiro el carousel cap a la dreta
        if (cInput.GetButtonDown(InputButton.RIGHT))
        {
            //Desactivo l'element actual i incremento la variable per mostrar l'element anterior
            _elements[_activeElement].SetActive(false);
            // _activeElement = (_activeElement + 1) % StageManager.current.playerData.totalLeaflets;
            _elements[_activeElement].SetActive(true);
        }

        // Tiro el carousel cap a l'esquerre
        if (cInput.GetButtonDown(InputButton.LEFT))
        {
            //Debug.Log("Li dono al left. Tinc: " + StageManager.current.playerData.totalLeaflets + " leaflets i estic al: " + _activeElement + " abans de canviar");
            //Desactivo l'element actual i decremento la variable per mostrar l'element anterior
            _elements[_activeElement].SetActive(false);

            if (_activeElement == 0)
            {
                // _activeElement = StageManager.current.playerData.totalLeaflets;
            }

            _activeElement -= 1;

            //Debug.Log("El nou active element és: " + _activeElement);

            _elements[_activeElement].SetActive(true);
        }
	}

    void OnPauseExit()
    {
        if (this.gameObject.activeInHierarchy)
        {
            this.gameObject.SetActive(false);
        }
    }
}
	
