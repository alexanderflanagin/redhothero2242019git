using UnityEngine;
using System.Collections;

public class ScaleTexture : MonoBehaviour {

	// Use this for initialization
	void Start () {
		transform.localScale = new Vector3( GetComponent<GUITexture>().pixelInset.width / 1920, GetComponent<GUITexture>().pixelInset.height / 1080, 1 );
		GetComponent<GUITexture>().pixelInset = new Rect(0, 0, 0, 0);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
