﻿using UnityEngine;
using System;
using System.Collections;


[Serializable]
public class FightData {

	public float fightTime;
	
	public int[] wins;

	public int[] deaths;

	public int lastWinner;

	public int[] lastDeaths;
}
