using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Runtime.Serialization.Formatters.Binary;

[Serializable]
public class GameData
{
    /// <summary>
    /// Hem de carregar l'arxiu en disc al carregar el joc?
    /// </summary>
    public bool loadPlayerDataFile;

    public bool globalCheatsEnabled;

    public bool controllerCheatsEnabled;

    /*
	 * Hi ha compte enrera?
	 */
    public bool useCountDown;
	
	/*
	 * La lava es mou?
	 */
	public bool lavaMove;
	
	/*
	 * S'ha de carregar la mateixa escena quan s'acaba la fase o es va al menu?
	 */
	public bool reloadSceneAfterFinishLine;

    /*
	 * Es pot resetejar la pantalla? [ Input.GetButtonDown(InputButton.RESTART); ]
	 */
    public bool canReset;
	
	/*
	 * Resetejar la posicio del MainChar per a que la X i la Z siguin 0
	 */
	public bool resetMainChar;

	/*
	 * Recalcula tots els limits de camera de les pantalles.
	 */
	public bool forceCameraBoundariesReload;

	/*
	 * Devel time default to test
	 */
    //public GameManager.GameMode develGameMode;

	/*
	 * Suffix dels controllers
	 */
	public string[] develControllersSuffix;

	/*
	 * Numero de players
	 */
    //public int nPlayers;

	/*
	 * Temps que dura una partida de fight
	 */
    //public float fightTime;
}

[Serializable]
public class EnviroData
{
    /// <summary>
    /// Numero d'stages per enviro
    /// </summary>
    public static int STAGES_PER_ENVIRO = 10;

    /// <summary>
    /// Numero d'enviros que tenim
    /// </summary>
    public static int ENVIROS = 3;

    /// <summary>
    /// Definicio dels enviros
    /// </summary>
    public enum EnviroType
    {
        Coast = 0,
        Jungle = 1,
        Lab = 2
    }

    public int[] stages;
}
