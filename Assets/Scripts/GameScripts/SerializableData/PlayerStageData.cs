using UnityEngine;
using System;

/// <summary>
/// Per a guardar informacio de pantalles desbloquejades/completades
/// </summary>
[Serializable]
public class PlayerStageData
{
    /// <summary>
    /// Pickeables que hem agafat en aquesta pantalla
    /// </summary>
    public bool[] pickeablesPicked;

    /// <summary>
    /// Si ens l'hem passat
    /// </summary>
    public bool finished;

    /// <summary>
    /// El millor temps que hem fet
    /// </summary>
    public float bestTime;

    /// <summary>
    /// Si ja tenim l'speedrun (per a consultes rapides)
    /// </summary>
    public bool speedrunDone;

    /// <summary>
    /// Si l'hem desbloquejat
    /// </summary>
    public bool unlocked;

    /// <summary>
    /// Crea un /PlayerStageData/ amb el minim d'informacio
    /// </summary>
    public PlayerStageData()
    {
        this.pickeablesPicked = new bool[StandardStageManager.PICKEABLES_PER_STAGE];
        this.bestTime = float.MaxValue;
    }
}
