using UnityEngine;
using System.Collections;

[System.Serializable]
public class KimbStageData {

    public string sceneName;

    public int nEnemyTypes;

    public EnemiesManager.Enemies[] enemiesTypes;

    public int[] enemiesPoints;

    public int killPlayerPoints;

    public float waveFrequency;

    public KimbEnemyWave[] waves;
}

[System.Serializable]
public struct KimbEnemyElement
{
    public bool active;

    public EnemiesManager.Enemies type;
}

[System.Serializable]
public struct KimbEnemyWave {
    public KimbEnemyElement[] enemyWave; 
}
