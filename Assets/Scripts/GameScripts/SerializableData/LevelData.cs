using UnityEngine;
using UnityEngine.SceneManagement;
using System;
using System.Collections;
using System.Collections.Generic;

[Serializable]
public class LevelData {

	/// <summary>
    /// Identificador del nivell. De moment no el fem servir.
	/// </summary>
    [HideInInspector]
    public int ID;

    /// <summary>
    /// Stage enviro
    /// </summary>
    [HideInInspector]
    public int enviro;

    /// <summary>
    /// 
    /// </summary>
    [HideInInspector]
    public int inEnviroPos;

	/// <summary>
    /// Nom del .scene que carrega aquesta pantalla
	/// </summary>
	public string sceneName;

	/// <summary>
    /// Nom de la pantalla (per si cal)
	/// </summary>
	public string name;

    /// <summary>
    /// Quanta energia es necessita per a obrir aquesta pantalla. Entre 1 i 13
    /// </summary>
    public int cellsNeeded;

    public float speedrunTime;

    /// <summary>
    /// Mínima energia que apliquem al player a l'entrar aquest nivell
    /// </summary>
    public int forcedMinimumBatteries;

    /// <summary>
    /// Energia que forcem al player a tenir en aquest nivell
    /// </summary>
    public int forcedBatteries;

    public string devNextStageScene;

    /// <summary>
    /// Array amb la mida de la càmera
    /// </summary>
    //public float[] cameraBoundaries;

    public StandardStageManager.PickeablesOrder pickeablesOrder;

    public Sprite worldMapImage;

    public int theme;

    /// <summary>
    /// Funcio dummy que faig servir per quan intento agafar un level data del diccionari del Game Manager i no existeix
    /// </summary>
    /// <returns></returns>
    public static LevelData CreateDummy()
    {
        LevelData levelData = new LevelData();

        levelData.sceneName = SceneManager.GetActiveScene().name;

        levelData.name = "Dummy data. Will reload";

        levelData.speedrunTime = 14f;

        return levelData;
    }
}
