﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class FixScaleTiling : MonoBehaviour {

	// Use this for initialization
	void Start () {

		GetComponent<MeshRenderer> ().material.mainTextureScale = new Vector2 ( transform.localScale.x, 1 );

	}
}
