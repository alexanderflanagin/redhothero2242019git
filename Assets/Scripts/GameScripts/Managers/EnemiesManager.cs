using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

[Serializable]
public class EnemiesManager : Singleton<EnemiesManager>
{
    public GameObject Squakatoo;
    public GameObject GenericEnemy;
    public GameObject Spike;
    public GameObject Splurgh;
    public GameObject S_Tv;
    public GameObject S_TvPacer;
    public GameObject S_TvCopter;
    public GameObject S_TvBiker;
    public GameObject S_TvBikerTutorial;
    public GameObject SpikeBallWalker;
    public GameObject SpikeBallPacer;
    public GameObject S_TvEmitter;
    public GameObject Turret;
    public GameObject TurretAimer;
    public GameObject S_TvPacer_BlueSubstanced;
    public GameObject S_TV_Reactor;
    public GameObject DocClock;
    public GameObject GlowSplurgh;
    public GameObject S_Tv_Reactor_BlueSubstanced;

    /// <summary>
    /// Enum amb tots els tipus d'enemics
    /// </summary>
    public enum Enemies
    {
        Squakatoo = 3,
        GenericEnemy = 12,
        Spike = 31,
        Splurgh = 33,
        GlowSplurgh = 34,
        S_Tv = 36,
        S_TvPacer = 37,
        S_TvCopter = 38,
        S_TvBiker = 39,
        S_TvBikerTutorial = 41,
        SpikeBallWalker = 40,
        S_TvEmitter = 43,
        Turret = 42,
        TurretAimer = 44,
        S_TvPacer_BlueSubstanced = 50,
        SpikeBallPacer = 51,
        S_TV_Reactor = 52,
        DocClock = 60,
        S_Tv_Reactor_BlueSubstanced = 65
    }

    public enum EnemyDisable
    {
        TimeDisable,
        TriggerDisable,
        NoDisable
    }

    /// <summary>
    /// Instància pública del EnemiesPrefabList que hi hagi en aquell moment en escena
    /// </summary>
    //public static EnemiesManager Instance;

    /// <summary>
    /// Llista pùblica d'enemics ordenada segons el enum Enemies
    /// </summary>
    public Dictionary<int, GameObject> enemies;

    /// <summary>
    /// Event que es dispara quan la llista està plena, per a que tothom pugui agafar els GameObjects necessaris llavors
    /// </summary>
    public event Action ListReady;

    public bool listReadyAlreadyCalled = false;

    void Awake()
    {
        DontDestroyOnLoad(this);
    }

    /// <summary>
    /// Ens hem d'esperar a l'Start perquè abans no ha inicialitzat les referencies dels GameObjects
    /// Al fer Start, crea la llista i crida a l'esdeveniment ListReady per a que tothom que s'hi hagi subscrit pugui agafar el seu GameObject inicialitzat
    /// </summary>
    void Start()
    {
        //instance = this;

        enemies = new Dictionary<int, GameObject>();

        int position;
        GameObject enemyObject;

        foreach (Enemies enemy in Enum.GetValues(typeof(Enemies)))
        {
            position = (int)enemy;

            if (this.GetType().GetField(Enum.GetName(typeof(Enemies), enemy)) != null)
            {
                enemyObject = (GameObject)this.GetType().GetField(Enum.GetName(typeof(Enemies), enemy)).GetValue(this);
            }
            else
            {
                enemyObject = null;
            }

            enemies.Add(position, enemyObject);
        }


        if (ListReady != null)
        {
            ListReady();
        }

        listReadyAlreadyCalled = true;
    }
}
