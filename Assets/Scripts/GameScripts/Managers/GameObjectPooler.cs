using System.Collections.Generic;

using UnityEngine;
using UnityEngine.SceneManagement;

public class GameObjectPooler : MonoBehaviour
{
    /// <summary>
    /// Per defecte, quants enemics tindrem
    /// </summary>
    public static int DEFAULT_ENEMIES = 10;

    /// <summary>
    /// Per defecte, quantes particules tindrem
    /// </summary>
    public static int DEFAULT_PARTICLES = 3;

    /// <summary>
    /// Per defecte, quants projectils de cada tipus tindrem
    /// </summary>
    public static int DEFAULT_PROJECTILES = 6;

    /// <summary>
    /// Per defecte, quantes densitites tindrem
    /// </summary>
    public static int DEFAULT_DENSITIES = 15;

    /// <summary>
    /// Default number of trails to create at start
    /// </summary>
    public static int DEFAULT_TRAILS = 5;

    /// <summary>
    /// Variable estàtica per a poder accedir ràpidament a la classe
    /// </summary>
    public static GameObjectPooler Instance;

    /// <summary>
    /// Diccionari d'enemics
    /// </summary>
    private Dictionary<EnemiesManager.Enemies, List<GameObject>> _enemiesPool;

    /// <summary>
    /// Diccionari de projectils
    /// </summary>
    private Dictionary<ProjectilesManager.Projectiles, List<GameObject>> _projectilesPool;

    /// <summary>
    /// Diccionari de red hot densities
    /// </summary>
    private List<RedHotDensity> redHotDensitiesPool;

    /// <summary>
    /// Power Gloved objects trail list
    /// </summary>
    private List<TrailRendererController> powerGlovedTrailPool;

    protected GameObject enemiesPoolParent;

    protected GameObject projectilesPoolParent;

    protected GameObject densitiesPoolParent;

    protected GameObject trailsPoolParent;

    public Transform DensitiesPoolParentTransform
    {
        get
        {
            return densitiesPoolParent.transform;
        }
    }


    /// <summary>
    /// Al fer awake, inicialitzem la variable estàtica i creem el diccionari
    /// </summary>
    void Awake()
    {
        if (GameObjectPooler.Instance != null)
        {
            Debug.LogWarning("Per alguna raó hi ha dos GameObjectPooler instanciats");
            Destroy(this);
        }
        else
        {
            GameObjectPooler.Instance = this;
            DontDestroyOnLoad(gameObject);
        }

        enemiesPoolParent = new GameObject("EnemiesPool");
        enemiesPoolParent.transform.SetParent(transform);

        projectilesPoolParent = new GameObject("ProjectilesPool");
        projectilesPoolParent.transform.SetParent(transform);

        densitiesPoolParent = new GameObject("DensitiesPool");
        densitiesPoolParent.transform.SetParent(transform);

        trailsPoolParent = new GameObject("TrailsPool");
        trailsPoolParent.transform.SetParent(transform);

        _enemiesPool = new Dictionary<EnemiesManager.Enemies, List<GameObject>>();

        _projectilesPool = new Dictionary<ProjectilesManager.Projectiles, List<GameObject>>();

        redHotDensitiesPool = new List<RedHotDensity>();

        powerGlovedTrailPool = new List<TrailRendererController>();
        CreatePowerGlovedTrails();

        SceneManager.sceneUnloaded += OnSceneEnds;
    }

    /// <summary>
    /// Al acabar l'escena, deshabilito elements que hagin pogut quedar actius. Miro sempre si existeixen perque sino es produeix un error al editor al fer play/pause
    /// </summary>
    /// <param name="scene"></param>
    protected void OnSceneEnds(Scene scene)
    {
        // Set all pooled enemies to false
        //foreach(EnemiesManager.Enemies enemyType in _enemiesPool.Keys)
        //{
        //    foreach (GameObject enemy in _enemiesPool[enemyType])
        //    {
        //        if (enemy && enemy.activeInHierarchy)
        //        {
        //            enemy.SetActive(false);
        //        }
        //    }
        //}

        //foreach(RedHotDensity density in redHotDensitiesPool)
        //{
        //    if(density && density.gameObject.activeInHierarchy)
        //    {
        //        density.Release();
        //    }
        //}

        foreach(TrailRendererController controller in powerGlovedTrailPool)
        {
            if(controller)
            {
                controller.Active = false;
                controller.gameObject.SetActive(false);
                controller.transform.SetParent(trailsPoolParent.transform);
            }
            else
            {
                Debug.Log("Controller a la llista del pool que no existeix: " + controller);
            }
        }
    }

    /// <summary>
    /// Recupera una llista de les red hot densities, o en crea una de nova si totes s'estan fent servir
    /// </summary>
    /// <returns>Una density que pertany a la llista de \redHotDensitiesPool\</returns>
    public RedHotDensity GetRedHotDensity()
    {
        if (redHotDensitiesPool.Count == 0)
        {
            // Crearem per defecte un pool de 10 densities
            for (int i = 0; i < DEFAULT_DENSITIES; i++)
            {
                GameObject tmpObj = Instantiate(GeneralPurposeObjectManager.Instance.RedHotDensity) as GameObject;
                RedHotDensity density = tmpObj.GetComponent<RedHotDensity>();
                tmpObj.transform.SetParent(densitiesPoolParent.transform);
                tmpObj.SetActive(false);
                redHotDensitiesPool.Add(density);
            }
        }

        foreach (RedHotDensity density in redHotDensitiesPool)
        {
            if (!density.gameObject.activeInHierarchy && !density.InUse)
            {
                density.Serve();
                return density;
            }
        }

        // Si no hem retornat abans, vol dir que estem fent servir totes les densities
        GameObject densityObj = Instantiate(GeneralPurposeObjectManager.Instance.RedHotDensity) as GameObject;
        RedHotDensity redHotDensity = densityObj.GetComponent<RedHotDensity>();
        densityObj.transform.SetParent(densitiesPoolParent.transform);
        redHotDensitiesPool.Add(redHotDensity);
        redHotDensity.Serve();

        return redHotDensity;
    }

    /// <summary>
    /// Afegeix enemics al diccionari.
    /// Només crearà nous enemics si no n'hi ha d'aquest tipus o el número d'enemics ja creats és inferior al doble dels que es demanen
    /// </summary>
    /// <param name="type">Tipus de l'enemic a crear</param>
    /// <param name="num">Número a afegir</param>
    public void AddEnemy(EnemiesManager.Enemies type, int num)
    {
        //Debug.Log("Anem a afegir enemics al pool general del tipus: " + Enum.GetName(typeof(EnemiesManager.Enemies), type) + " - número: " + num, this.gameObject);

        if (_enemiesPool.ContainsKey(type))
        {
            if (_enemiesPool[type].Count < num * 2)
            {
                //Debug.Log("La llista és massa petita. L'anem a incrementar. Mida de la llista: " + current.Count + " - num: " + num, this.gameObject);
                for (int i = 0; i < num; i++)
                {
                    GameObject tmp = Instantiate(EnemiesManager.Instance.enemies[(int)type]);
                    tmp.transform.SetParent(enemiesPoolParent.transform);
                    tmp.SetActive(false);
                    _enemiesPool[type].Add(tmp);
                }
            }
        }
        else
        {
            // Encara no tenim enemics d'aquest tipus
            List<GameObject> current = new List<GameObject>();

            //Debug.Log("Encara no teniem llista d'enemics d'aquest tipus. En creem una de nova i l'omplim amb tants enemics com ens sol·liciti el jugador", this.gameObject);

            for (int i = 0; i < num; i++)
            {
                GameObject tmp = Instantiate(EnemiesManager.Instance.enemies[(int)type]);
                tmp.transform.SetParent(enemiesPoolParent.transform);
                tmp.SetActive(false);
                current.Add(tmp);
            }

            _enemiesPool.Add(type, current);
        }
    }

    /// <summary>
    /// Ens retorna un enemic del tipus sel·leccionat. Si no en té de disponibles, en crea de nous. Si no existeix la llista d'aquest tipus, també la crea
    /// </summary>
    /// <param name="type">Tipus de l'enemic</param>
    /// <returns>Un enemic ja instanciat i no actiu</returns>
    public GameObject GetEnemy(EnemiesManager.Enemies type)
    {
        //Debug.Log("Demanem un enemic del tipus " + Enum.GetName(typeof(EnemiesManager.Enemies), type), this.gameObject);
        if (_enemiesPool.ContainsKey(type))
        {
            foreach (GameObject obj in _enemiesPool[type])
            {
                if (obj.activeInHierarchy == false)
                {
                    return obj;
                }
            }
        }
        else
        {
            //Debug.Log("No teniem enemics del tipus " + Enum.GetName(typeof(EnemiesManager.Enemies), type) + ". Creem una llista i l'afegim");
            AddEnemy(type, 1);
        }


        // No hem tingut prou enemics per el que sigui, avisem i en creem un més
        //Debug.Log("No hem tingut prous enemics i hem incrementat la llista");
        GameObject tmp = Instantiate(EnemiesManager.Instance.enemies[(int)type]) as GameObject;
        tmp.transform.SetParent(enemiesPoolParent.transform);
        tmp.SetActive(false);
        _enemiesPool[type].Add(tmp);

        return tmp;
    }

    /// <summary>
    /// Afegeix projectils al diccionari.
    /// Només crearà nous projectils si no n'hi ha d'aquest tipus o el número de projectils ja creats és inferior al doble dels que es demanen
    /// </summary>
    /// <param name="type">Tipus del projectil a crear</param>
    /// <param name="num">Número a afegir</param>
    public void AddProjectiles(ProjectilesManager.Projectiles type, int num)
    {
        //Debug.Log("Anem a afegir enemics al pool general del tipus: " + Enum.GetName(typeof(EnemiesManager.Enemies), type) + " - número: " + num, gameObject);

        if (_projectilesPool.ContainsKey(type))
        {
            List<GameObject> current = _projectilesPool[type];

            //Debug.Log("Ja tenim la llista feta. Incrementarem només si ens fes falta per mida. Mida de la llista: " + current.Count, gameObject);

            if (current.Count < num * 2)
            {
                //Debug.Log("La llista és massa petita. L'anem a incrementar. Mida de la llista: " + current.Count + " - num: " + num, gameObject);
                for (int i = 0; i < num; i++)
                {
                    AddAndGetProjectile(type);
                }
            }
        }
        else
        {
            // Encara no tenim enemics d'aquest tipus
            List<GameObject> current = new List<GameObject>();

            //Debug.Log("Encara no teniem llista d'enemics d'aquest tipus. En creem una de nova i l'omplim amb tants enemics com ens sol·liciti el jugador", this.gameObject);
            _projectilesPool.Add(type, current);

            for (int i = 0; i < num; i++)
            {
                AddAndGetProjectile(type);
            }
        }
    }

    /// <summary>
    /// Adds a new projectile of the type /type/ into the dictionary and return them
    /// </summary>
    /// <param name="type">Type of the projectile to be created</param>
    /// <returns>The projectile created</returns>
    private GameObject AddAndGetProjectile(ProjectilesManager.Projectiles type)
    {
        GameObject projectile = Instantiate(ProjectilesManager.Instance.projectiles[(int)type]) as GameObject;
        projectile.transform.SetParent(projectilesPoolParent.transform);
        projectile.SetActive(false);
        _projectilesPool[type].Add(projectile);

        ProjectilesManager.Instance.AddProjectileOnDictionary(projectile.transform, projectile.GetComponent<Projectile>());

        return projectile;
    }

    /// <summary>
    /// Ens retorna un enemic del tipus sel·leccionat. Si no en té de disponibles, en crea de nous. Si no existeix la llista d'aquest tipus, també la crea
    /// </summary>
    /// <param name="type">Tipus de l'enemic</param>
    /// <returns>Un enemic ja instanciat i no actiu</returns>
    public GameObject GetProjectile(ProjectilesManager.Projectiles type)
    {
        //Debug.Log("Demanem un enemic del tipus " + Enum.GetName(typeof(EnemiesManager.Enemies), type), this.gameObject);
        if (_projectilesPool.ContainsKey(type))
        {
            foreach (GameObject obj in _projectilesPool[type])
            {
                if (obj.activeInHierarchy == false)
                {
                    return ResetProjectile(obj);
                }
            }
        }
        else
        {
            //Debug.Log("No teniem enemics del tipus " + Enum.GetName(typeof(EnemiesManager.Enemies), type) + ". Creem una llista i l'afegim");
            List<GameObject> list = new List<GameObject>();

            _projectilesPool.Add(type, list);
        }

        // No hem tingut prou enemics per el que sigui, avisem i en creem un més
        //Debug.Log("No hem tingut prous enemics i hem incrementat la llista");
        return AddAndGetProjectile(type);
    }

    /// <summary>
    /// Reset projectile tray particles
    /// </summary>
    /// <param name="projectile">GameObject of the projectile</param>
    /// <returns></returns>
    public GameObject ResetProjectile(GameObject projectile)
    {
        Projectile projectileComponent = ProjectilesManager.Instance.GetFromProjectileDictionary(projectile.transform);

        if (projectileComponent && projectileComponent.trail)
        {
            projectileComponent.trail.Clear();
        }

        return projectile;
    }

    /// <summary>
    /// Generate and populate default PowerGloved trails list
    /// </summary>
    private void CreatePowerGlovedTrails()
    {
        powerGlovedTrailPool = new List<TrailRendererController>();

        for (int i = 0; i < DEFAULT_TRAILS; i++)
        {
            CreatePowerGlovedTrail();
        }
    }

    /// <summary>
    /// Gets first free power gloved trail, resetting them if necessary
    /// </summary>
    /// <returns></returns>
    public TrailRendererController GetPowerGlovedTrail()
    {
        for(int i = 0; i < powerGlovedTrailPool.Count; i++)
        {
            if(!powerGlovedTrailPool[i].gameObject.activeInHierarchy)
            {
                powerGlovedTrailPool[i].trail.Clear();
                return powerGlovedTrailPool[i];
            }
        }

        // If we are here it means that we don't have free power gloved trails
        TrailRendererController controller = CreatePowerGlovedTrail();
        controller.Active = true;
        controller.gameObject.SetActive(true);

        return controller;
    }

    /// <summary>
    /// Retorna un PowerGloved trail
    /// </summary>
    /// <param name="trail"></param>
    public void ReturnPowerGlovedTrail(Transform trail)
    {
        trail.SetParent(trailsPoolParent.transform);
    }

    /// <summary>
    /// Genera un PowerGloved trail
    /// </summary>
    /// <returns>Retorna el controller d'aquest</returns>
    private TrailRendererController CreatePowerGlovedTrail()
    {
        GameObject trailObject = Instantiate(GeneralPurposeObjectManager.Instance.PowerGlovedDefaultTrail) as GameObject;
        trailObject.transform.SetParent(trailsPoolParent.transform);
        TrailRendererController controller = trailObject.GetComponent<TrailRendererController>();
        controller.Pooler = this;
        powerGlovedTrailPool.Add(controller);
        controller.gameObject.SetActive(false);

        return controller;
    }
}
