using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RedHotTarget : MonoBehaviour {

    /// <summary>
    /// Variable que conté l'actual instància del RedHotTarget.
    /// Per a que funcioni, aquest script està afegit al ScenarioManager
    /// </summary>
    public static RedHotTarget instance;

    /// <summary>
    /// Llista que guardarà tots els Red Hot Densities en pantalla
    /// </summary>
    private List<GameObject> _redHotDensities;

    /// <summary>
    /// Getter públic dels Red Hot Densities.
    /// </summary>
    public List<GameObject> redHotDensities
    {
        get
        {
            return _redHotDensities;
        }
    }

    /// <summary>
    /// Llista que guardara tots els enemics en pantalla
    /// </summary>
    private List<GameObject> _shootables;

    /// <summary>
    /// Getter public dels shootables
    /// </summary>
    public List<GameObject> shootables
    {
        get
        {
            return _shootables;
        }
    }

    /// <summary>
    /// Llista que guarda tots els punts imantats en pantalla
    /// </summary>
    private List<GameObject> _powerGlovedMagnets;

    /// <summary>
    /// Get public dels magnets
    /// </summary>
    public List<GameObject> powerGlovedMagnets
    {
        get
        {
            return _powerGlovedMagnets;
        }
    }

    /// <summary>
    /// Assignem la instància i creem les llistes
    /// </summary>
    void Awake()
    {
        instance = this;

        _redHotDensities = new List<GameObject>();

        _shootables = new List<GameObject>();

        _powerGlovedMagnets = new List<GameObject>();
    }

    /// <summary>
    /// Afegeix un element de tipus shootable a la llista
    /// </summary>
    /// <param name="shootable">GameObject que ha de ser afegit a la llista quan està en pantalla</param>
    public void AddShootable(GameObject shootable)
    {
        if (!_shootables.Contains(shootable))
        {
            _shootables.Add(shootable);
        }
    }

    /// <summary>
    /// Treu un element de tipus shootable de la llista
    /// </summary>
    /// <param name="shootable">GameObject que ha de ser tret de la llista que està en pantalla</param>
    public void RemoveShootable(GameObject shootable)
    {
        if (_shootables.Contains(shootable))
        {
            _shootables.Remove(shootable);
        }
    }

    /// <summary>
    /// Afegeix un nou portal a la llista
    /// </summary>
    /// <param name="redHotDensity">GameObject que ha de ser afegit a la llista de portals</param>
    public void AddRedHotDensity(GameObject redHotDensity)
    {
        if (!_redHotDensities.Contains(redHotDensity))
        {
            _redHotDensities.Add(redHotDensity);
        }
    }

    /// <summary>
    /// Treu un magnet de la llista
    /// </summary>
    /// <param name="powerGlovedMagnet">GameObject que ha de ser tret de la llista de magnets</param>
    public void RemovePowerGlovedMagnet(GameObject powerGlovedMagnet)
    {
        if (_powerGlovedMagnets.Contains(powerGlovedMagnet))
        {
            _powerGlovedMagnets.Remove(powerGlovedMagnet);
        }
    }

    /// <summary>
    /// Afegeix un nou magnet a la llista
    /// </summary>
    /// <param name="powerGlovedMagnet">GameObject que ha de ser afegit a la llista de magnets</param>
    public void AddPowerGlovedMagnet(GameObject powerGlovedMagnet)
    {
        if (!_powerGlovedMagnets.Contains(powerGlovedMagnet))
        {
            _powerGlovedMagnets.Add(powerGlovedMagnet);
        }
    }

    /// <summary>
    /// Treu un portal a la llista
    /// </summary>
    /// <param name="redHotDensity">GameObject que ha de ser tret de la llista de portals</param>
    public void RemoveRedHotDensity(GameObject redHotDensity)
    {
        if (_redHotDensities.Contains(redHotDensity))
        {
            _redHotDensities.Remove(redHotDensity);
        }
    }

    /// <summary>
    /// Busca el millor target (si existeix) segons el sistema RedHotTarget
    /// </summary>
    /// <param name="previous">El GameObject que teniem sel·leccionat abans (si en teniem algun)</param>
    /// <param name="axisAngle">L'angle que forma l'axis que volem fer servir</param>
    /// <param name="lookingDirection">Cap a on mira el Player/element que està fent target</param>
    /// <param name="position">Posició del Player/element que està fent target</param>
    /// <param name="colliderCenter">Centre del collider del Player/element</param>
    /// <param name="lastAngle">Últim angle que tenien les axis, per saber si l'hem canviat o no</param>
    /// <param name="layerMask">Mascara on farà el RayCast quan trobi un element, per veure si n'hi ha algun davant</param>
    /// <param name="list">Llista d'on consultarem els elements als que podem fer target</param>
    /// <returns>Retorna un GameObject amb la millor opció, o null si no en troba cap</returns>
    public static GameObject Target(GameObject previous, float axisAngle, int lookingDirection, Vector3 position, BoxCollider2D collider, float lastAngle, LayerMask layerMask, List<GameObject> list)
    {
        GameObject target = null;
        float bestAngle = -999;
        RaycastHit2D[] rays = new RaycastHit2D[5];
        // float angleSign = Mathf.Sign(axisAngle);
        // Debug.DrawLine(position + colliderCenter, position + colliderCenter + Vector3.right * 40 * lookingDirection * axisAngle, Color.red);
        Debug.DrawLine(position + (Vector3)collider.offset, position + (Vector3)collider.offset + Quaternion.AngleAxis(axisAngle, Vector3.forward * lookingDirection) * Vector3.right * 20 * lookingDirection, Color.red);

        //Debug.Log("Last angle: " + lastAngle + " - Axis angle: " + axisAngle + " - Previous: " + previous);

        if (lastAngle == axisAngle && previous != null && list.Contains(previous))
        {
            target = previous;
        }
        else
        {
            foreach (GameObject shootable in list)
            {
                //if ((lookingDirection == 1 && shootable.transform.position.x + 1 > position.x) ||
                //    (lookingDirection == -1 && shootable.transform.position.x < position.x + 1))
                {
                    // Debug.Log("Intersecta: " + collider.bounds.Intersects(shootable.collider2D.bounds));
                    if (!collider.bounds.Intersects(shootable.GetComponent<Collider2D>().bounds))
                    {
                        float angle = Vector3.Angle(Vector3.right * lookingDirection, shootable.transform.position - (position + (Vector3)collider.offset));
                        Vector3 cross = Vector3.Cross(Vector3.right * lookingDirection, shootable.transform.position - (position + (Vector3)collider.offset));

                        if (cross.z < 0)
                            angle = -angle;

                        angle *= lookingDirection;

                        //else if (Mathf.Abs(axisAngle - angle) < Mathf.Abs(bestAngle) && (!useRay || ray.collider && ray.collider.GetComponentInChildren<Shootable>().gameObject == shootable))
                        if (Mathf.Abs(axisAngle - angle) < Mathf.Abs(axisAngle - bestAngle))
                        {
                            // TODO Això s'hauria de fer nonAlloc i la layer hauria de ser de shootables
                            if ((Physics2D.LinecastNonAlloc(position + (Vector3)collider.offset, shootable.transform.position, rays, layerMask) > 0))
                            {
                                for (int i = 0; i < rays.Length; i++)
                                {
                                    if (!collider.bounds.Intersects(rays[i].collider.bounds))
                                    {
                                        target = rays[i].collider.gameObject;
                                        break;
                                    }
                                    else
                                    {
                                        target = shootable;
                                    }
                                }
                            }
                            else
                            {
                                target = shootable;
                            }
                            //Debug.Log(string.Format("Best angle {0} - current angle: {1}", bestAngle, Mathf.Abs(axisAngle - angle)), this.gameObject);
                            bestAngle = angle; // Agafem el S-tv que agafem, el angle es manté
                        }
                    }
                    
                }
            }
        }

        //Debug.Log(string.Format("Best angle: {0} - axisAngle: {1}", bestAngle, axisAngle));

        return target;
    }

    
}
