using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PooledObject : MonoBehaviour
{
    /// <summary>
    /// Pooler d'objectes al que pertany
    /// </summary>
    protected GameObjectPooler pooler;

    protected bool active;

    public bool Active
    {
        get
        {
            return active;
        }

        set
        {
            active = value;
        }
    }

    public GameObjectPooler Pooler
    {
        get
        {
            return pooler;
        }

        set
        {
            pooler = value;
        }
    }

    protected virtual void Awake()
    {
        SceneManager.sceneUnloaded += OnSceneEnds;
    }

    /// <summary>
    /// Al deshabilitar el trail, sigui perque acaba o perque es canvia l'escena, el tornem al pool
    /// </summary>
    protected virtual void OnDisable()
    {
        active = false;
    }

    protected void OnSceneEnds(Scene scene)
    {
        transform.SetParent(pooler.transform);
    }
}
