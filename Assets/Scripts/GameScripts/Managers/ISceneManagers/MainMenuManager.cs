using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections;
using System.IO;

public class MainMenuManager : MonoBehaviour, ISceneManager
{
    /// <summary>
    /// El botó per a començar a jugar / continuar partida
    /// </summary>
    public Button startGame;

    public Button startGameToControls;

    public Button continueButton;

    /// <summary>
    /// El botó per a crear una partida nova
    /// </summary>
    public Button newGame;

    [HideInInspector]
    public bool controlsViewed;

#if UNITY_EDITOR
    /// <summary>
    /// Just to force that GameManager is Loaded. Only usefull in Editor, in build Splash do that
    /// </summary>
    private void Awake()
    {
        // Vaig a forçar que carregui el GameManager, per si no hi es
        if(GameManager.Instance == null)
        {
            string ensureGameManagerLoaded = GameManager.Instance.currentLevelName;
        }
    }
#endif

    void Start()
    {
        // Re-sel·leccionem el primer element del event system
        EventSystem.current.SetSelectedGameObject(null);

        this.startGame.gameObject.SetActive(false);
        this.continueButton.gameObject.SetActive(false);
        this.startGameToControls.gameObject.SetActive(false);

        if (ExistSavedFile())
        {
            this.continueButton.gameObject.SetActive(true);
            EventSystem.current.SetSelectedGameObject(continueButton.gameObject);
        }
        else
        {
            if (controlsViewed)
            {
                this.startGame.gameObject.SetActive(true);
                EventSystem.current.SetSelectedGameObject(startGame.gameObject);
            }
            else
            {
                this.startGameToControls.gameObject.SetActive(true);
                EventSystem.current.SetSelectedGameObject(startGameToControls.gameObject);
            }

            this.newGame.interactable = false;
        }
    }

    public void ControlsViewed()
    {
        if(!ExistSavedFile())
        {
            this.startGame.gameObject.SetActive(true);
            this.startGameToControls.gameObject.SetActive(false);
        }
    }

    private bool ExistSavedFile()
    {
        return File.Exists(PlayerDataHolder.getFilePath());
    }
}
