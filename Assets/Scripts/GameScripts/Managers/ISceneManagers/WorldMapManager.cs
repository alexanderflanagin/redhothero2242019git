using UnityEngine;
using System.Collections;

public class WorldMapManager : MonoBehaviour, ISceneManager
{
    public static WorldMapManager current;

    /// <summary>
    /// El beam que connecta el centre amb l'stage sel·leccionada
    /// </summary>
    public LineRenderer beam;

    /// <summary>
    /// Quantitat que movem cada cop que canviem d'enviro
    /// </summary>
    public float onEnviroChangeMovement = 20f;

    /// <summary>
    /// Velocitat a la que movem la camera i el player al canviar d'enviro
    /// </summary>
    public float enviroChangeSpeed = 10f;

    /// <summary>
    /// Elements que mourem (sense amagar) al canviar d'enviro
    /// </summary>
    public Transform[] visibleItemsToMoveOnEnviroChange;

    /// <summary>
    /// Elements que mourem (amagant abans) al canviar d'enviro
    /// </summary>
    public Transform[] hiddenItemsToMoveOnEnviroChange;

    /// <summary>
    /// Per a no poder fer canvis d'enviro mentre ja n'estavem fent un
    /// </summary>
    private bool changingEnviro = false;

    /// <summary>
    /// Per a saber quina stage tenim actualment sel·leccionada
    /// </summary>
    private int selected = 0;

    /// <summary>
    /// Les partícules del centre, on es connecta el beam
    /// </summary>
    public Transform selectedStageParticles;

    /// <summary>
    /// Els stages sel·leccionables
    /// </summary>
    public StageDot[] stageDots;

    /// <summary>
    /// Red Hot Hero en format travel per quan entrem a la pantalla
    /// </summary>
    public GameObject travellingHero;

    /// <summary>
    /// Red Hot Hero flotant que es mostra en pantalla
    /// </summary>
    public GameObject redHotHero;

    /// <summary>
    /// Aquí carregarem la coroutina d'omplir la GUI, per a poder-la parar
    /// </summary>
    private IEnumerator fillGUI;

    /// <summary>
    /// Per guardar un registre dels stages unlocked (per si fem wrap zones)
    /// </summary>
    private bool[] unlockedStages;

    /// <summary>
    /// Assignem el valor static per a poder agafar aquest manager
    /// </summary>
    private void Awake()
    {
        current = this;
    }

    /// <summary>
    /// Al iniciar l'objecte, inicialitzem el diccionari, afegim el GUI manager, posem la direcció actual, etc.
    /// </summary>
    void Start()
    {
        unlockedStages = new bool[GameManager.Instance.gameData.stagesOrdered[0].stages.Length];

        // La posició 0 sempre serà el centre
        this.beam.SetPosition(0, this.selectedStageParticles.position);

        this.travellingHero.SetActive(false);

        PrepareStageDots();

        UpdateStageDotSelected();

        StartCoroutine(HandleInput());
    }

    /// <summary>
    /// Assigna a cada Stage Dot el nom d'escena que ha de carregar i les mostra o no segons si la tenim unlocked
    /// </summary>
    public void PrepareStageDots()
    {
        string defaultFirstLevel = GameManager.Instance.gameData.stagesData[GameManager.Instance.gameData.stagesOrdered[0].stages[0]].sceneName; ; // Això ho guardarem com a el nivell per defecte
        
        for (int i = 0; i < this.stageDots.Length; i++)
        {
            unlockedStages[i] = false;
            this.stageDots[i].sceneName = GameManager.Instance.gameData.stagesData[GameManager.Instance.gameData.stagesOrdered[(int)GameManager.Instance.currentEnviro].stages[i]].sceneName;

			PlayerStageData playerLevelData;
			if(!GameManager.Instance.playerData.levelsData.TryGetValue(this.stageDots[i].sceneName, out playerLevelData))
			{
                this.stageDots[i].gameObject.SetActive(false);
			}
			else if (!playerLevelData.unlocked && !playerLevelData.finished)
            {
                // Pot ser que ens l'haguem passat sense unlock si es el primer
                this.stageDots[i].gameObject.SetActive(false);
            }
            else if (this.stageDots[i].sceneName == "PereDevelop")
            {
                this.stageDots[i].gameObject.SetActive(false);
            }
            else
            {
                unlockedStages[i] = true;
                this.stageDots[i].gameObject.SetActive(true);
                //this.stageDotsUnlocked++;
            }

            // FIX Obrim sempre la primera stage
            // Això no caldrà quan s'obrin bé les escenes
            if (this.stageDots[i].sceneName == defaultFirstLevel && !this.stageDots[i].gameObject.activeInHierarchy)
            {
                this.stageDots[i].gameObject.SetActive(true);
                //this.stageDotsUnlocked++;
                this.selected = 0;
            }

            if (GameManager.Instance.levelComplete && this.stageDots[i].sceneName == GameManager.Instance.currentLevelName)
            {
                this.selected = i + 1;
            }
            else if (this.stageDots[i].sceneName == GameManager.Instance.currentLevelName)
            {
                this.selected = i;
            }
            else
            {
                // Posem el punter al darrer stage unlocked
                for(int j = 0; j < unlockedStages.Length; j++)
                {
                    if(unlockedStages[j])
                    {
                        selected = j;
                    }
                }
            }
        }
    }

    /// <summary>
    /// Actualitzem la stage dot sel·leccionada. Movem el raig, recuperem la info que en tenim i mostrem la info de la gui a través del /FillGUI/
    /// </summary>
    void UpdateStageDotSelected()
    {
        // Si encara estàvem omplint la GUI, ho cancel·lem
        if (this.fillGUI != null)
        {
            StopCoroutine(this.fillGUI);
        }

        this.beam.SetPosition(1, this.stageDots[selected].transform.position);

        LevelData levelData;
        if (!GameManager.Instance.gameData.sceneNameBasedStagesList.TryGetValue(this.stageDots[selected].sceneName, out levelData))
        {
            Debug.LogWarning("No tenim info del level data! Això no hauria de poder succeir");
            levelData = LevelData.CreateDummy();
        }

        PlayerStageData playerLevelData;
        if (!GameManager.Instance.playerData.levelsData.TryGetValue(this.stageDots[selected].sceneName, out playerLevelData))
        {
            Debug.LogWarning("El player no té info d'aquest nivell encara");
            playerLevelData = new PlayerStageData();
        }
        
        this.fillGUI = WorldMapHUD.current.FillGUI(levelData, playerLevelData);
        StartCoroutine(this.fillGUI);
    }

    /// <summary>
    /// Gestionem l'input per a canviar de pantalla - enviro
    /// </summary>
    /// <returns></returns>
    private IEnumerator HandleInput()
    {
        WaitForSeconds timeBetweenActions = new WaitForSeconds(0.2f);

        while (true)
        {
            if(!changingEnviro)
            {
                if (cInput.GetAxis(InputButton.HORIZONTAL_AXIS) > 0.9f)
                {
                    SelectNextStage();

                    UpdateStageDotSelected();
                    yield return timeBetweenActions;
                }

                if (cInput.GetAxis(InputButton.HORIZONTAL_AXIS) < -0.9f)
                {
                    SelectPreviousStage();

                    UpdateStageDotSelected();
                    yield return timeBetweenActions;
                }

                if (cInput.GetAxis(InputButton.VERTICAL_AXIS) > 0.9f)
                {
                    if (selected < 5)
                    {
                        SelectNextStage();
                    }
                    else
                    {
                        SelectPreviousStage();
                    }

                    UpdateStageDotSelected();
                    yield return timeBetweenActions;
                }

                if (cInput.GetAxis(InputButton.VERTICAL_AXIS) < -0.9f)
                {
                    if (selected > 4)
                    {
                        SelectNextStage();
                    }
                    else
                    {
                        SelectPreviousStage();
                    }

                    UpdateStageDotSelected();
                    yield return timeBetweenActions;
                }

                if (cInput.GetButtonDown(InputButton.TRAVEL + InputButton.ALT))
                {
                    SelectNextEnviro();
                }

                if (cInput.GetButtonDown(InputButton.BRAKE))
                {
                    SelectPreviousEnviro();
                }

                if(cInput.GetButtonDown(InputButton.PUNCH))
                {
                    ReturToMainMenu();
                }
            }

            yield return null;
        }
    }

    /// <summary>
    /// Selecciona el següent stage
    /// </summary>
    private void SelectNextStage()
    {
        // Si ja estem a l'ultim stage del enviro
        if (selected == EnviroData.STAGES_PER_ENVIRO - 1)
        {
            SelectNextEnviro();
            return;
        }

        // Si no, avancem al següent
        for (int i = selected + 1; i < EnviroData.STAGES_PER_ENVIRO; i++)
        {
            if (unlockedStages[i])
            {
                selected = i;
                break;
            }
        }
    }

    /// <summary>
    /// Selecciona l'stage anterior
    /// </summary>
    private void SelectPreviousStage()
    {
        if(selected == 0)
        {
            SelectPreviousEnviro();
            return;
        }

        for (int i = selected - 1; i >= 0; i--)
        {
            if (unlockedStages[i])
            {
                selected = i;
                break;
            }
        }
    }

    /// <summary>
    /// Sel.lecciona la primera pantalla del següent enviro
    /// </summary>
    private void SelectNextEnviro()
    {
        EnviroData.EnviroType newEnviro = EnviroData.EnviroType.Coast;

        switch (GameManager.Instance.currentEnviro)
        {
            case EnviroData.EnviroType.Lab:
                // Ja estem a la ultima posició del joc
                return;
            case EnviroData.EnviroType.Coast:
                newEnviro = EnviroData.EnviroType.Jungle;
                break;
            case EnviroData.EnviroType.Jungle:
                newEnviro = EnviroData.EnviroType.Lab;
                break;
        }

        int id = GameManager.Instance.gameData.stagesOrdered[(int)newEnviro].stages[0];
        string newSceneKey = GameManager.Instance.gameData.stagesData[id].sceneName;

        if (!GameManager.Instance.playerData.levelsData.ContainsKey(newSceneKey))
        {
            // No tenim desbloquejada aquesta pantalla
            return;
        }

        // Tenim desbloquejada la primera pantalla del seguent enviro. Canviem l'enviro i la sel.leccionem
        GameManager.Instance.currentEnviro = newEnviro;
        WorldMapHUD.current.UpdateEnviroImage();
        
        PrepareStageDots();

        selected = 0;

        // Fem la coroutina de moure'ns
        StartCoroutine(ChangeEnviro(Vector3.right));

        // Actualitzem l'stage que tenim sel.leccionat
        UpdateStageDotSelected();
    }

    /// <summary>
    /// Sel.lecciona la darrera pantalla desbloquejada de l'enviro anterior
    /// </summary>
    private void SelectPreviousEnviro()
    {
        EnviroData.EnviroType newEnviro = EnviroData.EnviroType.Coast;

        switch (GameManager.Instance.currentEnviro)
        {
            case EnviroData.EnviroType.Coast:
                // Ja estem a la ultima posició del joc
                return;
            case EnviroData.EnviroType.Jungle:
                newEnviro = EnviroData.EnviroType.Coast;
                break;
            case EnviroData.EnviroType.Lab:
                newEnviro = EnviroData.EnviroType.Jungle;
                break;
        }

        for (int i = EnviroData.STAGES_PER_ENVIRO - 1; i >= 0; i--)
        {
            int id = GameManager.Instance.gameData.stagesOrdered[(int)newEnviro].stages[i];
            string newSceneKey = GameManager.Instance.gameData.stagesData[id].sceneName;
            if (GameManager.Instance.playerData.levelsData.ContainsKey(newSceneKey))
            {
                // Ja hem trobat la pantalla desbloquejada
                GameManager.Instance.currentEnviro = newEnviro;
                WorldMapHUD.current.UpdateEnviroImage();

                PrepareStageDots();

                selected = i;
                // Fem la coroutina de moure'ns
                StartCoroutine(ChangeEnviro(Vector3.left));

                // Actualitzem l'stage que tenim sel.leccionat
                UpdateStageDotSelected();
                return;
            }
        }

        Debug.LogError("Hem tirat enrera en una pantalla que no era de Coast i no hem trobat cap pantalla anterior");
    }

    private void ReturToMainMenu()
    {
        GameManager.Instance.LoadLevel("MainMenu");
    }

    /// <summary>
    /// Canvia l'enviro que estem veient
    /// </summary>
    /// <param name="direction">Direccio cap a la que ens mourem</param>
    /// <returns></returns>
    private IEnumerator ChangeEnviro(Vector3 direction)
    {
        changingEnviro = true;

        for (int i = 0; i < hiddenItemsToMoveOnEnviroChange.Length; i++)
        {
            hiddenItemsToMoveOnEnviroChange[i].gameObject.SetActive(false);
        }

        yield return null;

        float currentMovement = 0;
        float alreadyMoved = 0;
        while(!Mathf.Approximately(currentMovement, onEnviroChangeMovement))
        {
            currentMovement = Mathf.Lerp(currentMovement, onEnviroChangeMovement, Time.deltaTime * enviroChangeSpeed);

            for(int i = 0; i < visibleItemsToMoveOnEnviroChange.Length; i++)
            {
                visibleItemsToMoveOnEnviroChange[i].position += (currentMovement - alreadyMoved) * direction;
            }

            alreadyMoved = currentMovement;
            yield return null;
        }

        for (int i = 0; i < hiddenItemsToMoveOnEnviroChange.Length; i++)
        {
            hiddenItemsToMoveOnEnviroChange[i].position += direction * onEnviroChangeMovement;
            hiddenItemsToMoveOnEnviroChange[i].gameObject.SetActive(true);
        }

        // Actualitzem els punts del beam
        beam.SetPosition(0, selectedStageParticles.position);
        beam.SetPosition(1, stageDots[selected].transform.position);

        changingEnviro = false;
    }

    private IEnumerator EnterStage()
    {
        AudioManager.Instance.PlayOneShoot(AudioClipsData.ClipsGroups.Enviro, "WorldMap Stage In");
        
        // Activamos travelling hero y ocultamos hero
        this.travellingHero.SetActive(true);
        this.travellingHero.transform.position = this.redHotHero.transform.position;

        this.redHotHero.SetActive(false);
        
        // Amaguem el raig
        this.beam.enabled = false;

        // Amaguem totes les stagedots
        for (int i = 0; i < this.stageDots.Length; i++)
        {
            this.stageDots[i].gameObject.SetActive(false);
        }

        // Amaguem tota la GUI
        Canvas[] allCanvas = GameObject.FindObjectsOfType<Canvas>();

        for (int i = 0; i < allCanvas.Length; i++)
        {
            allCanvas[i].gameObject.SetActive(false);
        }

        // Activem partícules d'arribada
        this.selectedStageParticles.GetComponentInParent<Animator>().enabled = true;

        for (int i = 0; i < 30; i++)
        {
            this.travellingHero.transform.position = Vector3.MoveTowards(this.travellingHero.transform.position, this.selectedStageParticles.position, Time.deltaTime * 16);
            yield return null;
        }

        GameManager.Instance.LoadLevel(this.stageDots[selected].sceneName);
    }

    void Update()
    {
        if (!changingEnviro && cInput.GetButtonDown(InputButton.JUMP))
        {
            StartCoroutine(EnterStage());
        }
    }
}
