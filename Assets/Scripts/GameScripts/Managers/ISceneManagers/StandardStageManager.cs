using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Com.LuisPedroFonseca.ProCamera2D;


public class StandardStageManager : MonoBehaviour, ISceneManager
{
    public static int PICKEABLES_PER_STAGE = 3;

    /// <summary>
    /// Getter directe del StandardStageManager actual
    /// </summary>
    public static StandardStageManager current;

    /// <summary>
    /// Array on enregistrem els enemics morts (per a estadistiques).
    /// </summary>
    public static int[] deadEnemies;

    public enum PickeablesOrder
    {
        Fuse_partCellCellCell = 0,
        CellFuse_partCellCell = 1,
        CellCellFuse_partCell = 2,
        CellCellCellFuse_part = 3,
        CellCellCellCell = 4,
    }

    public Action StageFinished;
    
    /// <summary>
    /// A quin enviro pertany
    /// </summary>
    public EnviroData.EnviroType enviroType;

    /// <summary>
    /// Quin es l'ordre d'aquesta pantalla
    /// </summary>
    public int position;

    public Sprite cellPickedImage;

    /// <summary>
    /// Acces public al component de customCameraModifiers. Per a poder-hi accedir des del player
    /// </summary>
    public CustomCameraModifiers customCameraModifiers;

    /// <summary>
    /// Llista amb tots els enemics actius d'aquest stage (per a apagar-los quan acabem)
    /// </summary>
    public List<EnemyController> activeEnemies;

    /// <summary>
    /// Llista amb totes les densities actives d'aquest stage (per a apagar-les quan acabem)
    /// </summary>
    public List<RedHotDensity> activeDensities;

    /// <summary>
    /// Per si volem for�ar a carregar un nivell despres d'aquest
    /// </summary>
    public string forcedNextLevel;

    /// <summary>
    /// Si hem tocat el spawn point del nivell actual
    /// </summary>
    [HideInInspector]
    public bool checkPointTouched;

    /// <summary>
    /// Informació d'aquest nivell segons la configuració
    /// </summary>
    [HideInInspector]
    public LevelData levelData;

    [HideInInspector]
    public GameObject downLimit;

    [HideInInspector]
    public GameObject topLimit;

    [HideInInspector]
    public GameObject leftLimit;

    [HideInInspector]
    public GameObject rightLimit;

    /// <summary>
    /// Les cells que tenia el player al començar
    /// </summary>
    private int atStartCells;

    //private bool fusePartPicked;

    private int atStartFusePartsPicked;

    /// <summary>
    /// Array of the max Height camera boundaries
    /// </summary>
	protected float[] cameraBoundaries;

    /// <summary>
    /// El player de la pantalla
    /// </summary>
	protected GameObject _player;

    /// <summary>
    /// El MainCharControl dels player (per a no haver-lo de buscar cada cop amb GetComponent)
    /// </summary>
	protected CharacterControl _playerControl;

    /// <summary>
    /// Etiqueta que mostra el crono per pantalla.
    /// </summary>
    protected Text _crono;

    /// <summary>
    /// Temps de la pantalla
    /// </summary>
    protected float _time;

    /// <summary>
    /// Temps que teniem quan hem començat
    /// </summary>
    protected float starTime;

    /// <summary>
    /// Retorna el player
    /// </summary>
    public GameObject player { get { return _player; } }

    /// <summary>
    /// Retorna els controls del player
    /// </summary>
    public CharacterControl playerControl { get { return _playerControl; } }

    /// <summary>
    /// Per saber si estem jugant
    /// </summary>
    public bool playing { get; set; }

    /// <summary>
    /// Getter public del time
    /// </summary>
    public float time
    {
        get
        {
            return _time;
        }
    }

    /// <summary>
    /// Array amb informacio de si una moneda esta o no agafada.
    /// Per defecte, aquest array es igual de llarg que coinInfo
    /// </summary>
    public bool[] pickeablesPicked { get; set; }

    

    /// <summary>
    /// Afegim un component de CustomCameraModifier per a modificar el behaviour de la camera durant el travel
    /// Ho poso aqui per si en un futur hi ha mes tipus de modificacions
    /// </summary>
    private void Awake()
    {
        customCameraModifiers = gameObject.AddComponent<CustomCameraModifiers>();
        activeEnemies = new List<EnemyController>();
        activeDensities = new List<RedHotDensity>();
    }

    protected void OnEnable()
    {
        transform.GetOrAddComponent<HUDManager>();

        current = this;

        //onEnterPlayerData = ScriptableObject.Instantiate(GameManager.Instance.playerData) as PlayerDataHolder;

        // Registrem els Events de pausa
        HUDManager.PauseEnter += OnPauseEnter;
        HUDManager.PauseExit += OnPauseExit;

        GenerateStageLimits();

        current = this;

        SpawnHero();

        RestoreFromSpawnPointIfNeeded();

        // Energia mínima necessaria
        // La treu de la configuració global
        if (GameManager.Instance.gameData.sceneNameBasedStagesList.TryGetValue(SceneManager.GetActiveScene().name, out levelData))
        {
            if (levelData.forcedBatteries != 0)
            {
                GameManager.Instance.playerData.SetBatteries(levelData.forcedBatteries);

            }
            else if (GameManager.Instance.playerData.GetBatteries() < levelData.forcedMinimumBatteries)
            {
                GameManager.Instance.playerData.SetBatteries(levelData.forcedMinimumBatteries);
            }

            GameManager.Instance.currentLevelName = levelData.sceneName;
        }
        else
        {
            levelData = LevelData.CreateDummy();
            Debug.Log("Aquesta escena no està al levels Data", gameObject);
        }
    }

    protected void OnDisable()
    {
        HUDManager.PauseExit();
        
        // Desregistrem els Events de pausa
        HUDManager.PauseEnter -= OnPauseEnter;
        HUDManager.PauseExit -= OnPauseExit;

        if (_player)
        {
            _player.SetActive(false);
        }

        if (activeEnemies != null && activeEnemies.Count > 0)
        {
            // Creo una llista copia perque, com que s'eliminaran ells mateixo de la llista, no tingui problemes amb el punter d'aquesta
            EnemyController[] activeEnemiesCopy = activeEnemies.ToArray();
            for (int i = 0; i < activeEnemiesCopy.Length; i++)
            {
                activeEnemiesCopy[i].gameObject.SetActive(false);
            }
        }

        if(activeDensities != null && activeDensities.Count > 0)
        {
            // Creo una llista copia perque, com que s'eliminaran ells mateixo de la llista, no tingui problemes amb el punter d'aquesta
            RedHotDensity[] activeDensitiesCopy = activeDensities.ToArray();
            for (int i = 0; i < activeDensitiesCopy.Length; i++)
            {
                activeDensitiesCopy[i].gameObject.SetActive(false);
            }
        }
    }

    protected void Start()
    {
        // Assignem el crono del hud
        GameObject cronoObject = GameObject.Find("TimerHUD/Text");

        if (cronoObject != null && cronoObject.GetComponent<Text>() != null)
        {
            _crono = cronoObject.GetComponent<Text>();
        }

        // Create enemies array
        deadEnemies = new int[Enum.GetValues(typeof(EnemiesManager.Enemies)).Length];

        // Posem el player a la càmera com a target
        //ProCamera2D.Instance.CameraTargets.Clear();
        //Debug.Log("Després del càmera clear");
        ProCamera2D.Instance.AddCameraTarget(this._player.transform);
        //Debug.Log("Després del camera add target. " + this._player.transform);
        ProCamera2D.Instance.MoveCameraInstantlyToPosition(new Vector3(this._player.transform.position.x, this._player.transform.position.y + 8.5f, ProCamera2D.Instance.transform.position.z));

        // Replace the cell spawns with cells and fuse parts
        CreatePickeables();

        StartCoroutine(StartScenario());

        // Play sound
        AudioManager.Instance.PlayOneShoot(AudioClipsData.ClipsGroups.Enviro, "StartStage");
    }

    protected void Update()
    {
        if (GameManager.Instance.gameData.gameData.canReset && cInput.GetButtonDown(InputButton.RESTART))
        {
            GameManager.Instance.checkPointProperties = null;
            GameManager.Instance.LoadLevel(SceneManager.GetActiveScene().name);
        }

        if (playing)
        {
            _time = this.starTime + Time.timeSinceLevelLoad;
        }
    }

    protected void GenerateStageLimits()
    {
        GameObject gameplay = GameObject.Find("Gameplay");

        if (gameplay == null)
        {
            Debug.LogError("Aquesta pantalla no té GameObject de Gameplay", this.gameObject);
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#endif
            return;
        }

        Collider2D[] colliders = gameplay.GetComponentsInChildren<Collider2D>();

        if (colliders.Length >= 1)
        {
            Bounds bounds = new Bounds(Vector3.zero, Vector3.zero);
            foreach (Collider2D c in colliders)
            {
                if (!c.isTrigger)
                {
                    bounds.Encapsulate(c.bounds);
                }
            }

            float minX, maxX, minY, maxY;
            float triggerSize = 4f;
            float toRoofDistance = 20f;
            float downMargin = 4;
            float topLeftRightMargin = 40;
            minX = bounds.min.x - topLeftRightMargin;
            minY = bounds.min.y - downMargin; // Li donem un marge per a que no es vegin les explosions
            maxX = bounds.max.x + topLeftRightMargin;
            maxY = bounds.max.y + topLeftRightMargin;

            // Creem els 4 colliders
            downLimit = new GameObject("DownLimit");
            downLimit.AddComponent<DisableActorsTrigger>().makeEnemiesDisappear = true;
            downLimit.transform.parent = transform;
            downLimit.transform.position = new Vector3((maxX - Mathf.Abs(minX)) / 2, minY - triggerSize);
            downLimit.transform.localScale = new Vector3(maxX - minX + triggerSize, triggerSize);
            downLimit.AddComponent<BoxCollider2D>().isTrigger = true;

            topLimit = new GameObject("TopLimit");
            topLimit.AddComponent<DisableActorsTrigger>().makeEnemiesDisappear = true;
            topLimit.transform.parent = transform;
            topLimit.transform.position = new Vector3((maxX - Mathf.Abs(minX)) / 2, maxY + triggerSize + toRoofDistance);
            topLimit.transform.localScale = new Vector3(maxX - minX + triggerSize, triggerSize);
            topLimit.AddComponent<BoxCollider2D>().isTrigger = true;

            leftLimit = new GameObject("LeftLimit");
            leftLimit.AddComponent<DisableActorsTrigger>().makeEnemiesDisappear = true;
            leftLimit.transform.parent = transform;
            leftLimit.transform.position = new Vector3(minX - triggerSize / 2, (maxY + minY + toRoofDistance) / 2);
            leftLimit.transform.localScale = new Vector3(triggerSize, maxY - minY + triggerSize + toRoofDistance);
            leftLimit.AddComponent<BoxCollider2D>().isTrigger = true;

            rightLimit = new GameObject("RightLimit");
            rightLimit.AddComponent<DisableActorsTrigger>().makeEnemiesDisappear = true;
            rightLimit.transform.parent = transform;
            rightLimit.transform.position = new Vector3(maxX + triggerSize / 2, (maxY + minY + toRoofDistance) / 2);
            rightLimit.transform.localScale = new Vector3(triggerSize, maxY - minY + triggerSize + toRoofDistance);
            rightLimit.AddComponent<BoxCollider2D>().isTrigger = true;
        }
    }

    /// <summary>
    /// Fa l'spawn del Hero. Té en compte si ja hi és el game object, si hi ha un objecte d'spawn o si no hi ha res (en cas de que no hi hagi res, ho col·loca a una posició fixe)
    /// </summary>
    protected void SpawnHero()
    {
        GameObject[] spawns = GameObject.FindGameObjectsWithTag("Respawn");
        
        _player = GameManager.Instance.redHotHero;

        if (spawns.Length > 0)
        {    
            _player.transform.position = spawns[0].transform.position;   
        }

        if(_player.activeInHierarchy)
        {
            Debug.LogWarning("Player must start scene inactive");
            _player.SetActive(false);
        }

        _player.SetActive(true);

        _playerControl = _player.GetComponent<CharacterControl>();

        _player.layer = Layers.Player;

        for (int i = 0; i < spawns.Length; i++)
        {
            Destroy(spawns[i]);
        }
    }

    /// <summary>
    /// Mirem si hem de fer un restore des del check point i recuperar dades. Sinó creem l'objecte checkpoint d'aquest escenari
    /// </summary>
    protected void RestoreFromSpawnPointIfNeeded()
    {
        // Si hem de començar en un spawn point, el posem a l'spawn point
        if (GameManager.Instance.checkPointProperties != null && GameManager.Instance.checkPointProperties.currentStage == SceneManager.GetActiveScene().buildIndex && GameManager.Instance.checkPointProperties.startInCheckPoint)
        {
            // Resetegem l'start in check point (tot i que diria que no cal)
            GameManager.Instance.checkPointProperties.startInCheckPoint = false;
            GameManager.Instance.checkPointProperties.alreadyLoadedFromCheckPoint = true;

            // Posem els temps actuals
            this.starTime = GameManager.Instance.checkPointProperties.currentTime;
            this._time = GameManager.Instance.checkPointProperties.currentTime;

            // Posem el player al check point
            CheckPoint checkPoint = GameObject.FindObjectOfType<CheckPoint>();
            this._player.transform.position = checkPoint.transform.position;
            checkPoint.startOnCheckpoint = true;

            // Pre-omplim els pickeables
            this.pickeablesPicked = new bool[GameManager.Instance.checkPointProperties.coinsPickedPreCheckPoint.Length];
            for (int i = 0; i < this.pickeablesPicked.Length; i++)
            {
                this.pickeablesPicked[i] = GameManager.Instance.checkPointProperties.coinsPickedPreCheckPoint[i];
            }

            if(GameManager.Instance.checkPointProperties.powerGlove && !this._playerControl.suit.hasPowerGlove)
            {
                this._playerControl.suit.hasPowerGlove = true;
                this._playerControl.suit.addGloves();
            }

            if (GameManager.Instance.checkPointProperties.blastGlove && !this._playerControl.suit.hasBlastGlove)
            {
                this._playerControl.suit.hasBlastGlove = true;
                this._playerControl.suit.addGloves();
            }
        }
        else
        {
            GameManager.Instance.checkPointProperties = new CheckPointProperties();
            GameManager.Instance.checkPointProperties.currentStage = SceneManager.GetActiveScene().buildIndex;
        }
    }

    /// <summary>
	/// Canvia els spawns de les coins per coins, tenint en compte si el player ja la te o no.
    /// </summary>
	protected void CreatePickeables()
    {
        // Get Spawn points and order it
        IEnumerable<GameObject> pickeables = GameObject.FindGameObjectsWithTag(Tags.Pickeable);
        pickeables = pickeables.OrderBy(pickeable => (pickeable.transform.position.x + pickeable.transform.position.y));

        PlayerStageData pld;
        // Mirem si ja ens havíem passat abans aquesta pantalla i recollim la info dels pickeables
        if (!GameManager.Instance.playerData.levelsData.TryGetValue(SceneManager.GetActiveScene().name, out pld))
        {
            pld = new PlayerStageData();
        }

        // Create array of coins
        if (this.pickeablesPicked == null)
        {
            this.pickeablesPicked = new bool[pickeables.Count()];
        }

        if ((pickeables.Count() != 3))
        {
            Debug.LogWarning("[ScenarioManager::Start] Error. No hi ha 3 pickeables en pantalla");
        }

        for (int i = 0; i < pickeables.Count(); i++)
        {
            GameObject pickeable = null;

            if (i == (int)this.levelData.pickeablesOrder)
            {
                // Es una Fuse Part
                if (this.pickeablesPicked[i] || pld.pickeablesPicked[i])
                {
                    pickeable = (GameObject)GameObject.Instantiate(GeneralPurposeObjectManager.Instance.fusePartPicked, pickeables.ElementAt(i).transform.position, Quaternion.Euler(new Vector3(00, 90, 0)));
                }
                else
                {
                    pickeable = (GameObject)GameObject.Instantiate(GeneralPurposeObjectManager.Instance.fusePart, pickeables.ElementAt(i).transform.position, Quaternion.Euler(new Vector3(00, 90, 0)));
                }
            }
            else
            {
                // És una cell
                if (this.pickeablesPicked[i] || pld.pickeablesPicked[i])
                {
                    pickeable = (GameObject)GameObject.Instantiate(GeneralPurposeObjectManager.Instance.cellPicked, pickeables.ElementAt(i).transform.position, Quaternion.Euler(new Vector3(00, 90, 0)));
                }
                else
                {
                    pickeable = (GameObject)GameObject.Instantiate(GeneralPurposeObjectManager.Instance.cell, pickeables.ElementAt(i).transform.position, Quaternion.Euler(new Vector3(00, 90, 0)));
                }
            }

            pickeable.GetComponent<Pickeable>().currentScenarioArrayIndex = i;

            if (pickeablesPicked[i])
            {
                HUDManager.current.coins[i].sprite = this.cellPickedImage;
            }

            Destroy(pickeables.ElementAt(i));
        }
    }

    protected virtual IEnumerator StartScenario()
    {
        yield return null; // Ens assegurem de que el player ha estat creat, etc.

        _playerControl.SetController(GameManager.Instance.gameData.gameData.develControllersSuffix[0]);
        _playerControl.StartPlaying();

        playing = true;
    }

    #region PAUSE
    /// <summary>
	/// Es llença quan entra la pausa
    /// </summary>
    protected void OnPauseEnter()
    {
        Time.timeScale = 0;

        if(AudioManager.Instance.pauseSnapshot)
        {
            AudioManager.Instance.pauseSnapshot.TransitionTo(0.1f);
        }
    }

    /// <summary>
	/// Es llença quan es surt de la pausa
    /// </summary>
    protected void OnPauseExit()
    {
        Time.timeScale = 1;

        if (AudioManager.Instance.defaultSnapshot)
        {
            AudioManager.Instance.defaultSnapshot.TransitionTo(0.1f);
        }
    }
    #endregion

    public IEnumerator End(bool success)
    {
        // Debug.Log ("[ScenarioManager::End] Level ended");
        HUDManager.current.DisablePauseButton();

        playing = false;

        string levelToLoad = "";

        if (success)
        {
            if(StageFinished != null)
            {
                StageFinished();
            }

            PlayerStageData pld = new PlayerStageData();
            pld.bestTime = this._time;
            pld.finished = true;
            pld.pickeablesPicked = this.pickeablesPicked;

            // Hem fet speed run?
            if (this._time <= this.levelData.speedrunTime)
            {
                pld.speedrunDone = true;
            }

            //ConsoleLog.Instance.Log(string.Format(">>>> Pickeables picked: {0} - {1} - {2}", this.pickeablesPicked[0], this.pickeablesPicked[1], this.pickeablesPicked[2]));

            if (this._playerControl.suit.hasPowerGlove)
            {
                GameManager.Instance.playerData.powerGlove = true;
            }

            if (this._playerControl.suit.hasBlastGlove)
            {
                GameManager.Instance.playerData.blastGlove = true;
            }

            // Primer el més important, guardem la info
            GameManager.Instance.SavePlayerData(pld);
            
            // Amago el contador de temps i el d'energia de la pantalla
            HUDManager.current.energyMeterHUD.SetActive(false);
            HUDManager.current.timerHUD.SetActive(false);
            HUDManager.current.coinsHUD.SetActive(false);

            if (GameManager.Instance.gameData.gameData.reloadSceneAfterFinishLine)
            {
                levelToLoad = SceneManager.GetActiveScene().name;
            }
            else
            {
                GameManager.Instance.levelComplete = true;
                if (string.IsNullOrEmpty(forcedNextLevel))
                {
                    levelToLoad = "WorldMap";
                }
                else
                {
                    levelToLoad = forcedNextLevel;
                }

            }

            // Resetegem la info del checkpoint
            GameManager.Instance.checkPointProperties = new CheckPointProperties();

            if (HUDManager.current)
            {
                HUDManager.current.StartStageCompleteHUD();
            }
            else
            {
                // El HUDManager ha de mostrar estadístiques i, quan es cridi el Next, cridar el LoadNextLevel. Si no el trobem, esperem 2 segons i el cridem manualment
                yield return new WaitForSeconds(2f);
                LoadNextLevel();
            }
        }
        else
        {
            // Hem mort. Tornem a carregar la pantalla
            //if (GameManager.Instance.checkPointProperties == null)
            //{
            //    GameManager.Instance.checkPointProperties = new CheckPointProperties();
            //    GameManager.Instance.checkPointProperties.currentStage = Application.loadedLevel;
            //}

            //GameManager.Instance.checkPointProperties.startInCheckPoint = this.checkPointTouched;

            //if (this.checkPointTouched)
            //{
            //    GameManager.Instance.currentStageCoinsPicked = this.cellsPicked;
            //}

            if (GameManager.Instance.checkPointProperties != null && GameManager.Instance.checkPointProperties.currentStage == SceneManager.GetActiveScene().buildIndex)
            {
                GameManager.Instance.checkPointProperties.currentTime = this._time;
                //Debug.Log("Aqui encara tinc el power glove: " + GameManager.Instance.checkPointProperties.powerGlove);
            }

            levelToLoad = SceneManager.GetActiveScene().name;

            yield return new WaitForSeconds(0.5f);
            GameManager.Instance.LoadLevel(levelToLoad);
        }
    }

    public void LoadNextLevel()
    {
        string levelToLoad;
        
        if (GameManager.Instance.gameData.gameData.reloadSceneAfterFinishLine)
        {
            levelToLoad = SceneManager.GetActiveScene().name;
        }
        else
        {
            GameManager.Instance.levelComplete = true;
            if (string.IsNullOrEmpty(forcedNextLevel))
            {
                levelToLoad = "WorldMap";
            }
            else
            {
                levelToLoad = forcedNextLevel;
            }
        }

        GameManager.Instance.LoadLevel(levelToLoad);
    }

    /// <summary>
    /// Mètode que es crida quan s'agafa una moneda
    /// </summary>
    /// <param name="index"></param>
    public void PickeablePicked(int index)
    {
        pickeablesPicked[index] = true;

        StartCoroutine(HUDManager.current.CoinPicked(index));
    }

    /// <summary>
    /// Metode que crida la línia de meta quan s'arriba
    /// </summary>
    public void Finish()
    {
        StartCoroutine(End(true));
    }

	/// <summary>
	/// Metode per a avisar que el player ha mort
	/// </summary>
	/// <param name="player">El player que ha mort</param>
    public void PlayerKilled(GameObject player)
    {
        StartCoroutine(End(false));
    }

    public void KilledByPlayer(GameObject player, GameObject killed)
    {
        // Do Nothing
    }

    public void PlayerKilledBy(GameObject player, GameObject killer)
    {
        // Do Nothing
    }
}
