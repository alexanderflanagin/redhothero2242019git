using UnityEngine;
using System.Collections;

public class TutorialStageManager : MonoBehaviour
{
    private EnergyManager _energyManager;

	// Use this for initialization
	protected void Start ()
    {
        StartCoroutine(setStartEnergy());

        GameObject tutorialEnergyMeterHUD = Instantiate(Resources.Load("GUI/HUD/TutorialNoEnergy")) as GameObject;

        //Debug.Log(tutorialEnergyMeterHUD);

        // tutorialEnergyMeterHUD.transform.parent = HUDManager.current.energyMeterHUD.transform;

        tutorialEnergyMeterHUD.transform.SetParent(HUDManager.current.energyMeterHUD.transform, false);
	}

    private IEnumerator setStartEnergy()
    {
        while ((_energyManager = GameObject.FindObjectOfType<EnergyManager>()) == null)
        {
            yield return null;
        }

        //Debug.Log("Energy manager és: " + _energyManager);

        _energyManager.energy = EnergyManager.POINTS_PER_BATTERY + EnergyManager.PER_TIME_DRAIN_POINTS;
    }
}
