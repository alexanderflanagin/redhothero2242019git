using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FightResultManager : MonoBehaviour {

    /// <summary>
    /// Label que mostra la informació
    /// </summary>
    public Text labelText;

    /// <summary>
    /// Agafa la informació de la fight del GameManager
    /// </summary>
	private FightData _fightData;

	// Use this for initialization
	void Start () {

		// Check de seguretat per a no tenir arrays buits
        //if( GameManager.Instance.currentGameMode != GameManager.GameMode.MultiplayerFight ) {
        //    GameManager.Instance.SetGameMode( GameManager.GameMode.MultiplayerFight );
        //}

		_fightData = GameManager.Instance.fightData;

		labelText.text = "";

		for( int i = 0; i < _fightData.lastDeaths.Length; i++ ) {
			labelText.text += "\nPlayer " + ( i + 1 );
			if( _fightData.lastWinner == i ) {
				labelText.text += " - WINNER!!";
			}

			labelText.text += "\n\tdeaths: " + _fightData.lastDeaths[i] + " - Total wins: " + _fightData.wins[i] + " - Total deaths: " + _fightData.deaths[i];
		}
	}
}
