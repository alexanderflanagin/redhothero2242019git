using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

public class ProjectilesManager : Singleton<ProjectilesManager>
{
    public GameObject BlastGlovesBasic;

    public GameObject EnemyBasic;

    /// <summary>
    /// Enum amb tots els tipus d'enemics
    /// </summary>
    public enum Projectiles
    {
        BlastGlovesBasic,
        EnemyBasic
    }

    /// <summary>
    /// Llista pùblica d'enemics ordenada segons el enum Enemies
    /// </summary>
    public List<GameObject> projectiles;

    /// <summary>
    /// We will save a dictionary of transforms to get faster the projectiles already instantiated
    /// </summary>
    private Dictionary<Transform, Projectile> projectilesParticlesDictionary;

    /// <summary>
    /// Event que es dispara quan la llista està plena, per a que tothom pugui agafar els GameObjects necessaris llavors
    /// </summary>
    private event Action ListReady;

    /// <summary>
    /// Ens diu si ja s'ha fet el ListReady
    /// </summary>
    private bool listReadyDone = false;

    /// <summary>
    /// Make it don't destroyable
    /// </summary>
    void Awake()
    {
        DontDestroyOnLoad(this);
    }

    /// <summary>
    /// Ens hem d'esperar a l'Start perquè abans no ha inicialitzat les referencies dels GameObjects
    /// Al fer Start, crea la llista i crida a l'esdeveniment ListReady per a que tothom que s'hi hagi subscrit pugui agafar el seu GameObject inicialitzat
    /// </summary>
    void Start()
    {
        projectiles = new List<GameObject>();

        int position;
        GameObject projectileObject;

        //Debug.Log("Game Object: " + BlastGlovesBasic, this.gameObject);

        foreach (Projectiles projectile in Enum.GetValues(typeof(Projectiles)))
        {
            position = (int)projectile;

            if (this.GetType().GetField(Enum.GetName(typeof(Projectiles), projectile)) != null)
            {
                projectileObject = (GameObject)this.GetType().GetField(Enum.GetName(typeof(Projectiles), projectile)).GetValue(this);
                //Debug.Log("El camp no és null: " + projectileObject, this.gameObject);
            }
            else
            {
                //Debug.Log("El camp és null", this.gameObject);
                projectileObject = null;
            }

            projectiles.Insert(position, projectileObject);


            //Debug.Log(string.Format("Projectile {0} added at position {1}: {2}", Enum.GetName(typeof(Projectiles), projectile), position, projectiles[position]), this.gameObject);
        }

        // Diem que ja esta apunt
        listReadyDone = true;
        if (ListReady != null)
        {
            ListReady();
        }
    }

    /// <summary>
    /// Allows us to add projectiles (or call them) even is the list is not prepared yet (subscribing them to an Action)
    /// </summary>
    /// <param name="actionToExecute">Action to execute when list is ready</param>
    public void ExecuteOnListReady(Action actionToExecute)
    {
        if(!listReadyDone)
        {
            ListReady += actionToExecute;
        }
        else
        {
            actionToExecute();
        }
    }

    /// <summary>
    /// Adds a projectile in the dictionary
    /// </summary>
    /// <param name="trans">The Transform of the projectile</param>
    /// <param name="proj">The Projectile component of the projectile</param>
    public void AddProjectileOnDictionary(Transform trans, Projectile proj)
    {
        if(projectilesParticlesDictionary == null)
        {
            projectilesParticlesDictionary = new Dictionary<Transform, Projectile>();
        }

        projectilesParticlesDictionary.Add(trans, proj);
    }

    /// <summary>
    /// Get a Projectile from the dictionary based on its transform
    /// </summary>
    /// <param name="trans">The transform of the projectile</param>
    /// <returns>Te component Projectile of the projectile</returns>
    public Projectile GetFromProjectileDictionary(Transform trans)
    {
        if(projectilesParticlesDictionary == null)
        {
            Debug.LogWarning("Trying to get a projectle and the dictionary is not already created", gameObject);
            return null;
        }

        if (!projectilesParticlesDictionary.ContainsKey(trans))
        {
            Debug.LogWarning("Trying to get a projectile not existing in the dictionary", gameObject);
            return null;
        }

        return projectilesParticlesDictionary[trans];
    }
}
