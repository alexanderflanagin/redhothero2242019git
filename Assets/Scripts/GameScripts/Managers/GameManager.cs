using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Classe que s'encarrega de tot el funcionament global del joc
/// </summary>
public class GameManager : Singleton<GameManager>
{
    #region Singletons
    /// <summary>
    /// Audio Manager
    /// </summary>
    public AudioManager audioManager;
    #endregion

    /// <summary>
    /// Enviro al que juguem actualment
    /// </summary>
    public EnviroData.EnviroType currentEnviro = EnviroData.EnviroType.Coast;

    /// <summary>
    /// String que defineix la sortida del programa si coincideix amb el loadLevelParams[0]
    /// </summary>
    public static string exitName = "Exit";

    /// <summary>
    /// El GameObject del player que mantindrem actiu
    /// </summary>
    public GameObject redHotHero;

    /// <summary>
    /// Informacio del player
    /// </summary>
    public PlayerDataHolder playerData;

    /// <summary>
    /// Index del nivell al que ens trobem actualmetn
    /// </summary>
    public string currentLevelName;

    public CheckPointProperties checkPointProperties = new CheckPointProperties();

    /// <summary>
    /// Per a saber si ens hem passat la pantalla
    /// </summary>
    public bool levelComplete;

    /// <summary>
    /// Objecte que guardara la configuracio de la fight (per sessio)
    /// </summary>
    public FightData fightData;

    /// <summary>
    /// La ultima frase que s'ha mostrat al loading manager. Per a no repetir-la
    /// </summary>
    public string loadingManagerLastSentence = "";

    /// <summary>
    /// Escrow in net players playing
    /// </summary>
#pragma warning disable 414
    private int playersInNet;
#pragma warning restore 414

    /// <summary>
    /// Objecte amb totes les variables globals de data del joc
    /// </summary>
    public GameDataHolder gameData;

    /// <summary>
    /// Constructor protected, per a que no pugui ser accedit des de fora
    /// </summary>
    protected GameManager() { }

#pragma warning disable 414
    private GameManagerConsoleCommand commands;
#pragma warning disable 414

    /// <summary>
    /// Classe per a afegir shortcuts a 4 botons: creueta del mando i CVBN
    /// La faig publica per a poder modificar-la en un futur des de la consola
    /// </summary>
    public ShortcutKeys shortcutKeys;

    /// <summary>
    /// Al crear el GameObject, el posem a don't destroy on load, deshabilitem el cursor, carreguem les dades de configuració i els singletons
    /// </summary>
    void Awake()
    {
        DontDestroyOnLoad(this.gameObject);

        PrepareInput();

        gameData = Resources.Load("Data/EditorConfig") as GameDataHolder;

        LoadLevelsData();

        Instantiate(Resources.Load("EnemiesManager"));
        Instantiate(Resources.Load("ProjectilesManager"));
        Instantiate(Resources.Load("GeneralPurposeObjectManager"));

        // Carreguem el player i la seva info
        LoadPlayerData();
        GameObject instantiatedRedHotHero = GameObject.FindGameObjectWithTag(Tags.Player);
        if (instantiatedRedHotHero != null)
        {
            Debug.LogWarning("We are using an in-scene Red Hot Hero. ==== JUST FOR DEVELOP PURPOSE ====");
            redHotHero = instantiatedRedHotHero;
        }
        else
        {
            redHotHero = Instantiate(Resources.Load("RedHotHero/RedHotHero")) as GameObject;
            redHotHero.SetActive(false);
        }

        // Carreguem els singletons i els deixem preparats per a futurs canvis d'escena
        LoadSingletons();
        SceneManager.sceneLoaded += OnSceneLoaded;

        playersInNet = 3;

        GameObject pooler = new GameObject("Pooler");
        pooler.AddComponent<GameObjectPooler>();

        // Preparem la consola?
        if (gameData.gameData.globalCheatsEnabled)
        {
            PrepareCommands();
        }

        Application.targetFrameRate = 60;
    }

    void LoadLevelsData()
    {
        this.gameData.sceneNameBasedStagesList = new Dictionary<string, LevelData>();
        
        for (int i = 0; i < this.gameData.stagesData.Length; i++)
        {
            this.gameData.stagesData[i].ID = i; // Assignem el seu ID per odre d'aparició

			if(gameData.sceneNameBasedStagesList.ContainsKey(gameData.stagesData[i].sceneName))
			{
				Debug.Log("Duplicated scene: " + gameData.stagesData[i].sceneName);
			}
			else
			{
				this.gameData.sceneNameBasedStagesList.Add(this.gameData.stagesData[i].sceneName, this.gameData.stagesData[i]);
			}
        }
    }

    /// <summary>
    /// Prepara la consola i els shortcutKeys
    /// </summary>
    private void PrepareCommands()
    {
        if (GeneralPurposeObjectManager.Instance.console)
        {
            GameObject console = GameObject.Instantiate(GeneralPurposeObjectManager.Instance.console) as GameObject;
            console.transform.parent = this.transform;
        }

        // Console commands
        commands = new GameManagerConsoleCommand();

        // Helper Keys
        shortcutKeys = new ShortcutKeys();
    }

    /// <summary>
    /// TODO Això podrà ser un mànager apart
    /// </summary>
    void PrepareInput()
    {
        // Permetem duplicats per al crouch
        cInput.allowDuplicates = true;

        // Botons del menu
        cInput.SetKey(InputButton.MENU_UP, Keys.UpArrow);
        cInput.SetKey(InputButton.MENU_DOWN, Keys.DownArrow);
        cInput.SetAxis(InputButton.MENU_VERTICAL_AXIS, InputButton.MENU_DOWN, InputButton.MENU_UP);
        cInput.SetKey(InputButton.MENU_LEFT, Keys.LeftArrow);
        cInput.SetKey(InputButton.MENU_RIGHT, Keys.RightArrow);
        cInput.SetAxis(InputButton.MENU_HORIZONTAL_AXIS, InputButton.MENU_LEFT, InputButton.MENU_RIGHT);
        cInput.SetKey(InputButton.MENU_ENTER, Keys.Enter);
        cInput.SetKey(InputButton.MENU_BACK, Keys.Backspace, Keys.Xbox1B);

#if UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX
		// Generals
        cInput.SetKey(InputButton.RESTART, Keys.Backspace, Keys.Xbox1Back);
		cInput.SetKey(InputButton.PAUSE, Keys.Space, Keys.Xbox1Start);

        // Player Solo
        cInput.SetKey(InputButton.LEFT, Keys.A, Keys.Xbox1LStickLeft);
        cInput.SetKey(InputButton.RIGHT, Keys.D, Keys.Xbox1LStickRight);
        cInput.SetAxis(InputButton.HORIZONTAL_AXIS, InputButton.LEFT, InputButton.RIGHT);

        cInput.SetKey(InputButton.UP, Keys.W, Keys.Xbox1LStickUp);
        cInput.SetKey(InputButton.DOWN, Keys.S, Keys.Xbox1LStickDown);
        cInput.SetAxis(InputButton.VERTICAL_AXIS, InputButton.DOWN, InputButton.UP);

        cInput.SetKey(InputButton.JUMP, Keys.K, Keys.Xbox1A);
        //cInput.SetKey(InputButton.SPRINT, Keys.RightShift, Keys.Xbox1TriggerRight);
        cInput.SetKey(InputButton.PUNCH, Keys.L, Keys.Xbox1B);
        cInput.SetKey(InputButton.SHOOT, Keys.J, Keys.Xbox1X);
        cInput.SetKey(InputButton.TRAVEL, Keys.I, Keys.Xbox1Y);
        cInput.SetKey(InputButton.TRAVEL + InputButton.ALT, Keys.N, Keys.Xbox1TriggerRight);
        cInput.SetKey(InputButton.ROLL, Keys.S, Keys.Xbox1BumperLeft);
        cInput.SetKey(InputButton.BRAKE, Keys.LeftShift, Keys.Xbox1TriggerLeft);
	
#else
        // Generals
        cInput.SetKey(InputButton.RESTART, Keys.Backspace, Keys.Xbox1Back);
		cInput.SetKey(InputButton.PAUSE, Keys.Space, Keys.Xbox1Start);

        // Player Solo
        cInput.SetKey(InputButton.LEFT, Keys.A, Keys.Xbox1LStickLeft);
        cInput.SetKey(InputButton.RIGHT, Keys.D, Keys.Xbox1LStickRight);
        cInput.SetAxis(InputButton.HORIZONTAL_AXIS, InputButton.LEFT, InputButton.RIGHT);

        cInput.SetKey(InputButton.UP, Keys.W, Keys.Xbox1LStickUp);
        cInput.SetKey(InputButton.DOWN, Keys.S, Keys.Xbox1LStickDown);
        cInput.SetAxis(InputButton.VERTICAL_AXIS, InputButton.DOWN, InputButton.UP);

        cInput.SetKey(InputButton.JUMP, Keys.K, Keys.Xbox1A);
        //cInput.SetKey(InputButton.SPRINT, Keys.RightShift, Keys.Xbox1TriggerRight);
        cInput.SetKey(InputButton.PUNCH, Keys.L, Keys.Xbox1B);
        cInput.SetKey(InputButton.SHOOT, Keys.J, Keys.Xbox1X);
        cInput.SetKey(InputButton.TRAVEL, Keys.I, Keys.Xbox1Y);
        cInput.SetKey(InputButton.TRAVEL + InputButton.ALT, Keys.N, Keys.Xbox1TriggerRight);
		cInput.SetKey(InputButton.ROLL, Keys.S, Keys.Xbox1BumperLeft);
        cInput.SetKey(InputButton.BRAKE, Keys.LeftShift, Keys.Xbox1TriggerLeft);
#endif
        cInput.deadzone = 0.5f;

        cInput.SetAxisDeadzone(InputButton.HORIZONTAL_AXIS, 0.01f);
        cInput.SetAxisSensitivity(InputButton.HORIZONTAL_AXIS, 24f);
        cInput.SetAxisGravity(InputButton.HORIZONTAL_AXIS, 24f);

        cInput.SetAxisDeadzone(InputButton.VERTICAL_AXIS, 0.01f);
        cInput.SetAxisSensitivity(InputButton.VERTICAL_AXIS, 24f);
        cInput.SetAxisGravity(InputButton.VERTICAL_AXIS, 24f);
    }

    //void OnEnable()
    //{
    //    Debug.Log("GameManager enabled", this.gameObject);
    //}

    //void OnDisable()
    //{
    //    Debug.Log("GameManager disabled", this.gameObject);
    //}

    /// <summary>
    /// Quan es carrega un nou nivell, carreguem els singletons
    /// </summary>
    void OnSceneLoaded(Scene previousScene, LoadSceneMode mode)
    {
        //Debug.Log("On level was loaded", this.gameObject);
        LoadSingletons();
    }

    /// <summary>
    /// Al carregar/recarregar nivells, carreguem els singletons "mànager" i els d'audio
    /// </summary>
    void LoadSingletons()
    {
        // Mirem si ja tenim el d'escena i sino en creem un
        //AudioManager[] otherAudioManagers = FindObjectsOfType<AudioManager>();
        //for (int i = 0; i < otherAudioManagers.Length; i++)
        //{
        //    if (otherAudioManagers[i] != audioManager)
        //    {
        //        Destroy(otherAudioManagers[i].gameObject);
        //    }
        //}

        if (audioManager == null)
        {
            GameObject audioManagerObj = GameObject.Instantiate(Resources.Load("AudioManager")) as GameObject;
            audioManagerObj.name = "Audio Manager - Game Manager";
            audioManager = audioManagerObj.GetComponent<AudioManager>();
        }

        DontDestroyOnLoad(audioManager.gameObject);
    }

    /// <summary>
    /// S'encarrega de carregar un nou nivell, tenint en compte la informació que es passa (no només el nom del nivell).
    /// <remarks>
    /// Els paràmetres venen separats per "|".
    /// Poden ser "Exit", "Reload", "MainMenu", "EnviroMap" i "Scenario".
    /// </remarks>
    /// - Si el primer paràmetre és "Exit", es tanca l'aplicació.
    /// - Si el primer paràmetre és "Reload", es torna a carregar el mateix nivell que hi ha actualment.
    /// - Si el primer paràmetre és "MainMenu", es carrega el menú principal.
    /// - Si el primer paràmetre és "EnviroMap", es carrega el mapa d'enviros.
    /// - Si el primer paràmetre és "Scenario", es carrega l'escenario amb el nom del segon paràmetre.
    /// </summary>
    /// <param name="newLevel">String amb informació del nivell que s'ha de carregar</param>
    public void LoadLevel(string newLevel)
    {
        string[] loadLevelParams = newLevel.Split('|');

        if (loadLevelParams[0] == exitName)
        {
            Application.Quit();
        }
        else if (loadLevelParams[0] == "Reload")
        {
            loadLevelParams[0] = SceneManager.GetActiveScene().name;
        }
        else if (loadLevelParams[0] == "MainMenu")
        {
            LoadPlayerData();
        }

        //if(loadLevelParams[0] != "MainMenu" && loadLevelParams[0] != SceneManager.GetActiveScene().name)
        if (loadLevelParams[0] != "MainMenu")
        {
            currentLevelName = loadLevelParams[0];
            SceneManager.LoadScene("Loading");
        }
        else
        {
            SceneManager.LoadScene(loadLevelParams[0]);
        }
    }

    /// <summary>
    /// Save the PlayerData var in one file
    /// </summary>
    public void SavePlayerData(PlayerStageData playerLevelData)
    {
        string activeSceneName = SceneManager.GetActiveScene().name;

        if (!this.playerData.levelsData.ContainsKey(activeSceneName))
        {
            ConsoleLog.Instance.Log(">>>> Saving for first time level data for level " + activeSceneName);
            this.playerData.levelsData.Add(activeSceneName, new PlayerStageData());
            this.playerData.levelsData[activeSceneName].bestTime = playerLevelData.bestTime;
            this.playerData.levelsData[activeSceneName].pickeablesPicked = new bool[playerLevelData.pickeablesPicked.Length];
        }
        else
        {
            ConsoleLog.Instance.Log(">>>> Updating level data for level " + activeSceneName);
        }

        this.playerData.levelsData[activeSceneName].finished = true;

        // Actualitzem el millor temps?
        if (this.playerData.levelsData[activeSceneName].bestTime > playerLevelData.bestTime)
        {
            this.playerData.levelsData[activeSceneName].bestTime = playerLevelData.bestTime;
        }

        if (!this.playerData.levelsData[activeSceneName].speedrunDone && playerLevelData.speedrunDone)
        {
            ConsoleLog.Instance.Log(">>>> Speed run for fist time " + activeSceneName);
            this.playerData.totalCells++;
        }
        this.playerData.levelsData[activeSceneName].speedrunDone = this.playerData.levelsData[activeSceneName].speedrunDone || playerLevelData.speedrunDone;

        // Actualitzem les cells i les fuse parts
        for (int i = 0; i < playerLevelData.pickeablesPicked.Length; i++)
        {
            //Debug.Log(i + " - " + this.playerData.levelsData[Application.loadedLevelName].pickeablesPicked[i] + " - " + playerLevelData.pickeablesPicked[i]);           
            if (!this.playerData.levelsData[activeSceneName].pickeablesPicked[i] && playerLevelData.pickeablesPicked[i])
            {
                ConsoleLog.Instance.Log(">>>> First time item picked i: " + i);
                // L'hem agafat per primer cop
                if (i == (int)StandardStageManager.current.levelData.pickeablesOrder)
                {
                    // Hem agafat un fuse bitxes!
                    this.playerData.totalFuseParts++;
                    ConsoleLog.Instance.Log(">>>> Fuse added");
                }
                else
                {
                    // Hem agafat una cell
                    this.playerData.totalCells++;
                    ConsoleLog.Instance.Log(">>>> Cell added");
                }
            }

            this.playerData.levelsData[activeSceneName].pickeablesPicked[i] = this.playerData.levelsData[activeSceneName].pickeablesPicked[i] || playerLevelData.pickeablesPicked[i];
        }

        UnlockNextStage();

        this.playerData.Save();
    }

    /// <summary>
    /// Desbloqueja el següent stage (en cas de que existeixi)
    /// Si la pantalla no està a la llista de Enviro/Stages, vol dir que era una pantalla secreta i no desbloqueja res
    /// </summary>
    void UnlockNextStage()
    {
        int currentStage;

        //Debug.Log("Id de la pantalla: " + StandardStageManager.current.levelData.ID);

        for (currentStage = 0; currentStage < 10; currentStage++)
        {
            if (StandardStageManager.current.levelData.sceneName == GameManager.Instance.gameData.stagesData[GameManager.Instance.gameData.stagesOrdered[(int)GameManager.Instance.currentEnviro].stages[currentStage]].sceneName)
            {
                // És el nuvell actual
                Debug.Log("El nivell actual es el: " + currentStage);
                break;
            }
        }

        if (currentStage >= 9)
        {
            // Ens hem passat l'enviro. Canviem el current enviro
            switch (this.currentEnviro)
            {
                case EnviroData.EnviroType.Coast:
                    this.currentEnviro = EnviroData.EnviroType.Jungle;
                    break;
                case EnviroData.EnviroType.Jungle:
                    this.currentEnviro = EnviroData.EnviroType.Lab;
                    break;
                case EnviroData.EnviroType.Lab:
                    Debug.Log("T'has passat el joc. Màquina.", this.gameData);
                    break;
            }

            // L'actual current stage es el 0 del nou enviro (-1 perquè hi sumarem un 1 més tard)
            currentStage = -1;
        }

        // Guay, seguim en el mateix enviro
        // GameManager.Instance.gameData.levelsData[GameManager.Instance.gameData.enviroData[(int)GameManager.Instance.currentEnviro].stages[currentStage + 1]].
        string name = "";

        name = this.gameData.stagesData[this.gameData.stagesOrdered[(int)this.currentEnviro].stages[currentStage + 1]].sceneName;

        PlayerStageData newStagePlayerLevelData = new PlayerStageData();

        if (this.playerData.levelsData.TryGetValue(name, out newStagePlayerLevelData))
        {
            newStagePlayerLevelData.unlocked = true;
            this.playerData.levelsData[name] = newStagePlayerLevelData;
        }
        else
        {
            newStagePlayerLevelData = new PlayerStageData();
            newStagePlayerLevelData.unlocked = true;
            this.playerData.levelsData.Add(name, newStagePlayerLevelData);
        }
    }

    /// <summary>
    /// Load the PlayerData var
    /// </summary>
    public void LoadPlayerData()
    {
        //Debug.Log("Carreguem el player data", this.gameObject);

        this.playerData = ScriptableObject.CreateInstance(typeof(PlayerDataHolder)) as PlayerDataHolder;
        this.playerData.Load();
    }

    void Update()
    {
        if(gameData.gameData.globalCheatsEnabled)
        {
            if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.R))
            {
                GameManager.Instance.checkPointProperties = null;
                SceneManager.LoadScene(0);
            }

            if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.Alpha1))
            {
                GameManager.Instance.checkPointProperties = null;
                GameManager.Instance.LoadLevel("Scenario|" + GameManager.Instance.gameData.stagesData[GameManager.Instance.gameData.stagesOrdered[0].stages[0]].sceneName);
            }

            if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.Alpha2))
            {
                GameManager.Instance.checkPointProperties = null;
                GameManager.Instance.LoadLevel("Scenario|" + GameManager.Instance.gameData.stagesData[GameManager.Instance.gameData.stagesOrdered[0].stages[1]].sceneName);
            }

            if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.Alpha3))
            {
                GameManager.Instance.checkPointProperties = null;
                GameManager.Instance.LoadLevel("Scenario|" + GameManager.Instance.gameData.stagesData[GameManager.Instance.gameData.stagesOrdered[0].stages[2]].sceneName);
            }

            if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.Alpha4))
            {
                GameManager.Instance.checkPointProperties = null;
                GameManager.Instance.LoadLevel("Scenario|" + GameManager.Instance.gameData.stagesData[GameManager.Instance.gameData.stagesOrdered[0].stages[3]].sceneName);
            }

            if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.Alpha5))
            {
                GameManager.Instance.checkPointProperties = null;
                GameManager.Instance.LoadLevel("Scenario|" + GameManager.Instance.gameData.stagesData[GameManager.Instance.gameData.stagesOrdered[0].stages[4]].sceneName);
            }

            if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.E))
            {
                if (StandardStageManager.current)
                {
                    if (FindObjectOfType<FinishLine>() != null)
                    {
                        StandardStageManager.current.player.transform.position = FindObjectOfType<FinishLine>().transform.position;
                    }
                }
            }

            shortcutKeys.Update();
        }
    }
}



