using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/// <summary>
/// S'encarrega de gestionar la carrega asincrona de nivells amb una pantalla amb tips
/// </summary>
public class LoadingManager : MonoBehaviour
{
    /// <summary>
    /// Usamos esta variable para mostrar la pantalla de carga un tiempo minimo
    /// </summary>
    public float loadingMinTime = 0.5f;
    
    /// <summary>
    /// Percent que portem carregat. De moment no el mostrem
    /// </summary>
    public Text percent;

    /// <summary>
    /// Text amb el tip que es mostrara
    /// </summary>
    public Text text;

    /// <summary>
    /// GameObject que mostrem quan ja podem premer un botó per a continuar
    /// </summary>
    public GameObject pressToContinue;

    /// <summary>
    /// GameObject que mostrem mentre s'esta carregant
    /// </summary>
    public GameObject loading;

    public GameObject[] objectsToDestroy;

    /// <summary>
    /// Operacio de carrega asincrona. Per a poder-la acabar quan premem el boto
    /// </summary>
    AsyncOperation asyncOperation;

    /// <summary>
    /// Llistat de frases que es poden fer servir i amb quines condicions
    /// </summary>
    public SentenceInfo[] sentences;

    /// <summary>
    /// Prepara una llista amb les possibles frases i en tria una a l'atzar. Comen�a a carregar de manera asincrona el nivell
    /// </summary>
    void Start()
    {
        pressToContinue.SetActive(false);
        loading.SetActive(true);

        asyncOperation = SceneManager.LoadSceneAsync(GameManager.Instance.currentLevelName, LoadSceneMode.Single);
        asyncOperation.allowSceneActivation = false;

        List<string> currentSentences = new List<string>();

        for(int i = 0; i < sentences.Length; i++)
        {
            if(sentences[i].sentence == GameManager.Instance.loadingManagerLastSentence || sentences[i].afterPowerGlove && !GameManager.Instance.playerData.powerGlove || sentences[i].afterBlastGlove && !GameManager.Instance.playerData.blastGlove)
            {
                // Repetimos frase o no tiene poderes necesarios, pasamos
                continue;
            }

            currentSentences.Add(sentences[i].sentence);
            
            if(sentences[i].makeItCommon)
            {
                currentSentences.Add(sentences[i].sentence);
            }
        }

        text.text = currentSentences[Random.Range(0, currentSentences.Count)];

        GameManager.Instance.loadingManagerLastSentence = text.text;

        StartCoroutine(LoadNextScene());
    }

    private IEnumerator LoadNextScene()
    {
        while (loadingMinTime > Time.timeSinceLevelLoad)
        {
            yield return null;
        }

        //Debug.Log("Min time waited");

        while(asyncOperation.progress < 0.8999f)
        {
            yield return null;
        }

        //Debug.Log("Progress complete");

        pressToContinue.SetActive(true);
        loading.SetActive(false);

        while(!cInput.anyKeyDown)
        {
            yield return null;
        }

        //Debug.Log("Any key down");

        asyncOperation.allowSceneActivation = true;

        pressToContinue.SetActive(false);

        //while (!asyncOperation.isDone)
        //{
        //    yield return null;
        //}

        //Debug.Log("Operation isDone!");

        //yield return null;

        //Scene activeScene = SceneManager.GetActiveScene();
        //Scene nextScene = SceneManager.GetSceneByName(GameManager.Instance.currentLevelName);
        //if (nextScene.IsValid())
        //{
        //    SceneManager.SetActiveScene(nextScene);
        //    //SceneManager.UnloadSceneAsync(activeScene);
        //}

        //for (int i = 0; i < objectsToDestroy.Length; i++)
        //{
        //    objectsToDestroy[i].SetActive(false);
        //}
    }
}

/// <summary>
/// Classe per a gestionar quina info tenen les frases que es mostren
/// </summary>
[System.Serializable]
public class SentenceInfo
{
    /// <summary>
    /// La frase que ha de sortir
    /// </summary>
    public string sentence;

    /// <summary>
    /// Si no surt fins que no tens el power glove
    /// </summary>
    public bool afterPowerGlove;

    /// <summary>
    /// Si no surt fins que no tens el blast glove
    /// </summary>
    public bool afterBlastGlove;

    /// <summary>
    /// Duplicamos probabilidades de que salga
    /// </summary>
    public bool makeItCommon;
}
