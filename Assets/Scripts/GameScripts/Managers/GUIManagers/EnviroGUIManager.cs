using UnityEngine;
using UnityEngine.UI;
using System.Collections;

/// <summary>
/// Gestiona la GUI del enviro map
/// </summary>
public class EnviroGUIManager : GUIManager {

    /// <summary>
    /// Stores the current EnviroGUIManager (if it exists)
    /// </summary>
    public static EnviroGUIManager current;

	/// <summary>
    /// Coins
	/// </summary>
	private Image[] _coins;

	/// <summary>
    /// Guardem la imatge d'una coin agafada
	/// </summary>
	private Sprite _coinPicked;

	/// <summary>
    /// Guardem la imatge d'una coin no agafada
	/// </summary>
    private Sprite _coinUnicked;

	/// <summary>
    /// Mostra en quin numero de pantalla estas
	/// </summary>
	private Text _stageNum;

    /// <summary>
    /// UILabel amb el text del challenge
    /// </summary>
    private Text _challengeText;

    /// <summary>
    /// S'executa quan es crea el GameObject
    /// </summary>
	void Awake() {
		_root = FindObjectOfType<Canvas>();

		// Agafem les monedes
        _coins = new Image[3];
        _coins[0] = _root.transform.Find("Container/DetailsFront/Coins/Coin0").GetComponent<Image>();
        _coins[1] = _root.transform.Find("Container/DetailsFront/Coins/Coin1").GetComponent<Image>();
        _coins[2] = _root.transform.Find("Container/DetailsFront/Coins/Coin2").GetComponent<Image>();

		// Aprofitem per a pillar les textures de picked o no
		_coinPicked = _coins[0].sprite;
		_coinUnicked = _coins[1].sprite;

		_stageNum = _root.transform.Find("Container/DetailsFront/StageNumLabel").GetComponent<Text>();

        //_challengeText = _root.transform.FindChild("Container/DetailsFront/ChallengeTxt").GetComponent<Text>();
	}

    /// <summary>
    /// S'executa al habilitar el GameObject
    /// </summary>
    void OnEnable()
    {
        EnviroGUIManager.current = this;
    }

    /// <summary>
    /// Actualitza la informaci� que mostra el HUD amb la de la pantalla que es passa per par�metre
    /// </summary>
    /// <param name="levelData">Informaci� del level que es vol mostrar</param>
	public void UpdateInfo( LevelData levelData ) {
		_stageNum.text = levelData.name;
        //_challengeText.text = levelData.challenge;

		for( int i = 0; i < 3; i++ ) {
			bool collected;
            //if( GameManager.Instance.playerData.coins.TryGetValue( levelData.coins[i].skin + "" + levelData.coins[i].order, out collected ) ) {
            //    _coins[i].sprite = _coinPicked;
            //} else {
            //    // _coins[i].mainTexture = _coinUnicked;
            //}
		}
	}
}
