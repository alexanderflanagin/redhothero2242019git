using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Collections;


public class HUDManager : GUIManager {

    public static HUDManager current;

	/// <summary>
    /// Pause enter Event
	/// </summary>
    public static Action PauseEnter;

	/// <summary>
    /// Pause exit Event
	/// </summary>
    public static Action PauseExit;

	/// <summary>
    /// Indica si esta o no pausat el joc
	/// </summary>
	private bool paused;

    /// <summary>
    /// Si pot ser pausat el joc
    /// </summary>
    public bool canBePaused;

	/// <summary>
    /// Coins
	/// </summary>
	private Image[] _coins;

	/// <summary>
    /// Coins getter
	/// </summary>
    public Image[] coins
    {
        get
        {
            return _coins;
        }
    }

    /// <summary>
    /// Timer
    /// </summary>
    private Text _timer;

    private GameObject _GUIStageManager;

    public Camera GUICamera;

	/// <summary>
    /// Menu de pausa
	/// </summary>
	private GameObject _pauseHUD;

    /// <summary>
    /// Getter p�blic del men� de pausa
    /// </summary>
	public GameObject pauseHUD 
    {
        get
        {
            return _pauseHUD;
        }
    }

    public HUDPauseManager hudPauseManager; 

    /// <summary>
    /// GameObject que conté tot el HUD del timer
    /// </summary>
    private GameObject _timerHUD;

    /// <summary>
    /// Getter públic del /_timerHUD/
    /// </summary>
    public GameObject timerHUD
    {
        get
        {
            return _timerHUD;
        }
    }

    /// <summary>
    /// GameObject que conté tot el HUD de les coins
    /// </summary>
    private GameObject _coinsHUD;

    /// <summary>
    /// Getter públic del /_coinsHUD/
    /// </summary>
    public GameObject coinsHUD
    {
        get
        {
            return _coinsHUD;
        }
    }

    private GameObject _stageCompleteHUD;

    private GameObject _energyMeterHUD;

    public GameObject energyMeterHUD
    {
        get
        {
            return _energyMeterHUD;
        }
    }
    
    /// <summary>
    /// Sound played when pause enter
    /// </summary>
	public string pauseClip = "PauseMenu";
	

    void Awake()
    {
        this.canBePaused = true;

        string basePath = "GUI/HUD/";

        if (!GameObject.Find("GUICamera"))
        {
            GameObject GuiCameraObj = Instantiate(Resources.Load(basePath + "GUICamera")) as GameObject;
            this.GUICamera = GuiCameraObj.GetComponent<Camera>();
        }
        else
        {
            this.GUICamera = GameObject.Find("GUICamera").GetComponent<Camera>();
        }

        _GUIStageManager = GameObject.Find("GUIStageManager");

        if (!_GUIStageManager)
        {
            _GUIStageManager = Instantiate(Resources.Load(basePath + "GUIStageManager")) as GameObject;
            _GUIStageManager.GetComponent<Canvas>().worldCamera = this.GUICamera;
        }
        else
        {
            this._GUIStageManager.GetComponent<Canvas>().worldCamera = this.GUICamera;
        }

        _pauseHUD = _GUIStageManager.transform.Find("PauseHUD").gameObject;
        _pauseHUD.SetActive(false);

        _timerHUD = _GUIStageManager.transform.Find("TimerHUD").gameObject;

        _coinsHUD = _GUIStageManager.transform.Find("CoinsHUD").gameObject;

        _stageCompleteHUD = _GUIStageManager.transform.Find("StageCompleteHUD").gameObject;
        _stageCompleteHUD.SetActive(false);

        _energyMeterHUD = _GUIStageManager.transform.Find("EnergyMeterHUD").gameObject;
        //_energyMeterHUD.SetActive(false);

        // Ara agafem les variables concretes que hem de modificar
        _timer = _timerHUD.GetComponentInChildren<Text>();

        if (GameManager.Instance.checkPointProperties != null)
        {
            this._timer.text = GameManager.Instance.checkPointProperties.currentTime.ToString("0.00").Replace(".", ":");
        }
        else
        {
            this._timer.text = "0.00";
        }

        // Coins
        _coins = new Image[3];
        _coins[0] = _coinsHUD.transform.Find("Coin1").GetComponent<Image>();
        _coins[1] = _coinsHUD.transform.Find("Coin2").GetComponent<Image>();
        _coins[2] = _coinsHUD.transform.Find("Coin3").GetComponent<Image>();

        // _stageCompleteHUD = GetComponentInChildren<StageCompleteHUD>();

        hudPauseManager = GetComponentInChildren<HUDPauseManager>();

        if (UnityEngine.SceneManagement.SceneManager.GetActiveScene().name == "WorldMap")
        {
            this._timerHUD.SetActive(false);
            this._coinsHUD.SetActive(false);
        }
        else
        {
            this._GUIStageManager.transform.Find("ToastCoastEnviroLogo").gameObject.SetActive(false);
        }
    }

	void OnEnable() {
		HUDManager.current = this;

        HUDManager.PauseEnter += OnPauseEnter;
        HUDManager.PauseExit += OnPauseExit;
	}

	void OnDisable() {
        HUDManager.PauseEnter -= OnPauseEnter;
        HUDManager.PauseExit -= OnPauseExit;
	}

	void Update() {
        _timer.text = StandardStageManager.current.time.ToString("0.00").Replace(".", ":");

        if (cInput.GetButtonDown(InputButton.PAUSE) && this.canBePaused)
        {
            if (!paused)
            {
                HUDManager.PauseEnter();
                // paused = true;
            }
            else
            {
                HUDManager.PauseExit();
            }
        }
	}

    void OnPauseEnter()
    {
        paused = true;

        if (HUDManager.current != null)
        {
            AudioManager.Instance.PlayOneShoot(AudioClipsData.ClipsGroups.Enviro, pauseClip);
            HUDManager.current.pauseHUD.SetActive(true);
        }
    }

    void OnPauseExit()
    {
        paused = false;

        if (HUDManager.current != null && HUDManager.current.pauseHUD != null)
        {
            AudioManager.Instance.PlayOneShoot(AudioClipsData.ClipsGroups.Enviro, pauseClip);
            HUDManager.current.pauseHUD.SetActive(false);
        }
    }

    public void EnablePauseButton()
    {
        this.canBePaused = true;
    }

    public void DisablePauseButton()
    {
        this.canBePaused = false;
    }

    /// <summary>
    /// Activa el HUD de Stage Complete i crida la funció que activa l'animator
    /// </summary>
    public void StartStageCompleteHUD()
    {
        this._stageCompleteHUD.SetActive(true);
        this._stageCompleteHUD.GetComponent<StageCompleteHUD>().StartEndStage();
    }

    public IEnumerator CoinPicked(int index)
    {
        this.coins[index].sprite = StandardStageManager.current.cellPickedImage;
        this.coins[index].transform.localScale = Vector3.one * 8;
        yield return null;
        this.coins[index].transform.localScale = Vector3.one * 7f;
        yield return null;
        this.coins[index].transform.localScale = Vector3.one * 5f;
        yield return null;
        this.coins[index].transform.localScale = Vector3.one * 2f;
        yield return null;
        this.coins[index].transform.localScale = Vector3.one;
    }
}
