using UnityEngine;
using System.Collections;
using System;

public class SkinManager : Singleton<MonoBehaviour> {

	[Serializable]
	public enum Skins { Default = 0, Blue = 1, Green = 2, Black = 3 }

	struct Skin {
		public string name;
		public int numberOfPieces;
	}
}
