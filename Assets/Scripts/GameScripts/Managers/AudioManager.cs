using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;
using System;
using System.Collections;
using System.Collections.Generic;

public class AudioManager : Singleton<AudioManager>
{
    /// <summary>
    /// Values that audio mixer should have while we are playing
    /// </summary>
    public AudioMixerSnapshot defaultSnapshot;

    /// <summary>
    /// Values that audio mixer should have while we are in pause
    /// </summary>
    public AudioMixerSnapshot pauseSnapshot;
    
    /// <summary>
    /// Current playing theme, to know if we must change it
    /// </summary>
    private int currentTheme;

    /// <summary>
    /// Temps de transicio entre que es baixa el volum d'un audio i es puja el seguent
    /// </summary>
    private const float VOLUME_TRANSITION_TIME = 15;

    /// <summary>
    /// Acces directe al component d'AudioSource
    /// </summary>
    public AudioSource audioSource { get; set; }

    /// <summary>
    /// Data Holder amb tota la info de clips (per a poder assignar volums, etc.)
    /// </summary>
    public AudioClipsData audioClipsData;

    /// <summary>
    /// Dictionary amb els clips ordenats per nom
    /// </summary>
    private Dictionary<string, AudioClipInfo> audioClipsInfo;

    /// <summary>
    /// Dictionary with all the themes
    /// </summary>
    private Dictionary<string, AudioClipInfo> themes;

    /// <summary>
    /// Mida inicial de la pool d'audio sources que tindrem
    /// </summary>
    public const int audioPoolSize = 10;

    /// <summary>
    /// Llista d'audio sources que crearem a l'inici de l'execucio
    /// </summary>
    private List<AudioSource> audioSourcePool;

    void Awake()
    {
        audioSource = transform.GetOrAddComponent<AudioSource>();

        audioSource.spatialBlend = 0;

        PrepareAudioClipsDictionary();

        PreparePool();

        // Register in SceneLoaded action
        SceneManager.sceneLoaded += NewSceneLoaded;

        // And play music for first time
        NewSceneLoaded(SceneManager.GetActiveScene(), LoadSceneMode.Single);
    }

    /// <summary>
    /// Load scene theme. If the theme is the same as we are playing, we let it play
    /// </summary>
    /// <param name="scene">Scene loaded</param>
    /// <param name="mode">Mode (always single)</param>
    public void NewSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        LevelData level;
        if (GameManager.Instance.gameData.sceneNameBasedStagesList.TryGetValue(scene.name, out level))
        {
            if (level.theme != currentTheme)
            {
                currentTheme = level.theme;

                int id = 0;
                for (id = 0; id < audioClipsData.themes.Length; id++)
                {
                    if (audioClipsData.themes[id].ID == level.theme)
                    {
                        break;
                    }
                }

                StartCoroutine(SwitchAudioClip(audioClipsData.themes[id]));
            }
        }
    }

    /// <summary>
    /// Canvio de theme entre el que estigues sonant i el del Win Runs del Finish Line
    /// </summary>
    public void EnterOnFinishLine()
    {
        int finishLineThemeID = 9;

        currentTheme = finishLineThemeID;

        int id = 0;
        for (id = 0; id < audioClipsData.themes.Length; id++)
        {
            if (audioClipsData.themes[id].ID == finishLineThemeID)
            {
                break;
            }
        }

        StartCoroutine(SwitchAudioClip(audioClipsData.themes[id]));
    }

    /// <summary>
    /// Switch current Audio Clip with the newClip doing a fade
    /// </summary>
    /// <param name="newClip">New clip to play</param>
    /// <returns></returns>
    private IEnumerator SwitchAudioClip(AudioClipInfo newClip)
    {
        float currentVolume;

        if (audioSource.isPlaying)
        {
            currentVolume = audioSource.volume;

            for (float i = currentVolume; i > 0; i -= currentVolume / VOLUME_TRANSITION_TIME)
            {
                audioSource.volume = i;
                yield return null;
            }

            audioSource.Stop();
        }
        
        audioSource.clip = newClip.file;
        audioSource.priority = newClip.priority;
        audioSource.outputAudioMixerGroup = newClip.mixerGroup;
        audioSource.Play();
        
        currentVolume = newClip.volume;

        for (float i = 0; i < currentVolume; i += currentVolume / VOLUME_TRANSITION_TIME)
        {
            audioSource.volume = i;
            yield return null;
        }

        audioSource.volume = currentVolume;
    }

    /// <summary>
    /// Agafa tota la info de /audioCLipsData/ i la transforma en un diccionari
    /// </summary>
    private void PrepareAudioClipsDictionary()
    {
        audioClipsInfo = new Dictionary<string, AudioClipInfo>();
        
        for (int i = 0; i < audioClipsData.playerClips.Length; i++)
        {
            string key = AudioClipsData.playerPrefix + audioClipsData.playerClips[i].name;
            if (audioClipsInfo.ContainsKey(key))
            {
                Debug.LogError("Name " + audioClipsData.playerClips[i].name + " for file: " + audioClipsData.playerClips[i].file + " is already used by " + audioClipsInfo[audioClipsData.playerClips[i].name].file, this.gameObject);
                continue;
            }

            audioClipsInfo.Add(key, audioClipsData.playerClips[i]);
        }

        for (int i = 0; i < audioClipsData.enemiesClips.Length; i++)
        {
            string key = AudioClipsData.enemyPrefix + audioClipsData.enemiesClips[i].name;
            if (audioClipsInfo.ContainsKey(key))
            {
                Debug.LogError("Name " + audioClipsData.enemiesClips[i].name + " for file: " + audioClipsData.enemiesClips[i].file + " is already used by " + audioClipsInfo[audioClipsData.enemiesClips[i].name].file, this.gameObject);
                continue;
            }

            audioClipsInfo.Add(key, audioClipsData.enemiesClips[i]);
        }

        for (int i = 0; i < audioClipsData.enviroClips.Length; i++)
        {
            string key = AudioClipsData.enviroPrefix + audioClipsData.enviroClips[i].name;
            if (audioClipsInfo.ContainsKey(key))
            {
                Debug.LogError("Name " + audioClipsData.enviroClips[i].name + " for file: " + audioClipsData.enviroClips[i].file + " is already used by " + audioClipsInfo[audioClipsData.enviroClips[i].name].file, this.gameObject);
                continue;
            }

            audioClipsInfo.Add(key, audioClipsData.enviroClips[i]);
        }
    }

    /// <summary>
    /// Crea una pool de mida /audioPoolSize/ d'audio source
    /// </summary>
    private void PreparePool()
    {
        audioSourcePool = new List<AudioSource>();

        for (int i = 0; i < audioPoolSize; i++)
        {
            audioSourcePool.Add(CreateAudioSource());
        }
    }

    /// <summary>
    /// Crea un audioSource amb la configuracio estandard
    /// </summary>
    /// <returns></returns>
    private AudioSource CreateAudioSource()
    {
        GameObject g = new GameObject("AudioSource Object");
        g.transform.SetParent(transform);
        AudioSource newAudioSource = g.AddComponent<AudioSource>();
        newAudioSource.playOnAwake = false;

        return newAudioSource;
    }

    /// <summary>
    /// Get first unused audio source to play a sound. If no free audio source in the pool, creates a new one and add it to the list
    /// </summary>
    /// <returns>A free audio source</returns>
    private AudioSource GetFreeAudioSource()
    {
        for(int i = 0; i < audioSourcePool.Count; i++)
        {
            if(!audioSourcePool[i].isPlaying)
            {
                return audioSourcePool[i];
            }
        }

        AudioSource newAudioSource = CreateAudioSource();
        audioSourcePool.Add(newAudioSource);

        return newAudioSource;
    }

    /// <summary>
    /// Fa un PlayOneShoot al audioSource que li passem de parametre tenint en compte la configuracio del clip
    /// </summary>
    /// <param name="name">Nom del clip que volem que soni</param>
    public void PlayOneShoot(AudioClipsData.ClipsGroups group, string name)
    {
        name = AudioClipsData.GetClipFullName(group, name);

        if (audioClipsInfo.ContainsKey(name))
        {
            AudioClipInfo audioClipInfo = audioClipsInfo[name];
            AudioSource audioSource = GetFreeAudioSource();
   
            // Si estem fent servir el propi, no canviem aquests valors (per si les mosques)
            audioSource.outputAudioMixerGroup = audioClipInfo.mixerGroup;
            audioSource.priority = audioClipInfo.priority;
            
            // Si no ens diu on ha de sonar, es un so 2D
            audioSource.spatialBlend = 0;

            audioSource.PlayOneShot(audioClipInfo.file, audioClipInfo.volume);
        }
    }

    /// <summary>
    /// Fa un PlayOneShoot a un audioSource lliure en la posicio /position/
    /// </summary>
    /// <param name="name">Nom del clip que volem que soni</param>
    /// <param name="position">Posicio on ha de sonar</param>
    /// <param name="spatialBlend">Nivel de 3D que volem que tingui el so. Per defecte 1, que vol dir totalment 3D</param>
    public void PlayOneShootAt(string name, Vector3 position, float spatialBlend = 1f)
    {
        if (audioClipsInfo.ContainsKey(name))
        {
            AudioClipInfo audioClipInfo = audioClipsInfo[name];
            AudioSource audioSource = GetFreeAudioSource();

            // Si estem fent servir el propi, no canviem aquests valors (per si les mosques)
            audioSource.outputAudioMixerGroup = audioClipInfo.mixerGroup;
            audioSource.priority = audioClipInfo.priority;

            // Si ens diu posicio on ha de sonar, es un so 3D
            audioSource.spatialBlend = spatialBlend;
            audioSource.transform.position = position;

            audioSource.PlayOneShot(audioClipInfo.file, audioClipInfo.volume);
        }
    }

    /// <summary>
    /// Reprodueix un so mentre el que retorni la funcio /end/ sigui fals
    /// </summary>
    /// <param name="name">Nom del so a reproduir</param>
    /// <param name="end">Funcio que retorna un boolea que ens diu quan hem d'acabar</param>
    public void LongPlay(string name, Func<bool> end)
    {
        if(!audioClipsInfo.ContainsKey(name))
        {
            return;
        }

        AudioClipInfo audioCipInfo = audioClipsInfo[name];
        AudioSource audioSource = GetFreeAudioSource();

        audioSource.clip = audioCipInfo.file;
        audioSource.volume = audioCipInfo.volume;
        audioSource.loop = true;

        audioSource.Play();

        StartCoroutine(LongPlay(audioSource, end));
    }
    
    /// <summary>
    /// Coroutina per a saber quan acabar un LongPlay
    /// </summary>
    /// <param name="audioSource">source que haurem d'aturar</param>
    /// <param name="end">Funcio que ens retorna el boolea de quan hem de parar</param>
    /// <returns></returns>
    private IEnumerator LongPlay(AudioSource audioSource, Func<bool> end)
    {
        while (!end())
        {
            yield return null;
        }

        audioSource.Stop();
    }
}
