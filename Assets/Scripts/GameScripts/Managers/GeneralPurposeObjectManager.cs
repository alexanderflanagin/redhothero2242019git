using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

public class GeneralPurposeObjectManager : MonoBehaviour
{
    /// <summary>
    /// Instància del Object Manager per a recuperar objectes
    /// </summary>
    public static GeneralPurposeObjectManager Instance;

    [Header("Pickeables")]
    /// <summary>
    /// Game Object de la cell que s'instancia al cell spawn
    /// </summary>
    public GameObject cell;

    /// <summary>
    /// Game Object de la cell que s'instancia al cell spawn en cas de que ja es tingui la cell
    /// </summary>
    public GameObject cellPicked;

    /// <summary>
    /// Game Object de la fuse part que s'instancia al fuse part spawn
    /// </summary>
    public GameObject fusePart;

    /// <summary>
    /// Game Object de la fuse part que s'instancia al fuse part spawn en cas de que ja es tingui la fuse part
    /// </summary>
    public GameObject fusePartPicked;

    [Header("Densities")]
    /// <summary>
    /// GameObject que es mostra al crear un portal
    /// </summary>
    public GameObject RedHotDensity;

    /// <summary>
    /// GameObject que es mostra al crear un portal al tutorial
    /// </summary>
    public GameObject RedHotDensityTutorial;

    [Header("Trails")]
    /// <summary>
    /// Trail per defecte que deixen els objectes que han sigut colpejats
    /// </summary>
    public GameObject PowerGlovedDefaultTrail;

    [Header("Blue Substance")]
    public ParticleSystem BlueSubstanceCross;

    public ParticleSystem BlueSubstanceBreak;

    public ParticleSystem BlueSubstanceShoot;

    [Header("Utils")]
    public GameObject console;

    public GameObject achivementWindow;

    void Awake()
    {
        if(Instance)
        {
            Debug.LogWarning("Error ja existia un General Purpose Object Manager. Eliminem el nou");
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }
}
