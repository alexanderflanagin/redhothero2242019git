using UnityEngine;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class PlayerDataHolder : ScriptableObject
{
    /// <summary>
    /// Informacio de les pantalles jugades
    /// </summary>
    public Dictionary<string, PlayerStageData> levelsData = new Dictionary<string,PlayerStageData>();

    [Header("Batteries Settings")]
    /// <summary>
    /// Recompte de cells agafades
    /// </summary>
    [Range(0, 90)]
    public int totalCells;

    /// <summary>
    /// Fem cas a les cells que tenim, o a les batteries?
    /// </summary>
    [Tooltip("Si volem definir directament les bateries i no les cells")]
    public bool overrideCells;

    /// <summary>
    /// Batteries que ja té generades
    /// </summary>
    [Range(2, 20)]
    public int totalBatteries;

    [Header("Fuses Settings")]
    /// <summary>
    /// Parts de fuse que té
    /// </summary>
    [Range(0, 30)]
    public int totalFuseParts;

    /// <summary>
    /// Fem cas a les fuse parts que tenim o als fuses?
    /// </summary>
    [Tooltip("Si volem definir directament els fuses i no les fuse parts")]
    public bool overrideFuseParts;

    /// <summary>
    /// Fuses que ja té generats
    /// </summary>
    [Range(1, 4)]
    public int totalFuses;

    [Header("Improvements")]
    /// <summary>
    /// Si té el power glove
    /// </summary>
    public bool powerGlove;

    /// <summary>
    /// Si té el blast glove
    /// </summary>
    public bool blastGlove;

    [Header("Others")]
    /// <summary>
    /// La última pantalla que s'ha jugat
    /// </summary>
    public int lastPlayedStage;

    public int totalLeaflets;

    [Header("Temporals")]
    /// <summary>
    /// Si estem en aquest mode, no ens baixa l'energia
    /// </summary>
    public bool godMode;

    public PlayerDataHolder()
    {
    }

    public int GetBatteries()
    {
        if (this.overrideCells)
        {
            return this.totalBatteries;
        }
        else
        {
            return EnergyManager.MIN_BATTERIES + Mathf.FloorToInt(this.totalCells / EnergyManager.CELLS_PER_BATTERY);
        }
    }

    public void SetBatteries(int batteries)
    {
        this.overrideCells = true;
        this.totalBatteries = batteries;
    }

    public int GetFuses()
    {
        if (this.overrideFuseParts)
        {
            return this.totalFuses;
        }
        else
        {
            return EnergyManager.MIN_FUSES + Mathf.FloorToInt(this.totalFuseParts / EnergyManager.FUSE_PARTS_PER_FUSE);
        }
    }

    /// <summary>
    /// Crea una copia els valors d'un altre data holder a aquest
    /// </summary>
    /// <param name="other"></param>
    public void copyFrom(PlayerDataHolder other)
    {
        if (other.levelsData != null)
        {
            this.levelsData = other.levelsData;
        }

        this.totalCells = other.totalCells;
        this.overrideCells = other.overrideCells;
        this.totalBatteries = other.totalBatteries;
        this.totalFuseParts = other.totalFuseParts;
        this.overrideFuseParts = other.overrideFuseParts;
        this.totalFuses = other.totalFuses;
        this.powerGlove = other.powerGlove;
        this.blastGlove = other.blastGlove;
        this.lastPlayedStage = other.lastPlayedStage;
        this.totalLeaflets = other.totalLeaflets;
    }

    

    /// <summary>
    /// Guarda els valors d'aquest data holder en un fitxer permanent
    /// </summary>
    public void Save()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(getFilePath());

        PlayerData playerData = new PlayerData();
        playerData.totalCells = this.totalCells;
        playerData.totalFuseParts = this.totalFuseParts;
        playerData.blastGlove = this.blastGlove;
        playerData.powerGlove = this.powerGlove;
        SaveStagesDataFromDictionary(ref playerData);

        ConsoleLog.Instance.Log(">>>> [PlayerDataHolder::Save]");
        ConsoleLog.Instance.Log("Data saved in file: " + getFilePath());
        ConsoleLog.Instance.Log("Total cells: " + playerData.totalCells);
        ConsoleLog.Instance.Log("Total fuse parts: " + playerData.totalFuseParts);
        ConsoleLog.Instance.Log("Blast Glove: " + playerData.blastGlove);
        ConsoleLog.Instance.Log("Power Glove: " + playerData.powerGlove);
        ConsoleLog.Instance.Log("<<<<");

        bf.Serialize(file, playerData);
        file.Close();
    }

    /// <summary>
    /// Si estem carregant info permanent, carrega el fiter de dades permanent i assigna els valors a aquest data holder
    /// Si no crea una copia del de l'editor
    /// </summary>
    public void Load()
    {
        if (GameManager.Instance.gameData.gameData.loadPlayerDataFile)
        {
            if (File.Exists(getFilePath()))
            {
                BinaryFormatter bf = new BinaryFormatter();
                FileStream file = File.Open(getFilePath(), FileMode.Open);

                try
                {
                    PlayerData playerData = (PlayerData)bf.Deserialize(file);
                    totalCells = playerData.totalCells;
                    totalFuseParts = playerData.totalFuseParts;
                    blastGlove = playerData.blastGlove;
                    powerGlove = playerData.powerGlove;
                    levelsData = RetrieveLevelsDataAsDictionary(playerData);
                }
                catch(Exception e)
                {
                    Debug.LogWarning("Data file has an incorrect format and can't be loaded. Error: " + e.Message);
                    PlayerDataHolder original = Resources.Load("Data/PlayerDataHolder") as PlayerDataHolder;
                    this.copyFrom(original);
                }
                finally
                {
                    file.Close();
                }
            }
        }
        else
        {
            PlayerDataHolder original = Resources.Load("Data/PlayerDataHolder") as PlayerDataHolder;
            this.copyFrom(original);
        }
    }

    /// <summary>
    /// Guarda un diccionari de /string/ i /PlayerLevelData/ en dues llistes per a poder-ho serialitzar
    /// </summary>
    /// <param name="list">Diccionari en format <string, PlayerLevelData></param>
    private void SaveStagesDataFromDictionary(ref PlayerData playerData)
    {
        playerData.stageNames = new string[levelsData.Count];
        playerData.stagesData = new PlayerStageData[levelsData.Count];

        int counter = 0;

        foreach (KeyValuePair<string, PlayerStageData> playerStageData in levelsData)
        {
            playerData.stageNames[counter] = playerStageData.Key;
            playerData.stagesData[counter] = playerStageData.Value;
            counter++;
        }
    }

    /// <summary>
    /// Recupera, des de dos strings, un diccionari en format <string, PlayerLevelData> amb la informaci� de les pantalles passades per el player
    /// </summary>
    /// <returns>El diccionari si hi ha dades, null si no n'hi ha o hi ha hagut alg�n error recuperant-les</returns>
    private Dictionary<string, PlayerStageData> RetrieveLevelsDataAsDictionary(PlayerData playerData)
    {
        Dictionary<string, PlayerStageData> dict = new Dictionary<string, PlayerStageData>();

        if (playerData.stageNames == null && playerData.stagesData == null)
        {
            Debug.Log("No teniem dades previes quan intentem crear el diccionari");
            return dict;
        }
        else if (playerData.stageNames == null || playerData.stagesData == null)
        {
            Debug.LogWarning(string.Format("La informacio guardada del player es erronea, alguna cosa no va be. \n StageNames: {0} - Level Datas {1}", playerData.stageNames, playerData.stagesData));
            return dict;
        }
        else if (playerData.stageNames.Length != playerData.stagesData.Length)
        {
            Debug.LogWarning(string.Format("La informacio guardada del player es erronea, no tenen la mateixa longitud les llistes de noms i data. \n StageNames: {0} - Level Datas {1}", playerData.stageNames.Length, playerData.stagesData.Length));
            return dict;
        }

        for (int i = 0; i < playerData.stageNames.Length; i++)
        {
            dict.Add(playerData.stageNames[i], playerData.stagesData[i]);
        }

        return dict;
    }

    /// <summary>
    /// Retorna el path del fitxer de dades persistents. M'interessa per si fem versions
    /// </summary>
    /// <returns>url on es troba el fitxer</returns>
    public static string getFilePath()
    {
        return Application.persistentDataPath + "/redHotHero.srhh";
    }

    /// <summary>
    /// Classe serializable per a poder guardar el PlayerDataHolder en un fitxer
    /// </summary>
    [Serializable]
    private class PlayerData
    {
        /// <summary>
        /// Per a guardar, de manera serialitzada, els stages que ens hem passat
        /// </summary>
        public string[] stageNames;

        /// <summary>
        /// Per a guardar, de manera serialitzada, la info dels stages que ens hem passat
        /// </summary>
        public PlayerStageData[] stagesData;

        /// <summary>
        /// Total cells que hem agafat
        /// </summary>
        public int totalCells;

        /// <summary>
        /// Total fuses que hem agafat
        /// </summary>
        public int totalFuseParts;

        /// <summary>
        /// Si ja tenim el power glove
        /// </summary>
        public bool powerGlove;

        /// <summary>
        /// Si ja tenim el blast glove
        /// </summary>
        public bool blastGlove;
    }
}
