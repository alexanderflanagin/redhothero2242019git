using UnityEngine;
using System.Collections;

public class FillFloorDataHolder : ScriptableObject
{
    [SerializeField]
    public EnviroTypeData[] enviroTypeFloors;    
}

[System.Serializable]
public class EnviroTypeData
{
    [SerializeField]
    public GameObject left;

    [SerializeField]
    public GameObject right;

    [SerializeField]
    public GameObject[] small;

    [SerializeField]
    public GameObject[] medium;

    [SerializeField]
    public GameObject[] large;
}
