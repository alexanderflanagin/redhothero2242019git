using UnityEngine;
using UnityEngine.Audio;

/// <summary>
/// Classe per a gestionar la info d'audios del joc
/// </summary>
public class AudioClipsData : ScriptableObject
{
    /// <summary>
    /// Prefix per als audios del Hero (per poder repetir nom amb altres llistes)
    /// </summary>
    public static string playerPrefix = "HERO";

    /// <summary>
    /// Prefix per als audios dels enemics (per a poder repetir noms amb altres llistes)
    /// </summary>
    public static string enemyPrefix = "ENEMY";

    /// <summary>
    /// Prefix per als audios del enviro (per a poder repetir noms amb altres llistes)
    /// </summary>
    public static string enviroPrefix = "ENVIRO";

    /// <summary>
    /// Enviro themes audio clips prefix
    /// </summary>
    public static string themesPrefix = "THEME";

    /// <summary>
    /// To pass the PlayOneShoot clips without the needed of add the prefix in the string
    /// </summary>
    public enum ClipsGroups
    {
        Player,
        Enemies,
        Enviro,
        Themes
    }

    /// <summary>
    /// Enviro themes audio clips
    /// </summary>
    public AudioClipInfo[] themes;

    public int themesCount;

    /// <summary>
    /// Clips d'audio del player
    /// </summary>
    public AudioClipInfo[] playerClips;

    /// <summary>
    /// Clips d'audio dels enemics
    /// </summary>
    public AudioClipInfo[] enemiesClips;

    /// <summary>
    /// Clips d'audio de l'enviro
    /// </summary>
    public AudioClipInfo[] enviroClips;

    /// <summary>
    /// Gets the clip name according to it's clips group
    /// </summary>
    /// <param name="group">Clip group of the clip</param>
    /// <param name="name">Name of the clip</param>
    /// <returns>Full formed name of the clip</returns>
    public static string GetClipFullName(ClipsGroups group, string name)
    {
        switch(group)
        {
            case ClipsGroups.Player:
                return playerPrefix + name;
            case ClipsGroups.Enemies:
                return enemyPrefix + name;
            case ClipsGroups.Enviro:
                return enviroPrefix + name;
            case ClipsGroups.Themes:
                return themesPrefix + name;
        }

        return name;
    }
}

/// <summary>
/// Classe per a representar cada clip d'audio i poder-ne editar les propietats individualment
/// </summary>
[System.Serializable]
public class AudioClipInfo
{
    public int ID;
    
    /// <summary>
    /// Nom del clip, com a referencia interna
    /// </summary>
    public string name;

    /// <summary>
    /// Fitxer d'audio que ha de sonar
    /// </summary>
    public AudioClip file;

    /// <summary>
    /// Audio Mixer Group per el que ha de sonar
    /// </summary>
    public AudioMixerGroup mixerGroup;

    /// <summary>
    /// Prioritat del so
    /// </summary>
    [Range(0, 256)]
    public int priority = 128;

    /// <summary>
    /// Volum del so
    /// </summary>
    [Range(0, 1)]
    public float volume = 1;
}
