using UnityEngine;

/// <summary>
/// Classe per a guardar info dels prefabs associats al tiled
/// </summary>
[System.Serializable]
public class TiledData : ScriptableObject
{
    /// <summary>
    /// Array amb info dels prefabs
    /// </summary>
    public TileInfo[] tilesInfo;

    [System.Serializable]
    public struct TileInfo
    {
        public enum Layer { FirstTileset = 1, SecondTileset = 325 }

        public enum ParentGameObject { Colliders, DotsAndCoins, RedHotDensities, Enemies, BlueSubstances, Boxes, EnemiesGizmos, Decorations }

        public enum FillType { Single } // , Scaled, Comple

        /// <summary>
        /// ID del tile dins del tiled
        /// </summary>
        public int ID;

        /// <summary>
        /// Layer a la que est�
        /// </summary>
        public Layer layer;

        /// <summary>
        /// GameObject on el volem posar
        /// </summary>
        public ParentGameObject parentGameObject;

        /// <summary>
        /// Com voldrem omplir el tiled. De moment nomes podem Simple
        /// </summary>
        public FillType fillType;

        /// <summary>
        /// El GameObject que hi hem de posar
        /// </summary>
        public GameObject prefab;

        /// <summary>
        /// Modificador en les x al crear el game object
        /// </summary>
        public float xModifier;

        /// <summary>
        /// Modificador en les y al crear el game object
        /// </summary>
        public float yModifier;
    }
}
