using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameDataHolder : ScriptableObject {

    public GameData gameData;

    public LevelData[] stagesData;

    public EnviroData[] stagesOrdered;

    /// <summary>
    /// Una llista amb informaci� dels nivells ordenada per el nom del nivell
    /// </summary>
    public Dictionary<string, LevelData> sceneNameBasedStagesList;

    public GameDataHolder(GameData gd, LevelData[] lsd)
    {
        gameData = gd;

        stagesData = lsd;
    }
}
