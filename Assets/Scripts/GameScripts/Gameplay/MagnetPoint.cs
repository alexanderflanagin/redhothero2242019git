using UnityEngine;
using System.Collections;

public class MagnetPoint : MonoBehaviour {

    void OnBecameVisible()
    {
        RedHotTarget.instance.AddPowerGlovedMagnet(this.gameObject);
    }

    void OnBecameInvisible()
    {
        RedHotTarget.instance.RemovePowerGlovedMagnet(this.gameObject);
    }
}
