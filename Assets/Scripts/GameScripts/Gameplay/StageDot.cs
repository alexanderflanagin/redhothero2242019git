using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class StageDot : MonoBehaviour {

    /// <summary>
    /// Nom de l'esena que ha de carregar
    /// </summary>
    [HideInInspector]
    public string sceneName;

    ///// <summary>
    ///// Sistema de partícules que creix i decreix
    ///// </summary>
    //public ParticleSystem particles;

    ///// <summary>
    ///// Glow de la dot
    ///// </summary>
    //public ParticleSystem glow;

    //private Rotation meshRotation;

    //private Animator animator;

    //[HideInInspector]
    //public Vector3 finalPoint;

    ///// <summary>
    ///// La farem servir per a for�ar que estigui activa en el WorldMap, encara que la pantalla no consti com a passada
    ///// </summary>
    //public bool forcedActive;
    
    //private bool active;

    //private LevelData myLevelData;

    //private float movementSpeed = 0.5f;

    //private GameObject portalParent;

    //private bool trying = false;

    //private int animatorUp = Animator.StringToHash("Up");

    //private int animatorAlreadyUp = Animator.StringToHash("AlreadyUp");

    //public TextMesh levelNameBubble;

    //void OnEnable()
    //{
    //    this.meshRotation = this.GetComponentInChildren<Rotation>();

    //    this.animator = this.GetComponentInChildren<Animator>();

    //    this.portalParent = this.transform.Find("RedHotGateParent").gameObject;

    //    this.levelNameBubble.gameObject.SetActive(false);

    //    // Debug.Log(GameManager.Instance.playerData.cells + " - " + GameManager.Instance.levelsData[sceneName].cellsNeeded);

    //    if (!GameManager.Instance.levelsData.TryGetValue(this.sceneName, out this.myLevelData))
    //    {
    //        Debug.LogWarning("StageDot sense level data", this.gameObject);

    //        this.myLevelData = LevelData.CreateDummy();
    //    }

    //    this.levelNameBubble.text = this.myLevelData.name;

    //    PlayerLevelData pld;

    //    // Mirem si ja est� activa l'StageDot
    //    if (GameManager.Instance.playerData.levelsData.TryGetValue(this.sceneName, out pld))
    //    {
    //        Debug.Log("S� que tenim informaci� d'aquesta pantalla: " + pld.finished);
    //        this.active = pld.finished;

    //        this.GetComponent<BoxCollider2D>().enabled = false;
    //    }

    //    this.active = this.active || this.forcedActive; // Estar� activa si ja ho est� o est� for�ada

    //    if (this.active)
    //    {
    //        StartActive();
    //    }
    //    else if (GameManager.Instance.levelComplete && GameManager.Instance.levelsData.ContainsKey(GameManager.Instance.currentLevelName))
    //    {
    //        // Mirem si ens hem passat la pantalla i si tenim la pantalla que ens hem passat al diccionari
    //        if (GameManager.Instance.levelsData[GameManager.Instance.currentLevelName].devNextStageScene == this.myLevelData.sceneName)
    //        {
    //            // Si la tenim, mirem si la seg�ent pantalla que hem d'accionar �s a la que estem
    //            Activate();
    //        }
    //    }
    //}

    //public void StartActive()
    //{
    //    if (!this.animator)
    //    {
    //        this.animator = this.GetComponentInChildren<Animator>();
    //    }
        
    //    this.transform.position += new Vector3(0, 3.5f, 0);
    //    this.animator.SetBool(this.animatorAlreadyUp, true);
    //    this.animator.SetBool(this.animatorUp, true);

    //    this.levelNameBubble.gameObject.SetActive(true);

    //    if (true || GameManager.Instance.playerData.totalBatteries >= this.myLevelData.cellsNeeded)
    //    {
    //        RedHotTarget.instance.AddRedHotDensity(this.gameObject);
    //    }
    //}

    //private IEnumerator ActivateStageDot()
    //{
    //    //this.trying = true;
    //    yield return null;
    //    yield return null;

    //    Debug.Log(StandardStageManager.current.playerControl);

    //    //StandardStageManager.current.playerControl.input.Freeze(0, false);

    //    yield return new WaitForSeconds(1f);

    //    this.animator.SetBool(this.animatorUp, true);

    //    float startTime = Time.time;

    //    Vector3 endPosition = this.transform.position + new Vector3(0, 3.5f, 0);

    //    while (this.transform.position != endPosition)
    //    {
    //        this.transform.position = Vector3.MoveTowards(this.transform.position, endPosition, 0.1f);
    //        yield return null;
    //    }

    //    //while (startTime + 3f > Time.time)
    //    //{
    //    //    StandardStageManager.current.player.transform.position = this.portalParent.transform.position + Vector3.down;
            
    //    //    yield return null;
    //    //}

    //    yield return new WaitForSeconds(1f);

    //    //if (GameManager.Instance.playerData.cells >= this.myLevelData.cellsNeeded)
    //    //{
    //    // yield return StartCoroutine(MoveToEndPosition());
        
    //    //}
    //    //else
    //    //{
    //    //    this.animator.SetBool(this.animatorUp, false);

    //    //    yield return new WaitForSeconds(1f);
    //    //}

    //    //this.trying = false;

    //    this.active = true;

    //    this.levelNameBubble.gameObject.SetActive(true);

    //    if (GameManager.Instance.playerData.totalBatteries >= this.myLevelData.cellsNeeded)
    //    {
    //        RedHotTarget.instance.AddRedHotDensity(this.gameObject);
    //    }

    //    if (true || GameManager.Instance.playerData.totalBatteries >= this.myLevelData.cellsNeeded)
    //    {
    //        RedHotTarget.instance.AddRedHotDensity(this.gameObject);
    //    }

    //    //StandardStageManager.current.playerControl.input.Defreeze();
    //}

    //private IEnumerator MoveToEndPosition()
    //{
    //    while (this.transform.position != this.finalPoint)
    //    {
    //        this.transform.position = Vector3.MoveTowards(this.transform.position, this.finalPoint, this.movementSpeed);
    //        yield return null;
    //    }
    //}

    ///// <summary>
    ///// Al fer-se visible, afegim el portal a la llista del RedHotTarget i ens assegurem que el tag sigui correcte
    ///// </summary>
    //void OnBecameVisible()
    //{
    //    if (this.active)
    //    {
    //        RedHotTarget.instance.AddRedHotDensity(this.gameObject);
    //    }
    //    // Debug.Log("Visible!", this.gameObject);
    //}

    ///// <summary>
    ///// Al sortir de pantalla, el treiem de la llista
    ///// </summary>
    //void OnBecameInvisible()
    //{
    //    RedHotTarget.instance.RemoveRedHotDensity(this.gameObject);
    //}

    //void OnTriggerEnter2D(Collider2D other)
    //{
    //    if(other.CompareTag(Tags.Player))
    //    {
    //        if (this.active)
    //        {
    //            StageDotEnter();
    //        }
    //        //else
    //        //{
    //        //    if (!trying)
    //        //    {
    //        //        StartCharge();
    //        //    }
    //        //}
    //    }
    //}

    ///// <summary>
    ///// Mètode que es crida al tocar una stageDot per a activar-la
    ///// </summary>
    //private void StageDotEnter()
    //{
    //    //Time.timeScale = 0;
        
    //    //WorldMapManager.current.stageDotInfo.SetActive(true);

    //    this.levelNameBubble.gameObject.SetActive(false);

    //    // BERNARDO
    //    // Modificació de les partícules al tocar la StageDot
    //    // Fer servir l'objecte this.particles i this.glow. Per exemple:
    //    this.particles.startSpeed = 40f;
    //    this.particles.startSize = 2f;
    //    this.particles.maxParticles = 24000;
    //    this.particles.startLifetime = 2;
    //    this.particles.emissionRate = 8000;
    //    this.glow.enableEmission = false;

    //    StartCoroutine(WaitForInput());
    //}

    ///// <summary>
    ///// Mètode que, amb la info del StageDot mostrada, espera input per part del player per a començar l'stage o cancel·lar
    ///// </summary>
    ///// <returns></returns>
    //private IEnumerator WaitForInput()
    //{
    //    //Time.timeScale = 0;
    //    //WorldMapManager.current.playerControl.Pause();
    //    //WorldMapManager.current.player.transform.position = this.transform.position + Vector3.down; // Corregim la posició d'arribada per a que quedi centrat

    //    bool enterLevel = false, closePopUp = false;

    //    //WorldMapManager.current.stageName.text = this.myLevelData.name;

    //    while(!enterLevel && !closePopUp)
    //    {
    //        enterLevel = cInput.GetKeyDown("Jump");
    //        closePopUp = cInput.GetKeyDown("Punch");
    //        yield return null;
    //    }

    //    //Time.timeScale = 1;

    //    if(enterLevel)
    //    {
    //        // BERNARDO
    //        // Modificació de les partícules al acceptar entrar a la pantalla
    //        // Fer servir l'objecte this.particles i this.glow. Per exemple: 
    //        this.particles.startSpeed = 40f;
    //        this.particles.startSize = 2f;
    //        this.particles.maxParticles = 24000;
    //        this.particles.startLifetime = 2;
    //        this.particles.emissionRate = 8000;
    //        this.glow.enableEmission = false;

    //        // Amaguem al player
    //        StandardStageManager.current.player.SetActive(false);

    //        // Temps que passa entre que modifiques les partícules i comença la pantalla
    //        yield return new WaitForSeconds(1f);

    //        GameManager.Instance.LoadLevel(this.sceneName);
    //    }
    //    else
    //    {
    //        //WorldMapManager.current.playerControl.UnPause();
            
    //        // BERNARDO
    //        // Modificació de les partícules al cancel·lar entrar a la pantalla
    //        // Fer servir l'objecte this.particles i this.glow. Per exemple:
    //        this.particles.startSpeed = 1.95f;
    //        this.particles.startSize = 0.2f;
    //        this.particles.maxParticles = 300;
    //        this.particles.startLifetime = 1;
    //        this.particles.emissionRate = 300;
    //        this.glow.enableEmission = true;

    //        this.levelNameBubble.gameObject.SetActive(false);
            
    //        //WorldMapManager.current.stageDotInfo.SetActive(false);
    //    }
    //}

    ///// <summary>
    ///// Activa el stageDot quan estava desactivat
    ///// </summary>
    //public void Activate()
    //{
    //    StartCoroutine(ActivateStageDot());
    //}
}
