using UnityEngine;
using System.Collections;

/// <summary>
/// Pojectil basic per a les turrets, S-tv emitters, etc.
/// </summary>
public class EnemyBasicProjectile : Projectile
{
    /// <summary>
    /// Velocitat del projectil
    /// </summary>
    public float _speed = 200f;

    /// <summary>
    /// Indica si s'ha de moure el projectil o no
    /// </summary>
    public bool move = true;

    /// <summary>
    /// Factory des d'on es dispara, per a ignorar-la també
    /// </summary>
    public GameObject factory;

    new private CircleCollider2D collider;

    /// <summary>
    /// Al fer Awake, registrem el component RendererVisibleEvent
    /// </summary>
    void Awake()
    {
        collider = GetComponent<CircleCollider2D>();

        gameObject.layer = Layers.Projectiles;
    }

    /// <summary>
    /// Si s'ha de moure, actualitzem la posici�
    /// </summary>
	void Update () {
        if (move)
        {
            transform.Translate(Vector3.forward * _speed * Time.deltaTime);
        }
	}

    /// <summary>
    /// Al entrar un trigger, si l'element amb el que col.lisiona no es el seu owner i es un enemic o el Player, el mata i es desactiva
    /// </summary>
    /// <param name="other"></param>
    void OnTriggerEnter2D(Collider2D other)
    {
        Physics2D.queriesHitTriggers = true;
        Collider2D blueSubstance = Physics2D.OverlapCircle(transform.position, collider.radius, 1 << Layers.BlueSubstance);
        Physics2D.queriesHitTriggers = false;

        if (other.gameObject.layer == Layers.BlueSubstance)
        {
            Instantiate(GeneralPurposeObjectManager.Instance.BlueSubstanceShoot, transform.position, Quaternion.identity);
            OnDestroyProjectile();
        }
        else if (blueSubstance)
        {
            Instantiate(GeneralPurposeObjectManager.Instance.BlueSubstanceShoot, transform.position, Quaternion.identity);
            OnDestroyProjectile();
        }
        else if (other.gameObject != this.owner && other.gameObject != this.factory)
        {
            if (other.GetComponent<Shootable>())
            {
                gameObject.SetActive(false);
                other.GetComponent<Shootable>().Touched(this);
            }
            else if (other.GetComponent<CharacterControl>() != null)
            {
                gameObject.SetActive(false);
                other.GetComponent<CharacterControl>().RecieveDamage(this.gameObject);
            }
            
            if (!other.isTrigger && other.gameObject.layer != Layers.RedHotDensity && other.gameObject.layer != Layers.RedHotDot)
            {
                OnDestroyProjectile();
            }
        }
    }

    /// <summary>
    /// Al destruir el projectil, cridem el mètode del pare i posem el move a falç
    /// </summary>
    public override void OnDestroyProjectile()
    {
        base.OnDestroyProjectile();
        move = false;
    }
}
