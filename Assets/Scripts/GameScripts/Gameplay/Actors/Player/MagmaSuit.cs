using UnityEngine;
using System;
using System.Collections;

/// <summary>
/// Magma suit.
/// </summary>
public class MagmaSuit : MonoBehaviour
{
    /// <summary>
    /// El gestor d'energia. La genera, et diu si en te prou i la consumeix
    /// </summary>
    public EnergyManager energy;

    /// <summary>
    /// Defineix si té o no té el Blast Glove
    /// </summary>
    public bool hasBlastGlove = false;

    /// <summary>
    /// Els guants que porta equipats (si es que en porta)
    /// </summary>
    public BlastGlovesBasic blastGlove;

    /// <summary>
    /// Defineix si té o no té el Power Glove
    /// </summary>
    public bool hasPowerGlove = false;

    public GameObject powerGlovePrefab;

    /// <summary>
    /// El gua que li permet fotre cops de puny
    /// </summary>
    [HideInInspector]
    public GameObject powerGloveObject;

    /// <summary>
    /// El script del power glove
    /// </summary>
    public PowerGlove powerGlove
    {
        get;
        set;
    }

    /// <summary>
    /// GameObject de la mà
    /// </summary>
    public GameObject bigArm;

    /// <summary>
    /// Material que assignarem a la mesh al agafar energia
    /// </summary>
    public Material onGetEnergyMaterial;

    [SerializeField]
	private CharacterControl _controller;

    // Use this for initialization
    private void OnEnable()
	{
        //this.addGloves();
        energy = transform.GetOrAddComponent<EnergyManager>();
        //energy.energyMeter = energyMeter;
        this.energy.onGetEnergyMaterial = this.onGetEnergyMaterial;

        // Assignem si té o no té el blast glove i el power glove segons el que ens digui el player data
        hasBlastGlove = this.hasBlastGlove || GameManager.Instance.playerData.blastGlove;
        hasPowerGlove = this.hasPowerGlove || GameManager.Instance.playerData.powerGlove;

        addGloves();
	}

    /// <summary>
    /// Equipa uns guants (de moment els bàsics)
    /// </summary>
    /// <param name="glovesType"></param>
    public void addGloves()
    {
        // Gloves per defecte, els que disparen en una direcció
        if (hasBlastGlove)
        {
            blastGlove = transform.GetOrAddComponent<BlastGlovesBasic>();
        }

        if (hasPowerGlove && powerGlove == null)
        {
            powerGloveObject = Instantiate(powerGlovePrefab) as GameObject;
            powerGlove = powerGloveObject.GetComponent<PowerGlove>();
            powerGloveObject.SetActive(false);
            powerGlove.owner = _controller;
            powerGloveObject.SetActive(true);
        }

        if (hasPowerGlove || hasBlastGlove)
        {
            transform.GetOrAddComponent<PlayerAim>();
        }
    }

	// Update is called once per frame
	void Update ()
	{
        if (_controller.input.shoot.down && blastGlove != null && _controller.movementFSM.currentHorizontal != PlayerState.Horizontal.Crouch && _controller.movementFSM.currentHorizontal != PlayerState.Horizontal.Roll)
        {
            if (this.powerGlove)
            {
                this.powerGlove.cancelCharge();
            }

            if (energy.hasEnougthEnergy(EnergyManager.SHOT_DRAIN_POINTS))
            {
                //_shoot = true;
                blastGlove.Shoot();
                energy.getEnergy(EnergyManager.SHOT_DRAIN_POINTS);
            }
            else
            {
                _controller.events.triggerEvent(CharacterEvents.EventName.NoEnergyFailShot);
            }
        }

        /* if (Input.GetAxis("ShootJ0") == 0)
        {
            _shoot = false;
        } */
	}

    public void RecieveDamage()
    {
        if(this.energy.energy <= EnergyManager.FIRST_FUSE_BATTERIES * EnergyManager.POINTS_PER_BATTERY)
        {
            // T'han tocat al darrer fuse. Mors
            this.energy.energy = 0;
        }
        else
        {
            // Mirem quant descarreguem fins al pròxim fuse
            int extraEnergy = this.energy.energy - EnergyManager.FIRST_FUSE_BATTERIES * EnergyManager.POINTS_PER_BATTERY;

            int extraFuses = Mathf.FloorToInt(extraEnergy / (EnergyManager.FUSE_BATTERIES * EnergyManager.POINTS_PER_BATTERY));

            int currentFuse = Mathf.Min(extraFuses + 1, this.energy.nFuses);

            this.energy.energy = (EnergyManager.FIRST_FUSE_BATTERIES * EnergyManager.POINTS_PER_BATTERY) + (EnergyManager.FUSE_BATTERIES * EnergyManager.POINTS_PER_BATTERY * (currentFuse - 1));
        }
    }
}
