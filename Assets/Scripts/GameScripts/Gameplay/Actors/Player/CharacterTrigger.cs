﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CharacterTrigger : MonoBehaviour {

	/*
	 * Diccionari que guarda els colliders que es troben dins del trigger
	 */
	public Dictionary<GameObject, float> insideColliders;

	/*
	 * Al start, iniciem el diccionari
	 */
	void Start() {
		insideColliders = new Dictionary<GameObject, float>();

        gameObject.layer = transform.parent.gameObject.layer;
	}

	/*
	 * Al entrar al trigger, afegim l'objecte al diccionari
	 */
	void OnTriggerEnter2D( Collider2D other ) {
		float time;
		if( !insideColliders.TryGetValue(other.gameObject, out time) ) {
			insideColliders.Add( other.gameObject, Time.realtimeSinceStartup );
		}
	}
	
	/*
	 * Al sortir del trigger, treiem l'objecte del diccionari
	 */
	void OnTriggerExit2D( Collider2D other ) {
		insideColliders.Remove( other.gameObject );
	}
}
