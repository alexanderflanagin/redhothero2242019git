using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;

/// <summary>
/// El player comença amb 2 batteries d'energia. Cada battery d'energia, són 100 punts d'energia.
/// Disparar gasta 25 punts
/// Cada segon perdem 10 punts automàticament
/// </summary>
public class EnergyManager : MonoBehaviour
{
    public static int MIN_BATTERIES = 3;

    public static int MAX_BATTERIES = 21;

    /// <summary>
    /// Quants punts té cada battery
    /// </summary>
    public static int POINTS_PER_BATTERY = 100;

    public static int CELLS_PER_BATTERY = 6;
    
    /// <summary>
    /// Quants punts recarrega agafar una dot
    /// </summary>
    public static int DOT_REGEN_POINTS = 10;

    /// <summary>
    /// Quants punts recarrega agafar una super dot.
    /// </summary>
    public static int SUPER_DOT_REGEN_POINTS = 30;

    /// <summary>
    /// Quants punts es gasten al disparar
    /// </summary>
    public static int SHOT_DRAIN_POINTS = 25;

    /// <summary>
    /// Mínima energia amb la que encara podem disparar
    /// </summary>
    public static int MIN_ENERGY = 100;

    /// <summary>
    /// Quanta energia perds cada vegada que passa un període determinat
    /// </summary>
    public static int PER_TIME_DRAIN_POINTS = 5;

    /// <summary>
    /// Quant temps ha de passar per a perdre energia
    /// </summary>
    public static float SECONDS_TO_DRAIN_POINTS = 0.2f;

    /// <summary>
    /// Quanta energia perdem si ens toquen
    /// </summary>
    public static int DAMAGE_PER_TOUCH = 300;

    /// <summary>
    /// Batteries per defecte que té un Fuse
    /// </summary>
    public static int FUSE_BATTERIES = 3;

    /// <summary>
    /// Batteries en concret que té el primer Fuse
    /// </summary>
    public static int FIRST_FUSE_BATTERIES = 2;

    public static int FUSE_PARTS_PER_FUSE = 4;

    public static int MIN_FUSES = 1;

    /// <summary>
    /// Material que posem a la mesh quan s'agafa una dot
    /// </summary>
    public Material onGetEnergyMaterial;

    private bool materialBlinking;

    /// <summary>
    /// Energia que tens. Per defecte, el primer fuse complet
    /// </summary>
    private int _energy = EnergyManager.POINTS_PER_BATTERY * EnergyManager.FIRST_FUSE_BATTERIES;

    /// <summary>
    /// El script de HUD del energy meter actual
    /// </summary>
    private HUDEnergyMeter currentEnergyMeterHUD;

    /// <summary>
    /// Controller del player. Per a llençar els events quan entrem i sortim de l'energia baixa
    /// </summary>
    private CharacterControl _controller;

    /// <summary>
    /// Getter i setter de l'energi que criden al mètode d'actualtizar el EnergyMeter
    /// </summary>
    public int energy
    {
        get { return _energy; }

        set
        {
            if (_energy > POINTS_PER_BATTERY && value <= POINTS_PER_BATTERY)
            {
                if (_controller)
                {
                    _controller.events.triggerEvent(CharacterEvents.EventName.EnergyLowEnter);
                }
            }

            if (_energy <= POINTS_PER_BATTERY && value > POINTS_PER_BATTERY)
            {
                if (_controller)
                {
                    _controller.events.triggerEvent(CharacterEvents.EventName.EnergyLowExit);
                }
            }
            
            if (value < 0)
            {
                _energy = 0;
            }
            else
            {
                _energy = value;
                updateEnergyMeter();
            }
        }
    }

    /// <summary>
    /// Energia màxima del suit (això serà variable en realitat)
    /// </summary>
    private int currentMaxEnergy;

    /// <summary>
    /// Número de batteries que tenim
    /// </summary>
    public int nBatteries;

    /// <summary>
    /// Número de fuses que té actualment. Per defecte en tindrà 1, el /FIRST_FUSE_BATTERIES/
    /// </summary>
    public int nFuses;

    /// <summary>
    /// Si ha de perdre energia
    /// </summary>
    [HideInInspector]
    public bool loseEnergy;

    /// <summary>
    /// Per a posar la pantalla en blanc i negre quan ens quedem sense energia
    /// </summary>
    private CC_Grayscale _cameraGrayscale;

    /// <summary>
    /// Mesh del Player para coger/cambiar los materiales
    /// </summary>
    private SkinnedMeshRenderer mesh;

    private Material[] originalMaterials;

    private void Awake()
    {
        _controller = GetComponent<CharacterControl>();

        mesh = _controller.mesh.GetComponentInChildren<SkinnedMeshRenderer>();
        originalMaterials = mesh.sharedMaterials;
    }

    // Use this for initialization
    void OnEnable()
    {
        mesh.sharedMaterials = originalMaterials;
        materialBlinking = false;

        // _cameraGrayscale = StageManager.current.camera.gameObject.GetComponent<CC_Grayscale>();
        _cameraGrayscale = FindObjectOfType<CC_Grayscale>();

        // Assignem les batteries del player segons el scriptable object
        nBatteries = GameManager.Instance.playerData.GetBatteries();

        nFuses = GameManager.Instance.playerData.GetFuses();

        currentMaxEnergy = nBatteries * POINTS_PER_BATTERY;
        energy = currentMaxEnergy;

        if (GameObject.FindObjectOfType<HUDEnergyMeter>())
        {
            // Agafem l'energia
            this.currentEnergyMeterHUD = GameObject.FindObjectOfType<HUDEnergyMeter>();
        }

        _controller.events.registerEvent(CharacterEvents.EventName.DieByNoEnergy, OnDieByNoEnergy);

        loseEnergy = true;

        StartCoroutine(DrainEnergy());
    }

    void OnDisable()
    {
        _controller.events.unregisterEvent(CharacterEvents.EventName.DieByNoEnergy, OnDieByNoEnergy);
    }

    void OnDieByNoEnergy()
    {
        _cameraGrayscale.enabled = true;
        _cameraGrayscale.amount = 1;
    }

    /// <summary>
    /// Et diu si té prou energia per a servir-la
    /// </summary>
    /// <param name="energy">Quanta energia es necessita</param>
    /// <returns></returns>
    public bool hasEnougthEnergy(int en)
    {
        //Debug.Log(string.Format("Tenim prou energia? {0} - {1}", en, _energy), this.gameObject);
        return (MIN_ENERGY < _energy);
    }

    /// <summary>
    /// Resta l'energia del suit
    /// </summary>
    /// <param name="energy"></param>
    public void getEnergy(int energy)
    {
        this.energy -= energy;

        if (_energy < 0)
        {
            //Debug.Log("Hi ha hagut algún problema, el getEnergy ha deixat les cel·lules per sota de zero.", this.gameObject);
        }
    }

    /// <summary>
    /// Afegeix energia al suit (mai més del màxim que pot tenir)
    /// </summary>
    /// <param name="en"></param>
    public void addEnergy(int en, bool show = true)
    {
        energy = Mathf.Min(_energy + en, currentMaxEnergy);

        StartCoroutine(blinkMaterial(en));

        this.currentEnergyMeterHUD.EnergyGet(en);
    }

    private IEnumerator blinkMaterial(int energy)
    {
        if (materialBlinking)
        {
            yield break;
        }

        this.materialBlinking = true;

        if (mesh)
        {
            Material[] materials = mesh.sharedMaterials;
            materials[0] = materials[2] = this.onGetEnergyMaterial;

            mesh.sharedMaterials = materials;

            yield return new WaitForSeconds((float)energy / 50);

            mesh.sharedMaterials = originalMaterials;
        }

        this.materialBlinking = false;
    }

    /// <summary>
    /// Actualitza el Energy Meter
    /// </summary>
    private void updateEnergyMeter()
    {
        if (this.currentEnergyMeterHUD)
        {
            this.currentEnergyMeterHUD.updateEnergy(nBatteries, energy, this.nFuses);
        }

        if (_cameraGrayscale)
        {
            if (_energy <= POINTS_PER_BATTERY && _energy > 0)
            {
                // Estem a la darrera battery, fem avisos sonors i el que faci falta
                _cameraGrayscale.enabled = true;
                _cameraGrayscale.amount = 1f - ((float)_energy / (float)POINTS_PER_BATTERY);
            }
            else if (_energy > POINTS_PER_BATTERY)
            {
                _cameraGrayscale.enabled = false;
                _cameraGrayscale.amount = 0;
            }
        }
    }

    /// <summary>
    /// Cada X temps, perdem automàticament Y d'energia (passi el que passi)
    /// </summary>
    /// <returns></returns>
    private IEnumerator DrainEnergy()
    {
        WaitForSeconds waitDranPointsSeconds = new WaitForSeconds(SECONDS_TO_DRAIN_POINTS);

        while (true)
        {
            if (GameManager.Instance.playerData.godMode || !loseEnergy)
            {
                yield return waitDranPointsSeconds;
                continue;
            }
                

            // Ho faig primer per a donar-li al Player 1 segon més de vida
            if (_controller.currentAlive != CharacterControl.StateAlive.Invulnerable)
            {
                energy -= PER_TIME_DRAIN_POINTS;
            }
            yield return waitDranPointsSeconds;
        }
    }

    /// <summary>
    /// Si l'energia arriba a 0, es mor
    /// </summary>
    void Update()
    {
        if (energy <= 0)
        {
            _controller.StartDying(false, true);
        }
    }

    //void OnPlayerDie()
    //{
    //    //_cameraGrayscale.enabled = false;
    //    //_cameraGrayscale.amount = 0;
    //}

    //void OnGUI()
    //{
    //    GUI.Label(new Rect(0, 0, 100, 20), "Energy: " + this.energy);
    //    GUI.Label(new Rect(0, 20, 100, 20), "Fuses: " + this.nFuses);

    //    int extraEnergy = this.energy - EnergyManager.FIRST_FUSE_BATTERIES * EnergyManager.POINTS_PER_BATTERY;

    //    int extraFuses = Mathf.FloorToInt(extraEnergy / (EnergyManager.FUSE_BATTERIES * EnergyManager.POINTS_PER_BATTERY));

    //    int currentFuse = Mathf.Min(extraFuses, this.nFuses);

    //    GUI.Label(new Rect(0, 40, 100, 20), "Active Fuses: " + currentFuse);
    //}
}
