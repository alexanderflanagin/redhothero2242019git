using UnityEngine;
using System.Collections;

/// <summary>
/// Classe que s'encarrega de frenar al Player quan es prem el bot� LT
/// </summary>
public class BernardoBrake : MonoBehaviour {

    /// <summary>
    /// El Control del Player
    /// </summary>
    private CharacterControl _control;

    private bool _active = false;

    /// <summary>
    /// Assignem el control del player
    /// </summary>
    void Awake()
    {
        _control = GetComponent<CharacterControl>();
    }

	/// <summary>
	/// A cada frame, mirem si el bot� "LT" ha estat premut
	/// </summary>
	void Update () {
        if (cInput.GetKey(InputButton.BRAKE))
        {
            _active = true;
            if (!_control.input.xAxis.autoValue)
            {
                _control.input.xAxis.autoValue = true;
                _control.input.xAxis.autoValueValue = 0;
            }
        }
        else if (_active)
        {
            _control.input.xAxis.autoValue = false;
            _active = false;
        }
	}
}
