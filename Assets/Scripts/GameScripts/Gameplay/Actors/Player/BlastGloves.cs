﻿using UnityEngine;
using System.Collections;

public abstract class BlastGloves : MonoBehaviour {

    public abstract void Shoot();
}
