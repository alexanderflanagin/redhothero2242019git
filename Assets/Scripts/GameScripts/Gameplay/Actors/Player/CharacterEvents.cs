using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class CharacterEvents {

	/**
	 * Possibles Events que dispara el player
	 */
	public enum EventName {
        BounceEnter,
        BounceExit,
        CrouchEnter,
        CrouchExit,
        DieEnter,
        DieByTouch,
        DieByNoEnergy,
        DieExit,
        EnergyLowEnter,
        EnergyLowExit,
        FallEnter,
        FallExit,
        GroundedEnter,
        GroundedExit,
        IdleEnter,
        IdleExit,
        InertiaEnter,
        InertiaExit,
        InertiaChangingDirection,
        JumpEnter,
        JumpExit,
        JumpFallEnter,
        JumpFallExit,
        NoEnergyFailShot,
        ObjectPowerGloved,
        PowerGloveCharge,
        PowerGloveReleased,
        PowerGloveCancelled,
        PowerGloveMovementEnd,
        // PowerGloveEnemyHitted,
        RecieveDamage,
        RollEnter,
        RollExit,
        RunEnter,
        RunExit,
        SlideEnter,
        SlideExit,
        StunnedEnter,
        StunnedExit,
        TravelEnter,
        TravelExit,
        TripOverEnter,
        TripOverExit,
        WallFallEnter,
        WallFallExit,
        WallGrabEnter,
        WallGrabExit,
        WallJumpEnter,
        WallJumpExit,
        BrakeEnter,
        BrakeExit,
        NothingPoweredFisting,
		//Events provisionals que falten pels audios.
		ReFill,
		RHDTargeted,
        RHDTargetedEnd,
		Bluesubstanced,
		Shoot
	}

    public Dictionary<EventName, Action> events = new Dictionary<EventName, Action>();

    public void triggerEvent(EventName name)
    {
        //Debug.Log("Triggering event: " + name);
        if (events.ContainsKey(name) && events[name] != null)
        {
            //Debug.Log("Has " + name + " gonna execute.");
            events[name]();
        }
    }

    public void registerEvent(EventName name, Action action)
    {
        if (events.ContainsKey(name))
        {
            //Debug.Log("Add event " + name + " for NOT the first time");
            events[name] += action;
        }
        else
        {
            //Debug.Log("Add event " + name + " for the first time");
            events.Add(name, action);
        }
    }

    public void unregisterEvent(EventName name, Action action)
    {
        if (events.ContainsKey(name))
        {
            events[name] -= action;
        }
    }
}
