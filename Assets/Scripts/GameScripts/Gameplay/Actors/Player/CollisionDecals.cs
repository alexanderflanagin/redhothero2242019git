using UnityEngine;
using System.Collections;

public class CollisionDecals : MonoBehaviour {

	//Ens hem de plantejar fer un sistema de pooling per
	//no instanciar i destruir a cada rato els quads
	//que generem a les col·lisions

    //private ParticleSystem.CollisionEvent[] collisionEvents = new ParticleSystem.CollisionEvent[20];
	public Texture2D[] decalTextures;
	public Material mat;
	public Color DecalColor;
	public float minPartSize;
	public float maxPartSize;
	public GameObject quad;


	IEnumerator ScaleOverLifetime( GameObject obj){
		float t = Time.time;
		float alpha = 1;
		while ( t < Time.time + 5 ) {
			t += Time.deltaTime;
//			obj.transform.position -= new Vector3 (0 , 0.01f , 0); 
			obj.transform.localScale += Vector3.one *  0.6f * Time.deltaTime ;
			alpha *= 0.95f;

			obj.GetComponent<Renderer>().material.SetColor ( "_TintColor", new Vector4 ( 1, 0, 0, alpha) );
//			obj.renderer.material.SetColor ()
			yield return new WaitForEndOfFrame();
		}
	}


	void OnParticleCollision ( GameObject other ){
        //int safeLength = particleSystem.safeCollisionEventSize;
        //if (collisionEvents.Length < safeLength) {
        //    collisionEvents = new ParticleSystem.CollisionEvent[safeLength];		
        //}

        //int numCollisionEvents = particleSystem.GetCollisionEvents ( other, collisionEvents );
        //int i = 0;

        //while ( i < numCollisionEvents ) {

        //    //if ( other.collider.tag == "BeachFloor" ) {
        //        Vector3 collisionPosition = collisionEvents[i].intersection;
        //        Vector3 collisionNormal = collisionEvents[i].normal;
        //        int index = ( int ) Random.Range ( 0, decalTextures.Length ); 
				
        //        //Debug.DrawRay ( collisionPosition, collisionNormal, Color.blue, 5.0f );
								
        //        GameObject decalClone = Instantiate (quad, collisionPosition + collisionNormal * 0.01f, Quaternion.LookRotation(-collisionNormal)) as GameObject;

        //        decalClone.transform.localScale = new Vector3 ( 1, 1, 1) * Random.Range ( minPartSize, maxPartSize );
        //        decalClone.renderer.material = mat;
        //        decalClone.renderer.material.SetColor ( "_TintColor", DecalColor * new Vector4 ( Random.Range( 0.3f, 1 ), 0, 0, 1 ) );
        //        decalClone.renderer.material.SetTexture ( "_MainTex", decalTextures[index] );
        //        StartCoroutine ("ScaleOverLifetime", decalClone);

        //        Destroy ( decalClone, 5.0f );
        //    //}

        //    //if (other.collider.tag != "BeachFloor")
        //    //{

        //    //    Vector3 collisionPosition = collisionEvents[i].intersection;
        //    //    Vector3 collisionNormal = collisionEvents[i].normal;
        //    //    int index = (int)Random.Range(0, decalTextures.Length);

        //    //    //Debug.DrawRay ( collisionPosition, collisionNormal, Color.blue, 5.0f );

        //    //    //				TODO mirar si podem crear un primitive quad i afegir rotació sobre el plà
        //    //    //				GameObject decalClone = Instantiate (GameObject.CreatePrimitive (PrimitiveType.Quad), collisionPosition + collisionNormal * 0.01f, Quaternion.LookRotation(-collisionNormal)) as GameObject;
        //    //    //				per alguna rao  al instanciar una nova primitiva em genera 2 planos en contes d'un. podria ser per que porta collider???

        //    //    GameObject decalClone = Instantiate(quad, collisionPosition + collisionNormal * 0.01f, Quaternion.LookRotation(-collisionNormal)) as GameObject;

        //    //    decalClone.transform.localScale = new Vector3(1, 1, 1) * Random.Range(minPartSize, maxPartSize);
        //    //    decalClone.renderer.material = mat;
        //    //    decalClone.renderer.material.SetColor("_TintColor", DecalColor * new Vector4(Random.Range(0.3f, 1), 0, 0, 1));
        //    //    decalClone.renderer.material.SetTexture("_MainTex", decalTextures[index]);
        //    //    Destroy(decalClone, 5.0f);
        //    //}
        //    i++;
        //}
	}
}
