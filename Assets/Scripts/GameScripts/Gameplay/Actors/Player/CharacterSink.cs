using UnityEngine;
using System.Collections;

/// <summary>
/// Mira si s'enfonsa el character (tot el seu collider ha sortit, per sota, del trigger de l'aigua)
/// </summary>
public class CharacterSink : MonoBehaviour
{
    /// <summary>
    /// El control del character
    /// </summary>
    public CharacterControl characterControl;

    /// <summary>
    /// Al detectar que un trigger 2D surt del character, mirem si ha estat l'aigua i en tal cas mirem si s'ha enfonsat
    /// </summary>
    /// <param name="other"></param>
    void OnTriggerExit2D(Collider2D other)
    {
        // Atravessa l'aigua
        if (other.gameObject.layer == Layers.WaterOwn)
        {
            // Mirem que no faci un roll o un crouch, perquè es podria aixecar
            if (characterControl.movementFSM.currentHorizontal != PlayerState.Horizontal.Crouch && characterControl.movementFSM.currentHorizontal != PlayerState.Horizontal.Roll)
            {
                // Mirem la posició
                if (characterControl.collider2D.bounds.max.y < other.transform.position.y)
                {
                    // Considerem que s'ha enfonsat
                    characterControl.input.Freeze(0, true);
                    characterControl.movementProps.speed = 0f;
                }
            }
        }
    }
}
