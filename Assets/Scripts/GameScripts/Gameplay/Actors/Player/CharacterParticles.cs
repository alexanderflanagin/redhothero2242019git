using System.Collections;
using UnityEngine;

/// <summary>
/// Notes sobre Particle Systems:
/// podem fer servir particleSystem.duration MOLA!
/// fora interessant doncs fer totes les particules amb Instantiate
/// i poderles anar disparant i destruint automágicamente? hem de plantejar-nos-ho-en
/// </summary>

public class CharacterParticles : Instantiator
{
    public InstantParticleSystemEvent[] particles;

    public ParticleSystem sprintParticleSystem;
    public ParticleSystem runParticleSystem;
    public ParticleSystem bounceParticleSystem;
    public ParticleSystem jumpStartSmokeTrailPS;

    public ParticleSystem punchMovementParticleSystem;

    /// <summary>
    /// Partícules que es llencen quan es vol disparar però no es té prou energía
    /// </summary>
    public ParticleSystem noEnergyFailShot;

    /// <summary>
    /// Particules que es llencen quan fa WallGrab
    /// </summary>
    public ParticleSystem wallGrabParticleSystem;

    /// <summary>
    /// Partícules que es llencen quan es fa inèrcia deixant anar el joystick
    /// </summary>
    public ParticleSystem inertiaPS;

    /// <summary>
    /// Partícules que es llencen quan es fa inèrcia movent el joystick en direcció contrària a la del moviment
    /// </summary>
    public ParticleSystem inertiaChangeDirectionPS;

    public ParticleSystem brakeParticles;

    public Vector3 brakeParticlesOffset;

    /// <summary>
    /// Posició per defecte de les partícules de wall grab, run i sprint quan es mira cap a 1
    /// </summary>
    private Vector3 _wallGrabParticlesOffset = new Vector3(0.5f, 1.3f, 0);

    /// <summary>
    /// Posició per defecte de les partícules de run quan es mira cap a 1
    /// </summary>
    private Vector3 _runParticlesOffset = new Vector3(-0.7f, 0.1f, 0);

    /// <summary>
    /// Posició per defecte de les partícules de sprint quan es mira cap a 1
    /// </summary>
    private Vector3 _sprintParticlesOffset = new Vector3(0, 0.1f, -5);

    /// <summary>
    /// Posició de les partícules del cap amb un offset.
    /// </summary>
    private Vector3 _energyMeterParticlesOffset = new Vector3(0f, 0f, 0.4f);

    public ParticleSystem energyMeterParticleSystem;
    public GameObject sprintTrailsGameObject;
    public GameObject redHotDensityGameObject;
    public GameObject stunGameObject;

    public GameObject bloodBurstGameObject; //esto se podría llamar igual que slide, con play(true) y con foreach PS enableActivate
    public GameObject punchTrailGameObject;

    /// <summary>
    /// Variable con la posicion del PunchTrail respecto al player
    /// </summary>
    private Vector3 _punchTrailGameObjectOffset = new Vector3(0.5f, 1.5f, 2);

    /// <summary>
    /// Variable con la rotacion del PunchTrail respecto al player
    /// </summary>
    private Vector3 _punchTrailGameObjectRotation = new Vector3(0, 0, 0);
    /// <summary>
    /// Variable con la escala del PunchTrail para hacer la rotacion cuando se gira.
    /// </summary>
    private Vector3 _punchTrailGameObjectScale = new Vector3(2.6f, 2.6f, 2.6f);

    private float _sprintParticleSpeed = 9.0f;
    private float _currentSpeed;

    private CharacterControl _control;

    private GameObject particlesGO;

    /// <summary>
    /// Transform del glove, per a treure'n la seva posició
    /// </summary>
    private Transform _glove;

    /// <summary>
    /// Offset per a les partícules de quan intentes disparar i no tens energia
    /// </summary>
    private Vector3 _noEnergyFailShotOffset = new Vector3(0.3f, 0);

    void Awake()
    {
        // Recuperem l'script del control, per a tenir els events
        _control = GetComponent<CharacterControl>();

        particlesGO = new GameObject("particles");
        particlesGO.transform.position = this.transform.position;
        particlesGO.transform.parent = this.gameObject.transform;

        _glove = transform.Find("Mesh/rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_R/ORG-upper_arm_R/ORG-forearm_R/ORG-hand_R");
    }

    void OnEnable()
    {
        StopAllParticles();
        StartCoroutine(LoadFX());

        _control.events.registerEvent(CharacterEvents.EventName.RunEnter, OnRunEnter);
        _control.events.registerEvent(CharacterEvents.EventName.InertiaEnter, OnInertiaEnter);
        _control.events.registerEvent(CharacterEvents.EventName.InertiaExit, OnInertiaExit);
        _control.events.registerEvent(CharacterEvents.EventName.InertiaChangingDirection, OnInertiaChangingDirection);
        _control.events.registerEvent(CharacterEvents.EventName.BounceEnter, OnBounceEnter);
        _control.events.registerEvent(CharacterEvents.EventName.DieByTouch, OnDieEnter);
        _control.events.registerEvent(CharacterEvents.EventName.JumpEnter, OnJumpEnter);
        _control.events.registerEvent(CharacterEvents.EventName.JumpExit, OnJumpExit);
        _control.events.registerEvent(CharacterEvents.EventName.WallGrabEnter, OnWallGrabEnter);
        _control.events.registerEvent(CharacterEvents.EventName.WallGrabExit, OnWallGrabExit);
        _control.events.registerEvent(CharacterEvents.EventName.NoEnergyFailShot, OnNoEnergyFailShot);
        _control.events.registerEvent(CharacterEvents.EventName.DieByNoEnergy, OnDieByNoEnergy);
        _control.events.registerEvent(CharacterEvents.EventName.PowerGloveReleased, OnPowerGloveReleased);
        _control.events.registerEvent(CharacterEvents.EventName.BrakeEnter, OnBrakeEnter);
        _control.events.registerEvent(CharacterEvents.EventName.BrakeExit, OnBrakeExit);

        foreach (InstantParticleSystemEvent ipse in particles)
        {
            if(ipse.prepared)
            {
                ipse.EnableEvents();
            }
        }
    }

    void OnDisable()
    {
        _control.events.unregisterEvent(CharacterEvents.EventName.RunEnter, OnRunEnter);

        _control.events.unregisterEvent(CharacterEvents.EventName.BounceEnter, OnBounceEnter);
        _control.events.unregisterEvent(CharacterEvents.EventName.DieByTouch, OnDieEnter);
        _control.events.unregisterEvent(CharacterEvents.EventName.JumpEnter, OnJumpEnter);
        _control.events.unregisterEvent(CharacterEvents.EventName.JumpExit, OnJumpExit);
        _control.events.unregisterEvent(CharacterEvents.EventName.WallGrabEnter, OnWallGrabEnter);
        _control.events.unregisterEvent(CharacterEvents.EventName.WallGrabExit, OnWallGrabExit);
        _control.events.unregisterEvent(CharacterEvents.EventName.NoEnergyFailShot, OnNoEnergyFailShot);
        _control.events.unregisterEvent(CharacterEvents.EventName.DieByNoEnergy, OnDieByNoEnergy);
        _control.events.unregisterEvent(CharacterEvents.EventName.PowerGloveReleased, OnPowerGloveReleased);
        _control.events.unregisterEvent(CharacterEvents.EventName.BrakeEnter, OnBrakeEnter);
        _control.events.unregisterEvent(CharacterEvents.EventName.BrakeExit, OnBrakeExit);

        foreach (InstantParticleSystemEvent ipse in particles)
        {
            if (ipse.prepared)
            {
                ipse.DisableEvents();
            }
        }
    }

    private void Start()
    {
        // Ho faig nomes una vegada perque el seu parent es don't destory on load
        foreach (InstantParticleSystemEvent ipse in particles)
        {
            ipse.Prepare(_control);
            ipse.EnableEvents();
        }
    }

    /// <summary>
    /// Corutina per instanciar els sistemes de particules i els game objects dels efectes
    /// </summary>
    /// <returns>The F.</returns>
    IEnumerator LoadFX()
    {
        yield return null;

        LoadParticleSystem(ref wallGrabParticleSystem, _wallGrabParticlesOffset, Vector3.zero, particlesGO);
        //estas de sprint se actualizan más abajo en sprintParticlesOffset)
        LoadParticleSystem(ref sprintParticleSystem, this._sprintParticlesOffset, new Vector3(0, 270, 0), particlesGO);
        LoadParticleSystem(ref jumpStartSmokeTrailPS, new Vector3(-0f, 0.1f, -1), new Vector3(90, 0, 0), particlesGO);
        LoadParticleSystem(ref runParticleSystem, new Vector3(-0.7f, 0.1f, 0), new Vector3(0, 270, 0), particlesGO);
        LoadParticleSystem(ref bounceParticleSystem, Vector3.zero, Vector3.zero, particlesGO);
        LoadGameObject(ref bloodBurstGameObject, new Vector3(0, 0.5f, 0), Vector3.zero, false);
        LoadGameObject(ref stunGameObject, new Vector3(-0.2f, 1, 0), new Vector3(0, 110, -10), particlesGO, false);
        LoadGameObject(ref punchTrailGameObject, _punchTrailGameObjectOffset, _punchTrailGameObjectRotation, particlesGO, false);

        LoadParticleSystem(ref brakeParticles, brakeParticlesOffset, Vector3.zero, particlesGO);

        // Emparentades les partícules de cap
        LoadParticleSystem(ref energyMeterParticleSystem, _energyMeterParticlesOffset, Vector3.zero, GameObject.Find("MCH-head_02"));

        if (energyMeterParticleSystem != null)
        {
            energyMeterParticleSystem.Play();
        }
    }

    void OnPowerGloveReleased()
    {
        if (punchTrailGameObject)
        {
            punchTrailGameObject.transform.localPosition = new Vector3(_punchTrailGameObjectOffset.x * _control.LookingDirection, _punchTrailGameObjectOffset.y, 0);
            punchTrailGameObject.transform.localScale = new Vector3(_punchTrailGameObjectScale.x * _control.LookingDirection, _punchTrailGameObjectScale.y, 0);
            punchTrailGameObject.SetActive(true);
            StartCoroutine(HidePunchTrail());
        }
    }

    /// <summary>
    /// Espera i frames y esconde las particulas del punch trail
    /// </summary>
    /// <returns></returns>
	private IEnumerator HidePunchTrail()
    {
        // Esperamos 6 frames
        for (int i = 0; i < 6; i++)
        {
            yield return null;
        }

        punchTrailGameObject.SetActive(false);
    }

    /// <summary>
    /// Mètode que es llença quan el player vol disparar però no té energia
    /// </summary>
    void OnNoEnergyFailShot()
    {
        if (noEnergyFailShot)
        {
            // Com que de moment no tinc les de disparar, faig servir les de jump
            Vector3 position = _glove.position + new Vector3(Mathf.Cos(_noEnergyFailShotOffset.x * _control.LookingDirection), Mathf.Sin(_noEnergyFailShotOffset.y));
            ParticleSystem ps = Instantiate(noEnergyFailShot, position, Quaternion.identity) as ParticleSystem;
            ps.GetOrAddComponent<ParticleSystemAutoDestroy>();
            ps.Play();
        }
    }

    /// <summary>
    /// Llama a la Coroutina que esperara hasta que este grounded para lanzar las particulas
    /// </summary>
    private void OnBrakeEnter()
    {
        StartCoroutine(WaitUntilGrounded());
    }

    /// <summary>
    /// Para las particulas de brake en seco
    /// </summary>
    private void OnBrakeExit()
    {
        brakeParticles.Clear();
        brakeParticles.Stop();
    }

    /// <summary>
    /// Espera hasta que el estado vertical sea grounded y lanza las particulas de brake
    /// </summary>
    /// <returns></returns>
    private IEnumerator WaitUntilGrounded()
    {
        while (_control.movementFSM.currentVertical != PlayerState.Vertical.Grounded)
        {
            yield return null;
        }

        brakeParticles.transform.localPosition = new Vector3(brakeParticlesOffset.x * _control.movementProps.lookingDirection, brakeParticlesOffset.y, brakeParticlesOffset.z);
        brakeParticles.Play();
    }

    void OnJumpEnter()
    {
        if (jumpStartSmokeTrailPS != null)
        {
            jumpStartSmokeTrailPS.Clear();
            jumpStartSmokeTrailPS.Simulate(0.0001f, false, true);
            jumpStartSmokeTrailPS.Play();
        }
    }

    void OnJumpExit()
    {
        if (jumpStartSmokeTrailPS != null)
        {
            jumpStartSmokeTrailPS.Stop();
        }
    }

    void OnRunEnter()
    {
        StartCoroutine(SprintFX());
    }

    /// <summary>
    /// Inicia les partícules de /inertiaPS/ i estem a una velocitat d'sprint o superior
    /// </summary>
    void OnInertiaEnter()
    {
        if (this._control.movementProps.speed >= MovementProperties.SPRINT_SPEED)
        {
            // Sprint o superior
        }
        else
        {
            // Run
        }
    }

    /// <summary>
    /// Para les partícules de /inertiaPS/ i les de /inertiaChangeDirectionPS/
    /// </summary>
    void OnInertiaExit()
    {
        if (this.inertiaPS != null)
        {
            this.inertiaPS.Stop();
        }

        if (this.inertiaChangeDirectionPS != null)
        {
            this.inertiaChangeDirectionPS.Stop();
        }
    }

    /// <summary>
    /// Para les partícules de /inertiaPS/ i engega les de /inertiaChangeDirectionPS/ i estem a una velocitat d'sprint o superior
    /// </summary>
    void OnInertiaChangingDirection()
    {
        if (this._control.movementProps.speed >= MovementProperties.SPRINT_SPEED)
        {
            // Sprint o superior
        }
        else
        {
            // Run
        }
    }

    void OnWallGrabEnter()
    {
        wallGrabParticleSystem.transform.localPosition = new Vector3(_wallGrabParticlesOffset.x * _control.LookingDirection, _wallGrabParticlesOffset.y, 0);

        if (wallGrabParticleSystem)
        {
            wallGrabParticleSystem.Stop();
        }

        wallGrabParticleSystem.Simulate(0.0001f, false, true);
        wallGrabParticleSystem.Play();
    }

    void OnWallGrabExit()
    {
        if (wallGrabParticleSystem)
        {
            wallGrabParticleSystem.Stop();
        }

        Invoke("StopWallGrabParticles", 1F);
    }

    void StopWallGrabParticles()
    {
        if (wallGrabParticleSystem)
        {
            wallGrabParticleSystem.Clear(true);
        }
    }

    void OnBounceEnter()
    {
        if (bounceParticleSystem)
        {
            //bounceParticleSystem.enableEmission = true;
            bounceParticleSystem.Play();
        }
    }

    void OnDieEnter()
    {
        // Debug.Log ("DIE");
        redHotDensityGameObject = Instantiate(GeneralPurposeObjectManager.Instance.RedHotDensity) as GameObject;
        redHotDensityGameObject.transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y + 1, 0);

        if (bloodBurstGameObject)
        {
            bloodBurstGameObject.SetActive(false);
            bloodBurstGameObject.transform.position = transform.position;
            bloodBurstGameObject.SetActive(true);
        }
    }

    void OnDieByNoEnergy()
    {
        if (energyMeterParticleSystem != null)
        {
            energyMeterParticleSystem.Stop();
        }
    }

    IEnumerator SprintFX()
    {
        yield return new WaitForSeconds(0.1f);

        sprintParticleSystem.Clear();
        sprintParticleSystem.transform.localPosition = new Vector3(_sprintParticlesOffset.x * _control.LookingDirection, _sprintParticlesOffset.y, 0);
        sprintParticleSystem.Simulate(0.0001f, true, true);

        runParticleSystem.Clear();
        runParticleSystem.transform.localPosition = new Vector3(_runParticlesOffset.x * _control.LookingDirection, _runParticlesOffset.y, 0);
        runParticleSystem.Simulate(0.0001f, true, true);

        //Debug.Log (sprintParticleSystem.transform.localPosition.x);
        while (_control.movementFSM.currentHorizontal == PlayerState.Horizontal.Run)
        {
            _currentSpeed = _control.movementProps.speed;
            if (sprintParticleSystem && (!sprintParticleSystem.isPlaying && _currentSpeed >= _sprintParticleSpeed && _control.movementFSM.currentVertical == PlayerState.Vertical.Grounded))
            {
                sprintParticleSystem.Play();
                yield return new WaitForSeconds(0.1f);
                // sprintTrailsGameObject.SetActive (true);
            }
            if (_control.movementFSM.currentVertical != PlayerState.Vertical.Grounded || _currentSpeed < _sprintParticleSpeed)
            {
                sprintParticleSystem.Stop();
                // sprintTrailsGameObject.SetActive (false);
            }

            if (runParticleSystem && (!runParticleSystem.isPlaying && _currentSpeed < _sprintParticleSpeed && _control.movementFSM.currentVertical == PlayerState.Vertical.Grounded))
            {
                runParticleSystem.Play();
            }

            if (runParticleSystem && (_currentSpeed > _sprintParticleSpeed || _control.movementFSM.currentVertical != PlayerState.Vertical.Grounded || _control.movementFSM.currentHorizontal == PlayerState.Horizontal.Slide))
            {
                runParticleSystem.Stop();
            }

            yield return null;
        }

        if (sprintParticleSystem)
            sprintParticleSystem.Stop();

        if (runParticleSystem)
            runParticleSystem.Stop();

        if (sprintTrailsGameObject)
        {
            sprintTrailsGameObject.SetActive(false);
        }
    }


    void StopAllParticles()
    {
        foreach (ParticleSystem ps in GetComponentsInChildren<ParticleSystem>())
        {
            ps.Stop();
        }
    }
}

/// <summary>
/// Classe per a sistemes de partícules que són instanciats puntualment
/// </summary>
[System.Serializable]
public class InstantParticleSystemEvent
{
    /// <summary>
    /// Quants sistemes de partícules utilitzem de manera rotativa
    /// </summary>
    public static int SYSTEMS_USED = 3;

    /// <summary>
    /// El sistema de partícules que instanciarem
    /// </summary>
    public ParticleSystem particleSystem;

    /// <summary>
    /// L'esdeveniment que llençarà aquest sistema de partícules
    /// </summary>
    public CharacterEvents.EventName triggerEventStart;

    /// <summary>
    /// Hem de parar les partícules amb un altre esdeveniment?
    /// </summary>
    public bool useTriggerEventStop;

    /// <summary>
    /// L'esdeveniment que para el sistema de partícules (si estava encés)
    /// </summary>
    public CharacterEvents.EventName triggerEventStop;

    /// <summary>
    /// Offset del sistema de partícules (o posició si no fem servir parent)
    /// </summary>
    public Vector3 offset = Vector3.zero;

    /// <summary>
    /// Rotació del sistema de partícules
    /// </summary>
    public Vector3 rotation = Vector3.zero;

    /// <summary>
    /// Si ha de seguir al pare quan s'instancia, o s'executen a la posició 
    /// </summary>
    public bool followParent;

    public bool offsetAtLookingDirection = false;

    [HideInInspector]
    public bool prepared = false;

    /// <summary>
    /// Els /SYSTEMS_USED/ ja generats
    /// </summary>
    private ParticleSystem[] systems;

    /// <summary>
    /// El system que fem servir en aquest moment
    /// </summary>
    private int current;

    /// <summary>
    /// El control del player on registrarem els esdeveniments
    /// </summary>
    private CharacterControl playerControl;

    /// <summary>
    /// Creem els sistemes i registrem l'esdeveniment (si existeix el /particleSystem/)
    /// </summary>
    /// <param name="control">El control d'on treurem els Events</param>
    public void Prepare(CharacterControl control)
    {
        systems = new ParticleSystem[SYSTEMS_USED];

        if (particleSystem)
        {
            for (int i = 0; i < SYSTEMS_USED; i++)
            {
                if (followParent)
                {
                    systems[i] = GameObject.Instantiate(particleSystem, Vector3.zero, Quaternion.Euler(rotation)) as ParticleSystem;
                    systems[i].transform.parent = control.gameObject.transform;
                    systems[i].transform.localPosition = offset;
                }
                else
                {
                    systems[i] = GameObject.Instantiate(particleSystem, offset, Quaternion.Euler(rotation)) as ParticleSystem;
                    systems[i].transform.SetParent(GameObjectPooler.Instance.transform);
                }
            }

            current = 0;

            playerControl = control;
        }

        prepared = true;
    }

    /// <summary>
    /// Funció que cridem al habilitar el CharacterParticles per a registar els Events
    /// </summary>
    public void EnableEvents()
    {
        playerControl.events.registerEvent(triggerEventStart, OnTriggerEventStart);

        if (useTriggerEventStop)
        {
            playerControl.events.registerEvent(triggerEventStop, OnTriggerEventStop);
        }
    }

    /// <summary>
    /// Funció que cridem al deshabilitar aquest sistema de particules per a eliminar els Events
    /// </summary>
    public void DisableEvents()
    {
        playerControl.events.unregisterEvent(triggerEventStart, OnTriggerEventStart);

        if (useTriggerEventStop)
        {
            playerControl.events.registerEvent(triggerEventStop, OnTriggerEventStop);
        }
    }

    /// <summary>
    /// Acció que s'executa al ser cridat l'esdeveniment al que ens hem registrat
    /// </summary>
    void OnTriggerEventStart()
    {
        this.systems[current].Clear();

        if (!followParent)
        {
            this.systems[current].transform.position = this.playerControl.transform.position + new Vector3(offset.x * this.playerControl.movementProps.lookingDirection, this.offset.y, this.offset.z);
        }
        else if (offsetAtLookingDirection)
        {
            this.systems[current].transform.localPosition = new Vector3(offset.x * playerControl.movementProps.lookingDirection, offset.y, offset.z);
        }

        this.systems[current].Simulate(0.0001f, true, true);
        this.systems[current].Play();

        current = (current + 1) % InstantParticleSystemEvent.SYSTEMS_USED;
    }

    void OnTriggerEventStop()
    {
        for (int i = 0; i < InstantParticleSystemEvent.SYSTEMS_USED; i++)
        {
            this.systems[i].Stop();
        }
    }
}

