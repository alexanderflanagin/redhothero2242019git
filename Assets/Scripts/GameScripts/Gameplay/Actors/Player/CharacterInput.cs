using UnityEngine;
using System.Collections;

public class CharacterInput : MonoBehaviour, IUserInput {

    private InputButton _xAxis;

    public InputButton xAxis
    {
        get
        {
            if (_xAxis == null)
            {
                _xAxis = new InputButton();
            }

            return _xAxis;
        }

        set
        {
            _xAxis = value;
        }
    }

    private InputButton _yAxis;

    public InputButton yAxis
    {
        get
        {
            if (_yAxis == null)
            {
                _yAxis = new InputButton();
            }

            return _yAxis;
        }

        set
        {
            _yAxis = value;
        }
    }

    private InputButton _roll;

    public InputButton roll
    {
        get
        {
            if (_roll == null)
            {
                _roll = new InputButton();
            }

            return _roll;
        }

        set
        {
            _roll = value;
        }
    }

    private InputButton _jump;

    public InputButton jump
    {
        get
        {
            if (_jump == null)
            {
                _jump = new InputButton();
            }

            return _jump;
        }

        set
        {
            _jump = value;
        }
    }

    private float _lastJumpDownTime;

    private InputButton _densityJump;

    public InputButton travel
    {
        get
        {
            if (_densityJump == null)
            {
                _densityJump = new InputButton();
            }

            return _densityJump;
        }

        set
        {
            _densityJump = value;
        }
    }

    private InputButton _shoot;

    public InputButton shoot
    {
        get
        {
            if (_shoot == null)
            {
                _shoot = new InputButton();
            }

            return _shoot;
        }

        set
        {
            _shoot = value;
        }
    }

    private InputButton _punch;

    public InputButton punch
    {
        get
        {
            if (_punch == null)
            {
                _punch = new InputButton();
            }

            return _punch;
        }

        set
        {
            _punch = value;
        }
    }

    private InputButton _brake;

    public InputButton brake
    {
        get
        {
            if (_brake == null)
            {
                _brake = new InputButton();
            }

            return _brake;
        }

        set
        {
            _brake = value;
        }
    }

	// Freeze input
	protected bool _freeze = false;
	public bool freeze { get { return _freeze; } }

    /// <summary>
    /// Per a saber si també congel·lem el teleport o no
    /// </summary>
    protected bool _freezeDensityJump = false;

    /// <summary>
    /// El controller ja esta preparat. Mirem que els noms siguin correctes
    /// </summary>
    public void Prepared()
    {
        if (!_xAxis.Prepare())
        {
            Debug.LogWarning("[CharacterInput::Prepared] xAxisName incorrect: " + _xAxis.name);
        }

        if (!_yAxis.Prepare())
        {
            Debug.LogWarning("[CharacterInput::Prepared] yAxisName incorrect: " + _yAxis.name);
        }

        if (!_roll.Prepare())
        {
            Debug.LogWarning("[CharacterInput::Prepared] rollName incorrect: " + _roll.name);
        }

        if (!_jump.Prepare())
        {
            Debug.LogWarning("[CharacterInput::Prepared] jumpName incorrect: " + _jump.name);
        }

        if (!_densityJump.Prepare())
        {
            Debug.LogWarning("[CharacterInput::Prepared] densityJump incorrect: " + _densityJump.name);
        }

        if (!_shoot.Prepare())
        {
            Debug.LogWarning("[CharacterInput::Prepared] shoot incorrect: " + _shoot.name);
        }

        if (!_punch.Prepare())
        {
            Debug.LogWarning("[CharacterInput::Prepared] punch incorrect: " + _punch.name);
        }
    }

    /// <summary>
    /// Freezes the controller, so no input is readed
    /// </summary>
    /// <param name="continueRunning">xAxis value for automatic move purpouses</param>
    /// <param name="cancelJump">If a jump that is currently being did should be stopped</param>
    public void Freeze(int continueRunning = 1, bool cancelJump = false)
    {
        MassiveAutoValue(true);

        _xAxis.autoValueValue = continueRunning;
        _yAxis.autoValueValue = 0;
        _roll.autoValueValue = 0;

        if (cancelJump)
        {
            _jump.autoValueValue = 0;
        }

        _densityJump.autoValueValue = 0;
        _shoot.autoValueValue = 0;
        _punch.autoValueValue = 0;
    }

    /// <summary>
    /// Torna a deixar llegir de l'input
    /// </summary>
    public void Defreeze()
    {
        MassiveAutoValue(false);
    }

    private void MassiveAutoValue(bool value)
    {
        _xAxis.autoValue = value;
        _yAxis.autoValue = value;
        _roll.autoValue = value;
        _jump.autoValue = value;
        _densityJump.autoValue = value;
        _shoot.autoValue = value;
        _punch.autoValue = value;
    }

    /// <summary>
    /// Fa un Freeze(0) però permet seguir fent servir el densityJump
    /// </summary>
    public void FreezeExceptTravel()
    {
        Freeze(0);
        //_freezeDensityJump = false;
        _densityJump.autoValue = false;
    }

    void Update()
    {
        if (_jump.down)
        {
            _lastJumpDownTime = Time.time;
        }
    }

    public bool StillCanJump(float time)
    {
        return _lastJumpDownTime + time > Time.time;
    }

    public void AutoXAxis(int direction, float time)
    {
        StopCoroutine("StopAutoXAxis");
        _xAxis.autoValue = true;
        _xAxis.autoValueValue = (float)direction;
        StartCoroutine("StopAutoXAxis", time);
    }

    protected IEnumerator StopAutoXAxis(float time)
    {
        yield return new WaitForSeconds(time);
        _xAxis.autoValue = false;
    }
}
