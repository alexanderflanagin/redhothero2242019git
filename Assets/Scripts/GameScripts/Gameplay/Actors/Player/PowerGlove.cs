using UnityEngine;
using System.Collections;

public class PowerGlove : MonoBehaviour
{
    /// <summary>
    /// Getter/Setter públic dle controller del propietari. Al regsitrar-lo, també registrem l'esdeveniment de DieEnter
    /// </summary>
    public CharacterControl owner
    {
        get
        {
            return _owner;
        }

        set
        {
            _owner = value;
            _owner.events.registerEvent(CharacterEvents.EventName.DieEnter, OnDieEnter);
        }
    }

    /// <summary>
    /// El tarnsform de l'area d'efecte que es crea
    /// </summary>
    public Transform effectArea;

    /// <summary>
    /// Si està carregants
    /// </summary>
    public bool charging;

    /// <summary>
    /// El transform de la fletxa
    /// </summary>
    public Transform arrow;

    /// <summary>
    /// Explosió que surt al colpejar correctament enemics
    /// </summary>
    public GameObject explosion;

    /// <summary>
    /// Trail que deixen els enemics per defecte
    /// </summary>
    public GameObject defaultTrail;

    /// <summary>
    /// Booleà de debug, per a veure o no l'àrea del collider
    /// </summary>
    public bool showArea = true;

    /// <summary>
    /// Collider que representa l'area que colpeja el puny al ser deixat anar
    /// </summary>
    public CircleCollider2D effectAreaCollider;

    /// <summary>
    /// Fem servir aquesta variable per a tenir una referència de l'últim objecte al que hem fet power glove, per a poder-lo seguir.
    /// En cas de que n'hi hagi més d'un de cop, n'agafarem qualsevol
    /// </summary>
    [HideInInspector]
    public GameObject lastPowerGloved;

    /// <summary>
    /// Llista dels objectes powergloveables que son dins del collider
    /// </summary>
    private Collider2D[] _insidePowergloveables;

    private int insidePowerGloveablesCount;

    /// <summary>
    /// Controller del propietari
    /// </summary>
    private CharacterControl _owner;

    /// <summary>
    /// Vector de correcció de la posició quan mirem cap a la dreta
    /// </summary>
    private Vector3 _positionCorrection;

    /// <summary>
    /// Vector de correcció de la posició quan mirem cap a l'esquerra
    /// </summary>
    private Vector3 _positionCorrectionNegative;

    /// <summary>
    /// El renderer de l'arrow
    /// </summary>
    private Renderer _arrowRenderer;

    /// <summary>
    /// El renderer de l'efect area
    /// </summary>
    private SpriteRenderer effectAreaRenderer;

    /// <summary>
    /// Coroutina de carregar el puny. La guardem per a poder-la cancel·lar
    /// </summary>
    private Coroutine _chargePunch;

    /// <summary>
    /// El centre de l'area d'efecte
    /// </summary>
    private Vector3 effectAreaColliderOffset;

    /// <summary>
    /// El radi de l'area d'efecte
    /// </summary>
    private float effectAreaColliderRadius;

    private Vector3 originalEffectAreaScale;

    public void cancelCharge()
    {
        if (_chargePunch != null)
        {
            _owner.events.triggerEvent(CharacterEvents.EventName.PowerGloveCancelled);
            effectAreaRenderer.enabled = false; // Ja que no ho fara la mateixa coroutina
            StopCoroutine(_chargePunch);
        }
    }

    /// <summary>
    /// Iniciem la llista de powergloveables, carreguem l'arrow i l'explosion
    /// </summary>
    private void Start()
    {
        _arrowRenderer = arrow.GetComponent<Renderer>();

        effectAreaRenderer = effectArea.GetComponent<SpriteRenderer>();
        effectAreaRenderer.enabled = false;
        originalEffectAreaScale = effectArea.localScale;

        explosion = Instantiate(explosion, arrow.transform.position + new Vector3(0, 0, 0.5f), transform.rotation) as GameObject;
        explosion.transform.parent = transform;
        explosion.SetActive(false);

        DebugUtils.AssertError(effectAreaCollider != null, "The Power Glove don't have an effect area");
        effectAreaColliderOffset = effectAreaCollider.offset;
        effectAreaColliderRadius = effectAreaCollider.radius;

        effectAreaCollider.enabled = false;

        _positionCorrection = effectAreaColliderOffset;
        _positionCorrectionNegative = new Vector3(-effectAreaColliderOffset.x, effectAreaColliderOffset.y);
    }

    /// <summary>
    /// Si tenim _owner, desregistrem els esdeveniments
    /// </summary>
    private void OnDisable()
    {
        if (_owner != null)
        {
            _owner.events.unregisterEvent(CharacterEvents.EventName.DieEnter, OnDieEnter);
        }
    }

    /// <summary>
    /// Al morir el player, desactivem el carregar del puny si estava carregant
    /// </summary>
    private void OnDieEnter()
    {
        // Parem el charge power punch
        StopAllCoroutines();
        effectAreaRenderer.enabled = false;
        _owner.events.triggerEvent(CharacterEvents.EventName.PowerGloveCancelled);
        charging = false;
    }
    
    /// <summary>
    /// Mira els PowerGloveables que te dins l'area del collider, i guarda el numero descartant aquells colliders que no ho son
    /// </summary>
    private void CheckPowerGloveables()
    {
        _insidePowergloveables = new Collider2D[10];
        insidePowerGloveablesCount = Physics2D.OverlapCircleNonAlloc(transform.position + effectAreaColliderOffset, effectAreaColliderRadius, _insidePowergloveables);

        int originalCount = insidePowerGloveablesCount;

        // Eliminem els elements colpejats que no son colpejables
        for (int i = 0; i < originalCount; i++)
        {
            if (_insidePowergloveables[i] != null)
            {
                EnemyController enemy = _insidePowergloveables[i].GetComponent<EnemyController>();
                if(_insidePowergloveables[i].gameObject.layer == Layers.FinalBoss)
                {
                    if(((FinalBossController)enemy).ShouldIgnoreDamage(true))
                    {
                        _insidePowergloveables[i] = null;
                        insidePowerGloveablesCount--;
                    }
                }
                else if ((enemy != null && !enemy.puncheable) || _insidePowergloveables[i].GetComponent<PowerGloved>() || _insidePowergloveables[i].GetComponent<IPowerGloveable>() == null)
                {
                    _insidePowergloveables[i] = null;
                    insidePowerGloveablesCount--;
                }
            }
        }
    }

    /// <summary>
    /// Colpeja els PowerGloveables que tingues dins de l'area
    /// </summary>
    private void PunchInsidePowerGloveables()
    {
        foreach (Collider2D go in _insidePowergloveables)
        {
            if (go == null)
            {
                // L'objecte ja ha estat destruit. El treiem de la llista i tornem al bucle
                continue;
            }

            IPowerGloveable powerGloveable = go.GetComponent<IPowerGloveable>();

            // Mirem si es pot colpejar, i si es pot mirem si no ha estat ja colpejat
            if (powerGloveable != null && !go.GetComponent<PowerGloved>())
            {
                Vector3 direction = _owner.playerAim.getLookingAtPointCorrectedPunch();

                if(!this.GetComponent<BreakableElement>())
                {
                    // Pujem el powergloved 0.1f per a que no toqui el terra al ser colpejat
                    go.transform.position += new Vector3(0, 0.1f, 0);
                }

                if (_owner.playerAim.target)
                {
                    Vector3 dir = (_owner.playerAim.target.transform.position + (Vector3)_owner.playerAim.target.GetComponent<Collider2D>().offset) - (go.transform.position + (Vector3)go.GetComponent<Collider2D>().offset);

                    powerGloveable.PowerGloved(Vector3.Normalize(dir));
                }
                else
                {
                    powerGloveable.PowerGloved(direction);
                }

                StandardStageManager.current.KilledByPlayer(_owner.gameObject, go.gameObject);

                lastPowerGloved = go.gameObject;
            }
        }

        if (lastPowerGloved)
        {
            lastPowerGloved.transform.GetInterface<IPowerGloveable>().theChosen = true;
        }
    }

    /// <summary>
    /// Coroutina de carregar el puny. Llença els Events necessaris. Manté la posició de carregar fins que es deixa anar el botó, després colpeja a tots els elements dins de la llista
    /// </summary>
    /// <returns></returns>
    private IEnumerator ChargePowerPunch()
    {
        bool explosionShowed = false;

        charging = true;

        _owner.events.triggerEvent(CharacterEvents.EventName.PowerGloveCharge);

        while (_owner.input.punch.check)
        {
            yield return null;
        }

        _owner.events.triggerEvent(CharacterEvents.EventName.PowerGloveReleased);

        // Mirem si tenim elements dins
        CheckPowerGloveables();

        if (insidePowerGloveablesCount > 0)
        {
            StartCoroutine(ShowExplosion());
            explosionShowed = true;
            _owner.events.triggerEvent(CharacterEvents.EventName.ObjectPowerGloved);
        }
        else
        {
            _owner.events.triggerEvent(CharacterEvents.EventName.NothingPoweredFisting);
        }

        charging = false;

        // Comencem a colpejar
        PunchInsidePowerGloveables();
        StartCoroutine(ShowEffectAreaRenderer());

        // Espera tot el temps de l'animació actual abans d'acabar-se
        float time = 0;
        float totalRecoveryTime = 0.1f;
        while (time < totalRecoveryTime)
        {
            // Cada frame, revisem si tenim elements dins i si es així els colpejem
            CheckPowerGloveables();

            if (insidePowerGloveablesCount > 0)
            {
                if(!explosionShowed)
                {
                    StartCoroutine(ShowExplosion());
                    explosionShowed = true;
                }

                _owner.events.triggerEvent(CharacterEvents.EventName.ObjectPowerGloved);
            }

            PunchInsidePowerGloveables();

            time += Time.deltaTime;
            yield return null;
        }

        // Acabem de colpejar
        effectAreaRenderer.enabled = false;

        _owner.events.triggerEvent(CharacterEvents.EventName.PowerGloveMovementEnd);
    }

    /// <summary>
    /// Mostra el grafic de l'area d'efecte del puny fent un fade in
    /// </summary>
    /// <returns></returns>
    private IEnumerator ShowEffectAreaRenderer()
    {
        Color color = effectAreaRenderer.color;
        color.a = 0;
        effectAreaRenderer.color = color;
        effectArea.rotation = Quaternion.Euler(0, 90 - 90 * _owner.LookingDirection, 0);
        effectAreaRenderer.enabled = true;

        while (color.a < 1)
        {
            color.a += Time.deltaTime * 10;
            effectAreaRenderer.color = color;
            yield return null;
        }
    }

    private IEnumerator ShowExplosion()
    {
        explosion.transform.position = transform.position + effectAreaColliderOffset + new Vector3(0, 0, 0.5f); // + Vector3.right * _owner.LookingDirection;
        explosion.SetActive(true);
        explosion.GetComponent<ParticleSystem>().Play();
        yield return new WaitForSeconds(0.4f);
        explosion.SetActive(false);
    }

    private void Update()
    {
        transform.position = _owner.transform.position;
        if (_owner.LookingDirection == 1)
        {
            effectAreaColliderOffset = _positionCorrection;
            arrow.transform.localPosition = new Vector3(-0.5f, 0, 0);
        }
        else
        {
            effectAreaColliderOffset = _positionCorrectionNegative;
            arrow.transform.localPosition = new Vector3(0.5f, 0, 0);
        }

        if (_owner.input.punch.down && canPowerGlove())
        {
            _chargePunch = StartCoroutine(ChargePowerPunch());
        }

        if (_owner.input.punch.check)
        {
            _arrowRenderer.enabled = true;
            
            // _arrow.transform.position = transform.position + _owner.playerAim.getLookingAtPoint();
            arrow.transform.position = _owner.transform.position + (Vector3)_owner.GetComponent<Collider2D>().offset + _owner.playerAim.getLookingAtPoint() * 2;
        }
        else
        {
            _arrowRenderer.enabled = false;
        }

        // Si estem veient l'sprite del effect area, el rotem
        if(effectAreaRenderer.enabled)
        {
            effectArea.Rotate(0, 0, 800 * Time.deltaTime);
        }
    }


    /// <summary>
    /// Mirem si podem fer power glove segons l'estat del player (no crouch, no roll, no wallgrab)
    /// </summary>
    /// <returns>true si podem, false altrament</returns>
    private bool canPowerGlove()
    {
        return _owner.movementFSM.currentHorizontal != PlayerState.Horizontal.Crouch && _owner.movementFSM.currentHorizontal != PlayerState.Horizontal.Roll && _owner.movementFSM.currentVertical != PlayerState.Vertical.WallGrab;
    }

    void OnDrawGizmos()
    {
        if (showArea)
        {
            //CircleCollider2D collider = this.GetComponent<CircleCollider2D>();
            Gizmos.DrawWireSphere(transform.position + effectAreaColliderOffset, effectAreaColliderRadius * this.transform.localScale.x);
        }
    }
}
