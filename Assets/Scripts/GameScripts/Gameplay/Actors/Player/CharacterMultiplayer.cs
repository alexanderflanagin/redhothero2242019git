using UnityEngine;
using System.Collections;

public class CharacterMultiplayer : MonoBehaviour {

	private Physics2DController _controller;

	private CharacterControl _control;

    //private float _stunTime = 1.5f;

	/// <summary>
    /// La LayerMask que portava per defecte el Guiri
	/// </summary>
	private LayerMask _defaultLayerMask;

	/// <summary>
    /// Una LayerMask sense col·lisio amb els altres players
	/// </summary>
	private LayerMask _noGuirisCollision;

	/// <summary>
    /// Per guardar l'ultim estat (per saber quan comença i acaba un roll
	/// </summary>
    //private CharacterControl.StateHorizontal _lastHorizontal;

	/// <summary>
    /// Per saber si estem fent un roll
	/// </summary>
    private bool _roll;

	// Use this for initialization
	void Awake () {
		_control = GetComponent<CharacterControl>();
		_controller = GetComponent<Physics2DController>();

        //StartCoroutine ( "CheckBounceAndStun" );

        //_lastHorizontal = _control.currentHorizontal;

		_defaultLayerMask = _controller.collisionMask;

        //_noGuirisCollision = ( 1 << LayerMask.NameToLayer("Ground") ) | ( 1 << LayerMask.NameToLayer("Enemy") );

        _noGuirisCollision = _controller.collisionMask & ~(1 << Layers.Player0) & ~(1 << Layers.Player1) & ~(1 << Layers.Player2) & ~(1 << Layers.Player3);

        //StartCoroutine( "CheckRollLayerMask" );
	}

    /// <summary>
    /// Al habilitar registrem esdeveniments de grounded i roll
    /// </summary>
    void OnEnable()
    {
        _controller.BlockedBelowEnter += OnBlockedDown;

        _control.events.registerEvent(CharacterEvents.EventName.RollEnter, OnRollEnter);
        _control.events.registerEvent(CharacterEvents.EventName.RollExit, OnRollExit);
        
    }

    /// <summary>
    /// Al acabar, li treiem els esdeveniments
    /// </summary>
    void OnDisable()
    {
        _controller.BlockedBelowEnter -= OnBlockedDown;

        _control.events.unregisterEvent(CharacterEvents.EventName.RollEnter, OnRollEnter);
        _control.events.unregisterEvent(CharacterEvents.EventName.RollExit, OnRollExit);
    }

    /// <summary>
    /// Si està bloquejat per sota per un Player, fem bounce
    /// </summary>
    void OnBlockedDown()
    {
        if (_controller.collisions.belowObject != null && _controller.collisions.belowObject.CompareTag(Tags.Player))
        {
            _control.EnterBounce();
        }
    }

    /// <summary>
    /// Al començar un roll, el deixem atravessar Players
    /// </summary>
    void OnRollEnter()
    {
        _defaultLayerMask = _controller.collisionMask;
        _roll = true;
        _controller.UpdateLayerMask(_noGuirisCollision);
    }

    /// <summary>
    /// Al acabar un roll, li tornem a posar la LayerMask normal
    /// </summary>
    void OnRollExit()
    {
        _roll = false;
        _controller.UpdateLayerMask(_defaultLayerMask);
    }

    /// <summary>
    /// Si detectem trigger amb un altre player que no està fent bounce, li fem fer
    /// </summary>
    /// <param name="other"></param>
    void OnTriggerEnter2D(Collider2D other)
    {
        if (_roll && other.gameObject.CompareTag(Tags.Player))
        {
            CharacterControl otherControl = other.gameObject.GetComponent<CharacterControl>();
            if (otherControl)
            {
                if (otherControl.movementFSM.currentVertical != PlayerState.Vertical.Bounce)
                {
                    otherControl.EnterBounce();
                }
            }
        }
    }

    //private IEnumerator CheckBounceAndStun() {
    //    while( true ) {
    //        if( _controller.grounded && _controller.groundColliderObject != null && _controller.groundColliderObject.tag == "Player" ) {
    //            // Enter bounce
    //            _control.EnterBounce();
    //        }

    //        if( _controller.blockedUp && _controller.topColliderObject.tag == "Player" ) {
    //            // Mirem que l'altre no estigui Stun, per a no entrar en bucles eterns
    //            if( _controller.topColliderObject.GetComponent<CharacterControl>().currentAlive == CharacterControl.StateAlive.Stunned ) {
    //                _control.EnterBounce();
    //            } else {
    //                // Enter stunt
    //                //_control.EnterStun( _stunTime );
    //            }

    //        }
    //        yield return null;
    //    }
    //}

    //private IEnumerator CheckRollLayerMask() {
    //    while( true ) {
    //        if( _control.currentHorizontal == CharacterControl.StateHorizontal.Roll && _lastHorizontal != _control.currentHorizontal  ) {
    //            _roll = true;
                
				
    //        } else if ( _lastHorizontal == CharacterControl.StateHorizontal.Roll && _lastHorizontal != _control.currentHorizontal ) {
    //            _roll = false;
                
    //        }
    //        _lastHorizontal = _control.currentHorizontal;
    //        yield return null;
    //    }
    //}
}
