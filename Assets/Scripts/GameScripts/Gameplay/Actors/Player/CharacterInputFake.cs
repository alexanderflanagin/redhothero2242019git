using UnityEngine;
using System.Collections;

//public class CharacterInputFake : MonoBehaviour, IUserInput {
	
//    public int xAxisDirection;

//    public int yAxisDirection;

//    public bool jumping;

//    public bool jumpingDown;

//    public bool sprinting;

//    public bool rolling;

//    public bool powerUpDown { get; set; }

//    // Handles direction
//    protected int _xAxis;
//    public int xAxis { get { return _xAxis; } set { _xAxis = value; }  }
//    public string xAxisName { get; set; }
//    protected bool _autoXAxis = false;
//    public bool autoXAxis { get; set; }
	
//    // Needed for crouch
//    protected int _yAxis;
//    public int yAxis { get { return _yAxis; } }
//    public string yAxisName { get; set; }
	
//    // Sprint button
//    protected bool _sprint;
//    public bool sprint { get { return _sprint; } }
//    public string sprintName { get; set; }
	
//    // Dash button
//    protected bool _dash;
//    public bool roll { get { return _dash; } }
//    protected bool _dashDown, canDashDown = true;
//    public bool rollDown { get { return _dashDown; } }
//    public string rollName { get; set; }
	
//    // Jump button
//    protected bool _jump;
//    public bool jump { get { return _jump; } }
//    protected bool _jumpDown;
//    protected float _lastJumpDownTime;
//    public bool jumpDown { get { return _jumpDown; } }
//    public string jumpName { get; set; }
	
//    // Power Up
//    protected bool _powerUp;
//    public string powerUpName { get; set; }
//    public bool powerUp { get { return _powerUp; } }
	
//    // Freeze input
//    protected bool _freeze = false;
//    public bool freeze { get { return _freeze; } }

//    protected bool _densityJump;
//    public bool densityJump { get { return _densityJump; } }
//    public string densityJumpName { get; set; }
//    protected bool _densityJumpDown;
//    public bool densityJumpDown { get { return _densityJumpDown; } }

//    public void Prepared () {
//        StartCoroutine( FakeInput() );
//    }
	
//    private IEnumerator FakeInput () {
//        Vector3 position = transform.position;
//        xAxisDirection = 0;
//        int direction = 1;
//        sprinting = true;
//        yield return new WaitForSeconds(0.5f);
//        while (true)
//        {
//            xAxisDirection = direction;
//            // jumping = false;
//            // jumpingDown = false;

//            yield return new WaitForSeconds(0.4f);

//            //sprinting = true;
//            jumping = true;
//            jumpingDown = true;

//            yield return null;

//            jumpingDown = false;
//            yield return new WaitForSeconds(0.5f);

//            jumping = false;

//            yield return new WaitForSeconds(0.6f);

//            direction *= -1;
//        }
		

//        //jumping = false;

//        //yield return new WaitForSeconds(1f);

//        //while(true) {
//        //    transform.position = position;
//        //    jumpingDown = true;
//        //    jumping = true;
//        //    yield return null;
//        //    jumpingDown = false;
//        //    yield return new WaitForSeconds(0.2f);
//        //    jumping = false;
//        //    yield return new WaitForSeconds(3);
//        //    jumpingDown = true;
//        //    yield return null;
//        //    jumpingDown = false;
//        //    yield return new WaitForSeconds(1f);
//        //    xAxisDirection = 0;
//        //    sprinting = false;
//        //    yield return new WaitForSeconds(1);
//        //}
//    }

//    void Update() {
//        _xAxis = xAxisDirection;
		
//        _yAxis = yAxisDirection;
		

//        _sprint = sprinting;
		

//        _dash = rolling;
//        _dashDown = !_dashDown && _dash && canDashDown;
//        canDashDown = !_dash;

//        _jump = jumping;
			
//        _jumpDown = jumpingDown;
		
//        if( _jumpDown ) {
//            _lastJumpDownTime = Time.time;
//        }

//        _powerUp = powerUpDown;
//    }

//    public void Freeze( int continueRunning = 1, bool cancelJump = false ) { }

//    public void Defreeze() { }

//    public bool StillCanJump( float time ) { return true; }

//    public void AutoXAxis( int direction, float time ) { }


//    public void FreezeExceptTeleport()
//    {
//        throw new System.NotImplementedException();
//    }
//}
