using UnityEngine;
using System.Collections;

/// <summary>
/// Estat que es dona quan el player est� en WallGrab i salta
/// </summary>
public class WallJumpVerticalState : PlayerStateVertical
{
    /// <summary>
    /// Retorna l'estat de WallJumpVertical
    /// </summary>
    public override PlayerState.Vertical currentState
    {
        get { return Vertical.WallJump; }
    }

    /// <summary>
    /// Al crear l'estat li assignem el seu control
    /// </summary>
    /// <param name="characterControl">El control del player propietari de la FSM</param>
    public WallJumpVerticalState(CharacterControl characterControl)
    {
        this.control = characterControl;
    }

    /// <summary>
    /// Posem el character de cara i li apliquem la força de salt
    /// </summary>
    public override void StartState()
    {
        // Event de que hem entrat
        this.control.events.triggerEvent(CharacterEvents.EventName.WallJumpEnter);

        // TODO hem de modificar l'estat horitzontal?
        this.control.movementFSM.ChangeCurrentHorizontalState(Horizontal.WallJump);
        
        if (this.control.fullWallGrab(this.control.movementProps.runDirection, this.control.collider2D.bounds.extents.x))
        {
            //  Anem al wall grab de cara
            this.control.movementProps.runDirection = -this.control.movementProps.runDirection;
            if (this.control.movementProps.runDirection != this.control.movementProps.lookingDirection)
            {
                this.control.RotateMainChar();
            }
        }

        // Posem la velocitat de sortida
        this.control.jumpProperties.verticalSpeed = JumpProperties.WALL_JUMP_HEIGHT;
    }

    /// <summary>
    /// Li apliquem la força de la gravetat a cada iteració
    /// </summary>
    public override void UpdateState()
    {
        this.control.jumpProperties.verticalSpeed -= this.control.jumpProperties.currentGravity * Time.deltaTime;
    }

    /// <summary>
    /// Mirem si hem d'acabar l'estat i anar a WallJump, WallGrab o JumpFall
    /// </summary>
    /// <returns>Cert si hem de canviar l'estat</returns>
    public override bool ShouldChangeState()
    {
        // Estem a punt de tocar una paret i volem saltar
        // IMPORTANT: ens saltem el pas de wall grab, fem directament un altre wall jump
        if(this.control.canWallJump() && this.control.input.jump.down)
        {
            this.nextState = Vertical.WallJump;
            return true;
        }

        // Mirem si estem tocant una paret i hem deixat de pujar
        if(this.control.canWallJump() && this.control.jumpProperties.verticalSpeed <= 0)
        {
            this.nextState = Vertical.WallGrab;
            return true;
        }

        // Mirem si hem començat a caure
        if(this.control.jumpProperties.verticalSpeed <= 0)
        {
            this.nextState = Vertical.JumpFall;
            return true;
        }

        return false;
    }

    /// <summary>
    /// Cridem l'event de que hem acabat
    /// </summary>
    public override void EndState()
    {
        this.control.events.triggerEvent(CharacterEvents.EventName.WallJumpExit);
    }
}
