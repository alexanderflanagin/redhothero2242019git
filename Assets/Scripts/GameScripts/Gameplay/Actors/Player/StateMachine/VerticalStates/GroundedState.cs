using UnityEngine;
using System.Collections;

/// <summary>
/// Estat vertical Grounded
/// </summary>
public class GroundedState : PlayerStateVertical
{
    /// <summary>
    /// Retorna l'estat de Grounded
    /// </summary>
    public override PlayerState.Vertical currentState
    {
        get
        {
            return PlayerState.Vertical.Grounded;
        }
    }

    /// <summary>
    /// Al crear l'estat li assignem el seu control
    /// </summary>
    /// <param name="cc">El control del player propietari de la FSM</param>
    public GroundedState(CharacterControl cc)
    {
        this.control = cc;
    }

    /// <summary>
    /// Cridem l'Event de que hem entrat a grounded
    /// </summary>
    public override void StartState()
    {
        control.events.triggerEvent(CharacterEvents.EventName.GroundedEnter);

        // Tallem els bounces si n'estavem fent
        control.jumpProperties.bounceCount = 0;
        control.jumpProperties.fallingTime = 0;
    }

    /// <summary>
    /// Li donem velocitat negativa per a actualitzar be el controller
    /// </summary>
    public override void UpdateState()
    {
        // Mantenim sempre una velocitat negativa, per a detectar les col.lisions per sota
        control.jumpProperties.verticalSpeed = -1;
    }

    /// <summary>
    /// Mirem si saltem o caiem
    /// </summary>
    /// <returns></returns>
    public override bool ShouldChangeState()
    {
        if(control.movementFSM.currentHorizontal == Horizontal.Brake)
        {
            // No podem fer res si estem en Brake
            return false;
        }
        
        // Saltem?
        if((this.control.input.jump.down || this.control.input.jump.check && this.control.input.StillCanJump(JumpProperties.PREVIOS_JUMP_PRESSED_CAN_JUMP)) && this.control.jumpProperties.canJump)
        {
            this.nextState = Vertical.Jump;
            return true;
        }

        // Caiem?
        if(!this.control.controller.collisions.below)
        {
            this.nextState = Vertical.Fall;
            return true;
        }

        return false;
    }

    /// <summary>
    /// No fem res
    /// </summary>
    public override void EndState()
    {
        // Do Nothing
    }
}
