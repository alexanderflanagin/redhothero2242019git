using UnityEngine;
using System.Collections;

/// <summary>
/// Estat que es dona quan ens separem d'un Wall Grab canviant el joystick de direccio
/// Durant uns frames encara podem fer WallJump o tornar al WallGrab, com si no ho haguessim fet
/// Si triguem massa, anem a JumpFall
/// </summary>
public class WallGrabSeparatedState : PlayerStateVertical
{
    /// <summary>
    /// Frames durant els quals té temps de rectificar el WallGrab
    /// </summary>
    private int rectifyFrames = 10;

    /// <summary>
    /// Frames que hem estat esperant abans de fer el JumpFall
    /// </summary>
    private int iterations;
    
    /// <summary>
    /// Retorna l'estat de Wall Grab Separated
    /// </summary>
    public override PlayerState.Vertical currentState
    {
        get { return Vertical.WallGrabSeparated; }
    }

    /// <summary>
    /// Al crear l'estat li assignem el seu control
    /// </summary>
    /// <param name="characterControl">El control del player propietari de la FSM</param>
    public WallGrabSeparatedState(CharacterControl characterControl)
    {
        this.control = characterControl;
    }

    /// <summary>
    /// Comencem a contar frames
    /// </summary>
    public override void StartState()
    {
        this.iterations = 0;
    }

    /// <summary>
    /// Anem sumant frames d'iteracions
    /// </summary>
    public override void UpdateState()
    {
        this.iterations++;
    }

    /// <summary>
    /// Mirem si hem rectificat l'estat a Wall Jump o Wall Grab, o ja han passat tots els frames i anem a Jump Fall
    /// </summary>
    /// <returns>Cert si hem de canviar d'estat</returns>
    public override bool ShouldChangeState()
    {
        if (this.control.input.jump.down || (this.control.input.jump.check && this.control.input.StillCanJump(JumpProperties.PREVIOS_JUMP_PRESSED_CAN_JUMP)))
        {
            this.nextState = Vertical.WallJump;
            return true;
        }

        if (this.control.input.xAxis.absoluteVal != this.control.movementProps.runDirection)
        {
            this.nextState = Vertical.WallGrab;
            return true;
        }

        if(this.iterations > this.rectifyFrames)
        {
            this.nextState = Vertical.JumpFall;
            return true;
        }

        return false;
    }

    /// <summary>
    /// No fem res
    /// </summary>
    public override void EndState()
    {
        // Do nothing
    }
}
