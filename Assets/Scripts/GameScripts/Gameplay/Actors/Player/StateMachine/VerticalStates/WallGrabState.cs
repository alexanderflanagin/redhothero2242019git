using UnityEngine;
using System.Collections;

/// <summary>
/// Estat vertical que te el player quan esta enganxat a una paret.
/// </summary>
/// <remarks>
/// Si es salta, es passa a fer un jump.
/// Si s'arriba al terra, es va a grounded
/// Si es separa de la paret, es fa un jumpFall normal (no es podra saltar des d'aquesta posicio)
/// Si no es cap de les anteriors, simplement es un Fall
/// </remarks>
public class WallGrabState : PlayerStateVertical
{
    /// <summary>
    /// Per saber la direcci� de moviment que duiem a l'entrar
    /// </summary>
    private int runDirectionAtEnter;
    
    /// <summary>
    /// Retorna l'estat de WallGrab
    /// </summary>
    public override PlayerState.Vertical currentState
    {
        get { return Vertical.WallGrab; }
    }

    /// <summary>
    /// Al crear l'estat li assignem el seu control
    /// </summary>
    /// <param name="characterControl">El control del player propietari de la FSM</param>
    public WallGrabState(CharacterControl characterControl)
    {
        this.control = characterControl;
    }

    /// <summary>
    /// Ens assegurem que la velocitat, al comen�ar a caure, sigui 0, i guardem la direcci� amb la que hem entrat
    /// </summary>
    public override void StartState()
    {
        // Event de que hem entrat
        this.control.events.triggerEvent(CharacterEvents.EventName.WallGrabEnter);
        
        // Tallem els bounces si n'estavem fent
        this.control.jumpProperties.bounceCount = 0;

        // Si estava saltant ho tallem
        this.control.jumpProperties.verticalSpeed = 0;

        this.runDirectionAtEnter = this.control.movementProps.runDirection;
    }

    /// <summary>
    /// Li apliquem la gravetat de la cauiguda tenint en compte la fricci�
    /// </summary>
    public override void UpdateState()
    {
        this.control.jumpProperties.verticalSpeed -= this.control.jumpProperties.currentGravity / JumpProperties.WALL_GRAB_FRICTION * Time.deltaTime;
    }

    /// <summary>
    /// Mirem si hem d'anar a JumpFall, Grounded o WallJump
    /// </summary>
    /// <returns></returns>
    public override bool ShouldChangeState()
    {
        if(this.control.controller.collisions.below)
        {
            this.nextState = Vertical.Grounded;
            return true;
        }

        if ((this.control.input.jump.down || this.control.input.jump.check && this.control.input.StillCanJump(JumpProperties.PREVIOS_JUMP_PRESSED_CAN_JUMP)) && this.control.jumpProperties.canJump)
        {
            this.nextState = Vertical.WallJump;
            return true;
        }

        if (this.control.movementProps.runDirection != this.runDirectionAtEnter || !this.control.fullWallGrab(this.control.movementProps.lookingDirection, MovementProperties.WALL_GRAB_DISTANCE_CHECKER))
        {
            this.nextState = Vertical.JumpFall;
            return true;
        }

        return false;
    }

    /// <summary>
    /// Cridem l'event de que hem acabat
    /// </summary>
    public override void EndState()
    {
        this.control.events.triggerEvent(CharacterEvents.EventName.WallGrabExit);
    }
}
