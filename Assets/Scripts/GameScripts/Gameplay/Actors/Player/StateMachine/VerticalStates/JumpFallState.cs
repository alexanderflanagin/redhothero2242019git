using UnityEngine;
using System.Collections;

/// <summary>
/// Estat vertical JumpFall
/// </summary>
public class JumpFallState : PlayerStateVertical
{
    /// <summary>
    /// Retorna l'estat de JumpFall
    /// </summary>
    public override PlayerState.Vertical currentState
    {
        get { return Vertical.JumpFall; }
    }

    /// <summary>
    /// Al crear l'estat li assignem el seu control
    /// </summary>
    /// <param name="characterControl">El control del player propietari de la FSM</param>
    public JumpFallState(CharacterControl characterControl)
    {
        this.control = characterControl;
    }

    /// <summary>
    /// Ens assegurem de que no entrem a una velocitat positiva al JumpFall
    /// </summary>
    public override void StartState()
    {
        DebugUtils.AssertWarning(this.control.jumpProperties.verticalSpeed < 0, "Hem entrat al JumpFall i teniem una velocitat positiva", this.control.gameObject);

        // Event de que hem entrat
        control.events.triggerEvent(CharacterEvents.EventName.JumpFallEnter);

        control.jumpProperties.fallingTime = 0;
    }

    /// <summary>
    /// Li aplico gravetat
    /// </summary>
    public override void UpdateState()
    {
        this.control.jumpProperties.verticalSpeed -= this.control.jumpProperties.currentGravity * Time.deltaTime;
        control.jumpProperties.fallingTime += Time.deltaTime;
    }

    /// <summary>
    /// Miro si haig d'anar a WallGrab, WallJump o Grounded
    /// </summary>
    /// <returns>Cert si haig de canviar d'estat</returns>
    public override bool ShouldChangeState()
    {
        if (this.control.fullWallGrab(this.control.movementProps.lookingDirection, MovementProperties.WALL_GRAB_DISTANCE_CHECKER))
        {
            this.nextState = Vertical.WallGrab;
            return true;
        }

        if ((this.control.canWallJump()) && this.control.input.jump.down)
        {
            this.nextState = Vertical.WallJump;
            return true;
        }

        if(this.control.controller.collisions.below)
        {
            this.nextState = Vertical.Grounded;
            return true;
        }

        return false;
    }

    /// <summary>
    /// No fem res
    /// </summary>
    public override void EndState()
    {
        // Do nothing
    }
}
