using UnityEngine;
using System.Collections;

/// <summary>
/// Estat vertical que adquireix el player quan cau, però no per acció d'haver acabat un salt
/// </summary>
public class FallState : PlayerStateVertical
{
    /// <summary>
    /// Quan hem començat. Per saber si encara podem saltar
    /// </summary>
    private float startTime;
    
    /// <summary>
    /// Retorna l'estat de Fall
    /// </summary>
    public override PlayerState.Vertical currentState
    {
        get { return Vertical.Fall; }
    }

    /// <summary>
    /// Al crear l'estat li assignem el seu control
    /// </summary>
    /// <param name="characterControl">El control del player propietari de la FSM</param>
    public FallState(CharacterControl characterControl)
    {
        this.control = characterControl;
    }

    /// <summary>
    /// Ens assegurem de que no entrem a una velocitat positiva al JumpFall
    /// </summary>
    public override void StartState()
    {
        // Event de que hem entrat
        this.control.events.triggerEvent(CharacterEvents.EventName.FallEnter);
        
        this.startTime = Time.time;
        control.jumpProperties.fallingTime = 0;
    }

    /// <summary>
    /// Li aplico gravetat
    /// </summary>
    public override void UpdateState()
    {
        this.control.jumpProperties.verticalSpeed -= this.control.jumpProperties.currentGravity * Time.deltaTime;
        control.jumpProperties.fallingTime += Time.deltaTime;
    }

    /// <summary>
    /// Miro si haig d'anar a WallGrab, Jump o Grounded
    /// </summary>
    /// <returns>Cert si haig de canviar d'estat</returns>
    public override bool ShouldChangeState()
    {
        if ((this.startTime + JumpProperties.FALLING_TIME_CAN_JUMP > Time.time && this.control.input.jump.down) && this.control.jumpProperties.canJump)
        {
            this.nextState = Vertical.Jump;
            return true;
        }

        if (this.control.fullWallGrab(this.control.movementProps.lookingDirection, MovementProperties.WALL_GRAB_DISTANCE_CHECKER))
        {
            this.nextState = Vertical.WallGrab;
            return true;
        }

        if (this.control.controller.collisions.below)
        {
            this.nextState = Vertical.Grounded;
            return true;
        }

        return false;
    }

    /// <summary>
    /// No fem res
    /// </summary>
    public override void EndState()
    {
        // Do nothing
    }
}
