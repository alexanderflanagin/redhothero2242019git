using UnityEngine;
using System.Collections;

/// <summary>
/// Estat vertical al que anem quan es fa un travel
/// </summary>
public class TravelVerticalState : PlayerStateVertical
{
    /// <summary>
    /// Retorna l'estat de Travel Vertical
    /// </summary>
    public override PlayerState.Vertical currentState
    {
        get { return Vertical.Travel; }
    }

    /// <summary>
    /// Al crear l'estat li assignem el seu control
    /// </summary>
    /// <param name="characterControl">El control del player propietari de la FSM</param>
    public TravelVerticalState(CharacterControl characterControl)
    {
        this.control = characterControl;
    }

    /// <summary>
    /// No fem res
    /// </summary>
    public override void StartState()
    {
        // Do nothing
    }

    /// <summary>
    /// No fem res
    /// </summary>
    public override void UpdateState()
    {
        // Do nothing
    }

    /// <summary>
    /// Mirem si hem acabat el travel des de fora i anem a Inertia
    /// </summary>
    /// <returns></returns>
    public override bool ShouldChangeState()
    {
        if(this.control.movementFSM.currentVertical != Vertical.Travel)
        {
            this.nextState = Vertical.Inertia;
            return true;
        }

        return false;
    }

    /// <summary>
    /// No fem res
    /// </summary>
    public override void EndState()
    {
        // Do nothing
    }
}
