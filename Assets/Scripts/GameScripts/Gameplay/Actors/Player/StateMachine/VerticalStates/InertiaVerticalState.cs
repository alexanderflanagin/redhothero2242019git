using UnityEngine;
using System.Collections;

/// <summary>
/// Es una inèrcia vertical de pujada
/// Mentre no xoquem per dalt, ni començem a caure, la tenim. Quan comencem a caure, anem a Fall
/// </summary>
public class InertiaVerticalState : PlayerStateVertical
{
    /// <summary>
    /// Retorna l'estat d'Inertia vertical
    /// </summary>
    public override PlayerState.Vertical currentState
    {
        get { return Vertical.Inertia; }
    }

    /// <summary>
    /// Al crear l'estat li assignem el seu control
    /// </summary>
    /// <param name="characterControl">El control del player propietari de la FSM</param>
    public InertiaVerticalState(CharacterControl characterControl)
    {
        this.control = characterControl;
    }

    /// <summary>
    /// No fem res
    /// </summary>
    public override void StartState()
    {
        // Do nothing
    }

    /// <summary>
    /// Apliquem gravetat
    /// </summary>
    public override void UpdateState()
    {
        this.control.jumpProperties.verticalSpeed -= this.control.jumpProperties.currentGravity * Time.deltaTime;
    }

    /// <summary>
    /// Mirem si hem d'acabar l'estat i anar a WallGrab o a Fall
    /// </summary>
    /// <returns></returns>
    public override bool ShouldChangeState()
    {
        // Si xoquem eliminem la velocitat de pujada (perque pot anar molt rapid) i fem Wall Grab
        if(this.control.controller.blockedFront)
        {
            this.control.jumpProperties.verticalSpeed = 0;
            this.nextState = Vertical.WallGrab;
            return true;
        }

        // Si hem acabat de pujar, caiem
        if(this.control.jumpProperties.verticalSpeed <= 0)
        {
            // TODO Si canviem de JumpFall a Fall podem fer el truc de saltar just després de fer un tp
            this.nextState = Vertical.JumpFall;
            return true;
        }

        return false;
    }

    /// <summary>
    /// No fem res
    /// </summary>
    public override void EndState()
    {
        // Do Nothing
    }
}
