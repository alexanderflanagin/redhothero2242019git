using UnityEngine;
using System.Collections;

/// <summary>
/// Estat vertical Jump
/// </summary>
public class JumpState : PlayerStateVertical
{
    /// <summary>
    /// Si no s'ha deixat de premer el boto de jump
    /// </summary>
    private bool stillJump;

    /// <summary>
    /// Per saber si hem passat el primer frame
    /// </summary>
    private bool firstFramePassed;

    /// <summary>
    /// Retorna l'estat de Jump
    /// </summary>
    public override PlayerState.Vertical currentState
    {
        get { return PlayerState.Vertical.Jump; }
    }

    /// <summary>
    /// Al crear l'estat li assignem el seu control
    /// </summary>
    /// <param name="characterControl">El control del player propietari de la FSM</param>
    public JumpState(CharacterControl characterControl)
    {
        this.control = characterControl;
    }

    /// <summary>
    /// Inicialitzem els flags i posem la velocitat de sortida vertical
    /// </summary>
    public override void StartState()
    {
        // Event de que hem entrat
        this.control.events.triggerEvent(CharacterEvents.EventName.JumpEnter);
        
        // Inicialitzem els flags
        this.stillJump = true;
        this.firstFramePassed = false;

        // Calculem la velocitat de sortida
        this.control.jumpProperties.verticalSpeed = this.control.CalculateJumpVerticalSpeed(false);
    }

    /// <summary>
    /// Cada frame mirem si segueix prement el bot� de jump i calculem la velocitat en relacio
    /// </summary>
    public override void UpdateState()
    {
        // Deixem passar el primer frame
        if(!this.firstFramePassed)
        {
            this.firstFramePassed = true;
            return;
        }

        // Ha mantingut premut el boto de jump tota l'estona?
        if(this.stillJump)
        {
            this.control.jumpProperties.verticalSpeed -= this.control.jumpProperties.currentGravity * Time.deltaTime;
            this.stillJump = this.control.input.jump.check;
        }
        else
        {
            // Acabem el salt cinc vegades mes rapid
            this.control.jumpProperties.verticalSpeed -= this.control.jumpProperties.currentGravity * Time.deltaTime * 5;
        }
    }

    /// <summary>
    /// Mirem si hem d'anar a JumpFall o a WallJump
    /// </summary>
    /// <returns>Cert si hem de canviar d'estat</returns>
    public override bool ShouldChangeState()
    {
        if(this.control.jumpProperties.verticalSpeed <= 0)
        {
            this.nextState = Vertical.JumpFall;
            return true;
        }

        if (this.control.canWallJump() && this.control.input.jump.down)
        {
            this.nextState = Vertical.WallJump;
            return true;
        }

        return false;
    }

    /// <summary>
    /// Cridem l'Event de que hem acabat
    /// </summary>
    public override void EndState()
    {
        this.control.events.triggerEvent(CharacterEvents.EventName.JumpExit);
    }
}
