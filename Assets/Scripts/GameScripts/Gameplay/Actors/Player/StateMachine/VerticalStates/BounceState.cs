using UnityEngine;
using System.Collections;

/// <summary>
/// Estat al que anem quan el player rebota sobre un objecte bounceable
/// </summary>
public class BounceState : PlayerStateVertical
{
    /// <summary>
    /// Retorna l'estat de Bounce
    /// </summary>
    public override Vertical currentState
    {
        get
        {
            return Vertical.Bounce;
        }
    }

    /// <summary>
    /// Al crear l'estat li assignem el seu control
    /// </summary>
    /// <param name="characterControl">El control del player propietari de la FSM</param>
    public BounceState(CharacterControl characterControl)
    {
        this.control = characterControl;
    }

    /// <summary>
    /// Al entrar, mirem si hem premut b� el bot� de salt, i si es el primer bounce seguit que fem o no
    /// </summary>
    public override void StartState()
    {
        DebugUtils.Assert(control.jumpProperties.bounceCount == 0, "Hem entrat al bounce i no es el primer que fem", this.control.gameObject);
        
        // Mirem si entrem saltant, i si es o no el primer cop, per saber com d'amunt anem
        if(this.control.input.jump.check)
        {
            switch(this.control.jumpProperties.bounceCount)
            {
                case 0:
                    this.control.jumpProperties.bounceHeight = JumpProperties.FIRST_BOUNCE_JUMP_HEIGHT;
                    break;
                case 1:
                    this.control.jumpProperties.bounceHeight = JumpProperties.SECOND_BOUNCE_JUMP_HEIGHT;
                    break;
                default:
                    this.control.jumpProperties.bounceHeight = JumpProperties.THIRD_BOUNCE_JUMP_HEIGHT;
                    break;
            }

            // Incrementem el numero de salts fets
            this.control.jumpProperties.bounceCount++;
        }
        else
        {
            this.control.jumpProperties.bounceHeight = JumpProperties.LOW_BOUNCE_HEIGHT;
            // Torna a 0 perque no els hem empalmat be.
            this.control.jumpProperties.bounceCount = 0;
        }

        // Calculem la velocitat de sortida del salt
        this.control.jumpProperties.verticalSpeed = this.control.CalculateJumpVerticalSpeed(true);

        // Event de que hem entrat
        this.control.events.triggerEvent(CharacterEvents.EventName.BounceEnter);
    }

    /// <summary>
    /// Apliquem gravetat
    /// </summary>
    public override void UpdateState()
    {
        this.control.jumpProperties.verticalSpeed -= this.control.jumpProperties.currentGravity * Time.deltaTime;
    }

    /// <summary>
    /// Mirem si hem de canviar d'estat i anar a WallGrab o a JumpFall
    /// </summary>
    /// <returns>Cert si hem de canviar d'estat</returns>
    public override bool ShouldChangeState()
    {
        if (this.control.fullWallGrab(this.control.movementProps.lookingDirection, MovementProperties.WALL_GRAB_DISTANCE_CHECKER))
        {
            this.nextState = Vertical.WallGrab;
            return true;
        }

        if (this.control.jumpProperties.verticalSpeed < 0)
        {
            this.nextState = Vertical.JumpFall;
            return true;
        }

        return false;
    }

    /// <summary>
    /// Do nothing
    /// </summary>
    public override void EndState()
    {
        // Do nothing
    }
}
