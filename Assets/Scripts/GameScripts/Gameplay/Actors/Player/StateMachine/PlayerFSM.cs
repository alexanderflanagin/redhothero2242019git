using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// Per a gestionar la maquina d'estats del Player
/// </summary>
public class PlayerFSM
{
    /// <summary>
    /// Diccionari d'estats verticals, indexats per \PlayerState.Vertical\
    /// </summary>
    public Dictionary<PlayerState.Vertical, PlayerStateVertical> VerticalStates;

    /// <summary>
    /// Diccionari d'estats horitzontals, indexats per \PlayerState.Horizontal\
    /// </summary>
    public Dictionary<PlayerState.Horizontal, PlayerStateHorizontal> HorizontalStates;

    /// <summary>
    /// Estat vertical actual
    /// </summary>
    public PlayerState.Vertical currentVertical;

    /// <summary>
    /// Estat horitzotnal actual
    /// </summary>
    public PlayerState.Horizontal currentHorizontal;

    /// <summary>
    /// Control del player que controla la m�quina d'estats
    /// </summary>
    private CharacterControl control;

    /// <summary>
    /// Inicialitza els dicionaris d'estats
    /// </summary>
    /// <param name="cc">el /CharacterControl/ del player</param>
    public void Init(CharacterControl cc)
    {
        this.control = cc;

        this.VerticalStates = new Dictionary<PlayerState.Vertical, PlayerStateVertical>();

        this.HorizontalStates = new Dictionary<PlayerState.Horizontal, PlayerStateHorizontal>();

        // Afegim tota la llista de verticals
        this.VerticalStates.Add(PlayerState.Vertical.Grounded, new GroundedState(this.control));
        this.VerticalStates.Add(PlayerState.Vertical.Jump, new JumpState(this.control));
        this.VerticalStates.Add(PlayerState.Vertical.JumpFall, new JumpFallState(this.control));
        this.VerticalStates.Add(PlayerState.Vertical.Fall, new FallState(this.control));
        this.VerticalStates.Add(PlayerState.Vertical.WallGrab, new WallGrabState(this.control));
        this.VerticalStates.Add(PlayerState.Vertical.WallJump, new WallJumpVerticalState(this.control));
        this.VerticalStates.Add(PlayerState.Vertical.Bounce, new BounceState(this.control));
        this.VerticalStates.Add(PlayerState.Vertical.Travel, new TravelVerticalState(this.control));
        this.VerticalStates.Add(PlayerState.Vertical.Inertia, new InertiaVerticalState(this.control));
        this.VerticalStates.Add(PlayerState.Vertical.WallGrabSeparated, new WallGrabSeparatedState(this.control));

        // Afegim tota la llista d'horitzontals
        this.HorizontalStates.Add(PlayerState.Horizontal.Idle, new IdleState(this.control));
        this.HorizontalStates.Add(PlayerState.Horizontal.Inertia, new InertiaState(this.control));
        this.HorizontalStates.Add(PlayerState.Horizontal.Run, new RunState(this.control));
        this.HorizontalStates.Add(PlayerState.Horizontal.Crouch, new CrouchState(this.control));
        this.HorizontalStates.Add(PlayerState.Horizontal.Roll, new RollState(this.control));
        this.HorizontalStates.Add(PlayerState.Horizontal.Travel, new TravelState(this.control));
        this.HorizontalStates.Add(PlayerState.Horizontal.WallJump, new WallJumpState(this.control));
        this.HorizontalStates.Add(PlayerState.Horizontal.Brake, new BrakeState(this.control));
    }

    /// <summary>
    /// Actualitza els estats de la FSM amb les comprovacions corresponents de que tot existeixi
    /// </summary>
    public void Update()
    {
        if(this.VerticalStates.ContainsKey(this.currentVertical))
        {
            this.VerticalStates[this.currentVertical].UpdateState();

            if(this.VerticalStates[this.currentVertical].ShouldChangeState())
            {
                ChangeCurrentVerticalState(this.VerticalStates[this.currentVertical].nextState);
            }
        }
        else
        {
            Debug.LogError("Current state not in FSM dictionary: " + this.currentVertical);
        }
        
        if(this.HorizontalStates.ContainsKey(this.currentHorizontal))
        {
            this.HorizontalStates[this.currentHorizontal].UpdateState();

            if (this.HorizontalStates[this.currentHorizontal].ShouldChangeState())
            {
                ChangeCurrentHorizontalState(this.HorizontalStates[this.currentHorizontal].nextState);
            }
        }
        else
        {
            Debug.LogError("Current state not in FSM dictionary: " + this.currentHorizontal);
        } 
    }

    /// <summary>
    /// Forcem un canvi de l'estat horitzontal des de fora de la maquina d'estats
    /// </summary>
    /// <param name="newState">El nou estat al que anirem</param>
    public void ChangeCurrentHorizontalState(PlayerState.Horizontal newState)
    {
        this.HorizontalStates[this.currentHorizontal].EndState();
        this.currentHorizontal = newState;
        this.HorizontalStates[this.currentHorizontal].StartState();
    }
    /// <summary>
    /// Forcem un canvi de l'estat vertical des de fora de la maquina d'estats
    /// </summary>
    /// <param name="newState">El nou estat al que anirem</param>
    public void ChangeCurrentVerticalState(PlayerState.Vertical newState)
    {
        this.VerticalStates[this.currentVertical].EndState();
        this.currentVertical = newState;
        this.VerticalStates[this.currentVertical].StartState();
    }
}
