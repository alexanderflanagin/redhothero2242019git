
/// <summary>
/// Classe abstracta per a declarar els estats del Player
/// </summary>
public abstract class PlayerState
{
    /// <summary>
    /// Horizontal State for the FSM
    /// </summary>
    public enum Horizontal { Idle, Inertia, Run, Crouch, Roll, Slide, Travel, WallJump, Brake };

    /// <summary>
    /// Vertical State for the FSM
    /// </summary>
    public enum Vertical { Grounded, Jump, JumpFall, Fall, WallGrab, WallJump, Bounce, Travel, Inertia, WallGrabSeparated };

    /// <summary>
    /// El /CharacterControl/ del player que té la màquina d'estats
    /// </summary>
    protected CharacterControl control;

    /// <summary>
    /// Es crida al iniciar un state, despr�s del Construct
    /// </summary>
    public abstract void StartState();

    /// <summary>
    /// Actualitza l'estat, 1 cop per frame
    /// </summary>
    public abstract void UpdateState();

    /// <summary>
    /// Mira si s'ha de canviar l'estat
    /// </summary>
    /// <returns>Cert si s'ha de canviar, fals altrament</returns>
    public abstract bool ShouldChangeState();

    /// <summary>
    /// Abans de canviar l'estat, fa els canvis necessaris
    /// </summary>
    public abstract void EndState();
}
