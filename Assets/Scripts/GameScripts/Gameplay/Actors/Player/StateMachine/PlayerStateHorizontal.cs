
/// <summary>
/// Classe abstracta per a declarar els estats del Player horitzontals
/// </summary>
public abstract class PlayerStateHorizontal : PlayerState
{
    /// <summary>
    /// Retorna el l'estat actual. Forcem a que es posi en cada classe heredada
    /// </summary>
    public abstract PlayerState.Horizontal currentState { get; }
    
    /// <summary>
    /// Guarda el valor del proxim estat un cop es detecti la necessitat de canviar d'estat
    /// </summary>
    public PlayerState.Horizontal nextState;
}
