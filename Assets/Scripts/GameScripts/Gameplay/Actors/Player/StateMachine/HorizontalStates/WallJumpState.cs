using UnityEngine;
using System.Collections;

/// <summary>
/// Estat horitzontal per al WallJump
/// </summary>
public class WallJumpState : PlayerStateHorizontal
{
    /// <summary>
    /// Per saber quan hem entrat al WallJump
    /// </summary>
    private float startTime;

    /// <summary>
    /// Retorna l'estat de Wall Jump
    /// </summary>
    public override PlayerState.Horizontal currentState
    {
        get { return Horizontal.WallJump; }
    }

    /// <summary>
    /// Al crear l'estat li assignem el seu control
    /// </summary>
    /// <param name="cc">El control del player propietari de la FSM</param>
    public WallJumpState(CharacterControl cc)
    {
        this.control = cc;
    }

    /// <summary>
    /// Mirem a quin instant comencem i posem la velocitat de WallJump
    /// </summary>
    public override void StartState()
    {
        this.startTime = Time.time;

        this.control.movementProps.speed = MovementProperties.WALL_JUMP_SPEED;
    }

    /// <summary>
    /// No fem res
    /// </summary>
    public override void UpdateState()
    {
        // Do Nothing
    }

    public override bool ShouldChangeState()
    {
        // Si som al mateix frame al que hem entrat
        if (this.startTime != Time.time)
        {
            // Si ha passat tot el temps que havia de passar dins de l'estat
            if (Time.time > this.startTime + MovementProperties.WALL_JUMP_AXIS_FREEZE_TIME)
            {
                this.nextState = Horizontal.Inertia;
                return true;
            }

            // Si estem bloquejats frontalment
            if (this.control.controller.blockedFront)
            {
                this.nextState = Horizontal.Idle;
                return true;
            }
        }

        return false;
    }

    /// <summary>
    /// No fem res
    /// </summary>
    public override void EndState()
    {
        // Do Nothing
    }
}
