using UnityEngine;
using System.Collections;

/// <summary>
/// Estat horitzontal per al Crouch
/// </summary>
public class CrouchState : PlayerStateHorizontal
{
    /// <summary>
    /// Per saber si ens podem aixecar
    /// </summary>
    private bool hitting;

    /// <summary>
    /// Marge superior quan estem mirant si ens podem aixecar
    /// </summary>
    private float marginTop = 0.3f;

    /// <summary>
    /// Posicio d'inici de la nostre deteccio de col.lisio
    /// </summary>
    private Vector2 pointA;

    /// <summary>
    /// Posicio final de la nostre deteccio de col.lisio
    /// </summary>
    private Vector2 pointB;

    /// <summary>
    /// Vector2 de la posicio del player
    /// </summary>
    private Vector2 position;

    /// <summary>
    /// Array per a guardar els resultats de la cerca de col·lisions
    /// </summary>
    private Collider2D[] results;

    /// <summary>
    /// Retorne l'estat de Crouch
    /// </summary>
    public override PlayerState.Horizontal currentState
    {
        get
        {
            return Horizontal.Crouch;
        }
    }

    /// <summary>
    /// Al crear l'estat li assignem el seu control
    /// </summary>
    /// <param name="cc">El control del player propietari de la FSM</param>
    public CrouchState (CharacterControl cc)
    {
        this.control = cc;
    }

    /// <summary>
    /// Iniciem paràmetres de l'estat, fem petit el controller i assegurem la velocitat a 0
    /// </summary>
    public override void StartState()
    {
        // Cridem a l'event de que hem entrat
        this.control.events.triggerEvent(CharacterEvents.EventName.CrouchEnter);
        
        // Fem que el player no pugui saltar i fem petit el controller
        this.control.jumpProperties.canJump = false;
        this.control.DecreaseController();
        
        // Iniciem valors de l'estat
        this.hitting = false;
        this.pointA = new Vector2(this.control.collider2D.MinX() + 0.05f, this.control.collider2D.MaxY());
        this.pointB = new Vector2(this.control.collider2D.MaxX() - 0.05f, this.control.collider2D.MaxY() + marginTop);
        this.position = new Vector2(this.control.transform.position.x, this.control.transform.position.y);
        this.results = new Collider2D[1];

        // Mirem si ja estem tocant alguna cosa per la part superior (sempre hauria de ser que no!)
        this.hitting = Physics2D.OverlapAreaNonAlloc(position + pointA, position + pointB, results, this.control.controller.collisionMask) != 0;

        // Elminem velocitat si en portavem
        this.control.movementProps.speed = 0;
    }

    /// <summary>
    /// Al actualitzar l'estat, mirem que no estiguem col·lisionant amb res per la part de dalt, per si ens volguessim aixecar
    /// </summary>
    public override void UpdateState()
    {
        // position = new Vector2(this.control.transform.position.x, this.control.transform.position.y);
        position = (Vector2)this.control.transform.position;

        Debug.DrawLine(position + pointA, position + pointB);

        hitting = Physics2D.OverlapAreaNonAlloc(position + pointA, position + pointB, results, this.control.controller.collisionMask) != 0 && !Physics2D.GetIgnoreCollision(this.control.collider2D, results[0]);

        //if (results[0] != null)
        //{
        //    Debug.Log("Crouch collision: " + results[0].name);
        //}
    }

    /// <summary>
    /// Mirem si hem d'anar a Idle
    /// </summary>
    /// <returns>Cert si hem de canviar d'estat</returns>
    public override bool ShouldChangeState()
    {
        if (!this.control.input.roll.check && !hitting)
        {
            this.nextState = Horizontal.Idle;
            return true;
        }
        
        return false;
    }

    /// <summary>
    /// Tornem a fer gran el collider
    /// </summary>
    public override void EndState()
    {
        this.control.jumpProperties.canJump = true;
        this.control.IncreaseController();
    }
}
