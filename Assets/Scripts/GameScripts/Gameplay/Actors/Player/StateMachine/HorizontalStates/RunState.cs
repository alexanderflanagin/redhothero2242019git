using UnityEngine;
using System.Collections;

/// <summary>
/// Estat horitzontal per al Run
/// </summary>
public class RunState : PlayerStateHorizontal
{
    /// <summary>
    /// La velocitat maxima que podem portar segons si fem sprint, super-sprint o run
    /// </summary>
    float currentMaxSpeed;

    /// <summary>
    /// L'acceleracio maxima que portem segons si fem sprint, super-sprint o run
    /// </summary>
    float currentAcceleration;

    /// <summary>
    /// Quan ens hem posat a correr. Per a avaluar les corbes
    /// </summary>
    float startRun;

    /// <summary>
    /// -1 si no ha començat l'sprint, el valor del temps si ha començat
    /// </summary>
    float startSprint;

    /// <summary>
    /// Retorne l'estat de Run
    /// </summary>
    public override PlayerState.Horizontal currentState
    {
        get
        {
            return Horizontal.Run;
        }
    }

    /// <summary>
    /// Al crear l'estat li assignem el seu control
    /// </summary>
    /// <param name="cc">El control del player propietari de la FSM</param>
    public RunState(CharacterControl cc)
    {
        this.control = cc;
    }

    /// <summary>
    /// Iniciem parametres de l'estat i ens assegurem de que estiguem mirant cap a la direcció correcta
    /// </summary>
    public override void StartState()
    {
        // Cridem a l'event de que hem entrat
        this.control.events.triggerEvent(CharacterEvents.EventName.RunEnter);
        
        // La velocitat maxima i l'acceleracio que podem portar (segons si fem sprint o no)
        this.currentMaxSpeed = 0;
        this.currentAcceleration = 0;

        // Quan ens hem posat a correr. Per a avaluar les corbes
        this.startRun = Time.time;

        this.startSprint = -1;

        if (this.control.input.xAxis.absoluteVal != 0 && this.control.input.xAxis.absoluteVal != this.control.movementProps.runDirection)
        {
            // Canviem de direccio
            if (this.control.movementProps.speed > 0)
            {
                // no hauriem d'estar aqui
                Debug.LogError("Hem entrat al Run amb velocitat. No hauriem d'estar aquí i anem a Inertia.");
            }

            this.control.movementProps.runDirection = (int)this.control.input.xAxis.absoluteVal;
        }

        if (this.control.movementProps.runDirection != this.control.movementProps.lookingDirection)
        {
            this.control.RotateMainChar();
        }
    }

    /// <summary>
    /// Mirem si estem fent run, speed o super_speed, calculem l'acceleracio actual i assgignem la velocitat d'aquest frame
    /// </summary>
    public override void UpdateState()
    {
        // Mirem si hem de començar a enregistra rel temps per el super sprint
        if (Mathf.Approximately(this.control.movementProps.speed, MovementProperties.SPRINT_SPEED) && this.startSprint == -1)
        {
            startSprint = Time.time;
        }
        else if (this.control.movementProps.speed < MovementProperties.SPRINT_SPEED)
        {
            startSprint = -1;
        }

        // Calculem velocitat actual
        currentMaxSpeed = MovementProperties.SPRINT_SPEED;

        // Agafem la curva d'acceleracio que farem servir segons si toquem el terra o no
        AnimationCurve curve = this.control.controller.collisions.below ? this.control.movementProps.runAcceleration : this.control.movementProps.onAiracceleration;

        currentAcceleration = curve.Evaluate(Time.time - startRun) * this.currentMaxSpeed;

        // La velocitat que duem és la màxima entre l'acceleracio que toca i la velocitat que ja duiem, i la mínima entre aquesta i la velocitat màxima
        this.control.movementProps.speed = Mathf.Min(currentMaxSpeed, Mathf.Max(this.control.movementProps.speed, this.currentAcceleration));
    }


    /// <summary>
    /// Mirem si hem d'anar a Inertia, Idle, Roll o Crouch
    /// </summary>
    /// <returns>Cert si hem de canviar d'estat</returns>
    public override bool ShouldChangeState()
    {
        if(control.input.brake.check)
        {
            this.nextState = Horizontal.Brake;
            return true;
        }

        // Si no toquem el joystick, canviem de direcció o anem més amunt que a la velocitat màxima
        if ((!this.control.input.xAxis.check) && this.control.movementProps.speed > MovementProperties.RUN_SPEED || this.control.input.xAxis.absoluteVal != this.control.movementProps.runDirection)
        {
            this.nextState = Horizontal.Inertia;
            return true;
        }

        // Si ens topem amb alguna paret davant
        if ((this.control.controller.blockedFront && this.control.LookingDirection == this.control.controller.collisions.faceDir))
        {
            this.nextState = Horizontal.Idle;
            return true;
        }

        // Roll
        if (this.control.testRoll())
        {
            this.nextState = Horizontal.Roll;
            return true;
        }

        // Crouch
        if (this.control.testCrouch())
        {
            this.nextState = Horizontal.Crouch;
            return true;
        }

        return false;
    }

    /// <summary>
    /// Cridem l'event de que s'ha acabat el Run
    /// </summary>
    public override void EndState()
    {
        this.control.events.triggerEvent(CharacterEvents.EventName.RunExit);
    }
}
