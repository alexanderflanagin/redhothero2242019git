using UnityEngine;
using System.Collections;

/// <summary>
/// Estat horitzontal per la Inertia
/// </summary>
public class InertiaState : PlayerStateHorizontal
{
    /// <summary>
    /// Segon d'entrada, per a analitzar la corva de la inercia
    /// </summary>
    private float startTime;

    /// <summary>
    /// Velocitat d'entrada, per a analitzar la corva de la inercia
    /// </summary>
    private float startSpeed;

    /// <summary>
    /// Factor multiplicador de la inercia, per si girem el joystick i l'accelerem
    /// </summary>
    private float multFactor;

    /// <summary>
    /// Per no consultar si estem corrent al primer frame, ho fem a partir del segon
    /// </summary>
    private bool firstFrame;

    /// <summary>
    /// Retorne l'estat d'Inertia
    /// </summary>
    public override PlayerState.Horizontal currentState
    {
        get
        {
            return Horizontal.Inertia;
        }
    }

    /// <summary>
    /// Al crear l'estat li assignem el seu control
    /// </summary>
    /// <param name="cc">El control del player propietari de la FSM</param>
    public InertiaState(CharacterControl cc)
    {
        this.control = cc;
    }

    /// <summary>
    /// Al iniciar l'estat, agafem velocitat i temps d'entrada i inicialitzem flags interns
    /// </summary>
    public override void StartState()
    {
        // Cridem a l'event de que hem entrat
        this.control.events.triggerEvent(CharacterEvents.EventName.InertiaEnter);
        
        // Moment en que l'inercia comenca i velocitat que porta
        this.startTime = Time.time;
        this.startSpeed = this.control.movementProps.speed;

        // Per saber si hem d'aplicar mes inercia per un canvi d'axis al joystick
        this.multFactor = 1;

        // Si es el primer frame en que est� en inercia
        this.firstFrame = true;
    }

    /// <summary>
    /// Apliquem inercia en cada frame
    /// </summary>
    public override void UpdateState()
    {
        // TODO posar el metode ApplyInertia en aquesta classe
        this.control.ApplyInertia(this.startTime, this.startSpeed, ref this.multFactor);
    }

    /// <summary>
    /// Mirem si hem de canviar a Run, Roll, Crouch o Idle
    /// </summary>
    /// <returns>Cert si hem de canviar d'estat</returns>
    public override bool ShouldChangeState()
    {
        // Si ja ens hem parat
        if (Mathf.Approximately(this.control.movementProps.speed, 0)) // IMPORTANT he canviat el "== 0" per el Approximately
        {
            this.nextState = Horizontal.Idle;
            return true;
        }

        if (control.input.brake.check)
        {
            this.nextState = Horizontal.Brake;
            return true;
        }

        // Si no es el primer frame (per evitar bugs), movem el joystick en la direccio que anavem
        if (!this.firstFrame && this.control.input.xAxis.check && this.control.input.xAxis.absoluteVal == this.control.movementProps.runDirection)
        {
            this.nextState = Horizontal.Run;
            return true;
        }
        
        // Si estem fent un roll
        if(this.control.testRoll())
        {
            this.nextState = Horizontal.Roll;
            return true;
        }

        // Si estem fent un crouch
        if(this.control.testCrouch())
        {
            this.nextState = Horizontal.Crouch;
            return true;
        }

        this.firstFrame = false;

        return false;
    }

    /// <summary>
    /// Cridem l'event de que s'ha acabat l'Inertia
    /// </summary>
    public override void EndState()
    {
        this.control.events.triggerEvent(CharacterEvents.EventName.InertiaExit);
    }
}
