using UnityEngine;

/// <summary>
/// Estat horitzontal per al Roll
/// </summary>
public class RollState : PlayerStateHorizontal
{
    /// <summary>
    /// Per saber si ens podem aixecar
    /// </summary>
    private bool hitting;

    /// <summary>
    /// Marge superior quan estem mirant si ens podem aixecar
    /// </summary>
    private float marginTop;

    /// <summary>
    /// Posicio d'inici de la nostre deteccio de col.lisio
    /// </summary>
    private Vector2 pointA;

    /// <summary>
    /// Posicio final de la nostre deteccio de col.lisio
    /// </summary>
    private Vector2 pointB;

    /// <summary>
    /// Vector2 de la posicio del player
    /// </summary>
    private Vector2 position;

    /// <summary>
    /// Array per a guardar els resultats de la cerca de col·lisions
    /// </summary>
    private Collider2D[] results;

    /// <summary>
    /// Per saber en quin moment hem entrat al Roll
    /// </summary>
    private float time;

    /// <summary>
    /// Per saber si hem de sortir per temps
    /// </summary>
    private bool exitByTime;

    /// <summary>
    /// Per saber si estem intentant saltar
    /// </summary>
    private bool jump;

    /// <summary>
    /// Retorne l'estat de Roll
    /// </summary>
    public override PlayerState.Horizontal currentState
    {
        get { return Horizontal.Roll; }
    }

    /// <summary>
    /// Al crear l'estat li assignem el seu control
    /// </summary>
    /// <param name="cc">El control del player propietari de la FSM</param>
    public RollState(CharacterControl cc)
    {
        this.control = cc;
    }

    /// <summary>
    /// Al iniciar l'estat, posem el flag de que ja no podra fer roll fins que es resetegi, iniciem el test de si es podra aixecar,
    /// guardem les dades d'entrada i ens assegurem que la velocitat mínima sigui la de Run
    /// </summary>
    public override void StartState()
    {
        // Cridem a l'event de que hem entrat
        this.control.events.triggerEvent(CharacterEvents.EventName.RollEnter);
        
        this.control.DecreaseController();
        
        // Ho fem servir per deixar un temps entre roll i roll, i esperar que es deixi de premer el boto
        this.control.movementProps.canRoll = false;

        this.marginTop = this.control.collider2D.size.y;

        // this.pointA = new Vector2(this.control.collider2D.offset.x - this.control.collider2D.size.x / 2 + 0.025f, this.control.collider2D.offset.y + this.control.collider2D.size.y / 2);
        // this.pointB = new Vector2(this.control.collider2D.offset.x + this.control.collider2D.size.x / 2 - 0.025f, this.control.collider2D.offset.y + this.control.collider2D.size.y / 2 + marginTop);
        this.pointA = new Vector2(this.control.collider2D.MinX() + 0.05f, this.control.collider2D.MaxY());
        this.pointB = new Vector2(this.control.collider2D.MaxX() - 0.05f, this.control.collider2D.MaxY() + marginTop);
        this.position = new Vector2(this.control.transform.position.x, this.control.transform.position.y);
        this.results = new Collider2D[1];

        // Mirem si ja estem tocant alguna cosa per la part superior (sempre hauria de ser que no!)
        hitting = Physics2D.OverlapAreaNonAlloc(position + pointA, position + pointB, results, this.control.controller.collisionMask) != 0;

        // Inicialitzem els parametres per a acabar l'estat
        this.time = Time.time;
        this.exitByTime = false;
        this.jump = false;

        // Si no anem minim a la velocitat de run, ens posem a la velocitat de run perque no accelerarem
        if (this.control.movementProps.speed < MovementProperties.RUN_SPEED)
        {
            this.control.movementProps.speed = MovementProperties.RUN_SPEED;
        }
    }

    /// <summary>
    /// Mirem si ha de sortir per temps, per salt, i si pot aixecar-se o esta tocant alguna cosa
    /// </summary>
    public override void UpdateState()
    {
        this.exitByTime = this.exitByTime || (this.time + MovementProperties.ROLL_MAX_TIME <= Time.time);
        this.position = (Vector2)this.control.transform.position;
        this.hitting = Physics2D.OverlapAreaNonAlloc(position + pointA, position + pointB, results, this.control.defaultLayerMask) != 0 && !Physics2D.GetIgnoreCollision(this.control.collider2D, results[0]);
        
        // Debug.DrawLine(position + pointA, position + pointB, Color.green);

        this.control.jumpProperties.canJump = !hitting;
        this.jump = this.control.input.jump.down && this.control.jumpProperties.canJump;
    }

    /// <summary>
    /// Mirem si hem d'anar a Inertia o a Run
    /// </summary>
    /// <returns>Cert si hem de canviar d'estat</returns>
    public override bool ShouldChangeState()
    {
        if ((exitByTime || jump) && !hitting)
        {
            // Si l'axis es cap a la mateixa direccio fem run, sino inertia
            if ((this.control.input.xAxis.absoluteVal == this.control.movementProps.runDirection))
            {
                this.nextState = Horizontal.Run;
            }
            else
            {
                this.nextState = Horizontal.Inertia;
            }

            return true;
        }
        
        return false;
    }

    /// <summary>
    /// Iniciem la coroutina de resetejar el roll, ens assegurem de que tenim el flag de que pot saltar a cert i tornem a fer crèixer el controller
    /// </summary>
    public override void EndState()
    {
        this.control.StartCoroutine(this.control.ResetRoll());
        this.control.jumpProperties.canJump = true;

        this.control.events.triggerEvent(CharacterEvents.EventName.RollExit);

        this.control.IncreaseController();
    }
}
