using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrakeState : PlayerStateHorizontal
{
    /// <summary>
    /// Retorne l'estat de Brake
    /// </summary>
    public override PlayerState.Horizontal currentState
    {
        get
        {
            return PlayerState.Horizontal.Brake;
        }
    }

    public BrakeState(CharacterControl cc)
    {
        control = cc;
    }

    /// <summary>
    /// Al iniciar ens assegurem que la velocitat es 0
    /// </summary>
    public override void StartState()
    {
        // Entrem a l'idle
        this.control.events.triggerEvent(CharacterEvents.EventName.BrakeEnter);

        this.control.movementProps.speed = 0;
    }

    public override void UpdateState()
    {
        // No fem res
    }

    public override bool ShouldChangeState()
    {
        if (control.input.brake.check)
        {
            return false;
        }
        else
        {
            nextState = Horizontal.Idle;
            return true;
        }
    }

    public override void EndState()
    {
        // No fem res
        control.events.triggerEvent(CharacterEvents.EventName.BrakeExit);
    }
}
