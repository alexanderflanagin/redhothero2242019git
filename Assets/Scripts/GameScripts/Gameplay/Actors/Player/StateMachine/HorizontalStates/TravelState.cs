using UnityEngine;
using System.Collections;

/// <summary>
/// Estat horitzontal per al Travel
/// </summary>
public class TravelState : PlayerStateHorizontal
{

    /// <summary>
    /// Retorne l'estat de Travel
    /// </summary>
    public override PlayerState.Horizontal currentState
    {
        get { return Horizontal.Travel; }
    }

    /// <summary>
    /// Al crear l'estat li assignem el seu control
    /// </summary>
    /// <param name="cc">El control del player propietari de la FSM</param>
    public TravelState(CharacterControl cc)
    {
        this.control = cc;
    }

    /// <summary>
    /// Al comencar l'estat no fem res
    /// </summary>
    public override void StartState()
    {
        // Do Nothing
    }

    /// <summary>
    /// Mentre som dins l'estat, mirem si haurem de tenir el player rotat al sortir
    /// </summary>
    public override void UpdateState()
    {
        if (this.control.input.xAxis.absoluteVal != 0 && this.control.input.xAxis.absoluteVal != this.control.movementProps.runDirection)
        {
            // Canviem de direccio
            this.control.movementProps.runDirection = (int)this.control.input.xAxis.absoluteVal;
            if (this.control.movementProps.lookingDirection != this.control.movementProps.runDirection)
            {
                this.control.RotateMainChar();
            }
        }
    }

    /// <summary>
    /// Mira si hem de sortir de l'estat (es canvia des de fora) i hem d'anar a Run o Inertia
    /// </summary>
    /// <returns>Cert si hem de sortir de l'estat</returns>
    public override bool ShouldChangeState()
    {
        if (Horizontal.Travel != this.control.movementFSM.currentHorizontal)
        {
            if(this.control.input.xAxis.check)
            {
                this.nextState = Horizontal.Run;
            }
            else
            {
                this.nextState = Horizontal.Inertia;
            }
            return true;
        }
        
        return false;
    }

    /// <summary>
    /// No fem res
    /// </summary>
    public override void EndState()
    {
        // Do Nothing
    }
}
