using UnityEngine;
using System.Collections;

/// <summary>
/// Estat horitzontal Idle per als characters
/// </summary>
public class IdleState : PlayerStateHorizontal
{
    /// <summary>
    /// Retorne l'estat d'Idle
    /// </summary>
    public override PlayerState.Horizontal currentState
    {
        get
        {
            return PlayerState.Horizontal.Idle;
        }
    }

    /// <summary>
    /// Al crear l'estat li assignem el seu control
    /// </summary>
    /// <param name="cc">El control del player propietari de la FSM</param>
    public IdleState(CharacterControl cc)
    {
        this.control = cc;
    }

    /// <summary>
    /// Al iniciar ens assegurem que la velocitat es 0
    /// </summary>
    public override void StartState()
    {
        // Entrem a l'idle
        this.control.events.triggerEvent(CharacterEvents.EventName.IdleEnter);
        
        this.control.movementProps.speed = 0;
    }

    /// <summary>
    /// No cal que fem res al actualitzar l'estat
    /// </summary>
    public override void UpdateState()
    {
        // No fem res
    }

    /// <summary>
    /// Mirem si hem de canviar l'estat a Roll, Run o Crouch
    /// </summary>
    /// <returns></returns>
    public override bool ShouldChangeState()
    {
        if (control.input.brake.check)
        {
            this.nextState = Horizontal.Brake;
            return true;
        }

        // Ens movem i ens ajupim a la vegada
        if (this.control.testRoll())
        {
            this.nextState = Horizontal.Roll;
            return true;
        }

        // Ens movem i no estem bloquejats
        if(this.control.input.xAxis.check && !(this.control.controller.blockedFront && this.control.input.xAxis.absoluteVal == this.control.movementProps.runDirection))
        {
            this.nextState = Horizontal.Run;
            return true;
        }

        // Ens ajupim
        if(this.control.testCrouch())
        {
            this.nextState = Horizontal.Crouch;
            return true;
        }

        return false;
    }

    // No fem res
    public override void EndState()
    {
        // No fem res
    }
}
