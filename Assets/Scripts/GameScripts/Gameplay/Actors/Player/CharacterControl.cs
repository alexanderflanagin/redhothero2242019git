using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System;

/// <summary>
/// Classe que s'encarrega de moure el Player
/// </summary>
public class CharacterControl : MonoBehaviour, ISlideable //, IKillable
{
    /// <summary>
    /// Classe serialitzable que conté una sèrie de variables que són els valors de moviment del Player
    /// </summary>
	public MovementProperties movementProps = new MovementProperties();

    /// <summary>
    /// Classe serialitzable que conté els valors de salt i moviment vertical del Player
    /// </summary>
	public JumpProperties jumpProperties = new JumpProperties();

    /// <summary>
    /// Classe serialitzable que conté els valors del travel del player
    /// </summary>
    public TravelProperties travelProperties = new TravelProperties();

    /// <summary>
    /// Classe per a guardar les variables relacionades amb la pausa
    /// </summary>
    public PauseProperties pauseProperties = new PauseProperties();

    /// <summary>
    /// Variable estatica per a assegurar-nos que no tenim mes d'un hero
    /// </summary>
    public static CharacterControl Instance;

    public KillNDie killNDie;

    /// <summary>
    /// Per a controlar quan es pot moure i quan no
    /// </summary>
    private bool move;

    /// <summary>
    /// State Machine per al moviment en horitzontal del player
    /// </summary>
    [HideInInspector]
    public PlayerFSM movementFSM;

	/// <summary>
    /// Alive states for the FSM
	/// </summary>
	public enum StateAlive { Start, Playing, Stunned, TripOver, Dead, Win, Invulnerable }

    /// <summary>
    /// Estat actual del enum StateAlive
    /// </summary>
	protected StateAlive _currentAlive;

    /// <summary>
    /// Getter extern per a saber l'estat actual del _currentAlive
    /// </summary>
	public StateAlive currentAlive { get { return _currentAlive; } }

	/// <summary>
    /// Input controller
	/// </summary>
	protected IUserInput _input;

    /// <summary>
    /// Getter públic del input controller
    /// </summary>
	public IUserInput input
    {
        get 
        { 
            return _input; 
        } 
    }
    
    /// <summary>
    /// Getter públic per a la variable _lookingDirection
    /// </summary>
    public int LookingDirection
    {
        get
        {
            return this.movementProps.lookingDirection;
        }
    }

    /// <summary>
    /// El Physics2DController del Player
    /// </summary>
    [SerializeField]
	protected Physics2DController _controller;

    public Physics2DController controller
    {
        get
        {
            return _controller;
        }
    }

	/// <summary>
    /// Si es pot matar (de moment ho farem servir per al final de la pantalla)
	/// </summary>
	private bool _killeable = true;

    /// <summary>
    /// Getter i setter públics de la variable _killeable
    /// </summary>
	public bool killeable 
    { 
        get 
        { 
            return _killeable; 
        } 
        set 
        { 
            _killeable = value; 
        } 
    }

    /// <summary>
    /// El seu collider2D
    /// </summary>
    [HideInInspector]
	public new BoxCollider2D collider2D;

	/// <summary>
    /// Guardem la LayerMask per defecte
	/// </summary>
    [HideInInspector]
	public LayerMask defaultLayerMask;

	public CharacterEvents events { get; set; }

    /// <summary>
    /// Variable creada per al escrow
    /// </summary>
    private bool jumping;

    /// <summary>
    /// GameObject pare on hi han totes les meshes (que s'han d'amagar, rotar, etc.)
    /// </summary>
    [SerializeField]
    private GameObject _mesh;

    /// <summary>
    /// Getter public del game object que té totes les meshes
    /// </summary>
    public GameObject mesh
    {
        get
        {
            return _mesh;
        }
    }

    public PlayerAim playerAim;

    /// <summary>
    /// Es el Magma Suit del player. El que defineix quins poders té actualment
    /// </summary>
    public MagmaSuit suit;

    /// <summary>
    /// Last position of the player
    /// </summary>
    [HideInInspector]
    public Vector3 lastPosition;

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;

            // El Hero ja no s'elimina mai
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
            return;
        }

        // Preparem la maquina d'estats
        movementFSM = new PlayerFSM();
        movementFSM.Init(this);

        // Recuperem el collider
        collider2D = GetComponent<BoxCollider2D>();

        // Default controller
        _input = transform.GetOrAddComponent<CharacterInput>();
        if (_input.xAxis.name == null)
        {
            SetController("");
        }

        defaultLayerMask = _controller.collisionMask;
    }

    /// <summary>
    /// Al fer enable, creem els events del player, li afegim l'script d'enfonsar-se, agafem el controller i registrem l'acció OnBlockedUpEnter
    /// </summary>
    void OnEnable()
    {
        Debug.Log("OnEnable del player");

        _mesh.SetActive(true);

        events = new CharacterEvents();

        // To test if events are working
        // transform.GetOrAddComponent<EventsDummy>();

        _controller.BlockedAboveEnter += OnBlockedUpEnter;
        _controller.BlockedFrontEnter += OnBlockedFrontEnter;
        _controller.BlockedBelowEnter += OnGroundedEnter;

        playerAim = transform.GetOrAddComponent<PlayerAim>();

        jumping = false;
        move = true;
        collider2D.enabled = true;

        _input.Freeze(0);

        if(movementProps.lookingDirection == -1)
        {
            RotateMainChar();
        }
        movementProps.Reset();

        jumpProperties.Reset();

        movementFSM.ChangeCurrentHorizontalState(PlayerState.Horizontal.Idle);
        movementFSM.ChangeCurrentVerticalState(PlayerState.Vertical.Fall);

        _controller.collisionMask = defaultLayerMask;

        _currentAlive = StateAlive.Start;

        HUDManager.PauseEnter += OnPauseEnter;
        HUDManager.PauseExit += OnPauseExit;
    }

    /// <summary>
    /// Al deshabilitar el player, des-registrem l'acció OnBlockedUpEnter
    /// </summary>
    void OnDisable()
    {
        _controller.BlockedAboveEnter -= OnBlockedUpEnter;
        _controller.BlockedFrontEnter -= OnBlockedFrontEnter;
        _controller.BlockedBelowEnter -= OnGroundedEnter;

        HUDManager.PauseEnter -= OnPauseEnter;
        HUDManager.PauseExit -= OnPauseExit;

        if(movementFSM.currentHorizontal == PlayerState.Horizontal.Travel || movementFSM.currentVertical == PlayerState.Vertical.Travel)
        {
            EndTravel(true);
        }
    }

    /// <summary>
    /// Assigna el controller d'aquest Player
    /// </summary>
    /// <param name="suffix">Suffix del controller (si en té) definit en el gameManager</param>
    public void SetController(string suffix)
    {
        if (_input == null)
        {
            _input = transform.GetOrAddComponent<CharacterInput>();
        }

        _input.xAxis.name = InputButton.HORIZONTAL_AXIS + suffix;
        _input.yAxis.name = InputButton.VERTICAL_AXIS + suffix;
        _input.jump.name = InputButton.JUMP + suffix;
        _input.roll.name = InputButton.ROLL + suffix;
        _input.travel.name = InputButton.TRAVEL + suffix;
        _input.shoot.name = InputButton.SHOOT + suffix;
        _input.punch.name = InputButton.PUNCH + suffix;
        _input.brake.name = InputButton.BRAKE + suffix;

        _input.Prepared();
    }

    #region Alive

    public void StartPlaying()
    {
        _input.Defreeze();
        
        _currentAlive = StateAlive.Playing;

        this.movementFSM.currentVertical = PlayerState.Vertical.Fall;
        this.movementFSM.currentHorizontal = PlayerState.Horizontal.Idle;

        jumping = true;
    }

    /// <summary>
    /// Fa tornar a apareixer un player, amb la possibilitat de donar-li un temps de invulnerabilitat
    /// </summary>
    public void RestartPlaying(bool invulnerable)
    {
        collider2D.enabled = true;
        _mesh.SetActive(true);

        StartPlaying();

        if (invulnerable)
        {
            // Li posem el estat invulnerable al currentAlive!
            StartCoroutine(SetInvulnerable());
        }
    }

    /// <summary>
    /// S'ocupa de fer el parpadeig del player
    /// </summary>
    /// <returns></returns>
    private IEnumerator SetInvulnerable()
    {
        _currentAlive = StateAlive.Invulnerable;

        _killeable = false;

        float startTime = Time.time;

        float invulnerableTime = 2f;

        while (startTime + invulnerableTime > Time.time)
        {
            _mesh.SetActive(false);
            yield return new WaitForSeconds(0.1f);
            _mesh.SetActive(true);
            yield return new WaitForSeconds(0.2f);
        }

        _killeable = true;

        _controller.UpdateLayerMask(Layers.addLayer(_controller.collisionMask, Layers.Enemy));

        _currentAlive = StateAlive.Playing;
    }

    public void StopPlaying(int continueRun)
    {
        if (continueRun == 1)
        {
            _currentAlive = StateAlive.Win;
        }
        else
        {
            // @TODO no seran els dos win!!
            _currentAlive = StateAlive.Win;
        }
        _killeable = false;

        collider2D.enabled = false; // Per a que no mori
        _controller.collisionMask = 1 << Layers.Ground; // Per a que no xoqui amb els enemics i el frenin

        _input.Freeze(continueRun);
    }

    public void StartDying(bool byTouch, bool byNoEnergy)
    {
        if (_killeable && _currentAlive != StateAlive.Dead)
        {
            movementProps.speed = 0;
            _input.Freeze(0, true);
            
            events.triggerEvent(CharacterEvents.EventName.DieEnter);

            _controller.UpdateLayerMask(Layers.removeLayer(_controller.collisionMask, Layers.Enemy));

            if (byTouch)
            {
                collider2D.enabled = false;
                events.triggerEvent(CharacterEvents.EventName.DieByTouch);
                mesh.SetActive(false);
                this.move = false;
            }
            else if (byNoEnergy)
            {
                events.triggerEvent(CharacterEvents.EventName.DieByNoEnergy);
            }
            else
            {
                // Segurament hem mort per el trigger que deshabilita actors. No fem cap so
            }

            _currentAlive = StateAlive.Dead;

            // Avisem al ScenarioManager de que el player ha mort
            StandardStageManager.current.PlayerKilled(this.gameObject);
        }
    }

    public void RecieveDamage(GameObject from)
    {
        if(movementFSM.currentHorizontal == PlayerState.Horizontal.Travel)
        {
            // No puc rebre mal mentre estic fent un travel
            return;
        }

        if (_currentAlive != StateAlive.Dead && _currentAlive != StateAlive.Invulnerable)
        {
            events.triggerEvent(CharacterEvents.EventName.RecieveDamage);

            if (from.GetComponent<EnemyController>())
            {
                if (from.GetComponent<EnemyController>().killOnTouch)
                {
                    suit.energy.energy = 0;
                }
                else
                {
                    this.suit.RecieveDamage();
                }
            }
            else
            {
                this.suit.RecieveDamage();
            }

            if (suit.energy.energy <= 0)
            {
                StandardStageManager.current.PlayerKilledBy(this.gameObject, from);
                StartDying(true, false);
            }
            else
            {
                StartCoroutine(TouchedByEnemy());
            }
        }
    }

    private IEnumerator TouchedByEnemy()
    {
        _currentAlive = StateAlive.Invulnerable;

        _killeable = false;

        int timesBlink = 5;

        int framesBlink = 1;

        Time.timeScale = 0.001f;

        int i = 0;

        _mesh.SetActive(false);

        for (int j = 0; j < timesBlink; j++)
        {
            for (i = 0; i < framesBlink; i++)
            {
                yield return null;
            }

            _mesh.SetActive(!_mesh.activeInHierarchy);
        }

        _mesh.SetActive(true);

        Time.timeScale = 1;

        _killeable = true;

        //_controller.UpdateLayerMask(Layers.addLayer(_controller.layerMask, Layers.Enemy));

        _currentAlive = StateAlive.Playing;
    }
	#endregion

	#region External functions
    /// <summary>
    /// Espera a que el jugador hagi deixat de premer el boto de roll, espera /ROLL_MIN_ARAM_REROLL/ segons i torna a habilitar el Roll
    /// </summary>
    /// <returns></returns>
    public IEnumerator ResetRoll()
    {
        while (input.roll.check)
        {
            // Debug.Log ("[MainCharControl::ResetDash] ResetDash");
            yield return null;
        }
        yield return new WaitForSeconds(MovementProperties.ROLL_MIN_ARAM_REROLL);
        movementProps.canRoll = true;
    }
    
    /// <summary>
    /// Li dona una velocitat vertical 0 instantànea quan es xoca amb alguna cosa.
    /// No és realment una funció externa, és una funció per als Events.
    /// </summary>
    void OnBlockedUpEnter()
    {
        if (_controller.collisions.aboveObject.layer != Layers.Enemy)
        {
            jumpProperties.verticalSpeed = 0;
        }
    }

    void OnBlockedFrontEnter()
    {
        GameObject frontObject = (_controller.collisions.faceDir == 1) ? _controller.collisions.rightObject : _controller.collisions.leftObject;
        if (frontObject && Layers.inMask(Layers.Enemies, frontObject.layer))
        {
            EnemyController enemy = frontObject.GetComponent<EnemyController>();
            if (killNDie.ShouldKillThatBecauseOfJump(enemy))
            {
                return;
            }
        }

        if (movementFSM.currentVertical != PlayerState.Vertical.Jump || movementFSM.currentVertical != PlayerState.Vertical.WallJump)
        {
            movementProps.speed = 0;
        }
    }

    /// <summary>
    /// Used to add certain behaviours on some floors on grounded
    /// </summary>
    void OnGroundedEnter()
    {
        if(_controller.collisions.belowObject != null && _controller.collisions.belowObject.layer == Layers.BouncingObject)
        {
            EnterBounce();
        }
    }

	/// <summary>
    /// Fa entrar l'estat TripOver al player
	/// </summary>
    /// <param name="time">Durant quan temps perdra velocitat</param>
    [Obsolete("Ja no fem servir el trip over", true)]
	public void EnterTripOver( float time ) {
        events.triggerEvent(CharacterEvents.EventName.TripOverEnter);

		if( movementProps.speed > MovementProperties.RUN_SPEED / 2 && _currentAlive != StateAlive.TripOver ) {
			_input.Freeze( movementProps.runDirection, true );
			_currentAlive = StateAlive.TripOver;
			// SwapVertical( StateVertical.JumpFall );
			StartCoroutine( "TripOver", time );
		}
	}

    /// <summary>
    /// Exectuta el tripOver durant el <c>time</c> donat. Durant aquest temps, primer cau, després es manté al terra i després s'aixeca
    /// </summary>
    /// <param name="time">El temps que estarà parat. Se li sumen 0.8s que tarda en caure i en aixecar-se</param>
    /// <returns></returns>
    [Obsolete("Ja no fem servir el trip over", true)]
	private IEnumerator TripOver( float time ) {
		float startTime = Time.time;
		float fallTime = 0.3f;
		float upTime = 0.5f;

		while( startTime + fallTime > Time.time ) {
			// Debug.Log ( "[MainCharControl::TripOver] Fall on the wings of floor" );
			yield return null;
		}

		_input.Freeze( 0, true );
		while( startTime + time - upTime > Time.time ) {
			// Debug.Log ( "[MainCharControl::TripOver] In the floor again" );
			yield return null;
		}

		while( startTime + time > Time.time ) {
			movementProps.speed = 0;
			// Debug.Log ( "[MainCharControl::TripOver] Tiger uppingcut" );
			yield return null;
		}

		_input.Defreeze( );
		if( !_input.xAxis.check ) {
            //SwapHorizontal( StateHorizontal.Inertia );
		} else {
            //SwapHorizontal( StateHorizontal.Run );
		}

		_currentAlive = StateAlive.Playing;

        events.triggerEvent(CharacterEvents.EventName.TripOverExit);
	}

	/// <summary>
	/// Fa entrar l'estat Slide al player des de l'exterior
	/// </summary>
	/// <param name="time">Indica quan temps estara relliscant</param>
	/// <param name="speed">A quina velocitat relliscara</param>
    [Obsolete("Player doesn't has Slider anymore")]
	public void EnterSlide( float time, float speed ) {
        if(!jumping)
        {
            jumping = !jumping;
        }
        //_slideTime = time;
		//_slideSpeed = speed;

        // TODO No tenim l'estat d'slide fet

        //if (this.cVertical != null)
        //{
        //    StopCoroutine(this.cVertical);
        //}

        //SwapHorizontal( StateHorizontal.Slide );
	}

	/// <summary>
    /// Herencia de ISlideable, per saber si pot relliscar
	/// </summary>
	/// <returns><c>true</c> sempre</returns>
	public bool CanSlide() {
		return true;
	}

	/// <summary>
    /// Fa entrar un salt involutantari
	/// </summary>
    public void EnterBounce()
    {
        DebugUtils.AssertWarning(this.movementFSM.currentVertical != PlayerState.Vertical.Bounce, "Hem entrat a bounce quan ja estàvem fent un bounce", this.gameObject);

        this.movementFSM.ChangeCurrentVerticalState(PlayerState.Vertical.Bounce);
    }

    /// <summary>
    /// Es crida quan hem de començar un Travel
    /// Treu la gravetat, posa les velocitats a 0, deshabilita els colliders i el fa inmortal
    /// </summary>
    public void EnterTravel(LayerMask newLayerMask)
    {
        // Llencem event de que ha començat el teleport
        events.triggerEvent(CharacterEvents.EventName.TravelEnter);

        // Matem el power punch si el teníem iniciat
        if (suit.powerGlove)
        {
            suit.powerGlove.cancelCharge();
        }

        jumpProperties.currentGravity = JumpProperties.NO_GRAVITY;
        movementProps.speed = 0;
        jumpProperties.verticalSpeed = 0;
        _killeable = false;

        // Fem que col.lisioni nomes amb la Layer de la Blue Substance
        this._controller.UpdateLayerMask(newLayerMask);
        Physics2D.IgnoreLayerCollision(gameObject.layer, Layers.Projectiles, true);

        GetComponent<KillNDie>().enabled = false;

        // Guardem dades de quan hem començat
        travelProperties.travelStartTime = Time.time;
        travelProperties.travelStartPosition = transform.position;

        // Modifiquem la camera
        StartCoroutine(StandardStageManager.current.customCameraModifiers.ModifyCameraAtTravel());

        this.movementFSM.ChangeCurrentHorizontalState(PlayerState.Horizontal.Travel);
        this.movementFSM.ChangeCurrentVerticalState(PlayerState.Vertical.Travel);
    }

    /// <summary>
    /// Es crida quan acaba un travel
    /// Torna a posar la gravetat, habilitar colliders i KillNDie i li dona una velocitat de sortida en relació al temps transcorregut i l'angle desplaçat
    /// </summary>
    public void EndTravel(bool forced)
    {
        jumpProperties.currentGravity = JumpProperties.GRAVITY;
        _killeable = true;

        // Recuperem les col.lisions
        this._controller.UpdateLayerMask(this.defaultLayerMask);
        Physics2D.IgnoreLayerCollision(gameObject.layer, Layers.Projectiles, false);

        GetComponent<KillNDie>().enabled = true;

        // Llencem event de que ha acabat el teleport
        events.triggerEvent(CharacterEvents.EventName.TravelExit);

        float travelTime = Time.time - travelProperties.travelStartTime;

        float angle = Mathf.Atan2(transform.position.y - travelProperties.travelStartPosition.y, transform.position.x - travelProperties.travelStartPosition.x);

        // Cal que rotem al player?
        if (transform.position.x > travelProperties.travelStartPosition.x)
        {
            if (this.movementProps.lookingDirection != 1)
            {
                RotateMainChar();
            }

            movementProps.runDirection = 1;
        }
        else if(transform.position.x < travelProperties.travelStartPosition.x)
        {
            if (this.movementProps.lookingDirection != -1)
            {
                RotateMainChar();
            }

            movementProps.runDirection = -1;
        }

        movementProps.speed = Mathf.Min(TravelProperties.MAX_OUT_HORIZONTAL_SPEED, travelTime * TravelProperties.OUT_HORIZONTAL_SPEED_MULTIPLIER * Mathf.Abs(Mathf.Cos(angle)));

        jumpProperties.verticalSpeed = Mathf.Min(TravelProperties.MAX_OUT_VERTICAL_SPEED, travelTime * TravelProperties.OUT_VERTICAL_SPEED_MULTIPLIER * Mathf.Sin(angle));

        if(!forced)
        {
            StartCoroutine(StandardStageManager.current.customCameraModifiers.RecoverCameraAfterTravel());
        }

        movementFSM.ChangeCurrentHorizontalState(PlayerState.Horizontal.Inertia);
        movementFSM.ChangeCurrentVerticalState(PlayerState.Vertical.Inertia);
    }

    public void OnPauseEnter()
    {
        this.pauseProperties.paused = true;

        input.Freeze(0);

        this.move = false;
        this.GetComponent<Animator>().speed = 0;
        this.pauseProperties.speed = this.movementProps.speed;
        this.pauseProperties.verticalSpeed = this.jumpProperties.verticalSpeed;
    }

    public void OnPauseExit()
    {
        this.movementProps.speed = this.pauseProperties.speed;
        this.jumpProperties.verticalSpeed = this.pauseProperties.verticalSpeed;
        this.GetComponent<Animator>().speed = 1;

        this.pauseProperties.paused = false;

        // Wait a frame before recover movement
        StartCoroutine(RecoverMove());
    }

    /// <summary>
    /// Because of pause, next frame will have Time.deltaTime = 0 and will break our movemement
    /// We wait one frame to recover deltaTime, and then enable move and input again
    /// </summary>
    /// <returns></returns>
    private IEnumerator RecoverMove()
    {
        yield return null;
        move = true;
        input.Defreeze();
    }

	#endregion

    void Update()
    {
        if (this.move)
        {
            // Anem massa ràpid caient i no estem fent travel?
            if (jumpProperties.verticalSpeed < -JumpProperties.MAX_FALLING_SPEED && this.movementFSM.currentVertical != PlayerState.Vertical.Travel)
            {
                jumpProperties.verticalSpeed = -JumpProperties.MAX_FALLING_SPEED;
            }

            // Actualitzem la maquina d'estats
            this.movementFSM.Update();

            lastPosition = transform.position;
            // Move things
            _controller.Move(new Vector2(movementProps.speed * movementProps.runDirection, jumpProperties.verticalSpeed) * Time.deltaTime);

            if (_controller.collisions.below && jumpProperties.verticalSpeed < 0)
            {
                jumpProperties.verticalSpeed = 0;
            }
        }
    }

	/// <summary>
    /// Gira al MainChar i actualitza la variable de cap a on mira.
	/// </summary>
    public virtual void RotateMainChar()
    {
        //Debug.Log("Al rotar, estic bloquejat frontalment: " + this._controller.blockedFront, this.gameObject);
        if (!this.pauseProperties.paused)
        {
            this.movementProps.lookingDirection = -this.movementProps.lookingDirection;
            if(gameObject.activeInHierarchy)
            {
                StartCoroutine(Rotate(this.movementProps.lookingDirection));
            }
            else
            {
                _mesh.transform.Rotate(0, -180 * movementProps.lookingDirection, 0);
            }
        }
    }

    private IEnumerator Rotate(int direction)
    {
        _mesh.transform.Rotate(0, -30 * direction, 0);
        yield return null;
        _mesh.transform.Rotate(0, -30 * direction, 0);
        yield return null;
        _mesh.transform.Rotate(0, -30 * direction, 0);
        yield return null;
        _mesh.transform.Rotate(0, -30 * direction, 0);
        yield return null;
        _mesh.transform.Rotate(0, -30 * direction, 0);
        yield return null;
        _mesh.transform.Rotate(0, -30 * direction, 0);
    }

	/// <summary>
    /// Aplica una inercia al moviment del MainChar
	/// </summary>
    public void ApplyInertia(float timeStart, float startSpeed, ref float multFactor)
    {
        // Rotem ja al player?
        if (multFactor == 1)
        {
            if (this._controller.collisions.below)
            {
                multFactor = (movementProps.runDirection == -_input.xAxis.absoluteVal) ? MovementProperties.INERTIA_CORRECTION_DIVIDER : 1;
            }
            else
            {
                multFactor = (movementProps.runDirection == -_input.xAxis.absoluteVal) ? MovementProperties.ON_AIR_INERTIA_CORRECTION_DIVIDER : 1;
            }

            // Si hem canviat ara el multFactor, i no estem tocant el terra, girem abans de tocar el terra
            if (multFactor != 1 && !this._controller.collisions.below)
            {
                RotateMainChar();
            }

            // Si aquest frame hem canviat de direcció l'input, avisem
            if (multFactor != 1)
            {
                this.events.triggerEvent(CharacterEvents.EventName.InertiaChangingDirection);
            }
        }

        float speed = 0;

        float timePosition = (Time.time - timeStart) * multFactor;

        //Debug.Log("Time Positon: " + timePosition + " real time start: " + (Time.time - timeStart));

        if (this._controller.collisions.below)
        {
            Keyframe lastFrame = this.movementProps.runInertia[this.movementProps.runInertia.length - 1];

            if (lastFrame.time < timePosition)
            {
                speed = 0;
            }
            else
            {
                speed = Mathf.Min(this.movementProps.speed, this.movementProps.runInertia.Evaluate(timePosition) * startSpeed);
            }
        }
        else
        {
            Keyframe lastFrame = this.jumpProperties.jumpInertia[this.jumpProperties.jumpInertia.length - 1];

            if (lastFrame.time < timePosition)
            {
                speed = 0;
            }
            else
            {
                speed = Mathf.Min(this.movementProps.speed, this.jumpProperties.jumpInertia.Evaluate(timePosition) * startSpeed);
            }
        }

        //Debug.Log(string.Format("Mult factor: {0} - Speed: {1} - start time: {2} - grounded: {3} - sprint: {4}", multFactor, speed, timeStart, this._controller.grounded, this._input.sprint.check), this.gameObject);

        if (speed <= MovementProperties.STOP_MOVEMENT_THRESHOLD)
        {
            movementProps.speed = 0f;
        }
        else
        {
            movementProps.speed = speed;
        }
    }

    /// <summary>
    /// Serveix per a saber des de fora si el _controller està blocked front
    /// </summary>
    /// <returns><c>true</c> si està _controller.blockedFront</returns>
    public bool IsBlockedForward()
    {
        return _controller.blockedFront;
    }

     //<summary>
     //Serveix per a saber des de fora si el _controller està grounded
     //</summary>
     //<returns><c>true</c> si _controller.grounded</returns>
    public bool IsGrounded()
    {
        return _controller.collisions.below;
    }

    /// <summary>
    /// Ens indica si tenim totes les condicions necessàries per a poder fer Wall Jump (fent un fullWallGrab en les dues direccions amb una mida igual al collider)
    /// </summary>
    /// <returns></returns>
    public bool canWallJump()
    {
        bool can = fullWallGrab(1, this.collider2D.bounds.extents.x) || fullWallGrab(-1, this.collider2D.bounds.extents.x); //  _controller.blockedFront;
        can = can && (!_input.xAxis.check || _input.xAxis.absoluteVal == this.movementProps.lookingDirection || this.movementFSM.currentVertical == PlayerState.Vertical.Jump || this.movementFSM.currentVertical == PlayerState.Vertical.JumpFall || this.movementFSM.currentVertical == PlayerState.Vertical.WallJump);
        can = can && !_input.punch.check;

        //Debug.Log(string.Format("Can wall grab: full grab: {0} - check: {1} - absoluteVal: {2} - looking direction: {3} - blocked front: {4}", fullWallGrab(), _input.xAxis.check, _input.xAxis.absoluteVal, _lookingDirection, _controller.blockedFront), this.gameObject);

        return can;
    }

    /// <summary>
    /// Ens indica si podem, i volem, fer un crouch
    /// </summary>
    /// <returns>/True/ si podem fer crouch, /false/ altrament</returns>
    public bool testCrouch()
    {
        return _controller.collisions.below && Mathf.Approximately(_input.roll.val, 1) && !_input.xAxis.check && !_input.punch.check;
    }

    /// <summary>
    /// Ens indica si podem, i volem, fer un roll
    /// </summary>
    /// <returns>/True/ si podem fer roll, /false/ altrament</returns>
    public bool testRoll()
    {
        return _controller.collisions.below && Mathf.Approximately(_input.roll.val, 1) && _input.xAxis.check && !_input.punch.check && this.movementProps.canRoll;
    }

	/// <summary>
	/// Mira si estem col·lisionant completament a certa banda i distància del player
	/// </summary>
    /// <param name="direction">Direcció cap a on volem mirar si està fent wall grab completament</param>
    /// <param name="distanceChecker">Distància a la que volem mirar si fa wall grab completament</param>
	/// <returns><c>true</c>, if wall grab was fulled, <c>false</c> otherwise.</returns>
    public bool fullWallGrab(int direction, float distanceChecker)
    {
        float distance = direction * (distanceChecker + BoxColliderRayInfo.skinWidth);
        //return PhysicsCheckCollisions.fullCollide(collider2D.offset + (Vector2)transform.position, collider2D.bounds.extents.y, collider2D.bounds.extents.y, distance, _controller.collisionMask);
        Vector2 center = collider2D.offset + (Vector2)transform.position;
        bool fullWallGrab = true;
        int count = 0;

        RaycastHit2D ray;

        // Aquest es el moviment que fara aquest frame
        // (en teoria quan miro si fa un fullWallGrab es despres de calcular el moviment, pero com que no ho fa multipliquem per 2)
        Vector2 positionModifier = new Vector2(0, jumpProperties.verticalSpeed * 2 * Time.deltaTime);
        bool[] rayCollideInfo = new bool[_controller.rayInfo.horizontalRayCount];

        for (int i = 0; i < _controller.rayInfo.horizontalRayCount; i++)
        {
            Vector2 startPos = direction == 1 ? _controller.rayInfo.raycastOrigins.topRight : _controller.rayInfo.raycastOrigins.topLeft;
            startPos += positionModifier;
            startPos += Vector2.down * (_controller.rayInfo.horizontalRaySpacing * i);
            Debug.DrawRay(startPos, Vector2.right * LookingDirection * distanceChecker, Color.cyan);
            ray = Physics2D.Raycast(startPos, Vector2.right * direction, distanceChecker, _controller.collisionMask);

            if (ray.collider == null)
            {
                fullWallGrab = false;
                rayCollideInfo[i] = false;
            }
            else
            {
                rayCollideInfo[i] = true;
                count++;
            }
        }

        if(!fullWallGrab)
        {
            // Si el darrer frame estava agafat, i mes de la meitat de rajos segueixen tocant...
            if (movementProps.lastFrameWallGrab >= (Time.frameCount - 1) && count > _controller.rayInfo.horizontalRayCount / 2)
            {
                fullWallGrab = true;
            }
            else if (rayCollideInfo[0] && rayCollideInfo[rayCollideInfo.Length-1])
            {
                // Fan wallGrab les dues puntes, així que true
                fullWallGrab = true;
            }
        }

        if(fullWallGrab)
        {
            movementProps.lastFrameWallGrab = Time.frameCount;
        }
        
        //Debug.Log("He passat per aquí el frame " + Time.frameCount + " - i el wall grab era " + fullWallGrab);

        return fullWallGrab;
    }

    /// <summary>
    /// Disminueix el CharacterController a la meitat i li col·loca el centre de forma correcta.
    /// </summary>
    public void DecreaseController()
    {
        collider2D.size = new Vector2(collider2D.size.x, collider2D.size.y / 2);
        collider2D.offset = new Vector2(collider2D.offset.x, collider2D.offset.y - collider2D.size.y / 2);
        _controller.rayInfo.CalculateRaySpacing();
    }

    /// <summary>
    /// Augmenta el CharacterController al doble i li col·loca el centre de forma correcte
    /// </summary>
    public void IncreaseController()
    {
        collider2D.offset = new Vector2(collider2D.offset.x, collider2D.offset.y + collider2D.size.y / 2);
        collider2D.size = new Vector2(collider2D.size.x, collider2D.size.y * 2);
        _controller.rayInfo.CalculateRaySpacing();
    }

    /// <summary>
    /// Calcula la velocitat de sortida del salt, en funcio de si esta rebotant i de si esta en movmient
    /// </summary>
    /// <param name="bouncing">Cert si esta saltant per rebot</param>
    /// <returns></returns>
    public float CalculateJumpVerticalSpeed(bool bouncing)
    {
        float speed = bouncing ? jumpProperties.bounceHeight : JumpProperties.JUMP_FORCE;
        float multiplier = (!bouncing && _input.xAxis.absoluteVal == 0) ? 0.9f : 1f;
        // Debug.Log ( "[MainCharControl::CalculateJumpVerticalSpeed] speed: " + speed + " - multiplier: " + multiplier );
        // return Mathf.Sqrt( jumpProperties.jumpHeight * multiplier * JumpProperties.GRAVITY ); OLD jump function
        return speed * multiplier;
    }
}
