using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class RedHotDensityTeleport : MonoBehaviour
{
    /// <summary>
    /// Velocitat que agafa el player al fer teleport
    /// </summary>
    private static float TELEPORT_SPEED = 70;

    /// <summary>
    /// El CharacterControl del player
    /// </summary>
    private CharacterControl _control;

    private WSP_HighRezLaser _laser;

    public GameObject aimMarkPrefab;

    /// <summary>
    /// GameObject per a mostrar on s'estÃ  apuntant
    /// </summary>
    [HideInInspector]
    public GameObject aimMark;

    public GameObject redHotMetamorphosisPrefab;

    /// <summary>
    /// GameObject que simula a en SMB quan s'estÃ  teletransportant
    /// </summary>
    [HideInInspector]
    public GameObject redHotMetamorphosis;

    /// <summary>
    /// El trail que deixa el Hero al moure's. Per a poder-lo resetejar
    /// </summary>
    private TrailRenderer redHotMetamorphosisTrail;

    /// <summary>
    /// A quina alÃ§ada estan els ulls del Player per a mirar si agafa RHDot per el camÃ­
    /// </summary>
    public Transform eyes;

    /// <summary>
    /// Esdeveniment que es crida al comencar un teleport
    /// </summary>
    public Action teleportEnter;

    /// <summary>
    /// Esdeveniment que es crida al acabar un teleport
    /// </summary>
    public Action teleportExit;

    /// <summary>
    /// Box Collider 2D del Game Object
    /// </summary>
    private BoxCollider2D _boxCollider;

    public LayerMask inTravelLayerMask;

    /// <summary>
    /// Al habilitar, agafem el CharacterControl
    /// </summary>
    void OnEnable()
    {
        _control = GetComponent<CharacterControl>();

        _laser = transform.Find("ToDensityBeam").gameObject.GetComponent<WSP_HighRezLaser>();
        _laser.gameObject.SetActive(false);
        _laser.LaserBeamActive = true;
        _laser.LaserCanFire = true;
        _laser.UseExtendedLength = false;
        //_laser.LaserFireTime = 999;

        aimMark = Instantiate(aimMarkPrefab, transform.position - new Vector3(0, 0, -5), Quaternion.identity) as GameObject;
        aimMark.SetActive(false);

        _boxCollider = GetComponent<BoxCollider2D>();

        if(redHotMetamorphosis == null)
        {
            redHotMetamorphosis = Instantiate(redHotMetamorphosisPrefab, transform.position + (Vector3)_boxCollider.offset, Quaternion.identity) as GameObject;
        }
        
        redHotMetamorphosisTrail = redHotMetamorphosis.GetComponentInChildren<TrailRenderer>(); // Guardem el trail abans de desactivar
        redHotMetamorphosis.SetActive(false);
        redHotMetamorphosis.transform.parent = transform;
        redHotMetamorphosis.tag = Tags.TravellingHero;
    }

    /// <summary>
    /// Creem el ZTarget de les explosions que hi ha en aquest moment en pantalla
    /// Saltem al millor portal segons l'inclinaciÃ³ del Joystick, si n'hi ha algun
    /// </summary>
    IEnumerator ZTarget()
    {
        // Variable on guardarem l'angle actual del joystic, millor angle d'aquesta ronda i Ãºltim angle utilitzat.
        float axisAngle = 999, lastAngle;

        // Guardem el millor GameObject per a disparar i una instÃ ncia del darrer que hem fet servir.
        GameObject bestPortal = null;

        // Mentre tinguem el botÃ³ de disparar premut...
        while (_control.input.travel.check)
        {
            lastAngle = axisAngle;

            Vector3 direction = new Vector3(Mathf.Abs(_control.input.xAxis.val), _control.input.yAxis.val).normalized;
            
            if(Mathf.Approximately(_control.input.xAxis.val, 0) && Mathf.Approximately(_control.input.yAxis.val, 0))
            {
                direction = Vector3.right;
            }

            axisAngle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
            //Debug.Log("Direction: " + direction + " - angle: " + axisAngle);

            bestPortal = RedHotTarget.Target(bestPortal, axisAngle, _control.LookingDirection, transform.position, _boxCollider, lastAngle, Layers.RedHotDensities, RedHotTarget.instance.redHotDensities);

            if (bestPortal)
            {
                _laser.CurrentTarget = bestPortal.transform;
                _laser.TargetHit = bestPortal.transform;
                _laser.UpdateLaserBeam();

                if(!_laser.gameObject.activeInHierarchy)
                {
                    _control.events.triggerEvent(CharacterEvents.EventName.RHDTargeted);
                }

                _laser.gameObject.SetActive(true);
                

                aimMark.transform.position = bestPortal.transform.position;
                aimMark.SetActive(false);
            }
            else
            {
                if (_laser.gameObject.activeInHierarchy)
                {
                    _control.events.triggerEvent(CharacterEvents.EventName.RHDTargetedEnd);
                }

                _laser.gameObject.SetActive(false);
                aimMark.SetActive(false);
            }

            yield return null;
        }

        if (_laser.gameObject.activeInHierarchy)
        {
            _control.events.triggerEvent(CharacterEvents.EventName.RHDTargetedEnd);
        }

        _laser.gameObject.SetActive(false);
        aimMark.SetActive(false);

        // Si ja està dins de blue substance, fa com que va a saltar però es queda al mateix lloc
        if (bestPortal && insideBlueSubstance())
        {
            StopCoroutine("DensityJump");
            StartCoroutine("DensityJump", this.gameObject);
        }
        else if (bestPortal)
        {
            StopCoroutine("DensityJump");
            StartCoroutine("DensityJump", bestPortal);
        }
    }

    private IEnumerator DensityJump(GameObject bestPortal)
    {
        if (this._control.currentAlive == CharacterControl.StateAlive.Dead)
        {
            // Hem entrat aquÃ­ un cop mort
            yield break;
        }

        // Li diem a el Control que entrem en un DensityJump (aixo desactiva triggers, gravetat, etc.)
        _control.EnterTravel(Layers.addLayer(0, Layers.BlueSubstance));

        // Desconnectem el control
        _control.input.FreezeExceptTravel();

        // Creo copia de la posicio del portal per si desaparegues
        Vector3 bestPortalPosition;

        // Busquem la direcció en la que ens movem
        Vector3 direction = bestPortal.transform.position - (transform.position + (Vector3)_control.collider2D.offset);

        List<GameObject> redHotDots;

        if (insideBlueSubstance())
        {
            Debug.Log("Som dins de blue substance");
            // Si estem dins de blue substance fem com que anem a fer travel però no el fem
            bestPortalPosition = transform.position;

            redHotDots = new List<GameObject>();
        }
        else
        {
            bestPortalPosition = new Vector3(bestPortal.transform.position.x, bestPortal.transform.position.y);

            // Mirem si podrem posar el player a lloc quan arribem
            bestPortalPosition = testPlayerFit(bestPortalPosition, direction);

            // Mirem si hi ha blue substance pel camí
            bestPortalPosition = this.testBlueSubstance(bestPortalPosition);

            // Busuquem tots els objectes que hem d'agafar per el camÃi
            redHotDots = FindRedHotDots(bestPortalPosition);
        }

        // Amaguem la mesh i mostrem la bola d'energia
        float trailTime = this.redHotMetamorphosisTrail.time;
        this.redHotMetamorphosisTrail.time = 0;
        redHotMetamorphosis.SetActive(true);
        redHotMetamorphosis.GetComponentInChildren<ParticleSystem>().Clear();
        _control.mesh.SetActive(false);
        
        // Re-activem el trail
        yield return null;
        this.redHotMetamorphosisTrail.time = trailTime;

        // Donem velocitats al player
        this._control.movementProps.runDirection = (int)Mathf.Sign(direction.x);
        float factor = TELEPORT_SPEED / Mathf.Sqrt(Mathf.Pow(direction.x, 2) + Mathf.Pow(direction.y, 2));
        this._control.movementProps.speed = Mathf.Abs(factor * direction.x);
        this._control.jumpProperties.verticalSpeed = factor * direction.y;
        //Debug.Log("Velocitat calculada en les X: " + _control.movementProps.speed + " - Velocitat y: " + _control.jumpProperties.verticalSpeed);

        Vector3 originalPosition = transform.position;
        float distance = Mathf.Abs(Vector3.Distance(transform.position, bestPortalPosition));

        float totalDistance = 0;

        float minDistanceToExit = 0.01f;

        bool wasBlocked = false;

        while (totalDistance < distance)
        {
            if ((_control.movementProps.runDirection == 1 && _control.controller.collisions.right) || (_control.movementProps.runDirection == -1 && _control.controller.collisions.left)
                || _control.controller.collisions.above || _control.controller.collisions.below)
            {
                wasBlocked = true;
                //Debug.Log(string.Format("I was blocked front {0}, above {1}, below {2}", _control.controller.blockedFront, _control.controller.collisions.above, _control.controller.collisions.below));
                break;
            }

            foreach (GameObject rhd in redHotDots)
            {
                if (rhd != null && rhd.activeInHierarchy && rhd.transform.position.x < this.transform.position.x)
                {
                    rhd.GetComponent<Pickeable>().Pick(this.gameObject);
                }
            }

            totalDistance = Mathf.Abs(Vector3.Distance(originalPosition, transform.position));

            float currentDistance = Vector3.Distance(transform.position, bestPortalPosition);

            if (totalDistance < distance && currentDistance < minDistanceToExit)
            {
                // Forcem la sortida
                totalDistance = distance;
            }

            // Retoquem velocitat si cal
            if (currentDistance < TELEPORT_SPEED * Time.deltaTime)
            {
                factor = (Vector3.Distance(transform.position, bestPortalPosition) / Time.deltaTime) / Mathf.Sqrt(Mathf.Pow(direction.x, 2) + Mathf.Pow(direction.y, 2));
                _control.movementProps.speed = Mathf.Abs(factor * direction.x);
                _control.jumpProperties.verticalSpeed = factor * direction.y;
            }

            yield return null;
        }

        if(!wasBlocked)
        {
            transform.position = bestPortalPosition;
        }

        // Desactivem la bola d'energia i mostrem la mesh
        redHotMetamorphosis.SetActive(false);
        _control.mesh.SetActive(true);

        // Tornem a donar control al jugador de l'input
        _control.input.Defreeze();

        // Despres d'un temps de freeze, apaguem el densityJump al control
        _control.EndTravel(false);
    }

    private List<GameObject> FindRedHotDots(Vector3 bestPortalPosition)
    {
        List<GameObject> redHotDots = new List<GameObject>();

        RaycastHit2D[] dots = new RaycastHit2D[50];

        int total = Physics2D.LinecastNonAlloc(eyes.position, bestPortalPosition, dots, Layers.RedHotDots);
        
        Debug.DrawLine(eyes.position, bestPortalPosition);

        //Debug.Log("En total hi ha " + total + " Red Hot Dots.", this.gameObject);

        for(int i = 0; i < total; i++)
        {
            if (dots[i].collider.GetComponent<Pickeable>())
            {
                redHotDots.Add(dots[i].collider.gameObject);
            }
        }

        return redHotDots;
    }

    /// <summary>
    /// Testeja si hi ha blue substance entre la posicio actual i el <paramref name="finalPoint"/>
    /// </summary>
    /// <param name="finalPoint">Punt final del trajecte</param>
    /// <returns>Si s'ha trobat blue substance, retorna la posicio on s'ha trobat. Altrament retorna el <paramref name="finalPoint"/></returns>
    private Vector3 testBlueSubstance(Vector3 finalPoint)
    {
        Vector3 start = transform.position + (Vector3)_boxCollider.offset;
        Vector3 end = finalPoint;
        RaycastHit2D ray;

        Physics2D.queriesHitTriggers = true;
        if (ray = Physics2D.Linecast(start, finalPoint, Layers.addLayer(0, Layers.BlueSubstance)))
        {
            // El 0.1f és el skin del controller
            end = ray.point + new Vector2(this._boxCollider.offset.x + (this._boxCollider.size.x / 2 + 0.05f) * this._control.movementProps.runDirection, 0);
        }
        Physics2D.queriesHitTriggers = false;

        return end;
    }

    private Vector3 testPlayerFit(Vector3 finalPoint, Vector3 direction)
    {
        //Debug.Log("Current position: " + _control.transform.position + " - final point: " + finalPoint + " - " + direction);
        
        // Donc per fet que una density no es pot crear MAI dins d'una paret
        float xDirection = (direction.x > 0) ? 1 : -1;
        float yDirection = (direction.y > 0) ? 1 : -1;
        float xRayLenght = _control.collider2D.bounds.extents.x;
        float yRayLenght = _control.collider2D.bounds.extents.y;

        LayerMask collideLayer = (1 << Layers.Ground) | (1 << Layers.OneWayPlatform);

        // Dibuixem una creu a on tenim el finalPoint inicial
        Debug.DrawLine(finalPoint - Vector3.right - Vector3.up, finalPoint + Vector3.right + Vector3.up, Color.yellow, 20);
        Debug.DrawLine(finalPoint - Vector3.up + Vector3.right, finalPoint + Vector3.up - Vector3.right, Color.yellow, 20);

        RaycastHit2D hit = Physics2D.Raycast(finalPoint, Vector2.right * xDirection, xRayLenght, collideLayer);

        // Dibixem un raig blau en horitzontal on busquem col.lisions
        Debug.DrawRay(finalPoint, Vector2.right * xDirection * xRayLenght, Color.blue, 20);

        if (hit)
        {
            finalPoint.x = hit.point.x - (_control.collider2D.bounds.extents.x * xDirection);
        }

        hit = Physics2D.Raycast(finalPoint, Vector2.up * yDirection, yRayLenght, collideLayer);

        // Dibixem un raig blau en vertical on busquem col.lisions
        Debug.DrawRay(finalPoint, Vector2.up * yDirection * yRayLenght, Color.blue, 20);

        if (hit)
        {
            finalPoint.y = hit.point.y - (_control.collider2D.bounds.extents.y * yDirection);
        }

        // El centrem al cos del player, no als peus
        finalPoint = finalPoint - (Vector3)_control.collider2D.offset;

        // Dibuixem una creu a on tenim el finalPoint final
        Debug.DrawLine(finalPoint - Vector3.right - Vector3.up, finalPoint + Vector3.right + Vector3.up, Color.magenta, 20);
        Debug.DrawLine(finalPoint - Vector3.up + Vector3.right, finalPoint + Vector3.up - Vector3.right, Color.magenta, 20);

        // Dibuixem on hauria de quedar el quadrat del collider del player
        Vector3 topRight = finalPoint + (Vector3)_control.collider2D.offset + (Vector3)_control.collider2D.size / 2;
        Vector3 bottomLeft = finalPoint + (Vector3)_control.collider2D.offset - (Vector3)_control.collider2D.size / 2;
        Vector3 topLeft = finalPoint + (Vector3)_control.collider2D.offset + new Vector3(-_control.collider2D.size.x, _control.collider2D.size.y) / 2;
        Vector3 bottomRight = finalPoint + (Vector3)_control.collider2D.offset - new Vector3(-_control.collider2D.size.x, _control.collider2D.size.y) / 2;
        Debug.DrawLine(bottomLeft, topLeft, Color.magenta, 20);
        Debug.DrawLine(topLeft, topRight, Color.magenta, 20);
        Debug.DrawLine(topRight, bottomRight, Color.magenta, 20);
        Debug.DrawLine(bottomRight, bottomLeft, Color.magenta, 20);   

        return finalPoint;
    }

    /// <summary>
    /// Mira si el player esta dins de la blue substance tenint en compte la mida del seu box collider
    /// </summary>
    /// <returns>Cert si esta dins de la blue substance, fals altrament</returns>
    private bool insideBlueSubstance()
    {
        // Fem el check en un quadradet de .5f per .5f
        Vector3 min = transform.position + (Vector3)_control.collider2D.offset - new Vector3(.25f, .25f);
        Vector3 max = transform.position + (Vector3)_control.collider2D.offset + new Vector3(.25f, .25f);
        // Mostrem el quadrat en pantalla (per debug)
        Debug.DrawLine(min, min + new Vector3(.5f, 0), Color.cyan, 2f);
        Debug.DrawLine(min + new Vector3(.5f, 0), max, Color.cyan, 2f);
        Debug.DrawLine(max, max - new Vector3(.5f, 0), Color.cyan, 2f);
        Debug.DrawLine(max - new Vector3(.5f, 0), min, Color.cyan, 2f);

        Physics2D.queriesHitTriggers = true;
        bool inside = Physics2D.OverlapArea(min, max, 1 << Layers.BlueSubstance) != null;
        Physics2D.queriesHitTriggers = false;

        return inside;
    }

	/// <summary>
	/// Mirem si es prem el botÃ³ de teletransport.
	/// </summary>
	void Update () {
        //Debug.Log(Input.GetAxis("HorizontalJ0") + " - " + Input.GetAxis("VerticalJ0"));

        if (_control.input.travel.down)
        {
            StartCoroutine(ZTarget());
        }
	}
}
