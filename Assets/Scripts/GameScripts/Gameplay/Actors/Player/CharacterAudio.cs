using UnityEngine;


public class CharacterAudio : MonoBehaviour
{
    /// <summary>
	/// Volum del jump
	/// </summary>
    [AudioClip(AudioClipsData.ClipsGroups.Player)]
    public string jumpName;

    /// <summary>
    /// Volum del punch
    /// </summary>
    [AudioClip(AudioClipsData.ClipsGroups.Player)]
    public string punchName;

    [AudioClip(AudioClipsData.ClipsGroups.Player)]
    public string nothingPowerGlovedName;

    /// <summary>
    /// Volum del punch
    /// </summar>
    [AudioClip(AudioClipsData.ClipsGroups.Player)]
    public string refillName;

    /// <summary>
    /// Volum del travel
    /// </summary>
    [AudioClip(AudioClipsData.ClipsGroups.Player)]
    public string travelName;

    /// <summary>
    /// Volum de quan agafes una coin
    /// </summary>
    [AudioClip(AudioClipsData.ClipsGroups.Player)]
    public string itemName;

    /// <summary>
    /// So de quan reps mal
    /// </summary>
    [AudioClip(AudioClipsData.ClipsGroups.Player)]
    public string damageName;

    /// <summary>
    /// Volum quan mors
    /// </summary>
    [AudioClip(AudioClipsData.ClipsGroups.Player)]
    public string dieName;

    public float groundedLongFallingMinTime = 0.35f;

    /// <summary>
    /// So quan toques el terra de normal
    /// </summary>
    [AudioClip(AudioClipsData.ClipsGroups.Player)]
    public string groundedName;

    /// <summary>
    /// So quan toques el terra despres de molta estona caient
    /// </summary>
    [AudioClip(AudioClipsData.ClipsGroups.Player)]
    public string groundedLongFallingName;

    /// <summary>
    /// Volume quan rebotes al saltar sobre algo.
    /// </summary>
    [AudioClip(AudioClipsData.ClipsGroups.Player)]
    public string bounceName;

    [AudioClip(AudioClipsData.ClipsGroups.Player)]
    public string highBounceName;

    /// <summary>
    /// El roll volume no te efecte en el long play, la deixo com a privada per no confondre i mirar si hi ha temps.
    /// </summary>
    [AudioClip(AudioClipsData.ClipsGroups.Player)]
    public string rollName;

    /// <summary>
    /// Volum de quan fa el salt entre parets
    /// </summary>
    [AudioClip(AudioClipsData.ClipsGroups.Player)]
    public string wallJumpName;

    /// <summary>
    /// Volum de quan fa el salt entre parets
    /// </summary>
    [AudioClip(AudioClipsData.ClipsGroups.Player)]
    public string rhdTargeted;

    /// <summary>
    /// Volum de quan vol disparar i no pot
    /// </summary>
    [AudioClip(AudioClipsData.ClipsGroups.Player)]
    public string noEnergyOnFailShootName;

    /// <summary>
    /// Volum de quan dispara
    /// </summary>
    [AudioClip(AudioClipsData.ClipsGroups.Player)]
    public string shootName;

    /// <summary>
    /// Sound played when player dies by no energy
    /// </summary>
    [AudioClip(AudioClipsData.ClipsGroups.Player)]
    public string energyOutClip = "PowerOff";

    [AudioClip(AudioClipsData.ClipsGroups.Player)]
    public string inertiaClip;

    /// <summary>
    /// Should play Red Hot Density targeting sound?
    /// </summary>
    private bool rhdTargeting = false;

    private CharacterControl _control;

    void OnEnable()
    {
        // Agafem el MainCharControl
        _control = GetComponent<CharacterControl>();


        _control.events.registerEvent(CharacterEvents.EventName.JumpEnter, OnJumpEnter);
        _control.events.registerEvent(CharacterEvents.EventName.RollEnter, OnRollEnter);
        _control.events.registerEvent(CharacterEvents.EventName.BounceEnter, OnBounceEnter);
        _control.events.registerEvent(CharacterEvents.EventName.DieByTouch, OnDieEnter);
        _control.events.registerEvent(CharacterEvents.EventName.DieByNoEnergy, OnDieByNoEnergy);
        _control.events.registerEvent(CharacterEvents.EventName.GroundedEnter, OnGrounded);
        _control.events.registerEvent(CharacterEvents.EventName.ReFill, OnReFill);
        _control.events.registerEvent(CharacterEvents.EventName.WallJumpEnter, OnWallJump);
        _control.events.registerEvent(CharacterEvents.EventName.RHDTargeted, OnRHDTargeted);
        _control.events.registerEvent(CharacterEvents.EventName.RHDTargetedEnd, OnRHDTargetedEnd);
        _control.events.registerEvent(CharacterEvents.EventName.NoEnergyFailShot, OnFailShot);
        _control.events.registerEvent(CharacterEvents.EventName.Bluesubstanced, OnFailShot);
        _control.events.registerEvent(CharacterEvents.EventName.Shoot, OnShoot);
        _control.events.registerEvent(CharacterEvents.EventName.RecieveDamage, OnDamage);
        _control.events.registerEvent(CharacterEvents.EventName.InertiaEnter, OnInertia);

        _control.events.registerEvent(CharacterEvents.EventName.TravelEnter, OnTravelEnter);
        _control.events.registerEvent(CharacterEvents.EventName.ObjectPowerGloved, OnObjectPowerGloved);
        _control.events.registerEvent(CharacterEvents.EventName.NothingPoweredFisting, OnNothingPowerGloved);
    }

    void OnDisable()
    {
        _control.events.unregisterEvent(CharacterEvents.EventName.JumpEnter, OnJumpEnter);
        _control.events.unregisterEvent(CharacterEvents.EventName.RollEnter, OnRollEnter);
        _control.events.unregisterEvent(CharacterEvents.EventName.BounceEnter, OnBounceEnter);
        _control.events.unregisterEvent(CharacterEvents.EventName.DieByTouch, OnDieEnter);
        _control.events.unregisterEvent(CharacterEvents.EventName.DieByNoEnergy, OnDieByNoEnergy);
        _control.events.unregisterEvent(CharacterEvents.EventName.GroundedEnter, OnGrounded);
        _control.events.unregisterEvent(CharacterEvents.EventName.ReFill, OnReFill);
        _control.events.unregisterEvent(CharacterEvents.EventName.WallJumpEnter, OnWallJump);
        _control.events.unregisterEvent(CharacterEvents.EventName.RHDTargeted, OnRHDTargeted);
        _control.events.unregisterEvent(CharacterEvents.EventName.RHDTargetedEnd, OnRHDTargetedEnd);
        _control.events.unregisterEvent(CharacterEvents.EventName.NoEnergyFailShot, OnFailShot);
        _control.events.unregisterEvent(CharacterEvents.EventName.Bluesubstanced, OnFailShot);
        _control.events.unregisterEvent(CharacterEvents.EventName.Shoot, OnShoot);
        _control.events.unregisterEvent(CharacterEvents.EventName.RecieveDamage, OnDamage);
        _control.events.unregisterEvent(CharacterEvents.EventName.InertiaEnter, OnInertia);

        _control.events.unregisterEvent(CharacterEvents.EventName.TravelEnter, OnTravelEnter);
        _control.events.unregisterEvent(CharacterEvents.EventName.ObjectPowerGloved, OnObjectPowerGloved);
        _control.events.unregisterEvent(CharacterEvents.EventName.NothingPoweredFisting, OnNothingPowerGloved);
    }

    public void ItemAudio()
    {
        AudioManager.Instance.PlayOneShoot(AudioClipsData.ClipsGroups.Player, itemName);
    }

    void OnJumpEnter()
    {
        AudioManager.Instance.PlayOneShoot(AudioClipsData.ClipsGroups.Player, jumpName);
    }

    void OnRollEnter()
    {
        AudioManager.Instance.LongPlay(AudioClipsData.playerPrefix + rollName,
            () => { return _control.movementFSM.currentHorizontal != PlayerState.Horizontal.Roll; });
    }

    void OnRHDTargeted()
    {
        rhdTargeting = true;
        AudioManager.Instance.LongPlay(AudioClipsData.playerPrefix + rhdTargeted,
            () => { return !rhdTargeting; });
    }

    void OnRHDTargetedEnd()
    {
        rhdTargeting = false;
    }

    void OnBounceEnter()
    {
        if(_control.jumpProperties.bounceCount > 0)
        {
            // Estem fent un bounce alt
            AudioManager.Instance.PlayOneShoot(AudioClipsData.ClipsGroups.Player, highBounceName);
        }
        else
        {
            AudioManager.Instance.PlayOneShoot(AudioClipsData.ClipsGroups.Player, bounceName);
        }
    }

    void OnDamage()
    {
        AudioManager.Instance.PlayOneShoot(AudioClipsData.ClipsGroups.Player, damageName);
    }

    void OnDieEnter()
    {
        AudioManager.Instance.PlayOneShoot(AudioClipsData.ClipsGroups.Player, dieName);
    }

    void OnTravelEnter()
    {
        AudioManager.Instance.PlayOneShoot(AudioClipsData.ClipsGroups.Player, travelName);
    }

    void OnNothingPowerGloved()
    {
        AudioManager.Instance.PlayOneShoot(AudioClipsData.ClipsGroups.Player, nothingPowerGlovedName);
    }

    void OnObjectPowerGloved()
    {
        AudioManager.Instance.PlayOneShoot(AudioClipsData.ClipsGroups.Player, punchName);
    }

    void OnDieByNoEnergy()
    {
        AudioManager.Instance.PlayOneShoot(AudioClipsData.ClipsGroups.Player, energyOutClip);
    }

    void OnGrounded()
    {
        if(_control.jumpProperties.fallingTime < groundedLongFallingMinTime)
        {
            AudioManager.Instance.PlayOneShoot(AudioClipsData.ClipsGroups.Player, groundedName);
        }
        else
        {
            AudioManager.Instance.PlayOneShoot(AudioClipsData.ClipsGroups.Player, groundedLongFallingName);
        }
        
    }

    void OnReFill()
    {
        AudioManager.Instance.PlayOneShoot(AudioClipsData.ClipsGroups.Player, refillName);
    }

    void OnWallJump()
    {
        AudioManager.Instance.PlayOneShoot(AudioClipsData.ClipsGroups.Player, wallJumpName);
    }

    void OnFailShot()
    {
        AudioManager.Instance.PlayOneShoot(AudioClipsData.ClipsGroups.Player, noEnergyOnFailShootName);
    }

    void OnShoot()
    {
        AudioManager.Instance.PlayOneShoot(AudioClipsData.ClipsGroups.Player, shootName);
    }

    void OnInertia()
    {
        if(_control.IsGrounded())
        {
            AudioManager.Instance.PlayOneShoot(AudioClipsData.ClipsGroups.Player, inertiaClip);
        }
    }
}
