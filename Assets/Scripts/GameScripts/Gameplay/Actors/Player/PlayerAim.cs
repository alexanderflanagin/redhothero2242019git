using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Classe que s'encarrega de mirar cada frame cap a on està mirant el player
/// </summary>
public class PlayerAim : MonoBehaviour
{
    /// <summary>
    /// L'angle màxim de correcció a l'hora de calcular on apuntem
    /// </summary>
    private static float MAX_CORRECTION_ANGLE = 6f;

    /// <summary>
    /// Idem as before but for punch
    /// </summary>
    private static float MAX_CORRECTION_ANGLE_PUNCH = 24f;
    
    /// <summary>
    /// El Controller del Player
    /// </summary>
    private CharacterControl _controller;

    /// <summary>
    /// La meitat de PI. Ho fem servir per a calcular direccions
    /// </summary>
    //private float halfPi = Mathf.PI / 2;

    /// <summary>
    /// Per saber si aquest frame ja hem calculat valors
    /// </summary>
    private int _lastCalculatedFrame = -1;

    /// <summary>
    /// Per saber si aquest frame ja hem calculat la posició corregida del tret
    /// </summary>
    private int _lastCalculatedFrameCorrectedShoot = -1;

    /// <summary>
    /// Per saber si aquest frame ja hem calculat la posició corregida del puny
    /// </summary>
    private int _lastCalculatedFrameCorrectedPunch = -1;

    /// <summary>
    /// Vector direccional de cap a on està apuntant el jugador
    /// </summary>
    private Vector3 _lookingAtPoint;

    /// <summary>
    /// Vector direccional de cap a on està apuntant el jugador corregit segons els objectes shootables
    /// </summary>
    private Vector3 _lookingAtPointCorrectedShoot;

    /// <summary>
    /// Vector direccional de cap a on està apuntant el jugador corregit segons els objectes magnetics per al power punch
    /// </summary>
    private Vector3 _lookingAtPointCorrectedPunch;

    public Shootable target;

    private Transform _hand;

    /// <summary>
    /// Al fer awake, busquem el controller del Player
    /// </summary>
    void Awake()
    {
        _controller = GetComponentInParent<CharacterControl>();
    }

    void Start()
    {
        _hand = transform.Find("Eyes");
    }

    /// <summary>
    /// Busquem el vector direccional de cap a on està mirant el player. Revisem si ja ho hem fet aquest frame primer
    /// </summary>
    /// <returns>El vector direccional de cap a on mira</returns>
    public Vector3 getLookingAtPoint()
    {
        if (_lastCalculatedFrame < Time.frameCount)
        {
            PlayerAimDef();
            _lookingAtPoint.Normalize();
            Debug.DrawLine(_hand.transform.position, _hand.transform.position + _lookingAtPoint * 20, Color.blue);
            _lastCalculatedFrame = Time.frameCount;
        }

        return _lookingAtPoint;
    }

    /// <summary>
    /// Assigna el valor de lookingAtPoint segons a on apunten els axis del player
    /// </summary>
    private void PlayerAimDef()
    {
        DebugUtils.AssertError(_controller.input.xAxis.alreadyChecked && _controller.input.yAxis.alreadyChecked, "Error axis not defined");

        if (!Mathf.Approximately(_controller.input.yAxis.val, 0))
        {
            _lookingAtPoint = new Vector3(_controller.input.xAxis.val, _controller.input.yAxis.val).normalized;
        }
        else
        {
            _lookingAtPoint = Vector3.right * _controller.LookingDirection;
        }
    }

    /// <summary>
    /// Busca el vector direccional corregit de cap a on s'apunta tenint en compte una llista de GameObjects
    /// </summary>
    /// <param name="list">La llista de GameObjects a tenir en compte a l'hora de corregir l'apuntat</param>
    /// <returns>El vector direccional corregit de cap a on apuntem</returns>
    private Vector3 getLookingAtPointCorrected(List<GameObject> list, bool powerGlove)
    {
        Vector3 lookingAtPoint = getLookingAtPoint();
        Vector3 lookingAtCorrected = lookingAtPoint;

        target = null;

        float correctedAngle = 360;
        // Si tenim shootables, buscarem el més ben colocat per a rebre el tret
        if (list.Count > 0)
        {
            float maxCorrectionAngle = powerGlove ? MAX_CORRECTION_ANGLE_PUNCH : MAX_CORRECTION_ANGLE;
            foreach (GameObject go in list)
            {
                float angle = Mathf.Atan2(go.transform.position.y - _hand.transform.position.y, go.transform.position.x - _hand.transform.position.x) * Mathf.Rad2Deg;
                float angle2 = Mathf.Atan2(lookingAtPoint.y, lookingAtPoint.x) * Mathf.Rad2Deg;
                //Debug.Log("Angle: " + angle + " - Angle2: " + angle2, go.gameObject);
                if (Mathf.Abs(Mathf.Abs(angle) - Mathf.Abs(angle2)) < maxCorrectionAngle && Mathf.Abs(Mathf.Abs(angle) - Mathf.Abs(angle2)) < correctedAngle)
                {
                    correctedAngle = angle;
                    lookingAtCorrected = Vector3.Normalize(new Vector3(Mathf.Cos(angle * Mathf.Deg2Rad), Mathf.Sin(angle * Mathf.Deg2Rad)));
                    target = go.GetComponent<Shootable>();
                }
            }
        }

        return lookingAtCorrected;
    }

    /// <summary>
    /// Calcula el vector direccional corregit de cap a on apuntem al disparar
    /// </summary>
    /// <returns>Vector direccional corregit amb els objectes shootables</returns>
    public Vector3 getLookingAtPointCorrectedShoot()
    {
        if (_lastCalculatedFrameCorrectedShoot < Time.frameCount)
        {
            // Si disparem recte, toquem res?
            // Punt a on disparem
            Vector3 direction = getLookingAtPoint();

            // Distancia maxima a la que volem mirar si toquem res
            float distance = 20;

            // Posicio final segons la direccio i la distancia
            Vector3 finalPosition = _hand.transform.position + direction * distance;

            Debug.DrawLine(_hand.transform.position, finalPosition, Color.cyan);

            RaycastHit2D hit = Physics2D.Linecast(_hand.position, finalPosition, _controller.controller.collisionMask);
            if (hit && hit.transform.GetComponent<Shootable>())
            {
                // Si disparant recte ja toquem un shootable...
                target = hit.transform.GetComponent<Shootable>();
                _lookingAtPointCorrectedShoot = getLookingAtPoint();
            }
            else
            {
                _lookingAtPointCorrectedShoot = getLookingAtPointCorrected(RedHotTarget.instance.shootables, false);
            }

            Debug.DrawLine(_hand.transform.position, _hand.transform.position + _lookingAtPointCorrectedShoot * 20, Color.blue);
            _lastCalculatedFrameCorrectedShoot = Time.frameCount;
        }

        return _lookingAtPointCorrectedShoot;
    }

    /// <summary>
    /// Calcula el vector direccional corregit de cap a on apuntem al colpejar
    /// </summary>
    /// <returns>Vector direccional corregit amb els objectes power gloved magnets</returns>
    public Vector3 getLookingAtPointCorrectedPunch()
    {
        if (_lastCalculatedFrameCorrectedPunch < Time.frameCount)
        {
            _lookingAtPointCorrectedPunch = getLookingAtPointCorrected(RedHotTarget.instance.powerGlovedMagnets, true);

            Debug.DrawLine(_hand.transform.position, _hand.transform.position + _lookingAtPointCorrectedPunch * 20, Color.cyan);
            
            _lastCalculatedFrameCorrectedPunch = Time.frameCount;

            //Debug.Log(this._lookingAtPointCorrectedPunch.ToString("0.0000"));

            // Quan colpegem, mai ho podem fer cap al terra
            if (this._lookingAtPointCorrectedPunch.y < 0)
            {
                this._lookingAtPointCorrectedPunch = new Vector3(this._controller.movementProps.lookingDirection, 0, 0);
            }

            //Debug.Log("After: " + this._lookingAtPointCorrectedPunch.ToString("0.0000"));

            //UnityEditor.EditorApplication.isPaused = true;
        }

        return _lookingAtPointCorrectedPunch;
    }
}
