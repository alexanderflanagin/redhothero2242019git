using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class KillNDie : MonoBehaviour
{
    /// <summary>
    /// Threshold al que toquem la part superior d'un enemic primer de cara i considerem que l'hem matat igual
    /// </summary>
    public float jumpIgnoreThreshold = 0.7f;

    public float punchIgnoreThreshold = 0.3f;

    /// <summary>
    /// Layer mask que ens indica quins elements ens maten
    /// </summary>
    public LayerMask killedByLayerMask;

    public Color jumpThresholdSquare;

    public Color punchThresholdSquare;

    /// <summary>
    /// El character control per a cridar el startDiying o el bouncing
    /// </summary>
    private CharacterControl _control;

	/// <summary>
    /// El controller fisic per a agafar les col·lisions i els objectes que les provoquen
	/// </summary>
	private Physics2DController _controller;

    /// <summary>
    /// Informacio de col.lisions del boxCollider
    /// </summary>
    private BoxColliderRayInfo boxColliderRayInfo;

    /// <summary>
    /// Informacio de col.lisions amb rayCast, per a no crear-les cada frame
    /// </summary>
    private RaycastHit2D[] hits;

    /// <summary>
    /// Darrer collider que ens ha tocat i ens ha fet mal
    /// </summary>
    private Collider2D lastHitCollider;

    /// <summary>
    /// Ultim frame que hem mirat si ignoravem un enemic, per a fer cache del resultat
    /// </summary>
    private int lastFrameJumpIgnoreChecked;

    /// <summary>
    /// Ultim resultat que hem obtingut al mirar si ignoravem un enemic al fer jump
    /// </summary>
    private bool lastFrameJumpIgnoreResult;

    private void Awake()
    {
        _controller = GetComponent<Physics2DController>();
        _control = GetComponent<CharacterControl>();
    }

    /// <summary>
    /// Al fer enable, s'espera al final del frame i fa l'enable
    /// </summary>
    void OnEnable()
    {
        if (boxColliderRayInfo == null)
        {
            boxColliderRayInfo = new BoxColliderRayInfo();
        }

        
        _controller.BlockedAboveEnter += OnBlockedUpEnter;
        _controller.BlockedBelowEnter += OnGroundedEnter;
        _controller.BlockedFrontEnter += OnBlockedFrontEnter;
        _controller.HorizontallyCrushed += OnCrushed;
        _controller.VerticallyCrushed += OnCrushed;

        //StartCoroutine(EnableWithDependences());

        boxColliderRayInfo.Initialize(GetComponent<BoxCollider2D>());

        hits = new RaycastHit2D[1];
    }

    /// <summary>
    /// Cridat per l'enable, espera al final del frame per a recollir el controller i aixi assegurar-se de que existeix
    /// </summary>
    /// <returns></returns>
    //private IEnumerator EnableWithDependences()
    //{
    //    yield return new WaitForEndOfFrame();

        
    //}

    /// <summary>
    /// Al deshabilitar el component, treiem els Events del registre
    /// </summary>
    void OnDisable()
    {
        if (_controller != null)
        {
            _controller.BlockedAboveEnter -= OnBlockedUpEnter;
            _controller.BlockedBelowEnter -= OnGroundedEnter;
            _controller.BlockedFrontEnter -= OnBlockedFrontEnter;
            _controller.HorizontallyCrushed -= OnCrushed;
            _controller.VerticallyCrushed -= OnCrushed;
        }
    }

    /// <summary>
    /// Al començar l'execucio, agafem el component del control i els dels triggers
    /// </summary>
 //   void Start() {
		
	//}

    /// <summary>
    /// Cada frame mirem si tenim alguna col.lisio interna
    /// </summary>
    void Update()
    {
        boxColliderRayInfo.UpdateRaycastOrigins();

        // Fem que els raycasts detectin elements encara que els creem dins
        bool queriesStartIncolliders = Physics2D.queriesStartInColliders;
        Physics2D.queriesStartInColliders = true;

        CheckInside();
        //CheckBeingCrushed();

        Physics2D.queriesStartInColliders = queriesStartIncolliders;
    }

    /// <summary>
    /// Mirem si tenim enemics dins de l'skin width del player, i si no ens havien tocat ja
    /// </summary>
    void CheckInside()
    {
        bool wasIgnored = false;
        Vector2 offset = Vector2.zero;
        float lenght = boxColliderRayInfo.raycastOrigins.topLeft.y - boxColliderRayInfo.raycastOrigins.bottomLeft.y;
        for (int i = 0; i < boxColliderRayInfo.verticalRayCount; i++)
        {
            offset = Vector2.right * (boxColliderRayInfo.verticalRaySpacing * i);
            //Debug.DrawRay(boxColliderRayInfo.raycastOrigins.bottomLeft + offset, Vector2.up * lenght, Color.yellow);
            if (Physics2D.RaycastNonAlloc(boxColliderRayInfo.raycastOrigins.bottomLeft + offset, Vector2.up, hits, lenght, killedByLayerMask) > 0)
            {
                // Mirem si és un enemic i rebem mal
                EnemyController enemy = hits[0].collider.GetComponent<EnemyController>();
                if (enemy != null && !enemy.harmless)
                {
                    if(ShouldKillThatBecauseOfJump(enemy))
                    {
                        JumpOnEnemy(enemy);
                    }
                    else if (ShouldIgnoreThatBecauseOfPowerGlove(enemy))
                    {
                        // Do nothing
                        Debug.Log("Ho estem ignorant per el powerglove");
                    }
                    else
                    {
                        // Algú ens ha tocat
                        if (hits[0].collider.gameObject.layer == Layers.Enemy)
                        {
                            // Guardem qui ha sigut el darrer en tocar-nos, per a que no ens torni a tocar el següent frame
                            lastHitCollider = hits[0].collider;
                            lastHitCollider.gameObject.layer = Layers.IgnoredEnemy;
                            _control.RecieveDamage(enemy.gameObject);
                        }
                        else if (hits[0].collider.gameObject.layer == Layers.FinalBoss)
                        {
                            // No cal guardar info perquè morim
                            _control.RecieveDamage(enemy.gameObject);
                        }
                        else
                        {
                            wasIgnored = true;
                        }

                        return;
                    }
                }
            }
        }

        if(!wasIgnored && lastHitCollider != null)
        {
            lastHitCollider.gameObject.layer = Layers.Enemy;
            lastHitCollider = null;
        }
    }

    /// <summary>
    /// L'assignem als Events de Crhus per a matar al player
    /// </summary>
    void OnCrushed()
    {
        _control.StartDying(true, false);
    }

    /// <summary>
    /// Al entrar una col·lisio superior, si es un enemic, mor el character
    /// </summary>
    void OnBlockedUpEnter()
    {
        if (_controller.collisions.aboveObject != null)
        {
            if (!Layers.inMask(killedByLayerMask, _controller.collisions.aboveObject.layer))
                return;

            EnemyController enemy = _controller.collisions.aboveObject.GetComponent<EnemyController>();
            if (enemy != null && !enemy.harmless)
            {
                _control.RecieveDamage(enemy.gameObject);
            }
        }
    }

    /// <summary>
    /// Al entrar una col·lisio frontal, si no l'ha registrat primer el ground, mirem si es enemic i matem al character.
    /// </summary>
    void OnBlockedFrontEnter()
    {
        GameObject frontObject = (_controller.collisions.faceDir == 1) ? _controller.collisions.rightObject : _controller.collisions.leftObject;
        if (frontObject)
        {
            if (!Layers.inMask(killedByLayerMask, frontObject.layer))
                return;

            EnemyController enemy = frontObject.GetComponent<EnemyController>();
            if (ShouldKillThatBecauseOfJump(enemy))
            {
                if (enemy != null)
                {
                    JumpOnEnemy(enemy);
                }
            }
            else if (ShouldIgnoreThatBecauseOfPowerGlove(enemy))
            {
                // Just ignore it
                Debug.Log("Estem ignorant a un enemic que ens hauria de matar perque ha entrat per davant");
            }
            else if (enemy != null && !enemy.harmless)
            {
                _control.RecieveDamage(enemy.gameObject);
            }
        }
    }

    /// <summary>
    /// Al entrar una col·lisio posterior, si no l'ha registrat primer el ground, mirem si es enemic.
    /// Si ho és i es mouen en la mateixa direcció, matem al character. Sino no perque considerem que nomes s'han tocat les "esquenes".
    /// </summary>
    void OnBlockedBackEnter()
    {
        GameObject backObject = (_controller.collisions.faceDir == -1) ? _controller.collisions.rightObject : _controller.collisions.leftObject;
        if (backObject != null)
        {
            if (!Layers.inMask(killedByLayerMask, backObject.layer))
                return;

            EnemyController enemy = backObject.GetComponent<EnemyController>();
            if (ShouldKillThatBecauseOfJump(enemy))
            {
                if (enemy != null)
                {
                    JumpOnEnemy(enemy);
                }
            }
            else if (enemy != null && !enemy.harmless)
            {
                if (enemy.GetComponent<Physics2DController>() && enemy.GetComponent<Physics2DController>().collisions.faceDir != -_controller.collisions.faceDir)
                {
                    _control.RecieveDamage(enemy.gameObject);
                }
            }
        }
    }

    /// <summary>
    /// Al detectar una col·lisio inferior, mirem si no l'ha registrat primer una col·lisio lateral i provem de matar l'enemic.
    /// Si no es "mortal", farem un bounce i el materem si es "killable". Si es "mortal" ens matara igualment.
    /// </summary>
    void OnGroundedEnter()
    {
        if (_controller.collisions.belowObject != null)
        {
            if (!Layers.inMask(killedByLayerMask, _controller.collisions.belowObject.layer))
                return;

            EnemyController enemy = _controller.collisions.belowObject.GetComponent<EnemyController>();
            if (enemy != null)
            {
                JumpOnEnemy(enemy);
            }
        }
    }

    private void JumpOnEnemy(EnemyController enemy)
    {
        enemy.JumpedOver();

        if (enemy.mortal)
        {
            _control.RecieveDamage(enemy.gameObject);
        }

        if (enemy.killOnTouch)
        {
            // Ja no farem el bounce perque morim
            return;
        }

        // TODO Quan tornem a tenir l'slug, ho habilitem
        _control.EnterBounce();
        //if (!enemy.GetComponent<Slug>())
        //{
        //    _control.EnterBounce();
        //}
    }

    /// <summary>
    /// Metode que ens diu si la distància entre la posició màxima d'un enemic i la nostre és menor que la d'un llindar
    /// </summary>
    /// <param name="enemy">Enemic amb qui volem comparar</param>
    /// <returns>Cert si és menor o igual al llindar, fals altrament</returns>
    public bool ShouldKillThatBecauseOfJump(EnemyController enemy)
    {
        if(Time.frameCount == lastFrameJumpIgnoreChecked)
        {
            return lastFrameJumpIgnoreResult;
        }

        Physics2DController physicsController = enemy.GetComponent<Physics2DController>();
        EnemyMove enemyMove = enemy.GetComponent<EnemyMove>();
        Collider2D enemyCollider = enemy.GetComponent<Collider2D>();
        PathElement pathElement = null;
        if (enemy.transform.parent != null)
        {
            pathElement = enemy.transform.parent.GetComponent<PathElement>();
        }

        float maxYEnemy = enemyCollider.bounds.max.y;
        float minYPlayer = _control.collider2D.bounds.min.y;

        Debug.DrawLine(new Vector3(enemy.transform.position.x - 1, maxYEnemy - jumpIgnoreThreshold), new Vector3(enemy.transform.position.x + 1, maxYEnemy - jumpIgnoreThreshold), Color.magenta, 2f);

        // 1: Mirem si ell es mou de pujada i jo no estic tocant el terra
        if (!_controller.collisions.below &&
            ((enemyMove && physicsController && physicsController.collisions.moveAmountOld.y > 0) || (pathElement && pathElement.movement.y > 0)))
        {
            float lastY = pathElement ? pathElement.lastPosition.y : enemyMove.lastPosition.y;

            DebugUtils.DrawRectangle((Vector2)_control.lastPosition + _control.collider2D.offset - _control.collider2D.size / 2, (Vector2)_control.lastPosition + _control.collider2D.offset + _control.collider2D.size / 2, Color.green, 2f);
            DebugUtils.DrawRectangle((Vector2)enemyMove.lastPosition + enemyCollider.offset - (Vector2)enemyCollider.bounds.extents, (Vector2)enemyMove.lastPosition + enemyCollider.offset + (Vector2)enemyCollider.bounds.extents, Color.blue, 2f);

            if (lastY + enemyCollider.offset.y + enemyCollider.bounds.extents.y < _control.lastPosition.y + _control.collider2D.offset.y - _control.collider2D.size.y / 2)
            {
                lastFrameJumpIgnoreChecked = Time.frameCount;
                lastFrameJumpIgnoreResult = true;
                return true;
            }
            else
            {
                lastFrameJumpIgnoreChecked = Time.frameCount;
                lastFrameJumpIgnoreResult = false;
                return false;
            }
        }

        lastFrameJumpIgnoreChecked = Time.frameCount;
        lastFrameJumpIgnoreResult = maxYEnemy - jumpIgnoreThreshold < minYPlayer;
        return lastFrameJumpIgnoreResult;
    }

    /// <summary>
    /// Mirem si hem de ignorar a un enemic que ens toca per davant perque estem carregant el PowerGlove
    /// </summary>
    /// <param name="enemy">L'enemic que mirem si hem d'ignorar</param>
    /// <returns></returns>
    private bool ShouldIgnoreThatBecauseOfPowerGlove(EnemyController enemy)
    {
        if(!_control.suit.powerGlove || !_control.suit.powerGlove.charging || enemy.mortal)
        {
            return false;
        }

        Collider2D enemyCollider = enemy.GetComponent<Collider2D>();

        // Tenim PowerGlove i esta carregant
        float playerX = _control.LookingDirection == 1 ? _control.collider2D.bounds.max.x : _control.collider2D.bounds.min.x;
        float enemyX = enemyCollider.bounds.center.x > _control.collider2D.bounds.center.x ? enemyCollider.bounds.min.x : enemyCollider.bounds.max.x;

        return Mathf.Abs(playerX - enemyX) < punchIgnoreThreshold;
    }

#if UNITY_EDITOR
    /// <summary>
    /// Quan sel.leccionem el Hero, mostrem per on van els quadres del threshold del puny i del jump
    /// </summary>
    private void OnDrawGizmosSelected()
    {
        Collider2D collider = GetComponent<Collider2D>();

        Vector2 bottomLeft = collider.bounds.min;
        Vector2 topRight = collider.bounds.max;
        Vector2 bottomRight = new Vector2(topRight.x, bottomLeft.y);
        Vector2 topLeft = new Vector2(bottomLeft.x, topRight.y);
        Vector2 jumpThreshold = new Vector2(0, jumpIgnoreThreshold);
        Vector2 punchThreshold = new Vector2(-punchIgnoreThreshold, 0);

        // Jump
        Gizmos.color = jumpThresholdSquare;
        Gizmos.DrawLine(topRight, bottomLeft + jumpThreshold);
        Gizmos.DrawLine(bottomLeft + jumpThreshold, bottomRight + jumpThreshold);
        Gizmos.DrawLine(topLeft, bottomRight + jumpThreshold);

        // Punch
        Gizmos.color = punchThresholdSquare;
        Gizmos.DrawLine(topRight + punchThreshold, bottomRight + punchThreshold);
        Gizmos.DrawLine(topRight + punchThreshold, bottomLeft);
        Gizmos.DrawLine(bottomRight + punchThreshold, topLeft);
    }
#endif
}
