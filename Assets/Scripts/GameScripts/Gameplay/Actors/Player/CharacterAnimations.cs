using UnityEngine;
using System.Collections;

public class CharacterAnimations : MonoBehaviour
{
    /// <summary>
    /// Animator del player
    /// </summary>
    private Animator _animator;

    /// <summary>
    /// Hash del Turn per al cap
    /// </summary>
    private int _headHash = Animator.StringToHash("Turn");

    /// <summary>
    /// Hash del dead (de any state al Idle).
    /// </summary>
    private int _deadHash = Animator.StringToHash("Dead");

    /// <summary>
    /// Hash per a la variable de la velocitat
    /// </summary>
    private int _speedHash = Animator.StringToHash("Speed");

    /// <summary>
    /// Hash per a la variable booleana de si està stunned
    /// </summary>
    private int _stunnedHash = Animator.StringToHash("Stunned");

    /// <summary>
    /// Hash per al float Vertical Speed
    /// </summary>
    private int _verticalSpeedHash = Animator.StringToHash("VerticalSpeed");

    /// <summary>
    /// Hash per al booleà Teleport
    /// </summary>
    private int _teleportHash = Animator.StringToHash("Teleport");

    /// <summary>
    /// Hash per al booleà Inertia
    /// </summary>
    private int _inertiaHash = Animator.StringToHash("Inertia");

    private int inertiaChangeDirectionHash = Animator.StringToHash("InertiaChangeDirection");

    /// <summary>
    /// Hash per a quan peguem amb el PowerGlove
    /// </summary>
    private int powerGloveCharge = Animator.StringToHash("PunchCharge");

    /// <summary>
    /// Hash del cancel·lar el girar el cap
    /// </summary>
    //private int _cancelTurn = Animator.StringToHash("CancelTurn");

    private int _slidingHash = Animator.StringToHash("Sliding");

    private int _jumpingHash = Animator.StringToHash("Jumping");

    private int _rollingHash = Animator.StringToHash("Rolling");

    private int _fallingHash = Animator.StringToHash("Falling");

    private int _jumpFallingHash = Animator.StringToHash("JumpFalling");

    private int _groundedHash = Animator.StringToHash("Grounded");

    private int _crouchHash = Animator.StringToHash("Crouch");

    private int _wallGrabHash = Animator.StringToHash("WallGrab");

    private int _triumpgHash = Animator.StringToHash("Triumph");

    private int _energy = Animator.StringToHash("Energy");

    private int _noEnergy = Animator.StringToHash("NoEnergy");

    private int wallJump = Animator.StringToHash("WallJump");

    private int brakeEnterHash = Animator.StringToHash("BrakeEnter");

    private int brakeHash = Animator.StringToHash("Brake");

    /// <summary>
    /// Control del objecte player
    /// </summary>
    private CharacterControl _control;

    /// <summary>
    /// Red Hot Density Teleport, per a saber quan llencem l'animació de teleport
    /// </summary>
    private RedHotDensityTeleport _RHDTeleport;

    private Quaternion originalRootNodeRotation;

    private void Awake()
    {
        _control = GetComponent<CharacterControl>();

        _RHDTeleport = GetComponent<RedHotDensityTeleport>();

        _animator = GetComponent<Animator>();
    }

    /// <summary>
    /// Al habilitar el component, agafem el CharacterControl i registrem els Events
    /// </summary>
    void OnEnable()
    {
        // Reset animacions (no se ni si cal)
        _animator.Rebind();

        // Inicialitzem tots els hash
        _animator.SetInteger(_energy, 100);
        
        //_animator.SetFloat(_speedHash, 0);
        //_animator.SetFloat(_verticalSpeedHash, 0);
        //_animator.SetBool(_deadHash, false);
        //_animator.SetBool(_headHash, false);
        //_animator.SetBool(_deadHash, false);
        //_animator.SetBool(_stunnedHash, false);
        //_animator.SetBool(_teleportHash, false);
        //_animator.SetBool(_inertiaHash, false);
        //_animator.SetBool(inertiaChangeDirectionHash, false);
        //_animator.SetBool(_slidingHash, false);
        //_animator.SetBool(_jumpingHash, false);
        //_animator.SetBool(_rollingHash, false);
        //_animator.SetBool(_fallingHash, false);
        //_animator.SetBool(_jumpFallingHash, false);
        //_animator.SetBool(_groundedHash, false);
        //_animator.SetBool(_crouchHash, false);
        //_animator.SetBool(_wallGrabHash, false);
        //_animator.SetBool(_triumpgHash, false);
        //_animator.SetBool(wallJump, false);
        //_animator.SetBool(powerGloveCharge, false);
        //_animator.ResetTrigger(_noEnergy);
        //_animator.ResetTrigger(_teleportHash);
        //_animator.ResetTrigger(brakeEnterHash);

        _control.events.registerEvent(CharacterEvents.EventName.IdleEnter,OnIdleEnter);
        _control.events.registerEvent(CharacterEvents.EventName.InertiaEnter,OnInertiaEnter);
        _control.events.registerEvent(CharacterEvents.EventName.InertiaExit, OnInertiaExit);
        _control.events.registerEvent(CharacterEvents.EventName.InertiaChangingDirection, OnInertiaChangingDirection);
        _control.events.registerEvent(CharacterEvents.EventName.RunEnter,OnRunEnter);
        _control.events.registerEvent(CharacterEvents.EventName.RollEnter,OnRollEnter);
        _control.events.registerEvent(CharacterEvents.EventName.SlideEnter,OnSlideEnter);
        _control.events.registerEvent(CharacterEvents.EventName.GroundedEnter,OnGroundedEnter);
        _control.events.registerEvent(CharacterEvents.EventName.BounceEnter,OnBounceEnter);
        _control.events.registerEvent(CharacterEvents.EventName.CrouchEnter,OnCrouchEnter);
        _control.events.registerEvent(CharacterEvents.EventName.JumpFallEnter,OnJumpFallEnter);
        _control.events.registerEvent(CharacterEvents.EventName.FallEnter,OnFallEnter);
        _control.events.registerEvent(CharacterEvents.EventName.StunnedEnter,OnStunnedEnter);
        _control.events.registerEvent(CharacterEvents.EventName.StunnedExit,OnStunnedExit);
        _control.events.registerEvent(CharacterEvents.EventName.JumpEnter,OnJumpEnter);
        _control.events.registerEvent(CharacterEvents.EventName.TravelEnter,OnTeleportEnter);
        _control.events.registerEvent(CharacterEvents.EventName.TravelExit,OnTeleportExit);
        _control.events.registerEvent(CharacterEvents.EventName.WallGrabEnter,OnWallGrabEnter);
        _control.events.registerEvent(CharacterEvents.EventName.WallGrabExit,OnWallGrabExit);
        _control.events.registerEvent(CharacterEvents.EventName.DieByNoEnergy, OnNoEnergy);
        _control.events.registerEvent(CharacterEvents.EventName.WallJumpEnter, OnWallJumpEnter);
        _control.events.registerEvent(CharacterEvents.EventName.WallJumpExit, OnWallJumpExit);
        _control.events.registerEvent(CharacterEvents.EventName.PowerGloveCharge, OnPowerGloveCharge);
        _control.events.registerEvent(CharacterEvents.EventName.PowerGloveReleased, OnPowerGloveReleased);
        _control.events.registerEvent(CharacterEvents.EventName.PowerGloveCancelled, OnPowerGloveReleased);
        _control.events.registerEvent(CharacterEvents.EventName.BrakeEnter, OnBrakeEnter);
        _control.events.registerEvent(CharacterEvents.EventName.BrakeExit, OnBrakeExit);

        //StartCoroutine(CheckHeadTurn());
    }

	/// <summary>
    /// Al deshabilitar-lo, des-registrem els Events
	/// </summary>
	void OnDisable()
    {
        _control.events.unregisterEvent(CharacterEvents.EventName.IdleEnter,OnIdleEnter);
        _control.events.unregisterEvent(CharacterEvents.EventName.InertiaEnter, OnInertiaEnter);
        _control.events.unregisterEvent(CharacterEvents.EventName.InertiaExit, OnInertiaExit);
        _control.events.unregisterEvent(CharacterEvents.EventName.InertiaChangingDirection, OnInertiaChangingDirection);
        _control.events.unregisterEvent(CharacterEvents.EventName.RunEnter, OnRunEnter);
        _control.events.unregisterEvent(CharacterEvents.EventName.RollEnter, OnRollEnter);
        _control.events.unregisterEvent(CharacterEvents.EventName.SlideEnter, OnSlideEnter);
        _control.events.unregisterEvent(CharacterEvents.EventName.GroundedEnter, OnGroundedEnter);
        _control.events.unregisterEvent(CharacterEvents.EventName.BounceEnter, OnBounceEnter);
        _control.events.unregisterEvent(CharacterEvents.EventName.CrouchEnter, OnCrouchEnter);
        _control.events.unregisterEvent(CharacterEvents.EventName.JumpFallEnter, OnJumpFallEnter);
        _control.events.unregisterEvent(CharacterEvents.EventName.FallEnter, OnFallEnter);
        _control.events.unregisterEvent(CharacterEvents.EventName.StunnedEnter, OnStunnedEnter);
        _control.events.unregisterEvent(CharacterEvents.EventName.StunnedExit, OnStunnedExit);
        _control.events.unregisterEvent(CharacterEvents.EventName.JumpEnter, OnJumpEnter);
        _control.events.unregisterEvent(CharacterEvents.EventName.TravelEnter, OnTeleportEnter);
        _control.events.unregisterEvent(CharacterEvents.EventName.TravelExit, OnTeleportExit);
        _control.events.unregisterEvent(CharacterEvents.EventName.WallGrabEnter, OnWallGrabEnter);
        _control.events.unregisterEvent(CharacterEvents.EventName.WallGrabExit, OnWallGrabExit);
        _control.events.unregisterEvent(CharacterEvents.EventName.DieByNoEnergy, OnNoEnergy);
        _control.events.unregisterEvent(CharacterEvents.EventName.WallJumpEnter, OnWallJumpEnter);
        _control.events.unregisterEvent(CharacterEvents.EventName.WallJumpExit, OnWallJumpExit);
        _control.events.unregisterEvent(CharacterEvents.EventName.PowerGloveCharge, OnPowerGloveCharge);
        _control.events.unregisterEvent(CharacterEvents.EventName.PowerGloveReleased, OnPowerGloveReleased);
        _control.events.unregisterEvent(CharacterEvents.EventName.PowerGloveCancelled, OnPowerGloveReleased);
        _control.events.unregisterEvent(CharacterEvents.EventName.BrakeEnter, OnBrakeEnter);
        _control.events.unregisterEvent(CharacterEvents.EventName.BrakeExit, OnBrakeExit);
    }

    /// <summary>
    /// Si no estem pausats, actualitzem la velocitat
    /// </summary>
    private void Update()
    {
        if (!_control.pauseProperties.paused)
        {
            _animator.SetFloat(_speedHash, _control.movementProps.speed);
        }
    }

    void OnIdleEnter()
    {
        _animator.SetBool(_rollingHash, false);
        _animator.SetBool(_slidingHash, false);
        _animator.SetBool(_crouchHash, false);
        _animator.SetBool(_inertiaHash, false);
    }

    void OnInertiaEnter()
    {
        _animator.SetBool(_inertiaHash, true);
    }

    void OnInertiaExit()
    {
        _animator.SetBool(_inertiaHash, false);
        _animator.SetBool(inertiaChangeDirectionHash, false); // Per si l'havíem posat a true
    }

    void OnInertiaChangingDirection()
    {
        _animator.SetBool(inertiaChangeDirectionHash, true);
    }

    void OnRollEnter()
    {
        _animator.SetBool(_rollingHash, true);
    }

    void OnRunEnter()
    {
        _animator.SetBool(_rollingHash, false);
        _animator.SetBool(_slidingHash, false);
        _animator.SetBool(_inertiaHash, false);
    }

    /// <summary>
    /// Al entrar Slide
    /// </summary>
    void OnSlideEnter()
    {
        _animator.SetBool(_slidingHash, true);
    }

    void OnGroundedEnter()
    {
        _animator.SetBool(_groundedHash, true);
        _animator.SetBool(_fallingHash, false);
    }

    void OnFallEnter()
    {
        _animator.SetBool(_groundedHash, false);
        _animator.SetBool(_wallGrabHash, false);
        _animator.SetBool(_fallingHash, true);
    }

    void OnBounceEnter()
    {
        _animator.SetBool(_groundedHash, false);
    }

    void OnCrouchEnter()
    {
        _animator.SetBool(_crouchHash, true);
    }

    void OnJumpEnter()
    {
        _animator.SetFloat(_verticalSpeedHash, 0);
        _animator.SetBool(_groundedHash, false);

        StartCoroutine(jumpHeights());
    }

    private IEnumerator jumpHeights()
    {
        bool twoReached = false, fourReached = false, fiveReached = false;
        
        // Alçada a la que hem començat a saltar, per a saber quan ens hem mogut
        float startY = this.transform.position.y;

        while (this._control.movementFSM.currentVertical == PlayerState.Vertical.Jump)
        {
            if (!twoReached && this.transform.position.y - startY >= 2)
            {
                twoReached = true;
                // Avisem al controller
                this._animator.SetFloat(this._verticalSpeedHash, 2);
            }

            if (!fourReached && this.transform.position.y - startY >= 4)
            {
                fourReached = true;
                // Avisem al controller
                this._animator.SetFloat(this._verticalSpeedHash, 4);
            }

            if (!fiveReached && this.transform.position.y - startY >= 5)
            {
                fiveReached = true;
                // Avisem al controller
                this._animator.SetFloat(this._verticalSpeedHash, 5);
            }

            yield return null;
        }
    }

    void OnJumpFallEnter()
    {
        _animator.SetBool(_jumpingHash, false);
        this._animator.SetBool(this._fallingHash, true);
    }

    void OnStunnedEnter()
    {
        _animator.SetBool(_stunnedHash, true);
    }

    void OnStunnedExit()
    {
        _animator.SetBool(_stunnedHash, false);
    }

    void OnWallGrabEnter()
    {
        _animator.SetBool(_wallGrabHash, true);
    }

    void OnWallGrabExit()
    {
        _animator.SetBool(_wallGrabHash, false);
    }

    /// <summary>
    /// Al començar un teleport, booleà de Teleport a true
    /// </summary>
    void OnTeleportEnter()
    {
        _animator.SetBool(_groundedHash, false);
    }

    /// <summary>
    /// Al acabar un teleport, booleà de Teleport a false
    /// </summary>
    void OnTeleportExit()
    {
        _animator.SetTrigger(_teleportHash);
    }

    void OnNoEnergy()
    {
        _animator.SetInteger(_energy, 0);
        _animator.SetTrigger(_noEnergy);
    }

    void OnWallJumpEnter()
    {
        this._animator.SetBool(this.wallJump, true);
        this._animator.SetBool(this._fallingHash, false);
    }

    void OnWallJumpExit()
    {
        this._animator.SetBool(this.wallJump, false);
    }

    /// <summary>
    /// Quan comencem a carregar el PowerGlove
    /// </summary>
    void OnPowerGloveCharge()
    {
        _animator.SetBool(powerGloveCharge, true);
    }

    /// <summary>
    /// Quan deixem anar el PowerGlove
    /// </summary>
    void OnPowerGloveReleased()
    {
        _animator.SetBool(powerGloveCharge, false);
    }

    /// <summary>
    /// Va mirant si ha de fer girar el cap del Player segons la distància amb la lava
    /// </summary>
    /// <returns></returns>
    //private IEnumerator CheckHeadTurn()
    //{
    //    float margin = 18f;

    //    GameObject lavaWall = GameObject.FindGameObjectWithTag("LavaWall");

    //    if (lavaWall == null)
    //    {
    //        yield break;
    //    }

    //    CharacterInput _input = GetComponent<CharacterInput>();

    //    while (true)
    //    {
    //        if (_input.sprint.val == 0 && transform.position.x - lavaWall.transform.position.x < margin)
    //        {
    //            _animator.SetBool(_headHash, true);
    //        }
    //        else
    //        {
    //            _animator.SetBool(_headHash, false);
    //        }

    //        yield return new WaitForEndOfFrame();
    //    }
    //}

    /// <summary>
    /// Mètode públic per a començar l'animació de la Coin Collected
    /// </summary>
    public void CoinCollected()
    {
        StartCoroutine("CelebrateCoinCollected");
    }

    /// <summary>
    /// Fa l'animació de celebració, espera 0.2 segons i va al locomotion blend o al vertical locomotion blend segons si està grounded o no
    /// </summary>
    /// <returns></returns>
    private IEnumerator CelebrateCoinCollected()
    {
        // Li diem a l'animator que es tot chachi piruli!
        _animator.SetBool(_triumpgHash, true);

        yield return new WaitForSeconds(0.3f);

        // Li diem a l'animator que tot es una merda again i volem abandonar aquesta vida cruel
        _animator.SetBool(_triumpgHash, false);

    }

    private void OnBrakeEnter()
    {
        _animator.SetTrigger(brakeEnterHash);
        _animator.SetBool(brakeHash, true);
    }

    private void OnBrakeExit()
    {
        _animator.SetBool(brakeHash, false);
    }
}
