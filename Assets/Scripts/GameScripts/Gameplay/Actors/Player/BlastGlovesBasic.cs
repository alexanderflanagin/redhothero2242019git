using UnityEngine;
using System.Collections;

/// <summary>
/// Classe que gestiona el dispar bàsic del Red Hot Hero
/// </summary>
public class BlastGlovesBasic : BlastGloves
{
    /// <summary>
    /// El controller del Player
    /// </summary>
    private CharacterControl _controller;

    
    /// <summary>
    /// El transform de cada un dels ossos que es mouen de manera manual per a simular l'apuntar
    /// </summary>
    private Transform _armRight, _body, _neck, _armLeft, _hand, _head;

    /// <summary>
    /// El Animator del Player
    /// </summary>
    private Animator _animator;

    /// <summary>
    /// El sprite de la fletxa d'apuntar.
    /// </summary>
    public Sprite aimArrow
    {
        set
        {
            if (_aim != null)
            {
                _aim.AddComponent<SpriteRenderer>().sprite = value;
            }
        }
    }

    /// <summary>
    /// El gameobject que conté l'sprite de la fletxa d'auntar <see cref="aimArrow"/>
    /// </summary>
    private GameObject _aim;

    /// <summary>
    /// Al fer awake creem l'objete <see cref="_aim"/>, recuperem el controller i l'animator, i busquem tots els ossos necessaris per a simular l'apuntar
    /// </summary>
    void Awake()
    {
        // aimArrow = Instantiate(aimArrow) as Sprite;
        _aim = new GameObject("Aim");
        _aim.transform.parent = transform;
        _aim.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
        _aim.SetActive(false);

        _controller = GetComponent<CharacterControl>();

        _armRight = transform.Find("Mesh/rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_R/ORG-upper_arm_R/");
        _body = transform.Find("Mesh/rig/root/ORG-hips/ORG-spine/ORG-chest");
        _neck = transform.Find("Mesh/rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-neck");
        _armLeft = transform.Find("Mesh/rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_L/ORG-upper_arm_L/");
        _hand = transform.Find("Mesh/rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_R/ORG-upper_arm_R/ORG-forearm_R/ORG-hand_R");

        _animator = GetComponent<Animator>();
    }

    /// <summary>
    /// Al fer enable, registrme l'esdeveniment que posarà instàncies dels projectils al GameObjectPooler
    /// </summary>
    void OnEnable()
    {
        // Registrem un esdeveniment al EnemiesManager per a que ens avisi quan puguem cridar a la creació de llistes
        ProjectilesManager.Instance.ExecuteOnListReady(OnListReady);
    }

    /// <summary>
    /// Quan la llista del GameObjectPooler està llesta, li diem quins projectils necessitarem
    /// </summary>
    void OnListReady()
    {
        GameObjectPooler.Instance.AddProjectiles(ProjectilesManager.Projectiles.BlastGlovesBasic, 20);
    }

    /// <summary>
    /// Mètode public que crida la coroutina del disparar
    /// </summary>
    public override void Shoot()
    {
		StartCoroutine(ShootCoroutine());
	}
	
	/// <summary>
	/// Activem la fletxa, esperem que es deixi anar el botó de disparar i després llancem el projectil
    /// </summary>
    /// <returns></returns>
    private IEnumerator ShootCoroutine()
    {
        _aim.SetActive(true);
        while (_controller.input.shoot.check)
        {
            _aim.transform.position = _hand.transform.position;
            _aim.transform.right = _controller.playerAim.getLookingAtPoint();
            yield return null;
        }
        _aim.SetActive(false);

        if (this._controller.currentAlive == CharacterControl.StateAlive.Dead)
        {
            // Si morim amb el dispar carregat
            yield break;
        }

        GameObject projectile = GameObjectPooler.Instance.GetProjectile(ProjectilesManager.Projectiles.BlastGlovesBasic);
        projectile.transform.position = _hand.transform.position;

        projectile.transform.forward = _controller.playerAim.getLookingAtPointCorrectedShoot();

        BlastGlovesBasicProjectile blastGlovesBasicProjectile = projectile.GetComponent<BlastGlovesBasicProjectile>();
        blastGlovesBasicProjectile.target = _controller.playerAim.target;
        blastGlovesBasicProjectile.owner = gameObject;

        projectile.SetActive(true);

        _controller.events.triggerEvent(CharacterEvents.EventName.Shoot);
    }

    /// <summary>
    /// En el LateUpdate, rotem els ossos necessàris per a que simuli l'apuntar
    /// </summary>
    void LateUpdate()
    {
        if (_controller.movementFSM.currentHorizontal != PlayerState.Horizontal.Crouch && _controller.movementFSM.currentHorizontal != PlayerState.Horizontal.Roll)
        {
            if (_controller.input.shoot.down)
            {
                _animator.SetBool("Aim", true);
            }

            if (_controller.input.shoot.down || _controller.input.shoot.check)
            {
                _body.eulerAngles = new Vector3(0, 90 * _controller.LookingDirection, -90);

                _armRight.right = -_controller.playerAim.getLookingAtPoint();
                if (Mathf.Approximately(_controller.playerAim.getLookingAtPoint().y, 0) || _controller.LookingDirection < 0)
                {
                    _armRight.rotation *= Quaternion.Euler(180, 0, 0);
                }

                _neck.right = _controller.playerAim.getLookingAtPoint();
                if(_controller.LookingDirection < 0 && !Mathf.Approximately(_controller.playerAim.getLookingAtPoint().y, 0))
                {
                    _neck.rotation *= Quaternion.Euler(-40, 90, 90);
                }
                else
                {
                    _neck.rotation *= Quaternion.Euler(40, 90, -90);
                }
            }
        }
        if (_controller.input.shoot.up)
        {
            _animator.SetBool("Aim", false);
        }
    }
}
