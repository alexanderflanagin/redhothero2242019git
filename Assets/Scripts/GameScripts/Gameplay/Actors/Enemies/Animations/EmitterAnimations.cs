using UnityEngine;

public class EmitterAnimations : EnemyAnimations
{
    /// <summary>
    /// Shoot patern al que registrarem els Events per a les animcions
    /// </summary>
    private ShootPattern shootPattern;

    /// <summary>
    /// Hash de l'animacio de morir
    /// </summary>
    private int dieHash = Animator.StringToHash("Die");

    /// <summary>
    /// Hash de l'animacio de disparar
    /// </summary>
    private int shootHash = Animator.StringToHash("Shoot");

    /// <summary>
    /// Agafem el shoot patern
    /// </summary>
    protected override void Awake()
    {
        base.Awake();

        this.shootPattern = this.GetComponentInChildren<ShootPattern>();
    }

    /// <summary>
    /// Registrem les accions per a disparar i morir
    /// </summary>
    void OnEnable()
    {
        animator.SetBool(dieHash, false);

        controller.events.registerEvent(EnemyEvents.EventName.Die, OnDie);

        this.shootPattern.preShoot += OnShoot;
        this.shootPattern.postShoot += OnEndShoot;
        shootPattern.forceEndShoot += OnEndShoot;
    }

    /// <summary>
    /// Des-registrem les accions per a disparar i morir
    /// </summary>
    void OnDisable()
    {
        controller.events.unregisterEvent(EnemyEvents.EventName.Die, OnDie);

        this.shootPattern.preShoot -= OnShoot;
        this.shootPattern.postShoot -= OnEndShoot;
        shootPattern.forceEndShoot -= OnEndShoot;
    }

    /// <summary>
    /// Animacio de morir
    /// </summary>
    /// <param name="enemy"></param>
    void OnDie(GameObject enemy)
    {
        animator.SetBool(dieHash, true);
    }

    /// <summary>
    /// Arrenco animacio de disparar
    /// </summary>
    void OnShoot()
    {
        animator.SetBool(this.shootHash, true);
    }

    /// <summary>
    /// Finalitzo animacio de disparar
    /// </summary>
    void OnEndShoot()
    {
        animator.SetBool(this.shootHash, false);
    }
}
