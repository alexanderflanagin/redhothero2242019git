using UnityEngine;
using System.Collections;

public class SquakatooAnimations : EnemyAnimations
{
    private int _deadHash = Animator.StringToHash("Dead");

    // Use this for initialization
    void OnEnable()
    {
        controller = GetComponent<EnemyController>();

        animator = GetComponent<Animator>();

        animator.SetBool(_deadHash, false);

        //Debug.Log("Controller: " + _controller + " events: " + _controller.events, this.gameObject);

        controller.events.registerEvent(EnemyEvents.EventName.Die, OnDie);
    }

    void OnDisable()
    {
        // _animator.SetBool(_deadHash, false);
        controller.events.unregisterEvent(EnemyEvents.EventName.Die, OnDie);
    }

    void OnDie(GameObject enemy)
    {
        //Debug.Log("On Die cridat", this.gameObject);
        animator.SetBool(_deadHash, true);
    }
}
