using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinalBossAnimations : MonoBehaviour
{
    public static int hashDamagedTrigger = Animator.StringToHash("Damaged");

    public static int hashKilledTrigger = Animator.StringToHash("Killed");

    public Animator animator;

    public FinalBossController controller;

    protected EnemyState.State lastState;

    private void Update()
    {
        if(controller.state != lastState)
        {
            if(controller.state == EnemyState.State.Damaged)
            {
                animator.SetTrigger(hashDamagedTrigger);
            }
            else if (controller.state == EnemyState.State.Dead)
            {
                animator.SetTrigger(hashKilledTrigger);
            }
        }

        lastState = controller.state;
    }
}
