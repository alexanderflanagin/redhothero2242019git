using System.Collections;
using UnityEngine;

/// <summary>
/// Class used to animate textures
/// </summary>
public class AnimatedTexture : MonoBehaviour
{
    /// <summary>
    /// Should start when the object is enabled?
    /// </summary>
    public bool autoStart = true;

    /// <summary>
    /// Should stop after all frames are showed? And remain in the last frame
    /// </summary>
    public bool oneShot = false;

    /// <summary>
    /// Tilesheet columns
    /// </summary>
    public int columns = 2;

    /// <summary>
    /// Tilesheet rows
    /// </summary>
    public int rows = 2;

    /// <summary>
    /// Animation frames per second
    /// </summary>
    public float framesPerSecond = 10f;

    /// <summary>
    /// Renderer who should change texture
    /// </summary>
    public new Renderer renderer;

    /// <summary>
    /// Tilesheet to animate
    /// </summary>
    public Texture texture;

    /// <summary>
    /// The current frame to display
    /// </summary>
    private int index = 0;

    private void Awake()
    {
        //set the tile size of the texture (in UV units), based on the rows and columns
        Vector2 size = new Vector2(1f / columns, 1f / rows);
        renderer.material.SetTexture("_MainTex", texture);
        renderer.material.SetTexture("_EmissionMap", texture);
        renderer.material.SetTextureScale("_MainTex", size);
    }

    void OnEnable()
    {
        if (autoStart)
        {
            StartCoroutine(UpdateTiling());
        }
        else
        {
            // Posem la primera textura
            Vector2 offset = new Vector2(0, (float)(rows - 1) / rows);
            renderer.material.SetTextureOffset("_MainTex", offset);
        }
    }

    public IEnumerator UpdateTiling()
    {
        float x = 0f;
        float y = 0f;
        Vector2 offset = Vector2.zero;

        // If it isn't autoStart, it means that we are showing frame 0 (top-left) and when we start we want to show frame 1 at the first frame
        bool ignoreFirst = !autoStart;

        while (true)
        {
            for (int i = rows - 1; i >= 0; i--) // y
            {
                y = (float)i / rows;

                for (int j = 0; j <= columns - 1; j++) // x
                {
                    if (ignoreFirst)
                    {
                        // Al no fer auto-start, considerem que quan s'activa ens volem saltar el primer frame que era el que mostravem fins ara
                        j++;
                        ignoreFirst = false;
                    }

                    x = (float)j / columns;

                    offset.Set(x, y);

                    renderer.material.SetTextureOffset("_MainTex", offset);
                    yield return new WaitForSeconds(1f / framesPerSecond);
                }
            }

            if(oneShot)
            {
                yield break;
            }
        }

    }
}
