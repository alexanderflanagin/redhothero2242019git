using UnityEngine;
using System.Collections;

public class StvAnimations : EnemyAnimations
{
    private int _deadHash = Animator.StringToHash("Dead");

    // Use this for initialization
    void OnEnable()
    {
        animator.SetBool(_deadHash, false);

        controller.events.registerEvent(EnemyEvents.EventName.Die, OnDie);
    }

    void OnDisable()
    {
        // _animator.SetBool(_deadHash, false);
        controller.events.unregisterEvent(EnemyEvents.EventName.Die, OnDie);
    }

    void OnDie(GameObject enemy)
    {
        //Debug.Log("On Die cridat", this.gameObject);
        animator.SetBool(_deadHash, true);
    }
}
