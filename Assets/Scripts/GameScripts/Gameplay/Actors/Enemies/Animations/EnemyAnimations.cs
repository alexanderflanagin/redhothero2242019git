using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(EnemyController))]
public abstract class EnemyAnimations : MonoBehaviour
{
    /// <summary>
    /// Guardarem instancia de l'animator de l'enemic
    /// </summary>
    protected Animator animator;

    /// <summary>
    /// Guardarem instancia del controller de l'enemic
    /// </summary>
    protected EnemyController controller;

    protected virtual void Awake()
    {
        this.animator = this.GetComponent<Animator>();

        this.controller = this.GetComponent<EnemyController>();
    }
}
