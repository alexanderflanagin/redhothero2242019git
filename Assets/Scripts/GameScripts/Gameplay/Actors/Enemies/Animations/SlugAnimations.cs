using UnityEngine;
using System.Collections;

/// <summary>
/// Dispara les animacions del Slug
/// <remarks>
/// BlockedRight: Es dispara quan l'Slug comença a rotar (segons el EnemyMove) sigui per contacte o per vertigen (des de la dreta).
/// BlockedLeft:  Es dispara quan l'Slug comença a rotar (segons el EnemyMove) sigui per contacte o per vertigen (des de l'esquerra).
/// StepLeft: Es dispara cada cop que l'Slug agafa impuls i la seva direcció és negativa
/// StepRight: Es dispara cada cop que l'Slug agafa impuls i la seva direcció és positiva
/// </remarks>
/// </summary>
public class SlugAnimations : EnemyAnimations
{
    /// <summary>
    /// El EnemyMove (en aquest cas un Slug)
    /// </summary>
    private Slug _movement;

    /// <summary>
    /// Hash de la variable [BlockedRight] del Animator
    /// </summary>
    private int _blockedRight = Animator.StringToHash("BlockedRight");

    /// <summary>
    /// Hash de la variable [BlockedLeft] del Animator
    /// </summary>
    private int _blockedLeft = Animator.StringToHash("BlockedLeft");

    /// <summary>
    /// Hash de la variable [StepLeft] del Animator
    /// </summary>
    private int _stepLeft = Animator.StringToHash("StepLeft");

    /// <summary>
    /// Hash de la variable [StepLeft] del Animator
    /// </summary>
    private int _stepRight = Animator.StringToHash("StepRight");

    /// <summary>
    /// Al habilitar el complement, recollim l'animator, el controller i el EnemyMove i iniciem els Events
    /// </summary>
    void OnEnable()
    {
        animator = GetComponent<Animator>();

        _movement = GetComponent<Slug>();
        _movement.StepLeft += OnStepLeft;
        _movement.StepRight += OnStepRight;
        _movement.TurnEvent += OnBlockedEnter;
    }

    /// <summary>
    /// Al fer disable, des-registrem els Events
    /// </summary>
    void OnDisable()
    {
        _movement.StepLeft -= OnStepLeft;
        _movement.StepRight -= OnStepRight;
        _movement.TurnEvent -= OnBlockedEnter;
    }

    /// <summary>
    /// Es dispara al fer agafar impuls anant cap a l'esquerra. Dispara el trigger de l'animator StepLeft
    /// </summary>
    void OnStepLeft()
    {
        animator.SetTrigger(_stepLeft);
    }

    /// <summary>
    /// Es dispara al fer agafar impuls anant cap a la dreta. Dispara el trigger de l'animator StepRight
    /// </summary>
    void OnStepRight()
    {
        animator.SetTrigger(_stepRight);
    }

    /// <summary>
    /// Es dispara al anar a rotar. Si la direcció és positiva dispara el BlockedRight, sinó el BlockedLeft
    /// </summary>
    void OnBlockedEnter()
    {
        //if (_movement.direction == 1)
        //{
        //    _animator.SetTrigger(_blockedRight);
        //}
        //else
        //{
        //    _animator.SetTrigger(_blockedLeft);
        //}
    }
}
