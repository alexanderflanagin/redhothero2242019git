using UnityEngine;
using System.Collections;

public class DocClockAnimations : EnemyAnimations
{
    /// <summary>
    /// Hash per quan ens toquen (salten a sobre o ens disparen)
    /// </summary>
    private int touchedHash = Animator.StringToHash("TouchedTrigger");

    /// <summary>
    /// Hash per quan hem de morir (ara en realitat nomes tornem a l'estat inicial)
    /// </summary>
    private int dieHash = Animator.StringToHash("Dead");

    /// <summary>
    /// Registrem els Events que s'han de llancar
    /// </summary>
    void OnEnable()
    {
        controller.events.registerEvent(EnemyEvents.EventName.JumpedOver, OnTouched);
        controller.events.registerEvent(EnemyEvents.EventName.TouchedByProjectile, OnTouched);

        controller.events.registerEvent(EnemyEvents.EventName.Die, OnDie);
    }

    /// <summary>
    /// Des-registrem els Events que s'han de llancar
    /// </summary>
    void OnDisable()
    {
        controller.events.unregisterEvent(EnemyEvents.EventName.JumpedOver, OnTouched);
        controller.events.unregisterEvent(EnemyEvents.EventName.TouchedByProjectile, OnTouched);

        controller.events.unregisterEvent(EnemyEvents.EventName.Die, OnDie);
    }

    /// <summary>
    /// Al ser tocat (sigui per salt o per dispar)
    /// </summary>
    /// <param name="enemy"></param>
    void OnTouched(GameObject enemy)
    {
        animator.SetTrigger(touchedHash);
    }

    /// <summary>
    /// Al morir (immediat o despres d'haver sigut tocat)
    /// </summary>
    /// <param name="enemy"></param>
    void OnDie(GameObject enemy)
    {
        animator.SetTrigger(dieHash);
    }
}
