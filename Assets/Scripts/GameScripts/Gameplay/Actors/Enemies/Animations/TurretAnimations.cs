using UnityEngine;

public class TurretAnimations : EnemyAnimations
{
    /// <summary>
    /// El shooter de la torreta
    /// </summary>
    protected Shooter shooter;

    /// <summary>
    /// Hash del parametre BeakOpen del animator
    /// </summary>
    protected int hashBeakOpenParam = Animator.StringToHash("BeakOpen");

    /// <summary>
    /// Fem el Awake de la base i recuperem el shooter
    /// </summary>
    protected override void Awake()
    {
        base.Awake();

        this.shooter = this.GetComponent<Shooter>();
    }

    /// <summary>
    /// Registrem Events del shooter abans, despres si ens cancel.len el disparar
    /// </summary>
    void OnEnable()
    {
        foreach (ShootPattern pattern in this.shooter.shootPattern)
        {
            pattern.preShoot += OnPreShoot;
            pattern.postShoot += OnPostShoot;
            pattern.forceEndShoot += OnPostShoot;
        }
    }

    /// <summary>
    /// Des-registrem Events del shooter abans, despres si ens cancel.len el disparar
    /// </summary>
    void OnDisable()
    {
        foreach (ShootPattern pattern in this.shooter.shootPattern)
        {
            pattern.preShoot -= OnPreShoot;
            pattern.postShoot -= OnPostShoot;
            pattern.forceEndShoot -= OnPostShoot;
        }
    }

    /// <summary>
    /// Li diem al animator que obri el beak
    /// </summary>
    protected void OnPreShoot()
    {
        this.animator.SetBool(hashBeakOpenParam, true);
    }

    /// <summary>
    /// Li diem al animator que tanqui el beak
    /// </summary>
    protected void OnPostShoot()
    {
        if(animator.isInitialized)
        {
            animator.SetBool(hashBeakOpenParam, false);
        }
    }
}
