﻿using UnityEngine;
using System;
using System.Collections;

/// <summary>
/// Classe que fem servir per a poder tenir meshes en fills i que aquests disparin el becamevisible i inivisible enlloc de les funcions de Unity
/// </summary>
public class RendererVisibleEvent : MonoBehaviour {

    public event Action visible;

    public event Action invisible;

    void OnBecameVisible()
    {
        if (visible != null)
        {
            visible();
        }
    }

    void OnBecameInvisible()
    {
        if (invisible != null)
        {
            invisible();
        }
    }
}
