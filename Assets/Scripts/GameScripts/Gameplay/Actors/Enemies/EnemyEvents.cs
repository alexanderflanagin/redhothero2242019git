using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class EnemyEvents {

    public delegate void EnemyEvent(GameObject sender);

    public enum EventName
    {
        Die,
        Disable,
        Explode,
        Fall,
        Ground,
        Move,
        Rotate,
        SlideEnd,
        SlideStart,
        Reborn,
        PowerGloved,
        PowerGlovedCrashed,
        PowerGlovedLost,
        EnableCannon,
        DisableCannon,
        Hide,
        JumpedOver,
        TouchedByProjectile
    }

    public Dictionary<EventName, EnemyEvent> events = new Dictionary<EventName, EnemyEvent>();

    public GameObject myGameObject;

    public void triggerEvent(EventName name)
    {
        //Debug.Log("Triggering event: " + name);
        if (events.ContainsKey(name) && events[name] != null)
        {
            //Debug.Log("Has " + name + " gonna execute.");
            events[name](myGameObject);
        }
    }

    public void registerEvent(EventName name, EnemyEvent action)
    {
        if (events.ContainsKey(name))
        {
            //Debug.Log("Add event " + name + " for NOT the first time");
            events[name] += action;
        }
        else
        {
            //Debug.Log("Add event " + name + " for the first time");
            events.Add(name, action);
        }
    }

    public void unregisterEvent(EventName name, EnemyEvent action)
    {
        if (events.ContainsKey(name))
        {
            events[name] -= action;
        }
    }
}
