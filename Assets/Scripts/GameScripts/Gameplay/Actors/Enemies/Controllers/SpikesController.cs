using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpikesController : EnemyController
{
    public enum Position { ground, left, right, top }
    
    /// <summary>
    /// Direccio que tenen les spikes: amunt, avall, dreta o esquerra
    /// </summary>
    public Position position;

    /// <summary>
    /// Collider dels spikes
    /// </summary>
    public new Collider2D collider;

    /// <summary>
    /// Farem un raig de la llargada del skinWidth del player
    /// </summary>
    private float checkLength = BoxColliderRayInfo.skinWidth * 2;

    /// <summary>
    /// Primer punt on tirem un raig
    /// </summary>
    private Vector3 pointA;

    /// <summary>
    /// Segon punt on tirem un raig
    /// </summary>
    private Vector3 pointB;

    /// <summary>
    /// Direccio cap on mirem si tenim algú
    /// </summary>
    private Vector3 checkDirection;

    /// <summary>
    /// LayerMask del que ha de matar
    /// </summary>
    private LayerMask shouldKillLayer = Layers.addLayer(0, Layers.Player);

    private void Start()
    {
        pointA = pointB = collider.offset;

        switch (position)
        {
            case Position.ground:
                pointA += new Vector3(-collider.bounds.extents.x + BoxColliderRayInfo.skinWidth, collider.bounds.extents.y - BoxColliderRayInfo.skinWidth);
                pointB += new Vector3(collider.bounds.extents.x - BoxColliderRayInfo.skinWidth, collider.bounds.extents.y - BoxColliderRayInfo.skinWidth);
                checkDirection = Vector3.up;
                break;
            case Position.left:
                pointA += new Vector3(-collider.bounds.extents.x + BoxColliderRayInfo.skinWidth, -collider.bounds.extents.y + BoxColliderRayInfo.skinWidth);
                pointB += new Vector3(-collider.bounds.extents.x + BoxColliderRayInfo.skinWidth, collider.bounds.extents.y - BoxColliderRayInfo.skinWidth);
                checkDirection = Vector3.right;
                break;
            case Position.right:
                pointA += new Vector3(collider.bounds.extents.x + BoxColliderRayInfo.skinWidth, -collider.bounds.extents.y + BoxColliderRayInfo.skinWidth);
                pointB += new Vector3(collider.bounds.extents.x + BoxColliderRayInfo.skinWidth, collider.bounds.extents.y - BoxColliderRayInfo.skinWidth);
                checkDirection = Vector3.left;
                break;
            case Position.top:
                pointA += new Vector3(-collider.bounds.extents.x + BoxColliderRayInfo.skinWidth, -collider.bounds.extents.y + BoxColliderRayInfo.skinWidth);
                pointB += new Vector3(collider.bounds.extents.x - BoxColliderRayInfo.skinWidth, -collider.bounds.extents.y + BoxColliderRayInfo.skinWidth);
                checkDirection = Vector3.down;
                break;
        }
    }

    private void Update()
    {
        Debug.DrawRay(transform.position + pointA, checkDirection * checkLength, Color.red);
        Debug.DrawRay(transform.position + pointB, checkDirection * checkLength, Color.red);

        RaycastHit2D[] hits = new RaycastHit2D[1];

        // Mirem el punt A
        if (Physics2D.RaycastNonAlloc(transform.position + pointA, checkDirection, hits, checkLength, shouldKillLayer) > 0)
        {
            CharacterControl characterControl = hits[0].collider.gameObject.GetComponent<CharacterControl>();
            if(characterControl)
            {
                characterControl.RecieveDamage(gameObject);
            }
        }

        hits = new RaycastHit2D[1];

        // Mirem el punt B
        if (Physics2D.RaycastNonAlloc(transform.position + pointB, checkDirection, hits, checkLength, shouldKillLayer) > 0)
        {
            CharacterControl characterControl = hits[0].collider.gameObject.GetComponent<CharacterControl>();
            if (characterControl)
            {
                characterControl.RecieveDamage(gameObject);
            }
        }
    }
}
