using UnityEngine;
using System.Collections;

public class TurretController : EnemyController
{
    private GameObject _headMesh;

    //public Cannon cannon;

    public TurretFactory factory;

    public bool followParent = true;

    public Shooter shooter;

    /// <summary>
    /// El fem de tipus turret, inicialitzem les explosions i agafem el mesh del head
    /// </summary>
    protected override void Awake()
    {
        // Afegim les explosions primer perque les agafin les llistes d'atributs
        //_startExplosion = transform.GetOrAddComponent<InstantExplosion>();
        //_endExplosion = transform.GetOrAddComponent<EndExplosion>();

        base.Awake();
        type = EnemiesManager.Enemies.Turret;
        
        _headMesh = transform.Find("Mesh").gameObject;

        this.shooter = this.GetComponent<Shooter>();
    }

    public override void OnEnable()
    {
        base.OnEnable();

        _headMesh.SetActive(true);

        //cannon = GetComponent<Cannon>();

        this.followParent = true;

        // cannon.enabled = true;
        // changeCollidersStatus(true);

        events.registerEvent(EnemyEvents.EventName.PowerGloved, OnPowerGloved);

    }



    public override void OnDisable()
    {
        base.OnDisable();
        events.unregisterEvent(EnemyEvents.EventName.PowerGloved, OnPowerGloved);
    }

    /// <summary>
    /// Override del Rotate perqué aquest no gira
    /// </summary>
    public override void OnRotate(GameObject enemy)
    {
        
    }

    public override void OnPowerGloved(GameObject enemy)
    {
        // base.OnPowerGloved();
        // events.triggerEvent(EnemyEvents.EventName.DisableCannon);
        // cannon.canShoot = false;

        this.shooter.ForceEndShooting();

        this.followParent = false;
        //Debug.Log("Ja no faig follow parent");
    }

    public void ExternalMove(Vector3 movement, Space space)
    {
        //Debug.Log("Faig follow parent");
        this.transform.Translate(movement, space);
    }

    /// <summary>
    /// Cridem els esdeveniments que tingui per defecte la classe EnemyController i a més cridem el d'explotar
    /// </summary>
    public override void Die()
    {
        base.Die();
        events.triggerEvent(EnemyEvents.EventName.Explode);

        // Si volia disparar, ho cancel·lem
        // cannon.canShoot = false;
        // events.triggerEvent(EnemyEvents.EventName.DisableCannon);
        this.shooter.ForceEndShooting();

        // Amaguem les malles
        _headMesh.SetActive(false);

        // Treiem els colliders
        // changeCollidersStatus(false);
    }

    
}
