using UnityEngine;
using System.Collections;

public class TutorialStvController : StvController, ITutoriable
{
    public enum TutorialType { Hop, PowerGlove, BlastGloves };

    public TutorialType tutorialType;

    public Bubble powerGloveBubble;
    
    public Bubble blastGloveBubble;

    public Bubble myBubble { get; set; }

    protected override void Awake()
    {
        base.Awake();

        powerGloveBubble.gameObject.SetActive(false);
        blastGloveBubble.gameObject.SetActive(false);
    }

    public void SetTutorialType(TutorialType type)
    {
        this.tutorialType = type;

        myBubble = null;

        switch (tutorialType)
        {
            case TutorialType.PowerGlove:
                myBubble = powerGloveBubble;
                break;
            case TutorialType.BlastGloves:
                myBubble = blastGloveBubble;
                break;
        }

        if (tutorialType == TutorialType.Hop)
        {
            _startExplosion.SetTutorialDensity(true);
        }
        else
        {
            ShowBubble();
        }
    }

    public override void Die()
    {
        base.Die();
        HideBubble();
    }

    public void ShowBubble()
    {
        if (myBubble != null)
        {
            myBubble.gameObject.SetActive(true);
        }
    }

    public void HideBubble()
    {
        if (myBubble != null)
        {
            myBubble.gameObject.SetActive(false);
        }
    }
}
