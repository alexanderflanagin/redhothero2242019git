using UnityEngine;
using System.Collections;

public class DocClockController : StvController
{
    /// <summary>
    /// Segons que triguem a morir
    /// </summary>
    public float diyingTime = 2.0f;

    public float onPowerGlovedStopTime = 0.5f;

    /// <summary>
    /// Animated texture to change the screen once touched
    /// </summary>
    public AnimatedTexture animatedTexture;

    /// <summary>
    /// Doc Clock body renderer (to change material color)
    /// </summary>
    public Renderer meshRenderer;

    /// <summary>
    /// Hem sigut aixafats i estem morint
    /// </summary>
    private bool diying = false;

    /// <summary>
    /// Fem el DocClock de tipus DocClock
    /// </summary>
    protected override void Awake()
    {
        base.Awake();
        type = EnemiesManager.Enemies.DocClock;
    }

    /// <summary>
    /// Al habilitar-lo, ens assegurem de que no estigui morint ja
    /// </summary>
    public override void OnEnable()
    {
        base.OnEnable();
        diying = false;
    }

    /// <summary>
    /// Al ser tocat per un projectil, comencem a morir
    /// </summary>
    /// <param name="by"></param>
    public override void OnTouchedByProjectile(Projectile by)
    {
        events.triggerEvent(EnemyEvents.EventName.TouchedByProjectile);
        if (!diying)
        {
            StartCoroutine(StartDiying());
            diying = true;
        }
    }

    /// <summary>
    /// Al ser saltat a sobre, comencem a morir
    /// </summary>
    public override void JumpedOver()
    {
        events.triggerEvent(EnemyEvents.EventName.JumpedOver);
        if (!diying)
        {
            StartCoroutine(StartDiying());
            diying = true;
            //events.triggerEvent(EnemyEvents.EventName.JumpedOver);
        }
    }

    /// <summary>
    /// Al ser coplejat, cridem la coroutina de mort per cop
    /// </summary>
    /// <param name="lookingAtPoint"></param>
    public override void PowerGloved(Vector3 lookingAtPoint)
    {
        StartCoroutine(PowerGlovedCollidedDie());
    }

    /// <summary>
    /// Al ser xocar contra un objecte colpejat, cridem la coroutina de mort per cop
    /// </summary>
    /// <param name="lookingAtPoint"></param>
    public override void Collide(GameObject with)
    {
        StartCoroutine(PowerGlovedCollidedDie());
    }

    /// <summary>
    /// Morim si hem de morir. Apart, ens parem, esperem *onPowerGlovedStopTime* i ens seguim movent
    /// </summary>
    /// <param name="lookingAtPoint"></param>
    private IEnumerator PowerGlovedCollidedDie()
    {
        if(!diying)
        {
            StartCoroutine(StartDiying());
            diying = true;
        }

        events.triggerEvent(EnemyEvents.EventName.PowerGloved);
        yield return new WaitForSeconds(onPowerGlovedStopTime);
        events.triggerEvent(EnemyEvents.EventName.Move);
    }

    /// <summary>
    /// Ens esperem /diyingTime/ abans de morir. Mentrestant fem pampallugues
    /// </summary>
    /// <returns></returns>
    private IEnumerator StartDiying()
    {
        animatedTexture.StartCoroutine(animatedTexture.UpdateTiling());

        float startTime = Time.time;

        Color originalColor = meshRenderer.material.GetColor("_Color");

        while (startTime + diyingTime > Time.time)
        {
            meshRenderer.material.SetColor("_Color", Color.Lerp(originalColor, Color.red, (Time.time - startTime) / diyingTime ));
            yield return null;
        }

        meshRenderer.material.color = originalColor;

        Die();
    }
}
