using UnityEngine;
using System.Collections;

public class OffensivePlatformTimedController : EnemyController
{
    /// <summary>
    /// El collider del mateix game object, el que fa d'enemic
    /// </summary>
    private new Collider2D collider;

    /// <summary>
    /// L'animator per a poder cridar l'animació de sortir i amagar punxes
    /// </summary>
    private Animator animator;

    /// <summary>
    /// Temps que estan a dalt les punxes
    /// </summary>
    private static float upTime = 1;

    /// <summary>
    /// Temps que estan abaix les punxes
    /// </summary>
    private static float downTime = 2;

    /// <summary>
    /// Delay abans de començar. Per a poder fer punxes que comencin a diferents moments
    /// </summary>
    [Range(1,31)]
    public int decisecondsDelay = 1;

    /// <summary>
    /// Fem la offensive platform de tipus offensive platform
    /// </summary>
    protected override void Awake()
    {
        base.Awake();
        this.type = EnemiesManager.Enemies.Spike;

        this.collider = this.GetComponent<Collider2D>();

        this.animator = this.GetComponent<Animator>();
    }

    /// <summary>
    /// Comencem la coroutina del run
    /// </summary>
    public override void OnEnable()
    {
        base.OnEnable();
        StartCoroutine(Run());
    }

    /// <summary>
    /// Desactivem el collider, ens esperem el delay i comencem a pujar i baixar les punxes
    /// </summary>
    /// <returns></returns>
    private IEnumerator Run()
    {
        this.collider.enabled = false;
        this.animator.SetBool("SpikeOut", false);

        int framesFromAnimationToColliderUp = 7;

        int framesFromAnimationToColliderDown = 5;

        yield return new WaitForSeconds((float)decisecondsDelay / 10f);

        while (true)
        {
            yield return new WaitForSeconds(OffensivePlatformTimedController.downTime);
            this.animator.SetBool("SpikeOut", true);
            for (int i = 0; i < framesFromAnimationToColliderUp; i++)
            {
                yield return null;
            }
            
            this.collider.enabled = true;
            testIfPlayerInCollider();
            yield return new WaitForSeconds(OffensivePlatformTimedController.upTime);
            this.animator.SetBool("SpikeOut", false);
            for (int i = 0; i < framesFromAnimationToColliderDown; i++)
            {
                yield return null;
            }

            this.collider.enabled = false;
        }
    }

    /// <summary>
    /// Aquesta funció mira si, al pujar les punxes, el player és allà. Si és això, l'avisa de que ha mort
    /// </summary>
    private void testIfPlayerInCollider()
    {
        Collider2D player;
        if ((player = Physics2D.OverlapArea(this.collider.bounds.min, this.collider.bounds.max, Layers.Players)))
        {
            player.gameObject.GetComponent<CharacterControl>().StartDying(true, false);
        }
    }

    /// <summary>
    /// Override del Rotate perqué aquest no gira
    /// </summary>
    public override void OnRotate(GameObject enemy)
    {
        
    }
}
