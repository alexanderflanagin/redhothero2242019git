using UnityEngine;
using System.Collections;

public class OffensivePlatformController : EnemyController
{
    /// <summary>
    /// El collider del mateix game object, el que fa d'enemic
    /// </summary>
    private new Collider2D collider;

    /// <summary>
    /// L'animator per a poder cridar l'animació de sortir i amagar punxes
    /// </summary>
    private Animator animator;

    /// <summary>
    /// Layers que activen les punxes
    /// </summary>
    public LayerMask overLayer;

    /// <summary>
    /// Temps que estan a dalt les punxes
    /// </summary>
    private static float upTime = 1;

    /// <summary>
    /// Temps que estan abaix les punxes
    /// </summary>
    private static float downTime = 0.1f;

    /// <summary>
    /// El box collider del terra. Per saber des d'on sortiran les punxes
    /// </summary>
    public BoxCollider2D ground;

    /// <summary>
    /// Alçada fins a la que fa mal aquesta plataforma
    /// </summary>
    public float triggerHeight;

    /// <summary>
    /// Si ara mateix està activada i funcionant (o pujant i baixant)
    /// </summary>
    private bool running;

    /// <summary>
    /// Fem la offensive platform de tipus offensive platform
    /// </summary>
    protected override void Awake()
    {
        base.Awake();
        this.type = EnemiesManager.Enemies.Spike;

        this.collider = this.GetComponent<Collider2D>();

        this.animator = this.GetComponent<Animator>();
    }

    /// <summary>
    /// Comencem la coroutina del run
    /// </summary>
    public override void OnEnable()
    {
        base.OnEnable();

        this.collider.enabled = false;
    }

    /// <summary>
    /// Iniciem la coroutina que mira si tenim res a sobre
    /// </summary>
    void Start()
    {
        StartCoroutine(TestIfOver());
    }

    /// <summary>
    /// Mira cada frame si hi ha algun objecte de la capa /overLayer/
    /// </summary>
    /// <returns></returns>
    private IEnumerator TestIfOver()
    {
        Vector3 heighVector = new Vector3(0, this.triggerHeight);

        Collider2D[] overGroundCollider = new Collider2D[1];

        while(true)
        {
            if (!running && Physics2D.OverlapAreaNonAlloc(this.ground.bounds.min, this.ground.bounds.max + heighVector, overGroundCollider, this.overLayer) > 0)
            {
                StartCoroutine(Run());
            }

            yield return null;
        }
    }

    /// <summary>
    /// Desactivem el collider, ens esperem el delay i comencem a pujar i baixar les punxes
    /// </summary>
    /// <returns></returns>
    private IEnumerator Run()
    {
        running = true;

        int framesFromAnimationToColliderUp = 30;

        int framesFromAnimationToColliderDown = 10;

        this.animator.SetBool("SpikeOut", true);
        for (int i = 0; i < framesFromAnimationToColliderUp; i++)
        {
            yield return null;
        }
            
        this.collider.enabled = true;
        testIfPlayerInCollider();
        yield return new WaitForSeconds(OffensivePlatformController.upTime);
        this.animator.SetBool("SpikeOut", false);
        for (int i = 0; i < framesFromAnimationToColliderDown; i++)
        {
            yield return null;
        }

        this.collider.enabled = false;
        

        running = false;
    }

    /// <summary>
    /// Aquesta funció mira si, al pujar les punxes, el player és allà. Si és això, l'avisa de que ha mort
    /// </summary>
    private void testIfPlayerInCollider()
    {
        Collider2D player;
        if ((player = Physics2D.OverlapArea(this.collider.bounds.min, this.collider.bounds.max, Layers.Players)))
        {
            player.gameObject.GetComponent<CharacterControl>().StartDying(true, false);
        }
    }

    /// <summary>
    /// Override del Rotate perqué aquest no gira
    /// </summary>
    public override void OnRotate(GameObject enemy)
    {
        
    }
}
