using UnityEngine;

public class SplurghController : EnemyController
{
    /// <summary>
    /// El cos que queda després de que es mati
    /// </summary>
    private GameObject _deadBody;

    /// <summary>
    /// El box collider del slug (que farem servir per fer el custom pacer)
    /// </summary>
    [HideInInspector]
    public BoxCollider2D boxCollider;
    
    /// <summary>
    /// Fem l'S-tv de tipus s-tv
    /// </summary>
    protected override void Awake()
    {
        base.Awake();
        type = EnemiesManager.Enemies.Splurgh;

        if (GetComponent<Glower>())
        {
            _deadBody = transform.Find("DeadBody").gameObject;
            _deadBody.SetActive(false);
        }
    }

    public override void OnEnable()
    {
        ChangeCollidersStatus(true);

        Slug slugMove = GetComponent<Slug>();
        boxCollider = GetComponent<BoxCollider2D>();

        meshObject.transform.localPosition = Vector3.zero;
        meshObject.transform.rotation = Quaternion.identity;

        switch (slugMove.moveFloor)
        {
            case Factory.MoveFloor.wallLeft:
                meshObject.transform.Rotate(0, 180, 90);
                break;

            case Factory.MoveFloor.wallRight:
                meshObject.transform.localPosition = new Vector3(0.978f, 0.329f, 0);
                meshObject.transform.Rotate(0, 0, 90);
                break;

            case Factory.MoveFloor.roof:
                meshObject.transform.Rotate(0, 180, 180);
                meshObject.transform.localPosition = new Vector3(0, 0.965f, 0);
                break;
        }


        if (slugMove.moveFloor == Factory.MoveFloor.wallLeft || slugMove.moveFloor == Factory.MoveFloor.wallRight)
        {
            boxCollider.size = new Vector2(boxCollider.size.y, boxCollider.size.x);
            boxCollider.offset = new Vector2(boxCollider.offset.y, boxCollider.offset.x);
        }

        // 2 el coloquem a la paret que toqui
        Vector3 rayDirection = Vector3.zero;
        Vector2 positionCorrection = Vector2.zero;

        switch (slugMove.moveFloor)
        {
            case Factory.MoveFloor.floor:
                rayDirection = -Vector3.up;
                positionCorrection = Vector2.zero;
                break;
            case Factory.MoveFloor.roof:
                rayDirection = Vector3.up;
                positionCorrection = new Vector2(0, -boxCollider.size.y);
                break;
            case Factory.MoveFloor.wallLeft:
                rayDirection = -Vector3.right;
                // TODO Estem impedint que la splurgh es posi dins de la paret. Quan tinguem el controller nou mirem com ho hem de fer
                positionCorrection = new Vector2(boxCollider.size.x - boxCollider.offset.x - 0.49f, 0);
                break;
            case Factory.MoveFloor.wallRight:
                rayDirection = Vector3.right;
                positionCorrection = new Vector2(-boxCollider.size.x, 0);
                break;
        }

        RaycastHit2D hit = Physics2D.Raycast(transform.position, rayDirection, Mathf.Infinity, Layers.addLayer(0, Layers.Ground));
        //Debug.DrawRay(transform.position, rayDirection);
        if(!hit)
        {
            Debug.LogWarning("El slug no ha trobat cap lloc on posar-se", this.gameObject);
        }
        else
        {
            this.transform.position = hit.point + positionCorrection;
        }
    }

    public override void OnDisable()
    {
        base.OnDisable();

        // Tornem la mesh a la seva mida original
        meshObject.transform.rotation = Quaternion.identity;

        // Retornem el collider a la seva mida original
        Slug slugMove = GetComponent<Slug>();
        if (slugMove.moveFloor == Factory.MoveFloor.wallLeft || slugMove.moveFloor == Factory.MoveFloor.wallRight)
        {
            boxCollider.size = new Vector2(boxCollider.size.y, boxCollider.size.x);
            boxCollider.offset = new Vector2(boxCollider.offset.y, boxCollider.offset.x);
        }
    }

    /// <summary>
    /// Cridem els esdeveniments que tingui per defecte la classe EnemyController i a més cridem el d'explotar
    /// </summary>
    public override void Die()
    {
        base.Die();
        events.triggerEvent(EnemyEvents.EventName.Explode);

        if (_deadBody)
        {
            _deadBody.SetActive(true);
        }
    }

    public override void PowerGloved(Vector3 lookingAtPoint)
    {
        events.triggerEvent(EnemyEvents.EventName.PowerGloved);
        Die();
    }
}
