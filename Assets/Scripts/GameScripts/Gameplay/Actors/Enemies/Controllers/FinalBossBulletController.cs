using UnityEngine;

public class FinalBossBulletController : EnemyController
{
    /// <summary>
    /// If creates a density when it dies
    /// </summary>
    public bool createDensity;

    /// <summary>
    /// The collider of the bullet
    /// </summary>
    public new BoxCollider2D collider;

    /// <summary>
    /// How *fast* the bullet rotates
    /// </summary>
    public Vector3 rotationVector = new Vector3(90, 90, 180);

    /// <summary>
    /// To know if we are powergloved (to stop rotating ourself and start rotating by PowerGloved)
    /// </summary>
    protected bool powerGloved;

    public override void OnEnable()
    {
        base.OnEnable();
        events.registerEvent(EnemyEvents.EventName.PowerGloved, StopRotating);

        transform.rotation = Quaternion.identity;

        _physicsController.BlockedFrontEnter += OnBlockedFront;
    }

    public override void OnDisable()
    {
        base.OnDisable();
        
        events.unregisterEvent(EnemyEvents.EventName.PowerGloved, StopRotating);

        _physicsController.BlockedFrontEnter -= OnBlockedFront;
    }

    private void Update()
    {
        if(!powerGloved)
        {
            meshObject.transform.RotateAround(collider.bounds.center, new Vector3(1, 0, 0), rotationVector.x * Time.deltaTime);
            meshObject.transform.RotateAround(collider.bounds.center, new Vector3(0, 1, 0), rotationVector.y * Time.deltaTime);
            meshObject.transform.RotateAround(collider.bounds.center, new Vector3(0, 0, 1), rotationVector.z * Time.deltaTime);
        }
    }

    private void StopRotating(GameObject enemy)
    {
        _physicsController.BlockedFrontEnter -= OnBlockedFront;
        powerGloved = true;
    }

    public override void Collide(GameObject with)
    {
        AudioManager.Instance.PlayOneShoot(AudioClipsData.ClipsGroups.Enviro, "BreakableCollide");

        Die();
    }

    /// <summary>
    /// If it is blocked front, should Collide. If blocker object is IPowerGloveable will Collide too
    /// </summary>
    void OnBlockedFront()
    {
        // Sempre es el de la dreta perque dispara cap a la dreta
        GameObject blocker = _physicsController.collisions.rightObject;

        IPowerGloveable iPowerGloveable = blocker.transform.GetInterface<IPowerGloveable>();
        if(iPowerGloveable != null)
        {
            iPowerGloveable.Collide(gameObject);
        }

        Collide(blocker);
    }

    public override void Die()
    {
        base.Die();
        events.triggerEvent(EnemyEvents.EventName.Explode);
        gameObject.SetActive(false);
    }
}
