using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using Com.LuisPedroFonseca.ProCamera2D;

[RequireComponent(typeof(Liner))]
[RequireComponent(typeof(Shootable))]
[RequireComponent(typeof(Physics2DController))]
public class FinalBossController : EnemyController
{
    [HideInInspector]
    public EnemyState.State state;

    /// <summary>
    /// Final boss collider
    /// </summary>
    public new BoxCollider2D collider;
    
    /// <summary>
    /// Walker script
    /// </summary>
    public Liner liner;

    /// <summary>
    /// Shooter script
    /// </summary>
    public Shooter shooter;

    /// <summary>
    /// Mask to show current health
    /// </summary>
    public Image healthBarMask;

    /// <summary>
    /// Direct access to cannon
    /// </summary>
    public FinalBossCannon cannon;

    public GameObject meshGameObject;

    /// <summary>
    /// Finish line to move to the final boss position when dies
    /// </summary>
    public GameObject finishLine;

    [Header("Particles")]
    public ParticleSystem explosionParticles;

    public GameObject damagedParticles;

    public ParticleSystem shootParticles;


    [Header("Values")]
    /// <summary>
    /// Final boss max health
    /// </summary>
    [Tooltip("Final boss max health")]
    public float health = 10;

    /// <summary>
    /// Final boss moving speed
    /// </summary>
    [Tooltip("Final boss moving speed")]
    public float speed = 10;

    /// <summary>
    /// Acceleration to recover speed (and deceleration between recovering and normal walking)
    /// </summary>
    [Tooltip("Acceleration to recover speed (and deceleration between recovering and normal walking)")]
    public float acceleration = 2;

    public float animDieTime = 6.292f;

    public float onDieZoomTime = 1f;

    /// <summary>
    /// Damage and effects caused when player powerglove the final boss
    /// </summary>
    [Tooltip("Damage and effects caused when player powerglove the final boss")]
    public DamageTypeStats powerGlovedDamage;

    /// <summary>
    /// Damage and effects caused when a powergloved object collides with the final boss
    /// </summary>
    [Tooltip("Damage and effects caused when a powergloved object collides with the final boss")]
    public DamageTypeStats collideDamage;

    /// <summary>
    /// Damage and effects caused when a projectile touches the final boss
    /// </summary>
    [Tooltip("Damage and effects caused when a projectile touches the final boss")]
    public DamageTypeStats shootedDamage;

    public float waitBeforeStartMoving;

    /// <summary>
    /// Until what Time.time it will be stopped because some damages
    /// </summary>
    protected float untilStoppedTime;

    /// <summary>
    /// Energy (health) of the final boss
    /// </summary>
    protected float energy;

    protected float lastFramePowerGloved;

    /// <summary>
    /// Action executed in update
    /// </summary>
    protected Action updateAction;

    protected bool alreadyDead = false;

    [Header("Debug Purpouse")]
    public bool debuggingMesh;

    public MeshRenderer finalBossMesh;

    public Color walkingColor;

    public Color damagedColor;

    public Color recoveringColor;

    public Color deadColor;

    protected override void Awake()
    {
        base.Awake();
        _direction = 1; // Ens movem cap a la dreta
        liner.speed = speed;
        energy = health;

        _physicsController.UpdateLayerMask(0);
    }

    private void Start()
    {
        updateAction = Waiting;

        finishLine.SetActive(false);

        if (explosionParticles)
        {
            Instantiator.LoadParticleSystem(ref explosionParticles, Vector3.zero, Vector3.zero, gameObject);
            explosionParticles.Stop();
        }

        cannon.shootParticles = GameObject.Instantiate(shootParticles, cannon.transform) as ParticleSystem;
        cannon.shootParticles.transform.localPosition = Vector3.right;
        cannon.shootParticles.Stop();
    }

    public override void OnRotate(GameObject enemy)
    {
        // Do Nothing
    }

    public override void OnTouchedByProjectile(Projectile by)
    {
        if (!ShouldIgnoreDamage())
        {
            RecieveDamage(shootedDamage);
            Instantiate(damagedParticles, by.transform.position, Quaternion.identity);
        }
    }

    public override void PowerGloved(Vector3 lookingAtPoint)
    {
        RecieveDamage(powerGlovedDamage);
        Instantiate(damagedParticles, StandardStageManager.current.player.transform.position, Quaternion.identity);
    }

    public override void Collide(GameObject with)
    {
        if(!ShouldIgnoreDamage())
        {
            RecieveDamage(collideDamage);
            Instantiate(damagedParticles, with.transform.position, Quaternion.identity);
        }
    }

    /// <summary>
    /// Usefull to know if it should recieve damage or not
    /// </summary>
    /// <returns>True if it shouldn't recieve damage</returns>
    public bool ShouldIgnoreDamage(bool powerGloved = false)
    {
        if(powerGloved)
        {
            if (Time.frameCount >= (lastFramePowerGloved + 1))
            {
                // Ignorem perqu� el darrer frame el vam colpejar
                lastFramePowerGloved = Time.frameCount;
                return true;
            }

            lastFramePowerGloved = Time.frameCount;
        }

        return false;
    }

    private void RecieveDamage(DamageTypeStats damage)
    {
        if(state == EnemyState.State.Waiting)
        {
            // Quan estem fent waiting encara no se'ns pot fer mal
            return;
        }

        untilStoppedTime = Time.time + damage.timeToRecover;
        liner.speed = 0;
        energy -= damage.energyDrain;
        healthBarMask.fillAmount = (health - energy) / health;

        if (energy <= 0)
        {
            state = EnemyState.State.Dead;
            updateAction = Dead;
        }
        else
        {
            state = EnemyState.State.Damaged;
            updateAction = Damaged;
        }
    }

    private void Update()
    {
        if(updateAction != null)
        {
            updateAction();
        }

        CheckPowerGloved();
    }

    void Waiting()
    {
        if (Time.timeSinceLevelLoad > waitBeforeStartMoving)
        {
            //shooter.StartShotting();
            events.triggerEvent(EnemyEvents.EventName.Move);

            state = EnemyState.State.Walking;
            updateAction = Walking;

            if(cannon != null)
            {
                cannon.movingCannon = true;
            }
        }
    }

    void Walking()
    {
        if(debuggingMesh)
        {
            finalBossMesh.material.color = walkingColor;
        }

        if (!Mathf.Approximately(liner.speed, speed))
        {
            liner.speed = Mathf.Lerp(liner.speed, speed, Time.deltaTime * acceleration);
        }
        else
        {
            liner.speed = speed;
        }
    }

    void Damaged()
    {
        if (debuggingMesh)
        {
            finalBossMesh.material.color = damagedColor;
        }

        if (untilStoppedTime < Time.time)
        {
            liner.speed = 0;
            state = EnemyState.State.Recovering;
            updateAction = Recovering;
        }
    }

    void Recovering()
    {
        if (debuggingMesh)
        {
            finalBossMesh.material.color = recoveringColor;
        }

        liner.speed = Mathf.Lerp(liner.speed, speed, Time.deltaTime * acceleration);

        if(Mathf.Approximately(liner.speed, speed))
        {
            liner.speed = speed;
            state = EnemyState.State.Walking;
            updateAction = Walking;
        }
    }

    void Dead()
    {
        if (!alreadyDead)
        {
            if (debuggingMesh)
            {
                finalBossMesh.material.color = deadColor;
            }

            if (shooter)
            {
                // Do Nothing
                foreach (GameObject cannon in shooter.cannonsObjects)
                {
                    cannon.SetActive(false);
                }
            }

            // Desactivem mesh i collider i mostrem la meta
            collider.enabled = false;
            StartCoroutine(WaitToDie());
            alreadyDead = true;
        }
    }

    private IEnumerator WaitToDie()
    {
        // Parem al player
        StandardStageManager.current.playerControl.input.Freeze(0);
        StandardStageManager.current.playerControl.suit.energy.loseEnergy = false;

        // Parem la camera
        ProCamera2D.Instance.Zoom(4f, onDieZoomTime);
        ProCamera2DShake.Instance.Shake(animDieTime, Vector2.one * 2, 20, .5f, -1, default(Vector3), 0.1f);
        yield return new WaitForSeconds(onDieZoomTime);
        ProCamera2D.Instance.enabled = false;

        yield return new WaitForSeconds(animDieTime - onDieZoomTime);

        if(meshGameObject)
        {
            meshGameObject.SetActive(false);
        }

        if (explosionParticles)
        {
            explosionParticles.Play();
        }

        finishLine.transform.position = transform.position;
        finishLine.SetActive(true);

        // Reactivem la camera
        ProCamera2D.Instance.enabled = true;

        // Reactivem el player
        StandardStageManager.current.playerControl.input.Defreeze();
        StandardStageManager.current.playerControl.suit.energy.loseEnergy = true;
    }

    /// <summary>
    /// Used to check if some powergloved element is trespassing me (because with my movement maybe I'm ignoring it)
    /// </summary>
    void CheckPowerGloved()
    {
        int verticalRays = 2;
        float distanceBetweenRays = .5f;
        float yPosition = collider.bounds.max.y;
        float distance = collider.size.y;
        RaycastHit2D hit;
        LayerMask checkLayerMask = (1 << Layers.Ground) | (1 << Layers.Enemy);

        for (int i = 0; i < verticalRays; i++)
        {
            float xPosition = collider.bounds.max.x - (i * distanceBetweenRays + distanceBetweenRays);

            if(hit = Physics2D.Raycast(new Vector2(xPosition, yPosition), Vector3.down, distance, checkLayerMask))
            {
                PowerGloved powerGloved = hit.collider.GetComponent<PowerGloved>();
                if (powerGloved)
                {
                    Collide(powerGloved.gameObject);
                    powerGloved.GetComponent<IPowerGloveable>().Collide(gameObject);
                    return;
                }
            }
        }
    }

    /// <summary>
    /// Class used to define diferent types of damage recieved by the final boss
    /// </summary>
    [System.Serializable]
    public class DamageTypeStats
    {
        /// <summary>
        /// How much time it will be stopped
        /// </summary>
        public float timeToRecover;

        /// <summary>
        /// How much energy will lost with this damage
        /// </summary>
        public float energyDrain;
    }
}
