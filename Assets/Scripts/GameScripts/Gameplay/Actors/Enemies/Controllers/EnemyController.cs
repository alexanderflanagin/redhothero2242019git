using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class EnemyController : MonoBehaviour, IPowerGloveable
{
    /// <summary>
    /// Els esdeveniments d'aquest Controller. Per a que es comuniquin els components de l'enemic
    /// </summary>
    public EnemyEvents events = new EnemyEvents();

    /// <summary>
    /// Si l'enemic es pot matar (practic per als enemics generics)
    /// </summary>
    public bool killable;

    /// <summary>
    /// Si l'enemic es mortal (practic per als enemics generics)
    /// </summary>
    public bool mortal;

    /// <summary>
    /// Si l'enemic mata nomes de tocar-lo
    /// </summary>
    public bool killOnTouch;

    /// <summary>
    /// Si l'enemic, per si sol, no fa mal
    /// </summary>
    public bool harmless;

    /// <summary>
    /// Per quan li fem un power glove
    /// </summary>
    private bool wasHarmless;

    /// <summary>
    /// Ens indica si l'enemic es pot disparar, i si mor al ser disparat
    /// </summary>
    public bool shootable;

    /// <summary>
    /// Ens indica si l'enemic es pot colpejar amb el power glove
    /// </summary>
    public bool puncheable;

    /// <summary>
    /// El de l'enemic. Per defecte el fem generic, pero els controllers fills l'han de sobreescriure
    /// </summary>
    public EnemiesManager.Enemies type = EnemiesManager.Enemies.GenericEnemy;

    /// <summary>
    /// Variable que fem servir per saber si estem vius
    /// Per no morir dos cops en dos frames seguits perque no s'han actualitzat les fisiques
    /// </summary>
    protected bool alive;

    /// <summary>
    /// El Controller de fisiques
    /// </summary>
    protected Physics2DController _physicsController;

    /// <summary>
    /// GameObject d'on pengen totes les meshes. Per si s'han de rotar, amagar, escalar, etc.
    /// </summary>
    protected GameObject _meshObject;

    /// <summary>
    /// Getter public de l'objecte de les meshes.
    /// </summary>
    public GameObject meshObject
    {
        get
        {
            return _meshObject;
        }
    }

    /// <summary>
    /// Movement direction
    /// </summary>
    protected int _direction = -1;

    /// <summary>
    /// Getter per a la direccio del moviment
    /// </summary>
    public int direction
    {
        get
        {
            return _direction;
        }
    }

    /// <summary>
    /// Llista de colliders del Enemy
    /// </summary>
    protected List<Collider2D> colliders;

    /// <summary>
    /// Hereta de IPowerGloveable. Ens diu si es l'objecte que esta seguint la camera
    /// </summary>
    public bool theChosen { get; set; }

    /// <summary>
    /// Controller d'inici d'explosio
    /// </summary>
    protected StartExplosion _startExplosion;

    /// <summary>
    /// Getter public per al controller d'inici d'explosio
    /// </summary>
    public StartExplosion startExplosion
    {
        get
        {
            return _startExplosion;
        }
    }

    /// <summary>
    /// Controller de final d'explosiÃ³
    /// </summary>
    protected EndExplosion _endExplosion;

    /// <summary>
    /// Mal que fa aquest enemic quan toca al player
    /// </summary>
    public int damage
    {
        get
        {
            return EnergyManager.DAMAGE_PER_TOUCH;
        }
    }

    /// <summary>
    /// Les habilitats de l'enemic
    /// </summary>
    protected EnemyAbility[] abilities;

    /// <summary>
    /// Els atributs de l'enemic
    /// </summary>
    protected EnemyAttribute[] attributes;

    /// <summary>
    /// Crea el physics2DController (si no en te) i hi assigna la layer de col.lisiona. Tambe busca l'objecte mesh de l'enemic
    /// </summary>
    protected virtual void Awake()
    {
        _physicsController = this.transform.GetOrAddComponent<Physics2DController>();

        _meshObject = this.transform.Find("Mesh").gameObject;

        if (_meshObject == null)
        {
            Debug.LogWarning("This enemy don't have a mesh", this.gameObject);
        }

        this.colliders = new List<Collider2D>();

        // Creem la llista de colliders amb els fills
        foreach (Collider2D col in GetComponentsInChildren<Collider2D>())
        {
            this.colliders.Add(col);
        }

        if (this.shootable)
        {
            this.transform.GetOrAddComponent<Shootable>().touched += OnTouchedByProjectile;
        }

        // Tipus d'explosions per defecte. Les afegim abans d'agafar els atributs
        _startExplosion = this.GetComponent<StartExplosion>();
        _endExplosion = this.GetComponent<EndExplosion>();

        events.myGameObject = this.gameObject;

        // Recuperem habilitats i atributs
        this.abilities = this.GetComponentsInChildren<EnemyAbility>();
        this.attributes = this.GetComponentsInChildren<EnemyAttribute>();

        for (int i = 0; i < this.abilities.Length; i++)
        {
            this.abilities[i].Prepare(this);
        }

        for (int i = 0; i < this.attributes.Length; i++)
        {
            this.attributes[i].Prepare(this);
        }
    }

    /// <summary>
    /// Per a activar o desactivar tots els colliders 2D de l'objecte i els seus fills
    /// </summary>
    /// <param name="enable">Si volem habilitar o deshabilitar els colliders</param>
    protected void ChangeCollidersStatus(bool enable)
    {
        foreach (Collider2D col in colliders)
        {
            col.enabled = enable;
        }
    }

    /// <summary>
    /// Al habilitar el gameObject, afegim l'OnRotate a la llista d'Events
    /// </summary>
    public virtual void OnEnable()
    {
        _physicsController.UpdateLayerMask(Layers.EnemiesCollideWith);
        _physicsController.HorizontallyCrushed += Crushed;
        _physicsController.VerticallyCrushed += Crushed;

        events.registerEvent(EnemyEvents.EventName.Rotate, OnRotate);
        events.registerEvent(EnemyEvents.EventName.Hide, OnHide);
        if (puncheable)
        {
            events.registerEvent(EnemyEvents.EventName.PowerGloved, OnPowerGloved);
        }
        
        ChangeCollidersStatus(true);
        meshObject.SetActive(true);

        for (int i = 0; i < this.abilities.Length; i++ )
        {
            this.abilities[i].Enable();
        }

        for (int i = 0; i < this.attributes.Length; i++)
        {
            this.attributes[i].Enable();
        }

        // M'afegeixo a la llista d'enemics actius
        StandardStageManager.current.activeEnemies.Add(this);

        alive = true;
    }

    /// <summary>
    /// Al deshabilitar el gameObject, treiem l'OnRotate de la llista d'Events
    /// </summary>
    public virtual void OnDisable()
    {
        events.unregisterEvent(EnemyEvents.EventName.Rotate, OnRotate);
        events.unregisterEvent(EnemyEvents.EventName.Hide, OnHide);

        if (puncheable)
        {
            events.unregisterEvent(EnemyEvents.EventName.PowerGloved, OnPowerGloved);
        }

        for (int i = 0; i < this.abilities.Length; i++)
        {
            this.abilities[i].Disable();
        }

        for (int i = 0; i < this.attributes.Length; i++)
        {
            this.attributes[i].Disable();
        }

        _physicsController.HorizontallyCrushed -= Crushed;
        _physicsController.VerticallyCrushed -= Crushed;

        EnemyDisable enemyDisable = GetComponent<EnemyDisable>();
        if(enemyDisable != null)
        {
            Destroy(enemyDisable);
        }

        // Em trec de la llista d'enemics actius
        StandardStageManager.current.activeEnemies.Remove(this);
    }

    /// <summary>
    /// Al comenÃ§ar a moure, cridem l'Event Move
    /// </summary>
    public virtual void Move()
    {
        this.events.triggerEvent(EnemyEvents.EventName.Move);
    }

    /// <summary>
    /// Al OnRotate per defecte, simplement girem la malla 180Âº en les Y
    /// </summary>
    public virtual void OnRotate(GameObject enemy)
    {
        _meshObject.transform.Rotate(0, 180, 0);
    }

    /// <summary>
    /// FunciÃ³ que es crida al rebre l'esdeveniment de Hide
    /// </summary>
    public virtual void OnHide(GameObject enemy)
    {
        this.gameObject.SetActive(false);
    }

    /// <summary>
    /// Si Ã©s colpejat, fem que no s'amagui per l'event Hide de manera natural
    /// </summary>
    public virtual void OnPowerGloved(GameObject enemy)
    {
        this.events.unregisterEvent(EnemyEvents.EventName.Hide, OnHide);
    }

    /// <summary>
    /// Al morir, cridem l'Event Die
    /// </summary>
    public virtual void Die()
    {
        alive = false;

        // Al morir, deshabilitem totes les habilitats i atributs (no se si caldran els atributs)
        for (int i = 0; i < this.abilities.Length; i++)
        {
            this.abilities[i].Disable();
        }

        for (int i = 0; i < this.attributes.Length; i++)
        {
            this.attributes[i].Disable();
        }
        
        if (this.shootable)
        {
            this.GetComponent<Shootable>().Deactivate();
        }
        
        this.events.triggerEvent(EnemyEvents.EventName.Die);

        if (this.GetComponent<PowerGloved>())
        {
            this.GetComponent<PowerGloved>().DisablePowerGloved();
        }

        this.ChangeCollidersStatus(false);

        if(gameObject.activeInHierarchy)
        {
            StartCoroutine(DisableAfterDie(3.5f));
        }
    }

    /// <summary>
    /// Deshabilita aquest game object despres de /time/ segons sense fer-lo servir
    /// </summary>
    /// <param name="time">Temps que ha d'esperar abans de deshabilitar-lo</param>
    /// <returns></returns>
    public virtual IEnumerator DisableAfterDie(float time)
    {
        yield return new WaitForSeconds(time);

        // TODO: Fer una variable per a trackejar aixo, no fer servir la llista de colliders
        if(colliders != null && colliders.Count > 0)
        {
            if(colliders[0].isActiveAndEnabled)
            {
                // Per alguna rao els colliders tornen a estar habilitats, per tant no deshabilitem el game object
                yield break;
            }
        }

        gameObject.SetActive(false);
    }

    /// <summary>
    /// Metode per defecte per a deshabilitar enemics sense matar-los
    /// </summary>
    public virtual void Disable()
    {
        this.events.triggerEvent(EnemyEvents.EventName.Disable);

        if (this.GetComponent<PowerGloved>())
        {
            this.GetComponent<PowerGloved>().DisablePowerGloved();
        }

        this.gameObject.SetActive(false);
    }

    /// <summary>
    /// FunciÃ³ que crida el PowerGlove del player al colpejar aquest objecte
    /// </summary>
    /// <param name="lookingAtPoint">Vector direccional de cap a on apunta el player</param>
    public virtual void PowerGloved(Vector3 lookingAtPoint)
    {
        if(!puncheable)
        {
            // Aquest objecte no es pot colpejar
            return;
        }

        events.triggerEvent(EnemyEvents.EventName.PowerGloved);

        if (this.GetComponent<EnemyDisable>())
        {
            // Si ha estat colpejat ja no el deshabilitem de la seva manera natural
            Destroy(this.GetComponent<EnemyDisable>());
        }

        PowerGloved pG = transform.GetOrAddComponent<PowerGloved>();
        pG.direction = Quaternion.Inverse(transform.rotation) * lookingAtPoint;
        pG.objectToMove = this.gameObject;

        pG.AddTrail(gameObject.GetComponent<Collider2D>().offset);
    }

    /// <summary>
    /// Al entrar un trigger mirem si es el Tsunami de Magma, i si es aquest matem l'enemic
    /// </summary>
    /// <param name="other"></param>
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.layer == Layers.Enemy && other.gameObject.CompareTag(Tags.LavaWall))
        {
            this.Die();
        }
    }

    /// <summary>
    /// Funcio heretada del IPowerGloveable que es crida just abans de ser colpejat
    /// </summary>
    public void SaveState()
    {
        wasHarmless = harmless;
        harmless = true;
    }

    /// <summary>
    /// Funcio heretada del IPowerGloveable que es crida quan desapareix aquest objecte despres de ser colpejat
    /// </summary>
    public void RecoverState()
    {
        harmless = wasHarmless;
    }

    /// <summary>
    /// FunciÃ³ heretada del IPowerGloveable que es llenÃ§a quan l'enemic colpejat xoca
    /// </summary>
    public virtual void Collide(GameObject with)
    {
        if (!puncheable)
        {
            // Aquest objecte no es pot colpejar
            return;
        }

        Physics2D.queriesHitTriggers = true;
        Collider2D blueSubstance = Physics2D.OverlapArea(this.GetComponent<Collider2D>().bounds.min, this.GetComponent<Collider2D>().bounds.max, 1 << Layers.BlueSubstance);
        Physics2D.queriesHitTriggers = false;

        if (blueSubstance && blueSubstance.transform.IsChildOf(this.transform))
        {
            if (blueSubstance.gameObject.GetComponent<BlueSubstancedElement>())
            {
                blueSubstance.gameObject.GetComponent<BlueSubstancedElement>().Collide(gameObject);
            }
        }
        else
        {
            if (theChosen)
            {
                FindObjectOfType<PowerGlove>().lastPowerGloved = null;
            }
            
            this.events.triggerEvent(EnemyEvents.EventName.PowerGlovedCrashed);

            this.Die();
        }
    }

    /// <summary>
    /// FunciÃ³ heretada del IPowerGloveable que es llenÃ§a quan l'enemic es destruit per temps
    /// </summary>
    public void DestroyByTime()
    {
        if (theChosen)
        {
            FindObjectOfType<PowerGlove>().lastPowerGloved = null;
        }

        if (_meshObject.activeInHierarchy)
        {
            this.events.triggerEvent(EnemyEvents.EventName.PowerGlovedLost);

            this.Die();
        }
    }

    /// <summary>
    /// Hem sigut tocats per un dispar
    /// </summary>
    public virtual void OnTouchedByProjectile(Projectile by)
    {
        if(alive)
        {
            Die();
        }
    }

    /// <summary>
    /// El player ens ha saltat a sobre
    /// </summary>
    public virtual void JumpedOver()
    {
        if (killable && alive)
        {
            Die();
        }
    }

    /// <summary>
    /// Dues plataformes ens han aixafat
    /// </summary>
    public virtual void Crushed()
    {
        if(type != EnemiesManager.Enemies.Spike)
        {
            Die();
        }
    }
}
