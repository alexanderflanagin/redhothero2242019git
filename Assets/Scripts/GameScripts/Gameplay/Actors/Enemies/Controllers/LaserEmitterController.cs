using UnityEngine;
using System.Collections;

public class LaserEmitterController : EnemyController
{
    /// <summary>
    /// Layers amb les que col.lisiona el laser (sigui perque danya o sigui perque el paren)
    /// </summary>
    public LayerMask collideWith;

    /// <summary>
    /// Layers a les que mata el laser. De moment nomes el player
    /// </summary>
    public LayerMask kills;

    /// <summary>
    /// El line renderer del laser
    /// </summary>
    public LineRenderer lineRenderer;

    /// <summary>
    /// Temps que es invulnerable el player a aquest laser un cop li ha fet mal
    /// </summary>
    public float onDamageInvulnerableTime = 1f;

    /// <summary>
    /// Rotacio del laser, donada per la seva factory LaserPath
    /// </summary>
    public Transform rotation;

    /// <summary>
    /// Distancia maxima del laser
    /// </summary>
    protected float distance = 200f;

    /// <summary>
    /// Ens indica si rota o no el Laser Emitter
    /// </summary>
    public bool rotating;

    /// <summary>
    /// Particuls que apareixen quan el laser toca alguna cosa
    /// </summary>
    public GameObject collisionParticles;

    /// <summary>
    /// Ens diu si estem fent mal al player de manera continuada (i per tant només l'afecta la primera vegada)
    /// </summary>
    private bool damaging;

    /// <summary>
    /// Temps al que ha tocat al player
    /// </summary>
    private float damageTime;

    /// <summary>
    /// Al habilitar, fem el que faci el pare i iniciem la coroutina /Laser/
    /// </summary>
    public override void OnEnable()
    {
        base.OnEnable();

        StandardStageManager.current.playerControl.events.registerEvent(CharacterEvents.EventName.TravelEnter, RemovePlayerFromLayers);
        StandardStageManager.current.playerControl.events.registerEvent(CharacterEvents.EventName.TravelExit, AddPlayerToLayers);

        StartCoroutine(Laser());
    }

    public override void OnDisable()
    {
        base.OnDisable();
        StandardStageManager.current.playerControl.events.unregisterEvent(CharacterEvents.EventName.TravelEnter, RemovePlayerFromLayers);
        StandardStageManager.current.playerControl.events.unregisterEvent(CharacterEvents.EventName.TravelExit, AddPlayerToLayers);

    }

    /// <summary>
    /// Treu la layer del Player del collideWith per quan esta en travel
    /// </summary>
    void RemovePlayerFromLayers()
    {
        collideWith = Layers.removeLayer(collideWith, Layers.Player);
    }

    /// <summary>
    /// Afegeix la layer del player al collideWith per quan acaba un travel
    /// </summary>
    void AddPlayerToLayers()
    {
        collideWith = Layers.addLayer(collideWith, Layers.Player);
    }

    protected virtual IEnumerator Laser()
    {
        // Array de resultats de hits al fer el line cast (la farem de 1 de llarg)
        RaycastHit2D[] hitResults = new RaycastHit2D[1];

        // Si el darrer frame havia col.lisionat, per a tornar el raig a la seva distancia maxima
        bool lastFrameCollide = false;

        // El punt final del raig (on xoca per darrer cop)
        Vector3 endPoint;
        
        while(this.rotation == null)
        {
            yield return null;
        }
        
        lineRenderer.SetPosition(1, -this.rotation.right * this.distance);

        while(true)
        {
            Physics2D.queriesHitTriggers = true;
            if (Physics2D.LinecastNonAlloc(this.transform.position, this.transform.position - this.rotation.right * distance, hitResults, this.collideWith) > 0)
            {
                endPoint = hitResults[0].point;

                // Posem les particules de col.lisio
                collisionParticles.transform.position = endPoint;

                // Debug.Log(endPoint, this.gameObject);

                lineRenderer.SetPosition(1, endPoint - this.transform.position);

                lastFrameCollide = true;

                // Si apart de col.lisionar, ha de fer mal
                if(Layers.inMask(this.kills, hitResults[0].transform.gameObject.layer))
                {
                    CharacterControl control = hitResults[0].transform.GetComponent<CharacterControl>();

                    if (control != null)
                    {
                        // Es el primer cop que toquem al player
                        if (!damaging)
                        {
                            control.RecieveDamage(this.gameObject);
                            damageTime = Time.time;
                        }

                        damaging = true;
                    }
                    else
                    {
                        EnemyController controller = hitResults[0].transform.GetComponent<EnemyController>();
                        if (controller != null && controller.killable)
                        {
                            controller.Die();
                        }
                    }
                }
                else
                {
                    damaging = false;
                }
            }
            else
            {
                damaging = false;

                // Fem que no es vegin les particules
                collisionParticles.transform.position = Vector3.right * 100;

                if (lastFrameCollide)
                {
                    lastFrameCollide = false;
                    lineRenderer.SetPosition(1, -this.rotation.right * this.distance);
                }
                else if (this.rotating)
                {
                    lineRenderer.SetPosition(1, -this.rotation.right * this.distance);
                }
            }
            
            Physics2D.queriesHitTriggers = false;

            damaging = damaging && damageTime + onDamageInvulnerableTime > Time.time;

            yield return null;
        }
    }
}

	
