using UnityEngine;
using System.Collections;

public class StvController : EnemyController
{
    /// <summary>
    /// Fem l'S-tv de tipus s-tv
    /// </summary>
    protected override void Awake()
    {
        // Afegim aixo primer perque son atributs
        _startExplosion = transform.GetOrAddComponent<InstantExplosion>();
        _endExplosion = transform.GetOrAddComponent<EndExplosion>();

        // Cridem l'awake del pare
        base.Awake();

        // Indiquem que el tipus d'enemic es un S-Tv
        type = EnemiesManager.Enemies.S_Tv;
    }

    /// <summary>
    /// Quan es torna visible, avisem al canó de que està activat
    /// </summary>
    protected void OnBecameVisible()
    {
        events.triggerEvent(EnemyEvents.EventName.EnableCannon);
    }

    /// <summary>
    /// Quan es torna invisible, desactivem el canó
    /// </summary>
    protected void OnBecameInvisible()
    {
        events.triggerEvent(EnemyEvents.EventName.DisableCannon);
    }

    /// <summary>
    /// Override del Rotate per a girar la mesh de la manera correcta per a l'S-tv
    /// </summary>
    public override void OnRotate(GameObject enemy)
    {
        _direction *= -1;

        if (_direction == -1)
        {
            _meshObject.transform.eulerAngles = new Vector2(0f, 0f);
        }
        else
        {
            _meshObject.transform.eulerAngles = new Vector2(transform.localRotation.x, transform.localRotation.y - 60f);
        }
    }

    /// <summary>
    /// Cridem els esdeveniments que tingui per defecte la classe EnemyController i a més cridem el d'explotar
    /// </summary>
    public override void Die()
    {
        base.Die();
        
        events.triggerEvent(EnemyEvents.EventName.Explode);
    }
}
