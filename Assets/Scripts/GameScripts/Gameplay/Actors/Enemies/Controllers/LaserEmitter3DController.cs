using UnityEngine;
using System.Collections;

public class LaserEmitter3DController : LaserEmitterController
{
    public float meshZPosition = -6f;

    public float lineRendererNearZ = -10f;

    public Vector3 lineRendererStartPoint = new Vector3(0.05f, 0);

    protected Vector3 rayStartModifier = new Vector3(-0.1f, -0.1f);

    protected Vector3 rayEndModifier = new Vector3(0.1f, 0.1f);

    protected override void Awake()
    {
        base.Awake();

        this._meshObject.transform.position = this._meshObject.transform.position - new Vector3(0, 0, this.meshZPosition);

        this.lineRendererStartPoint.z = -this.meshZPosition;
        this.lineRenderer.SetPosition(0, this.lineRendererStartPoint);

        this.lineRendererStartPoint.z = this.lineRendererNearZ;
        this.lineRenderer.SetPosition(1, this.lineRendererStartPoint);
    }

    /// <summary>
    /// Override of the Laser coroutine. We don't need to rotate, and the line render is drawed in the Z
    /// </summary>
    /// <returns></returns>
    protected override IEnumerator Laser()
    {
        // Array de resultats de hits al fer el line cast (la farem de 1 de llarg)
        RaycastHit2D[] hitResults = new RaycastHit2D[1];

        bool startInColliders;

        // Si el darrer frame havia col�lisionat, per a tornar el raig a la seva dist�ncia m�xima
        bool lastFrameCollide = false;

        while (true)
        {
            Debug.DrawLine(this.transform.position + this.rayStartModifier, this.transform.position + this.rayEndModifier, Color.magenta);

            // Test if we have a collision
            startInColliders = Physics2D.queriesStartInColliders;
            Physics2D.queriesStartInColliders = true;
            if (Physics2D.LinecastNonAlloc(this.transform.position + this.rayStartModifier, this.transform.position + this.rayEndModifier, hitResults, this.collideWith) > 0)
            {
                this.lineRendererStartPoint.z = 1;
                this.lineRenderer.SetPosition(1, this.lineRendererStartPoint);

                lastFrameCollide = true;
                
                // Si apart de col·lisionar, ha de fer mal
                if (Layers.inMask(this.kills, hitResults[0].transform.gameObject.layer))
                {
                    CharacterControl control = hitResults[0].transform.GetComponent<CharacterControl>();

                    if (control != null)
                    {
                        if (!Physics2D.GetIgnoreCollision(this.GetComponent<Collider2D>(), control.GetComponent<Collider2D>()))
                        {
                            // Mirem si est� ignorant les col�lisions amb el laser. Com que no les podem ignorar del raig, fem servir el pare per a saber-ho
                            control.RecieveDamage(this.gameObject);
                        }
                    }
                }
            }
            else if (lastFrameCollide)
            {
                this.lineRendererStartPoint.z = this.lineRendererNearZ;
                this.lineRenderer.SetPosition(1, this.lineRendererStartPoint);

                lastFrameCollide = false;
            }
            Physics2D.queriesStartInColliders = startInColliders;

            // Test if we need to "stop" the line renderer


            yield return null;
        }
    }
}
