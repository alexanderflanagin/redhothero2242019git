using UnityEngine;
using System.Collections;

public class SquakatooController : EnemyController
{
    /// <summary>
    /// Fem l'S-tv de tipus s-tv
    /// </summary>
    protected override void Awake()
    {
        base.Awake();
        type = EnemiesManager.Enemies.Squakatoo;

        //_startExplosion = transform.GetOrAddComponent<InstantExplosion>();

        //_endExplosion = transform.GetOrAddComponent<EndExplosion>();
    }

    /// <summary>
    /// El bird no rota
    /// </summary>
    public override void OnRotate(GameObject enemy)
    {
        _meshObject.transform.Rotate(new Vector3(0, 180, 0));
    }

    /// <summary>
    /// Cridem els esdeveniments que tingui per defecte la classe EnemyController i a més cridem el d'explotar
    /// </summary>
    public override void Die()
    {
        base.Die();
        //events.triggerEvent(EnemyEvents.EventName.Explode);

        StartCoroutine("Fall");
    }

    private IEnumerator Fall()
    {
        //float ySpeed = -4f;

        //float y;

        transform.position = new Vector3(transform.position.x, transform.position.y, 2);

        GetComponent<Collider2D>().enabled = false;

        for (int i = 1; i < 10; i++)
        {
            transform.position += new Vector3(0, 10 / i * Time.deltaTime, 0);
            yield return null;
        }

        while (transform.position.y > -20)
        {
            transform.position -= new Vector3(0, 5f * Time.deltaTime, 0);
            yield return null;
        }

        events.triggerEvent(EnemyEvents.EventName.Hide);
    }
}
