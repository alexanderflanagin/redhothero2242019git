using UnityEngine;
using System.Collections;

public class LaserEmitterBrokenController : LaserEmitterController
{
    /// <summary>
    /// Temps que esta ences el laser (visualment)
    /// </summary>
    public float onTime = 3f;

    /// <summary>
    /// Temps que esta apagat el laser (visualment)
    /// </summary>
    public float offTime = 3f;

    /// <summary>
    /// Sequencia de parpalleig del laser quan s'apaga. La sequencia sera temps apagat, temps ences, temps apagat, etc.
    /// </summary>
    public float[] blinkOffSequence = new float[] { 0.04f, 0.09f, 0.11f, 0.06f };

    /// <summary>
    /// Sequencia de parpalleig del laser quan s'engega. La sequencia sera temps ences, temps apagat, temps ences, etc.
    /// </summary>
    public float[] blinkOnSequence = new float[] { 0.06f, 0.11f, 0.09f, 0.04f };

    /// <summary>
    /// Per a guardar la layerMask original que mata
    /// </summary>
    protected LayerMask originalKillLayerMask;

	// Use this for initialization
    public override void OnEnable()
    {
        base.OnEnable();

        this.originalKillLayerMask = this.kills;

        StartCoroutine(Blink());
    }
	
    /// <summary>
    /// Engega i apaga el laser /onTime/ i /offTime/ seguint les sequencies de /blinkOffSequence/ i /blinkOnSequence/
    /// </summary>
    /// <returns></returns>
	protected IEnumerator Blink()
    {
        while(true)
        {
            yield return new WaitForSeconds(onTime);

            this.kills = 0;

            for (int i = 0; i < blinkOffSequence.Length; i++)
            {
                this.lineRenderer.enabled = !this.lineRenderer.enabled;
                collisionParticles.SetActive(lineRenderer.enabled);
                yield return new WaitForSeconds(blinkOffSequence[i]);
            }

            this.lineRenderer.enabled = false;
            collisionParticles.SetActive(false);

            yield return new WaitForSeconds(offTime);

            for (int i = 0; i < blinkOnSequence.Length; i++)
            {
                this.lineRenderer.enabled = !this.lineRenderer.enabled;
                collisionParticles.SetActive(lineRenderer.enabled);
                yield return new WaitForSeconds(blinkOnSequence[i]);
            }

            this.lineRenderer.enabled = true;
            this.kills = this.originalKillLayerMask;

            collisionParticles.SetActive(true);
        }
    }
}
