using UnityEngine;
using System.Collections;

/// <summary>
/// Controller per als enemics walkers que disparen quan veuen al Player
/// </summary>
[RequireComponent(typeof(Shooter))]
[RequireComponent(typeof(Walker))]
[RequireComponent(typeof(Seer))]
public class EmitterController : StvController
{
    /// <summary>
    /// Basic movement
    /// </summary>
    public Walker walker;

    /// <summary>
    /// Ability to see the player
    /// </summary>
    public Seer seer;

    /// <summary>
    /// Ability to shoot
    /// </summary>
    public Shooter shooter;

    /// <summary>
    /// Register /type/ for statistical purposes
    /// </summary>
    protected override void Awake()
    {
        base.Awake();

        DebugUtils.AssertError(walker, "Walker script missing", gameObject);
        DebugUtils.AssertError(seer, "Seer script missing", gameObject);
        DebugUtils.AssertError(shooter, "Shooter script missing", gameObject);

        this.type = EnemiesManager.Enemies.S_TvEmitter;
    }

    /// <summary>
    /// Saves references to enemie's abilities and prepare delegates for when he sees and stop see the Player
    /// </summary>
    public override void OnEnable()
    {
        base.OnEnable();

        this.seer.onSee += OnSeePlayer;
        this.seer.onStopSeing += OnStopSeingPlayer;
    }

    /// <summary>
    /// Override of the /OnRotate/ method to rotate right the mesh
    /// </summary>
    /// <param name="enemy"></param>
    public override void OnRotate(GameObject enemy)
    {
        base.OnRotate(enemy);

        if (_direction == -1)
        {
            _meshObject.transform.eulerAngles = new Vector2(0f, 0f);
        }
        else
        {
            _meshObject.transform.eulerAngles = new Vector2(0, 180f);
        }

        Vector3 cannonPosition;
        for (int i = 0; i < shooter.cannonsObjects.Length; i++)
        {
            cannonPosition = shooter.cannonsObjects[i].transform.localPosition;
            cannonPosition.x *= -1;
            shooter.cannonsObjects[i].transform.localPosition = cannonPosition;
        }
    }

    /// <summary>
    ///  When it sees the player stops and start shooting
    /// </summary>
    protected void OnSeePlayer()
    {
        this.walker.EnterIdle();

        this.shooter.StartShotting();
    }

    /// <summary>
    /// When it no longer sees the player stop shooting and start walking again
    /// </summary>
    protected void OnStopSeingPlayer()
    {
        this.shooter.EndShotting();

        StartCoroutine(ReturnToWalk());
    }

    /// <summary>
    /// Coroutine to wait until the projectile is shooted before to walk again
    /// </summary>
    /// <returns></returns>
    protected IEnumerator ReturnToWalk()
    {
        while (this.shooter.IsCurrentlyShooting())
        {
            yield return null;
        }

        this.walker.EnterWalk();
    }
}
