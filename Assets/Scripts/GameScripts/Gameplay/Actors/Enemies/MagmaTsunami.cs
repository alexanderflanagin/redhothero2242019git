using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using Com.LuisPedroFonseca.ProCamera2D;

/// <summary>
/// Gestiona el tsunami de magma
/// </summary>
public class MagmaTsunami : MonoBehaviour {

    /// <summary>
    /// Estats del MagmaTsunami. Movent-se o parat
    /// </summary>
    public enum MagmaTsunamiState { Stopped, Moving };

    /// <summary>
    /// El GameObject que cont� el pla que crea l'efecte de distorsi� per calor que aplicarem a la c�mera
    /// </summary>
    public GameObject heatDistortionPlane;

    /// <summary>
    /// El mesh renderer del pla de distorsi�, per a modificar-li el material
    /// </summary>
    private MeshRenderer heatDistortionMesh;

    private MeshRenderer heatColorMesh;

    private Color heatColor;

    /// <summary>
    /// Factor de multiplicaci� al c�lcul segons dist�ncia per a la transpar�ncia
    /// </summary>
    public float materialTintMultiplier = 0.5f;

    public float verticalOffsetSpeed = 10f;

    private Vector2 verticalOffset = Vector2.zero;

    /// <summary>
    /// Per a saber si s'est� movent o no
    /// </summary>
	public bool moving { get; set; }

    /// <summary>
    /// Velocitat de moviment del Magma Tsunami
    /// </summary>
	public float speed = 9f;

    /// <summary>
    /// Mutiplicaci� de l'efecte de shake segons la dist�ncia del tsunami amb el player
    /// </summary>
    public float cameraShakeMultiplier = 16f;

    /// <summary>
    /// Base sobre la que fem el c�mera shake, per a allargar la corva. 0 m�s curta, 1 m�s llarga
    /// </summary>
    [Range(0,1)]
    public float cameraShakeBase = 0.5f;

    /// <summary>
    /// Dist�ncia entre el tsunami i el player
    /// </summary>
    private float distance;

    /// <summary>
    /// Offset que tenim calculat entre el centre del tsunami i la part de davant
    /// </summary>
    private float distanceOffset;

    /// <summary>
    /// Offset de la posició del Magma Tsunami si es comença des del check point
    /// </summary>
    private Vector3 checkpointStartOffset = new Vector3(-30, 0, 0);

    void OnEnable()
    {
        StandardStageManager.current.playerControl.events.registerEvent(CharacterEvents.EventName.DieEnter, OnPlayerDie);
        StandardStageManager.current.StageFinished += OnPlayerDie;

        if (GameManager.Instance.checkPointProperties != null && GameManager.Instance.checkPointProperties.currentStage == SceneManager.GetActiveScene().buildIndex && (GameManager.Instance.checkPointProperties.startInCheckPoint || GameManager.Instance.checkPointProperties.alreadyLoadedFromCheckPoint))
        {
            // Comencem des del checkpoint
            Transform checkPoint = FindObjectOfType<CheckPoint>().transform;

            this.transform.position = checkPoint.position + this.checkpointStartOffset;
        }
    }

    void OnDisable()
    {
        StandardStageManager.current.playerControl.events.unregisterEvent(CharacterEvents.EventName.DieEnter, OnPlayerDie);
        StandardStageManager.current.StageFinished -= OnPlayerDie;
    }

    void OnPlayerDie()
    {
        moving = false;
    }

	// Use this for initialization
	void Start () {
		MagmaTsunamiSwitch( MagmaTsunamiState.Stopped );

#if UNITY_EDITOR
        if (GameManager.Instance.gameData.gameData.lavaMove)
        {
            this.moving = true;
        }
#else
        this.moving = true;
#endif

        this.distanceOffset = 10f;

        if (this.heatDistortionPlane)
        {
            this.heatDistortionPlane = Instantiate(this.heatDistortionPlane) as GameObject;
            this.heatDistortionPlane.transform.SetParent(Camera.main.transform);
            this.heatDistortionPlane.transform.localPosition = new Vector3(0, 0, 3);
            this.heatDistortionPlane.transform.localScale = new Vector3(8, 8, 1);

            this.heatDistortionMesh = this.heatDistortionPlane.GetComponent<MeshRenderer>();

            this.heatColorMesh = this.heatDistortionPlane.transform.Find("Color").GetComponent<MeshRenderer>();
            this.heatColor = this.heatColorMesh.material.GetColor("_TintColor");
            // this.heatDistortionColor = this.heatDistortionMesh.material.GetColor(this.materialTintPropertyName);
        }

        // Per si no estava activat ja a la c�mara
        //ProCamera2D.Instance.UseNumericBoundaries = true;
        ProCamera2D.Instance.transform.GetOrAddComponent<ProCamera2DNumericBoundaries>().UseNumericBoundaries = true;
        ProCamera2D.Instance.transform.GetOrAddComponent<ProCamera2DNumericBoundaries>().UseLeftBoundary = true;
        //ProCamera2D.Instance.UseLeftBoundary = true;
        
	}

    public void MagmaTsunamiSwitch(MagmaTsunamiState state)
    {
        switch (state)
        {
            case MagmaTsunamiState.Stopped:
                StartCoroutine("Stopped");
                break;
            case MagmaTsunamiState.Moving:
                StartCoroutine("Moving");
                break;
        }
    }

    IEnumerator Stopped()
    {
        while (!moving)
        {
            yield return null;
        }

        MagmaTsunamiSwitch(MagmaTsunamiState.Moving);
    }

    IEnumerator Moving()
    {
        float distanceModifier;

        ProCamera2DNumericBoundaries numericBoundaries = ProCamera2D.Instance.gameObject.GetComponent<ProCamera2DNumericBoundaries>();
        
        while (moving)
        {
            transform.position = new Vector3(transform.position.x + speed * Time.deltaTime, transform.position.y, transform.position.z);

            this.distance = (StandardStageManager.current.player.transform.position.x - this.transform.position.x) - this.distanceOffset;

            distanceModifier = Mathf.Pow(this.cameraShakeBase, this.distance);

            numericBoundaries.LeftBoundary = this.transform.position.x + this.distanceOffset / 2;

            ProCamera2DShake.Instance.Shake(Time.deltaTime, Vector2.one * (this.cameraShakeMultiplier * distanceModifier), smoothness: 0.15f);

            this.heatColor.a = materialTintMultiplier * distanceModifier;
            
            this.heatColorMesh.material.SetColor("_TintColor", this.heatColor);

            // Debug.Log(string.Format("Distance: {0}, Curve position: {1}", this.distance, distanceModifier), this.gameObject);

            this.verticalOffset.y += this.verticalOffsetSpeed * Time.deltaTime * distanceModifier;

            this.heatDistortionMesh.material.SetTextureOffset("_MainTex", this.verticalOffset);

            yield return null;
        }

        MagmaTsunamiSwitch(MagmaTsunamiState.Stopped);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.GetComponent<EnemyController>() != null)
        {
            other.GetComponent<EnemyController>().Die();
        }
        else if (other.GetComponent<CharacterControl>())
        {
            other.GetComponent<CharacterControl>().StartDying(true, false);
        }
    }
}
