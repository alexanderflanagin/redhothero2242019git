using UnityEngine;
using System.Collections;

/// <summary>
/// S'encarrega de deshabilitar enemics quan fa TIME_TO_DISABLE que no estan en pantalla
/// </summary>
public class TimeDisable : EnemyDisable {

    /// <summary>
    /// Temps que han d'estar fora de la pantalla per a desapar�ixer
    /// </summary>
    public static float TIME_TO_DISABLE = 2f;

    /// <summary>
    /// Al fer-se invisibles, invoquem el Disable amb un retr�s
    /// </summary>
    void OnBecameInvisible()
    {
        Invoke("Disable", TIME_TO_DISABLE);
        //Debug.Log("Became Invisible and prepare to being disabled", this.gameObject);
    }

    /// <summary>
    /// Al fer-se visibles, cancelem el Disable si havia estat cridat
    /// </summary>
    void OnBecameVisible()
    {
        CancelInvoke("Disable");
    }

    /// <summary>
    /// Desactiva el GameObject
    /// </summary>
    void Disable()
    {
        //Debug.Log("Being disabled", this.gameObject);
        this.GetComponent<EnemyController>().Disable();
    }
}
