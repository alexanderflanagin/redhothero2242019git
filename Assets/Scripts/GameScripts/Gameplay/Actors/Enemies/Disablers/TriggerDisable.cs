using UnityEngine;
using System.Collections;

/// <summary>
/// Deshabilitamos un enemigo si sale de su trigger de acción y no está en pantalla
/// NOTA: Si sale del área de efecto, aunque vuelva a entrar se deshabilitará si sale de pantalla
/// </summary>
public class TriggerDisable : EnemyDisable
{
    private GameObject _parentFactory;

    public GameObject parentFactory
    {
        set
        {
            _parentFactory = value;
        }
    }

    void OnTriggerExit2D(Collider2D trigger)
    {
        if(!_parentFactory)
        {
            Debug.Log("Deshabilitem perque no tenim Parent Factory i segurament s'està tancant l'escena");
            GetComponent<EnemyController>().Disable();
        }
        else if (trigger.gameObject == _parentFactory.GetComponentInChildren<WakeUpTrigger>().gameObject)
        {
            MeshRenderer meshRenderer = GetComponent<MeshRenderer>();
            // Surt del seu pròpi trigger
            if(!gameObject.activeInHierarchy)
            {
                GetComponent<EnemyController>().Disable();
            }
            else if (!meshRenderer.isVisible)
            {
                //Debug.Log("Not visible, so disable", this.gameObject);
                GetComponent<EnemyController>().Disable();
            }
            else
            {
                // El deshabilitem més tard
                StartCoroutine(hideWhenNotVisible(meshRenderer));
            }
        }
    }

    private IEnumerator hideWhenNotVisible(MeshRenderer meshRenderer)
    {
        while (meshRenderer.isVisible)
        {
            yield return null;
        }

        this.GetComponent<EnemyController>().Disable();
    }

}
