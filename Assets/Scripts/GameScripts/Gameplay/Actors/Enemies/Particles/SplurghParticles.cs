using UnityEngine;
using System.Collections;
/// <summary>
/// Component de Slug que s'encarrega de llençar les partícules. Les partícules forman part del prefab de moment però es canviaran a prefabs/particles quan torni a treballar amb elles
/// </summary>
public class SplurghParticles : Instantiator {
	/// <summary>
	/// El component Slug que s'encarrega dels seus moviments
	/// </summary>
	private Slug _movement;
	/// <summary>
	/// El component Physics2DController per fer checks de grounded
	/// </summary>
	private Physics2DController _controller;
	/// <summary>
	/// Sistema de particules per quan gira
	/// </summary>
	public ParticleSystem turnParticles;
	/// <summary>
	/// Sistema de particules per quan fa un pas
	/// </summary>
	public ParticleSystem splurgSprinkles;
	/// <summary>
	/// Sistema de particules que s'executa sempre que està viu
	/// </summary>
	public ParticleSystem splurgSlime;
	/// <summary>
	/// Sistema de particules per quan el Slug mor
	/// </summary>
	public ParticleSystem deathParticles;

	private GameObject _splurgParticles;

    private EnemyController _enemyController;

	/// <summary>
	/// Al habilitar el Game Object carreguem el component Slug i el Physics2DController.
	/// Si existeix trail particles i el Slug toca a terra iniciem les particules
	/// i executem la corutina que carrega els events.
	/// </summary>
	void OnEnable() {
        _controller = GetComponent<Physics2DController>();
        _enemyController = GetComponent<SplurghController>();
		_movement = GetComponent<Slug>();

        _enemyController.meshObject.SetActive(true);

		LoadFX();
		StartCoroutine (LoadEvents());
	}

    /// <summary>
    /// Carrega els events un frame més tard només si existeixen les partícules corresponents
    /// </summary>
    /// <returns>The events.</returns>
    private IEnumerator LoadEvents()
    {
        yield return null;

        if (splurgSlime)
        {
            _enemyController.events.registerEvent(EnemyEvents.EventName.Ground, OnGrounded);
        }
        if (splurgSprinkles)
        {
            _movement.StepLeft += OnStep;
            _movement.StepRight += OnStep;
        }
        if (turnParticles)
        {
            _movement.TurnEvent += OnTurn;
        }
        if ( deathParticles ) {
            _enemyController.events.registerEvent(EnemyEvents.EventName.Die, OnDeath);
        }
    }

	/// <summary>
	/// Carreguem els prefabs de particules que necessitarem fins que muntem sistema de pool.
	/// </summary>
	/// <returns>The F.</returns>
	private void LoadFX() {
		_splurgParticles = new GameObject ();
		_splurgParticles.name = "SplurgParticles @ " + this.transform.position.x.ToString("0.0" + "x");
		_splurgParticles.transform.position = this.transform.position;
		_splurgParticles.transform.parent = this.transform;

		LoadParticleSystem ( ref deathParticles, new Vector3 (0, 0.33f, 0), Vector3.zero, _splurgParticles );
		LoadParticleSystem ( ref splurgSlime, new Vector3 (0, 0, 0), Vector3.zero, _splurgParticles );
		LoadParticleSystem ( ref splurgSprinkles, new Vector3 (-0.4f, 0.33f, 0), Vector3.zero, _splurgParticles );
	}

    /// <summary>
    /// Al deshabilitar el GameObject (o el component) treiem els metodes de partícules dels events de Slug.
    /// </summary>
    void OnDisable()
    {
        if (splurgSlime)
        {
            _enemyController.events.unregisterEvent(EnemyEvents.EventName.Ground, OnGrounded);
        }
        if (splurgSprinkles)
        {
            _movement.StepLeft -= OnStep;
            _movement.StepRight -= OnStep;
        }
        if (turnParticles)
        {
            _movement.TurnEvent -= OnTurn;
        }
        if (deathParticles)
        {
            _enemyController.events.unregisterEvent(EnemyEvents.EventName.Die, OnDeath);
        }
    }

    public void OnGrounded(GameObject enemy)
    {
        splurgSlime.Play();
        splurgSlime.Clear();
    }

	/// <summary>
	/// Executa les particlesStep.
	/// </summary>
    public void OnStep()
    {
        splurgSprinkles.transform.localPosition = new Vector3(0.6f * _enemyController.direction, 0, 0);
        splurgSprinkles.Play();
    }

    /// <summary>
    /// Executa les turnParticles.
    /// </summary>
    public void OnTurn()
    {
        //potser necessitem variable per position ja que el slug no te el centre al mig
        float rotationY;

        if (_enemyController.direction == 1)
        {
            //Moving Right
            rotationY = 0;
        }
        else
        {
            //Moving Left
            rotationY = 90;
        }
        if (turnParticles)
        {
            turnParticles.transform.eulerAngles = new Vector2(270, rotationY);
            turnParticles.transform.localPosition = new Vector3(-0.4f * _enemyController.direction, 0, 0);
            turnParticles.Play();
        }
    }

	/// <summary>
	/// Executa les deathParticles
	/// </summary>
    public void OnDeath(GameObject enemy)
    {
        _enemyController.meshObject.SetActive(false);
        deathParticles.Play();
    }

}
