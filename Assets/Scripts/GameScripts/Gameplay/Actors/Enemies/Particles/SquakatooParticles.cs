using UnityEngine;
using System.Collections;

public class SquakatooParticles : Instantiator
{
    /// <summary>
    /// El controller de l'enemic
    /// </summary>
    private EnemyController _controller;

    /// <summary>
    /// El Game Object de l'explosió que farà al morir
    /// </summary>
    public GameObject smallExplosion;

    /// <summary>
    /// El particle system que s'ha d'habilitar al morir
    /// </summary>
    private ParticleSystem _explosionParticles;

    /// <summary>
    /// Al crear l'objecte, instanciem les partícules i les posem al seu lloc
    /// </summary>
    void Awake()
    {
        _controller = GetComponent<EnemyController>();

        if (smallExplosion != null)
        {
            //Debug.Log("Existeixen les small explosions");
            smallExplosion = Instantiate(smallExplosion) as GameObject;
            smallExplosion.transform.parent = transform;
            smallExplosion.transform.localPosition = new Vector3(0, 0.5f, 0);
            smallExplosion.transform.localEulerAngles = new Vector3(-90, 0, 0);
            _explosionParticles = smallExplosion.GetComponent<ParticleSystem>();
        }
    }

	/// <summary>
	/// Cada cop que s'habilita l'enemic, parem les particules (per si seguien en Play) i afegim l'OnExplode al seu Event
	/// </summary>
	void OnEnable ()
    {
        _explosionParticles.Stop();

        _controller.events.registerEvent(EnemyEvents.EventName.Die, OnExplode);
	}

    /// <summary>
    /// Des-registrem el OnExplode
    /// </summary>
    void OnDisable()
    {
        _controller.events.unregisterEvent(EnemyEvents.EventName.Die, OnExplode);
    }

    /// <summary>
    /// Al explotar, cridem la coroutina que s'esperarà 0.2 segons abans d'instanciar l'explosió 
    /// </summary>
    void OnExplode(GameObject enemy)
    {
        if (_explosionParticles != null)
        {
            _explosionParticles.Play();
        }
    }
}
