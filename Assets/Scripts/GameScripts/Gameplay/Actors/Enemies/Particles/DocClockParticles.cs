using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class to manage DocClock particles
/// </summary>
public class DocClockParticles : StvParticles
{
    /// <summary>
    /// Particles while countdown to explode
    /// </summary>
    public GameObject countdownParticles;

    /// <summary>
    /// Disable Countdown particles and register events
    /// </summary>
    protected override void OnEnable()
    {
        countdownParticles.SetActive(false);

        base.OnEnable();
        _controller.events.registerEvent(EnemyEvents.EventName.JumpedOver, StartCountdownParticles);
        _controller.events.registerEvent(EnemyEvents.EventName.TouchedByProjectile, StartCountdownParticles);

        _controller.events.registerEvent(EnemyEvents.EventName.Explode, StopCountdownParticles);
    }

    /// <summary>
    /// Unregister all events
    /// </summary>
    protected override void OnDisable()
    {
        base.OnDisable();
        _controller.events.unregisterEvent(EnemyEvents.EventName.JumpedOver, StartCountdownParticles);
        _controller.events.unregisterEvent(EnemyEvents.EventName.TouchedByProjectile, StartCountdownParticles);

        _controller.events.unregisterEvent(EnemyEvents.EventName.Explode, StopCountdownParticles);
    }

    /// <summary>
    /// Activate countdown particles
    /// </summary>
    /// <param name="enemy"></param>
    private void StartCountdownParticles(GameObject enemy)
    {
        countdownParticles.SetActive(true);
    }

    /// <summary>
    /// Deactivate countdownparticles
    /// </summary>
    /// <param name="enemy"></param>
    private void StopCountdownParticles(GameObject enemy)
    {
        countdownParticles.SetActive(false);
    }
}
