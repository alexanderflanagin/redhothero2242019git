using UnityEngine;

/// <summary>
/// Destroy things
/// </summary>
public class FinalBossDestroyEverything : MonoBehaviour
{
    /// <summary>
    /// Destroy all IPowerGloveables founded in the way
    /// </summary>
    /// <param name="other"></param>
    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.layer == Layers.RedHotDensity)
        {
            // Eliminem la RedHotDensity
            RedHotDensity density = other.GetComponent<RedHotDensity>();
            DebugUtils.AssertError(density != null, "Some object in RedHotDensity layer without RedHotDensity script");

            if(density)
            {
                density.gameObject.SetActive(false);
            }
        }

        IPowerGloveable iPowerGloveable = other.GetInterface<IPowerGloveable>();
        if(iPowerGloveable != null && !other.GetComponent<PowerGloved>())
        {
            iPowerGloveable.Collide(gameObject);
        }
    }	
}
