using UnityEngine;
using System.Collections;

/// <summary>
/// Classe pare d'on derivar els Cannons
/// </summary>
public abstract class Cannon : MonoBehaviour
{
    /// <summary>
    /// Si pot disparar (encara que ara no ho estigui fent)
    /// </summary>
    public bool canShoot;

    /// <summary>
    /// Si estem en la coroutina del shoot (per si volem apagar el cano un cop disparat)
    /// </summary>
    public bool isShooting;

    /// <summary>
    /// Si carrega el projectil o el llença sense carregar. Per defecte es true
    /// </summary>
    public bool doCharge = true;

    /// <summary>
    /// Per a donar-li una direccio
    /// </summary>
    /// <param name="quat">La rotacio que marca la direccio</param>
    public abstract void SetDirection(Quaternion quat);
    
    /// <summary>
    /// Per a disparar un projectil
    /// </summary>
    public abstract void Shoot();

    /// <summary>
    /// Per a cancel.lar el dispar i descartar qualsevol projectil que s'estigues creant
    /// </summary>
    public abstract void CancelShoot();
}
