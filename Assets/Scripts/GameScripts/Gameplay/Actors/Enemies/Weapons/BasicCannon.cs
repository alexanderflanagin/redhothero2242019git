using UnityEngine;
using System.Collections;

/// <summary>
/// Cano basic dels enemics
/// </summary>
public class BasicCannon : Cannon
{
    /// <summary>
    /// Posició del canó (d'on sortiran els projectils)
    /// </summary>
    public Transform cannonPosition;

    /// <summary>
    /// Name of the shot clip
    /// </summary>
    public string shotClip = "Shot";

    /// <summary>
    /// Direcció dels projectils
    /// </summary>
    private Vector3 _direction = Vector3.left;

    /// <summary>
    /// Coroutina del release projectile, per a poder-la cancel·lar
    /// </summary>
    private IEnumerator _releaseProjectile;

    /// <summary>
    /// El projectil actual, per a poder-lo parar des de la funció CancelShoot
    /// </summary>
    GameObject _currentProjectile;

	private float explosionVolume = 0f;

    void Start()
    {
        ProjectilesManager.Instance.ExecuteOnListReady(OnProjectilesListReady);
    }

    /// <summary>
    /// Add 5 projectiles to the pooler
    /// </summary>
    private void OnProjectilesListReady()
    {
        GameObjectPooler.Instance.AddProjectiles(ProjectilesManager.Projectiles.EnemyBasic, 5);
    }

	public override void SetDirection(Quaternion quat)
    {
        _direction = quat * Vector3.left;
    }

    /// <summary>
    /// Dispara un projectil. Prepara el projectil i avisa a la coroutina que s'ocuparà de deixar-lo anar
    /// </summary>
    public override void Shoot()
    {
        if(this.isShooting)
        {
            Debug.LogWarning("Li hem demanat que torni a disparar quan ja estava disparant", this.gameObject);
            return;
        }
        
        this.isShooting = true; // Comencem un dispar
        
        _currentProjectile = GameObjectPooler.Instance.GetProjectile(ProjectilesManager.Projectiles.EnemyBasic);
        EnemyBasicProjectile projectile = _currentProjectile.GetComponent<EnemyBasicProjectile>();
        _currentProjectile.transform.position = cannonPosition.position;
        // TODO La direcció haurà d'anar definida segons on mira l'objecte
        _currentProjectile.transform.forward = _direction;
        projectile.move = false;
        // _currentProjectile.GetComponent<EnemyBasicProjectile>().owner = this.transform.parent.gameObject;
        projectile.owner = this.GetComponentInParent<EnemyController>().gameObject;

        TurretController turretController;
        if (turretController = this.transform.parent.GetComponent<TurretController>())
        {
            projectile.factory = turretController.factory.gameObject;
        }
        
        _currentProjectile.SetActive(true);

        _releaseProjectile = ReleaseProjectile();

        StartCoroutine(_releaseProjectile);
    }

    /// <summary>
    /// Cancel·la un dispar i deshabilita el canó
    /// </summary>
    public override void CancelShoot()
    {
        this.canShoot = false;

        if (_releaseProjectile != null)
        {
            StopCoroutine(_releaseProjectile);
        }
        
        if (_currentProjectile)
        {
            _currentProjectile.SetActive(false);
            _currentProjectile = null;
        }

        this.isShooting = false; // Hem acabat de disparar (per obligacio o per que hem disparat)
    }

    /// <summary>
    /// S'espera 0.5 segons amb el projectil sense moure'l i després el deixa anar
    /// </summary>
    /// <param name="projectile">El projectil que es vol llençar</param>
    /// <returns></returns>
    IEnumerator ReleaseProjectile()
    {
        //Debug.Log("Començo un release projectile");
        canShoot = true;

        if(this.doCharge)
        {
            float time = Time.time;
            float releaseTime = 0.2f;
            while (canShoot && time + releaseTime > Time.time && _currentProjectile)
            {
                _currentProjectile.transform.position = cannonPosition.position;
                yield return null;
            }
        }

        if (canShoot)
        {
            if (_currentProjectile)
			{
                AudioManager.Instance.PlayOneShoot(AudioClipsData.ClipsGroups.Enemies, shotClip);
                _currentProjectile.GetComponent<EnemyBasicProjectile>().move = true;
            }
            _currentProjectile = null;
        }
        else if(_currentProjectile)
        {
            _currentProjectile.SetActive(false);
        }

        this.isShooting = false; // Hem acabat de disparar (per obligacio o per que hem disparat)
    }
}
