using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FinalBossCannon : Cannon
{
    /// <summary>
    /// Y position, relative to the Cannon, who sets the bottom position when moving
    /// </summary>
    [Tooltip("Y position, relative to the Cannon, who sets the bottom position when moving")]
    public float yBottom;

    /// <summary>
    /// Y position, relative to the Cannon, who sets the top position when moving
    /// </summary>
    [Tooltip("Y position, relative to the Cannon, who sets the top position when moving")]
    public float yTop;

    /// <summary>
    /// Cannon moving speed
    /// </summary>
    [Tooltip("Cannon moving speed")]
    [Range(2, 15)]
    public float cannonSpeed = 4f;

    /// <summary>
    /// Graphics which should move the Cannon
    /// </summary>
    [Tooltip("Graphics which should move the Cannon")]
    public Transform cannonGraphics;

    /// <summary>
    /// Projectiles shooted by this cannon. In pattern order
    /// </summary>
    [Tooltip("Projectiles shooted by this cannon. In pattern order")]
    public GameObject[] projectiles;

    /// <summary>
    /// Time between cannon stops and shoot a projectile
    /// </summary>
    [Tooltip("Time between cannon stops and shoot a projectile")]
    public float timeBeforeShoot = 0.5f;

    /// <summary>
    /// Time between cannon shoots a projectile and move again
    /// </summary>
    [Tooltip("Time between cannon shoots a projectile and move again")]
    public float timeAfterShoot = 0.5f;

    [HideInInspector]
    public ParticleSystem shootParticles;

    private WaitForSeconds timeBeforeShootWaitForSeconds;

    private WaitForSeconds timeAfterShootWaitForSeconds;

    private List<GameObject> projectilesPrefabs;

    private int counter;

    [HideInInspector]
    public bool movingCannon;

    private Vector3 bottomPosition;

    private Vector3 topPosition;

    private Vector3 middlePosition;

    private int direction;

    /// <summary>
    /// Tell us if we did a shoot at mid point this time
    /// </summary>
    private bool shootAtMid;

    private void Start()
    {
        projectilesPrefabs = new List<GameObject>();
        counter = 0;

        // De moment en posem 10 de cada, encara que no sera aixi segurament
        for (int j = 0; j < 10; j++)
        {
            for (int i = 0; i < projectiles.Length; i++)
            {
                projectilesPrefabs.Add(AddEnemy(projectiles[i]));
            }
        }

        bottomPosition = transform.position + Vector3.up * yBottom;
        topPosition = transform.position + Vector3.up * yTop;
        middlePosition = transform.position + Vector3.up * (yTop - yBottom) / 2;
        direction = 1;

        timeBeforeShootWaitForSeconds = new WaitForSeconds(timeBeforeShoot);
        timeAfterShootWaitForSeconds = new WaitForSeconds(timeAfterShoot);
    }

    private GameObject AddEnemy(GameObject enemy)
    {
        GameObject go = Instantiate(enemy) as GameObject;
        go.SetActive(false);

        return go;
    }

    public override void CancelShoot()
    {
        //throw new NotImplementedException();
    }

    public override void SetDirection(Quaternion quat)
    {
        //throw new NotImplementedException();
    }

    public override void Shoot()
    {
        StartCoroutine(ShootCoroutine());
    }

    private IEnumerator ShootCoroutine()
    {
        movingCannon = false;

        // Animation stuff
        //Material mat = cannonGraphics.GetComponent<MeshRenderer>().material;
        //Color previousColor = mat.color;
        //mat.color = Color.red;

        if(shootParticles)
        {
            shootParticles.Play();
        }

        yield return timeBeforeShootWaitForSeconds;

        //mat.color = previousColor;

        GameObject projectile = projectilesPrefabs[counter];
        EnemyController controller = projectile.GetComponent<EnemyController>();
        counter = (counter + 1) % projectilesPrefabs.Count;
        projectile.transform.position = transform.position;

        projectile.SetActive(true);

        controller.events.triggerEvent(EnemyEvents.EventName.Rotate);
        controller.Move();

        yield return timeAfterShootWaitForSeconds;

        if (shootParticles)
        {
            shootParticles.Stop();
        }

        movingCannon = true;
    }

    private void Update()
    {
        if(movingCannon)
        {
            MoveCannon();

            if(Mathf.Approximately(transform.position.y, bottomPosition.y))
            {
                Shoot();
            }
            else if (Mathf.Approximately(transform.position.y, topPosition.y))
            {
                Shoot();
            }
            else if (Mathf.Approximately(transform.position.y, middlePosition.y))
            {
                Shoot();
            }
        }
    }

    private void MoveCannon()
    {
        Vector3 position = transform.position;
        position += Vector3.up * direction * cannonSpeed * Time.deltaTime;

        if (direction == 1 && position.y > topPosition.y)
        {
            // Anem amunt i encara ja som dalt
            position.y = topPosition.y;
            direction = -1;
            shootAtMid = false;
        }
        else if (direction == -1 && transform.position.y < bottomPosition.y)
        {
            // Anem avall i encara ja som baix
            position.y = bottomPosition.y;
            direction = 1;
            shootAtMid = false;
        }
        else if (!shootAtMid && ((direction == 1 && transform.position.y > middlePosition.y)
            || (direction == -1 && transform.position.y < middlePosition.y)))
        {
            shootAtMid = true;
            position.y = middlePosition.y;
        }

        transform.position = position;


        cannonGraphics.position = new Vector3(cannonGraphics.position.x, transform.position.y, cannonGraphics.position.z);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;

        Vector3 bottomPosition = transform.position + Vector3.up * yBottom;
        Gizmos.DrawLine(bottomPosition + Vector3.up, bottomPosition + Vector3.down);
        Gizmos.DrawLine(bottomPosition + Vector3.left, bottomPosition + Vector3.right);

        Vector3 topPosition = transform.position + Vector3.up * yTop;
        Gizmos.DrawLine(topPosition + Vector3.up, topPosition + Vector3.down);
        Gizmos.DrawLine(topPosition + Vector3.left, topPosition + Vector3.right);
    }
}
