using UnityEngine;
using System;
using System.Collections;

public class Seer : EnemyAbility
{
    /// <summary>
    /// Quina es la posicio des d'on comencem a mirar si hi veiem
    /// </summary>
    public Transform eyes;

    /// <summary>
    /// La distancia a la que hi veiem
    /// </summary>
    public float searchDistance = 10f;

    /// <summary>
    /// Accio que s'executa quan veiem alguna cosa
    /// </summary>
    public Action onSee;

    /// <summary>
    /// Accio que s'executa cada frame que estem veient alguna cosa
    /// </summary>
    public Action onSeing;

    /// <summary>
    /// Accio que s'executa quan deixem de veure alguna cosa
    /// </summary>
    public Action onStopSeing;

    /// <summary>
    /// Amb quines layers interactuem que veiem alguna cosa
    /// </summary>
    public LayerMask seeLayer;

    /// <summary>
    /// Per saber si ja estavem veient res o no
    /// </summary>
    protected bool seing;

    /// <summary>
    /// Per saber si estem buscant ara mateix
    /// </summary>
    protected bool searching;

    /// <summary>
    /// Al habilitar, preparem els results del raycast i comencem a buscar
    /// </summary>
    public override void Enable()
    {
        StartCoroutine(search());

        controller.events.registerEvent(EnemyEvents.EventName.PowerGloved, (GameObject enemy) => { Disable(); });
    }

    /// <summary>
    /// Al deshabilitar, deixem de buscar
    /// </summary>
    public override void Disable()
    {
        this.searching = false;
    }

    /// <summary>
    /// Cada frame, mirem si tenim algun objecte a la/les layer/s /viewLayer/ i si es aixi, executem el /onView/
    /// </summary>
    /// <returns></returns>
    protected IEnumerator search()
    {
        this.seing = false;

        this.searching = true;

        // Hi guardem els resultats del line cast, per a no crear l'espai de memoria cada vegada
        RaycastHit2D[] raycastResult = new RaycastHit2D[1];

        while (this.searching)
        {
            Debug.DrawLine(this.eyes.position, this.eyes.position + new Vector3(this.searchDistance * this.controller.direction, 0, 0));
            
            if(Physics2D.LinecastNonAlloc(this.eyes.position, this.eyes.position + new Vector3(this.searchDistance * this.controller.direction, 0, 0), raycastResult, this.seeLayer) > 0)
            {
                // Hem vist alguna cosa
                if (!seing && this.onSee != null)
                {
                    this.onSee();
                    seing = true;
                }

                if(this.onSeing != null)
                {
                    this.onSeing();
                }

                seing = true;
            }
            else if(seing)
            {
                seing = false;

                if(this.onStopSeing != null)
                {
                    this.onStopSeing();
                }
            }

            yield return null;
        }
    }
}
