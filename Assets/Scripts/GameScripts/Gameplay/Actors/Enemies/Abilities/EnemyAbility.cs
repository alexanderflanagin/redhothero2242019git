using UnityEngine;
using System.Collections;

/// <summary>
/// Classe base que fare servir per a generar les habilitats. La creo perque al fer arbres de classes es vegi mes clar
/// </summary>
public abstract class EnemyAbility : MonoBehaviour
{
    /// <summary>
    /// Controller de l'enemic
    /// </summary>
    protected EnemyController controller;

    /// <summary>
    /// Al preparar l'habilitat, li assignem el seu EnemyController
    /// </summary>
    /// <param name="controller">L'EnemyController del seu propietari</param>
    public virtual void Prepare(EnemyController controller)
    {
        this.controller = controller;
    }

    /// <summary>
    /// Que fara aquesta habilitat al ser habilitat
    /// </summary>
    public abstract void Enable();

    /// <summary>
    /// Que fara aquesta habilitat al ser deshabilitat
    /// </summary>
    public abstract void Disable();
}
