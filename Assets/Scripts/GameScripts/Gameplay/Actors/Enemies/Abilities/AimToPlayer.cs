using UnityEngine;
using System.Collections;

public class AimToPlayer : AimDirection
{
    public Transform aimGameObject;
    
    /// <summary>
    /// Temps que s'espera abans de comen�ar a apuntar al generar-se
    /// </summary>
    protected float preAimWait = 0.5f;

    /// <summary>
    /// Rotation speed in degrees per second
    /// </summary>
    protected float rotationSpeed = 30f;

    /// <summary>
    /// Maxim angle que gira per segon
    /// </summary>
    protected float maxAnglePerSecond = 180f;

    /// <summary>
    /// Target al que esta apuntant
    /// </summary>
    protected Transform target;

    /// <summary>
    /// Per saber si pot rotar. Para quan est� carregant un dispar
    /// </summary>
    protected bool canAim = true;

    public override void Enable()
    {
        this.target = StandardStageManager.current.player.transform;

        this.canAim = true;

        StartCoroutine(Aim(true));

        // No te perque ser un cano, pot ser un objecte que simplement apunti al player
        ShootPattern shootPattern = this.GetComponent<ShootPattern>();
        if(shootPattern)
        {
            shootPattern.preShoot += OnPreShoot;
            shootPattern.postShoot += OnPostShoot;
        }

        controller.events.registerEvent(EnemyEvents.EventName.PowerGloved, (GameObject enemy) => { canAim = false; });
    }

    public override void Disable()
    {
        this.canAim = false;

        ShootPattern shootPattern = this.GetComponent<ShootPattern>();
        if (shootPattern)
        {
            shootPattern.preShoot -= OnPreShoot;
            shootPattern.postShoot -= OnPostShoot;
        }
    }

    public override Quaternion GetDirection()
    {
        return this.transform.rotation;
    }

    protected void OnPreShoot()
    {
        this.canAim = false;
    }

    protected void OnPostShoot()
    {
        this.canAim = true;
        StartCoroutine(Aim(false));
    }

    /// <summary>
    /// Per sempre, si pot rotar inenta rotar enfocant al player a la velocitat /rotationSpeed/ amb un angle m�xim de /maxAnglePerSecond/. Tamb� actualitza la rotaci� del can�
    /// </summary>
    /// <param name="preAim">Si ha d'esperar abans de començar a apuntar</param>
    /// <returns></returns>
    protected IEnumerator Aim(bool preAim)
    {
        if(preAim)
        {
            yield return new WaitForSeconds(this.preAimWait);
        }

        while (this.canAim)
        {
            Vector2 targetPosition = (Vector2)this.target.position + this.target.GetComponent<BoxCollider2D>().offset;
            Vector3 targetDirection = (Vector3)(targetPosition - (Vector2)this.transform.position).normalized;
            Vector3 currentForward = -this.transform.right;
            Vector3 newForward = Vector3.Slerp(currentForward, targetDirection, Time.deltaTime * this.rotationSpeed);
            newForward = new Vector3(newForward.x, newForward.y, 0);

            float angle = Vector3.Angle(currentForward, newForward);
            float maxAngle = maxAnglePerSecond * Time.deltaTime;
            if (angle > maxAngle)
            {
                // it's rotating too fast, clamp the vector
                newForward = Vector3.Slerp(currentForward, newForward, maxAngle / angle);
            }

            // assign the new forward to the transform
            this.transform.right = -newForward;

            if (this.aimGameObject != null)
            {
                this.aimGameObject.rotation = this.transform.rotation;
            }
            
            yield return null;
        }
    }
}
