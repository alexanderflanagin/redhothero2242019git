using UnityEngine;
using System.Collections;

/// <summary>
/// Abstract class from which to inherit all the aim classes
/// </summary>
[DisallowMultipleComponent]
public abstract class AimDirection : EnemyAbility
{
    public abstract override void Enable();

    public abstract override void Disable();

    /// <summary>
    /// Gets the direction at which the Shooter is aiming
    /// </summary>
    /// <returns>The direction in Quaternion form</returns>
    public abstract Quaternion GetDirection();
}
