using System.Collections;
using UnityEngine;

public class FinalBossShootPattern : ShootPattern
{
    /// <summary>
    /// Temps que passa entre trets d'una mateixa rafaga
    /// </summary>
    private WaitForSeconds shortTimeBetweenShoots = new WaitForSeconds(0.2f);

    /// <summary>
    /// Numero de trets en una rafaga
    /// </summary>
    private int numShoots = 1;

    /// <summary>
    /// Temps que passa entre rafagues
    /// </summary>
    private WaitForSeconds longTimeBetweenShoots = new WaitForSeconds(2f);

    public override void Disable()
    {
        //throw new NotImplementedException();
    }

    public override void Enable()
    {
        //throw new NotImplementedException();
    }

    public override void EndShooting()
    {
        //throw new NotImplementedException();
    }

    public override void ForceEndShooting()
    {
        //throw new NotImplementedException();
    }

    /// <summary>
    /// Iniciem coroutina de dispar (que no parara fins que mori)
    /// </summary>
    public override void StartShooting()
    {
        //throw new NotImplementedException();
        canShoot = true;
        StartCoroutine(Shooting());
    }

    /// <summary>
    /// Espera temps entre rafagues, fa rafaga, torna a esperar, etc.
    /// </summary>
    /// <returns></returns>
    private IEnumerator Shooting()
    {
        yield return longTimeBetweenShoots;

        while (canShoot)
        {
            for(int i = 0; i < numShoots; i++)
            {
                yield return shortTimeBetweenShoots;
                if(!canShoot)
                {
                    yield break;
                }

                if (preShoot != null)
                {
                    preShoot();
                }

                cannon.SetDirection(aimDirection.GetDirection());
                cannon.Shoot();                

                if(postShoot != null)
                {
                    postShoot();
                }
            }

            yield return longTimeBetweenShoots;
        }
    }
}
