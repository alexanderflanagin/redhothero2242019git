using UnityEngine;
using System.Collections;

public class AimStraight : AimDirection
{
    /// <summary>
    /// Transform d'un game object que ens indicara la direccio (normalment la fletxa groga i verda)
    /// </summary>
    public Transform director;

    /// <summary>
    /// Per saber cap a on mirava el controller el darrer cop que hem disparat. Si disparem molt sovinten la mateixa direccio, no ens cal recalcular Quaternions
    /// </summary>
    protected int lastControllerDirection = 0;

    /// <summary>
    /// Last rotation of the director. If controller direction and rotation don't change, we don't need to recalculate
    /// </summary>
    protected Quaternion lastDirectorRotation;

    /// <summary>
    /// Quaterion de la ultima direccio cap a la que hem disparat
    /// </summary>
    protected Quaternion lastDirection = Quaternion.identity;

    public override void Enable()
    {
        // Do Nothing
    }

    public override void Disable()
    {
        // Do Nothing
    }

    /// <summary>
    /// Mira si hem canviat de direccio o no, i si ho hem fet calcula, segons la nova direccio, cap a on disparem
    /// </summary>
    /// <returns>Quaternion amb la direccio cap a la que disparem</returns>
    public override Quaternion GetDirection()
    {
        if (this.lastControllerDirection != this.controller.direction || this.lastDirectorRotation != this.director.rotation)
        {
            if (this.controller.direction == -1)
            {
                this.lastDirection = this.director.rotation;
            }
            else
            {
                this.lastDirection = this.director.rotation * Quaternion.Euler(0, 180, 0);
            }

            this.lastControllerDirection = this.controller.direction;
            this.lastDirectorRotation = this.director.rotation;
        }

        return lastDirection;
    }
}
