using UnityEngine;
using System.Collections;

/// <summary>
/// Shoot the cannon every /shootTime/ when /StartShooting/ until /EndShooting/
/// </summary>
public class BasicShootPattern : ShootPattern
{
    /// <summary>
    /// Cada quanta estona ha de disparar
    /// </summary>
    public float shootTime = 1.0f;

    /// <summary>
    /// L'últim cop que s'ha disparat. Ho guardem per si s'habilita i deshabilita molt ràpid, que no dispari varis cops
    /// </summary>
    protected float lastShootTime;

    /// <summary>
    /// Coroutina per a disparar periodicament mentre vegi el player
    /// </summary>
    protected IEnumerator shooting;

    /// <summary>
    /// Coroutina de cada dispar per separat
    /// </summary>
    protected IEnumerator singleShoot;

    /// <summary>
    /// Guardem que poden disparar perque ens han habilitat
    /// </summary>
    public override void Enable()
    {
        // Ens han habilitat. Podem disparar si cal
        this.canShoot = true;

        controller.events.registerEvent(EnemyEvents.EventName.PowerGloved, (GameObject enemy) => { Disable(); });
    }

    /// <summary>
    /// Si ens deshabiliten, ja no podem disparar
    /// </summary>
    public override void Disable()
    {
        // Ens assegurarem de parar la coroutina
        this.canShoot = false;

        this.cannon.CancelShoot();

        if (singleShoot != null)
        {
            StopCoroutine(singleShoot);
            currentlyShooting = false;
        }

        if (shooting != null)
        {
            StopCoroutine(shooting);
        }
    }

    /// <summary>
    /// Comença a disparar. Guardem que /isShooting/ i iniciem la coroutina
    /// </summary>
    public override void StartShooting()
    {
        if (this.canShoot && !isShooting)
        {
            // Iniciarem la coroutina
            this.isShooting = true;

            shooting = Shooting();
            StartCoroutine(shooting);
        }
    }

    /// <summary>
    /// Avisem de que ja no estem disparant
    /// </summary>
    public override void EndShooting()
    {
        // Pararem la coroutina
        this.isShooting = false;

        if(shooting != null)
        {
            StopCoroutine(shooting);
        }
    }

    /// <summary>
    /// Forcem a que es pari el cano i tot el que esta disparant
    /// </summary>
    public override void ForceEndShooting()
    {
        this.cannon.CancelShoot();

        if (singleShoot != null)
        {
            StopCoroutine(singleShoot);
            currentlyShooting = false;
        }

        if (shooting != null)
        {
            StopCoroutine(shooting);
        }

        if (forceEndShoot != null)
        {
            forceEndShoot();
        }

        isShooting = false;
    }

    /// <summary>
    /// Mirem que poguem disparar per temps des del darrer com que hem disparat, i disparem cada /shootTime/ els /cannons/
    /// </summary>
    /// <returns></returns>
    protected IEnumerator Shooting()
    {
        WaitForSeconds singleShootTime = new WaitForSeconds(ShootPattern.PRE_POST_SHOOT_ANIMATIONS_TIME + shootTime + ShootPattern.PRE_POST_SHOOT_ANIMATIONS_TIME);

        while (this.canShoot)
        {
            singleShoot = SingleShoot();
            StartCoroutine(singleShoot);

            yield return singleShootTime;
        }
    }

    /// <summary>
    /// Per a disparar una sola vegada pero respectant els temps entre dispars, abans de disparar i despres de disparar
    /// </summary>
    /// <returns></returns>
    protected IEnumerator SingleShoot()
    {
        currentlyShooting = true;

        // Això en principi només pot passar la primera volta que entres
        while (this.lastShootTime + this.shootTime > Time.time)
        {
            yield return null;
        }

        if (this.preShoot != null)
        {
            this.preShoot();
            yield return new WaitForSeconds(ShootPattern.PRE_POST_SHOOT_ANIMATIONS_TIME);
        }

        this.cannon.SetDirection(this.aimDirection.GetDirection());
        this.cannon.Shoot();

        this.lastShootTime = Time.time;

        if (this.postShoot != null)
        {
            yield return new WaitForSeconds(ShootPattern.PRE_POST_SHOOT_ANIMATIONS_TIME);
            this.postShoot();
        }

        currentlyShooting = false;
    }
}
