using UnityEngine;
using System;

/// <summary>
/// Describes the behaviour of a shooter when it starts to shoot
/// </summary>
[DisallowMultipleComponent]
[RequireComponent(typeof(AimDirection))]
public abstract class ShootPattern : EnemyAbility
{
    /// <summary>
    /// The time before shoot and after shoot to call our animations
    /// </summary>
    public static float PRE_POST_SHOOT_ANIMATIONS_TIME = 0.1f;
    
    /// <summary>
    /// The cannon that have the shooter
    /// </summary>
    public Cannon cannon { get; set; }

    /// <summary>
    /// The direction to shoot
    /// </summary>
    public AimDirection aimDirection { get; set; }

    /// <summary>
    /// Ens diu si pot disparar (si esta habilitat)
    /// </summary>
    public bool canShoot;

    /// <summary>
    /// Si esta disparant o esperant per a tornar a disparar. Si ha entrat el /StartShooting/ i encara no l'/EndShooting/
    /// </summary>
    public bool isShooting;

    /// <summary>
    /// Ens diu si actualment esta disparant un projectil (no esperant per a tornar a disparar)
    /// </summary>
    public bool currentlyShooting = false;

    /// <summary>
    /// Action called /PRE_POST_SHOOT_ANIMATIONS_TIME/ before the shoot
    /// </summary>
    public Action preShoot;

    /// <summary>
    /// Action called /PRE_POST_SHOOT_ANIMATIONS_TIME/ after the shoot
    /// </summary>
    public Action postShoot;

    /// <summary>
    /// Action called when the pattern is forced to end, cancelling anything "being prepared"
    /// </summary>
    public Action forceEndShoot;

    public abstract override void Enable();

    public abstract override void Disable();

    /// <summary>
    /// If /canShoot/, start shooting
    /// </summary>
    public abstract void StartShooting();

    /// <summary>
    /// End shooting
    /// </summary>
    public abstract void EndShooting();

    /// <summary>
    /// Force the end of the shoots, destroying anything not shooted
    /// </summary>
    public abstract void ForceEndShooting();
}
