
/// <summary>
/// Dispara un cop quan s'activa el StartShooting
/// </summary>
public class ShootOnDemand : ShootPattern
{
    /// <summary>
    /// Ens deixa disparar, tot i que en principi no ens servira de res
    /// </summary>
    public override void Enable()
    {
        this.canShoot = true;
    }

    /// <summary>
    /// Cancel�la els dispars, tot i que en principi no ens servira de res
    /// </summary>
    public override void Disable()
    {
        this.canShoot = false;   
    }

    /// <summary>
    /// Do Nothing
    /// </summary>
    public override void EndShooting()
    {
        // Do Nothing
    }

    /// <summary>
    /// Dispara un cop tots els canons
    /// </summary>
    public override void StartShooting()
    {
        // Debug.Log("Disparo un sol cop");
        this.cannon.SetDirection(this.aimDirection.GetDirection());
        this.cannon.Shoot();
    }

    public override void ForceEndShooting()
    {
        this.cannon.CancelShoot();
    }
}
