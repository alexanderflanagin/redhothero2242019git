using UnityEngine;
using System.Collections;

/// <summary>
/// Enemic que dispara cada cert temps per� en la direcci� del player
/// </summary>
[RequireComponent(typeof(EnemyController))]
public class Aimer : NotAimer
{
    /// <summary>
    /// Temps que s'espera abans de comen�ar a apuntar al generar-se
    /// </summary>
    protected float preAimWait = 0.5f;

    /// <summary>
    /// Rotation speed in degrees per second
    /// </summary>
    protected float rotationSpeed = 60f;

    /// <summary>
    /// Maxim angle que gira per segon
    /// </summary>
    protected float maxAnglePerSecond = 360f;

    /// <summary>
    /// Target al que esta apuntant
    /// </summary>
    protected Transform target;

    /// <summary>
    /// Per saber si pot rotar. Para quan est� carregant un dispar
    /// </summary>
    protected bool canRotate = true;

    /// <summary>
    /// Parent enable, registrem que no pugui rotar on power gloved, li assignem el target, li diem que ara pot rotar i comencem a apuntar
    /// </summary>
    void OnEnable()
    {
        base.Enable();

        this.controller.events.registerEvent(EnemyEvents.EventName.PowerGloved, OnPowerGloved);

        this.target = StandardStageManager.current.player.transform;

        this.canRotate = true;

        StartCoroutine(Aim());
    }

    /// <summary>
    /// Des-registrem events
    /// </summary>
    void OnDisable()
    {
        this.controller.events.unregisterEvent(EnemyEvents.EventName.PowerGloved, OnPowerGloved);
    }

    /// <summary>
    /// Per sempre, si pot rotar inenta rotar enfocant al player a la velocitat /rotationSpeed/ amb un angle m�xim de /maxAnglePerSecond/. Tamb� actualitza la rotaci� del can�
    /// </summary>
    /// <returns></returns>
    protected IEnumerator Aim()
    {
        yield return new WaitForSeconds(this.preAimWait);

        while(true)
        {
            if (this.canRotate)
            {
                Vector2 targetPosition = (Vector2)this.target.position + this.target.GetComponent<BoxCollider2D>().offset;
                Vector3 targetDirection = (Vector3)(targetPosition - (Vector2)this.transform.position).normalized;
                Vector3 currentForward = -this.transform.right;
                Vector3 newForward = Vector3.Slerp(currentForward, targetDirection, Time.deltaTime * this.rotationSpeed);
                newForward = new Vector3(newForward.x, newForward.y, 0);

                float angle = Vector3.Angle(currentForward, newForward);
                float maxAngle = maxAnglePerSecond * Time.deltaTime;
                if (angle > maxAngle)
                {
                    // it's rotating too fast, clamp the vector
                    newForward = Vector3.Slerp(currentForward, newForward, maxAngle / angle);
                }

                // assign the new forward to the transform
                this.transform.right = -newForward;

                // this.transform.eulerAngles = new Vector3(0, 0, z);
                this._cannon.SetDirection(this.transform.rotation);
            }
            yield return null;
        }
    }

    /// <summary>
    /// Abans de disparar, cridem al base i li diem que no pot rotar
    /// </summary>
    protected override void PreShoot()
    {
        base.PreShoot();
        this.canRotate = false;
    }

    /// <summary>
    /// Despr�s de disparar, cridem al base i li diem que ja pot rotar
    /// </summary>
    protected override void PostShoot()
    {
        base.PostShoot();
        this.canRotate = true;
    }

    /// <summary>
    /// Al ser colpejat, ja no pot rotar sol
    /// </summary>
    /// <param name="enemy"></param>
    protected void OnPowerGloved(GameObject enemy)
    {
        this.canRotate = false;
    }

}
