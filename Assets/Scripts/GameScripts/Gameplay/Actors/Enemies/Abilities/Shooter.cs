using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// The enemie's hability to shoot
/// </summary>
public class Shooter : EnemyAbility
{
    /// <summary>
    /// All the cannons of the shooter
    /// </summary>
    public GameObject[] cannonsObjects;

    /// <summary>
    /// 
    /// </summary>
    [HideInInspector]
    public List<ShootPattern> shootPattern;

    /// <summary>
    /// When enable the EnemyController, we prepare the shooter
    /// </summary>
    public override void Enable()
    {
        // Prepare();
        this.controller.events.registerEvent(EnemyEvents.EventName.Die, OnDie);

        this.shootPattern = new List<ShootPattern>();

        foreach (GameObject cannonObj in this.cannonsObjects)
        {
            ShootPattern shootPattern = cannonObj.GetComponent<ShootPattern>();
            if (shootPattern == null)
            {
                Debug.LogWarning("The cannon " + cannonObj + " has no Shoot Pattern", this.gameObject);
                continue;
            }

            shootPattern.cannon = cannonObj.transform.GetOrAddComponent<Cannon>();
            shootPattern.cannon.canShoot = true; // Per si l'haviem deshabilitat
            shootPattern.aimDirection = cannonObj.transform.GetOrAddComponent<AimDirection>();

            this.shootPattern.Add(shootPattern);
        }
    }

    /// <summary>
    /// When disable the EnemyController, we force all cannons to end shooting
    /// </summary>
    public override void Disable()
    {
        ForceEndShooting();

        this.controller.events.unregisterEvent(EnemyEvents.EventName.Die, OnDie);
    }

    /// <summary>
    /// Prepare the /shootPattern/ and start shooting
    /// </summary>
    public void StartShotting()
    {
        for(int i = 0; i < this.shootPattern.Count; i++)
        {
            this.shootPattern[i].StartShooting();
        }
    }

    /// <summary>
    /// End /shootPattern/ shooting, enabling it to release any projectile that is already prepared
    /// </summary>
    public void EndShotting()
    {
        for (int i = 0; i < this.shootPattern.Count; i++)
        {
            this.shootPattern[i].EndShooting();
        }
    }

    /// <summary>
    /// Force the end shooting and remove all prepared projectiles from cannons
    /// </summary>
    public void ForceEndShooting()
    {
        for (int i = 0; i < this.shootPattern.Count; i++)
        {
            this.shootPattern[i].ForceEndShooting();
        }
    }

    /// <summary>
    /// Test if some of the cannons of the shooter is currently shooting
    /// </summary>
    /// <returns>True if some cannon shooting, false otherwise</returns>
    public bool IsCurrentlyShooting()
    {
        bool shooting = false;

        for(int i = 0; i < shootPattern.Count; i++)
        {
            shooting = shooting || shootPattern[i].currentlyShooting;
        }

        return shooting;
    }

    /// <summary>
    /// On enemy die, force the end of shooting
    /// </summary>
    /// <param name="enemy">The enemy who dies</param>
    protected void OnDie(GameObject enemy)
    {
        ForceEndShooting();
    }
}
