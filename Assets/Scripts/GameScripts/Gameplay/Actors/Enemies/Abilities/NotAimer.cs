using UnityEngine;
using System.Collections;

/// <summary>
/// Enemics que disparen cada cert temps sense apuntar
/// </summary>
[RequireComponent(typeof(EnemyController))]
public class NotAimer : EnemyAbility
{
    /// <summary>
    /// Cada quanta estona ha de disparar
    /// </summary>
    public float shootTime = 1.0f;

    /// <summary>
    /// Component Cannon per a avisar-lo quan ha de disparar
    /// </summary>
    protected Cannon _cannon;

    /// <summary>
    /// Component RendererVisibleElement que tindrà algun element per a avisar-lo de quan ha de disparar.
    /// </summary>
    protected RendererVisibleEvent _visibleEvent;

    /// <summary>
    /// Controller de l'enemic
    /// </summary>
    // protected EnemyController _controller;

    /// <summary>
    /// GameObject que ens diu la direcció cap a la que dispararem
    /// </summary>
    public GameObject director;

    /// <summary>
    /// La coroutina que s'encarrega de disparar. Per a poder acabar-la des de fora
    /// </summary>
    protected IEnumerator _shoot;

    /// <summary>
    /// L'últim cop que s'ha disparat. Ho guardem per si s'habilita i deshabilita molt ràpid, que no dispari varis cops
    /// </summary>
    protected float _lastShootTime;

    /// <summary>
    /// Registrem components i esdeveniments
    /// </summary>
    
	protected Animator _animator;

	public override void Enable()
    {
        _cannon = GetComponent<Cannon>();
		_animator = GetComponent<Animator>();

        // Prepararem per disparar. Ens subscriurem al event del renderer
        _visibleEvent = GetComponent<RendererVisibleEvent>();

        // _controller = GetComponent<EnemyController>();

        this.controller.events.registerEvent(EnemyEvents.EventName.EnableCannon, OnVisible);
        this.controller.events.registerEvent(EnemyEvents.EventName.DisableCannon, OnInvisible);
        this.controller.events.registerEvent(EnemyEvents.EventName.Explode, OnInvisible);
        this.controller.events.registerEvent(EnemyEvents.EventName.Die, OnInvisible);
        this.controller.events.registerEvent(EnemyEvents.EventName.PowerGloved, OnInvisible);
        this.controller.events.registerEvent(EnemyEvents.EventName.PowerGlovedCrashed, OnInvisible);
    }

    /// <summary>
    /// Des-registrem esdeveniments
    /// </summary>
    public override void Disable()
    {
        this.controller.events.unregisterEvent(EnemyEvents.EventName.Die, OnInvisible);
        this.controller.events.unregisterEvent(EnemyEvents.EventName.Explode, OnInvisible);
        this.controller.events.unregisterEvent(EnemyEvents.EventName.EnableCannon, OnVisible);
        this.controller.events.unregisterEvent(EnemyEvents.EventName.DisableCannon, OnInvisible);
        this.controller.events.unregisterEvent(EnemyEvents.EventName.PowerGloved, OnInvisible);
        this.controller.events.unregisterEvent(EnemyEvents.EventName.PowerGlovedCrashed, OnInvisible);
    }

    /// <summary>
    /// Al fer-se visible, engeguem la coroutina de disparar
    /// </summary>
    void OnVisible(GameObject enemy)
    {
        if (_shoot == null)
        {
            _shoot = Shoot();
        }
        else
        {
            StopCoroutine(_shoot);
        }
        
        _cannon.canShoot = false;
        StartCoroutine(_shoot);
    }

    /// <summary>
    /// Al fer-se invisible, apaguem la coroutina de disparar
    /// </summary>
    void OnInvisible(GameObject enemy)
    {
        // cannon.release = false;
        if (_shoot != null)
        {
            StopCoroutine(_shoot);
        }
        _cannon.CancelShoot();
    }

    /// <summary>
    /// Mentre està encesa, disparem i esperem els segons necessàris abans de tornar-hi
    /// </summary>
    /// <returns></returns>
    protected IEnumerator Shoot()
    {
        yield return null;

        Debug.Log("Començo a disparar");

        if (_lastShootTime + shootTime > Time.time)
        {
            yield return new WaitForSeconds(shootTime);
        }

        while (true)
        {
            // if (_cannon.prepared)
            {
                if (director != null)
                {
                    _cannon.SetDirection(director.transform.rotation);
                }
                
                PreShoot();
				yield return new WaitForSeconds(0.1f);
                _cannon.Shoot();
				yield return new WaitForSeconds(0.15f);
                PostShoot();
                _lastShootTime = Time.time;
            }

            yield return new WaitForSeconds(shootTime);

        }
    }

    protected virtual void PreShoot()
    {
        _animator.SetBool("BeakOpen", true);
    }

    protected virtual void PostShoot()
    {
        _animator.SetBool("BeakOpen", false);
    }
}
