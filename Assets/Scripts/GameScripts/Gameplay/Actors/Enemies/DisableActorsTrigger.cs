using UnityEngine;
using System.Collections;

/// <summary>
/// Script per a posar a gameobjects que continguin un trigger per a que deshabilitin all� que hi passi (player i/o enemies)
/// </summary>
public class DisableActorsTrigger : MonoBehaviour
{
    /// <summary>
    /// Si es cert, enlloc de matar als enemics els deshabilita
    /// </summary>
    [Tooltip("Si es cert, enlloc de matar als enemics els deshabilita")]
    public bool makeEnemiesDisappear = true;

    void OnTriggerEnter2D(Collider2D other)
    {
        if(Layers.inMask(Layers.Players, other.gameObject.layer))
        {
            // Es el player
            CharacterControl characterControl = other.GetComponent<CharacterControl>();
            DebugUtils.AssertWarning(characterControl != null, "Some object in Players layers but without CharacterController", this);

            if(characterControl)
            {
                characterControl.StartDying(false, false);
            }
        }
        else if (Layers.inMask(Layers.Enemies, other.gameObject.layer))
        {
            // Es un enemig
            EnemyController enemyController = other.GetComponent<EnemyController>();
            DebugUtils.AssertWarning(enemyController != null, "Some object in Enemies layers but without EnemyController", this);

            if(enemyController)
            {
                // Mirem si el fem desapareixer o el fem explotar
                if (makeEnemiesDisappear)
                {
                    enemyController.Disable();
                }
                else
                {
                    enemyController.Die();
                }
            }
        }
    }

    private void OnDrawGizmos()
    {
        BoxCollider2D collider = GetComponent<BoxCollider2D>();

        if(collider != null)
        {
            Vector3 topLeft = new Vector3(collider.bounds.min.x, collider.bounds.max.y);
            Vector3 bottomRight = new Vector3(collider.bounds.max.x, collider.bounds.min.y);

            Gizmos.color = Color.blue;
            Gizmos.DrawLine(collider.bounds.min, topLeft);
            Gizmos.DrawLine(topLeft, collider.bounds.max);
            Gizmos.DrawLine(collider.bounds.max, bottomRight);
            Gizmos.DrawLine(bottomRight, collider.bounds.min);
        }
    }
}
