using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Physics2DController))]
[RequireComponent(typeof(EnemyMove))]
public class Pacer : MonoBehaviour {

    /// <summary>
    /// El EnemyController al que notificarem que ha de rotar
    /// </summary>
    private EnemyController _enemyController;

    /// <summary>
    /// El Physics2DController del GameObject
    /// </summary>
    private Physics2DController _controller;

    /// <summary>
    /// Vector origen del raig
    /// </summary>
    private Vector2 _origin;

    /// <summary>
    /// Direcció del raig
    /// </summary>
    private Vector2 _direction;

    /// <summary>
    /// Mida del raig
    /// </summary>
    private float _distance;

    /// <summary>
    /// BoxCollider del Pacer
    /// </summary>
    private BoxCollider2D _boxCollider;

    /// <summary>
    /// Distancia a la que re-mirem que no hi hagi cap collider, per a que el forat no sigui massa petit
    /// </summary>
    private Vector2 ensureCheckDistance = new Vector3(.5f, 0);

    private bool _check;

    void Awake()
    {
        _enemyController = GetComponent<EnemyController>();

        _controller = GetComponent<Physics2DController>();

        _boxCollider = (BoxCollider2D)GetComponent<Collider2D>();
    }

    /// <summary>
    /// Carreguem el EnemyMove i el Physicis2DController i inicialitzem la direcció i la distància
    /// </summary>
    void Start()
    {
        _direction = -Vector2.up;

        _distance = 1f;
    }

    void OnEnable()
    {
        if (_enemyController == null)
        {
            _enemyController = GetComponent<EnemyController>();
        }

        _enemyController.events.registerEvent(EnemyEvents.EventName.Move, Check);
        _enemyController.events.registerEvent(EnemyEvents.EventName.Die, UnCheck);
        _enemyController.events.registerEvent(EnemyEvents.EventName.PowerGloved, UnCheck);
        _enemyController.events.registerEvent(EnemyEvents.EventName.Explode, UnCheck);
    }

    void OnDisable()
    {
        _enemyController.events.unregisterEvent(EnemyEvents.EventName.Move, Check);
        _enemyController.events.unregisterEvent(EnemyEvents.EventName.Die, UnCheck);
        _enemyController.events.unregisterEvent(EnemyEvents.EventName.PowerGloved, UnCheck);
        _enemyController.events.unregisterEvent(EnemyEvents.EventName.Explode, UnCheck);
    }

    void Check(GameObject enemy)
    {
        _check = true;
    }

    void UnCheck(GameObject enemy)
    {
        _check = false;
    }

    /// <summary>
    /// Cada frame llencem un raig i mirem si, estant a terra, hem de rotar o no l'EnemyMove
    /// </summary>
    void Update()
    {
        if (_check)
        {
            _origin = _boxCollider.offset + new Vector2(transform.position.x + (_enemyController.direction * _boxCollider.size.x / 2), transform.position.y);

            if (_controller.collisions.below && !Physics2D.Raycast(_origin, _direction, _distance, _controller.collisionMask))
            {
                // No detectem res a sota. Mirem si d'aqui poca distancia hi ha res
                if(!Physics2D.Raycast(_origin + ensureCheckDistance * _enemyController.direction, _direction, _distance, _controller.collisionMask))
                {
                    // Tampoc hi ha res mes endavant
                    _enemyController.events.triggerEvent(EnemyEvents.EventName.Rotate);
                }
            }
        }
    }

    /// <summary>
    /// Pintem una línia rosa per a mostrar on s'està comprovant
    /// </summary>
    void OnDrawGizmos()
    {
        Gizmos.color = Color.magenta;
        Gizmos.DrawRay(_origin, _direction);
    }
}
