using UnityEngine;
using System.Collections;

public class SmartJumper : EnemyMove
{

    public float largeJumpHeight = 4;

    public float smallJumpHeight = 2;

    public float movementSpeed;

    private float largeJumpDistance;

    private float smallJumpDistance;

    //private float verticalSpeed;

    private KazanArmyController kaController;

    [Range(1, 8)]
    public new int speed;

    private Vector3 arrivePoint = Vector3.zero;

    private bool jumping = false;

    void Awake()
    {
        this._controller = this.GetComponent<Physics2DController>();

        this.kaController = this.GetComponent<KazanArmyController>();
    }

    // Use this for initialization
    void Start()
    {
        //StartCoroutine(Jump());
        // this.kaController.OnRotate(this.gameObject);
        StartCoroutine(Patrol());
    }

    private IEnumerator Patrol()
    {
        while (true)
        {
            if (!this.jumping)
            {
                testIfJump();
            }

            this._controller.Move(new Vector2(speed * Time.deltaTime * this.kaController.direction, this.verticalSpeed));
            yield return null;
        }
    }

    private IEnumerator Jump()
    {
        // yield return new WaitForSeconds(0.5f);
        this.jumping = true;
        Debug.Log("JUMP MAMA");

        _verticalSpeed = this.largeJumpHeight * Time.deltaTime;
        moveArrivePoint();
        Debug.Log("He comen�at a saltar: " + Time.frameCount);
        // this._controller.Move(new Vector2(0, this.verticalSpeed), this.kaController.direction);
        yield return null;

        while (!this._controller.collisions.below)
        {

            yield return null;
            // Debug.Log("Vertical speed: " + this.verticalSpeed + " - Gravity * delta time: " + JumpProperties.GRAVITY * Time.deltaTime * Time.deltaTime);
            _verticalSpeed -= JumpProperties.GRAVITY * Time.deltaTime * Time.deltaTime;
            // this._controller.Move(new Vector2(speed * Time.deltaTime * this.kaController.direction, this.verticalSpeed), this.kaController.direction);
        }
        Debug.Log("He tocat el terra: " + Time.frameCount);
        this.jumping = false;
        // yield return new WaitForSeconds(0.5f);
        // StartCoroutine(Jump());
    }

    private void testIfJump()
    {
        float distance = (this.largeJumpHeight * Time.deltaTime / (JumpProperties.GRAVITY * Time.deltaTime * Time.deltaTime)) * this.speed * Time.deltaTime;

        RaycastHit2D ray;

        if (ray = Physics2D.Raycast(this.transform.position, Vector2.right, distance * this.kaController.direction, Layers.EnemiesCollideWith))
        {
            // Hi ha alguna cosa. Saltem
            StartCoroutine(Jump());
        }
    }

    private void moveArrivePoint()
    {
        Debug.Log("Frames saltant: " + Mathf.Ceil((this.largeJumpHeight * Time.deltaTime) / (JumpProperties.GRAVITY * Time.deltaTime * Time.deltaTime)) * 2);
        float distance = (this.largeJumpHeight * Time.deltaTime / (JumpProperties.GRAVITY * Time.deltaTime * Time.deltaTime)) * this.speed * Time.deltaTime;
        Debug.Log("Distance: " + distance);

        this.arrivePoint = this.transform.position + new Vector3(distance * this.kaController.direction, this.transform.position.y);
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(this.arrivePoint, 3f);
    }
}
