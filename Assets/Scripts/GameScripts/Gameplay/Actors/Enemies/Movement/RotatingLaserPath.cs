﻿using UnityEngine;

public class RotatingLaserPath : CircularMovingPath
{
    protected override void AddPathBehaviourAtElement(int index, GameObject element)
    {
        LaserPathElementRotating movingPathElement = element.AddComponent<LaserPathElementRotating>();
        movingPathElement.path = this;
        this.elements[index] = movingPathElement;
    }
}
