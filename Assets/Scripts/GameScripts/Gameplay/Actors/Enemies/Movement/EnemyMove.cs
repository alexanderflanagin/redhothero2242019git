using UnityEngine;

public abstract class EnemyMove : MonoBehaviour
{
    /// <summary>
    /// Enemy Controller d'aquest enemic
    /// </summary>
    protected EnemyController _enemyController;

    /// <summary>
    /// Physics controller
    /// </summary>
    protected Physics2DController _controller;

    /// <summary>
    /// Movement speed (x by default).
    /// </summary>
    protected float _speed;

    /// <summary>
    /// Getter public de la velocitat actual
    /// </summary>
    public float speed
    {
        get
        {
            return _speed;
        }
        set
        {
            _speed = value;
        }
    }

    /// <summary>
    /// Vertical speed
    /// </summary>
    protected float _verticalSpeed;

    /// <summary>
    /// Getter de la velocitat vertical
    /// </summary>
    public float verticalSpeed { get { return _verticalSpeed; } }

    /// <summary>
    /// Per si es mou per un pla que no es el de terra
    /// </summary>
    public Factory.MoveFloor moveFloor;

    /// <summary>
    /// To know where it was the past frame
    /// </summary>
    [HideInInspector]
    public Vector3 lastPosition = Vector3.zero;
}
