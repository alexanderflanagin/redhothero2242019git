using UnityEngine;
using System.Collections;

public class Follower : EnemyMove
{
    /// <summary>
    /// Target al que volem tocar
    /// </summary>
    private GameObject _target;

    /// <summary>
    /// Getter public per al target
    /// </summary>
    public GameObject target
    {
        set
        {
            _target = value;
        }
    }

    /// <summary>
    /// Temps que anem rectes abans de començar a fer follow
    /// </summary>
    private float _straightTime = 1f;

    /// <summary>
    /// Velocitat a la que podem rotar
    /// </summary>
    public float rotationSpeed = 16f;

    /// <summary>
    /// Angle maxim que podem fer cada segon
    /// </summary>
    public float maxAnglePerSecond = 90f;

    void Awake()
    {
        _speed = 4;

        _enemyController = GetComponent<EnemyController>();
    }

    void OnEnable()
    {
        _controller = GetComponent<Physics2DController>();

        _enemyController.events.registerEvent(EnemyEvents.EventName.Move, OnMoveStart);
        _enemyController.events.registerEvent(EnemyEvents.EventName.Die, OnMoveStop);
        _enemyController.events.registerEvent(EnemyEvents.EventName.PowerGloved, OnMoveStop);
    }

    void OnDisable()
    {
        _enemyController.events.unregisterEvent(EnemyEvents.EventName.Move, OnMoveStart);
        _enemyController.events.unregisterEvent(EnemyEvents.EventName.Die, OnMoveStop);
        _enemyController.events.unregisterEvent(EnemyEvents.EventName.PowerGloved, OnMoveStop);
    }

    public void OnMoveStart(GameObject enemy)
    {
        StartCoroutine(StraightMove());
    }

    public void OnMoveStop(GameObject enemy)
    {
        _speed = 0;
    }

    /// <summary>
    /// Moviment recte durant _straightTime
    /// </summary>
    /// <returns></returns>
    private IEnumerator StraightMove()
    {
        if (_target != null) // Si no té target, no li canviem la direcció
        {
            transform.LookAt(_target.transform.position);
        }

        float time = Time.time;

        while (time + _straightTime > Time.time)
        {
            lastPosition = transform.position;
            _controller.Move(Vector3.forward * speed * Time.deltaTime, Vector2.zero);
            yield return null;
        }

        StartCoroutine(Follow());
    }

    /// <summary>
    /// El moviment, tenint en compte de si segueix a un target
    /// </summary>
    /// <returns></returns>
    private IEnumerator Follow()
    {
        while (true)
        {
            if (_target != null) // Si no té target, no li canviem la direcció
            {
                Vector2 targetPosition = (Vector2)_target.transform.position + _target.GetComponent<BoxCollider2D>().offset;
                Vector3 targetDirection = (Vector3)(targetPosition - (Vector2)transform.position).normalized;
                Vector3 currentForward = transform.forward;
                Vector3 newForward = Vector3.Slerp(currentForward, targetDirection, Time.deltaTime * rotationSpeed);
                newForward = new Vector3(newForward.x, newForward.y, 0);

                float angle = Vector3.Angle(currentForward, newForward);
                float maxAngle = maxAnglePerSecond * Time.deltaTime;
                if (angle > maxAngle)
                {
                    // it's rotating too fast, clamp the vector
                    newForward = Vector3.Slerp(currentForward, newForward, maxAngle / angle);
                }

                // assign the new forward to the transform
                transform.forward = newForward;
            }

            lastPosition = transform.position;
            _controller.Move(Vector3.forward * speed * Time.deltaTime, Vector2.zero);
            yield return null;
        }
    }
}
