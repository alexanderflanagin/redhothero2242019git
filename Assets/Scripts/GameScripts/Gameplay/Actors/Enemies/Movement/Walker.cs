using UnityEngine;

/// <summary>
/// La classe Walker dona per fet que l'enemic tambe cau!
/// </summary>
public class Walker : EnemyMove
{
    /// <summary>
    /// Fins que no toquem terra per primera vegada no comencem a caminar
    /// </summary>
    private bool xMoving = false;

    /// <summary>
    /// Si ha estat power gloved, no ha de caure
    /// </summary>
    private bool affectedByGravity = false;

    void Awake()
    {
        _enemyController = GetComponent<EnemyController>();

        _controller = GetComponent<Physics2DController>();
    }

    /// <summary>
    /// Rotem si cal, el posem parat, re-habilitem la mesh (si cal) i registrem events
    /// </summary>
    void OnEnable()
    {
        if (_enemyController.direction != -1)
        {
            _enemyController.events.triggerEvent(EnemyEvents.EventName.Rotate);
        }

        _speed = 0;

        // Busquem la mesh i l'habilitem (per si s'havia deshabilitat i el tenim a un pool)
        if (_enemyController.meshObject != null)
        {
            _enemyController.meshObject.SetActive(true);
        }

        // Registrem l'event del move
        _enemyController.events.registerEvent(EnemyEvents.EventName.Move, OnMoveStart);
        _enemyController.events.registerEvent(EnemyEvents.EventName.Die, OnMoveStop);
        _enemyController.events.registerEvent(EnemyEvents.EventName.PowerGloved, OnMoveStop);
    }

    /// <summary>
    /// Des-registrem Events
    /// </summary>
    void OnDisable()
    {
        _enemyController.events.unregisterEvent(EnemyEvents.EventName.Move, OnMoveStart);
        _enemyController.events.unregisterEvent(EnemyEvents.EventName.Die, OnMoveStop);
        _enemyController.events.unregisterEvent(EnemyEvents.EventName.PowerGloved, OnMoveStop);
    }

    /// <summary>
    /// Quan haguem de comen�ar a moure'ns
    /// </summary>
    /// <param name="enemy"></param>
    void OnMoveStart(GameObject enemy)
    {
        xMoving = false;
        affectedByGravity = true;
    }

    /// <summary>
    /// Quan haguem de parar de moure'ns
    /// </summary>
    /// <param name="enemy"></param>
    void OnMoveStop(GameObject enemy)
    {
        affectedByGravity = false;

        EnterIdle();
    }

    /// <summary>
    /// Al Update el movem de manera normal amb el seu controller
    /// </summary>
    protected void Update()
    {
        if(affectedByGravity)
        {
            _verticalSpeed -= JumpProperties.GRAVITY * Time.deltaTime;

            if (_verticalSpeed < -JumpProperties.MAX_FALLING_SPEED)
            {
                _verticalSpeed = -JumpProperties.MAX_FALLING_SPEED;
            }

            lastPosition = transform.position;
            _controller.Move(new Vector2(_enemyController.direction * _speed, _verticalSpeed) * Time.deltaTime);

            if (_controller.collisions.below)
            {
                _verticalSpeed = 0;

                if (!xMoving)
                {
                    // Per defecte, no comencem a caminar fins que hem tocat terra per primera vegada
                    EnterWalk();
                    xMoving = true;
                }

            }

            Bounce();
        }
        

        if (transform.position.y < -20.0f)
        {
            _enemyController.events.triggerEvent(EnemyEvents.EventName.Die);
        }
    }

    /// <summary>
    /// Override del Bounce
    /// </summary>
    public void Bounce()
    {
        if ((_controller.collisions.right && _controller.collisions.faceDir == 1) || (_controller.collisions.left && _controller.collisions.faceDir == -1))
        {
            _enemyController.events.triggerEvent(EnemyEvents.EventName.Rotate);
        }
    }

    /// <summary>
    /// Metode per a passar l'enemic de caminant a idle
    /// </summary>
    public void EnterIdle()
    {
        _speed = 0;
    }

    /// <summary>
    /// Metode per a passar l'enemic de idle a caminant
    /// </summary>
    public void EnterWalk()
    {
        _speed = EnemiesProperties.WALKER_DEFAULT_SPEED;
    }
}
