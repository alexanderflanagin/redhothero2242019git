using UnityEngine;
using System.Collections;

public class RotatingTurretPath : CircularMovingPath
{
    //protected override void Start()
    //{
    //    base.Start();

    //    // Li posem la direcci� i la rotaci� a tots els lasers que crea
    //    for (int i = 0; i < this.elements.Length; i++)
    //    {
    //        LaserEmitterController controller = this.elements[i].GetOrAddComponent<LaserEmitterController>();
    //    }
    //}

    protected override void AddPathBehaviourAtElement(int index, GameObject element)
    {
        TurretPathRotating movingPathElement = element.AddComponent<TurretPathRotating>();
        movingPathElement.path = this;
        this.elements[index] = movingPathElement;
    }
}
