using UnityEngine;
using System.Collections;

/// <summary>
/// Funcionalitat que s'afegeix als objectes que han estat coplejats per el PowerGlove del Hero
/// </summary>
public class PowerGloved : MonoBehaviour
{
    /// <summary>
    /// Direcció en la que surt disparat
    /// </summary>
    public Vector3 direction;

    /// <summary>
    /// Vector de rotacio que te l'element
    /// </summary>
    public Vector3 rotationVector;

    /// <summary>
    /// Velocitat minima de rotacio en un axis
    /// </summary>
    public float rotationMinSpeed = 400;

    /// <summary>
    /// Velocitat maxima de rotacio en un axis
    /// </summary>
    public float rotationMaxSpeed = 500;

    /// <summary>
    /// Velocitat a la que sortirà disparat l'objecte colpejat
    /// </summary>
    private float _speed = 24f;

    /// <summary>
    /// Per a guardar si anteriorment el collider de l'objecte era trigger o no
    /// </summary>
    private bool _isTrigger;

    /// <summary>
    /// El Controller de l'enemic colpejat
    /// </summary>
    private IPowerGloveable _powerGloveable;

    /// <summary>
    /// La rotació que tenia abans de ser colpejat
    /// </summary>
    private Quaternion _rotation;

    /// <summary>
    /// La posició que tenia abans de ser colpejat
    /// </summary>
    private Vector3 _position;

    /// <summary>
    /// Collider del element que ha sigut PowerGloved
    /// </summary>
    private Collider2D _collider;

    /// <summary>
    /// Game Object que movem. Diria que es innecesari
    /// </summary>
    public GameObject objectToMove;

    /// <summary>
    /// Controller de fisiques que te/tindra l'objecte colpejat
    /// </summary>
    private Physics2DController attachedController;

    /// <summary>
    /// Per saber si tenia o no controller abans (per a deixar-lo com estava)
    /// </summary>
    private bool hasController;

    //private bool isMovingPathElement;

    /// <summary>
    /// Guardem la layer mask original de col.lisions del controller (si en tenia)
    /// </summary>
    private LayerMask controllerOriginalCollisionMask;

    private TrailRendererController trailController;

    void OnEnable()
    {
        // Recuperem el component IPowerGloveable i guardem l'estat
        _powerGloveable = transform.GetInterface<IPowerGloveable>();
        _powerGloveable.SaveState();

        // Recuperem el collider, guardem si era trigger o no i ens assegurem de que ara ho sigui
        _collider = GetComponent<Collider2D>();

        // Guardem les posicions inicials
        _rotation = _powerGloveable.meshObject.transform.localRotation;
        _position = _powerGloveable.meshObject.transform.localPosition;

        // Generem una rotacio aleatoria
        rotationVector = new Vector3(Random.Range(rotationMinSpeed, rotationMaxSpeed), Random.Range(rotationMinSpeed, rotationMaxSpeed), Random.Range(rotationMinSpeed, rotationMaxSpeed));

        // Posem/agafem el controller, actualitzem la layer mask i registrem els Events que s'han de llencar quan xoca
        attachedController = GetComponent<Physics2DController>();
        hasController = attachedController != null;
        if (hasController)
        {
            controllerOriginalCollisionMask = attachedController.collisionMask;
        }
        else
        {
            attachedController = gameObject.AddComponent<Physics2DController>();
        }

        attachedController.UpdateLayerMask(Layers.PowerGlovedElements);
        attachedController.BlockedFrontEnter += OnBlockedFront;
        attachedController.BlockedAboveEnter += OnBlockedAbove;
        attachedController.BlockedBelowEnter += OnBlockedBelow;

        // Elimino les rotacions que tingués
        transform.rotation = Quaternion.identity;

        // Estava en un path?
		if(transform.parent != null)
		{
			PathElement pathElement = transform.parent.GetComponent<PathElement>();
			if (pathElement)
			{
				// El path ja no l'ha de moure
				//isMovingPathElement = pathElement.moving;
				pathElement.moving = false;
			}
		}	
        
    }

    void OnDisable()
    {
        // Des-registrem els events que s'han de llencar quan xoca
        attachedController.BlockedFrontEnter -= OnBlockedFront;
        attachedController.BlockedAboveEnter -= OnBlockedAbove;
        attachedController.BlockedBelowEnter -= OnBlockedBelow;
    }

    /// <summary>
    /// Movem l'objecte amb el Physics2DController i el rotem
    /// </summary>
    void Update()
    {
        attachedController.Move(direction * _speed * Time.deltaTime);

        if (_powerGloveable != null)
        {
            _powerGloveable.meshObject.transform.RotateAround(_collider.bounds.center, new Vector3(1, 0, 0), rotationVector.x * Time.deltaTime);
            _powerGloveable.meshObject.transform.RotateAround(_collider.bounds.center, new Vector3(0, 1, 0), rotationVector.y * Time.deltaTime);
            _powerGloveable.meshObject.transform.RotateAround(_collider.bounds.center, new Vector3(0, 0, 1), rotationVector.z * Time.deltaTime);
        }

        if(trailController != null)
        {
            trailController.transform.position = transform.position + trailController.offset;
        }
    }

    /// <summary>
    /// Al desapareixer de pantalla, ens auto-destruim al cap de 2 segons
    /// </summary>
    void OnBecameInvisible()
    {
        Invoke("DestroyByTime", 2f);
    }

    /// <summary>
    /// Recuperem l'estat original, avisem al PowerGloveable que ens destruim per temps i ens destruim
    /// </summary>
    void DestroyByTime()
    {
        RestoreOriginalState();
        _powerGloveable.DestroyByTime();
        _powerGloveable = null;
        Destroy(this);
    }

    /// <summary>
    /// Retornem l'objecte al seu estat normal
    /// </summary>
    void RestoreOriginalState()
    {
        _powerGloveable.RecoverState();

        _collider.isTrigger = _isTrigger;
        _powerGloveable.meshObject.transform.localPosition = _position;
        _powerGloveable.meshObject.transform.localRotation = _rotation;

        if (hasController)
        {
            attachedController.UpdateLayerMask(controllerOriginalCollisionMask);
        }
        else
        {
            Destroy(attachedController);
        }

        //if (isMovingPathElement)
        //{
        //    // Estava en un path?
        //    PathElement pathElement = transform.parent.GetComponent<PathElement>();
        //    if (pathElement)
        //    {
        //        // El path ja no l'ha de moure
        //        pathElement.moving = isMovingPathElement;
        //    }
        //}
    }

    /// <summary>
    /// Si xoquem per davant, busquem si l'objecte amb el que xoquem es el dret o l'esquerra i cridem el Collide
    /// </summary>
    void OnBlockedFront()
    {
        GameObject other = (direction.x > 0) ? attachedController.collisions.rightObject : attachedController.collisions.leftObject;
        Collide(other);
    }

    /// <summary>
    /// Recuperem l'objecte amb el que hem xocat i cridem el Collide
    /// </summary>
    void OnBlockedBelow()
    {
        Collide(attachedController.collisions.belowObject);
    }

    /// <summary>
    /// Recuperem l'objecte amb el que hem xocat i cridem el Collide
    /// </summary>
    void OnBlockedAbove()
    {
        Collide(attachedController.collisions.aboveObject);
    }

    /// <summary>
    /// Retornem el seu estat original, el fem xocar, i si ha xocat amb un objecte tambe IPowerGloveable el fem xocar
    /// </summary>
    /// <param name="with">El GameObject amb el que hem xocat</param>
    void Collide(GameObject with)
    {
        DebugUtils.AssertError(with != null, "We have collided, but we don't know with what");
        
        // Si hem entrat aqui es perque HAVIEM de col.lisionar
        RestoreOriginalState();
        _powerGloveable.Collide(with);
        if (with.transform.GetInterface<IPowerGloveable>() != null)
        {
            with.transform.GetInterface<IPowerGloveable>().Collide(gameObject);
        }
        _powerGloveable = null;
        Destroy(this);
    }

    /// <summary>
    /// Deshabilitem (i destruim) aquest component
    /// </summary>
    public void DisablePowerGloved()
    {
        RestoreOriginalState();
        _powerGloveable = null;
        Destroy(this);
    }

    /// <summary>
    /// Adds a trail to this powergloved element
    /// </summary>
    /// <param name="localPosition">Local position of the trail inside the element</param>
    public void AddTrail(Vector3 localPosition)
    {
        TrailRendererController trail = GameObjectPooler.Instance.GetPowerGlovedTrail();
        trail.transform.rotation = Quaternion.identity;
        //trail.transform.SetParent(transform);
        trail.offset = localPosition;
        trail.transform.position = transform.position + trail.offset;
        
        trail.gameObject.SetActive(true);

        trailController = trail;
    }

    /// <summary>
    /// If it has created a trail, removeit
    /// </summary>
    private void OnDestroy()
    {
        // Si tenia trail li treiem
        if (trailController)
        {
            trailController.Release();
            trailController = null;
        }
    }
}
