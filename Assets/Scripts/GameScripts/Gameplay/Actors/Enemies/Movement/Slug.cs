 using UnityEngine;
using System.Collections;

public class Slug : EnemyMove
{
    /// <summary>
    /// Forca amb la que surt el llimac
    /// </summary>
    private float _pushForce = 4.5f;

    /// <summary>
    /// Factor de fricci� que t� amb el terra i li fa disminuir la velocitat
    /// </summary>
    private float _friction = 0.08f;

    /// <summary>
    /// Quanta estona triga a girar-se en total (trigar� la meitat a frenar-se i l'altre meitat a girar-se, tot i que aix� es pot canviar)
    /// </summary>
    private float _timeToTurn = 0.5f;

    /// <summary>
    /// Defineix si pot rotar o no (no pot mentre est� rotant)
    /// </summary>
    private bool _canRotate = true;

    /// <summary>
    /// Defineix si es pot moure (no pot mentre est� rotant)
    /// </summary>
    private bool _moving = false;

    /// <summary>
    /// Delegate que farem servir per a crear els Events d'animaci� de l'Slug
    /// </summary>
    public delegate void Step();

    /// <summary>
    /// Event que es dispara cada cop que l'Slug agafa impuls anant cap a l'esquerra
    /// </summary>
    public event Step StepLeft;

    /// <summary>
    /// Event que es dispara cada cop que l'Slug agafa impuls anant cap a la dreta
    /// </summary>
    public event Step StepRight;

    /// <summary>
    /// Event que es dispara cada cop que l'Slug gira
    /// </summary>
    public event Step TurnEvent;

    /// <summary>
    /// NONONONONO AIXO SERA DEL CONTROLLER PERO ARA NO HO FARE!
    /// </summary>
    float _direction;

    /// <summary>
    /// Al habilitar el GameObject, carreguem el controller i li donem la direcci�.
    /// Tamb� Creem la coroutina per a carregar els events.
    /// </summary>
    void OnEnable()
    {
        _controller = GetComponent<Physics2DController>();

        _enemyController = GetComponent<EnemyController>();

        _direction = -1;

        _canRotate = true;

        StartCoroutine(LoadEvents());
    }

    /// <summary>
    /// Coroutina per a carregar els Events un frame m�s tard
    /// </summary>
    /// <returns></returns>
    private IEnumerator LoadEvents()
    {
        yield return null;
        _enemyController.events.registerEvent(EnemyEvents.EventName.Die, Die);
        _enemyController.events.registerEvent(EnemyEvents.EventName.Move, Move);
    }

    /// <summary>
    /// Al deshabilitar el GameObject, treiem les execucions dels Events
    /// </summary>
    void OnDisable()
    {
        _enemyController.events.unregisterEvent(EnemyEvents.EventName.Die, Die);
        _enemyController.events.unregisterEvent(EnemyEvents.EventName.Move, Move);
    }

    /// <summary>
    /// M�tode heretat de classe. Comen�a a moure el GameObject
    /// </summary>
    public void Move(GameObject enemy)
    {
        _moving = true;
        StartCoroutine(Moving());
    }

    /// <summary>
    /// Mentre em permeti moure (el _moving), moc el Slug
    /// </summary>
    /// <returns></returns>
    private IEnumerator Moving()
    {
        // bool turning = false;
        while (_moving)
        {
            _speed -= _friction;

            if (_speed <= 0)
            {
                _speed = _pushForce;

                if (_direction == 1 && StepRight != null)
                    StepRight();

                if (_direction == -1 && StepLeft != null)
                    StepLeft();
            }

            if(moveFloor == Factory.MoveFloor.wallLeft || moveFloor == Factory.MoveFloor.wallRight)
            {
                lastPosition = transform.position;
                _controller.Move(Vector2.up *_speed * _direction * Time.deltaTime);

                if (_controller.collisions.above || _controller.collisions.below)
                {
                    Rotate();
                }
            }
            else
            {
                lastPosition = transform.position;
                _controller.Move(Vector2.right * _speed * _direction * Time.deltaTime);

                if ((_controller.collisions.right && _controller.collisions.faceDir == 1) || (_controller.collisions.left && _controller.collisions.faceDir == -1))
                {
                    Rotate();
                }
            }

            CheckBottom();

            yield return null;
        }
    }

    public void CheckBottom()
    {
        Vector3 rayDirection = Vector3.zero;
        Vector2 rayPosition = Vector2.zero;
        float skinWidth = 0.015f;

        BoxCollider2D collider = ((SplurghController)_enemyController).boxCollider;

        switch (moveFloor)
        {
            case Factory.MoveFloor.floor:
                rayDirection = -Vector3.up;
                rayPosition = (Vector2) transform.position + new Vector2(collider.size.x / 2 * _direction, skinWidth);
                break;
            case Factory.MoveFloor.roof:
                rayDirection = Vector3.up;
                rayPosition = (Vector2)transform.position + new Vector2(collider.size.x / 2 * _direction, collider.size.y - skinWidth);
                break;
            case Factory.MoveFloor.wallLeft:
                rayDirection = -Vector3.right;
                rayPosition = (Vector2)transform.position + new Vector2(skinWidth, collider.size.y / 2 * _direction);
                break;
            case Factory.MoveFloor.wallRight:
                rayDirection = Vector3.right;
                rayPosition = (Vector2)transform.position + new Vector2((collider.size.x - skinWidth), collider.size.y / 2 * _direction);
                break;
        }

        //Debug.Log("Ray position: " + rayPosition + " - ray direction: " + rayDirection);
        //Debug.DrawRay(rayPosition, rayDirection, Color.red);

        RaycastHit2D hit = Physics2D.Raycast(rayPosition, rayDirection, 0.15f, Layers.addLayer(0, Layers.Ground));
        if (!hit)
        {
            Rotate();
        }
    }

    /// <summary>
    /// Si es pot rotar (ho defineix el _canRotate), rota el Slug fent servir la coroutina Turn
    /// </summary>
    public void Rotate()
    {
        if (_canRotate)
        {
            if (TurnEvent != null)
                TurnEvent();
            _canRotate = false;
            _moving = false;
            StartCoroutine(Turn());
        }
    }

    /// <summary>
    /// Durant el temps _timeToTurn, gira el personatge. Gasta la meitat per girar-se i l'altre meitat per a tornar-se a girar
    /// </summary>
    /// <returns></returns>
    private IEnumerator Turn()
    {
        _direction = -_direction;
        _speed = 0;
        yield return new WaitForSeconds(_timeToTurn);
        _moving = true;
        StartCoroutine(Moving());
        _canRotate = true;
    }

    /// <summary>
    /// Atura les coroutines i destrueix el game object
    /// </summary>
    public void Die(GameObject enemy)
    {
        StopAllCoroutines();
    }
}
