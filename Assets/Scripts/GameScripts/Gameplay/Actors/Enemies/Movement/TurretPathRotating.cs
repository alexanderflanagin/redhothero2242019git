using UnityEngine;
using System.Collections;

public class TurretPathRotating : PathElementCircular
{
    protected TurretFactory factory;

    void Awake()
    {
        this.factory = this.GetComponent<TurretFactory>();
    }

    protected override void Translate()
    {
        base.Translate();

        if(this.factory.currentTurretController != null && this.factory.currentTurretController.followParent)
        {
            this.factory.currentTurretController.transform.right = -(this.transform.position - this.path.transform.position).normalized;

            // this.factory.currentTurretController.cannon.SetDirection(this.factory.currentTurretController.transform.rotation);

            this.factory.currentTurretController.ExternalMove(this.movement, Space.World);
        }
    }
}
