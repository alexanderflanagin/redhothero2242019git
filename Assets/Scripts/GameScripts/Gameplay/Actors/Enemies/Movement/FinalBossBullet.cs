using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinalBossBullet : EnemyMove
{
    public float maxSpeed = 20;

    protected bool alive;

    void Awake()
    {
        _enemyController = GetComponent<EnemyController>();

        _controller = GetComponent<Physics2DController>();
    }

    private void OnEnable()
    {
        _speed = 0;
        alive = false;

        _enemyController.events.registerEvent(EnemyEvents.EventName.Move, OnMoveStart);
        _enemyController.events.registerEvent(EnemyEvents.EventName.Die, OnMoveStop);
        _enemyController.events.registerEvent(EnemyEvents.EventName.PowerGloved, OnMoveStop);
    }

    private void OnDisable()
    {
        _enemyController.events.unregisterEvent(EnemyEvents.EventName.Move, OnMoveStart);
        _enemyController.events.unregisterEvent(EnemyEvents.EventName.Die, OnMoveStop);
        _enemyController.events.unregisterEvent(EnemyEvents.EventName.PowerGloved, OnMoveStop);
    }

    private void OnMoveStart(GameObject enemy)
    {
        alive = true;
        _speed = maxSpeed;
    }

    private void OnMoveStop(GameObject enemy)
    {
        alive = false;
        _speed = 0;
    }

    private void Update()
    {
        if(alive)
        {
            lastPosition = transform.position;
            _controller.Move(Vector2.right * _speed * Time.deltaTime);
        }
    }
}
