using UnityEngine;
using System.Collections;

public class LaserPath : LinearMovingPath
{
    /// <summary>
    /// Direcci� cap a la que apuntaran els lasers
    /// </summary>
    public Transform direction;

    protected override void Start()
    {
        base.Start();

        // Li posem la direcci� i la rotaci� a tots els lasers que crea
        for(int i = 0; i < this.elements.Length; i++)
        {
            LaserEmitterController[] controllers = this.elements[i].GetComponentsInChildren<LaserEmitterController>();

            for(int j = 0; j < controllers.Length; j++)
            {
                controllers[j].rotation = this.direction;
                controllers[j].meshObject.transform.rotation = this.direction.rotation;
            }
        }

        // Amaguem la fletxa de direccio, pero no l'esborrem perque necessitarem el seu transform
        this.direction.GetComponent<SpriteRenderer>().enabled = false;
    }
}
