using UnityEngine;

public class LaserPathElementRotating : PathElementCircular
{
	protected LaserEmitterController controller;

    protected Transform dummyTransform;

    /// <summary>
    /// Creem la transform dummy que farem servir per a rotar
    /// </summary>
    void Awake()
	{
		this.controller = this.GetOrAddComponent<LaserEmitterController>();

        this.dummyTransform = new GameObject("DummyTransform").transform;
        this.dummyTransform.SetParent(this.transform);

        this.controller.rotation = this.dummyTransform;

        this.controller.rotating = true;
    }

    /// <summary>
    /// Posem l'element en la rotacio correcte
    /// </summary>
    private void Start()
    {
        if (controller.rotation != null)
        {
            controller.rotation.right = -(transform.position - path.transform.position).normalized;
            controller.meshObject.transform.rotation = controller.rotation.rotation;
        }
    }

    /// <summary>
    /// Movem i rotem l'element
    /// </summary>
    protected override void Translate ()
	{
		base.Translate ();

        if (this.controller.rotation != null)
        {
            this.controller.rotation.right = -(this.transform.position - this.path.transform.position).normalized;

            this.controller.meshObject.transform.rotation = this.controller.rotation.rotation;
        }
    }
}
