using UnityEngine;
using System.Collections;

public class Liner : EnemyMove
{
    [HideInInspector]
    public bool moving;

    void Awake()
    {
        _enemyController = GetComponent<EnemyController>();

        _controller = GetComponent<Physics2DController>();
    }

	void OnEnable()
    {
        if (_enemyController.direction != -1)
        {
            _enemyController.events.triggerEvent(EnemyEvents.EventName.Rotate);
        }

        // Registrem l'event del move
        _enemyController.events.registerEvent(EnemyEvents.EventName.Move, OnMoveStart);
        _enemyController.events.registerEvent(EnemyEvents.EventName.Die, OnMoveStop);
        _enemyController.events.registerEvent(EnemyEvents.EventName.PowerGloved, OnMoveStop);
	}

    void OnDisable()
    {
        _enemyController.events.unregisterEvent(EnemyEvents.EventName.Move, OnMoveStart);
        _enemyController.events.unregisterEvent(EnemyEvents.EventName.Die, OnMoveStop);
        _enemyController.events.unregisterEvent(EnemyEvents.EventName.PowerGloved, OnMoveStop);
    }

    void OnMoveStart(GameObject enemy)
    {
        moving = true;
        
        // Li donem la velocitat per defecte
        _speed = EnemiesProperties.LINER_DEFAULT_SPEED;
    }

    void OnMoveStop(GameObject enemy)
    {
        _speed = 0;
        moving = false;
    }

    private void Update()
    {
        if(moving)
        {
            lastPosition = transform.position;
            _controller.Move(Vector2.right * _speed * _enemyController.direction * Time.deltaTime);
        }
    }
}
