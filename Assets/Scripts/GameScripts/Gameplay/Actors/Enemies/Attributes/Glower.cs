using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Light))]
public class Glower : MonoBehaviour
{
    public bool onlyOnDie;

    public float maxIntensity = 1;

    private EnemyController _enemyController;

    private Light _light;

    void Awake()
    {
        _enemyController = GetComponent<EnemyController>();

        _light = GetComponent<Light>();
        _light.enabled = false;
    }

    // Use this for initialization
    void OnEnable()
    {
        if (onlyOnDie)
        {
            _enemyController.events.registerEvent(EnemyEvents.EventName.Die, StartGlow);
        }
        else
        {
            _enemyController.events.registerEvent(EnemyEvents.EventName.Move, StartGlow);
        }
    }

    // Update is called once per frame
    void OnDisable()
    {
        if (onlyOnDie)
        {
            _enemyController.events.unregisterEvent(EnemyEvents.EventName.Die, StartGlow);
        }
        else
        {
            _enemyController.events.unregisterEvent(EnemyEvents.EventName.Move, StartGlow);
        }
    }

    void StartGlow(GameObject enemy)
    {
        StartCoroutine("Glow");
    }

    private IEnumerator Glow()
    {
        _light.intensity = 0;
        _light.enabled = true;
        while (_light.intensity < maxIntensity)
        {
            _light.intensity += 0.1f;
            yield return null;
        }

        while (true)
        {
            _light.intensity = maxIntensity + Mathf.PingPong(Time.time, 0.2f) - 0.1f;
            yield return null;
        }
    }
}
