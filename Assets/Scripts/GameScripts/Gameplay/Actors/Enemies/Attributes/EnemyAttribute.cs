using UnityEngine;
using System.Collections;

/// <summary>
/// Classe base que fare servir per a generar els atributs. La creo perque al fer arbres de classes es vegi mes clar
/// </summary>
[RequireComponent(typeof(EnemyController))]
public abstract class EnemyAttribute : MonoBehaviour
{
    /// <summary>
    /// Controller de l'enemic
    /// </summary>
    protected EnemyController controller;

    /// <summary>
    /// Al preparar l'atribut, li assignem el seu EnemyController
    /// </summary>
    /// <param name="controller">L'EnemyController del seu propietari</param>
    public virtual void Prepare(EnemyController controller)
    {
        this.controller = controller;
    }

    /// <summary>
    /// Que fara aquest atribut al ser habilitat
    /// </summary>
    public abstract void Enable();

    /// <summary>
    /// Que far� aquest atribut al ser deshabilitat
    /// </summary>
    public abstract void Disable();
}
