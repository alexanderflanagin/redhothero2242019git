using UnityEngine;
using System.Collections;

public class KazanArmyEyes : MonoBehaviour {

    public Transform eyesPosition;

    [Range(10,90)]
    public float viewAngle;

    [Range(4,12)]
    public float viewRange;

    private Physics2DController controller;

    private KazanArmyController kaController;

	// Use this for initialization
	void Start () {
        this.controller = this.GetComponent<Physics2DController>();

        this.kaController = this.GetComponent<KazanArmyController>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnDrawGizmos()
    {
        if (!this.kaController)
        {
            this.kaController = this.GetComponent<KazanArmyController>();
        }
        
        float betaAngle = (180 - viewAngle) / 2;

        float triangleBase = this.viewRange / (Mathf.Tan(betaAngle * Mathf.Deg2Rad));

        Vector3 topPosition = this.eyesPosition.position + new Vector3(this.viewRange * this.kaController.direction, triangleBase);

        Vector3 bottomPosition = this.eyesPosition.position + new Vector3(this.viewRange * this.kaController.direction, -triangleBase);


        Gizmos.color = Color.blue;

        Gizmos.DrawLine(this.eyesPosition.position, topPosition);
        Gizmos.DrawLine(this.eyesPosition.position, this.eyesPosition.position + new Vector3(this.viewRange * this.kaController.direction, 0));
        Gizmos.DrawLine(this.eyesPosition.position, bottomPosition);
        Gizmos.DrawLine(bottomPosition, topPosition);


    }
}
