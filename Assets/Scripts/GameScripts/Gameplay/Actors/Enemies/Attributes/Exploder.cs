using UnityEngine;
using System.Collections;

public class Exploder : MonoBehaviour {

    private Physics2DController _controller;

    private EnemyController _control;

    void Awake()
    {
        _controller = GetComponent<Physics2DController>();

        _control = GetComponent<EnemyController>();
    }

    void OnTriggerEnter2D (Collider2D other)
    {
        if (Layers.inMask(_controller.collisionMask, other.gameObject.layer))
        {
            _control.events.triggerEvent(EnemyEvents.EventName.Die);
            _control.events.triggerEvent(EnemyEvents.EventName.Explode);
        }
    }
}
