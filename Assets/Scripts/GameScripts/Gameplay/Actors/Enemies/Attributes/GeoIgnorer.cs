using UnityEngine;
using System.Collections;

public class GeoIgnorer : MonoBehaviour
{

	// Use this for initialization
	void Start () {
        GetComponent<Physics2DController>().collisionMask = 0;
	}
}
