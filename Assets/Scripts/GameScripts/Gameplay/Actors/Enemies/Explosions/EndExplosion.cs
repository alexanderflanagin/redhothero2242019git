using UnityEngine;
using System.Collections;
using System;

public class EndExplosion : MonoBehaviour
{
    /// <summary>
    /// El Controller de l'enemic per a registrar els esdeveniments
    /// </summary>
    protected EnemyController controller;

    /// <summary>
    /// Agafem el controller
    /// </summary>
    void Awake()
    {
        controller = GetComponent<EnemyController>();
    }
}
