using UnityEngine;
using System.Collections;
using System;

public class StartExplosion : MonoBehaviour {

	/// <summary>
    /// El controller de l'enemic, des d'on treurem els Events
    /// </summary>
    protected EnemyController controller;

    /// <summary>
    /// El gameObject de la RedHotDensity que crea
    /// </summary>
    protected RedHotDensity _myDensity;

    /// <summary>
    /// Getter públic per al RedHotDensity
    /// </summary>
    public RedHotDensity myDensity
    {
        get
        {
            return _myDensity;
        }
    }

    /// <summary>
    /// Distància a la que sortirà l'explosió respecte al centre del GameObject pare
    /// </summary>
    public Vector3 desviationPosition = Vector3.up;

    public bool tutorialDensity { get; set; }

    /// <summary>
    /// Agafem el EnemyController
    /// </summary>
    void Awake()
    {
        controller = GetComponent<EnemyController>();
    }

    public void SetTutorialDensity(bool tutorial)
    {
        tutorialDensity = tutorial;
        if (tutorial)
        {
            GameObject tutorialDensityGameObject = Instantiate(GeneralPurposeObjectManager.Instance.RedHotDensityTutorial) as GameObject;
            _myDensity = tutorialDensityGameObject.GetComponent<RedHotDensityTutorial>();
            _myDensity.gameObject.SetActive(false);
        }
    }
}
