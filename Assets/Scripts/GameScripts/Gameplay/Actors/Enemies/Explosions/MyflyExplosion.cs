using UnityEngine;
using System.Collections;

public class MyflyExplosion : EndExplosion
{
    /// <summary>
    /// Registrem l'esdeveniment de destruir el portal al morir
    /// </summary>
    void OnEnable()
    {
        controller.events.registerEvent(EnemyEvents.EventName.Explode, OnDie);
    }

    /// <summary>
    /// Desregistrem l'esdeveniment
    /// </summary>
    void OnDisable()
    {
        if(controller != null && controller.startExplosion.myDensity != null)
        {
            controller.startExplosion.myDensity.Release();
        }

        controller.events.unregisterEvent(EnemyEvents.EventName.Explode, OnDie);
    }

    /// <summary>
    /// Al morir, llencem l'esdeveniment que apagarà el portal al cap de 2 segons
    /// </summary>
    void OnDie(GameObject enemy)
    {
        StartCoroutine(HiddeDensity());
    }

    /// <summary>
    /// Ens esperem 2 segons i desactivem la density
    /// </summary>
    /// <returns></returns>
    IEnumerator HiddeDensity()
    {
        yield return new WaitForSeconds(2.0f);

        // Mirem que tinguem la explosio, que no hagi explotat contra una factory i no l'haguem creat
        if (controller != null && controller.startExplosion.myDensity != null)
        {
            controller.startExplosion.myDensity.Release();
            controller.events.triggerEvent(EnemyEvents.EventName.Hide);
        }
    }
	
}
