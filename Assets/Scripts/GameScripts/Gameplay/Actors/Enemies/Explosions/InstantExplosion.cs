using UnityEngine;
using System.Collections;

public class InstantExplosion : StartExplosion
{
    /// <summary>
    /// Deixem instanciat el prefab de la RedHotDensity
    /// </summary>
    //void Start()
    //{
    //    if (!tutorialDensity)
    //    {
    //        _myDensity = GameObjectPooler.Instance.GetRedHotDensity();
    //    }
        
    //    // _myDensity.transform.parent = transform;
    //    _myDensity.SetActive(false);
    //}

    /// <summary>
    /// Registrem l'esdeveniment OnDie
    /// </summary>
    void OnEnable()
    {
        controller.events.registerEvent(EnemyEvents.EventName.Explode, OnDie);
    }

    /// <summary>
    /// Des-registrem l'esdeveniment OnDie
    /// </summary>
    void OnDisable()
    {
        controller.events.unregisterEvent(EnemyEvents.EventName.Explode, OnDie);
    }

    /// <summary>
    /// Al morir, movem la RedHotDensity on toca i l'habilitem
    /// </summary>
    void OnDie(GameObject enemy)
    {
        // Check if i'm inside a factory
        float radius = 1.5f;
        LayerMask enemyFactoryMask = Layers.addLayer(0, Layers.EnemyFactory);
        Collider2D collider = GetComponent<Collider2D>();

        if(collider)
        {
            Debug.DrawRay((Vector2)transform.position + collider.offset + Vector2.down * radius, Vector2.up * radius * 2, Color.red);
            Debug.DrawRay((Vector2)transform.position + collider.offset + Vector2.left * radius, Vector2.right * radius * 2, Color.green);

            if (Physics2D.Raycast((Vector2)transform.position + collider.offset + Vector2.down * radius, Vector2.up, radius * 2, enemyFactoryMask))
            {
                //Debug.Log("No instancio perque soc dins duna enemyFactory");
                return;
            }

            if (Physics2D.Raycast((Vector2)transform.position + collider.offset + Vector2.left * radius, Vector2.right, radius * 2, enemyFactoryMask))
            {
                //Debug.Log("No instancio perque soc dins duna enemyFactory");
                return;
            }

        }

        // If not, show density
        if (!tutorialDensity)
        {
            _myDensity = GameObjectPooler.Instance.GetRedHotDensity();
        }
        _myDensity.transform.position = transform.position + desviationPosition;
        _myDensity.transform.rotation = Quaternion.identity;
        _myDensity.gameObject.SetActive(true);
    }
}
