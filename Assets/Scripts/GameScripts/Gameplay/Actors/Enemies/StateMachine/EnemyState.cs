using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyState
{
	public enum State
    {
        Waiting,
        Walking,
        Damaged,
        Recovering,
        Dead
    }
}
