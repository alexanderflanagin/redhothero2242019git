using UnityEngine;

[RequireComponent(typeof(DocClockController))]
public class DocClockAudio : StvAudio
{
    /// <summary>
    /// Name of the clip played when the docclock countdown is activated
    /// </summary>
    public string activateClip = "ActivateDocClock";

    /// <summary>
    /// Register events when JumpedOver and TouchedByProjectile
    /// </summary>
    protected override void OnEnable()
    {
        base.OnEnable();

        controller.events.registerEvent(EnemyEvents.EventName.JumpedOver, OnActivate);
        controller.events.registerEvent(EnemyEvents.EventName.TouchedByProjectile, OnActivate);
    }

    /// <summary>
    /// Play sound and unregister events
    /// </summary>
    /// <param name="enemy"></param>
    private void OnActivate(GameObject enemy)
    {
        AudioManager.Instance.PlayOneShoot(AudioClipsData.ClipsGroups.Enemies, activateClip);

        controller.events.unregisterEvent(EnemyEvents.EventName.JumpedOver, OnActivate);
        controller.events.unregisterEvent(EnemyEvents.EventName.TouchedByProjectile, OnActivate);
    }
}
