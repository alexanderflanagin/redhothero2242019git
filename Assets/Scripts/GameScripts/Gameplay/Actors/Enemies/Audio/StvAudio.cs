using UnityEngine;

[RequireComponent(typeof(StvController))]
public class StvAudio : MonoBehaviour
{
    /// <summary>
    /// Name of the clip played when explodes
    /// </summary>
    public string deathClip = "StvExplosion";

    /// <summary>
    /// To register events
    /// </summary>
    protected EnemyController controller;

    /// <summary>
    /// Get the controller
    /// </summary>
    void Awake()
    {
        controller = GetComponent<EnemyController>();
    }

    /// <summary>
    /// Register events
    /// </summary>
    protected virtual void OnEnable()
    {
        controller.events.registerEvent(EnemyEvents.EventName.Die, OnDie);
    }

    /// <summary>
    /// Unregister events
    /// </summary>
    protected virtual void OnDisable()
    {
        controller.events.unregisterEvent(EnemyEvents.EventName.Die, OnDie);
    }

    /// <summary>
    /// Play death clip
    /// </summary>
    /// <param name="enemy"></param>
    void OnDie(GameObject enemy)
    {
        AudioManager.Instance.PlayOneShoot(AudioClipsData.ClipsGroups.Enemies, deathClip);
    }
}
