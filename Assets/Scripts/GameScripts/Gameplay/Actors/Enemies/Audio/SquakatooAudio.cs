using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SquakatooController))]

public class SquakatooAudio : MonoBehaviour {

    /// <summary>
    /// Name of the clip played when born
    /// </summary>
    public string squakClip = "Squak";

    /// <summary>
    /// To register events
    /// </summary>
    protected EnemyController controller;

    /// <summary>
    /// Get the controller
    /// </summary>
    void Awake()
    {
        controller = GetComponent<EnemyController>();
    }

    /// <summary>
    /// Register events
    /// </summary>
    protected virtual void OnEnable()
    {
        controller.events.registerEvent(EnemyEvents.EventName.Move, OnMove);
    }

    /// <summary>
    /// Unregister events
    /// </summary>
    protected virtual void OnDisable()
    {
        controller.events.unregisterEvent(EnemyEvents.EventName.Move, OnMove);
    }

    private void OnMove(GameObject enemy)
    {
        AudioManager.Instance.PlayOneShoot(AudioClipsData.ClipsGroups.Enemies, squakClip);
    }
}
