using UnityEngine;
using System.Collections;

public class BlastGlovesBasicProjectile : Projectile
{   
    /// <summary>
    /// Velocitat del projectil
    /// </summary>
    public float _speed = 45f;

    /// <summary>
    /// Si ha de moure o no
    /// </summary>
    public bool move = true;

    /// <summary>
    /// Si al ser disparat te un target fixat i intenta anar cap alla
    /// </summary>
    private Shootable _target;

    /// <summary>
    /// Velocitat de rotacio al perseguir un target
    /// </summary>
    public float rotationSpeed = 16f;

    /// <summary>
    /// Maxim d'angle que pot fer per segon al perseguir un target
    /// </summary>
    public float maxAnglePerSecond = 90f;

    /// <summary>
    /// Getter/setter public del target. Tambe assigna ja la variable _targetBoxCollider
    /// </summary>
    public Shootable target
    {
        get
        {
            return _target;
        }

        set
        {
            _target = value;

            if (_target != null)
            {
                _targetBoxCollider = _target.GetComponent<BoxCollider2D>();
            }
        }
    }

    /// <summary>
    /// Al assignar un target, recuperem el seu BoxCollider2D
    /// </summary>
    private BoxCollider2D _targetBoxCollider;

    private CircleCollider2D _collider;

    void Awake()
    {
        _collider = this.GetComponent<CircleCollider2D>();
    }

    /// <summary>
    /// Mentre s'hagi de moure, el transportem segons la seva velocitat
    /// </summary>
    void Update()
    {
        if (move)
        {
            if (_target != null && _target.active && _target.gameObject.layer != Layers.FinalBoss) // Si no te target, o ja te la mesh desactivada
            {
                Vector2 targetPosition = (Vector2)_target.transform.position + _targetBoxCollider.offset;
                Vector3 targetDirection = ((Vector3)targetPosition - transform.position).normalized;
                Vector3 currentForward = transform.forward;
                Vector3 newForward = Vector3.Slerp(currentForward, targetDirection, Time.deltaTime * rotationSpeed);

                float angle = Vector3.Angle(currentForward, newForward);
                float maxAngle = maxAnglePerSecond * Time.deltaTime;
                if (angle > maxAngle)
                {
                    // it's rotating too fast, clamp the vector
                    newForward = Vector3.Slerp(currentForward, newForward, maxAngle / angle);
                }

                // assign the new forward to the transform
                transform.forward = newForward;
            }

            transform.Translate(Vector3.forward * _speed * Time.deltaTime);
        }
    }

    /// <summary>
    /// Al entrar un trigger, si l'altre es un enemic el matem
    /// </summary>
    /// <param name="other"></param>
    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject == owner)
        {
            return;
        }

        Physics2D.queriesHitTriggers = true;
        Collider2D blueSubstance = Physics2D.OverlapCircle(transform.position, _collider.radius, 1 << Layers.BlueSubstance);
        Physics2D.queriesHitTriggers = false;
        
        if (other.gameObject.layer == Layers.BlueSubstance)
        {
            Instantiate(GeneralPurposeObjectManager.Instance.BlueSubstanceShoot, transform.position, Quaternion.identity);
            OnDestroyProjectile();
        }
        else if (blueSubstance)
        {
            Instantiate(GeneralPurposeObjectManager.Instance.BlueSubstanceShoot, transform.position, Quaternion.identity);
            OnDestroyProjectile();
        }
        else if ((!other.isTrigger || other.GetComponent<PowerGloved>()) && other.GetComponent<EnemyController>())
        {
            EnemyController enemy = other.GetComponent<EnemyController>();

            if (enemy.shootable)
            {
                enemy.OnTouchedByProjectile(this);
                StandardStageManager.current.KilledByPlayer(owner, other.gameObject);
            }

            OnDestroyProjectile();
        }
        else if(other.GetComponent<Projectile>())
        {
            other.GetComponent<Projectile>().OnDestroyProjectile();
            OnDestroyProjectile();
        }
        else if (other.GetComponent<Shootable>())
        {
            other.GetComponent<BreakableElement>().DestroyMe();
            OnDestroyProjectile();
        }
        else if (other.GetComponent<CharacterControl>())
        {
            CharacterControl control = other.GetComponent<CharacterControl>();
            control.RecieveDamage(this.gameObject);
            OnDestroyProjectile();
        }
    }
}
