using UnityEngine;
using System.Collections;

/// <summary>
/// Detecta col�lisions al centre del projectil per a destruirlo
/// </summary>
public class ProjectileCenter : MonoBehaviour {

    /// <summary>
    /// El projectil al qual pertany
    /// </summary>
    private Projectile _parent;

	/// <summary>
	/// Al start recuperem el seu _parent
	/// </summary>
	void Start () {
        _parent = GetComponentInParent<Projectile>();
	}

    /// <summary>
    /// Al col·lisionar amb una layer ground, es destrueix
    /// </summary>
    /// <param name="other">Colldier2D amb el qual col·lisiona</param>
    void OnTriggerEnter2D(Collider2D other)
    {
        if (!other.isTrigger && other.gameObject.layer == Layers.Ground)
        {
            _parent.OnDestroyProjectile();
        }
    }
}
