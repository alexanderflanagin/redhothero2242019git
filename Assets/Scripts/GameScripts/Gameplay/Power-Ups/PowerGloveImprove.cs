using UnityEngine;

/// <summary>
/// Millora del player per a que tingui el Blast Glove
/// </summary>

public class PowerGloveImprove : MonoBehaviour
{
    /// <summary>
    /// Les partícules que surten quan el player agafa aquesta millora
    /// </summary>
    public ParticleSystem pickUpExplosion;
	
    /// <summary>
    /// Audio clip played when the update is picked
    /// </summary>
    public string pickAudioClip = "PickElement";

    void Start()
    {
        // Canviem la mesh si ja està agafat
        if (GameManager.Instance.playerData.powerGlove || (GameManager.Instance.checkPointProperties != null && GameManager.Instance.checkPointProperties.currentStage == Application.loadedLevel && GameManager.Instance.checkPointProperties.powerGlove))
        {
            transform.Find("GloveNoPicked").gameObject.SetActive(false);
        }
        else
        {
            transform.Find("GlovePicked").gameObject.SetActive(false);
        }
    }

    /// <summary>
    /// Si toquem el player, li afegim la capacitat de disparar, llencem part�cules i eliminem el game object
    /// </summary>
    /// <param name="other">El collider amb el que fa trigger</param>
    void OnTriggerEnter2D(Collider2D other)
    {
        MagmaSuit magmaSuit = other.GetComponent<MagmaSuit>();
        if (magmaSuit)
        {
            AudioManager.Instance.PlayOneShoot(AudioClipsData.ClipsGroups.Enviro, pickAudioClip);
            magmaSuit.hasPowerGlove = true;
            magmaSuit.addGloves();

            if (pickUpExplosion)
            {
                ParticleSystem ps = Instantiate(pickUpExplosion, this.transform.position, Quaternion.identity) as ParticleSystem;
                ps.GetOrAddComponent<ParticleSystemAutoDestroy>();
                ps.Play();
            }

            Destroy(this.gameObject);
        }
    }
}
