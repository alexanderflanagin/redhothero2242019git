using UnityEngine;
using System;
using System.Collections;

[RequireComponent(typeof(MeshRenderer))]
public class Shootable : MonoBehaviour
{
    /// <summary>
    /// Definició del delegate per quan un projectil toca un shootable
    /// </summary>
    /// <param name="from"></param>
    public delegate void ShootableEvent(Projectile from);
    
    /// <summary>
    /// Delegate que fem servir per a que els altres scripts registrin les accions
    /// </summary>
    public ShootableEvent touched;

    /// <summary>
    /// Apart de poder-se disparar, s'ha d'auto-apuntar cap a ella?
    /// </summary>
    public bool addToTargetList = true;

    /// <summary>
    /// Mesh que detecta si som visibles o no
    /// </summary>
    public MeshRenderer mesh;

    /// <summary>
    /// Per saber si esta actiu el shootable independentment de com estigui la mesh
    /// </summary>
    [HideInInspector]
    public bool active;

    /// <summary>
    /// Si no hem assignat el mesh per editor, l'agafem
    /// </summary>
    private void Awake()
    {
        if(mesh == null)
        {
            mesh = GetComponent<MeshRenderer>();
        }
    }

    private void OnEnable()
    {
        active = true;

        if (mesh != null)
        {
            mesh.enabled = active;
        }
    }

    /// <summary>
    /// Al fer-se visible, l'afegim a la llista
    /// </summary>
    void OnBecameVisible()
    {
        if(addToTargetList && active)
        {
            RedHotTarget.instance.AddShootable(this.gameObject);
        }
    }

    /// <summary>
    /// Al fer-se invisible (per la raó que sigui), el treiem també de la llista de shootables
    /// </summary>
    void OnBecameInvisible()
    {
        //Debug.Log("Shootable becoming invisible: " + this.gameObject, this.gameObject);
        RedHotTarget.instance.RemoveShootable(this.gameObject);
    }

    void OnDisable()
    {
        RedHotTarget.instance.RemoveShootable(this.gameObject);
    }

    /// <summary>
    /// Activates a shootable again, re-enabling the mesh and adding it to the shootables list if it's necessary
    /// </summary>
    public void Activate()
    {
        active = true;

        if (mesh != null)
        {
            mesh.enabled = active;
        }

        if(addToTargetList && mesh.isVisible)
        {
            RedHotTarget.instance.AddShootable(this.gameObject);
        }
    }

    /// <summary>
    /// Mètode públic per a poder treure un element de la llista de shootables manualment
    /// </summary>
    public void Deactivate()
    {
        // Si ens treuen de la llista manualment, entenem que ja no hi hem d'estar
        active = false;

        RedHotTarget.instance.RemoveShootable(this.gameObject);
    }

    /// <summary>
    /// Metode que crida el projectil al tocar l'element shootable. Crida al delegate per a que faci el que s'hagi de fer
    /// </summary>
    /// <param name="from"></param>
    public void Touched(Projectile from)
    {
        if(this.touched != null)
        {
            this.touched(from);
        }
    }
}
