﻿using UnityEngine;
using System.Collections;

public class SandCastle : MonoBehaviour {

	public GameObject _sandCastle;
	public GameObject _sandCastleDestroyed;
	public GameObject _spade;
	public GameObject _spadeDestroyed;

	private bool _canBeTriggered = true;

	public ParticleSystem _dust;

	// Use this for initialization
	void Start () {

//		_dust = this.GameObject.Find ("Dust").GetComponent<ParticleSystem>();
//		_sandCastle =  GameObject.Find ("SandCastle");
//		_sandCastleDestroyed = GameObject.Find ("SandCastleDestroyed");
//		_spade = GameObject.Find ("Spade");
//		_spadeDestroyed = GameObject.Find ("SpadeDestroyed");

		if (_spadeDestroyed){
//			Debug.Log ( "SandCastle :: SpadeDestroyed goes OFF" );

			_spadeDestroyed.SetActive (false);
		}

		if (_sandCastleDestroyed){
//			Debug.Log ( "SandCastle :: SandCastleDestroyed goes OFF");

			_sandCastleDestroyed.SetActive (false);
		}

	}


	void OnTriggerEnter2D( Collider2D other ) {
//		Debug.Log ( "SandCastle :: OnTriggerEnter" );

		if( other.tag == "Player" && _canBeTriggered ) {
//			Debug.Log ( "SandCastle :: player and can be triggered" );

			_dust.Play ();
			_dust.enableEmission = true;
			_sandCastle.SetActive (false);
			_sandCastleDestroyed.SetActive (true);
			if ( _spade && _spadeDestroyed ) {
//				Debug.Log ( "SandCastle :: spade and spadedestroyed are in scene" );

				_spade.SetActive (false);
				_spadeDestroyed.SetActive (true);
			}
//			Debug.Log ( "SandCastle :: no spade or spadedestroyed" );

			_canBeTriggered = false;
//			other.GetComponent<MainCharControl>().EnterTripOver( time );
		}
	}
//
//	void OnTriggerStay2D ( Collider2D other ){
//		Debug.Log ( "SandCastle :: OnTriggerStay" );
//		
//	}
//
//	void OnTriggerExit2D( Collider2D other ) {
//		_canBeTriggered = true;
//		Debug.Log ( "SandCastle :: TriggerExit" );
//	}
}
