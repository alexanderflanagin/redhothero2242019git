using UnityEngine;
using System.Collections;

public class SlideElement : MonoBehaviour {

	/*
	 * Temps que esta relliscant
	 */
	public float time = 4.0f;

	/*
	 * Velocitat que agafa. Es fa servir nomes per a en Flip
	 */
	public float speed = 16.0f;

	/*
	 * Al entrar un trigger, mirem si pot relliscar i si es aixi el fem relliscar.
	 */
	void OnTriggerEnter2D( Collider2D other ) {
		// Debug.Log ( "[TripOverElement::OnTriggerEnter] other: " + other );
		ISlideable slideable = other.GetInterface<ISlideable>();
		if( slideable != null && slideable.CanSlide() ) {
			slideable.EnterSlide( time, speed );
		}
	}
}
