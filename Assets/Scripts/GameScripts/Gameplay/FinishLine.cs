using UnityEngine;
using System.Collections;

public class FinishLine : MonoBehaviour 
{
	public GameObject gate;

    [AudioClip(AudioClipsData.ClipsGroups.Enviro)]
    public string onEnterSound;

    protected new ParticleSystem particleSystem;

    private void Awake()
    {
        particleSystem = GetComponentInChildren<ParticleSystem>();
    }

    void OnBecameVisible()
    {
        RedHotTarget.instance.AddRedHotDensity(this.gameObject);
    }

    void OnBecameInvisible()
    {
        RedHotTarget.instance.RemoveRedHotDensity(this.gameObject);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag(Tags.Player))
        {
            AudioManager.Instance.PlayOneShoot(AudioClipsData.ClipsGroups.Enviro, onEnterSound);
            AudioManager.Instance.EnterOnFinishLine();

            // myScenario.End(true)
            var main = particleSystem.main;
            main.startSpeed = 20;
            main.startSize = 2f;
            main.maxParticles = 4000;
            main.startLifetime = 2;

            var emission = particleSystem.emission;
            emission.rateOverTime = 2000;
			//GetComponentInChildren<ParticleSystem>().emissionRate = 2000;

            StandardStageManager.current.Finish();
            other.gameObject.SetActive(false);
			gate.SetActive(false);
        }
    }
}
