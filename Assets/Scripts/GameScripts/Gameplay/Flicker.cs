﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Fa parpellejar un objecte en pantalla
/// </summary>
public class Flicker : MonoBehaviour {

    /// <summary>
    /// Temps per defecte que està ences
    /// </summary>
    public float onTime = 0.2f;

    /// <summary>
    /// Temps per defecte que està apagat
    /// </summary>
    public float offTime = 0.2f;

    /// <summary>
    /// El temps d'estar encés i apagat és aleatori?
    /// Es fan servir de paràmetres el onTime i offTime, juntament amb els randomize
    /// </summary>
    public bool randomTime = true;

    /// <summary>
    /// Quina variació de temps hi pot haver (+-) quan està engegat si el randomTime està actiu
    /// </summary>
    public float onRandomizeTime = 0.2f;

    /// <summary>
    /// Quina variació de temps hi pot haver (+-) quan està engegat si el randomTime està inactiu
    /// </summary>
    public float offRandomizeTime = 0.4f;

    /// <summary>
    /// Llista de tots els renderers que s'han d'encendre i apagar per a que faci flickering
    /// </summary>
    private List<Renderer> _renderers;

    /// <summary>
    /// Al habilitar, creem la llista de renderers i comencem la coroutina de Flick
    /// </summary>
    void OnEnable()
    {
        _renderers = new List<Renderer>();
        _renderers.AddRange(GetComponentsInChildren<Renderer>());
        _renderers.Add(this.GetComponent<Renderer>());
        
        StartCoroutine("Flick");
    }

    /// <summary>
    /// Activem i desactivem els renderers de l'objecte. Tenim en compte temps i si ho estem fent random o no
    /// </summary>
    /// <returns></returns>
    private IEnumerator Flick()
    {
        while (true)
        {
            foreach (Renderer rend in _renderers)
            {
                rend.enabled = true;
            }

            if (randomTime)
            {
                yield return new WaitForSeconds(Random.Range(onTime - onRandomizeTime, onTime + onRandomizeTime));
            }
            else
            {
                yield return new WaitForSeconds(onTime);
            }

            foreach (Renderer rend in _renderers)
            {
                rend.enabled = false;
            }

            if (randomTime)
            {
                yield return new WaitForSeconds(Random.Range(offTime - offRandomizeTime, offTime + offRandomizeTime));
            }
            else
            {
                yield return new WaitForSeconds(offTime);
            }

            yield return null;
        }
    }

    /// <summary>
    /// Al deshabilitar, treiem la coroutina de Flick i activem els renderers (per si fossin necessaris)
    /// </summary>
    void OnDisable()
    {
        StopCoroutine("Flick");
        foreach (Renderer rend in _renderers)
        {
            rend.enabled = true;
        }
    }
}
