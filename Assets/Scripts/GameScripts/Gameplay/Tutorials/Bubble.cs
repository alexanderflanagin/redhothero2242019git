using UnityEngine;
using System.Collections;

public class Bubble : MonoBehaviour {

    private IEnumerator _blink;

    private SpriteRenderer _content;

    void OnEnable()
    {
        _content = this.GetComponent<SpriteRenderer>();
        
        _blink = Blink();
        StartCoroutine(_blink);
    }

    void OnDisable()
    {
        if (_blink != null)
        {
            StopCoroutine(_blink);
        }
    }

    private IEnumerator Blink()
    {
        while (true)
        {
            _content.enabled = !_content.enabled;
            yield return new WaitForSeconds(0.6f);
        }
    }
}
