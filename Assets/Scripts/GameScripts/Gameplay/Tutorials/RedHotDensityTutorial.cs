using UnityEngine;
using System.Collections;

public class RedHotDensityTutorial : RedHotDensity, ITutoriable
{
    public Bubble myBubbleGet;

    public Bubble myBubble { get; set; }

    protected override void OnEnable()
    {
        base.OnEnable();
        myBubble = myBubbleGet;
    }

    /// <summary>
    /// Al fer-se visible, afegim el portal a la llista del RedHotTarget i ens assegurem que el tag sigui correcte
    /// </summary>
    void OnBecameVisible()
    {
        gameObject.layer = Layers.RedHotDensity;
        RedHotTarget.instance.AddRedHotDensity(this.gameObject);
        ShowBubble();
        // Debug.Log("Visible!", this.gameObject);
    }

    /// <summary>
    /// Al sortir de pantalla, el treiem de la llista
    /// </summary>
    void OnBecameInvisible()
    {
        RedHotTarget.instance.RemoveRedHotDensity(this.gameObject);
        HideBubble();
        // Debug.Log("Invisible!", this.gameObject);
    }

    /// <summary>
    /// Si deshabilitem el portal també el treiem de la llista
    /// </summary>
    void OnDisable()
    {
        //Debug.Log("OnDisable called", this.gameObject);
        RedHotTarget.instance.RemoveRedHotDensity(this.gameObject);
        // Debug.Log("Invisible!", this.gameObject);
    }

    public void ShowBubble()
    {
        myBubble.gameObject.SetActive(true);
    }

    public void HideBubble()
    {
        myBubble.gameObject.SetActive(false);
    }
}
