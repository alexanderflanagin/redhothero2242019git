using UnityEngine;
using System.Collections;

/// <summary>
/// Interface per aquells objectes que han de mostrar globus de di�leg en els tutorials
/// </summary>
public interface ITutoriable {

    /// <summary>
    /// El globus de di�leg que t� assignat l'objecte
    /// </summary>
    Bubble myBubble { get; set; }

    /// <summary>
    /// Mostra el globus de di�leg
    /// </summary>
    void ShowBubble();

    /// <summary>
    /// Amaga el globus de di�leg
    /// </summary>
    void HideBubble();
}
