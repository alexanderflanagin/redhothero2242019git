using UnityEngine;
using System.Collections;

/// <summary>
/// Classe per a gestionar el comportament de les dots d'energia al tutorial
/// </summary>
public class RedHotDotTutorial : RedHotDot, ITutoriable {

    public Bubble myBubble { get; set; }

    private EnergyManager _energyManager;

    void Awake()
    {
        myBubble = this.GetComponentInChildren<Bubble>();

        ShowBubble();
    }

    public override void Pick(GameObject picker)
    {
        if (_valid)
        {
            HideBubble();
        }

        base.Pick(picker);
    }

    void Update()
    {
        if (_energyManager == null)
        {
            _energyManager = FindObjectOfType<EnergyManager>() as EnergyManager;
        }

        if ((myBubble.gameObject.activeInHierarchy && _energyManager.energy > EnergyManager.POINTS_PER_BATTERY) || picked)
        {
            // Si l'energia es major que una cell i estavem mostrant, amaguem
            HideBubble();
        }
        else if (!myBubble.gameObject.activeInHierarchy && _energyManager.energy < EnergyManager.POINTS_PER_BATTERY && !picked)
        {
            // Si l'energia és menor que una cell i no estavem mostrant, mostrem
            ShowBubble();
        }
    }

    public void ShowBubble()
    {
        myBubble.gameObject.SetActive(true);
    }

    public void HideBubble()
    {
        myBubble.gameObject.SetActive(false);
    }
}
