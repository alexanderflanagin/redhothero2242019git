using UnityEngine;
using System.Collections;

public class TutorialNoEnergy : MonoBehaviour {

    private bool _firstTime = true;

    private EnergyManager _energyManager;

    private bool _lowEnergy = false;

    public GameObject[] images;

    // Use this for initialization
	void Start () {
        foreach (GameObject img in images)
        {
            img.SetActive(false);
        }
	}
	
	// Update is called once per frame
	void Update () {
        if (_energyManager == null)
        {
            _energyManager = FindObjectOfType<EnergyManager>() as EnergyManager;
        }

        if (_energyManager.energy > EnergyManager.POINTS_PER_BATTERY)
        {
            _lowEnergy = false;
        }
        else if (_energyManager.energy < EnergyManager.POINTS_PER_BATTERY)
        {
            if (!_lowEnergy)
            {
                if (_firstTime)
                {
                    _firstTime = false;
                    StartCoroutine(firstTimeNoEnergy());
                }
                _lowEnergy = true;
                StartCoroutine(ShowCells());
            }
        }
	}

    private IEnumerator ShowCells()
    {
        int count = 0;

        float tutorialTime = 0.15f;

        while (_lowEnergy)
        {
            images[count].SetActive(true);
            yield return new WaitForSeconds(tutorialTime);
            images[(count + 1) % images.Length].SetActive(true);
            yield return null;
            images[count].SetActive(false);
            yield return new WaitForSeconds(tutorialTime);
            count = (count + 1) % images.Length;
        }

        foreach (GameObject img in images)
        {
            img.SetActive(false);
        }
    }

    private IEnumerator firstTimeNoEnergy()
    {
        Time.timeScale = 0f;
        yield return null;
        Time.timeScale = 1f;
    }
}
