﻿using UnityEngine;
using System.Collections;

public class PropMovement : MonoBehaviour {

	/*
	 * Speed relative to world absolute rotation
	 */
	public Vector3 speed;

	/*
	 * Speed relative to local rotation
	 */
	private Vector3 _localSpeed;
	public Vector3 localSpeed { get { return _localSpeed; }}


	void Awake () {
		_localSpeed = transform.InverseTransformDirection( speed );
	}
	
	void Update (){
		transform.Translate( _localSpeed * Time.deltaTime );
	}
}
