using UnityEngine;
using System.Collections;

public class WhaleParticleSystem : MonoBehaviour {

	public ParticleSystem outOfWater;
	public ParticleSystem splash;
	public ParticleSystem bubbles;
	public ParticleSystem smallSplash;

	public GameObject ripple;

	// Use this for initialization
	void Start () {
		if ( ripple != null )
			ripple.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void ParticlesPlayOnce ( ParticleSystem ps ) {
		ps.Play();
        ParticleSystem.EmissionModule emission = ps.emission;
        emission.enabled = true;
	}

//	void Particle

	void RippleIn() {
		if ( ripple != null )
			ripple.SetActive (true);
	}

	void RippleOut(){
		if ( ripple != null )
			ripple.SetActive (false);
	}

	IEnumerator OutOfWater(){
		outOfWater.Play();
        ParticleSystem.EmissionModule emission = outOfWater.emission;
        emission.enabled = true;

        yield return new WaitForSeconds (0.2f);
        emission.enabled = false;
		yield return new WaitForSeconds (0.5f);
		outOfWater.Stop();
	}

	IEnumerator Splash() {
		splash.Play();
        ParticleSystem.EmissionModule emission = splash.emission;
        emission.enabled = true;

        yield return new WaitForSeconds (0.2f);
        emission.enabled = false;
		yield return new WaitForSeconds (0.5f);
		splash.Stop();
	}

	IEnumerator Bubbles() {
		bubbles.Play();
        ParticleSystem.EmissionModule emission = bubbles.emission;
        emission.enabled = true;

        yield return new WaitForSeconds (0.2f);
        emission.enabled = false;
		yield return new WaitForSeconds (0.5f);
		bubbles.Stop();
	}

	IEnumerator SmallSplash() {
		smallSplash.Play();
        ParticleSystem.EmissionModule emission = smallSplash.emission;
        emission.enabled = true;

        yield return new WaitForSeconds (0.2f);
        emission.enabled = false;
		yield return new WaitForSeconds (0.5f);
		smallSplash.Stop();
	}
}
