﻿using UnityEngine;
using System.Collections;

public class WhaleAnimations : MonoBehaviour {
//
//	public bool cigar;
//	public bool jump0;
//	public bool jump1;
//	public bool swim;

	public enum WhaleAnim { cigar, jump0, jump1, swim };
	public WhaleAnim whaleState;

//	public enum PlayMode { Triggered, AutoOnce, AutoLoop };
//	public PlayMode playMode;

	private Animator _whaleAnimator;



	// Use this for initialization
	void Start () {
		_whaleAnimator = this.GetComponent<Animator>();
//		StartCoroutine ( "WhaleAnimController" );
		_whaleAnimator.speed = 0;
//		if ( playMode == PlayMode.AutoOnce ) {
//			GetComponentInChildren<Animation>().wrapMode = WrapMode.Once;
//			_whaleAnimator.GetCurrentAnimationClipState(0)[0].clip.wrapMode = WrapMode.Once;
//			StartCoroutine ( "WhaleAnimController" );
//		}
//
//		if ( playMode == PlayMode.AutoLoop ) {
//			StartCoroutine ( "WhaleAnimController" );
//			GetComponentInChildren<Animation>().wrapMode = WrapMode.Loop;

//			StartCoroutine ( "WhaleAnimController" );
//		}
	}
	
	// Update is called once per frame
	void Update () {
//		Debug.Log (whaleState);
	}

	void OnTriggerEnter2D ( Collider2D other ){
//		if (other.tag == "Player" && playMode == PlayMode.Triggered );
		StartCoroutine ( "WhaleAnimController" );
	}

//	IEnumerator 

	IEnumerator WhaleAnimController () {
//		while ( true ) {
		_whaleAnimator.speed = 1;
			if ( whaleState == WhaleAnim.cigar ){
				_whaleAnimator.SetBool ( "Cigar", true );
				_whaleAnimator.SetBool ( "Jump0", false );
				_whaleAnimator.SetBool ( "Jump1", false );
				_whaleAnimator.SetBool ( "Swim", false );
				yield return null;
			}

			if ( whaleState == WhaleAnim.jump0 ){

				_whaleAnimator.SetBool ( "Cigar", false );
				_whaleAnimator.SetBool ( "Jump0", true );
				_whaleAnimator.SetBool ( "Jump1", false );
				_whaleAnimator.SetBool ( "Swim", false );
				yield return null;
			}

			if ( whaleState == WhaleAnim.jump1 ){

				_whaleAnimator.SetBool ( "Cigar", false );
				_whaleAnimator.SetBool ( "Jump0", false );
				_whaleAnimator.SetBool ( "Jump1", true );
				_whaleAnimator.SetBool ( "Swim", false );
				yield return null;
			}

			if ( whaleState == WhaleAnim.swim ){
				_whaleAnimator.SetBool ( "Cigar", false );
				_whaleAnimator.SetBool ( "Jump0", false );
				_whaleAnimator.SetBool ( "Jump1", false );
				_whaleAnimator.SetBool ( "Swim", true );
				

			}
//		Debug.Log (_whaleAnimator.GetCurrentAnimationClipState(0)[0].clip.name + "" + _whaleAnimator.GetCurrentAnimationClipState(0)[0].clip.wrapMode);
//		_whaleAnimator.GetCurrentAnimationClipState(0)[0].clip.wrapMode = WrapMode.Once;
		yield return null;
//
//			yield return null;
//		}
	}
}
