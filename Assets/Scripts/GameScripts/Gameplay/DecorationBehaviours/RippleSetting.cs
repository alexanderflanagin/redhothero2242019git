﻿using UnityEngine;
using System.Collections;

public class RippleSetting: MonoBehaviour {

	public float size;
	public float emission;

	// Use this for initialization
	void Start () {
		transform.position = new Vector3 ( transform.position.x, transform.position.y, transform.position.z );
		GetComponent<ParticleSystem>().startSize = size;
		if ( transform.parent != null  && transform.parent.GetComponent<PropMovement>() != null ) {
//			Debug.Log ("Magnitude " + Vector3.Magnitude ( transform.parent.GetComponent<PropMovement>().localSpeed ) );
			GetComponent<ParticleSystem>().emissionRate =  Vector3.Magnitude ( transform.parent.GetComponent<PropMovement>().localSpeed );
		}
	}
}
