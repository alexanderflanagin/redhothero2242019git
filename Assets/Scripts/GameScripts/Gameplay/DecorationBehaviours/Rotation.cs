﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Rota un objecte per euler angles en relació al world
/// </summary>
public class Rotation : MonoBehaviour
{

    /// <summary>
    /// La rotació que li donarem per frame (es veurà afectada per el deltaTime)
    /// </summary>
    public Vector3 rotation;

    /// <summary>
    /// Fem rotar el GameObject cada frame amb els eulers que tenim en relació al World
    /// </summary>
    void Update()
    {
        transform.Rotate(rotation * Time.deltaTime, Space.World);
    }
}
