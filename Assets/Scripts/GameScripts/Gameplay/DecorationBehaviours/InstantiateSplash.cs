using UnityEngine;
using System.Collections;

/// <summary>
/// Use this script in water for creating watersplashes onTriggerEnter  
/// </summary>
public class InstantiateSplash : MonoBehaviour
{
    /// <summary>
    /// Numero d'instancies que tindrem per a llencar splash
    /// </summary>
    private static int numInstances = 5;    
    
    /// <summary>
    /// GameObject que conté la malla i la animació del splash
    /// </summary>
	public GameObject splash;

    /// <summary>
    /// So que sonara quan algu/alguna cosa s'enfonsi
    /// </summary>
    [AudioClip(AudioClipsData.ClipsGroups.Enviro)]
    public string drownAudioClip;

    /// <summary>
    /// Instancies de l'splash
    /// </summary>
    private GameObject[] splashInstances;

    /// <summary>
    /// Instancia actual que haurem de llencar
    /// </summary>
    private int currentInstanceIndex = 0;

    /// <summary>
    /// Al fer enable, instanciem l'splash
    /// </summary>
    void OnEnable()
    {
        splashInstances = new GameObject[numInstances];

        for(int i = 0; i < numInstances; i++)
        {
            splashInstances[i] = Instantiate(splash) as GameObject;

        }

        currentInstanceIndex = 0;
    }

    /// <summary>
    /// Al fer trigger, si l'element que hi entra és un s-tv o el player, instanciem l'splash
    /// </summary>
    /// <param name="other">El Collider2D amb el que fa trigger</param>
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag(Tags.Whatever) || other.gameObject.CompareTag(Tags.Player))
        {
            //Debug.Log("Triggering Element at: " + other.transform.position);
            StartCoroutine(SplashTheCasbah(other.transform.position));
        }
    }

    /// <summary>
    /// Instancia l'splash a la posició on ha caigut l'altre element
    /// </summary>
    /// <param name="other">El transform del GameObject que provoca l'splash</param>
    /// <returns></returns>
	IEnumerator SplashTheCasbah(Vector3 position)
    {
        GameObject splash = splashInstances[currentInstanceIndex];

        if(splash.activeInHierarchy)
        {
            // Ya se estaba usando este splash, no lo volvemos a coger
            yield break;
        }

        position.y = -0.2f;
        splash.transform.position = position;
        splash.SetActive(true);
        currentInstanceIndex = (currentInstanceIndex + 1) % numInstances;

        AudioManager.Instance.PlayOneShoot(AudioClipsData.ClipsGroups.Enviro, drownAudioClip);

        yield return new WaitForSeconds(1.4f);
        splash.SetActive(false);
    }
}
