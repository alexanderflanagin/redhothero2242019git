using UnityEngine;
using System.Collections;
using System;

/// <summary>
/// Classe que gestiona les cells
/// </summary>
public class Cell : Pickeable
{
    /// <summary>
    /// Override del m�tode per a quan un player agafa la cell
    /// </summary>
    /// <param name="picker">El GameObject que l'ha agafat</param>
    public override void Pick(GameObject picker)
    {
        // Afegim un d'energia al energy manager
        if (picker.GetComponent<EnergyManager>())
        {
            picker.GetComponent<EnergyManager>().addEnergy(EnergyManager.POINTS_PER_BATTERY);
        }

        StartCoroutine("Collected");
    }

    /// <summary>
    /// Coroutina per a avisar al player de que ha agafat la cell, animacions, etc.
    /// </summary>
    /// <returns></returns>
	private IEnumerator Collected() {
		Destroy( GetComponent<Rotation>() );

		// Deshabilitem la mesh i el collider
        StandardStageManager.current.player.GetComponent<CharacterAnimations>().CoinCollected();
		StandardStageManager.current.player.GetComponent<CharacterAudio>().ItemAudio();
		GetComponentInChildren<MeshRenderer>().enabled = false;
		GetComponent<Collider2D>().enabled = false;

        if (this.particleSystem != null && !this.particleSystem.isPlaying)
        {
            this.particleSystem.transform.rotation = Quaternion.Euler(Vector3.zero);
            this.particleSystem.Play();
        }

		// Avisem al ScenarioManager de que l'hem agafat
        StandardStageManager.current.PickeablePicked(currentScenarioArrayIndex);

		yield return new WaitForSeconds( 0.5f );
        this.particleSystem.Stop();
		// yield return new WaitForSeconds( 1f );
		Destroy( this.gameObject );
	}
}
