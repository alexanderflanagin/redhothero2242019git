using UnityEngine;
using System.Collections;

public class FusePart : Pickeable
{
    public override void Pick(GameObject picker)
    {
        if (picker.GetComponent<EnergyManager>())
        {
            picker.GetComponent<EnergyManager>().addEnergy(EnergyManager.POINTS_PER_BATTERY);
        }

        StartCoroutine(Picked());
    }

    public IEnumerator Picked()
    {
        Destroy(GetComponent<Rotation>());

        // Deshabilitem la mesh i el collider
        StandardStageManager.current.player.GetComponent<CharacterAnimations>().CoinCollected();
        StandardStageManager.current.player.GetComponent<CharacterAudio>().ItemAudio();
        GetComponentInChildren<MeshRenderer>().enabled = false;
        GetComponent<Collider2D>().enabled = false;

        if (this.particleSystem != null && !this.particleSystem.isPlaying)
        {
            this.particleSystem.transform.rotation = Quaternion.Euler(Vector3.zero);
            this.particleSystem.Play();
        }

        // Avisem al ScenarioManager de que l'hem agafat
        StandardStageManager.current.PickeablePicked(this.currentScenarioArrayIndex);

        yield return new WaitForSeconds(0.5f);
        this.particleSystem.Stop();
        // yield return new WaitForSeconds( 1f );
        Destroy(this.gameObject);
    }
	
}
