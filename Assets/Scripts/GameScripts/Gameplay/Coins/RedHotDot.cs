using UnityEngine;
using System.Collections;

/// <summary>
/// Classe per a gestionar el comportament de les dots d'energia
/// </summary>
public class RedHotDot : Pickeable
{
    [AudioClip(AudioClipsData.ClipsGroups.Player)]
    public string pickUpAudio;

    /// <summary>
    /// Per a saber si la dot es v�lida (diria que no es necessari)
    /// </summary>
    protected bool _valid;

    /// <summary>
    /// Animator de la dot (per el desapar�ixer)
    /// </summary>
    protected Animator _animator;

    /// <summary>
    /// És una super-dot?
    /// </summary>
    public bool super = false;

    /// <summary>
    /// Al fer enable, la posem com a no agafada, i recuperem l'animator
    /// </summary>
    void OnEnable()
    {
        _valid = true;
        
        _animator = GetComponentInChildren<Animator>();

        if (_animator)
        {
            _animator.SetBool("Picked", false);
        }

        picked = false;
    }
    
    /// <summary>
    /// M�tode pick que poden extendre els fills. Al ser agafada recuperem energia i iniciem la coroutina de picked
    /// </summary>
    /// <param name="picker">El GameObject que l'ha agafat</param>
    public override void Pick(GameObject picker)
    {
        if (_valid)
        {
            // Afegim un d'energia al energy manager
            if (picker.GetComponent<EnergyManager>())
            {
                if (super)
                {

                    picker.GetComponent<EnergyManager>().addEnergy(EnergyManager.SUPER_DOT_REGEN_POINTS);
                }
                else
                {
                    picker.GetComponent<EnergyManager>().addEnergy(EnergyManager.DOT_REGEN_POINTS);
                }
            }

            AudioManager.Instance.PlayOneShoot(AudioClipsData.ClipsGroups.Player, pickUpAudio);
            StartCoroutine("PickAnimation", picker);
            _valid = false;
        }
    }

    /// <summary>
    /// La coroutina de picked. Avisem al animator (si en t�) i deshabilitem la dot
    /// </summary>
    /// <param name="picker">El GameObject que l'ha agafat</param>
    /// <returns></returns>
    protected IEnumerator PickAnimation(GameObject picker)
    {
        if (_animator)
        {
            _animator.SetBool("Picked", true);
        }

        if (regen)
        {
            deactivateDot();
            yield return new WaitForSeconds(regenTime);
            activateDot();
        }
        else
        {
            gameObject.SetActive(false);
        }
        
    }

    /// <summary>
    /// Desactiva tots els components de la dot
    /// </summary>
    private void deactivateDot()
    {
        foreach (Collider2D col in this.GetComponents<Collider2D>())
        {
            col.enabled = false;
        }

        if (particleSystem != null)
        {
            particleSystem.enableEmission = false;
            particleSystem.gameObject.SetActive(false);
        }
    }

    /// <summary>
    /// Activa els components de la dot
    /// </summary>
    private void activateDot()
    {
        foreach (Collider2D col in this.GetComponents<Collider2D>())
        {
            col.enabled = true;
        }

        if (particleSystem != null)
        {
            particleSystem.gameObject.SetActive(true);
            particleSystem.enableEmission = true;
        }

        _valid = true;

        if (_animator)
        {
            _animator.SetBool("Picked", false);
        }

        picked = false;
    }
}
