using UnityEngine;
using System.Collections;

public class Pickeable : MonoBehaviour
{
    /// <summary>
    /// Posicio d'aquesta moneda dins del escenari actual
    /// </summary>
    public int currentScenarioArrayIndex { get; set; }
    
    /// <summary>
    /// Ens indica si la l'hem agafat o no
    /// </summary>
    protected bool picked;

    /// <summary>
    /// Si s'ha de regenerar al cap de regenTime
    /// </summary>
    public bool regen = true;

    /// <summary>
    /// Temps que passa absn de que torni a generar partícules després de ser agafada
    /// </summary>
    public float regenTime = 4f;

    /// <summary>
    /// El sistema de partícules de la dot
    /// </summary>
    public new ParticleSystem particleSystem;

    public virtual void Pick(GameObject picker)
    {

    }

    /// <summary>
    /// Al detectar un trigger, si �s del charactercontrol i no est� picked, li fem un Pick
    /// </summary>
    /// <param name="other">El collider2d que ha entrat</param>
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag(Tags.Player) && !picked)
        {
            picked = true;
            Pick(other.gameObject);
        }
    }
}
