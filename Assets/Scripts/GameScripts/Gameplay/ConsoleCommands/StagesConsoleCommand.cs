using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;

public class StagesConsoleCommand
{
    // Possible strings
    public const string SHOW = "show";

    public const string ADD = "add";

    public const string REMOVE = "rm";

    public const string HELP = "help";

    public static string stagesData(params string[] args)
    {
        if(args.Length == 0 || args[0] == SHOW)
        {
            return showData();
        }
        else
        {
			List<string> argsList;
			// Eliminem el primer argument perque ja no ens cal
            switch (args[0])
            {
                case ADD:
					argsList = new List<string>(args);    
					argsList.RemoveAt(0);
					args = argsList.ToArray();
                    string toReturn = addStages(args);

                    if (WorldMapManager.current != null)
                    {
                        WorldMapManager.current.PrepareStageDots();
                    }

                    return toReturn;
                case REMOVE:
					argsList = new List<string>(args);    
					argsList.RemoveAt(0);
					args = argsList.ToArray();
                    return removeStages(args);
                case HELP:
                case "h":
                case "?":
                    return help();
            }
        }

        return "";
    }

    public static string help()
    {
        string text = "You can see all the stages info just with:\nstages\nor using:\nstages show\n\n";
        text += "You can add info about completed stages with:\nstages add NUM\nwhere num is the enviro plus the order of the stage in that enviro.\n";
        text += "For example, to add the third stage of coast you must write:\nstages add 02\n";
        text += "You can also add a range of stages separating them with a \"-\". For example:\n";
        text += "stages add 00-09 #add all the first enviro";
        return text;
    }

    public static string showData()
    {
        string toReturn = "";
        for (int i = 0; i < GameManager.Instance.gameData.stagesOrdered.Length; i++)
        {
            for (int j = 0; j < GameManager.Instance.gameData.stagesOrdered[i].stages.Length; j++)
            {
                int id = GameManager.Instance.gameData.stagesOrdered[i].stages[j];
                toReturn += string.Format("{0}{1} - {2}", i, j, GameManager.Instance.gameData.stagesData[id].sceneName);

                PlayerStageData levelData;
                if (GameManager.Instance.playerData.levelsData.TryGetValue(GameManager.Instance.gameData.stagesData[id].sceneName, out levelData))
                {
                    toReturn += " ==> [x]";
                }
                else
                {
                    toReturn += " ==> [ ]";
                }

                toReturn += "\n";
            }
            toReturn += "======================================\n";
        }

        return toReturn;
    }

    public static string addStages(params string[] args)
    {
        if(args.Length == 0)
        {
            for(int i = 0; i < GameManager.Instance.gameData.stagesOrdered.Length; i++)
            {
                for(int j = 0; j < GameManager.Instance.gameData.stagesOrdered[i].stages.Length; j++)
                {
                    int id = GameManager.Instance.gameData.stagesOrdered[i].stages[j];

                    if (!GameManager.Instance.playerData.levelsData.ContainsKey(GameManager.Instance.gameData.stagesData[id].sceneName))
                    {
                        addStageData(i, j);

                        return "Next stage added. Enviro " + i + " stage " + j;
                    }
                }
            }

            return "No stage added because all stages were added";
        }

        Regex validator = new Regex(@"^[0-2][0-9]$");
        if(validator.IsMatch(args[0]))
        {
            // Es una sola pantalla
            int enviro = (int)Char.GetNumericValue(args[0][0]);
            int stage = (int)Char.GetNumericValue(args[0][1]);

            addStageData(enviro, stage);

            return "Es la pantalla " + stage + " del enviro " + enviro + " i es un valor correcte";
        }

        validator = new Regex(@"^[0-2][0-9]\-[0-2][0-9]$");
        if(validator.IsMatch(args[0]))
        {
            // Es un range
            int enviroStart = (int)Char.GetNumericValue(args[0][0]);
            int stageStart = (int)Char.GetNumericValue(args[0][1]);

            int enviroEnd = (int)Char.GetNumericValue(args[0][3]);
            int stageEnd = (int)Char.GetNumericValue(args[0][4]);

            for(int i = enviroStart; i <= enviroEnd; i++)
            {
                for(int j = 0; j < 10; j++)
                {
                    if(i == enviroStart && j < stageStart)
                    {
                        continue;
                    }
                    if(i == enviroEnd && j > stageEnd)
                    {
                        break;
                    }

                    addStageData(i, j);
                }
            }
            return "Es un rang de pantalles correcte";
        }

        return "Invalid stage";
    }

    public static string removeStages(params string[] args)
    {
        return "stage removed";
    }

    private static void addStageData(int enviro, int stage, bool finished = false, float time = float.MaxValue, bool[] pickeables = null)
    {
        pickeables = pickeables ?? new bool[4] { false, false, false, false };

        int id = GameManager.Instance.gameData.stagesOrdered[enviro].stages[stage];

        string key = GameManager.Instance.gameData.stagesData[id].sceneName;
        PlayerStageData playerLevelData = new PlayerStageData();
        playerLevelData.unlocked = true;
        playerLevelData.finished = finished;
        playerLevelData.bestTime = time;
        playerLevelData.pickeablesPicked = pickeables;
        
        if(!GameManager.Instance.playerData.levelsData.ContainsKey(key))
        {
            GameManager.Instance.playerData.levelsData.Add(key, playerLevelData);
        }
        else
        {
            Debug.Log("Adding an alredy created stage. Ignoring: " + enviro + " - " + stage);
        }
    }
}
