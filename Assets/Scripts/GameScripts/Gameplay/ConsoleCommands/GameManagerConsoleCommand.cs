using UnityEngine;
using System;
using System.Collections.Generic;
using System.IO;

public class GameManagerConsoleCommand
{
    public GameManagerConsoleCommand()
    {
        ConsoleCommandsRepository repo = ConsoleCommandsRepository.Instance;
        repo.RegisterCommand("help", Help);
        repo.RegisterCommand("player-data", ViewPlayerData);
        repo.RegisterCommand("clear-data", RemovePlayerData);

        repo.RegisterCommand("stages", StagesConsoleCommand.stagesData);

        repo.RegisterCommand("save", save);

        repo.RegisterCommand("p", Pause);
        repo.RegisterCommand("unpause", UnPause);

        // Chetos
        repo.RegisterCommand("pacomode", GodMode);
        repo.RegisterCommand("e", GetEnergy);
    }

    public string Help(params string[] args)
    {
        string text = "p -> Pause\n";
        text += "unpause -> Unpause\n";
        text += "player-data [act|file]-> View Player Data\n";
        text += "clear-data -> Remove player data file\n";
        text += "player-aim -> Change player aim type";
        text += "stages -> Show all stages data";
        text += "save -> Save current player config";

        return text;
    }

    public string ViewPlayerData(params string[] args)
    {
        string text = "Player info:\n";
        text += "Cells: {0}\n";
        text += "Fuse parts: {1}\n";
        text += "PowerGlove: {2}\n";
        text += "BlastGlove: {3}\n";

        string forStageInfo = "{0} - Time: {1} | Pickeables: {2} {3} {4} | Speed run done: {5}\n";

        List<string> values = new List<string>();

        if (args.Length == 0 || args[0] == "act")
        {
            values.Add(GameManager.Instance.playerData.totalCells.ToString());
            values.Add(GameManager.Instance.playerData.totalFuseParts.ToString());
            values.Add(GameManager.Instance.playerData.powerGlove.ToString());
            values.Add(GameManager.Instance.playerData.blastGlove.ToString());

            foreach (KeyValuePair<string, PlayerStageData> pld in GameManager.Instance.playerData.levelsData)
            {
                text += string.Format(forStageInfo, pld.Key, pld.Value.bestTime, pld.Value.pickeablesPicked[0], pld.Value.pickeablesPicked[1], pld.Value.pickeablesPicked[2], pld.Value.speedrunDone);
            }
        }
        else if(args[0] == "file")
        {
            bool oldLoadPlayerDataFile = GameManager.Instance.gameData.gameData.loadPlayerDataFile;
            GameManager.Instance.gameData.gameData.loadPlayerDataFile = true;

            PlayerDataHolder pdh = ScriptableObject.CreateInstance(typeof(PlayerDataHolder)) as PlayerDataHolder;
            pdh.Load();

            GameManager.Instance.gameData.gameData.loadPlayerDataFile = oldLoadPlayerDataFile;

            values.Add(pdh.totalCells.ToString());
            values.Add(pdh.totalFuseParts.ToString());
            values.Add(pdh.powerGlove.ToString());
            values.Add(pdh.blastGlove.ToString());

            foreach (KeyValuePair<string,PlayerStageData> pld in pdh.levelsData)
            {
                text += string.Format(forStageInfo, pld.Key, pld.Value.bestTime, pld.Value.pickeablesPicked[0], pld.Value.pickeablesPicked[1], pld.Value.pickeablesPicked[2], pld.Value.speedrunDone);
            }
        }

        return string.Format(text, values.ToArray());
    }

    public string RemovePlayerData(params string[] args)
    {
        if (File.Exists(Application.persistentDataPath + "/redHotHero.srhh"))
        {
            File.Delete(Application.persistentDataPath + "/redHotHero.srhh");

            GameManager.Instance.LoadPlayerData();

            if (WorldMapManager.current != null)
            {
                WorldMapManager.current.PrepareStageDots();
            }

            return "File deleted and player data reset";
        }
        else
        {
            return "No save file found";
        }
    }

    public string save(params string[] args)
    {
        GameManager.Instance.playerData.Save();
        return "Saved";
    }

    public string Pause(params string[] args)
    {
        Time.timeScale = 0;
        return "Paused";
    }

    public string UnPause(params string[] args)
    {
        Time.timeScale = 1;
        return "Unpaused";
    }

    

    public string GodMode(params string[] args)
    {
        GameManager.Instance.playerData.godMode = !GameManager.Instance.playerData.godMode;

        if (GameManager.Instance.playerData.godMode)
            return "Pacomode enabled";
        else
            return "Pacomode disabled";
    }

    public string GetEnergy(params string[] args)
    {
        string text;

        if (!StandardStageManager.current)
        {
            text = "[ERROR] You aren't in a Standard Stage, so no player loaded";
            return text;
        }

        if (!StandardStageManager.current.playerControl)
        {
            text = "[ERROR] No player loaded";
            return text;
        }

        StandardStageManager.current.playerControl.suit.energy.energy = EnergyManager.POINTS_PER_BATTERY * StandardStageManager.current.playerControl.suit.energy.nBatteries;
        return "Energy";
    }
}
