using UnityEngine;
using System.Collections;

public class RedHotDensity : MonoBehaviour
{
    /// <summary>
    /// Particle System de la red hot density
    /// </summary>
    public ParticleSystem particles;

    /// <summary>
    /// Should this density regenerate itself?
    /// </summary>
    public bool isFactory;

    /// <summary>
    /// El rotation de la cell, per a poder-lo engegar i apagar
    /// </summary>
    public GameObject densityOffGraphics;

    /// <summary>
    /// Time between the density is picked and can be picked again
    /// </summary>
    public float regenerateTime;

    /// <summary>
    /// Time between the particles are regenerated and the density is usefull again
    /// </summary>
    public float aliveTimeBeforeUsefull;

    /// <summary>
    /// Hash per a la animació d'absorvir
    /// </summary>
    [HideInInspector]
    protected int absorvHash = Animator.StringToHash("Absorv");

    /// <summary>
    /// Animator del particle system
    /// </summary>
    [HideInInspector]
    public Animator animator;

    protected bool inUse;

    /// <summary>
    /// El collider, per a poder-lo habilitar i deshabilitar
    /// </summary>
    protected Collider2D[] colliders;

    /// <summary>
    /// WaitForSeconds cached for regenerate particles (if isFactory)
    /// </summary>
    protected WaitForSeconds regenerationTime;

    /// <summary>
    /// WaitForSeconds cached for let particles grow before make it usable again (if isFactory)
    /// </summary>
    protected WaitForSeconds afterParticlesStartTime;

    public bool InUse
    {
        get
        {
            return inUse;
        }
    }

    protected virtual void Awake()
    {
        animator = GetComponentInChildren<Animator>();

        if (isFactory)
        {
            regenerationTime = new WaitForSeconds(regenerateTime - aliveTimeBeforeUsefull);
            afterParticlesStartTime = new WaitForSeconds(aliveTimeBeforeUsefull);
        }
    }

    protected virtual void OnEnable()
    {
        if(isFactory)
        {
            densityOffGraphics.SetActive(false);
        }

        this.colliders = this.GetComponents<Collider2D>();

        foreach (Collider2D col in this.colliders)
        {
            col.enabled = true;
        }

        // Per si s'ha tallat la coroutina de moures
        particles.transform.localPosition = Vector3.zero;
        animator.SetBool(absorvHash, false);

        if (!isFactory)
        {
            StandardStageManager.current.activeDensities.Add(this);
        }
    }

    /// <summary>
    /// Al fer-se visible, afegim el portal a la llista del RedHotTarget i ens assegurem que el tag sigui correcte
    /// </summary>
    void OnBecameVisible()
    {
        gameObject.layer = Layers.RedHotDensity;
        RedHotTarget.instance.AddRedHotDensity(gameObject);
    }

    /// <summary>
    /// Al sortir de pantalla, el treiem de la llista
    /// </summary>
    void OnBecameInvisible()
    {
        if (RedHotTarget.instance)
        {
            RedHotTarget.instance.RemoveRedHotDensity(gameObject);
        }
    }

    /// <summary>
    /// Si deshabilitem el portal també el treiem de la llista
    /// </summary>
    void OnDisable()
    {
        if (RedHotTarget.instance)
        {
            RedHotTarget.instance.RemoveRedHotDensity(gameObject);
        }

        if (!isFactory)
        {
            StandardStageManager.current.activeDensities.Remove(this);
            inUse = false;
        }

        StopAllCoroutines();
    }

    /// <summary>
    /// If player touches it, goes to player
    /// </summary>
    /// <param name="other"></param>
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.layer == Layers.Player)
        {
            StartCoroutine(GoToPlayer());
        }
    }

    /// <summary>
    /// Agafem aquesta density del pooler
    /// </summary>
    public void Serve()
    {
        gameObject.SetActive(true);
        inUse = true;
    }

    /// <summary>
    /// Retornem aquesta density al pooler
    /// </summary>
    public void Release()
    {
        gameObject.SetActive(false);
        inUse = false;
    }

    private IEnumerator GoToPlayer()
    {
        CharacterControl player = StandardStageManager.current.playerControl;
        
        // Si no tenim partícules (no es veuria res) abandonem
        if (!this.particles)
        {
            yield break;
        }

        // Deshabilitem colliders i treiem la RHD del RHT
        foreach (Collider2D col in this.colliders)
        {
            col.enabled = false;
        }

        RedHotTarget.instance.RemoveRedHotDensity(this.gameObject);

        // Avisem a l'animator
        this.animator.SetBool(absorvHash, true);

        if(isFactory)
        {
            densityOffGraphics.SetActive(true);
        }

        // Donem l'energia al player
        player.suit.energy.addEnergy(EnergyManager.SUPER_DOT_REGEN_POINTS);

        // Movem les partícules
        float speed = 100f;
        float followingTime = 1f;
        float startTime = Time.time;

        while (startTime + followingTime > Time.time)
        {
            this.particles.transform.position = Vector3.MoveTowards(this.particles.transform.position, player.transform.position + Vector3.up, speed * Time.deltaTime);
            
            yield return null;
        }

        // Tornem les partícules a lloc
        particles.transform.localPosition = Vector3.zero;

        if(isFactory)
        {
            StartCoroutine(RegenerateDensity());
        }
        //else
        //{
        //    Release();
        //}
    }

    private IEnumerator RegenerateDensity()
    {
        yield return regenerateTime;

        animator.SetBool(absorvHash, false);
        densityOffGraphics.SetActive(false);

        yield return afterParticlesStartTime; // Deixem que creixin una mica les partícules

        foreach (Collider2D col in this.colliders)
        {
            col.enabled = true;
        }

        if (this.GetComponent<Renderer>().isVisible)
        {
            RedHotTarget.instance.AddRedHotDensity(this.gameObject);
        }
    }

}
