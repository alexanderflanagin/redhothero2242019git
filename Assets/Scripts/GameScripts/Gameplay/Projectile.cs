using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour
{
    /// <summary>
    /// Temps que dura un projectil abans d'apagar-se sol
    /// </summary>
    public static float PROJECTILE_LIFETIME = 3f;

    /// <summary>
    /// Temps que dura un projectil que ha re-entrat a la pantalla (en general per a pantalles d'scroll vertical) abans d'apagar-se sol
    /// </summary>
    public static float PROJECTILE_REENTER_LIFETIME = 0.3f;

    /// <summary>
    /// Per a saber qui ha disparat el projectil
    /// </summary>
    public GameObject owner;

    /// <summary>
    /// Particules que surten al destruir un projectil
    /// </summary>
    public ParticleSystem onDestroyParticles;

    /// <summary>
    /// Particles that make the tray (if necessary)
    /// </summary>
    public TrailRenderer trail;

    /// <summary>
    /// Al habilitar el projectil, preparem ja per a que es destrueixi al cap de PROJECTILE_LIFETIME segons
    /// </summary>
    protected void OnEnable()
    {
        Invoke("OnDestroyProjectile", PROJECTILE_LIFETIME);
    }

    /// <summary>
    /// Si el projectil es deshabilita, cancel�lem el Invoke (per si no ha mort per temps, que al tornar a neixer no mori per el temps anterior)
    /// </summary>
    protected void OnDisable()
    {
        CancelInvoke("OnDestroyProjectile");
    }

    /// <summary>
    /// Que passa quan es destrueix el projectil
    /// </summary>
    public virtual void OnDestroyProjectile()
    {
        if (onDestroyParticles != null)
        {
            ParticleSystem particles = Instantiate(onDestroyParticles, transform.position, Quaternion.identity) as ParticleSystem;
            particles.GetOrAddComponent<ParticleSystemAutoDestroy>();
        }

        gameObject.SetActive(false);
    }
}
