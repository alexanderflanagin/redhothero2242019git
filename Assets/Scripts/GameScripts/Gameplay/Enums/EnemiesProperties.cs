

public class EnemiesProperties
{
	public static float LINER_DEFAULT_SPEED = MovementProperties.SPRINT_SPEED;

    public static float WALKER_DEFAULT_SPEED = 10f;

    public static float WHATEVER_JUMP_MARGIN = 0.3f;

    public static float WHATEVER_SLIDE_SPEED = 6.0f;

}
