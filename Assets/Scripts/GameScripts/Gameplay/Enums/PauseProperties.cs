using UnityEngine;
using System.Collections;

public class PauseProperties
{
    public bool paused;

    public float speed;

    public float verticalSpeed;

    public int movementDirection;

    public CharacterControl.StateAlive stateAlive;

    public PlayerState.Horizontal stateHorizontal;

    public PlayerState.Vertical stateVertical;
}
