using UnityEngine;
using System.Collections;

public class CheckPointProperties
{
    /// <summary>
    /// ID de l'stage actual, per a assegurar-nos que no agafem info d'un stage anterior
    /// </summary>
    public int currentStage;
    
    /// <summary>
    /// Si el pròxim nivell que comencem el comencem ja en el spawn point
    /// </summary>
    public bool startInCheckPoint;

    /// <summary>
    /// Si hem carregat aquesta escena des del check point
    /// </summary>
    public bool alreadyLoadedFromCheckPoint;

    /// <summary>
    /// Temps que porta jugant aquesta pantalla
    /// </summary>
    public float currentTime;

    public bool[] coinsPickedPreCheckPoint;

    /// <summary>
    /// Si ja té el power glove (només ens servirà a la pantalla on agafes el power glove)
    /// </summary>
    public bool powerGlove;

    /// <summary>
    /// Si ja té el blast glove (només ens servirà a la pantalla on agages el blast glove)
    /// </summary>
    public bool blastGlove;
}
