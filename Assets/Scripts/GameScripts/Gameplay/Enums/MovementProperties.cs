using UnityEngine;
using System;

[Serializable]
public class MovementProperties
{
    /// <summary>
    /// Velocitat del run
    /// </summary>
    public const float RUN_SPEED = 12f;

    /// <summary>
    /// Acceleraci� en estat run
    /// </summary>
    //public const float RUN_ACCELERATION = 32f;

    public AnimationCurve runAcceleration = AnimationCurve.Linear(0, 0, 0.25f, 1);

    public AnimationCurve onAiracceleration = AnimationCurve.Linear(0, 0, 0.5f, 1);

    /// <summary>
    /// Llindar d'acabar un moviment. Si el moviment �s menor a aquest valor, el considerem m�nim i per tant 0
    /// </summary>
    public const float STOP_MOVEMENT_THRESHOLD = 0.15f;

    /// <summary>
    /// In�rcia del run
    /// </summary>
    //public const float RUN_INERTIA = 445f;
    public AnimationCurve runInertia = AnimationCurve.Linear(0, 1, 0.25f, 0);
	
    /// <summary>
    /// Velocitat de l'sprint
    /// </summary>
	public const float SPRINT_SPEED = 12f;

    public const float SUPERSPRINT_SPEED = 12f;

    public const float SPRINT_TO_SUPERSPRINT_TIME = 2f;

    /// <summary>
    /// Velocitat horitzontal a la qual sortim disparats des d'una paret
    /// </summary>
    public const float WALL_JUMP_SPEED = 14f;

    public const float WALL_JUMP_AXIS_FREEZE_TIME = 0.2f;

    public const float WALL_GRAB_DISTANCE_CHECKER = 0.1f;

    /// <summary>
    /// Temps m�xim del roll
    /// </summary>
	public const float ROLL_MAX_TIME = 0.3f;

    /// <summary>
    /// Temps m�nim que ha de passar entre que es surt d'un roll i s'entra al seg�ent
    /// </summary>
	public const float ROLL_MIN_ARAM_REROLL = 0.2f;

    /// <summary>
    /// Temps de l'stun
    /// </summary>
    [Obsolete("Ja no fem servir stun i per tant no l'hauriem de tenir")]
	public const float STUN_TIME = 2f;

	/// <summary>
	/// Acceleraci� del player a l'aire
	/// </summary>
	public const float JUMP_ACCELERATION = 32f; // 0.125"

    /// <summary>
    /// Divideix la inèrcia del player quan es prem el joystick en direcció contraria a la direcció actual
    /// </summary>
    public const float INERTIA_CORRECTION_DIVIDER = 2f;

    /// <summary>
    /// Divideix la inèrcia del player quan es prem el joystick en direcció contraria a la direcció actual i som a l'aire
    /// </summary>
	public const float ON_AIR_INERTIA_CORRECTION_DIVIDER = 4f;
	
	/// <summary>
	/// Velocitat actual del player
	/// </summary>
	public float speed = 0f;
	
    /// <summary>
    /// Direcci� actual de moviment
    /// </summary>
	public int runDirection = 0;

    /// <summary>
    /// Enter que indica cap a quina direcci� es mira (sobre l'eix de les X)
    /// </summary>
    public int lookingDirection;

    /// <summary>
    /// Si pot tornar a fer roll o no (depen d'una variable de temps externa).
    /// </summary>
    public bool canRoll = true;

    /// <summary>
    /// L'ultim frame que hem fet un wallgrab
    /// </summary>
    public int lastFrameWallGrab;

    public void Reset()
    {
        lookingDirection = 1;
        runDirection = 1;
        lastFrameWallGrab = 0;
        speed = 0;
        canRoll = true;
    }
}
