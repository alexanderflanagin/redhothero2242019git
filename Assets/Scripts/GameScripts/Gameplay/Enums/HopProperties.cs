using UnityEngine;

/// <summary>
/// Classe amb les dades relacionades amb el Hop
/// </summary>
public class TravelProperties {
	// Constants

    /// <summary>
    /// Multiplicador vertical a la força de sortida del hop (es multiplica al temps que s'ha estat en hop)
    /// </summary>
	public const float OUT_VERTICAL_SPEED_MULTIPLIER = 270f;

    /// <summary>
    /// Multiplicador horitzontal a la for�a de sortida del hop (es multiplica al temps que s'ha estat en hop)
    /// </summary>
	public const float OUT_HORIZONTAL_SPEED_MULTIPLIER = 270f;

    /// <summary>
    /// Velocitat m�xima de sortida vertical del hop
    /// </summary>
    public static float MAX_OUT_VERTICAL_SPEED = 17f;

    /// <summary>
    /// Velocitat m�xima de sortida horitzontal del hop
    /// </summary>
    public static float MAX_OUT_HORIZONTAL_SPEED = 22.5f;
	
	// Changing values
    /// <summary>
    /// Time.time en el que ha comen�at el hop
    /// </summary>
	public float travelStartTime = 0f;

    /// <summary>
    /// Posici� inicial del hop
    /// </summary>
    public Vector2 travelStartPosition;
}
