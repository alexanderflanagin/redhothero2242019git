using UnityEngine;

[System.Serializable]
public class JumpProperties
{
	/// <summary>
	/// For�a de sortida per al salt normal
	/// </summary>
	public const float JUMP_FORCE = 23.5f;

    public const float JUMP_ONE_HEIGHT = 1.6f;
    
    public const float JUMP_TWO_HEIGHT = 2.2f;

    public const float JUMP_THREE_HEIGHT = 3.2f;

    public const float JUMP_FOUR_HEIGHT = 4.2f;

    public const float JUMP_CANCELL_FORCE = 6f;

    /// <summary>
    /// Gravetat est�ndard
    /// </summary>
	public const float GRAVITY = 50f;

    /// <summary>
    /// Gravetat 0
    /// </summary>
    public const float NO_GRAVITY = 0f;

    /// <summary>
    /// Al�ada del rebot sobre un enemic sense salt
    /// </summary>
	public const float LOW_BOUNCE_HEIGHT = 10f;
	
    /// <summary>
    /// Al�ada del rebot sobre un enemic en cas de ser el primer salt consecutiu
    /// </summary>
	public const float FIRST_BOUNCE_JUMP_HEIGHT = 26f;

    /// <summary>
    /// Al�ada del rebot sobre un enemic en cas de ser el segon salt consecutiu
    /// </summary>
	public const float SECOND_BOUNCE_JUMP_HEIGHT = 26f;

    /// <summary>
    /// Al�ada del rebot sobre un enemic en cas de ser el tercer salt consecutiu
    /// </summary>
	public const float THIRD_BOUNCE_JUMP_HEIGHT = 29f;

    /// <summary>
    /// Alçada del salt quan es fa des d'una paret
    /// </summary>
    public const float WALL_JUMP_HEIGHT = 18f;

	/// <summary>
	/// Temps que pot estar caient el Player abans de perdre la possibilitat de saltar "a l'aire"
	/// </summary>
	public const float FALLING_TIME_CAN_JUMP = 0.15f;

    /// <summary>
    /// Temps anterior a tocar a terra que es pot pr�mer el bot� de salt per a saltar nom�s de tocar el terra
    /// </summary>
	public const float PREVIOS_JUMP_PRESSED_CAN_JUMP = 0.2f;
	
    /// <summary>
    /// Velocitat m�xima de caiguda
    /// </summary>
	public const float MAX_FALLING_SPEED = 19f;

    /// <summary>
    /// El quoficient de la friccio que pateix el player al fer wall grab, per saber quan es relanteix la caiguda
    /// </summary>
    public const float WALL_GRAB_FRICTION = 1f;

    public AnimationCurve jumpInertia = AnimationCurve.Linear(0, 1, 1, 0);

	/// <summary>
	/// Gravetat actual (l'assignem a 0 quan fem travels)
	/// </summary>
    public float currentGravity = GRAVITY;

    /// <summary>
    /// Al�ada del bounce. En un principi, /LOW_BOUNCE_HEIGHT/
    /// </summary>
	public float bounceHeight = LOW_BOUNCE_HEIGHT;
	
    /// <summary>
    /// Velocitat vertical que portem en aquest moment
    /// </summary>
	public float verticalSpeed = 0f;

    /// <summary>
    /// Per a limitar el Jump des del moviment horitzotanl
    /// </summary>
    public bool canJump = true;

    /// <summary>
    /// Portem un control de quants bounce hem fet.
    /// </summary>
    public int bounceCount = 0;

    public float fallingTime = 0;

    public void Reset()
    {
        currentGravity = GRAVITY;
        bounceHeight = LOW_BOUNCE_HEIGHT;
        verticalSpeed = 0;
        canJump = true;
        bounceCount = 0;
        fallingTime = 0;
    }
}
