using UnityEngine;
using System.Collections;

public class BlueSubstance : MonoBehaviour
{
    public Vector3 particlesOffset = new Vector3(0, 1);

    [AudioClip(AudioClipsData.ClipsGroups.Enviro)]
    public string inClip = "BlueSubstanceInOut";

    [AudioClip(AudioClipsData.ClipsGroups.Enviro)]
    public string outClip = "BlueSubstanceInOut";

    /// <summary>
    /// If isCage, we don't need the animator neither the sounds
    /// </summary>
    public bool isCage = false;

    private new Collider2D collider;

	private Animator animator;

    void Start()
    {
        this.collider = this.GetComponent<Collider2D>();

		this.animator = this.GetComponent<Animator>();

        if(isCage)
        {
            animator.enabled = false;
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if(isCage)
        {
            return;
        }

        if (other.gameObject.layer == Layers.Player || other.gameObject.layer == Layers.Enemy)
        {
            ShowParticles(other, true);
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (isCage)
        {
            return;
        }

        if (other.gameObject.layer == Layers.Player || other.gameObject.layer == Layers.Enemy)
        {
            ShowParticles(other, false);
        }
    }

    void ShowParticles(Collider2D other, bool enter)
    {
        if (GeneralPurposeObjectManager.Instance.BlueSubstanceCross)
        {
            ParticleSystem enterParticles;
            if (this.transform.position.x < other.transform.position.x)
            {
                enterParticles = Instantiate(GeneralPurposeObjectManager.Instance.BlueSubstanceCross, new Vector3(this.collider.bounds.max.x, other.transform.position.y, 0) + this.particlesOffset, GeneralPurposeObjectManager.Instance.BlueSubstanceCross.transform.rotation) as ParticleSystem;
            }
            else
            {
                enterParticles = Instantiate(GeneralPurposeObjectManager.Instance.BlueSubstanceCross, new Vector3(this.collider.bounds.min.x, other.transform.position.y, 0) + this.particlesOffset, GeneralPurposeObjectManager.Instance.BlueSubstanceCross.transform.rotation) as ParticleSystem;
            }

            enterParticles.Play();
            ParticleSystem.EmissionModule particleEmitter = enterParticles.emission;
            particleEmitter.enabled = true;

            if(enter)
            {
                AudioManager.Instance.PlayOneShoot(AudioClipsData.ClipsGroups.Enviro, inClip);
            }
            else
            {
                AudioManager.Instance.PlayOneShoot(AudioClipsData.ClipsGroups.Enviro, outClip);
            }

			if (this.animator) {
				this.animator.SetTrigger("Bounce");
			}
        }
    }
}
