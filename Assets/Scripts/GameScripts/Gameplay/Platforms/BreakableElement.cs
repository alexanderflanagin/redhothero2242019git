using UnityEngine;
using System.Collections;

/// <summary>
/// Script que fa que un element de l'escenari pugui ser colpejat.
/// </summary>
public class BreakableElement : MonoBehaviour, IPowerGloveable
{
    /// <summary>
    /// Temps que triga a tornar a apareixer un objecte despres d'haver sigut trencat
    /// </summary>
    public virtual float SPAWN_TIME { get { return 1.0f; } }

    /// <summary>
    /// Mesh del objecte powergloveable (el que es rota quan es colpeja)
    /// </summary>
    public GameObject mesh;

    /// <summary>
    /// Getter public del GameObject que conte la mesh
    /// </summary>
    public GameObject meshObject
    {
        get
        {
            return mesh;
        }
    }

    /// <summary>
    /// Partï¿½cules que salten quan es colpeja
    /// </summary>
    public ParticleSystem powerGlovedParticles;

    /// <summary>
    /// Particules que salten al colÂ·lisionar
    /// </summary>
    public ParticleSystem explodeParticles;

    /// <summary>
    /// AudioSource del gameObject que conte l'audio de quan es trenca
    /// </summary>
    protected AudioSource _audio;

    /// <summary>
    /// Indica si aquest GameObject ha sigut l'escollit per a que segueixi la camera al ser colpejat
    /// </summary>
    public bool theChosen { get; set; }

    /// <summary>
    /// Referencia al shootable (si ho es)
    /// </summary>
    private Shootable shootable;

    /// <summary>
    /// Referencia al collider
    /// </summary>
    protected new Collider2D collider;

    /// <summary>
    /// Referencia al Blue Substanced Element del child (si en té)
    /// </summary>
    private BlueSubstancedElement blueSubstancedElement;

    /// <summary>
    /// Cantonada esquerra inferior del collider per al spawn
    /// </summary>
    protected Vector3 colliderMin;

    /// <summary>
    /// Cantonada dreta superior del collider per al spawn
    /// </summary>
    protected Vector3 colliderMax;

    /// <summary>
    /// La posicio incial de l'objecte
    /// </summary>
    protected Vector3 startPosition = Vector3.zero;

    /// <summary>
    /// Escala original de l'objecte, per a reconstruir-lo amb el spawnme
    /// </summary>
    protected Vector3 originalScale;

    /// <summary>
    /// Cert si esta actualment destruit (i per tant l'hem d'ignorar)
    /// </summary>
    protected bool destroyed;

    protected virtual void Awake()
    {
        startPosition = this.transform.position;
        originalScale = transform.localScale;
    }

    /// <summary>
    /// Al habilitar, preparem les partÃ­cules i l'Audio
    /// </summary>
    protected virtual void OnEnable()
    {
        GameObject particles = new GameObject("Particles");
        particles.transform.parent = this.transform;
        particles.transform.localPosition = Vector3.zero;

        _audio = this.gameObject.GetComponent<AudioSource>();

        if (explodeParticles)
        {
            Instantiator.LoadParticleSystem(ref explodeParticles, Vector3.zero, Vector3.zero, particles);
            var em = explodeParticles.emission;
            em.enabled = false;
        }

        if (powerGlovedParticles)
        {
            Instantiator.LoadParticleSystem(ref powerGlovedParticles, Vector3.zero, Vector3.zero, particles);
            var em = powerGlovedParticles.emission;
            em.enabled = false;
        }

        shootable = GetComponent<Shootable>();
        if (shootable)
        {
            shootable.touched += DestroyFromShoot;
        }

        blueSubstancedElement = GetComponentInChildren<BlueSubstancedElement>();

        collider = GetComponent<Collider2D>();
        // Agafem les mides del collider per a fer el spawn
        colliderMin = collider.bounds.min;
        colliderMax = collider.bounds.max;
    }

    /// <summary>
    /// Al ser colpejat, hi afegim l'script PowerGloved, instanciem particules de powerGloved i les del tail
    /// </summary>
    /// <param name="lookingAtPoint"></param>
    public virtual void PowerGloved(Vector3 lookingAtPoint)
    {
        DestroyMe();
    }

    /// <summary>
    /// Do nothing
    /// </summary>
    public virtual void SaveState()
    {
        // Do Nothing
    }

    /// <summary>
    /// Do nothing
    /// </summary>
    public virtual void RecoverState()
    {
        this.meshObject.SetActive(true);
        collider.enabled = true;

        if (blueSubstancedElement)
		{
            blueSubstancedElement.RecoverState();

		}
    }

    /// <summary>
    /// Amaguem la mesh, posem les partÃ­cules d'explosiÃ³, avisem de que hem sigut destruits si erem el theChosen i reproduim so.
    /// </summary>
    public virtual void Collide(GameObject with)
    {
        Physics2D.queriesHitTriggers = true;
        Collider2D blueSubstance = Physics2D.OverlapArea(collider.bounds.min, collider.bounds.max, 1 << Layers.BlueSubstance);
        Physics2D.queriesHitTriggers = false;

        if (blueSubstance && blueSubstance.transform.IsChildOf(this.transform))
        {
            if (blueSubstancedElement)
            {
                blueSubstancedElement.Collide(gameObject);
            }
        }
        else
        {
            if (theChosen)
            {
                FindObjectOfType<PowerGlove>().lastPowerGloved = null;
            }

            DestroyMe();
        }
    }

    /// <summary>
    /// Si herem el theChosen, avisem i desprÃ©s destruim
    /// </summary>
    public void DestroyByTime()
    {
        if (theChosen)
        {
            FindObjectOfType<PowerGlove>().lastPowerGloved = null;
        }

        DestroyMe();
    }

    public void DestroyFromShoot(Projectile from)
    {
        DestroyMe();
    }

    /// <summary>
    /// Metode generic per a "destruir" els objectes breakeables
    /// </summary>
    public virtual void DestroyMe()
    {
        if(destroyed)
        {
            return;
        }

        destroyed = true;

        PowerGloved powerGloved = this.GetComponent<PowerGloved>();
        if (powerGloved)
        {
            Destroy(powerGloved);
        }
        
        if(shootable)
        {
            shootable.Deactivate();
        }

        meshObject.SetActive(false);
        collider.enabled = false; // Per si no ho haviem fet ja, que no ens barri el pas

        if (explodeParticles)
        {
            ParticleSystem.EmissionModule emissionModule = explodeParticles.emission;
            emissionModule.enabled = true;
            explodeParticles.Play();
        }

        if (_audio)
        {
            _audio.Play();
        }
    }

    /// <summary>
    /// Search for players and enemies inside the position where its gonna regenerate the collider and move them if necessary
    /// </summary>
    protected void MoveCollidingPhysics2DControllers()
    {
        Collider2D[] colliders = new Collider2D[4];

        if (Physics2D.OverlapAreaNonAlloc(collider.bounds.min, collider.bounds.max, colliders, Layers.Players | Layers.Enemies) > 0)
        {
            foreach (Collider2D col in colliders)
            {
                if (col != null)
                {
                    Physics2DController controller = col.transform.GetComponent<Physics2DController>();
                    Vector2 outsideMovement = new Vector2(0, collider.bounds.max.y - (col.transform.position.y + col.offset.y - col.bounds.extents.y));

                    if (controller != null)
                    {
                        controller.Move(outsideMovement, true, true, outsideMovement);
                    }
                    //col.gameObject.transform.position = new Vector3(col.gameObject.transform.position.x, (collider.bounds.max.y - col.offset.y + col.bounds.extents.y), col.gameObject.transform.position.z);
                }
            }
        }
    }

    /// <summary>
    /// Fa spawn de l'objecte. Després d'esperar \SPAWN_TIME\, mira si es pot crear, creix lentament i habilita els colliders quan està complet
    /// </summary>
    /// <returns></returns>
    protected IEnumerator SpawnMe()
    {
        yield return new WaitForSeconds(SPAWN_TIME);

        transform.position = startPosition;

        int steps = 5;

        Vector3 scaleModifier = originalScale / (float)steps;

        transform.localScale = Vector3.zero;

        collider.enabled = false;
        meshObject.SetActive(true);

        for (int i = 0; i < steps; i++)
        {
            transform.localScale += scaleModifier;
            MoveCollidingPhysics2DControllers();
            yield return null;
            yield return null;
        }

        collider.enabled = true;
        destroyed = false;

        // Ens assegurem que l'escala és exactament la mateixa. Per a no perdre la mida si es trenca molts cops
        transform.localScale = originalScale;
    }
}
