using UnityEngine;

public abstract class MovingPath : MonoBehaviour,  IWakeUpable
{
	/// <summary>
	/// Indica si s'esta movent la plataforma
	/// </summary>
	protected bool moving = false;

    public bool isMoving { get { return moving; } }

	/// <summary>
	/// Velocitat de moviment
	/// </summary>
	[Range(1,20)]
	public float speed;
	
	/// <summary>
	/// Plataforma amb el collider sobre la que es posara el Guiri
	/// </summary>
	public GameObject platform;
	
	/// <summary>
	/// Element que es moura
	/// </summary>
	public GameObject gObject;
	
	/// <summary>
	/// L'element que es moura ja instanciat
	/// </summary>
	[HideInInspector]
	public GameObject instantiatedGameObject;
	
	/// <summary>
	/// Distancia amb el trigger. Necessari per el IWakeUp
	/// </summary>
	[HideInInspector]
	public float triggerDitance;

	/// <summary>
	/// Offset del trigger que el desperta
	/// </summary>
	public Vector2 triggerOffset;

	/// <summary>
	/// Mida del trigger que el desperta
	/// </summary>
	public Vector2 triggerSize;

	/// <summary>
	/// Numero d'elements que tindra
	/// </summary>
	public int numElements;

    /// <summary>
    /// LayerMask per a saber quins elements dels que tenim 
    /// </summary>
    protected LayerMask isMovingPlatform = Layers.MovingPlatforms;

    /// <summary>
    /// Array amb els elements MovingPathElements que te
    /// </summary>
    protected PathElement[] elements;

	/// <summary>
	/// Distancia entre els elements
	/// </summary>
	public float distance;

	/// <summary>
	/// Per saber si hem de mostrar un element grafic /graphicGuide/ als punts de navegacio
	/// </summary>
	public bool useGraphicGuides;
	
	/// <summary>
	/// L'element grafic que es mostra als punts de navegacio
	/// </summary>
	public GameObject graphicGuide;
	
	/// <summary>
	/// El line renderer del GameObject. Mostrem una línia que uneix els punts de navegacio
	/// </summary>
	public LineRenderer lineRenderer;

	/// <summary>
	/// Per a saber el darrer frame que s'ha mogut. Ens serveix per a poder moure la plataforma externament
	/// </summary>
	protected int lastFrameMoved = 0;

	/// <summary>
	/// Instanciem el/s GameObject/s que es mostrara/n
	/// </summary>
	protected void InstantiateElements()
	{
		// Com a mínim sempre tindrem un element
		if(this.numElements < 1)
		{
			this.numElements = 1;
		}
		
		// Eliminem l'instantiated element (si existia)
		if(this.instantiatedGameObject)
		{
			Destroy(this.instantiatedGameObject);
		}
		
		if(this.gObject)
		{
			// Netejem el quadrat verd
			this.platform.SetActive(false);
			
			CreateElementsArray();
			
			for(int i = 0; i < this.numElements; i++)
			{
				InstantiateElement(i);
			}
		}
		else
		{
			// Posant els grafics per defecte
			Debug.Log("No te grafics la plataforma mòbil. Fem servir el quadrat verd");
		}
	}

	/// <summary>
	/// Instancia un element dels que tindra el MovingElementsPath segons la posicio que tinguem al saber quin numero d'element es
	/// </summary>
	/// <param name="index">Numero de l'element que estem instanciant</param>
	protected void InstantiateElement(int index)
	{
		// Creem el Game Object i el posem com a fill del moving
		GameObject element = Instantiate(this.gObject) as GameObject;
		element.name = "Element-" + index;
		element.transform.SetParent(this.transform);
		
		// Hi afegim rigidbody si cal
		if (element.GetComponent<Collider2D>() || element.GetComponentInChildren<Collider2D>())
		{
			if (!element.GetComponent<Rigidbody2D>())
			{
				element.AddComponent<Rigidbody2D>().isKinematic = true;
			}
		}
		
		// Afegim el path element al game object i el posem a la llista
		AddPathBehaviourAtElement(index, element);

        PathElement elementPathElement = element.GetComponent<PathElement>();

        element.transform.position = CalculatePosition(index, elementPathElement);

        if(Layers.inMask(isMovingPlatform, element.layer))
        {
            MovingPlatform elementMovingPlatform = element.AddComponent<MovingPlatform>();
            elementMovingPlatform.passengerMask = Layers.MovingPlatformPassengers;
            elementMovingPlatform.pathElement = elementPathElement;
            elementPathElement.isMovingPlatform = true;
        }

        //element.GetComponent<PathElement>().PrepareSecureTrigger();
	}

	#region IWakeUpable

	/// <summary>
	/// Desperta la plataforma. Herencia del IWakeUp
	/// </summary>
	public virtual void WakeUp()
	{
		if (!this.moving)
		{
			this.moving = true;
			for(int i = 0; i < this.elements.Length; i++)
			{
				this.elements[i].WakeUp();
			}
		}
	}

	/// <summary>
	/// Posa la distancia del trigger. Herencia del IWakeUp
	/// </summary>
	/// <param name="distance"></param>
	public void SetTriggerDistance(float distance)
	{
		triggerDitance = distance;
	}
	
	/// <summary>
	/// Retorna la distancia del trigger. Herencia del IWakeUp
	/// </summary>
	/// <returns>Distancia amb el trigger</returns>
	public float GetTriggerDistance()
	{
		return triggerDitance;
	}

	/// <summary>
	/// Posa el trigger a dormir. No parem els elements
	/// </summary>
	public void Sleep()
	{
		
	}
	
	public void SetTriggerOffset(Vector2 offset)
	{
		this.triggerOffset = offset;
	}
	
	public void SetTriggerSize(Vector2 size)
	{
		this.triggerSize = size;
	}
	
	public Vector2 GetTriggerOffset()
	{
		return this.triggerOffset;
	}
	
	public Vector2 GetTriggerSize()
	{
		return this.triggerSize;
	}

	#endregion

	/// <summary>
	/// Crea l'array d'elements (segons el tipus de MovingElementsPath, fara servir uns MovingElements o uns altres)
	/// </summary>
	protected abstract void CreateElementsArray();

	/// <summary>
	/// Afegeix el funcionament de MovingElement a un GameObject
	/// </summary>
	/// <param name="index">El numero d'element que estem creant</param>
	/// <param name="element">El GameObject</param>
	protected abstract void AddPathBehaviourAtElement(int index, GameObject element);

	/// <summary>
	/// Calcula la posicio on sera l'element que estem creantç
	/// </summary>
	/// <returns>The position</returns>
	/// <param name="index">Numero de l'element</param>
	/// <param name="movingPathElement">Element al que li calculem la posicio</param>
	protected abstract Vector3 CalculatePosition(int index, PathElement movingPathElement);
}
