using UnityEngine;
using System.Collections;

public class PathElementCircular : PathElement
{
    /// <summary>
    /// L'angle que t� en aquell moment la plataforma. Comen�a a la posici� del /radius/
    /// </summary>
    public float angle;

    /// <summary>
    /// Es fa servir per a calcular el següent moviment a cada frame. Calcula el moviment necessàri per a moure's a la propera posició, que vindrà donada per el sinus i el cosinus de l'angle
    /// Si ja s'ha fet aquest frame, no es torna a fer.
    /// </summary>
    public override void CalculateNextMove()
    {
        CircularMovingPath ownerPath = path as CircularMovingPath;

        if (this.lastFrameMoved != Time.frameCount)
        {
            this.lastFrameMoved = Time.frameCount;

            if (ownerPath.clockwise)
            {
                this.angle -= ownerPath.speed * 0.1f * Time.deltaTime;
            }
            else
            {
                this.angle += ownerPath.speed * 0.1f * Time.deltaTime;
            }

            this.movement = ownerPath.transform.position + new Vector3(Mathf.Cos(this.angle), Mathf.Sin(this.angle)) * ownerPath.radius - this.transform.position;
        }
    }
	
}
