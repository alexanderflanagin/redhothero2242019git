using UnityEngine;
using System.Collections;

/// <summary>
/// Script que fa que un element de l'escenari pugui ser colpejat.
/// </summary>
public class BreakableElementTutorial : BreakableElement, ITutoriable
{
    /// <summary>
    /// Per a poder assignar la bombolla manualment des de l'editor
    /// </summary>
    public Bubble myBubbleGet;

    /// <summary>
    /// Getter public de la bombolla, heredat de la interfície
    /// </summary>
    public Bubble myBubble { get; set; }

    /// <summary>
    /// Per saber si ja tenia el power glove el frame anterior
    /// </summary>
    private bool lastFrameHasPowerGlove;

    /// <summary>
    /// Al habilitar, cridem al pare, preparem la bombolla i mirem si ja té el power glove
    /// </summary>
    protected virtual void OnEnable()
    {
        base.OnEnable();
        
        this.myBubble = this.myBubbleGet;

        this.lastFrameHasPowerGlove = GameManager.Instance.playerData.powerGlove;

        if (!GameManager.Instance.playerData.powerGlove)
        {
            HideBubble();
        }
    }

    /// <summary>
    /// Abans de destruir-se, amaga la bombolla
    /// </summary>
    public override void DestroyMe()
    {
        if (destroyed)
        {
            return;
        }

        HideBubble();
        base.DestroyMe();
    }

    /// <summary>
    /// Cada frame mira si ha aconseguit el power glove, i en tal cas s'activen
    /// </summary>
    void Update()
    {
        if (!this.lastFrameHasPowerGlove && StandardStageManager.current.playerControl.suit.hasPowerGlove)
        {
            ShowBubble();
        }

        this.lastFrameHasPowerGlove = StandardStageManager.current.playerControl.suit.hasPowerGlove;
    }

    /// <summary>
    /// Mostra la bombolla d'informació
    /// </summary>
    public void ShowBubble()
    {
        myBubble.gameObject.SetActive(true);
    }

    /// <summary>
    /// Amaga la bombolla d'informació
    /// </summary>
    public void HideBubble()
    {
        myBubble.gameObject.SetActive(false);
    }
}
