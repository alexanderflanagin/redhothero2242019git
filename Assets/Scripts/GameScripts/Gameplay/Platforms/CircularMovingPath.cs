using UnityEngine;
using System.Collections;

public class CircularMovingPath : MovingPath
{
    /// <summary>
    /// El GameObject que representa el radi i el punt de sortida de la plataforma
    /// </summary>
    public Vector3 radiusLocalPosition;

    /// <summary>
    /// Per si va en sentit de les agulles del rellotge, o sentit contrari
    /// </summary>
    public bool clockwise;

    /// <summary>
    /// El radi de la circumferencia, calculat segons la posici� del /radius/
    /// </summary>
    [HideInInspector]
    public float radius;

    /// <summary>
    /// El color que agafaran els gizmos d'aquesta plataforma
    /// </summary>
    public Color gizmosColor;

	// Use this for initialization
    protected virtual void Start()
    {
        this.radius = Mathf.Sqrt(Mathf.Pow((radiusLocalPosition).x, 2) + Mathf.Pow((radiusLocalPosition).y, 2));

        //float startAngle = Mathf.Deg2Rad * Vector3.Angle((this.transform.position - this.radiusPosition), Vector3.right) * Mathf.Sign((this.transform.position - this.radiusPosition).y);

        InstantiateElements();
	}

    protected override void AddPathBehaviourAtElement(int index, GameObject element)
    {
        PathElementCircular movingPathElement = element.AddComponent<PathElementCircular>();
        movingPathElement.path = this;
        this.elements[index] = movingPathElement;
    }

    protected override void CreateElementsArray()
    {
        this.elements = new PathElementCircular[this.numElements];
    }

    protected override Vector3 CalculatePosition(int index, PathElement movingPathElement)
    {
        //Vector3 position = this.radiusPosition;

        //Vector3 v3 = this.radiusPosition - this.transform.position;
        float perimeter = 2 * Mathf.PI * this.radius;
        float angle = (this.distance * index) / perimeter * 360;
        angle = Vector3.Angle(radiusLocalPosition, transform.position) + angle;
        // Debug.Log("Initial angle: " + Vector3.Angle(this.radiusPosition, this.transform.position) + "- Radius: " + this.radius + " - Distance: " + (this.distance * index) + " - Perimeter: " + perimeter + " - angle: " + angle);

        angle = angle * Mathf.Deg2Rad;

        ((PathElementCircular)movingPathElement).angle = angle;

        return this.transform.position + new Vector3(Mathf.Cos(angle), Mathf.Sin(angle)) * this.radius;
    }

	/// <summary>
	/// Desperta la plataforma. Herencia del IWakeUp
	/// </summary>
	public override void WakeUp()
	{
		if (!this.moving)
		{
			this.moving = true;
			for(int i = 0; i < this.elements.Length; i++)
			{
				this.elements[i].WakeUp();
			}
		}
	}

#if UNITY_EDITOR
    /// <summary>
    /// Al pintar Gizmos, mostrem la linia del radi
    /// </summary>
    void OnDrawGizmos()
    {
        Gizmos.color = new Color(gizmosColor.r, gizmosColor.g, gizmosColor.b);

        Gizmos.DrawLine(this.transform.position, transform.position + this.radiusLocalPosition);

        Gizmos.DrawWireSphere(this.transform.position, Mathf.Sqrt(Mathf.Pow((radiusLocalPosition).x, 2) + Mathf.Pow((radiusLocalPosition).y, 2)));
    }
#endif
}
