using UnityEngine;

public class PathElement : MonoBehaviour
{
    /// <summary>
    /// Path sobre el que ens movem
    /// </summary>
    public MovingPath path;

    /// <summary>
    /// Moviment actual del GameObject, per a moure el possible GameObject que tinguem a sobre (o al costat o a on sigui)
    /// </summary>
    [HideInInspector]
    public Vector2 movement;

    [HideInInspector]
    public Vector3 lastPosition = Vector3.zero;

    /// <summary>
    /// Si es moving platform, el moviment el farem des del moving platform
    /// </summary>
    [HideInInspector]
    public bool isMovingPlatform;

    /// <summary>
    /// L'ultim frame al que hem mogut. Per a poder-lo moure des de qualsevol banda i no moure'l dos cops per frame
    /// </summary>
    protected float lastFrameMoved;

    /// <summary>
    /// Index del punt al que esta / es mou del Moving Path
    /// </summary>
    public int index = 0;

    /// <summary>
    /// Si s'esta movent (per a poder moure a l'update enlloc de a una coroutina)
    /// </summary>
    [HideInInspector]
    public bool moving = false;

    /// <summary>
    /// Li diem que comenci a moure
    /// </summary>
    public void WakeUp()
    {
        moving = true;
    }

    /// <summary>
    /// Mou la plataforma si esta moving
    /// </summary>
    void Update()
    {
        if(moving)
        {
            CalculateNextMove();

			Translate ();
        }
    }

    /// <summary>
    /// Si no estem sent moguts per una Moving Platform, ens desplacem
    /// </summary>
	protected virtual void Translate()
	{
        if(!isMovingPlatform)
        {
            lastPosition = transform.position;
            transform.Translate(this.movement);
        }
	}

    /// <summary>
    /// Calcula el proxim moviment de la plataforma. Ho fem servir perque pot ser que algu demani aquest moviment abans del seu move
    /// </summary>
    public virtual void CalculateNextMove()
    {
        LinearMovingPath ownerPath = path as LinearMovingPath;

        if (this.lastFrameMoved != Time.frameCount)
        {
            this.lastFrameMoved = Time.frameCount;

            if (this.transform.position == ownerPath.points[this.index])
            {
                int newIndex = (this.index + 1) % ownerPath.points.Length;

                if(this.GetComponent<SingleEnemyFactory>())
                {
                    RotateEnemy(newIndex);
                }

                this.index = newIndex;
            }

            if (ownerPath.Spawner && this.index == 0)
            {
                // Ha estat power gloved?
				if(this.GetInterface<IPowerGloveable>() != null)
				{
					// Destroy(this.GetComponent<PowerGloved>());
					this.GetInterface<IPowerGloveable>().RecoverState();
				}
				// Aquí s'hauria de veure si s'amaga la plataforma o que fa
                this.transform.position = ownerPath.points[0];
                this.movement = Vector3.zero;
            }
            else
            {
                this.movement = Vector3.MoveTowards(this.transform.position, ownerPath.points[index], ownerPath.speed * Time.deltaTime) - this.transform.position;
            }
        }
    }

    /// <summary>
    /// Si es un enemic el que tenim, l'avisem de que roti (si cal) al canviar de punt
    /// </summary>
    /// <param name="newIndex">Nou index al que canviarem</param>
    protected void RotateEnemy(int newIndex)
    {
        EnemyController controller = this.GetComponent<SingleEnemyFactory>().enemyController;

        LinearMovingPath ownerPath = path as LinearMovingPath;

        if (controller != null)
        {
            if (ownerPath.points[newIndex].x > ownerPath.points[this.index].x && controller.direction == -1)
            {
                controller.events.triggerEvent(EnemyEvents.EventName.Rotate);
            }
            else if (ownerPath.points[newIndex].x < ownerPath.points[this.index].x && controller.direction == 1)
            {
                controller.events.triggerEvent(EnemyEvents.EventName.Rotate);
            }
        }
    }
}
