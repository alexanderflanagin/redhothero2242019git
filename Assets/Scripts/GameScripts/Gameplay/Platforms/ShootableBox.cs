using UnityEngine;
using System;
using System.Collections;

[Obsolete("Aquesta class de shootable box ja no es fa servir. Ara es munta amb dues classes", true)]
public class ShootableBox : MonoBehaviour {

    /// <summary>
    /// Mesh del objecte powergloveable (el que es rota quan es colpeja)
    /// </summary>
    public GameObject mesh;

    /// <summary>
    /// Getter públic del GameObject que conté la mesh
    /// </summary>
    public GameObject meshObject
    {
        get
        {
            return mesh;
        }
    }

    /// <summary>
    /// Particules que salten al col·lisionar
    /// </summary>
    public ParticleSystem collideParticles;

    /// <summary>
    /// AudioSource del gameObject que cont� l'audio de quan es trenca
    /// </summary>
    private AudioSource _audio;

    /// <summary>
    /// Al habilitar, preparem les partícules i l'Audio
    /// </summary>
    void OnEnable()
    {
        GameObject particles = new GameObject("Particles");
        particles.transform.parent = this.transform;
        particles.transform.localPosition = Vector3.zero;

        _audio = this.gameObject.GetComponent<AudioSource>();

        if (collideParticles)
        {
            Instantiator.LoadParticleSystem(ref collideParticles, Vector3.zero, Vector3.zero, particles);
            collideParticles.enableEmission = false;
        }
    }

    /// <summary>
    /// Al fer-se visible, l'afegim a la llista
    /// </summary>
    void OnBecameVisible()
    {
        RedHotTarget.instance.AddShootable(this.gameObject);
    }

    void OnBecameInvisible()
    {
        RedHotTarget.instance.RemoveShootable(this.gameObject);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.GetComponent<Projectile>() || other.GetComponent<ProjectileCenter>())
        {
            this.meshObject.SetActive(false);

            if (collideParticles)
            {
                collideParticles.enableEmission = true;
                collideParticles.Play();
            }
            this.GetComponent<Collider2D>().enabled = false;
            RedHotTarget.instance.RemoveShootable(this.gameObject);
            StartCoroutine(DestroyMe());

            if (other.GetComponent<Projectile>())
            {
                other.GetComponent<Projectile>().OnDestroyProjectile();
            }
            else
            {
                other.transform.parent.GetComponent<Projectile>().OnDestroyProjectile();
            }
        }
    }

    private IEnumerator DestroyMe()
    {
        yield return new WaitForSeconds(4f);
        Destroy(this.gameObject);
    }
}
