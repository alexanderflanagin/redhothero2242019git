using UnityEngine;
using System.Collections.Generic;

[RequireComponent(typeof(PathElement))]
public class MovingPlatform : Physics2DPlatform
{
    /// <summary>
    /// El PathElement al que pertany
    /// </summary>
    [HideInInspector]
    public PathElement pathElement;

    /// <summary>
    /// Fem el Start de la base i ens assegurem de que tingui el pathElement assignat
    /// </summary>
    public override void Start()
    {
        base.Start();

        DebugUtils.AssertError(pathElement != null, "Moving Platform without a Path Element", this.gameObject);
    }

    /// <summary>
    /// Calcula el moviment que han de fer els passatgers, estiguin tocant per la banda que estiguin tocant
    /// </summary>
    protected override void CalculatePassengerMovement()
    {
        HashSet<Transform> movedPassengers = new HashSet<Transform>();
        passengerMovement = new List<PassengerMovement>();

        float directionX = Mathf.Sign(movement.x);
        float directionY = Mathf.Sign(movement.y);

        // Vertically moving platform
        if (movement.y != 0)
        {
            float rayLength = Mathf.Abs(movement.y) + BoxColliderRayInfo.skinWidth;

            for (int i = 0; i < rayInfo.verticalRayCount; i++)
            {
                Vector2 rayOrigin = (directionY == -1) ? rayInfo.raycastOrigins.bottomLeft : rayInfo.raycastOrigins.topLeft;
                rayOrigin += Vector2.right * (rayInfo.verticalRaySpacing * i);
                RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.up * directionY, rayLength, passengerMask);

                if (hit && hit.distance != 0)
                {
                    if (!movedPassengers.Contains(hit.transform))
                    {
                        movedPassengers.Add(hit.transform);
                        float pushX = (directionY == 1) ? movement.x : 0;
                        float pushY = movement.y - (hit.distance - BoxColliderRayInfo.skinWidth) * directionY;

                        passengerMovement.Add(new PassengerMovement(hit.transform, new Vector3(pushX, pushY), directionY == 1, true));
                    }
                }
            }
        }

        // Horizontally moving platform
        if (movement.x != 0)
        {
            float rayLength = Mathf.Abs(movement.x) + BoxColliderRayInfo.skinWidth;

            for (int i = 0; i < rayInfo.horizontalRayCount; i++)
            {
                Vector2 rayOrigin = (directionX == -1) ? rayInfo.raycastOrigins.bottomLeft : rayInfo.raycastOrigins.bottomRight;
                rayOrigin += Vector2.up * (rayInfo.horizontalRaySpacing * i);
                RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.right * directionX, rayLength, passengerMask);

                if (hit && hit.distance != 0)
                {
                    if (!movedPassengers.Contains(hit.transform))
                    {
                        movedPassengers.Add(hit.transform);
                        float pushX = movement.x - (hit.distance - BoxColliderRayInfo.skinWidth) * directionX;
                        float pushY = -BoxColliderRayInfo.skinWidth;

                        passengerMovement.Add(new PassengerMovement(hit.transform, new Vector3(pushX, pushY), false, true));
                    }
                }
            }
        }

        // Passenger on top of a horizontally or downward moving platform
        if (directionY == -1 || movement.y == 0 && movement.x != 0)
        {
            float rayLength = BoxColliderRayInfo.skinWidth * 2;

            for (int i = 0; i < rayInfo.verticalRayCount; i++)
            {
                Vector2 rayOrigin = rayInfo.raycastOrigins.topLeft + Vector2.right * (rayInfo.verticalRaySpacing * i);
                RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.up, rayLength, passengerMask);

                if (hit && hit.distance != 0)
                {
                    if (!movedPassengers.Contains(hit.transform))
                    {
                        movedPassengers.Add(hit.transform);
                        float pushX = movement.x;
                        float pushY = movement.y;

                        passengerMovement.Add(new PassengerMovement(hit.transform, new Vector3(pushX, pushY), true, false));
                    }
                }
            }
        }
    }

    /// <summary>
    /// Li diu al pathElement que calculi el seguent moviment. Si no es mou el pathElement, restem quiets
    /// </summary>
    protected override void CalculatePlatformMovement()
    {
        if(pathElement.moving)
        {
            pathElement.CalculateNextMove();
            movement = pathElement.movement;
        }
        else
        {
            movement = Vector2.zero;
        }
    }

}
