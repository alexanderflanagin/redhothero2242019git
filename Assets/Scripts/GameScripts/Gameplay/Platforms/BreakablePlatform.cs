using UnityEngine;
using System.Collections;

public class BreakablePlatform : MonoBehaviour
{
    /// <summary>
    /// Temps que tremola abans de fer desapareixer el collider (tenir en compte com es l'animacio)
    /// </summary>
    public float trembleSeconds = 1f;

    /// <summary>
    /// Si s'ha de tornar a crear o no la plataforma
    /// </summary>
    public bool shouldRespawn = true;

    /// <summary>
    /// Temps que esta desactivada la plataforma abans de tornar-se a crear
    /// </summary>
    public float respawnTime = 3f;

    /// <summary>
    /// El collider de la plataforma
    /// </summary>
    public new Collider2D collider2D;

    /// <summary>
    ///  L'animator de la plataforma
    /// </summary>
    public Animator animator;

    /// <summary>
    /// LayerMask dels objectes que trenquen la platform si ho son sobre
    /// </summary>
    public LayerMask objectsBreaksPlatform;

    /// <summary>
    /// Si s'esta trencant o esta trencada
    /// </summary>
    private bool breaking;

    /// <summary>
    /// Hash del trigger per a comencar a tremolar
    /// </summary>
    private int trembleTrigger = Animator.StringToHash("StartTremble");

    /// <summary>
    /// Has del trigger per a tornar a habilitar la plataforma
    /// </summary>
    private int enableTrigger = Animator.StringToHash("Enable");

    /// <summary>
    /// Si el player esta a menys d'aquesta distancia, tirem rajos per mirar si el tenim a sobre
    /// </summary>
    private float playerDistanceEnable = 10;

    /// <summary>
    /// Distancia distancia on posem el raig per dintre i per sobre del collider
    /// </summary>
    private float testSkin = 0.015f;

    /// <summary>
    /// Posicio on comencem el raig
    /// </summary>
    private Vector3 upperDistanceToCheck;

    /// <summary>
    /// Ens assegurem que no s'estigui trencant
    /// </summary>
    void Start()
    {
        breaking = false;

         upperDistanceToCheck = new Vector3(-testSkin, testSkin);
    }

    /// <summary>
    /// Si no ens estem trencant i el player esta aprop, mirem si el tenim a sobre amb un raig
    /// </summary>
    void Update()
    {
        if(!breaking)
        {
            if(Vector3.Distance(transform.position, StandardStageManager.current.player.transform.position) < playerDistanceEnable)
            {
                Physics2D.queriesStartInColliders = true;
                RaycastHit2D ray = Physics2D.Raycast(collider2D.bounds.max + upperDistanceToCheck, Vector2.left, collider2D.bounds.size.x - testSkin * 2, objectsBreaksPlatform);
                Physics2D.queriesStartInColliders = false;
                
                if(ray)
                {
                    StartCoroutine(Break());
                }
            }
        }
    }

    /// <summary>
    /// Proces de trencar-se i tornar-se a generar (si /shouldRespawn)
    /// </summary>
    /// <returns></returns>
    IEnumerator Break()
    {
        breaking = true;

        animator.SetTrigger(trembleTrigger);

        yield return new WaitForSeconds(trembleSeconds);

        collider2D.enabled = false;

        if(shouldRespawn)
        {
            yield return new WaitForSeconds(respawnTime);

            animator.SetTrigger(enableTrigger);

            collider2D.enabled = true;

            breaking = false;
        }
    }
}
