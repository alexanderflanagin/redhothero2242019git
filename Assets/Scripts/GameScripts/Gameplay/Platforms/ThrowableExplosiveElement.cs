using UnityEngine;
using System.Collections;


public class ThrowableExplosiveElement : ThrowableElement
{
    /// <summary>
    /// Sound played on explode
    /// </summary>
    public string explosionClip = "BreakableCollide";
    
    /// <summary>
    /// El temps que esta la red hot density viva un cop explota l'objecte
    /// </summary>
    private float RED_HOT_DENSITY_TIME = 2.0f;

    /// <summary>
    /// La red hot density que deixar� al destruir-se
    /// </summary>
    private RedHotDensity redHotDensity;

    /// <summary>
    /// Al destruir l'objecte, crida el destroy me del parent i instancia una red hot density
    /// </summary>

    public override void DestroyMe()
    {
        if (destroyed)
        {
            return;
        }

        base.DestroyMe();

        AudioManager.Instance.PlayOneShoot(AudioClipsData.ClipsGroups.Enviro, explosionClip);

        redHotDensity = GameObjectPooler.Instance.GetRedHotDensity();
        redHotDensity.transform.position = this.transform.position + (Vector3)this.GetComponent<BoxCollider2D>().offset; //  +new Vector3(0, this.transform.localScale.y, 0);

        StartCoroutine(DisableDensity());
    }

    /// <summary>
    /// Deshabilita la red hot density
    /// </summary>
    private IEnumerator DisableDensity()
    {
        yield return new WaitForSeconds(RED_HOT_DENSITY_TIME);

        if (redHotDensity)
        {
            redHotDensity.Release();
        }
    }
}
