using UnityEngine;
using System.Collections;

public class ThrowableElement : BreakableElement
{
    /// <summary>
    /// La rotacio inicial de l'objecte
    /// </summary>
    protected Quaternion meshStartRotation;

    protected override void Awake()
    {
        base.Awake();
        meshStartRotation = mesh.transform.rotation;
    }

    /// <summary>
    /// Al ser colpejat, "llencem" l'objecte
    /// </summary>
    /// <param name="lookingAtPoint"></param>
    public override void PowerGloved(Vector3 lookingAtPoint)
    {
        PowerGloved pG = transform.GetOrAddComponent<PowerGloved>();
        pG.direction = Quaternion.Inverse(transform.rotation) * lookingAtPoint;
        pG.objectToMove = this.gameObject;

        if (this.powerGlovedParticles)
        {
            ParticleSystem.EmissionModule emission = powerGlovedParticles.emission;
            emission.enabled = true;
            powerGlovedParticles.Play();
        }

        pG.AddTrail(collider.offset);
    }

    /// <summary>
    /// Destrueix l'objecte i inicia la co-routina del spawn
    /// </summary>
    public override void DestroyMe()
    {
        if (destroyed)
        {
            return;
        }

        base.DestroyMe();

        StartCoroutine(SpawnMe());

        mesh.transform.rotation = meshStartRotation;
    }
}
