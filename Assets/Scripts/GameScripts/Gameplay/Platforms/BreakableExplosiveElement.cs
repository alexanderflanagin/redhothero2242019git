using UnityEngine;
using System.Collections;

/// <summary>
/// Classe que exten els Breakables per a que, després de trencar-se, generin un portal de RedHotDensity
/// </summary>
public class BreakableExplosiveElement : BreakableElement
{
    /// <summary>
    /// El temps que esta la red hot density viva un cop explota l'objecte
    /// </summary>
    private float RED_HOT_DENSITY_TIME = 2.0f;

    /// <summary>
    /// Time needed to spawn
    /// </summary>
    public override float SPAWN_TIME { get { return 5f; } }

    /// <summary>
    /// La red hot density que deixarà al destruir-se
    /// </summary>
    private RedHotDensity redHotDensity;

    /// <summary>
    /// Al destruir l'objecte, crida el destroy me del parent i instancia una red hot density
    /// </summary>
    public override void DestroyMe()
    {
        if (destroyed)
        {
            return;
        }

        base.DestroyMe();

        redHotDensity = GameObjectPooler.Instance.GetRedHotDensity();
        redHotDensity.transform.position = transform.position + (Vector3)GetComponent<Collider2D>().offset; //  +new Vector3(0, this.transform.localScale.y, 0);
        
        StartCoroutine(SpawnMe());
        StartCoroutine(DisableDensity());
    }

    /// <summary>
    /// Deshabilita la red hot density
    /// </summary>
    private IEnumerator DisableDensity()
    {
        yield return new WaitForSeconds(RED_HOT_DENSITY_TIME);

        if (redHotDensity)
        {
            redHotDensity.Release();
        }
    }
}
