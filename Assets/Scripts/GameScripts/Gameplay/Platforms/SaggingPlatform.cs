using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Plataformes que baixen quan hi puja un element a sobre
/// </summary>
public class SaggingPlatform : Physics2DPlatform
{
    /// <summary>
    /// Quantes unitats ha de caure la plataforma
    /// </summary>
	public float fallAmount;

    /// <summary>
    /// Multiplicador de la velocitat de moviment de la plataforma
    /// </summary>
    [Range(0.4f, 2f)]
    public float movementTime;

    /// <summary>
    /// Velocitat que portem en les Y
    /// </summary>
    private float yVelocity;

    /// <summary>
    /// Posicio inicial en les Y
    /// </summary>
	private float originalY;

    /// <summary>
    /// Posicio final de les Y
    /// </summary>
    private float finalY;

    /// <summary>
    /// Fem el Start del Controller i calculem les Y fins on haurem de pujar/baixar
    /// </summary>
    public override void Start()
    {
        base.Start();

        originalY = transform.position.y;
        finalY = originalY - fallAmount;
    }

    /// <summary>
    /// Primer mirem si tenim passatgers, i despres calculem on ens hem de moure segons aixo
    /// </summary>
    protected override void CalculatePlatformMovement()
    {
        // Aixi sabem si tenim algu a sobre
        FindPassengers();

        // Si estavem pujant i s'incorpora algú, frenem en sec
        if (passengerMovement.Count > 0 && yVelocity > 0)
        {
            yVelocity = 0;
        }

        float destinationY = (passengerMovement.Count > 0) ? finalY : originalY;

        float targetY = Mathf.SmoothDamp(transform.position.y, destinationY, ref yVelocity, movementTime, Mathf.Infinity, Time.deltaTime);

        movement.y = targetY - this.transform.position.y;
    }

    protected override void CalculatePassengerMovement()
    {
        HashSet<Transform> movedPassengers = new HashSet<Transform>();
        passengerMovement = new List<PassengerMovement>();

        float directionX = Mathf.Sign(movement.x);
        float directionY = Mathf.Sign(movement.y);

        // Vertically moving platform
        if (movement.y != 0)
        {
            float rayLength = Mathf.Abs(movement.y) + BoxColliderRayInfo.skinWidth;

            for (int i = 0; i < rayInfo.verticalRayCount; i++)
            {
                Vector2 rayOrigin = (directionY == -1) ? rayInfo.raycastOrigins.bottomLeft : rayInfo.raycastOrigins.topLeft;
                rayOrigin += Vector2.right * (rayInfo.verticalRaySpacing * i);
                RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.up * directionY, rayLength, passengerMask);

                if (hit && hit.distance != 0)
                {
                    if (!movedPassengers.Contains(hit.transform))
                    {
                        movedPassengers.Add(hit.transform);
                        float pushX = (directionY == 1) ? movement.x : 0;
                        float pushY = movement.y - (hit.distance - BoxColliderRayInfo.skinWidth) * directionY;

                        passengerMovement.Add(new PassengerMovement(hit.transform, new Vector3(pushX, pushY), directionY == 1, true));
                    }
                }
            }
        }

        // Horizontally moving platform
        //if (movement.x != 0)
        //{
        //    float rayLength = Mathf.Abs(movement.x) + BoxColliderRayInfo.skinWidth;

        //    for (int i = 0; i < rayInfo.horizontalRayCount; i++)
        //    {
        //        Vector2 rayOrigin = (directionX == -1) ? rayInfo.raycastOrigins.bottomLeft : rayInfo.raycastOrigins.bottomRight;
        //        rayOrigin += Vector2.up * (rayInfo.horizontalRaySpacing * i);
        //        RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.right * directionX, rayLength, passengerMask);

        //        if (hit && hit.distance != 0)
        //        {
        //            if (!movedPassengers.Contains(hit.transform))
        //            {
        //                movedPassengers.Add(hit.transform);
        //                float pushX = movement.x - (hit.distance - BoxColliderRayInfo.skinWidth) * directionX;
        //                float pushY = -BoxColliderRayInfo.skinWidth;

        //                passengerMovement.Add(new PassengerMovement(hit.transform, new Vector3(pushX, pushY), false, true));
        //            }
        //        }
        //    }
        //}

        // Passenger on top of a horizontally or downward moving platform
        if (directionY == -1)
        {
            float rayLength = BoxColliderRayInfo.skinWidth * 2;

            for (int i = 0; i < rayInfo.verticalRayCount; i++)
            {
                Vector2 rayOrigin = rayInfo.raycastOrigins.topLeft + Vector2.right * (rayInfo.verticalRaySpacing * i);
                RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.up, rayLength, passengerMask);

                if (hit && hit.distance != 0)
                {
                    if (!movedPassengers.Contains(hit.transform))
                    {
                        movedPassengers.Add(hit.transform);
                        float pushX = movement.x;
                        float pushY = movement.y;

                        passengerMovement.Add(new PassengerMovement(hit.transform, new Vector3(pushX, pushY), true, false));
                    }
                }
            }
        }
    }

    /// <summary>
    /// Mirem si tenim passatgers a sobre
    /// </summary>
    protected void FindPassengers()
    {
        HashSet<Transform> movedPassengers = new HashSet<Transform>();
        passengerMovement = new List<PassengerMovement>();

        for (int i = 0; i < rayInfo.verticalRayCount; i++)
        {
            // De moment mirem si no hi ha ningu, per tant sempre ho mirem a dalt
            Vector2 rayOrigin = rayInfo.raycastOrigins.topLeft;
            rayOrigin += Vector2.right * (rayInfo.verticalRaySpacing * i);
            // Sempre mirem amunt perque no hi ha ningu
            RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.up, BoxColliderRayInfo.skinWidth * 2, passengerMask);

            Debug.DrawRay(rayOrigin, Vector2.up * BoxColliderRayInfo.skinWidth * 2, Color.red);

            if (hit)
            {
                if (!movedPassengers.Contains(hit.transform))
                {
                    movedPassengers.Add(hit.transform);
                    passengerMovement.Add(new PassengerMovement(hit.transform, this.movement, true, false));
                }
            }
        }
    }

    /// <summary>
    /// Dibuxem les lines que mostren on anira a parar la plataforma
    /// </summary>
    void OnDrawGizmos()
    {
        Bounds bounds = GetComponent<BoxCollider2D>().bounds;

        Vector3 max = bounds.max;
        Vector3 min = bounds.min;

        // Estem en el modo play?
        if(Application.isPlaying)
        {
            max.y += originalY - transform.position.y;
            min.y += originalY - transform.position.y;
        }

        Gizmos.color = new Color(147, 0, 211);

        // Verticals
        Gizmos.DrawLine(min, min + new Vector3(0, -fallAmount));
        Gizmos.DrawLine(new Vector3(max.x, min.y), new Vector3(max.x, min.y - fallAmount));

        // Horitzontals
        Gizmos.DrawLine(new Vector3(min.x, max.y - fallAmount), new Vector3(max.x, max.y - fallAmount));
        Gizmos.DrawLine(new Vector3(min.x, min.y - fallAmount), new Vector3(max.x, min.y - fallAmount));

        // Diagonals
        Gizmos.DrawLine(new Vector3(min.x, max.y - fallAmount), new Vector3(max.x, min.y - fallAmount));
        Gizmos.DrawLine(new Vector3(min.x, min.y - fallAmount), new Vector3(max.x, max.y - fallAmount));
    }
}
