using UnityEngine;
using System.Collections;

public class BlueSubstancedElement : MonoBehaviour, IPowerGloveable
{
    /// <summary>
    /// La malla de la blue substance, per a poder-la rotar i amagar
    /// </summary>
    public GameObject mesh;

    /// <summary>
    /// El controller de l'enemic (si es enemic)
    /// </summary>
    private EnemyController _enemyController;

    /// <summary>
    /// La rotacio inicial de la malla
    /// </summary>
    private Quaternion meshRotation;

    /// <summary>
    /// El getter public de la mesh
    /// </summary>
    public GameObject meshObject
    {
        get
        {
            return this.mesh;
        }
    }

    /// <summary>
    /// Indica si es l'objecte que esta seguint la c�mera de power glove. Sempre ser� fal�
    /// </summary>
    public bool theChosen
    {
        get
        {
            return false;
        }
        set
        {
            
        }
    }

    private IPowerGloveable parent;

    public GameObject trail
    {
        get
        {
            return null;
        }

        set { }
    }

    /// <summary>
    /// La posicio inicial de l'objecte. Com que al ser destruit puja 0.1f, per a mantenir la posicio exacte farem servir aquesta variable
    /// </summary>
    private Vector3 originalLocalPosition;

	void Awake()
	{
		this.originalLocalPosition = this.transform.localPosition;
	}

    /// <summary>
    /// Al habilitar busquem la mesh i registrem els esdeveniments si existeix un EnemyController
    /// </summary>
    void OnEnable()
    {
        this.meshRotation = this.mesh.transform.rotation;

        this.GetComponent<Collider2D>().enabled = true;
        this.mesh.SetActive(true);

        if (transform.parent.GetComponent<EnemyController>())
        {
            _enemyController = transform.parent.GetComponent<EnemyController>();
            _enemyController.events.registerEvent(EnemyEvents.EventName.Rotate, OnRotate);
            _enemyController.events.registerEvent(EnemyEvents.EventName.Die, OnExplode);
        }

        if (transform.parent.GetInterface<IPowerGloveable>() != null)
        {
            this.parent = transform.parent.GetInterface<IPowerGloveable>();
        }
    }

    /// <summary>
    /// Al deshabilitar des-registrem els esdeveniments si existeix un EnemyController
    /// </summary>
    void OnDisable()
    {
        if (_enemyController)
        {
            _enemyController.events.unregisterEvent(EnemyEvents.EventName.Rotate, OnRotate);
            _enemyController.events.unregisterEvent(EnemyEvents.EventName.Die, OnExplode);
        }
    }

    /// <summary>
    /// Registrem aquest m�tode per a deshabilitar la blue substance si l'enemic mor
    /// </summary>
    /// <param name="enemy">El GameObject de l'enemic</param>
    void OnExplode(GameObject enemy)
    {
        disableSubstance();
    }

    /// <summary>
    /// Al rotar, mirem si la rotaci� que tenim �s la original. Si es aix�, rotem un valor preestablert. Sin�, recuperem la posici� inicial
    /// </summary>
    /// <param name="enemy">El GameObject de l'enemic</param>
    void OnRotate(GameObject enemy)
    {
        if (this.mesh.transform.rotation == this.meshRotation)
        {
            this.mesh.transform.rotation = Quaternion.Euler(0, -60f, 0);
        }
        else
        {
            this.mesh.transform.rotation = this.meshRotation;
        }
        
    }

    /// <summary>
    /// Al deshabilitar la blue substance, amaguem el collider i la mesh, i retornem la mesh a la seva rotaci� original (per si estava girada)
    /// </summary>
    private void disableSubstance()
    {
        this.GetComponent<Collider2D>().enabled = false;
        this.mesh.SetActive(false);
        this.mesh.transform.rotation = this.meshRotation;

        Instantiate(GeneralPurposeObjectManager.Instance.BlueSubstanceBreak, transform.position, Quaternion.identity);
    }

    /// <summary>
    /// Al detectar que ha estat colpejada, deshabilitem la blue substance
    /// </summary>
    /// <param name="lookingAtPoint">El vector de cap a on s'est� mirant. No el fem servir ja que no desplacem la blue substance</param>
    public void PowerGloved(Vector3 lookingAtPoint)
    {
        disableSubstance();
    }

    /// <summary>
    /// Her�ncia del IPowerGloveable. No el fem servir
    /// </summary>
    public void SaveState()
    {

    }

    /// <summary>
    /// Al acabar la trajectoria al ser colpejat, recuperem la blue substance
    /// </summary>
    public void RecoverState()
    {
        this.GetComponent<Collider2D>().enabled = true;
        this.mesh.SetActive(true);

		this.transform.localPosition = this.originalLocalPosition;
    }

    /// <summary>
    /// Al detectar una col�lisi� de powergloveable, deshabilitem la blue substance
    /// </summary>
    public void Collide(GameObject with)
    {
        disableSubstance();

        if (this.parent != null)
        {
            this.parent.Collide(with);
        }
    }

    /// <summary>
    /// Her�ncia del IPowerGloveable. No el fem servir
    /// </summary>
    public void DestroyByTime()
    {
        
    }
}
