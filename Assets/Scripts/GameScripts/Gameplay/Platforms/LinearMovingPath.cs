using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class LinearMovingPath : MovingPath
{
    /// <summary>
    /// Si al fer la darrera posició tornem a la primera, o desfem el camí recorregut
    /// </summary>
    public bool closeCircuit;

    /// <summary>
    /// Si aquesta casella està activada, quan arriba a la darrera posició desapareix la plataforma i s'en crea una de nova (movem el collider)
    /// </summary>
    public bool Spawner;

	/// <summary>
    /// Llista publica de posicions que defineixen els punts per on passa la plataforma
	/// </summary>
	public Vector3[] navigationPoints;

	/// <summary>
    /// Lista amb tots els punts per on es passa
	/// </summary>
    [HideInInspector]
	public Vector3[] points;

    /// <summary>
    /// Index del punt al que estem anant
    /// </summary>
    private int index = 0;

	// Use this for initialization
    protected virtual void Start()
    {
        PreparePoints();

        InstantiateElements();

        this.platform.transform.position = this.points[0];
	}

    /// <summary>
    /// Prepara l'array /points[]/ amb totes les posicions per les que ha de passar la plataforma
    /// </summary>
    protected void PreparePoints()
    {
        // Creem un nou array amb tots els punts. Si la ruta no és circular, els duplicarem al revés
        if (this.closeCircuit || this.Spawner)
        {
            this.points = new Vector3[this.navigationPoints.Length];
        }
        else
        {
            this.points = new Vector3[this.navigationPoints.Length * 2];
        }

        if(!this.useGraphicGuides)
        {
            this.lineRenderer.enabled = false;
        }
        else if (this.closeCircuit)
        {
            this.lineRenderer.SetVertexCount(this.navigationPoints.Length + 1);
        }
        else
        {
            this.lineRenderer.SetVertexCount(this.navigationPoints.Length);
        }

        for (int i = 0; i < this.navigationPoints.Length; i++)
        {
            this.points[i] = this.navigationPoints[i] + this.transform.position;

            if (this.useGraphicGuides)
            {
                this.lineRenderer.SetPosition(i, this.points[i]);
                
                // Posem les guies gráfiques
                if (this.graphicGuide != null)
                {
                    Instantiate(this.graphicGuide, this.points[i], Quaternion.identity);
                }
            }
        }

        if (!this.closeCircuit && !this.Spawner)
        {
            int j = this.navigationPoints.Length;
            for (int i = this.navigationPoints.Length - 1; i >= 0; i--)
            {
                this.points[j] = this.navigationPoints[i] + this.transform.position;
                j++;
            }
        }

        if (this.closeCircuit && this.useGraphicGuides)
        {
            this.lineRenderer.SetPosition(this.navigationPoints.Length, this.points[0]);
        }
    }

    /// <summary>
    /// Crea l'array d'elements. Es pot sobreescriure per l'herencia
    /// </summary>
    protected override void CreateElementsArray()
    {
        this.elements = new PathElement[this.numElements];
    }

    protected override void AddPathBehaviourAtElement(int index, GameObject element)
    {
        PathElement movingPathElement = element.AddComponent<PathElement>();
        movingPathElement.path =  this;
        this.elements[index] = movingPathElement;
    }

    /// <summary>
    /// Calcula la posició a la que ha d'anar un element
    /// </summary>
    /// <param name="index">El número de l'element</param>
    /// <returns>La posició a la que començarà</returns>
    protected override Vector3 CalculatePosition(int index, PathElement movingPathElement)
    {
        Vector3 position = this.points[0];

        if(index == 0)
        {
            return position;
        }

        float accumDistance = 0;

        int i = 0;

        bool end = false;

        while(!end)
        {
            while (!end && i < this.points.Length - 1)
            {
                // Debug.Log("i: " + i + " - distance: " + Vector3.Distance(this.points[i], this.points[i + 1]) + " - accumDistance: " + accumDistance + " - distance * index: " + this.distance * index);
                if (accumDistance + Vector3.Distance(this.points[i], this.points[i + 1]) >= this.distance * index)
                {
                    position = (this.distance * index - accumDistance) * Vector3.Normalize(this.points[i + 1] - this.points[i]) + this.points[i];
                    movingPathElement.index = i + 1;
                    end = true;
                }
                else
                {
                    accumDistance += Vector3.Distance(this.points[i], this.points[i + 1]);
                }
                i++;
            }
            i = 0;
        }

        return position;
    }

	    

    void OnDrawGizmos()
    {
        // Gizmos.DrawLine(this.platform.transform.position, this.navigationPoints[0].transform.position);

        for (int i = 0; i < this.navigationPoints.Length - 1; i++)
        {
            Gizmos.DrawSphere(this.transform.position + this.navigationPoints[i], 0.2f);

            Gizmos.DrawLine(this.transform.position + this.navigationPoints[i], this.transform.position + this.navigationPoints[(i + 1)]);

            //if (this.navigationPoints[i] != null && this.navigationPoints[i + 1] != null)
            //{
            //    Gizmos.DrawLine(this.transform.position + this.navigationPoints[i], this.transform.position + this.navigationPoints[(i + 1)]);
            //}
            //else if (this.navigationPoints[i] == null)
            //{
            //    this.navigationPoints = removeElement(i);
            //}
            //else if (this.navigationPoints[i + 1] == null)
            //{
            //    this.navigationPoints = removeElement(i + 1);
            //}
        }

        if (closeCircuit && this.navigationPoints.Length > 0)
        {
            Gizmos.DrawLine(this.transform.position + this.navigationPoints[0], this.transform.position + this.navigationPoints[this.navigationPoints.Length - 1]);
        }
    }

    private Vector3[] removeElement(int i)
    {
        Vector3[] newPoints = new Vector3[this.navigationPoints.Length - 1];
        for (int j = 0; j < i; j++)
        {
            newPoints[j] = navigationPoints[j];
        }

        for (int j = i + 1; j < this.navigationPoints.Length; j++)
        {
            newPoints[j - 1] = this.navigationPoints[j];
        }

        return newPoints;
    }
}
