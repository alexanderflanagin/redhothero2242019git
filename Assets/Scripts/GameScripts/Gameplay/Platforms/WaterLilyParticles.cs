﻿using UnityEngine;
using System.Collections;

public class WaterLilyParticles : MonoBehaviour {

	public ParticleSystem splashParticles;
	//public ParticleSystem rippleParticles;
	//public ParticleSystem rippleSingle;

	private ParticleSystem _tmp;

	private GameObject _lilyParticles;

	void OnEnable () {

		_lilyParticles = new GameObject();
		_lilyParticles.name = "LilyParticles " + this.transform.position.x;

		if (splashParticles){
			_tmp = Instantiate ( splashParticles, new Vector3 (transform.position.x, -1.5f, 0), Quaternion.Euler (splashParticles.transform.rotation.eulerAngles - transform.parent.rotation.eulerAngles ) ) as ParticleSystem;
			_tmp.transform.parent = this.transform;
			
			//splashParticles = _tmp;
		}
//		if (rippleParticles) {
//			_tmp = Instantiate ( rippleParticles, new Vector3 (transform.position.x, -1.50f, 0), Quaternion.Euler (Vector3.zero - transform.parent.rotation.eulerAngles )  ) as ParticleSystem;
//			_tmp.transform.parent = _lilyParticles.transform;
//			rippleParticles = _tmp;
//		}
//		if (rippleSingle) {
//			_tmp = Instantiate ( rippleSingle, new Vector3 (transform.position.x,  -0.295f, 0), Quaternion.Euler (Vector3.zero - transform.parent.rotation.eulerAngles )  ) as ParticleSystem;
//			_tmp.transform.parent = _lilyParticles.transform;
//			rippleSingle = _tmp;
//		}
	}

	void OnDisable () {
		Destroy (_lilyParticles);
	}

	public void OnTriggerEnter2D ( Collider2D other ) {
		Debug.Log (">>>>>>>>>>>>>>" + other);
		if  ( other.gameObject.layer == Layers.WaterOwn ) {
			if (splashParticles){
				splashParticles.Play ();
			}
//			if (rippleParticles){
//				Debug.Log ("Destroy " + rippleParticles);
//				Destroy ( rippleParticles.gameObject );
//			}
//			if (rippleSingle){
////				rippleBurst.transform.position += Vector3.up;
////				rippleBurst.enableEmission = true;
//				rippleSingle.Play ();
//				print ("PLAY RIPPLEBURST MAN");
//			}
		}
	}
}
