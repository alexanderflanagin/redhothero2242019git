using UnityEngine;
using System;
using System.Collections;

[RequireComponent (typeof (AudioSource) ) ]
public class CheckPoint : MonoBehaviour
{
    /// <summary>
    /// Objectes que estan activats des del principi i s'apaguen quan actives el check point
    /// </summary>
    public GameObject[] startActivated;

    /// <summary>
    /// Objectes que estan apagats des del principi i s'activen quan actives el check point
    /// </summary>
    public GameObject[] activatedOnTouch;

    [AudioClip(AudioClipsData.ClipsGroups.Enviro)]
    public string checkPoint = "CheckPoint";

    [AudioClip(AudioClipsData.ClipsGroups.Enviro)]
    public string checkPointRespawn;

    [HideInInspector]
    public bool startOnCheckpoint = false;

    /// <summary>
    /// Animator del check point
    /// </summary>
    private Animator animator;

    /// <summary>
    /// Hash de la variable per a activar el check point de l'animator
    /// </summary>
    private int animatorActivateHash = Animator.StringToHash("Activate");

    private bool refillingEnergy = false;

	void Awake()
    {
        this.animator = this.GetComponent<Animator>();
    }

    /// <summary>
    /// Apaguem els elements /activatedOnTouch/ i activem els elements /startActivated/
    /// </summary>
    void OnEnable()
    {
        Array.ForEach(startActivated, element => element.SetActive(true));
        Array.ForEach(activatedOnTouch, element => element.SetActive(false));
    }

    /// <summary>
    /// Si detecta al player, i encara no havia estat activat, el marca com a activat
    /// </summary>
    /// <param name="other">Collider que entra al seu trigger</param>
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag(Tags.Player))
        {
            if(!StandardStageManager.current.checkPointTouched)
            {
                CheckPointActivated();
            }

            StartCoroutine(RefillEnergy(other));
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if(other.CompareTag(Tags.Player))
        {
            refillingEnergy = false;
        }
    }

    /// <summary>
    /// Avisa al stage manager i mostra gràficament que ha estat activat
    /// </summary>
    public void CheckPointActivated()
    {
        if(startOnCheckpoint)
        {
            AudioManager.Instance.PlayOneShoot(AudioClipsData.ClipsGroups.Enviro, checkPointRespawn);
            startOnCheckpoint = false;
        }
        else
        {
            AudioManager.Instance.PlayOneShoot(AudioClipsData.ClipsGroups.Enviro, checkPoint);
        }

        SaveCheckPointData();

        this.animator.SetBool(this.animatorActivateHash, true);

        Array.ForEach(startActivated, element => element.SetActive(false));
        Array.ForEach(activatedOnTouch, element => element.SetActive(true));
    }

    /// <summary>
    /// Guarda la informació del check point
    /// </summary>
    private void SaveCheckPointData()
    {
        StandardStageManager.current.checkPointTouched = true;

        if (GameManager.Instance.checkPointProperties == null || GameManager.Instance.checkPointProperties.currentStage != Application.loadedLevel)
        {
            GameManager.Instance.checkPointProperties = new CheckPointProperties();
            GameManager.Instance.checkPointProperties.currentStage = Application.loadedLevel;
        }

        GameManager.Instance.checkPointProperties.startInCheckPoint = true;
        GameManager.Instance.checkPointProperties.coinsPickedPreCheckPoint = new bool[StandardStageManager.current.pickeablesPicked.Length];
        GameManager.Instance.checkPointProperties.powerGlove = StandardStageManager.current.playerControl.suit.hasPowerGlove;
        GameManager.Instance.checkPointProperties.blastGlove = StandardStageManager.current.playerControl.suit.hasBlastGlove;

        for (int i = 0; i < StandardStageManager.current.pickeablesPicked.Length; i++)
        {
            GameManager.Instance.checkPointProperties.coinsPickedPreCheckPoint[i] = StandardStageManager.current.pickeablesPicked[i];
        }
    }

    private IEnumerator RefillEnergy(Collider2D player)
    {
        refillingEnergy = true;

        do
        {
            StandardStageManager.current.playerControl.suit.energy.addEnergy(EnergyManager.POINTS_PER_BATTERY);
            yield return new WaitForSeconds(0.1f);
        } while (refillingEnergy);
    }
}
