using UnityEngine;
using Com.LuisPedroFonseca.ProCamera2D;

/// <summary>
/// Representacio grafica de les guies per a la camera d'scroll vertical
/// </summary>
public class VerticalCenterLines : MonoBehaviour
{
    /// <summary>
    /// Posici� d'inici de la l�nia esquerra
    /// </summary>
    private Vector3 _leftStart = Vector3.zero;
    /// <summary>
    /// Posici� de final per a la l�nia esquerra
    /// </summary>
    private Vector3 _leftEnd = Vector3.zero;

    /// <summary>
    /// Posici� d'inici de la l�nia dreta
    /// </summary>
    private Vector3 _rightStart = Vector3.zero;
    /// <summary>
    /// Posici� de final per a la l�nia dreta
    /// </summary>
    private Vector3 _rightEnd = Vector3.zero;

    /// <summary>
    /// Posici� d'inici de la l�nia del centre
    /// </summary>
    private Vector3 _centerStart = Vector3.zero;
    /// <summary>
    /// Posici� de final per a la l�nia del centre
    /// </summary>
    private Vector3 _centerEnd = Vector3.zero;

    /// <summary>
    /// Posici� d'inici de la l�nia que marca la posici� d'inici en les Y
    /// </summary>
    private Vector3 _startStart = Vector3.zero;
    /// <summary>
    /// Posici� de final de la l�nia que marca la posici� d'inici en les Y
    /// </summary>
    private Vector3 _startEnd = Vector3.zero;

    /// <summary>
    /// Posici� d'inici de la l�nia que marca la posici� de final en les Y
    /// </summary>
    private Vector3 _endStart = Vector3.zero;
    /// <summary>
    /// Posici� de final de la l�nia que marca la posici� de final en les Y
    /// </summary>
    private Vector3 _endEnd = Vector3.zero;

    /// <summary>
    /// Tranform del gameObject que representa el centre
    /// </summary>
    public Transform center;

    /// <summary>
    /// Tranform del gameObject que representa el l'inici
    /// </summary>
    public Transform start;

    /// <summary>
    /// Tranform del gameObject que representa el el final
    /// </summary>
    public Transform end;

    /// <summary>
    /// Dist�ncia lateral de les l�nies que marquen final de la c�mera per esquerra i dreta.
    /// Aix� no crec que sigui necess�ri, pero per si canviem com funciona la c�mera m�s endavant
    /// </summary>
    public float lateralDistance = 13f;

    private void Start()
    {
        AddCameraBoundaries();

        PrepareCameraLimits();
    }

    private void AddCameraBoundaries()
    {
        ProCamera2DNumericBoundaries numericBoundaries = Camera.main.transform.GetOrAddComponent<ProCamera2DNumericBoundaries>();
        numericBoundaries.LeftBoundary = center.transform.position.x - lateralDistance;
        numericBoundaries.RightBoundary = center.transform.position.x + lateralDistance;
    }

    /// <summary>
    /// Crea els colliders que mouen al player d'una banda a l'altre
    /// </summary>
    private void PrepareCameraLimits()
    {
        GameObject leftLimit = new GameObject("LeftLimit");
        leftLimit.transform.parent = transform;

        GameObject rightLimit = new GameObject("RightLimit");
        rightLimit.transform.parent = transform;

        VerticalLimit leftLimitLimit = leftLimit.AddComponent<VerticalLimit>();
        VerticalLimit rightLimitLimit = rightLimit.AddComponent<VerticalLimit>(); ;

        BoxCollider2D leftCollider = leftLimit.AddComponent<BoxCollider2D>();
        leftCollider.size = new Vector2(1, 300); // Li donem un marge a les Y perquè no es pugui passar saltant
        leftCollider.isTrigger = true;

        BoxCollider2D rightCollider = rightLimit.AddComponent<BoxCollider2D>();
        rightCollider.size = new Vector2(1, 300); // Li donem un marge a les Y perquè no es pugui passar saltant
        rightCollider.isTrigger = true;

        leftCollider.transform.position = new Vector3(center.transform.position.x - lateralDistance, 150);
        rightCollider.transform.position = new Vector3(center.transform.position.x + lateralDistance, 150);

        leftLimitLimit.rightTrigger = false;
        leftLimitLimit.otherPosition = rightCollider.transform.position.x;

        rightLimitLimit.rightTrigger = true;
        rightLimitLimit.otherPosition = leftCollider.transform.position.x;
    }


    // Update is called once per frame
    void OnDrawGizmos () {
#if UNITY_EDITOR
        if (center)
        {
            _leftStart = new Vector3(center.position.x - lateralDistance, 0);
            _leftEnd = new Vector3(center.position.x - lateralDistance, 300);

            _rightStart = new Vector3(center.position.x + lateralDistance, 0);
            _rightEnd = new Vector3(center.position.x + lateralDistance, 300);

            _centerStart = new Vector3(center.position.x, 0);
            _centerEnd = new Vector3(center.position.x, 300);
        }

        if (start)
        {
            _startStart = new Vector3(-30, start.position.y, 0);
            _startEnd = new Vector3(70, start.position.y, 0);
        }

        if (end)
        {
            _endStart = new Vector3(-30, end.position.y, 0);
            _endEnd = new Vector3(70, end.position.y, 0);
        }
#endif
        Gizmos.color = Color.blue;
        Gizmos.DrawLine(_centerStart, _centerEnd);
        Gizmos.color = Color.magenta;
        Gizmos.DrawLine(_leftStart, _leftEnd);
        Gizmos.DrawLine(_rightStart, _rightEnd);
        Gizmos.DrawLine(_startStart, _startEnd);
        Gizmos.DrawLine(_endStart, _endEnd);
	}
}
