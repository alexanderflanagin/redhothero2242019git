using UnityEngine;
using System.Collections;

public class StandardStageCamera : StageCamera
{
    /// <summary>
    /// Estats de la càmera, segons el que està fent
    /// </summary>
    public enum CamState
    {
        Wait, PlayerFollow, PowerGloved, Hop
    };

    protected float _cameraFollowSpeed = StageCamera.CAMERA_MAX_SPEED;

    protected static float TARGET_DISTANCE_SLOW = 5.5f;

    protected static float TARGET_DISTANCE_FAST = 10.0f;

    protected static float SMOOTH_DAMP_MOVED_TIME = 0.4f;

    protected static float SMOOTH_DAMP_NOT_MOVED_TIME = 0.6f;

    protected static float RUN_FRUSTUM_DIVIDER = 6;

    /// <summary>
    /// El control del player que esta seguint la camera
    /// </summary>
    protected CharacterControl _playerControl;


    protected MovementProperties _playerProps;
    //protected bool _dash;
    public bool _stillWallGrabbed;

    public bool showGuides = true;

    /// <summary>
    /// La posició mínima a la que pot estar la Y. Per defecte és NORMAL_MIN_Y
    /// </summary>
	private float _minY = 6.5f;
	
	public float playerToTopThreshold = -2.5f;

    public float playerToBottomThreshold = -3.5f;

    /// <summary>
    /// La Z en la que comença la càmera (per defecte -22f)
    /// </summary>
    protected float _startZ;

    /// <summary>
    /// Getter public del _startZ
    /// </summary>
    public float startZ
    {
        get
        {
            return _startZ;
        }
    }

    /// <summary>
    /// Per si algú ha modificat la Z actual
    /// </summary>
    public float currentZ;

    /// <summary>
    /// Amplada de la projecció de la càmera en el nostre pla
    /// </summary>
    protected float _frustumWidth;

    /// <summary>
    /// Alçada de la projecció de la càmera en el nostre pla
    /// </summary>
    protected float _frustumHeight;

    public float lerpTime;

    /// <summary>
    /// Velocitat cap a on mira la càmera (si es mou, i sinó cap a on a quedat mirant)
    /// </summary>
    private int _lookingDirection;

    /// <summary>
    /// Si ara està enganxada al player
    /// </summary>
    private bool _hookedToPlayer;

    /// <summary>
    /// Posicio (en les X) en la qual s'ha desenganxat el player
    /// </summary>
    private float unhookPosition;

    /// <summary>
    /// Velocitat horitzontal
    /// </summary>
    private float _horizontalSpeed;

    private float _lastPlayerXPosition;

    private float _lastTimeXMove;

    private float _horizontalMarginLimit = -5;

    // Use this for initialization
    protected void Start()
    {
        _currentPlayer = StandardStageManager.current.player;

        _playerControl = _currentPlayer.GetComponent<CharacterControl>();

        //SwapCamState(CamState.Wait);
    }

    void OnEnable()
    {
        //StandardStageManager.current.playerControl.events.registerEvent(CharacterEvents.EventName.HopEnter, OnHopStarts);
        //StandardStageManager.current.playerControl.events.registerEvent(CharacterEvents.EventName.DieEnter, OnPlayerDie);

        this.properties = new StandardStageCameraProperties();
    }

    void OnDisable()
    {
        //StandardStageManager.current.playerControl.events.unregisterEvent(CharacterEvents.EventName.HopEnter, OnHopStarts);
        //StandardStageManager.current.playerControl.events.unregisterEvent(CharacterEvents.EventName.DieEnter, OnPlayerDie);
    }

	void OnPlayerDie() {
		StopAllCoroutines ();
	}

    void OnHopStarts()
    {
        StopCoroutine("Wait");
        StopCoroutine("PlayerFollow");
        StopCoroutine("PowerGloved");
        StopCoroutine("Hop");

        SwapCamState(CamState.Hop);
    }
    
    public override void SetAtStartPosition()
    {
        // _startZ = -40f;
        _startZ = -30f;
        // _startZ = -15f;

        _minY = _startZ * -0.28f;

        currentZ = _startZ;

        transform.position = new Vector3(0, _minY, _startZ);

        // int direction = 1;

        // if(StandardStageManager.current.player.transform.position.y < )

        // Posem la càmera sobre el player
        CalculateFrustum();

        float y = StandardStageManager.current.player.transform.position.y + this._frustumHeight / 4;

        transform.position = new Vector3(this._currentPlayer.transform.position.x + this._frustumWidth / 2 - this._frustumWidth / 3, y, _startZ);
    }

    public void SwapCamState(CamState state)
    {
        // StopAllCoroutines();
        //switch (state)
        //{
        //    case CamState.Wait:
        //        StartCoroutine("Wait");
        //        break;
        //    case CamState.PlayerFollow:
        //        StartCoroutine("PlayerFollow");
        //        break;
        //    case CamState.PowerGloved:
        //        StartCoroutine("PowerGloved");
        //        break;
        //    case CamState.Hop:
        //        StartCoroutine("Hop");
        //        break;
        //}
    }

    //void OnDrawGizmos()
    //{
    //    float minY, maxY;

    //    minY = transform.position.y - _frustumHeight / 2;
    //    maxY = transform.position.y + _frustumHeight / 2;

    //    Gizmos.color = Color.green;
    //    if (this._playerControl && this._playerControl.currentHorizontal == CharacterControl.StateHorizontal.DensityJump)
    //    {
    //        Gizmos.DrawLine(new Vector3(transform.position.x, minY), new Vector3(transform.position.x, maxY));
    //        Gizmos.DrawLine(new Vector3(transform.position.x - _frustumWidth / 2, transform.position.y), new Vector3(transform.position.x + _frustumWidth / 2, transform.position.y));
    //    }
    //    else
    //    {
    //        Gizmos.DrawLine(new Vector3(transform.position.x - _frustumWidth / 2 + _frustumWidth / 3, minY), new Vector3(transform.position.x - _frustumWidth / 2 + _frustumWidth / 3, maxY));
    //        Gizmos.DrawLine(new Vector3(transform.position.x - _frustumWidth / 2 + _frustumWidth / 3 * 2, minY), new Vector3(transform.position.x - _frustumWidth / 2 + _frustumWidth / 3 * 2, maxY));

    //        Gizmos.color = Color.cyan;
    //        Gizmos.DrawLine(new Vector3(transform.position.x - _frustumWidth / 2 + _frustumWidth / 9 * 2, minY), new Vector3(transform.position.x - _frustumWidth / 2 + _frustumWidth / 9 * 2, maxY));
    //        Gizmos.DrawLine(new Vector3(transform.position.x - _frustumWidth / 2 + _frustumWidth / 9 * 4, minY), new Vector3(transform.position.x - _frustumWidth / 2 + _frustumWidth / 9 * 4, maxY));
    //        Gizmos.DrawLine(new Vector3(transform.position.x - _frustumWidth / 2 + _frustumWidth / 9 * 5, minY), new Vector3(transform.position.x - _frustumWidth / 2 + _frustumWidth / 9 * 5, maxY));
    //        Gizmos.DrawLine(new Vector3(transform.position.x - _frustumWidth / 2 + _frustumWidth / 9 * 7, minY), new Vector3(transform.position.x - _frustumWidth / 2 + _frustumWidth / 9 * 7, maxY));
    //    }

    //    Gizmos.color = Color.red;
    //    Gizmos.DrawLine(new Vector3(transform.position.x - _frustumWidth / 2, minY), new Vector3(transform.position.x - _frustumWidth / 2, maxY));
    //    Gizmos.DrawLine(new Vector3(transform.position.x - _frustumWidth / 2, maxY), new Vector3(transform.position.x + _frustumWidth / 2, maxY));
    //    Gizmos.DrawLine(new Vector3(transform.position.x + _frustumWidth / 2, maxY), new Vector3(transform.position.x + _frustumWidth / 2, minY));
    //    Gizmos.DrawLine(new Vector3(transform.position.x + _frustumWidth / 2, minY), new Vector3(transform.position.x - _frustumWidth / 2, minY));

    //}

    protected virtual void CalculateFrustum()
    {
        bool frustumFrameAnteriorEnllocDeDefinitiu = true;
        if (frustumFrameAnteriorEnllocDeDefinitiu)
        {
            _frustumHeight = 2.0f * Mathf.Abs(transform.position.z) * Mathf.Tan(this.GetComponent<Camera>().fieldOfView * 0.5f * Mathf.Deg2Rad);
        }
        else
        {
            _frustumHeight = 2.0f * Mathf.Abs(currentZ) * Mathf.Tan(this.GetComponent<Camera>().fieldOfView * 0.5f * Mathf.Deg2Rad);
        }
        
        _frustumWidth = _frustumHeight * Camera.main.aspect;
    }

    /// <summary>
    /// Calcula la posició en les X de la càmera, tenint en compte el player i totes les normes bàsiques
    /// </summary>
    /// <returns></returns>
    protected virtual float CalculateXPosition()
    {
        // Possibilitats
        // 1 - La càmera ja està enganxada a nosaltres i seguim en la mateixa direcció.
        // 1.1 - Si ens parem, es para. Enganxada. Velocitat = 0
        // 1.2 - Si correm, ens segueix. Enganxada. Velocitat = Velocitat player
        // 2 - La càmera està enganxada i canviem de direcció
        // 2.1 - Si trobem a la zona entre 1/4 i 1/3 o 2/3 i 3/4
        // 2.1.1 - Ens movem dintre d'aquesta zona. No Enganxada. Velocitat = 0
        // 2.1.2 - Arribem al límit de 1/4 o 3/4. Enganxada. Velocitat = acceleració per a posar-se a 1/3
        // 2.2 - Ens trobem entre 1/3 i 2/3. No Enganxada. Velocitat = 0
        // 3 - La càmera no està enganxada i ens movem en la mateixa direcció que ella
        // 3.1 - Si ens trobem entre 1/4 i 2/3. No enganxada. Velocitat = 0
        // 3.2 - Si ens trobem a 2/3. Enganxada. Velocitat = acceleració per a posar-se a 1/3
        // 3.3 - Si ens trobem entre 2/3 i 3/4. No enganxada. Velocitat = 0
        // 3.4 - Si ens trobem a més de 3/4. Enganxada. Velocitat = acceleració per a posar-se a 1/3
        
        // Posició actual de la càmera
        float cameraX = this.transform.position.x;

        // Per a saber si fem la transició suau o brusca
        bool softTransition = true;

        CalculateFrustum();

        float center = this.transform.position.x - _frustumWidth / StandardStageCamera.RUN_FRUSTUM_DIVIDER * _lookingDirection;
        //Debug.Log("transform: " + transform.position.x + " - center: " + center);
        float aNinth = _frustumWidth / 9;

        if (StillWallGrabbed())
        {
            return this.transform.position.x;
        }

        float distanceToCamera;

        distanceToCamera = this._frustumWidth / StandardStageCamera.RUN_FRUSTUM_DIVIDER * this._playerControl.LookingDirection;

        float followMaxSpeed = 0.8f, recoverAndRotateMaxSpeed = 1.2f;

        // La X a la que ens voldriem moure
        float desiredX = _playerControl.transform.position.x + distanceToCamera; // +_playerProps.speed * Time.deltaTime;

        float desiredSpeed = 0;

        //Debug.Log(string.Format("This position: {0} - Desired position: {1}", this.transform.position.x, desiredPosition), this.gameObject);

        // Primer mirem quina és la posició a la que ens volem moure
        if (_hookedToPlayer && _lookingDirection == _playerControl.LookingDirection)
        {
            if (desiredX * _lookingDirection >= this.transform.position.x * _lookingDirection)
            {
                // 1.1 i 1.2
                desiredSpeed = Mathf.Min(followMaxSpeed, Mathf.Abs(desiredX - transform.position.x)) * _lookingDirection;
                softTransition = false;
            }
            else
            {
                // 1.4
                desiredSpeed = 0;
                //_hookedToPlayer = true;
            }
        }
        else if (_hookedToPlayer)
        {
            // Al girar-se
            desiredSpeed = 0;
            _hookedToPlayer = false;
            this.unhookPosition = this._playerControl.transform.position.x;
            //Debug.Log("Unhook position: " + this.unhookPosition, this.gameObject);
        }
        else if (!this._hookedToPlayer)
        {
            if (this._playerControl.transform.position.x > this.unhookPosition + aNinth)
            {
                this._lookingDirection = 1;
                desiredSpeed = recoverAndRotateMaxSpeed + _playerControl.movementProps.speed;
                _hookedToPlayer = true;
                //Debug.Log("Re-hook position: " + this._playerControl.transform.position.x + " - a Ninth: " + aNinth, this.gameObject);
            }
            else if (this._playerControl.transform.position.x < this.unhookPosition - aNinth)
            {
                this._lookingDirection = -1;
                desiredSpeed = -recoverAndRotateMaxSpeed - _playerControl.movementProps.speed;
                _hookedToPlayer = true;
                //Debug.Log("Re-hook position negative: " + this._playerControl.transform.position.x + " - a Ninth: " + aNinth, this.gameObject);
            }
            else
            {
                this._hookedToPlayer = false;
                desiredSpeed = 0;
            }
        }

        float timeToRecoverPosition = 1f;
        float speedToRecoverPosition = 0.2f;

        // Per a posar la càmera automàticament on està el Player
        if (_playerControl.movementProps.speed == 0 && !_hookedToPlayer && desiredSpeed == 0)
        {
            // El portem lentament cap al player
            if (!Mathf.Approximately(desiredX, this.transform.position.x) && _lastTimeXMove + timeToRecoverPosition < Time.time)
            {
                //Debug.Log(newPosition + " ----- " + this.transform.position.x);
                if (desiredX < this.transform.position.x)
                {
                    _lookingDirection = -1;
                    desiredSpeed = Mathf.Min(speedToRecoverPosition, Mathf.Abs(this.transform.position.x - desiredX)) * _lookingDirection;
                    softTransition = false;
                    //Debug.Log("Més petit desired speed: " + desiredSpeed);
                }
                else
                {
                    _lookingDirection = 1;
                    desiredSpeed = Mathf.Min(speedToRecoverPosition, Mathf.Abs(desiredX - this.transform.position.x));
                    softTransition = false;
                    //Debug.Log("Més gran desired speed: " + desiredSpeed);
                }
            }
            else if (Mathf.Approximately(desiredX, this.transform.position.x))
            {
                _hookedToPlayer = true;
                desiredSpeed = 0;
            }
        }
        else
        {
            _lastTimeXMove = Time.time;
        }

        // Si fem lerp, corregim el moviment per a que sigui suau
        if (softTransition)
        {
            _horizontalSpeed = Mathf.Lerp(_horizontalSpeed, desiredSpeed, lerpTime);
        }
        else
        {
            _horizontalSpeed = desiredSpeed;
        }

        // Defineix si hem corregit ja la posició de la càmera
        bool corrected = testCameraXPositionForced(speedToRecoverPosition); 

        if (!corrected)
        {

            corrected = testPlayerInScreen(aNinth, ref cameraX);

            if (!corrected)
            {
                corrected = testScreenLimits();
            }
            //else
            //{
            //    Debug.Log("Corrected in testPlayerInScreen");
            //}
        }
        //else
        //{
        //    Debug.Log("Corrected in test screen limits");
        //}

        return cameraX + _horizontalSpeed;
    }

    /// <summary>
    /// Mira si la càmera, amb el moviment calculat, queda dins de la pantalla
    /// </summary>
    /// <returns>Retorna cert si hem modificat el moviment per a que no surti</returns>
    protected bool testScreenLimits()
    {
        bool corrected = false;
        
        // Si la càmera cau fora dels límits de la pantalla, la posem dins
        if (this.transform.position.x + _horizontalSpeed + _frustumWidth / 2 > StandardStageManager.current.rightLimit.transform.position.x - _horizontalMarginLimit)
        {
            _horizontalSpeed = (StandardStageManager.current.rightLimit.transform.position.x - _horizontalMarginLimit) - _frustumWidth / 2 - this.transform.position.x;
            _hookedToPlayer = false;
            corrected = true;
        }
        else if (this.transform.position.x + _horizontalSpeed - _frustumWidth / 2 < StandardStageManager.current.leftLimit.transform.position.x + _horizontalMarginLimit)
        {
            _horizontalSpeed = (StandardStageManager.current.leftLimit.transform.position.x + _horizontalMarginLimit) + _frustumWidth / 2 - this.transform.position.x;
            _hookedToPlayer = false;
            corrected = true;
        }

        return corrected;
    }

    /// <summary>
    /// Mira si tenim al player dins de la càmera
    /// </summary>
    /// <param name="aNinth">Un nové del frustrum actual</param>
    /// <param name="cameraX">La posicó X de la càmera (actual i modifcada)</param>
    /// <returns>Cert si hem modificat la càmera</returns>
    protected bool testPlayerInScreen(float aNinth, ref float cameraX)
    {
        bool corrected = false;

        // Si el player està sortint de càmera, el seguim "a saco"
        if (cameraX + _horizontalSpeed > _currentPlayer.transform.position.x + aNinth * 2.5f)
        {
            cameraX = this._currentPlayer.transform.position.x + aNinth * 2.5f;
            this._horizontalSpeed = 0;
            corrected = true;
        }
        else if (cameraX + _horizontalSpeed < _currentPlayer.transform.position.x - aNinth * 2.5f)
        {
            cameraX = this._currentPlayer.transform.position.x - aNinth * 2.5f;
            this._horizontalSpeed = 0;
            corrected = true;
        }

        return corrected;
    }

    /// <summary>
    /// Mira si estem forçant la posició de la càmera
    /// </summary>
    /// <param name="speedToRecoverPosition">Velocitat de la càmera al posar-se allà a on vol posar-se</param>
    protected bool testCameraXPositionForced(float speedToRecoverPosition)
    {
        speedToRecoverPosition *= 4;

        bool modified = false;

        // Primer mirem quan temps porta dins

        // Mirem si estem forçant la posició de les X des de l'exterior
        if (this.properties.forcedMaxX || this.properties.forcedMinX)
        {
            //Debug.Log(string.Format("Forced X: {0} - horizontal speed: {1} - this.transform.position.x: {2} - frame: {3}", this.properties.forcedX, this._horizontalSpeed, this.transform.position.x, Time.frameCount), this.gameObject);

            if (Mathf.Approximately(this.properties.forcedX, this._horizontalSpeed + this.transform.position.x))
            {
                //Debug.Log("No et moguis");
                // this._horizontalSpeed = 0;
            }
            else if (this.properties.forcedX > this.transform.position.x)
            {
                //Debug.Log("Horizontal speed previa: " + this._horizontalSpeed);

                float distance = this.properties.forcedX - this.transform.position.x;

                this._horizontalSpeed = Mathf.Min(speedToRecoverPosition, distance);

                //Debug.Log("Més gran. Distance: " + distance + " - Horizontal speed: " + this._horizontalSpeed);
            }
            else
            {
                float distance = Mathf.Abs(this.transform.position.x - this.properties.forcedX);

                this._horizontalSpeed = -Mathf.Min(speedToRecoverPosition, distance);

                //Debug.Log("Aprox. Distance: " + distance + " - Horizontal speed: " + this._horizontalSpeed);
            }

            modified = true;
        }

        this.properties.forcedMaxX = false;
        this.properties.forcedMinX = false;

        return modified;
    }

    /// <summary>
    /// Calcula la posició de la Y en el frame actual seguint les normes per defecte
    /// </summary>
    /// <param name="direction"></param>
    /// <returns></returns>
    protected float CalculateYPosition(ref int direction)
    {
        float y = 0;

        float lastY = transform.position.y;

        bool lookingAround = false;

        float lookingAroundMovementSpeedDivider = 6;

        float difference;

        _minY = currentZ * -0.28f;

        // Estem mirant amb les Y?
        if (_playerControl.input.yAxis.check && CanLookAround(_playerControl))
        {
            lookingAround = true;
            y += _playerControl.input.yAxis.val / lookingAroundMovementSpeedDivider;
        }
        else
        {
            lookingAround = false;
        }


        difference = _currentPlayer.transform.position.y - lastY + y;
        
        if (!lookingAround)
        {
            if (difference > playerToTopThreshold)
            {
                y = Mathf.SmoothStep(0, _currentPlayer.transform.position.y - playerToTopThreshold - lastY, SMOOTH_DAMP_MOVED_TIME);

                direction = 1;
            }

            if (difference < playerToBottomThreshold)
            {
                // y = _currentPlayer.transform.position.y - playerToBottomThreshold - lastY;
                y = Mathf.SmoothStep(0, _currentPlayer.transform.position.y - playerToBottomThreshold - lastY, SMOOTH_DAMP_MOVED_TIME);
                direction = -1;
            }
        }
        else
        {
            // difference = y - transform.position.y;
            if (difference > 1.5f)
            {
                y -= _playerControl.input.yAxis.val / lookingAroundMovementSpeedDivider;
                direction = 1;
            }

            if (difference < -6)
            {
                y -= _playerControl.input.yAxis.val / lookingAroundMovementSpeedDivider;
                direction = -1;
            }
        }

        if (Mathf.Abs(y) > CAMERA_MAX_SPEED)
        {
            y = direction * CAMERA_MAX_SPEED;
        }

        y += lastY;

        y = secureY(y);

        return y;
    }

    protected float secureY(float y)
    {
        float newY = y;

        float speedToRecoverPosition = 0.1f;

        // Mirem si estem forçant la posició de les Y des de l'exterior
        if (this.properties.forcedMaxY || this.properties.forcedMinY)
        {
            if (Mathf.Approximately(this.properties.forcedY, this.transform.position.y))
            {
                newY = this.properties.forcedY;
            }
            else if (this.properties.forcedY > this.transform.position.y)
            {
                newY = this.transform.position.y + Mathf.Min(speedToRecoverPosition, this.properties.forcedY - this.transform.position.y);
            }
            else
            {
                newY = this.transform.position.y - Mathf.Min(speedToRecoverPosition, this.transform.position.y - this.properties.forcedY);
            }

            this.properties.forcedMaxY = false;
            this.properties.forcedMinY = false;
        }

        if (newY > this.maxHeight)
        {
            newY = this.maxHeight;
        }

        if (newY < this._minY)
        {
            newY = this._minY;
        }

        return newY;
    }

    protected float CalculateZPosition()
    {
        float z = currentZ;

        if (currentZ > -15f)
        {
            currentZ = z = -15f;
        }
        else if (currentZ < -40f)
        {
            currentZ = z = -40f;
        }

        return z;
    }

    #region Single Player FSM
    IEnumerator Wait()
    {
        while (!_canFollow)
        {
            yield return null;
        }

        SwapCamState(CamState.PlayerFollow);
    }

    private IEnumerator PlayerFollow()
    {
        float x = 0, y = 0, z;

        int direction = 1;

        _currentPlayer = StandardStageManager.current.player;
        _playerControl = StandardStageManager.current.playerControl;
        _playerProps = _currentPlayer.GetComponent<CharacterControl>().movementProps;

        _lookingDirection = _playerControl.LookingDirection;

        _hookedToPlayer = true;

        _horizontalSpeed = 0f;

        _lastPlayerXPosition = _currentPlayer.transform.position.x;

        while (_canFollow)
        {
            x = CalculateXPosition();

            y = CalculateYPosition(ref direction);

            z = CalculateZPosition();

            transform.position = new Vector3(x, y, z);

            // yield return new WaitForEndOfFrame();
            yield return null;
        }

        SwapCamState(CamState.Wait);
    }

    /// <summary>
    /// S'ocupa de seguir al Player quan aquest fa un HOP.
    /// El centra en les X i en les Y, però primer s'espera per a seguir-lo, sense haver de "tornar enrera"
    /// </summary>
    /// <returns></returns>
    protected IEnumerator Hop()
    {
        float x = 0, startX = _currentPlayer.transform.position.x;
        float y = 0, startY = _currentPlayer.transform.position.y;
        yield return null;
        yield return null;

        int direction = 1;
        
        do
        {
            x = transform.position.x;
            y = CalculateYPosition(ref direction);
            //Debug.Log(string.Format("Player x {0} - Start X {1} - Position x {2} - Player y {3} - Start y {4} - Position y {5}", _currentPlayer.transform.position.x, startX, transform.position.x, _currentPlayer.transform.position.y, startY, transform.position.y), this.gameObject);
            if ((_currentPlayer.transform.position.x > startX && x <= _currentPlayer.transform.position.x) || (_currentPlayer.transform.position.x < startX && x >= _currentPlayer.transform.position.x))
            {
                // Si el player era més a l'esquerra i la càmera és més a l'esquerra que el player O si el player era més a la dreta i la càmera és més a la dreta que el player
                x = _currentPlayer.transform.position.x;
            }

            if ((_currentPlayer.transform.position.y > startY && y <= _currentPlayer.transform.position.y) || (_currentPlayer.transform.position.y < startY && y >= _currentPlayer.transform.position.y))
            {
                // Si el player era més avall i la càmera és més avall que el player O si el player era més amunt i la càmera és més amunt que el player
                y = _currentPlayer.transform.position.y;
                y = secureY(y);
            }

            //Debug.Log(string.Format("x {0} - y {1}", x, y), this.gameObject);

            if (x + _frustumWidth / 2 > StandardStageManager.current.rightLimit.transform.position.x - _horizontalMarginLimit)
            {
                x = (StandardStageManager.current.rightLimit.transform.position.x - _horizontalMarginLimit) - _frustumWidth / 2;
            }
            else if (x - _frustumWidth / 2 < StandardStageManager.current.leftLimit.transform.position.x + _horizontalMarginLimit)
            {
                x = (StandardStageManager.current.leftLimit.transform.position.x + _horizontalMarginLimit) + _frustumWidth / 2;
            }

            transform.position = new Vector3(x, y, CalculateZPosition());
            yield return null;
        } while (_playerControl.movementFSM.currentVertical == PlayerState.Vertical.Travel);

        SwapCamState(CamState.PlayerFollow);
    }

    #endregion

    protected bool CanLookAround(CharacterControl playerControl)
    {
        bool can = false;

        float threshold = 0.4f;

        // can = _currentGuiriControl.currentHorizontal == CharacterControl.StateHorizontal.Idle && _currentGuiriControl.currentVertical == CharacterControl.StateVertical.Grounded && _currentGuiriControl.input.yAxis.check;

        can = Mathf.Abs(playerControl.transform.position.x - transform.position.x) <= threshold && playerControl.movementProps.speed == 0;

        return can && false;
    }

    protected bool StillWallGrabbed()
    {
        if (_playerControl.movementFSM.currentVertical == PlayerState.Vertical.WallGrab && !_playerControl.IsGrounded())
        {
            _stillWallGrabbed = true;
        }
        if (_playerControl.IsGrounded())
        {
            _stillWallGrabbed = false;
        }
        if (Mathf.Abs(transform.position.x - _currentPlayer.transform.position.x) > _frustumWidth / 4)
        {
            _stillWallGrabbed = false;
        }
        return _stillWallGrabbed;
    }

    public override void SwitchPhysicsControllerBetweenLimits(GameObject obj, bool right) { }

    //void Update()
    //{
    //    if (this.properties != null)
    //    {
    //        if (this._playerControl.IsGrounded())
    //        {
    //            this.properties.lastGroundedPosition = this._playerControl.transform.position.y;

    //            this.properties.lastGroundedTime = Time.time;
    //        }
    //    }
    //}
}

