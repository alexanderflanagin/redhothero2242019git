using UnityEngine;
using System.Collections;

/// <summary>
/// Classe general per a gestionar les càmeres dels Stages
/// </summary>
public abstract class StageCamera : MonoBehaviour {

    /// <summary>
    /// Velocitat máxima de moviment de la càmera
    /// </summary>
    public static float CAMERA_MAX_SPEED = 80.0f;

    /// <summary>
    /// A quin player esta seguint la camera
    /// </summary>
    protected GameObject _currentPlayer;

    /// <summary>
    /// Getter per al ScenarioManager
    /// </summary>
    public GameObject currentPlayer { get { return _currentPlayer;  } }

    /// <summary>
    /// Propietats de la càmera
    /// </summary>
    public StandardStageCameraProperties properties;

    /// <summary>
    /// Max Height given by the current scene
    /// </summary>
    public float maxHeight { get; set; }

    /// <summary>
    /// Indica si la càmera pot seguir o no al Player
    /// </summary>
    protected bool _canFollow;

    /// <summary>
    /// Propietat pública del _canFollow
    /// </summary>
    public bool canFollow
    {
        get
        {
            return _canFollow;
        }
        set
        {
            _canFollow = value;
        }
    }

    /// <summary>
    /// Les càmeres verticals i horitzontals poden tenir diferents principis, així que cadascú ho gestionarà diferent.
    /// </summary>
    public abstract void SetAtStartPosition();

    public abstract void SwitchPhysicsControllerBetweenLimits(GameObject obj, bool right);
}
