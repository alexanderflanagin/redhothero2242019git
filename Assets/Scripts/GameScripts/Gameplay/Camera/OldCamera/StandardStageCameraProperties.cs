using UnityEngine;
using System.Collections;

public class StandardStageCameraProperties
{
    // Última posició en Y a la que hem estat grounded
    public float lastGroundedPosition;

    // Últim time en el que hem estat grounded
    public float lastGroundedTime;

    public float enterTime;

    public float forcedX;

    public float forcedY;

    public bool forcedMinX;

    public bool forcedMaxX;

    public bool forcedMinY;

    public bool forcedMaxY;
}
