using UnityEngine;
using System.Collections;

/// <summary>
/// Representaci� gr�fica de les guies per a la c�mera d'scroll vertical
/// </summary>
[ExecuteInEditMode]
public class MultiplayerCenterLines : MonoBehaviour
{
    /// <summary>
    /// Posici� d'inici de la l�nia esquerra
    /// </summary>
    private Vector3 _leftStart = Vector3.zero;
    /// <summary>
    /// Posici� de final per a la l�nia esquerra
    /// </summary>
    private Vector3 _leftEnd = Vector3.zero;

    /// <summary>
    /// Posici� d'inici de la l�nia dreta
    /// </summary>
    private Vector3 _rightStart = Vector3.zero;
    /// <summary>
    /// Posici� de final per a la l�nia dreta
    /// </summary>
    private Vector3 _rightEnd = Vector3.zero;

    /// <summary>
    /// Posici� d'inici de la l�nia del centre
    /// </summary>
    private Vector3 _centerStart = Vector3.zero;
    /// <summary>
    /// Posici� de final per a la l�nia del centre
    /// </summary>
    private Vector3 _centerEnd = Vector3.zero;

    /// <summary>
    /// Tranform del gameObject que representa el centre
    /// </summary>
    public Transform center;

    /// <summary>
    /// Dist�ncia lateral de les l�nies que marquen final de la c�mera per esquerra i dreta.
    /// Aix� no crec que sigui necess�ri, pero per si canviem com funciona la c�mera m�s endavant
    /// </summary>
    public float lateralDistance = 13f;

	// Update is called once per frame
	void OnDrawGizmos () {
#if UNITY_EDITOR
        if (center)
        {
            _leftStart = new Vector3(center.position.x - lateralDistance, 0);
            _leftEnd = new Vector3(center.position.x - lateralDistance, 300);

            _rightStart = new Vector3(center.position.x + lateralDistance, 0);
            _rightEnd = new Vector3(center.position.x + lateralDistance, 300);

            _centerStart = new Vector3(center.position.x, 0);
            _centerEnd = new Vector3(center.position.x, 300);
        }
#endif
        Gizmos.color = Color.blue;
        Gizmos.DrawLine(_centerStart, _centerEnd);
        Gizmos.color = Color.magenta;
        Gizmos.DrawLine(_leftStart, _leftEnd);
        Gizmos.DrawLine(_rightStart, _rightEnd);
	}
}
