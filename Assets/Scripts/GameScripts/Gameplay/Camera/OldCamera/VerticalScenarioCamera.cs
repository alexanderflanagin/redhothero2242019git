using UnityEngine;
using System.Collections;
//[ExecuteInEditMode]


public class VerticalScenarioCamera : StageCamera
{
	/// <summary>
    /// El control del player que esta seguint la camera
	/// </summary>
	// private CharacterControl _currentPlayerControl;

	public bool showGuides = true;

    /// <summary>
    /// Distancia en les Z entre la càmera i el 0
    /// </summary>
    public float distance = 38f;

    /// <summary>
    /// Ens marcarà on està el centre de la càmera. A partir d'aquest punt no es podrà moure cap als costats excepte si està dalt o baix
    /// </summary>
    public Transform verticalCenter;

    /// <summary>
    /// Guardem la X del vertical Centre inicial (es mou amb la càmera)
    /// </summary>
    private float _verticalCenterX;

    /// <summary>
    /// Ens marcarà quan som a dalt de tot i ens podem moure cap a l'esquerra altre cop.
    /// </summary>
    public Transform startVerticalPosition;

    /// <summary>
    /// Guardem la Y del top position (es mou amb la càmera).
    /// </summary>
    private float _startVerticalPositionY;

    /// <summary>
    /// Ens marcarà a partir de quina alçada mínima podem tornar a seguir al Player (per l'esquerra)
    /// </summary>
    public Transform endVerticalPosition;

    /// <summary>
    /// Guardem la Y de l'alçada mínima
    /// </summary>
    private float _endVerticalPositionY;

    /// <summary>
    /// Diu si la càmera es mou a velocitat fixe o segueix al Player
    /// </summary>
    public bool fixedSpeed = true;

    /// <summary>
    /// Diu si la càmera puja o baixa
    /// </summary>
    public bool goingUp = true;

    public float playerToTopThreshold = -3.3f;

    public float playerToBottomThreshold = -4.8f;

#if UNITY_EDITOR
    void OnEnable()
    {
        // Mirem que tingui totes les posicions necessaris (start, end i center)
        if (verticalCenter == null)
        {
            Debug.LogError("La camera vertical no te definit el centre", this.gameObject);
            Debug.Break();
        }

        if (startVerticalPosition == null)
        {
            Debug.LogError("La camera vertical no te definit l'inici", this.gameObject);
            Debug.Break();
        }

        if (startVerticalPosition == null)
        {
            Debug.LogError("La camera vertical no te definit el final", this.gameObject);
            Debug.Break();
        }
    }
#endif

	// Use this for initialization
    void Start()
    {
        _currentPlayer = StandardStageManager.current.player;

        //_currentPlayerControl = _currentPlayer.GetComponent<CharacterControl>();

        // Posem els límits a la càmera i amagem els GameObjects
        _verticalCenterX = verticalCenter.position.x;

        _startVerticalPositionY = startVerticalPosition.position.y;
        _endVerticalPositionY = endVerticalPosition.position.y;

        if (fixedSpeed)
        {
            StartCoroutine(FixedVerticalMovement());
        }
        else
        {
            StartCoroutine(CameraY());
        }

        // Preparem els límits de la càmera
        PrepareCameraLimits();

		// TODO Si la càmera puja, ens carreguem la zona de kill de dalt (segons GDD pàgina 26)
        if (goingUp)
        {
            transform.Find("TopKillField").gameObject.SetActive(false);
        }
	}

    public override void SetAtStartPosition()
    {
        float frustumHeight = 2.0f * distance * Mathf.Tan(Camera.main.fieldOfView * 0.5f * Mathf.Deg2Rad);

        _startVerticalPositionY = _startVerticalPositionY + frustumHeight / 2;

        transform.position = new Vector3(_verticalCenterX, _startVerticalPositionY, -distance);
    }

    private void PrepareCameraLimits()
    {
        GameObject leftLimit = new GameObject("LeftLimit");
        leftLimit.transform.parent = transform;
        
        GameObject rightLimit = new GameObject("RightLimit");
        rightLimit.transform.parent = transform;

        leftLimit.AddComponent<VerticalLimit>().rightTrigger = false;
        rightLimit.AddComponent<VerticalLimit>().rightTrigger = true;

        BoxCollider2D leftCollider = leftLimit.AddComponent<BoxCollider2D>();
        leftCollider.size = new Vector2(1, _endVerticalPositionY - _startVerticalPositionY + 40); // Li donem un marge a les Y perquè no es pugui passar saltant
        leftCollider.isTrigger = true;

        BoxCollider2D rightCollider = rightLimit.AddComponent<BoxCollider2D>();
        rightCollider.size = new Vector2(1, _endVerticalPositionY - _startVerticalPositionY + 40); // Li donem un marge a les Y perquè no es pugui passar saltant
        rightCollider.isTrigger = true;

        leftCollider.transform.localPosition = new Vector3(-verticalCenter.localScale.x / 2 + 0.5f, (_endVerticalPositionY - _startVerticalPositionY) /2, -distance);
        rightCollider.transform.localPosition = new Vector3(verticalCenter.localScale.x / 2 - 0.5f, (_endVerticalPositionY - _startVerticalPositionY) / 2, -distance);
    }

    /// <summary>
    /// Fa que si un element surt per una banda, aparegui per l'altre
    /// </summary>
    /// <returns></returns>
    public override void SwitchPhysicsControllerBetweenLimits(GameObject element, bool fromRight)
    {
        float x;

        if(fromRight) {
            x = _verticalCenterX - verticalCenter.localScale.x / 2;
        }
        else
        {
            x = _verticalCenterX + verticalCenter.localScale.x / 2;
        }

        element.transform.position = new Vector3(x, element.transform.position.y);
        
    }

    /// <summary>
    /// Mou la càmera de baixada de forma fixe
    /// </summary>
    /// <returns></returns>
    public IEnumerator FixedVerticalMovement()
    {
        // Definim la velocitat màxima i la velocitat a la que comencem
        float speed = 1f;

        // Definim un multiplicador per a la velocitat. Si anem de baixada serà negatiu
        float multiplier = 1f;

        if (!goingUp)
        {
            multiplier = -1;
        }

        // Guardem les variables de forma local per a un accés més ràpid
        float x = transform.position.x;
        float z = transform.position.z;

        while (true)
        {
            // Incrementem la velocitat per a que no sigui un canvi brusc
            if (speed < CAMERA_MAX_SPEED)
            {
                speed = speed * 1.1f;
            }

            // Si ens passem, la fixem al max
            if (speed > CAMERA_MAX_SPEED)
            {
                speed = CAMERA_MAX_SPEED;
            }

            
            // Si encara no hem arribat al final (sigui de pujada o baixada), incrementem la posiciós
            if ((goingUp && transform.position.y < _endVerticalPositionY) || (!goingUp && transform.position.y > _endVerticalPositionY))
            {
                transform.position = new Vector3(x, transform.position.y + speed * Time.deltaTime * multiplier, z);
            }
            else
            {
                transform.position = new Vector3(transform.position.x, _endVerticalPositionY, z);
            }
            
            yield return null;
        }
    }

    public IEnumerator CameraY()
    {
        yield return null;

		float y;

		int direction = 1;
		float difference = 0;

        //Debug.Log("Initial transform.position.y: " + transform.position.y, this.gameObject);

        while (true)
        {
            if (showGuides)
            {
                Debug.DrawLine(new Vector2(transform.position.x - 20, transform.position.y + 4), new Vector2(transform.position.x + 2060, transform.position.y + 4), Color.red);
                Debug.DrawLine(new Vector2(transform.position.x - 20, transform.position.y - 4), new Vector2(transform.position.x + 2060, transform.position.y - 4), Color.red);
                Debug.DrawLine(new Vector2(-20, -2), new Vector2(500, -2), Color.green);
                Debug.DrawLine(new Vector2(-20, 16), new Vector2(500, 16), Color.green);
                Debug.DrawLine(new Vector2(-20, 30), new Vector2(500, 30), Color.green);
            }
			// y = desplaçament

			y = 0;

			difference = _currentPlayer.transform.position.y - transform.position.y;

            //Debug.Log(string.Format("transform.position.y: {0} - _currentGuiri.transform.position.y: {1} - difference: {2} ", transform.position.y, _currentPlayer.transform.position.y, difference), this.gameObject);

            if (difference > playerToTopThreshold)
            {
                y = _currentPlayer.transform.position.y - playerToTopThreshold - transform.position.y;
                direction = 1;
            }

            if (difference < playerToBottomThreshold)
            {
                y = _currentPlayer.transform.position.y - playerToBottomThreshold - transform.position.y;
                direction = -1;
            }

            if (Mathf.Abs(y) > JumpProperties.MAX_FALLING_SPEED * Time.deltaTime)
            {
                y = direction * (JumpProperties.MAX_FALLING_SPEED * Time.deltaTime);
            }

			y = transform.position.y + y;

            if ((goingUp && y < _startVerticalPositionY) || (!goingUp && y > _startVerticalPositionY))
            {
                y = _startVerticalPositionY;
            }
            else if ((goingUp && y > _endVerticalPositionY) || (!goingUp && y < _endVerticalPositionY))
            {
                y = _endVerticalPositionY;
            }

			transform.position = new Vector3 (transform.position.x, y, transform.position.z);

			yield return null;
		}
	}
}

