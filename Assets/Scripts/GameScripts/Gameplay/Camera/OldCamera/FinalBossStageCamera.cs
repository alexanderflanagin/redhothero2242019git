using UnityEngine;
using System.Collections;

public class FinalBossStageCamera : StageCamera
{
    void OnEnable()
    {
        Destroy(this.GetComponent<Animator>());
    }
    
    public override void SetAtStartPosition()
    {
        transform.position = new Vector3(17, 10, -30);
    }

    public override void SwitchPhysicsControllerBetweenLimits(GameObject obj, bool right) { }
}

