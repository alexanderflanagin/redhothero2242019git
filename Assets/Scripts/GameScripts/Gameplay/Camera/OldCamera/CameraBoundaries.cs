﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class CameraBoundaries : MonoBehaviour {

	private const int POINTS = 310;

	private const float MAX_HEIGHT = 900;

	private const float CAMERA_MARGIN = 7.5f;

	public static float[] CreateBoundaries() {
		// Aixo podra ser guardat com a variable i no calculat cada vegada
		float[] values = new float[POINTS];

		LayerMask layerMask = 1 << LayerMask.NameToLayer("CameraBoundaries");

		RaycastHit2D hit;

		Vector2 start;
		Vector2 end;

		for( int i = 0; i < POINTS; i++ ) {
			start = new Vector2(i, 0);
			end = new Vector2(i, MAX_HEIGHT);

			if( hit = Physics2D.Linecast( start, end, layerMask ) ) {
				values[i] = hit.point.y - CAMERA_MARGIN;
			} else {
				values[i] = MAX_HEIGHT;
			}
		}

		return values;
	}
}
