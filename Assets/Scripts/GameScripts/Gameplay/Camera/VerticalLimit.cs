using UnityEngine;
using System.Collections;

public class VerticalLimit : MonoBehaviour
{
    /// <summary>
    /// Si es el trigger de la dreta o el de l'esquerra
    /// </summary>
    public bool rightTrigger;

    /// <summary>
    /// Posicio a la que hem de moure si atravessa el trigger
    /// </summary>
    public float otherPosition;

    /// <summary>
    /// Su l'element que surt del trigger és Physics controlleable, avisem al _parent
    /// </summary>
    /// <param name="other">Collider de l'element que surt del trigger</param>
    void OnTriggerExit2D(Collider2D other)
    {
        Physics2DController controller = other.GetComponent<Physics2DController>();
        if (controller)
        {
            // És el player o un enemic!
            if ((!rightTrigger && controller.collisions.faceDir == -1) || (rightTrigger && controller.collisions.faceDir == 1))
            {
                SwitchPhysicsControllerBetweenLimits(other.gameObject);
            }
            return;
        }

        // Es projectil enemic?
        EnemyBasicProjectile enemyProjectile = other.GetComponent<EnemyBasicProjectile>();

        if(enemyProjectile)
        {
            if ((!rightTrigger && enemyProjectile.transform.position.x < transform.position.x) || (rightTrigger && enemyProjectile.transform.position.x > transform.position.x))
            {
                enemyProjectile.transform.position = new Vector3(otherPosition, enemyProjectile.transform.position.y);
            }
            return;
        }

        // Es projectil propi?
        BlastGlovesBasicProjectile playerProjectile = other.GetComponent<BlastGlovesBasicProjectile>();

        if (playerProjectile)
        {
            if ((!rightTrigger && playerProjectile.transform.position.x < transform.position.x) || (rightTrigger && playerProjectile.transform.position.x > transform.position.x))
            {
                GameObject projectile = GameObjectPooler.Instance.GetProjectile(ProjectilesManager.Projectiles.BlastGlovesBasic);
                projectile.transform.position = new Vector3(otherPosition, playerProjectile.transform.position.y);

                projectile.transform.forward = playerProjectile.transform.forward;

                BlastGlovesBasicProjectile newPlayerProjectile = projectile.GetComponent<BlastGlovesBasicProjectile>();
                newPlayerProjectile.target = playerProjectile.target;
                newPlayerProjectile.owner = null;

                projectile.SetActive(true);

                newPlayerProjectile.Invoke("OnDestroyProjectile", Projectile.PROJECTILE_REENTER_LIFETIME);

                playerProjectile.gameObject.SetActive(false);
            }
            return;
        }
    }

    /// <summary>
    /// Fa que si un element surt per una banda, aparegui per l'altre
    /// </summary>
    /// <returns></returns>
    public void SwitchPhysicsControllerBetweenLimits(GameObject element)
    {
        ResetTrail(element);

        element.transform.position = new Vector3(otherPosition, element.transform.position.y);
    }

    private void ResetTrail(GameObject element)
    {
        TrailRenderer trail = element.GetComponentInChildren<TrailRenderer>();

        if (trail)
        {
            trail.transform.SetParent(null);

            // Fem que s'amagui sol el trail quan s'acabi
            StartCoroutine(HideTrail(trail));

            // Generem el trail nou
            //GameObject newTrail = Instantiate(GeneralPurposeObjectManager.Instance.PowerGlovedDefaultTrail, element.transform.position + , Quaternion.identity) as GameObject;
            //newTrail.transform.parent = element.transform;

            // Si es IPoerGloveable, guardem la referència del trail
            PowerGloved powerGloved = element.GetComponent<PowerGloved>();
            if (powerGloved != null)
            {

                powerGloved.AddTrail(element.gameObject.GetComponent<Collider2D>().offset);
            }
        }
    }

    /// <summary>
    /// Amaga un trail que hagi quedat "a l'aire" al atravessar de banda a banda.
    /// Sempre revisem que existeixi perque es pot apagar varies vegades aquesta coroutina (si l'objecte atravessa varies vegades)
    /// </summary>
    /// <param name="trail">El trail que hem d'amagar</param>
    /// <returns></returns>
    private IEnumerator HideTrail(TrailRenderer trail)
    {
        GameObject trailGameObject = trail.gameObject;

        while (trailGameObject && trailGameObject.activeInHierarchy && trail.isVisible)
        {
            yield return null;
        }

        if(trailGameObject && trailGameObject.activeInHierarchy)
        {
            trailGameObject.SetActive(false);
        }
    }
}
