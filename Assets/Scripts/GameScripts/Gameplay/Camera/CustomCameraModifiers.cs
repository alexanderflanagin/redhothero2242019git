using UnityEngine;
using System.Collections;
using Com.LuisPedroFonseca.ProCamera2D;

public class CustomCameraModifiers : MonoBehaviour
{
    /// <summary>
    /// Velocitat a la que fem el canvi de un estat a l'altre
    /// </summary>
    public float speedMultiplier = 10;
    
    /// <summary>
    /// Component que gestiona el window de la camera
    /// </summary>
    private ProCamera2DCameraWindow cameraWindow;

    /// <summary>
    /// Rect per defecte que te el CameraWindow al començar
    /// </summary>
    private Rect defaultCameraWindowRect;

    /// <summary>
    /// Suavitat per defecte que te la camera al moures verticalment
    /// </summary>
    private float defaultVerticalSmoothness;

    /// <summary>
    /// Suavitat per defecte que te la camera al moures horizontalment
    /// </summary>
    private float defaultHorizontalSmoothness;

    private float defaultYOffset;

    /// <summary>
    /// Acces rapid al CharacterControl del StandardStageManager
    /// </summary>
    private CharacterControl character;

    /// <summary>
    /// Per si volem dibuixar grafics per a debuggar la camera
    /// </summary>
    private bool debugCamera = false;

    private void Start()
    {
        defaultVerticalSmoothness = ProCamera2D.Instance.VerticalFollowSmoothness;
        defaultHorizontalSmoothness = ProCamera2D.Instance.HorizontalFollowSmoothness;
        defaultYOffset = ProCamera2D.Instance.OffsetY;

        cameraWindow = ProCamera2D.Instance.GetComponent<ProCamera2DCameraWindow>();
        defaultCameraWindowRect = cameraWindow.CameraWindowRect;

        character = StandardStageManager.current.playerControl;
    }

    /// <summary>
    /// Redueix suament el smoothness i el rectangle de la window de la càmera
    /// </summary>
    /// <returns></returns>
    public IEnumerator ModifyCameraAtTravel()
    {
        Rect rect = cameraWindow.CameraWindowRect;
        float smoothness = ProCamera2D.Instance.VerticalFollowSmoothness;

        // Per a saber si hem acabat de moure la càmera
        bool end = false;

        // Minim per a considerar que hem acabat de fer petit un valor
        float endThreshold = 0.01f;

        // Dins del travel, movem mes rapid la camera
        ProCamera2D.Instance.VerticalFollowSmoothness = 0.05f;
        ProCamera2D.Instance.HorizontalFollowSmoothness = 0.05f;

        yield return null;
        yield return null;

        // Quina Y volem tenir? -1.2 si pugem, 1.2 si baixem

        // Velocitat mínima per a considerar que pugem o baixem i per tant moure el requadre
        float verticalSpeedTargetRectDivider = 200; // Aquest valor, per a modificar el targetRectY, el trec de que per una velocitat Y de 70 aprox. Per mantenir el 1.2 seria 58
        float targetRectY = -(character.jumpProperties.verticalSpeed / verticalSpeedTargetRectDivider);

        //Debug.Log("Al entrar al travel la nostre dirrecció és: " + Mathf.Sign(character.jumpProperties.verticalSpeed));

        while (!end && character.movementFSM.currentHorizontal == PlayerState.Horizontal.Travel)
        {
            //Debug.Log("Inici height: " + rect.height + " - y: " + rect.y + " - offset: " + ProCamera2D.Instance.OverallOffset.y);
            rect.height = Mathf.SmoothStep(rect.height, 0, Time.deltaTime * speedMultiplier / 2);
            rect.y = Mathf.SmoothStep(rect.y, targetRectY, Time.deltaTime * speedMultiplier);
            cameraWindow.CameraWindowRect = rect;
            ProCamera2D.Instance.OffsetY = Mathf.SmoothStep(ProCamera2D.Instance.OffsetY, 0, Time.deltaTime * speedMultiplier);

            //Debug.Log("End height: " + rect.height + " - y: " + rect.y + " - offset: " + ProCamera2D.Instance.OverallOffset.y);

            end = (Mathf.Abs(rect.y) < endThreshold) && (Mathf.Abs(ProCamera2D.Instance.OffsetY) < endThreshold) && (rect.height < endThreshold);

            yield return true;
        }
    }

    /// <summary>
    /// Amplia suaument el smoothness i el rectancle de la window de la càmera
    /// </summary>
    /// <returns></returns>
    public IEnumerator RecoverCameraAfterTravel()
    {
        Rect rect = cameraWindow.CameraWindowRect;

        // Per a saber si hem acabat de moure la càmera
        bool end = false;

        // Minim per a considerar que hem acabat de fer petit un valor
        float endThreshold = 0.01f;

        // Recuperem la suavitat normal al acabar el travel
        ProCamera2D.Instance.VerticalFollowSmoothness = defaultVerticalSmoothness;
        ProCamera2D.Instance.HorizontalFollowSmoothness = defaultHorizontalSmoothness;

        yield return null;
        yield return null;

        //Debug.Log("Sortim del travel i estem amb l'estat: " + character.movementFSM.currentHorizontal);

        while (!end && character.movementFSM.currentHorizontal != PlayerState.Horizontal.Travel)
        {
            ProCamera2D.Instance.VerticalFollowSmoothness = Mathf.SmoothStep(ProCamera2D.Instance.VerticalFollowSmoothness, defaultVerticalSmoothness, Time.deltaTime * speedMultiplier);
            ProCamera2D.Instance.HorizontalFollowSmoothness = Mathf.SmoothStep(ProCamera2D.Instance.HorizontalFollowSmoothness, defaultHorizontalSmoothness, Time.deltaTime * speedMultiplier);
            //Debug.Log("Al acabar. Inici height: " + rect.height + " - y: " + rect.y + " - offset: " + ProCamera2D.Instance.OverallOffset.y);
            rect.height = Mathf.SmoothStep(rect.height, defaultCameraWindowRect.height, Time.deltaTime * speedMultiplier / 2);
            rect.y = Mathf.SmoothStep(rect.y, defaultCameraWindowRect.y, Time.deltaTime * speedMultiplier);
            cameraWindow.CameraWindowRect = rect;
            ProCamera2D.Instance.OffsetY = Mathf.SmoothStep(ProCamera2D.Instance.OffsetY, defaultYOffset, Time.deltaTime * speedMultiplier);

            //Debug.Log("Al acabar. End height: " + rect.height + " - y: " + rect.y + " - offset: " + ProCamera2D.Instance.OverallOffset.y);

            end = (Mathf.Abs(rect.y) < Mathf.Abs(defaultCameraWindowRect.y) + endThreshold) && (Mathf.Abs(ProCamera2D.Instance.OffsetY) < endThreshold + Mathf.Abs(defaultYOffset));

            yield return true;
        }

        ProCamera2D.Instance.VerticalFollowSmoothness = defaultVerticalSmoothness;
        ProCamera2D.Instance.HorizontalFollowSmoothness = defaultHorizontalSmoothness;

        ProCamera2D.Instance.OffsetY = defaultYOffset;
        cameraWindow.CameraWindowRect = defaultCameraWindowRect;
    }

#if UNITY_EDITOR
    /// <summary>
    /// Dibuixem com es mou la camera, per debug.
    /// Com que nomes debuguem a l'editor, aixo ni ho computem en la build
    /// </summary>
    private void Update()
    {
        if (debugCamera)
        {
            float time = 1f;

            float cameraHeight = 2.0f * Mathf.Abs(ProCamera2D.Instance.transform.position.z) * Mathf.Tan(Camera.main.fieldOfView * 0.5f * Mathf.Deg2Rad);
            float cameraWidth = cameraHeight * Camera.main.aspect;

            float xMin = (cameraWindow.CameraWindowRect.x - cameraWindow.CameraWindowRect.width / 2);
            float yMin = (cameraWindow.CameraWindowRect.y - cameraWindow.CameraWindowRect.height / 2);
            float xMax = (cameraWindow.CameraWindowRect.x + cameraWindow.CameraWindowRect.width / 2);
            float yMax = (cameraWindow.CameraWindowRect.y + cameraWindow.CameraWindowRect.height / 2);

            Vector3 topLeft = ProCamera2D.Instance.transform.position + new Vector3(xMin * cameraWidth, yMax * cameraHeight, -ProCamera2D.Instance.transform.position.z);
            Vector3 topRight = ProCamera2D.Instance.transform.position + new Vector3(xMax * cameraWidth, yMax * cameraHeight, -ProCamera2D.Instance.transform.position.z);
            Vector3 bottomLeft = ProCamera2D.Instance.transform.position + new Vector3(xMin * cameraWidth, yMin * cameraHeight, -ProCamera2D.Instance.transform.position.z);
            Vector3 bottomRight = ProCamera2D.Instance.transform.position + new Vector3(xMax * cameraWidth, yMin * cameraHeight, -ProCamera2D.Instance.transform.position.z);

            Debug.DrawLine(topLeft, bottomLeft, Color.green, time);
            Debug.DrawLine(bottomLeft, bottomRight, Color.green, time);
            Debug.DrawLine(bottomRight, topRight, Color.green, time);
            Debug.DrawLine(topRight, topLeft, Color.green, time);
        }
    }
#endif
}
