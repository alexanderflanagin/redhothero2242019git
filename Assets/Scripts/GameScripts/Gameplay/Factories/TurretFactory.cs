using UnityEngine;
using System.Collections;

/// <summary>
/// Factory de les turrets. Aquest objecte es queda visible
/// </summary>
public class TurretFactory : MonoBehaviour, IWakeUpable
{
    /// <summary>
    /// Transform del fill Direction
    /// </summary>
    protected Transform _direction;

    /// <summary>
    /// Rotaci� que t� la direcci�
    /// </summary>
    //protected Quaternion _directionRot;

    /// <summary>
    /// Transform del fill Cannon
    /// </summary>
    protected Transform _cannon;

    /// <summary>
    /// Rotaci� que t� el can�
    /// </summary>
    protected Quaternion _cannonRot;

    /// <summary>
    /// Posici� que t� el can�
    /// </summary>
    protected Vector3 _cannonPos;

    /// <summary>
    /// Turret actual que t� la factory (si en te)
    /// </summary>
    protected GameObject currentTurret;

    public TurretController currentTurretController;

    public float triggerDistance;

    public Vector2 triggerOffset;

    public Vector2 triggerSize;

    /// <summary>
    /// Si esta habilitada la factory. Per quan genera turrets amb el player dins del trigger
    /// </summary>
    protected bool _enabled;

    /// <summary>
    /// El box collider de les potes de la factory
    /// </summary>
    //protected new BoxCollider2D collider2D;

    /// <summary>
    /// Al habilitar, guardem variables com�ns i eliminem els childs que no necessitem
    /// </summary>
    void OnEnable()
    {
        EnemiesManager.Instance.ListReady += OnListReady;

        _direction = transform.Find("Direction");

        //_directionRot = _direction.rotation;

        _cannon = transform.Find("Direction/Cannon");

        // Posem el parent del cannon com el de la torreta que creem
        _cannon.parent = transform;

        _cannonRot = _cannon.rotation;

        _cannonPos = _cannon.transform.localPosition;

        // Destroy(_direction.gameObject);

        this._direction.gameObject.GetComponent<Renderer>().enabled = false;

        //this.collider2D = this.GetComponent<BoxCollider2D>();
    }

    /// <summary>
    /// Quan la llista d'enemics est� llesta, hi guardem una Turret
    /// </summary>
    protected virtual void OnListReady()
    {
        GameObjectPooler.Instance.AddEnemy(EnemiesManager.Enemies.Turret, 1);
    }

    /// <summary>
    /// Al Start generem la primera Turret
    /// </summary>
    void Start()
    {
        StartCoroutine(GenerateTurret());
    }

    /// <summary>
    /// Cada cop que fem una turret la demanem a l'objecte pooler, l'escalem, la posem a lloc i despr�s l'habilitem
    /// </summary>
    /// <returns></returns>
    protected IEnumerator GenerateTurret()
    {
        Vector3 turretOffset = new Vector3(0, 0.84f);
        
        yield return null;
        this.currentTurret = GetTurret();

        this.currentTurret.transform.position = transform.position + this.transform.TransformDirection(turretOffset);

        this.currentTurret.transform.rotation = this._direction.rotation;
        
        this.currentTurret.transform.Find("Cannon").transform.localPosition = this._cannon.transform.rotation * Quaternion.Inverse(this.currentTurret.transform.rotation) * Vector3.left;

        this.currentTurretController = currentTurret.GetComponent<TurretController>();

        currentTurret.GetComponent<InstantExplosion>().desviationPosition = currentTurret.transform.Find("Mesh/Armature").position - currentTurret.transform.position;
        
        this.currentTurretController.factory = this;

        // Afegim Events que amaguen el collider si no tenim turret
        //currentTurretController.events.registerEvent(EnemyEvents.EventName.PowerGloved, OnTurretPowerGloved);
        //currentTurretController.events.registerEvent(EnemyEvents.EventName.TouchedByProjectile, OnTurretTouchedByProjectile);

        currentTurret.SetActive(true);

        //this.collider2D.enabled = false;

        BoxCollider2D turretCollider = currentTurret.GetComponent<BoxCollider2D>();

        for (float i = 0.5f; i < 1.1f; i += 0.1f)
        {
            currentTurret.transform.localScale = new Vector3(i, i, i);
            
            // Tenim al player molt aprop? El mourem a sobre la turret
            if(Vector3.Distance(currentTurret.transform.position, StandardStageManager.current.player.transform.position) < i * 2)
            {
                if(Physics2D.OverlapArea(turretCollider.bounds.min, turretCollider.bounds.max, Layers.addLayer(0, Layers.Player)))
                {
                    StandardStageManager.current.player.transform.position = new Vector3(StandardStageManager.current.player.transform.position.x,
                        currentTurret.transform.position.y + turretCollider.offset.y + turretCollider.size.y / 2 * i);
                }
            }

            yield return null;
            yield return null;
        }
        
        currentTurret.transform.localScale = Vector3.one;

        //this.currentTurretController.cannon.prepared = true;

        if (_enabled)
        {
            yield return new WaitForSeconds(0.5f);
            this.currentTurretController.shooter.StartShotting();
        }

        StartCoroutine(ReloadTurret());
    }

    /// <summary>
    /// Instancia el prefab del turret. Per a poder fer overrides amb turrets amb rotació, blue substanced, etc.
    /// </summary>
    /// <returns></returns>
    protected virtual GameObject GetTurret()
    {
        return GameObjectPooler.Instance.GetEnemy(EnemiesManager.Enemies.Turret);
    }

    /// <summary>
    /// Mentre tinguem turret activa no fem res. Quan ja no estigui activa, esperem 2segons i en creem una de nova
    /// </summary>
    /// <returns></returns>
    protected IEnumerator ReloadTurret()
    {
        //Debug.Log("Start ReloadTurret");
        while (currentTurret.activeInHierarchy)
        {
            yield return null;
        }
        //Debug.Log("Game object inactive");
        yield return new WaitForSeconds(2f);
        //Debug.Log("Lets generate another head");
        StartCoroutine(GenerateTurret());
    }

    /// <summary>
    /// Deshabilitem el collider per a que el cap no xoqui amb la mateixa factory
    /// </summary>
    /// <param name="sender"></param>
    //void OnTurretPowerGloved(GameObject sender)
    //{
    //    this.GetComponent<Collider2D>().enabled = false;
    //    this.currentTurretController.events.unregisterEvent(EnemyEvents.EventName.PowerGloved, OnTurretPowerGloved);
    //}

    /// <summary>
    /// Deshabilitem el collider per a poder-hi passar a traves
    /// </summary>
    /// <param name="sender"></param>
    //void OnTurretTouchedByProjectile(GameObject sender)
    //{
    //    this.GetComponent<Collider2D>().enabled = false;
    //    this.currentTurretController.events.unregisterEvent(EnemyEvents.EventName.TouchedByProjectile, OnTurretTouchedByProjectile);
    //}

    public void WakeUp()
    {
        _enabled = true;
        if (this.currentTurretController)
        {
            // currentTurret.GetComponent<Cannon>().prepared = true;
            // currentTurret.GetComponent<TurretController>().events.triggerEvent(EnemyEvents.EventName.EnableCannon);
            this.currentTurretController.shooter.StartShotting();
        }
    }

    public void Sleep()
    {
        _enabled = false;
        if (this.currentTurretController)
        {
            // currentTurret.GetComponent<TurretController>().events.triggerEvent(EnemyEvents.EventName.DisableCannon);
            this.currentTurretController.shooter.EndShotting();
        }
    }

    /// <summary>
    /// Posa la distancia del trigger. Herencia del IWakeUp
    /// </summary>
    /// <param name="distance">La distància del trigger</param>
    public void SetTriggerDistance(float distance)
    {
        triggerDistance = distance;
    }

    /// <summary>
    /// Retorna la distancia del triger. Herencia del IWakeUp
    /// </summary>
    /// <returns>La distància del trigger</returns>
    public float GetTriggerDistance()
    {
        return triggerDistance;
    }


    public void SetTriggerOffset(Vector2 offset)
    {
        triggerOffset = offset;
    }

    public void SetTriggerSize(Vector2 size)
    {
        triggerSize = size;
    }

    public Vector2 GetTriggerOffset()
    {
        return triggerOffset;
    }

    public Vector2 GetTriggerSize()
    {
        return triggerSize;
    }
}
