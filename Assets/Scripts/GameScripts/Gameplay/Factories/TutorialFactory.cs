using UnityEngine;
using System.Collections;

/// <summary>
/// Classe her�ncia de les factories per a poder posar el tipus de tutorial de l'enemic
/// </summary>
public class TutorialFactory : Factory
{
    /// <summary>
    /// El tipus de tutorial que tindr� l'enemic
    /// </summary>
    public TutorialStvController.TutorialType tutorialType;

    /// <summary>
    /// Override del instantiate enemy que ens permet afegir-hi el tipus de tutorial que volem que sigui
    /// </summary>
    /// <returns></returns>
    protected override GameObject InstantiateEnemy()
    {
        GameObject prefab = base.InstantiateEnemy();

        if (prefab.GetComponent<TutorialStvController>())
        {
            prefab.GetComponent<TutorialStvController>().SetTutorialType(tutorialType);
        }

        return prefab;
    }
}
