using UnityEngine;
using System.Collections;

public class StvReactorFactory : SingleEnemyFactory
{
    /// <summary>
    /// Icona del stv reactor per a les factories
    /// </summary>
    protected override string iconName
    {
        get
        {
            return "StvReactorLeft.png";
        }
    }

    /// <summary>
    /// El tipus d'enemic a instanciar es el StvReactor
    /// </summary>
    protected override EnemiesManager.Enemies enemy
    {
        get
        {
            return EnemiesManager.Enemies.S_TV_Reactor;
        }
    }

    /// <summary>
    /// Es ocult
    /// </summary>
    protected override bool hidden
    {
        get
        {
            return false;
        }
    }

    /// <summary>
    /// El respawnTime es de 2 segons
    /// </summary>
    protected override float respawnWaitTime
    {
        get
        {
            return 2f;
        }
    }
}
