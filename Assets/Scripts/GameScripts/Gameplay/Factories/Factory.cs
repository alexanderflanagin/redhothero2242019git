using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System;
using System.Collections;
using System.Collections.Generic;

public class Factory : MonoBehaviour, IWakeUpable
{
    public enum MoveFloor { floor = 0, wallRight = 1, roof = 2, wallLeft = 3 }

    /// <summary>
    /// Enemic que ha de generar
    /// </summary>
    public EnemiesManager.Enemies enemy = EnemiesManager.Enemies.S_Tv;

    /// <summary>
    /// Indica si els enemics han de sortir cap a l'esquerra o no The direction of the child
    /// </summary>
    public bool reverseDirection;

    /// <summary>
    /// Per saber per quin terra hem de caminar (de moment per els Splurghs)
    /// </summary>
    public MoveFloor moveFloor = MoveFloor.floor;

    /// <summary>
    /// Numero d'enemics a generar
    /// 0 : infinite
    /// </summary>
    public int maxEnemies = 0;

    /// <summary>
    /// Freqüencia (en segons) a la que surten els enemics
    /// </summary>
    public float shootingFrequency = 1.0f;

	public float delay = 0.0f;

    /// <summary>
    /// Summary si la factory genera enemics mentre està en pantalla
    /// </summary>
	public bool hidden = true;

    /// <summary>
    /// Distància que té amb el seu trigger
    /// </summary>
    public float triggerDistance;

    /// <summary>
    /// Si ha de ser llençat automàticament o no
    /// </summary>
    public bool autoTriggedOn;

    /// <summary>
    /// Si la factory s'ha d'apagar quan es surt del seu collider. En principi sempre s'apagarà, però pot ser que no es vulgui per alguna raó
    /// </summary>
    public bool triggerOffOnExit = true;

	/// <summary>
    /// Regenera enemics?
	/// </summary>
	public bool regenerateEnemies = true;

    /// <summary>
    /// Indica si els enemics que llença estan magnetitzats o no
    /// </summary>
    public bool punchMagnet = false;

    /// <summary>
    /// Com es deshabiliten els enemics d'aquesta factory
    /// </summary>
    public EnemiesManager.EnemyDisable survival = EnemiesManager.EnemyDisable.TimeDisable;

    /// <summary>
    /// Animator de la porta, per a obrir-la i tancar-la
    /// </summary>
    public Animator door;

    /// <summary>
    /// Segons que triga la porta a obrir-se respecte a la creacio de l'enemic. Pot ser negatiu si es abans
    /// </summary>
    public float timeBeforeDoorOpen;

    /// <summary>
    /// Segons que esta la porta oberta
    /// </summary>
    public float timeDoorOppened;

    /// <summary>
    /// Hash del animator per a Open
    /// </summary>
    private int openDoorHash = Animator.StringToHash("Open");

    /// <summary>
    /// Hash del animator per a Close
    /// </summary>
    private int closeDoorHash = Animator.StringToHash("Close");

    /// <summary>
    /// Si ja ha estat disparat
    /// </summary>
	protected bool _triggedOn;

    /// <summary>
    /// Getter per a la variable _triggedOn
    /// </summary>
	public bool triggedOn { get { return _triggedOn; } set { _triggedOn = value; } }

    /// <summary>
    /// Registre dels fills que té la factory
    /// </summary>
    protected List<GameObject> _childs;
	
    /// <summary>
    /// Triggers de la factory
    /// </summary>
    protected Transform[] triggers;

    [HideInInspector]
    public Vector2 triggerOffset;

    [HideInInspector]
    public Vector2 triggerSize;

    /// <summary>
    /// Collider si la factory es un element fisic de l'escenari
    /// </summary>
    public BoxCollider2D boxFactoryCollider;

    /// <summary>
    /// Collider del fill que te dins la factory (si es que en te)
    /// </summary>
    private Physics2DController insideChildPhysicsController;

    private int _enemiesCreated = 0;

    /// <summary>
    /// Renderer to test if we are on screen
    /// </summary>
    private new Renderer renderer;

    /// <summary>
    /// If it has no door, force it to be hidden
    /// </summary>
    private void Awake()
    {
        if(!door)
        {
            DebugUtils.AssertError(hidden, "Factory not hidden but without door. Force it to be hidden", this.gameObject);
            hidden = true;
        }
    }

    protected void OnEnable()
    {
        // Registrem un esdeveniment al EnemiesManager per a que ens avisi quan puguem cridar a la creació de llistes
        EnemiesManager.Instance.ListReady += OnListReady;
    }

    void OnListReady()
    {
        int toAdd = maxEnemies;
        
        if (maxEnemies == 0)
        {
            toAdd = GameObjectPooler.DEFAULT_ENEMIES - (int) (shootingFrequency * 2) + 1;
        }

        GameObjectPooler.Instance.AddEnemy(enemy, toAdd);
    }

	// Use this for initialization
    protected void Start()
    {
        _childs = new List<GameObject>();

		_triggedOn = autoTriggedOn;

        renderer = GetComponent<MeshRenderer>();

        StartCoroutine(Waiting());
    }

    /// <summary>
    /// Mirem si tenia un child dins, i si ja no es child actualitzem la layerMask d'aquest i el "perdem"
    /// </summary>
    private void Update()
    {
        if(insideChildPhysicsController != null)
        {
            if (!insideChildPhysicsController.rayInfo.collider.bounds.Intersects(boxFactoryCollider.bounds) || !insideChildPhysicsController.gameObject.activeInHierarchy)
            {
                insideChildPhysicsController.UpdateLayerMask(Layers.addLayer(insideChildPhysicsController.collisionMask, Layers.EnemyFactory));
                insideChildPhysicsController = null;
            }
        }
    }

    /// <summary>
    /// Desperta la plataforma. Herencia del IWakeUp
    /// </summary>
    public void WakeUp() {
		_triggedOn = true;
	}
	
	/// <summary>
    /// Posa la distancia del trigger. Herencia del IWakeUp
	/// </summary>
	/// <param name="distance">La distància del trigger</param>
	public void SetTriggerDistance( float distance ) {
		triggerDistance = distance;
	}
	
	/// <summary>
    /// Retorna la distancia del triger. Herencia del IWakeUp
	/// </summary>
	/// <returns>La distància del trigger</returns>
	public float GetTriggerDistance() {
		return triggerDistance;
	}

    protected IEnumerator Waiting()
    {
        bool shoot = false;
        while (!shoot)
        {
            shoot = _triggedOn && (!hidden || !OnScreen()) && (maxEnemies == 0 || _childs.Count < maxEnemies);
            yield return null;
        }

        StartCoroutine(Shooting());
    }

    protected IEnumerator Shooting() {
		bool factoryEnd = false;

		if(!Mathf.Approximately(this.delay, 0))
		{
			yield return new WaitForSeconds(this.delay);
		}

        if (shootingFrequency <= 0)
        {
            // Vigilem que la freqüencia de dispar no sigui massa baixa, perquè s'executa el loop dos vegades abans de tancar-lo
            shootingFrequency = 0.5f;
        }

        while (_triggedOn && (!hidden || !OnScreen()) && (maxEnemies == 0 || _childs.Count < maxEnemies))
        {
			// Sumem enemics generats
			// Instantiate Prefab
            //Debug.Log("Gonna create a new enemy!", this.gameObject);
            yield return StartCoroutine(Shoot());

			// Parem l'objecte?
            if (!regenerateEnemies && _childs.Count >= maxEnemies && maxEnemies != 0)
            {
				factoryEnd = true;
				break;
			}

            yield return new WaitForSeconds(shootingFrequency);
		}

        if (!factoryEnd)
        {
            StartCoroutine("Waiting");
        }
    }

    public IEnumerator Shoot()
    {
        // Si te el darrer enemic generat encara dins, no en generem més
        while(insideChildPhysicsController != null)
        {
            yield return null;
        }

        if(door != null)
        {
            AudioManager.Instance.PlayOneShoot(AudioClipsData.ClipsGroups.Enemies, "Ding");
            // Si el timeBeforeDoorOpen es negatiu, vol dir que hem d'obrir a l'instant i esperar-nos per a llencar l'enemic
            if (timeBeforeDoorOpen <= 0)
            {
                StartCoroutine(openCloseDoor());
                yield return new WaitForSeconds(-timeBeforeDoorOpen);
            }
        }

        GameObject prefab = InstantiateEnemy();
        EnemyController controller = prefab.GetComponent<EnemyController>();

        if(boxFactoryCollider != null)
        {
            insideChildPhysicsController = prefab.GetComponent<Physics2DController>();
            insideChildPhysicsController.UpdateLayerMask(Layers.removeLayer(insideChildPhysicsController.collisionMask, Layers.EnemyFactory));
        }

        // Per a que pugui entrar l'Start
        yield return null;
        yield return null;
        controller.Move();

        _childs.Add(prefab);
        _enemiesCreated++;

        // Need to rotate this?
        if((!this.reverseDirection && controller.direction == 1) || (this.reverseDirection && controller.direction == -1))
        {
            controller.events.triggerEvent(EnemyEvents.EventName.Rotate);
        }

        if (door != null)
        {
            // Si el timeBeforeDoorOpen es positiu, vol dir que hem d'obrir la porta despres d'haver llencat l'enemic
            if (timeBeforeDoorOpen > 0)
            {
                yield return new WaitForSeconds(timeBeforeDoorOpen);
                StartCoroutine(openCloseDoor());
            }
        }
    }

    private IEnumerator openCloseDoor()
    {
        door.SetTrigger(openDoorHash);
        yield return new WaitForSeconds(timeDoorOppened);
        door.SetTrigger(closeDoorHash);
    }


    protected virtual GameObject InstantiateEnemy()
    {
        GameObject prefab = GameObjectPooler.Instance.GetEnemy(enemy);
        prefab.transform.position = transform.position;

        if (prefab.GetComponent<EnemyDisable>())
        {
            Destroy(prefab.GetComponent<EnemyDisable>());
        }

        if (survival == EnemiesManager.EnemyDisable.TimeDisable)
        {
            prefab.AddComponent<TimeDisable>();
        }
        else if (survival == EnemiesManager.EnemyDisable.TriggerDisable)
        {
            prefab.AddComponent<TriggerDisable>().parentFactory = this.gameObject;
        }

        // Need to change the floor? Ho fem sempre perquè pot venir canviat d'una altra factory
        if (prefab.GetComponent<EnemyMove>())
        {
            prefab.GetComponent<EnemyMove>().moveFloor = moveFloor;
        }

        prefab.SetActive(true);

        EnemyController enemyController = prefab.GetComponent<EnemyController>();
        enemyController.events.registerEvent(EnemyEvents.EventName.Die, OnEnemyDieOrDisable);
        enemyController.events.registerEvent(EnemyEvents.EventName.Disable, OnEnemyDieOrDisable);

        return prefab;
	}

    void OnEnemyDieOrDisable(GameObject enemy)
    {
        if (_childs.Contains(enemy))
        {
            _childs.Remove(enemy);
        }
    }

	/// <summary>
    /// Mira si l'objecte esta en pantalla segons la seva posicio i la de la camera principal
	/// </summary>
	/// <returns></returns>
	protected bool OnScreen()
    {
        return renderer.isVisible;
    }

    public void Sleep()
    {
        _triggedOn = !triggerOffOnExit;
    }


    public void SetTriggerOffset(Vector2 offset)
    {
        triggerOffset = offset;
    }

    public void SetTriggerSize(Vector2 size)
    {
        triggerSize = size;
    }

    public Vector2 GetTriggerOffset()
    {
        return triggerOffset;
    }

    public Vector2 GetTriggerSize()
    {
        return triggerSize;
    }


#if UNITY_EDITOR
    /// <summary>
    /// Metode per a mostrar info de la Factory
    /// </summary>
    void OnDrawGizmos()
    {
        GUIStyle guiStyle = new GUIStyle();
        guiStyle.fontStyle = FontStyle.Bold;
        guiStyle.normal.textColor = Color.black;
        guiStyle.padding = new RectOffset(8, 8, 0, 0);
        guiStyle.alignment = TextAnchor.MiddleLeft;

        int nChilds = 0;
        if (_childs != null)
        {
            nChilds = _childs.Count;
        }

        string text = "==== " + enemy + " ====\nChilds: " + nChilds + "\nEnemies created: " + _enemiesCreated;
        text += "\nRegenerate: " + regenerateEnemies + " - Hidden: " + hidden + "\nShut Down Trigger On Exit: " + triggerOffOnExit + " \n";
        text += "Survival: " + survival + "\nMax Enemies: " + maxEnemies;

        Handles.Label(transform.position + Vector3.up, text, guiStyle);
    }
#endif
}
