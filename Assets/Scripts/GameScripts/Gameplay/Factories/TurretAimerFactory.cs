using UnityEngine;
using System.Collections;

/// <summary>
/// Factory de les turrets aimers. Aquest objecte es pot quedar visible
/// </summary>
public class TurretAimerFactory : TurretFactory
{
    /// <summary>
    /// Quan la llista d'enemics esta llesta, hi guardem una Turret de tipus aimer
    /// </summary>
    protected override void OnListReady()
    {
        GameObjectPooler.Instance.AddEnemy(EnemiesManager.Enemies.TurretAimer, 1);
    }

    /// <summary>
    /// Quan instanciem, retorna una turret aimer
    /// </summary>
    /// <returns></returns>
    protected override GameObject GetTurret()
    {
        return GameObjectPooler.Instance.GetEnemy(EnemiesManager.Enemies.TurretAimer);
    }
}
