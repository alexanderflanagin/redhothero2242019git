using UnityEngine;
using System.Collections;

public abstract class SingleEnemyFactory : MonoBehaviour
{
    /// <summary>
    /// Nom de l'icona que mostrem amb els gizmos
    /// </summary>
    protected abstract string iconName { get; }

    /// <summary>
    /// Tipus d'enemic que instanciem
    /// </summary>
    protected abstract EnemiesManager.Enemies enemy { get; }

    /// <summary>
    /// Si la regeneracio es oculta (en principi sempre ho sera)
    /// </summary>
    protected abstract bool hidden { get; }

    /// <summary>
    /// El temps que esperem despres d'haver mort abans de fer respawn
    /// </summary>
    protected abstract float respawnWaitTime { get; }

    /// <summary>
    /// El nostre meshRenderer. Per a mirar si es visible o no de manera rapida
    /// </summary>
    public MeshRenderer meshRenderer;

    public GameObject spawnParticles;

    public float spawnParticlesTime = 0.5f;

    public string spawnParticlesAudioClipName = "Spawn";

    /// <summary>
    /// Controller de l'enemic que instanciem
    /// </summary>
    [HideInInspector]
    public EnemyController enemyController;

    protected bool factoryMovingEnemy = true;

    protected virtual void Awake()
    {
        if(spawnParticles)
        {
            spawnParticles.SetActive(false);
        }
    }

    /// <summary>
    /// Al habilitar la factory, preparem el OnListReady per a ser cridat (o el cridem directament)
    /// </summary>
    protected virtual void OnEnable()
    {
        if(!EnemiesManager.Instance.listReadyAlreadyCalled)
        {
            EnemiesManager.Instance.ListReady += OnListReady;
        }
        else
        {
            OnListReady();
        }
    }

    private void OnDisable()
    {
        if(enemyController)
        {
            enemyController.events.unregisterEvent(EnemyEvents.EventName.Die, OnEnemyDie);
        }
    }

    /// <summary>
    /// Si te enemic instanciat, actualitzem la posicio per a que ens segueixi
    /// </summary>
    private void Update()
    {
        if(factoryMovingEnemy && enemyController != null)
        {
            SetInstantiatedPosition(enemyController.gameObject);
        }
    }

    /// <summary>
    /// Al tenir la llista llesta, afegim l'enemic del tipus necessari
    /// </summary>
    protected virtual void OnListReady()
    {
        GameObjectPooler.Instance.AddEnemy(this.enemy, 1);

        StartCoroutine(Respawn(true));
    }

    /// <summary>
    /// Mostra un icona del tipus de factoria
    /// </summary>
    protected virtual void OnDrawGizmos()
    {
        if(!string.IsNullOrEmpty(iconName))
        {
            Gizmos.DrawIcon(transform.position, iconName);
        }
    }

    /// <summary>
    /// Instancia l'enemic del tipus que te definit
    /// </summary>
    protected virtual void InstantiateEnemy()
    {
        GameObject instantiatedEnemy = GameObjectPooler.Instance.GetEnemy(this.enemy);
        //instantiatedEnemy.transform.SetParent(this.transform);

        this.enemyController = instantiatedEnemy.GetComponent<EnemyController>();

        if(this.enemyController.direction == 1)
        {
            this.enemyController.OnRotate(this.gameObject);
        }

        enemyController.events.registerEvent(EnemyEvents.EventName.Die, OnEnemyDie);
        enemyController.events.registerEvent(EnemyEvents.EventName.PowerGloved, OnEnemyPowerGloved);

        SetInstantiatedPosition(instantiatedEnemy);

        // Mirem si estava en un path element, i si es aixi es mou si el path element s'esta movent
        PathElement pathElement = GetComponent<PathElement>();
        if (pathElement && pathElement.path && pathElement.path.isMoving)
        {
            pathElement.WakeUp();
        }

        instantiatedEnemy.SetActive(true);
        factoryMovingEnemy = true;
    }

    /// <summary>
    /// Posa l'enemic a la posici� inicial que ha de tenir
    /// </summary>
    /// <param name="instantiated">El game object que s'ha instanciat</param>
    protected virtual void SetInstantiatedPosition(GameObject instantiated)
    {
        instantiated.transform.position = transform.position;
    }

    /// <summary>
    /// Al morir l'enemic que teniem creat, el desvinculem de la factory i cridem el respawn
    /// </summary>
    /// <param name="enemy"></param>
    protected virtual void OnEnemyDie(GameObject enemy)
    {
        enemyController.events.unregisterEvent(EnemyEvents.EventName.Die, OnEnemyDie);

        enemyController = null;

        factoryMovingEnemy = false;
        
        StartCoroutine(Respawn(false));
    }

    protected virtual void OnEnemyPowerGloved(GameObject enemy)
    {
        enemyController.events.unregisterEvent(EnemyEvents.EventName.PowerGloved, OnEnemyPowerGloved);

        factoryMovingEnemy = false;
    }

    /// <summary>
    /// Esperem el temps que faci falta per a fer respawn segons el /noHiddenWaitTime/ i fem respawn tenint en compte si es /hidden/ o no
    /// </summary>
    /// <returns></returns>
    protected IEnumerator Respawn(bool first)
    {
        if(!first)
        {
            yield return new WaitForSeconds(this.respawnWaitTime);
        }

        if(spawnParticles != null)
        {
            // Si havien quedat activades, les desactivem
            spawnParticles.SetActive(false);
        }

        while (hidden && meshRenderer.isVisible)
        {
            yield return null;
        }

        if (spawnParticles != null)
        {
            spawnParticles.SetActive(true);
            AudioManager.Instance.PlayOneShoot(AudioClipsData.ClipsGroups.Enemies, spawnParticlesAudioClipName);
            yield return new WaitForSeconds(spawnParticlesTime);
        }

        InstantiateEnemy();
    }
}
