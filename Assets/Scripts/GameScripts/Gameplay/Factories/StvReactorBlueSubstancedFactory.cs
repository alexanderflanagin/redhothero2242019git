using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StvReactorBlueSubstancedFactory : StvReactorFactory
{
    /// <summary>
    /// Icona del stv reactor per a les factories
    /// </summary>
    protected override string iconName
    {
        get
        {
            return "StvReactorBlueSubstancedLeft.png";
        }
    }

    /// <summary>
    /// El tipus d'enemic a instanciar es el StvReactor
    /// </summary>
    protected override EnemiesManager.Enemies enemy
    {
        get
        {
            return EnemiesManager.Enemies.S_Tv_Reactor_BlueSubstanced;
        }
    }
}
