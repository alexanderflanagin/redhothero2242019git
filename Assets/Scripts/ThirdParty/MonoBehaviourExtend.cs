using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

static public class MethodExtensionForMonoBehaviourTransform {
	/// <summary>
	/// Gets or add a component. Usage example:
	/// BoxCollider boxCollider = transform.GetOrAddComponent<BoxCollider>();
	/// </summary>
	public static T GetOrAddComponent<T> (this Component child) where T: Component {
		T result = child.GetComponent<T>();
		if (result == null) {
			result = child.gameObject.AddComponent<T>();
		}
		return result;
	}

	public static T GetInterface<T>(this Component inObj) where T : class
	{
		if (!typeof(T).IsInterface) {
			Debug.LogError(typeof(T).ToString() + ": is not an actual interface!");
			return null;
		}
		
		return inObj.GetComponents<Component>().OfType<T>().FirstOrDefault();
	}
	
	public static IEnumerable<T> GetInterfaces<T>(this Component inObj) where T : class
	{
		if (!typeof(T).IsInterface) {
			Debug.LogError(typeof(T).ToString() + ": is not an actual interface!");
			return Enumerable.Empty<T>();
		}
		
		return inObj.GetComponents<Component>().OfType<T>();
	}

	#region Collider2D shortcuts
	public static float MaxX ( this Collider2D collider ) {
		return collider.offset.x + collider.bounds.extents.x;
	}

    public static float MinX(this Collider2D collider)
    {
        return collider.offset.x - collider.bounds.extents.x;
	}

    public static float MaxY(this Collider2D collider)
    {
        return collider.offset.y + collider.bounds.extents.y;
	}

    public static float MinY(this Collider2D collider)
    {
        return collider.offset.y - collider.bounds.extents.y;
	}

    public static Vector2 BottomLeft(this Collider2D collider)
    {
        return collider.offset + new Vector2(-collider.bounds.extents.x, -collider.bounds.extents.y);
	}

    public static Vector2 BottomRight(this Collider2D collider)
    {
        return collider.offset + new Vector2(collider.bounds.extents.x, -collider.bounds.extents.y);
	}

    public static Vector2 TopLeft(this Collider2D collider)
    {
        return collider.offset + new Vector2(-collider.bounds.extents.x, collider.bounds.extents.y);
	}

    public static Vector2 TopRight(this Collider2D collider)
    {
        return collider.offset + new Vector2(collider.bounds.extents.x, collider.bounds.extents.y);
	}

	#endregion
}
