using UnityEngine;
using System.Collections;
using StopWatch = System.Diagnostics;

public class TestPerformance : MonoBehaviour {

	public GameObject test;

	// Use this for initialization
	void Start () {
		bool a = false;

		StopWatch.Stopwatch sw = new StopWatch.Stopwatch();
		sw.Start();
        for (int i = 0; i < 1000000; i++)
        {
            Physics2D.queriesStartInColliders = true;
            RaycastHit2D ray = Physics2D.Raycast(Vector3.zero, Vector2.left, 1);
            Physics2D.queriesStartInColliders = false;
        }
        sw.Stop();
		UnityEngine.Debug.Log ( "Changing physics2d queries start in colliders: " + sw.Elapsed );

		sw.Reset();
        sw.Start();
        for (int i = 0; i < 1000000; i++)
        {
            RaycastHit2D ray = Physics2D.Raycast(Vector3.zero, Vector2.left, 1);
            ray = Physics2D.Raycast(Vector3.zero, Vector2.left, 1);
            ray = Physics2D.Raycast(Vector3.zero, Vector2.left, 1);
        }
        sw.Stop();
        UnityEngine.Debug.Log("Throwing three rays: " + sw.Elapsed);

        //sw.Reset();
        //sw.Start();
        //for( int i = 0; i < 1000000; i++ ) {
        //	a = test.tag == "BeachFloor";
        //}
        //sw.Stop();
        //UnityEngine.Debug.Log ( "tag directe comparat amb string: " + sw.Elapsed );

        //sw.Reset();
        //sw.Start();
        //for( int i = 0; i < 1000000; i++ ) {
        //	a = test.layer == Layers.Background;
        //}
        //sw.Stop();
        //UnityEngine.Debug.Log ( "Layer amb manager: " + sw.Elapsed );

        //sw.Reset();
        //sw.Start();
        //for( int i = 0; i < 1000000; i++ ) {
        //	a = test.layer == 9;
        //}
        //sw.Stop();
        //UnityEngine.Debug.Log ( "layer amb enter directe: " + sw.Elapsed );

        //sw.Reset();
        //sw.Start();
        //for( int i = 0; i < 1000000; i++ ) {
        //	a = test.layer == LayerMask.NameToLayer("Background");
        //}
        //sw.Stop();
        //UnityEngine.Debug.Log ( "Layer amb layermask.nametolayer: " + sw.Elapsed + " , a " + a );
    }
}
