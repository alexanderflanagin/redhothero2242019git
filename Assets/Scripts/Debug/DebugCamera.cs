using UnityEngine;
using System.Collections;

public class DebugCamera : MonoBehaviour {

	public GUIElement mainCharInfo;
	public GUIElement mainCameraInfo;

	private CharacterControl mainCharControl;
	// private CameraMovement mainCamera;

	// Use this for initialization
	void Start () {
		GameObject mainChar = GameObject.FindWithTag("Player");
		mainCharControl = mainChar.GetComponent<CharacterControl>();

		transform.GetComponent<Camera>().enabled = false;

		// mainCamera = Camera.main.GetComponent<CameraMovement>();
	}
	
	// Update is called once per frame
	void Update () {
		mainCharInfo.GetComponent<GUIText>().text = "Speed: " + mainCharControl.movementProps.speed.ToString("0.00") + " - Vertical Speed: " + mainCharControl.jumpProperties.verticalSpeed.ToString("0.00");
        mainCharInfo.GetComponent<GUIText>().text += "\nHorizontal: " + mainCharControl.movementFSM.currentHorizontal + " - Vertical: " + mainCharControl.movementFSM.currentVertical;
		mainCharInfo.GetComponent<GUIText>().text += "\nRun Direction: " + mainCharControl.movementProps.runDirection;
		// mainCharInfo.guiText.text += "\nLooking right: " + mainCharControl.lookingRight;
		if( mainCharControl.input != null ) {
			mainCharInfo.GetComponent<GUIText>().text += "\nxAxis: " + mainCharControl.input.xAxis + " - yAxis: " + mainCharControl.input.yAxis;
			mainCharInfo.GetComponent<GUIText>().text += "\nJump: " + mainCharControl.input.jump + " - Dash: " + mainCharControl.input.roll;
		}
        //mainCharInfo.guiText.text += "\nGrounded: <b>" + mainCharControl.controller.grounded + "</b> - Blocked up: <b>" + mainCharControl.controller.blockedUp + "</b> - Blocked forward: <b>" + mainCharControl.controller.blockedFront + "</b>";


		// mainCameraInfo.guiText.text = "Camera Speed: " + mainCamera.Speed.ToString("0.00");

		if( Input.GetKeyDown( KeyCode.P ) ) {
			transform.GetComponent<Camera>().enabled = !transform.GetComponent<Camera>().enabled;
		}
	}
}
