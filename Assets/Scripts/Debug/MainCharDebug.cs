using UnityEngine;
using System.Collections;

public class MainCharDebug : MonoBehaviour {

	private CharacterControl mainCharControl;

	private CharacterControl.StateAlive stateAlive;
	private CharacterControl.StateAlive lastAlive;

	private PlayerState.Horizontal stateHorizontal;
    private PlayerState.Horizontal lastHorizontal;

    private PlayerState.Vertical stateVertical;
    private PlayerState.Vertical lastVertical;

#if JUMP_DEBUG || SPEED_DEBUG
	private float lastSpeed = 99;
	private float lastTime = 0;
	private float lastHeight = 0;
	private bool resetLastHeight = false;
	private bool resetLastMinHeight = true;
#endif

	// Use this for initialization
	void Start () {
		mainCharControl = (CharacterControl) FindObjectOfType (typeof (CharacterControl));

		/* lastAlive = stateAlive = mainCharControl.currentAlive;

		lastHorizontal = stateHorizontal = mainCharControl.currentHorizontal;

		lastVertical = stateVertical = mainCharControl.currentVertical; */
	}
	
	// Update is called once per frame
	void Update () {
		/* stateAlive = mainCharControl.currentAlive;
		stateHorizontal = mainCharControl.currentHorizontal;
		stateVertical = mainCharControl.currentVertical;

		if( stateAlive != lastAlive ) {

		}

		if( stateHorizontal != lastHorizontal ) {
			
		}

		if( stateVertical != lastVertical ) {
			
		}

		lastAlive = stateAlive;
		lastHorizontal = stateHorizontal;
		lastVertical = stateVertical; */

#if JUMP_DEBUG
		if( mainCharControl.transform.position.y < lastHeight && !resetLastHeight ) {
			Debug.Log ( "[MainCharDebug::Update] Last height: " + lastHeight + " - Time: " + lastTime );
			resetLastHeight = true;
			resetLastMinHeight = true;
		} else if ( mainCharControl.transform.position.y >= lastHeight && resetLastMinHeight ) {
			Debug.Log ( "[MainCharDebug::Update] Last height: " + lastHeight + " - Time: " + lastTime );
			resetLastMinHeight = false;
		} else if( mainCharControl.transform.position.y >= lastHeight ) {
			lastHeight = mainCharControl.transform.position.y;
			resetLastHeight = false;
		}
#endif

#if SPEED_DEBUG
		// Speed
		if( mainCharControl.movementProps.speed != lastSpeed && lastSpeed == 0 ) {
			Debug.Log ( "[MainCharDebug::Update] Last speed: " + lastSpeed + " - Time: " + lastTime + " - X Pos: " + mainCharControl.transform.position.x );
		}
		if( mainCharControl.movementProps.speed != lastSpeed && ( lastSpeed > 7.9 && lastSpeed < 8.1 ) ) {
			Debug.Log ( "[MainCharDebug::Update] Last speed: " + lastSpeed + " - Time: " + lastTime + " - X Pos: " + mainCharControl.transform.position.x );
		}
		if( mainCharControl.movementProps.speed != lastSpeed && lastSpeed == 12 ) {
			Debug.Log ( "[MainCharDebug::Update] Last speed: " + lastSpeed + " - Time: " + lastTime + " - X Pos: " + mainCharControl.transform.position.x );
		}
		if(
			( mainCharControl.movementProps.speed == 0 || mainCharControl.movementProps.speed == 8 || mainCharControl.movementProps.speed == 12 )
			&& mainCharControl.movementProps.speed != lastSpeed ) {
			Debug.Log ( "[MainCharDebug::Update] Speed: " + mainCharControl.movementProps.speed + " - Time: " + Time.time + " - X Pos: " + mainCharControl.transform.position.x );
		}
#endif

#if SPEED_DEBUG || JUMP_DEBUG
		lastSpeed = mainCharControl.movementProps.speed;
		lastHeight = mainCharControl.transform.position.y;
		lastTime = Time.time;
#endif
	}
}
