﻿using UnityEngine;
using System.Collections;

public class EventsDummy : MonoBehaviour {

    private CharacterControl _control;

    void OnEnable()
    {
        _control = GetComponent<CharacterControl>();

        _control.events.registerEvent(CharacterEvents.EventName.IdleEnter, OnIdleEnter);
        _control.events.registerEvent(CharacterEvents.EventName.IdleExit, OnIdleExit);
        _control.events.registerEvent(CharacterEvents.EventName.RunEnter, OnRunEnter);
        _control.events.registerEvent(CharacterEvents.EventName.RunExit, OnRunExit);
        _control.events.registerEvent(CharacterEvents.EventName.RollEnter, OnRollEnter);
        _control.events.registerEvent(CharacterEvents.EventName.RollExit, OnRollExit);
        _control.events.registerEvent(CharacterEvents.EventName.CrouchEnter, OnCrouchEnter);
        _control.events.registerEvent(CharacterEvents.EventName.CrouchExit, OnCrouchExit);
        _control.events.registerEvent(CharacterEvents.EventName.SlideEnter, OnSlideEnter);
        _control.events.registerEvent(CharacterEvents.EventName.SlideExit, OnSlideExit);

        _control.events.registerEvent(CharacterEvents.EventName.GroundedEnter, OnGroundedEnter);
        _control.events.registerEvent(CharacterEvents.EventName.GroundedExit, OnGroundedExit);
        _control.events.registerEvent(CharacterEvents.EventName.JumpEnter, OnJumpEnter);
        _control.events.registerEvent(CharacterEvents.EventName.JumpExit, OnJumpExit);
        _control.events.registerEvent(CharacterEvents.EventName.FallEnter, OnFallEnter);
        _control.events.registerEvent(CharacterEvents.EventName.FallExit, OnFallExit);
        _control.events.registerEvent(CharacterEvents.EventName.JumpFallEnter, OnJumpFallEnter);
        _control.events.registerEvent(CharacterEvents.EventName.JumpFallExit, OnJumpFallExit);
        _control.events.registerEvent(CharacterEvents.EventName.BounceEnter, OnBounceEnter);
        _control.events.registerEvent(CharacterEvents.EventName.BounceExit, OnBounceExit);
        _control.events.registerEvent(CharacterEvents.EventName.DieEnter, OnDieEnter);
        _control.events.registerEvent(CharacterEvents.EventName.DieExit, OnDieExit);
        _control.events.registerEvent(CharacterEvents.EventName.StunnedEnter, OnStunnedEnter);
        _control.events.registerEvent(CharacterEvents.EventName.StunnedExit, OnStunnedExit);
        _control.events.registerEvent(CharacterEvents.EventName.InertiaEnter, OnInertiaEnter);
        _control.events.registerEvent(CharacterEvents.EventName.InertiaExit, OnInertiaExit);
        _control.events.registerEvent(CharacterEvents.EventName.TripOverEnter, OnTripOverEnter);
        _control.events.registerEvent(CharacterEvents.EventName.TripOverExit, OnTripOverExit);
        _control.events.registerEvent(CharacterEvents.EventName.WallFallEnter, OnWallFallEnter);
        _control.events.registerEvent(CharacterEvents.EventName.WallFallExit, OnWallFallExit);
        _control.events.registerEvent(CharacterEvents.EventName.WallGrabEnter, OnWallGrabEnter);
        _control.events.registerEvent(CharacterEvents.EventName.WallGrabExit, OnWallGrabExit);
        _control.events.registerEvent(CharacterEvents.EventName.WallJumpEnter, OnWallJumpEnter);
        _control.events.registerEvent(CharacterEvents.EventName.WallJumpExit, OnWallJumpExit);
    }

    void OnDisable()
    {
        _control.events.registerEvent(CharacterEvents.EventName.IdleEnter, OnIdleEnter);
        _control.events.registerEvent(CharacterEvents.EventName.IdleExit, OnIdleExit);
        _control.events.registerEvent(CharacterEvents.EventName.RunEnter, OnRunEnter);
        _control.events.registerEvent(CharacterEvents.EventName.RunExit, OnRunExit);
        _control.events.registerEvent(CharacterEvents.EventName.RollEnter, OnRollEnter);
        _control.events.registerEvent(CharacterEvents.EventName.RollExit, OnRollExit);
        _control.events.registerEvent(CharacterEvents.EventName.CrouchEnter, OnCrouchEnter);
        _control.events.registerEvent(CharacterEvents.EventName.CrouchExit, OnCrouchExit);
        _control.events.registerEvent(CharacterEvents.EventName.SlideEnter, OnSlideEnter);
        _control.events.registerEvent(CharacterEvents.EventName.SlideExit, OnSlideExit);

        _control.events.registerEvent(CharacterEvents.EventName.GroundedEnter, OnGroundedEnter);
        _control.events.registerEvent(CharacterEvents.EventName.GroundedExit, OnGroundedExit);
        _control.events.registerEvent(CharacterEvents.EventName.JumpEnter, OnJumpEnter);
        _control.events.registerEvent(CharacterEvents.EventName.JumpExit, OnJumpExit);
        _control.events.registerEvent(CharacterEvents.EventName.FallEnter, OnFallEnter);
        _control.events.registerEvent(CharacterEvents.EventName.FallExit, OnFallExit);
        _control.events.registerEvent(CharacterEvents.EventName.JumpFallEnter, OnJumpFallEnter);
        _control.events.registerEvent(CharacterEvents.EventName.JumpFallExit, OnJumpFallExit);
        _control.events.registerEvent(CharacterEvents.EventName.BounceEnter, OnBounceEnter);
        _control.events.registerEvent(CharacterEvents.EventName.BounceExit, OnBounceExit);
        _control.events.registerEvent(CharacterEvents.EventName.DieEnter, OnDieEnter);
        _control.events.registerEvent(CharacterEvents.EventName.DieExit, OnDieExit);
        _control.events.registerEvent(CharacterEvents.EventName.StunnedEnter, OnStunnedEnter);
        _control.events.registerEvent(CharacterEvents.EventName.StunnedExit, OnStunnedExit);
        _control.events.registerEvent(CharacterEvents.EventName.InertiaEnter, OnInertiaEnter);
        _control.events.registerEvent(CharacterEvents.EventName.InertiaExit, OnInertiaExit);
        _control.events.registerEvent(CharacterEvents.EventName.TripOverEnter, OnTripOverEnter);
        _control.events.registerEvent(CharacterEvents.EventName.TripOverExit, OnTripOverExit);
        _control.events.registerEvent(CharacterEvents.EventName.WallFallEnter, OnWallFallEnter);
        _control.events.registerEvent(CharacterEvents.EventName.WallFallExit, OnWallFallExit);
        _control.events.registerEvent(CharacterEvents.EventName.WallGrabEnter, OnWallGrabEnter);
        _control.events.registerEvent(CharacterEvents.EventName.WallGrabExit, OnWallGrabExit);
        _control.events.registerEvent(CharacterEvents.EventName.WallJumpEnter, OnWallJumpEnter);
        _control.events.registerEvent(CharacterEvents.EventName.WallJumpExit, OnWallJumpExit);
    }

    void OnIdleEnter() { Debug.Log("OnIdleEnter called"); }

    void OnIdleExit() { Debug.Log("OnIdleExit called"); }

    void OnRunEnter() { Debug.Log("OnRunEnter called"); }

    void OnRunExit() { Debug.Log("OnRunExit called"); }

    void OnRollEnter() { Debug.Log("OnRollEnter called"); }

    void OnRollExit() { Debug.Log("OnRollExit called"); }

    void OnCrouchEnter() { Debug.Log("OnCrouchEnter called"); }

    void OnCrouchExit() { Debug.Log("OnCrouchExit called"); }

    void OnSlideEnter() { Debug.Log("OnSlideEnter called"); }

    void OnSlideExit() { Debug.Log("OnSlideExit called"); }

    
    void OnGroundedEnter() { Debug.Log("OnGroundedEnter called"); }

    void OnGroundedExit() { Debug.Log("OnGroundedExit called"); }

    void OnJumpEnter() { Debug.Log("OnJumpEnter called"); }

    void OnJumpExit() { Debug.Log("OnJumpExit called"); }

    void OnFallEnter() { Debug.Log("OnFallEnter called"); }

    void OnFallExit() { Debug.Log("OnFallExit called"); }

    void OnJumpFallEnter() { Debug.Log("OnJumpFallEnter called"); }

    void OnJumpFallExit() { Debug.Log("OnJumpFallExit called"); }

    void OnBounceEnter() { Debug.Log("OnBounceEnter called"); }

    void OnBounceExit() { Debug.Log("OnBounceExit called"); }

    void OnDieEnter() { Debug.Log("OnDieEnter called"); }

    void OnDieExit() { Debug.Log("OnDieExit called"); }

    void OnStunnedEnter() { Debug.Log("OnStunnedEnter called"); }

    void OnStunnedExit() { Debug.Log("OnStunnedExit called"); }

    void OnInertiaEnter() { Debug.Log("OnInertiaEnter called"); }

    void OnInertiaExit() { Debug.Log("OnInertiaExit called"); }

    void OnTripOverEnter() { Debug.Log("OnTripOverEnter called"); }

    void OnTripOverExit() { Debug.Log("OnTripOverExit called"); }

    void OnWallFallEnter() { Debug.Log("OnWallFallEnter called"); }

    void OnWallFallExit() { Debug.Log("OnWallFallExit called"); }

    void OnWallGrabEnter() { Debug.Log("OnWallGrabEnter called"); }

    void OnWallGrabExit() { Debug.Log("OnWallGrabExit called"); }

    void OnWallJumpEnter() { Debug.Log("OnWallJumpEnter called"); }

    void OnWallJumpExit() { Debug.Log("OnWallJumpExit called"); }
}
