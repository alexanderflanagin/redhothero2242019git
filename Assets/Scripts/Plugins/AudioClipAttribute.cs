using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioClipAttribute : PropertyAttribute
{
    public readonly AudioClipsData.ClipsGroups prefix;

    public AudioClipAttribute(AudioClipsData.ClipsGroups prefix)
    {
        this.prefix = prefix;
    }
}
