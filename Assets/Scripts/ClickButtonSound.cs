using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;


public class ClickButtonSound : MonoBehaviour
{
	private float pauseVolume = 0f;

    public string clickClip = "Click";


    // Use this for initialization
    public void OnClickSound()
    {
        AudioManager.Instance.PlayOneShoot(AudioClipsData.ClipsGroups.Enviro, clickClip);
    }
}
