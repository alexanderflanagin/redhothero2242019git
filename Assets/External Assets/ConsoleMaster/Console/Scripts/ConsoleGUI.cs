using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ConsoleGUI : MonoBehaviour {
    public ConsoleToggler consoleToggler;
    public ConsoleAction escapeAction;
    public ConsoleAction submitAction;
    [HideInInspector]
        public string input = "";
    private ConsoleLog consoleLog;
    private Rect consoleRect;
    private bool focus = false;
    private const int WINDOW_ID = 50;

    public Vector2 scroll;

    public GUIStyle guiStyle;

    private int lastLogLenght;

    private List<string> instructions;

    private int currentPosition = 0;

    private void Start() {
        consoleRect = new Rect(0, 0, Screen.width, Mathf.Min(300, Screen.height));
        consoleLog = ConsoleLog.Instance;
        instructions = new List<string>();
        instructions.Add("");
    }

    private void OnEnable() {
        focus = true;
    }

    private void OnDisable() {
        focus = true;
    }

    public void OnGUI()
    {
        if(consoleToggler.consoleEnabled)
        {
            GUILayout.Window(WINDOW_ID, consoleRect, RenderWindow, "Console");
        }
    }

    private void RenderWindow(int id)
    {
        HandleEscape();

        HandleSubmit();

        HandleUpDown();

        if (consoleLog.log.Length != lastLogLenght)
        {
            StartCoroutine(DelayedUpdate());
        }

        scroll = GUILayout.BeginScrollView(scroll, guiStyle);
        GUILayout.Label(consoleLog.log, guiStyle);
        GUILayout.EndScrollView();
        GUI.SetNextControlName("input");
        input = GUILayout.TextField(input, guiStyle);

        if (focus)
        {
            GUI.FocusControl("input");
            focus = false;
        }
    }

    private IEnumerator DelayedUpdate()
    {
        yield return null;
        scroll = new Vector2(0, consoleLog.log.Length);
        lastLogLenght = consoleLog.log.Length;
    }

    private void HandleSubmit()
    {
        if (KeyDown("[enter]") || KeyDown("return"))
        {
            if (submitAction != null)
            {
                instructions.Add(input);
                submitAction.Activate();
            }
            input = "";
            currentPosition = 0;
        }
    }

    private bool HandleEscape()
    {
        if (KeyDown("escape") || KeyDown("`"))
        {
            // escapeAction.Activate();
            consoleToggler.ToggleConsole();
            input = "";
            currentPosition = 0;
            return true;
        }

        return false;
    }

    private void HandleUpDown()
    {
        if(KeyDown("up"))
        {
            if(currentPosition == 0)
            {
                currentPosition = instructions.Count - 1;
            }
            else if(currentPosition > 1)
            {
                currentPosition--;
            }

            input = instructions[currentPosition];
            Debug.Log("Input up: " + input);
        }

        if(KeyDown("down"))
        {
            if (currentPosition == 0)
            {
                return;
            }
            else if (currentPosition < instructions.Count-1)
            {
                currentPosition++;
            }

            input = instructions[currentPosition];
            Debug.Log("Input down: " + input);
        }
    }

    private bool KeyDown(string key)
    {
        return Event.current.Equals(Event.KeyboardEvent(key));
    }
}
