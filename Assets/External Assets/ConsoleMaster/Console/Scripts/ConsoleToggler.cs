using UnityEngine;
using System.Collections;

public class ConsoleToggler : MonoBehaviour {
    public bool consoleEnabled = false;
    public ConsoleAction ConsoleOpenAction;
    public ConsoleAction ConsoleCloseAction;

    void Update()
    {
        if (!consoleEnabled && Input.GetKeyDown(KeyCode.Escape))
        {
            ToggleConsole();
        }
    }

    public void ToggleConsole()
    {
        consoleEnabled = !consoleEnabled;
        if (consoleEnabled)
        {
            ConsoleOpenAction.Activate();
        }
        else
        {
            ConsoleCloseAction.Activate();
        }
    }
}
