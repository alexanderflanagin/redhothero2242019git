using UnityEngine;
using UnityEditor;
using System.Collections;
using System.IO;

public class TiledImporterWindow : EditorWindow
{

    string path;
	string shortPath;

    bool develScene;

    bool createCranes;
    
    [MenuItem("Window/Tiled Importer")]
    public static void ShowWindow()
    {
        //Show existing window instance. If one doesn't exist, make one.
        EditorWindow.GetWindow(typeof(TiledImporterWindow));
    }

    void OnGUI()
    {
        GUILayout.Label("Tiled Importer", EditorStyles.boldLabel);
		GUILayout.BeginHorizontal ();
		GUILayout.Label("..." + shortPath);
        if (GUILayout.Button("Select JSON File"))
        {
            string url = Application.dataPath + "/TiledImporter";
			if(path != null && path != "") {
				url = Path.GetFullPath(path);
            }

			path = EditorUtility.OpenFilePanel("JSON Map", url, "json");
			shortPath = "";
			if(path.Length > 20) {
				shortPath = path.Substring(path.Length - 20);
			}
        }

		GUILayout.EndHorizontal();

        develScene = GUILayout.Toggle(develScene, "Escena de devel");

        createCranes = GUILayout.Toggle(createCranes, "Crear cranes");

        if (GUILayout.Button("Import"))
        {
            if (path != null && path != "")
            {
                TiledImporter.Import(path, createCranes);
            }
            // path = EditorUtility.OpenFilePanel("TMX Map", "", "tmx");
        }

        if (GUILayout.Button("Remove old"))
        {
            TiledImporter.RemoveOld(path);
        }
    }
}
