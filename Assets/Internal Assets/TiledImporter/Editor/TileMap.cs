using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;

public class TileMap {
	/// <summary>
	/// Node JSON amb tot el contingut
	/// </summary>
	public JSONNode allContent;

	/// <summary>
	/// Width del tile map
	/// </summary>
	public int width;

	/// <summary>
	/// Height del tile map
	/// </summary>
	public int height;

	/// <summary>
	/// Llistat de capes del tile map
	/// </summary>
	public List<TileMapLayer> layers;

    public const int SECOND_TILESET_FIRST_ID = 325;

    public const int BASIC_COLLIDER = 1;

    public const int ONE_WAY_PLATFORM = 2;

    public const int BLUE_SUBSTANCE = 3;

    public const int BLUE_SUBSTANCED_WALL = 5;

    public const int STV_REACTOR = 20 + SECOND_TILESET_FIRST_ID;

    public const int TURRET_HORIZONTAL_RIGHT = 38 + SECOND_TILESET_FIRST_ID;

    public const int TURRET_HORIZONTAL_LEFT = 39 + SECOND_TILESET_FIRST_ID;

    public const int TURRET_TOP_TO_GROUND = 41 + SECOND_TILESET_FIRST_ID;

    public const int BOX = 90 + SECOND_TILESET_FIRST_ID;

    public const int SPIKES = 91;

    public const int BLUE_SUBSTANCED_BOX = 108 + SECOND_TILESET_FIRST_ID;

    public const int DOT = 146;

    public const int TUTORIAL_DOT = 150;

    public const int SUPER_DOT = 147;

    public const int CELL = 148;

    public const int DENSITY = 145;

    public const int TUTORIAL_DENSITY = 149;

	/// <summary>
	/// Llegeix els atributs basics des de una cadena de text en format json. Width, height i genera les capes
	/// </summary>
	/// <param name="content">Content.</param>
	public void ParseFromJSON(string content) {
		allContent = JSON.Parse (content);

		height = allContent["height"].AsInt;

		width = allContent["width"].AsInt;

		layers = new List<TileMapLayer> ();

		// Afegim les layers
		for(int i = 0; i < allContent["layers"].Count; i++) {
			JSONNode l = allContent["layers"][i];
			TileMapLayer layer = new TileMapLayer();

			layer.height = l["height"].AsInt;
			layer.width = l["width"].AsInt;
			layer.name = l["name"];
			layer.visible = l["visible"].AsBool;
			layer.x = l["x"].AsInt;
			layer.y = l["y"].AsInt;

			layer.layerMatrix = new int[layer.width, layer.height];

			int counter = 0;

			for (int k = height - 1; k >= 0; k--) 
			{
				for (int j = 0; j < width; j++)
				{
					layer.layerMatrix[j, k] = l["data"][counter].AsInt;
					counter++;
					//Debug.Log(all["layers"][0]["data"][i * height + j] + " - " + layerMatrix[i, j]);
				}
			}

			layers.Add(layer);
		}
	}

    public List<GameObject> GenerateSingles(int id, GameObject prefab, float xModifier, float yModifier)
    {
        List<GameObject> singles = new List<GameObject>();

        foreach (TileMapLayer layer in layers)
        {
            if (layer.visible)
            {
                for (int y = layer.y; y < layer.height; y++)
                {
                    for (int x = layer.x; x < layer.width; x++)
                    {
                        if (layer.layerMatrix[x, y] == id)
                        {
                            singles.Add(GenerateSingleWithPrefab(x, y, prefab, xModifier, yModifier));
                        }
                    }
                }
            }
        }

        return singles;
    }

    public List<GameObject> GenerateDots()
    {
        List<GameObject> dots = new List<GameObject>();

        foreach (TileMapLayer layer in layers)
        {
            if (layer.visible)
            {
                for (int y = layer.y; y < layer.height; y++)
                {
                    for (int x = layer.x; x < layer.width; x++)
                    {
                        if (layer.layerMatrix[x, y] == TileMap.DOT || layer.layerMatrix[x, y] == TileMap.TUTORIAL_DOT || layer.layerMatrix[x, y] == TileMap.SUPER_DOT || layer.layerMatrix[x, y] == TileMap.CELL)
                        {
                            dots.Add(GenerateSingle(x, y, layer.layerMatrix[x, y]));
                        }
                    }
                }
            }
        }

        return dots;
    }

    public List<GameObject> GenerateDensities()
    {
        List<GameObject> densities = new List<GameObject>();

        foreach (TileMapLayer layer in layers)
        {
            if (layer.visible)
            {
                for (int y = layer.y; y < layer.height; y++)
                {
                    for (int x = layer.x; x < layer.width; x++)
                    {
                        if (layer.layerMatrix[x, y] == TileMap.DENSITY || layer.layerMatrix[x, y] == TileMap.TUTORIAL_DENSITY)
                        {
                            densities.Add(GenerateSingle(x, y, layer.layerMatrix[x, y]));
                        }
                    }
                }
            }
        }

        return densities;
    }

    public List<GameObject> GenerateBoxes()
    {
        List<GameObject> boxes = new List<GameObject>();

        foreach (TileMapLayer layer in layers)
        {
            if (layer.visible)
            {
                for (int y = layer.y; y < layer.height; y++)
                {
                    for (int x = layer.x; x < layer.width; x++)
                    {
                        if (layer.layerMatrix[x, y] == TileMap.BOX || layer.layerMatrix[x, y] == TileMap.BLUE_SUBSTANCED_BOX)
                        {
                            boxes.Add(GenerateSingle(x, y, layer.layerMatrix[x, y]));
                        }
                    }
                }
            }
        }

        return boxes;
    }

    public List<GameObject> GenerateEnemiesGizmos()
    {
        List<GameObject> enemiesGizmos = new List<GameObject>();

        foreach (TileMapLayer layer in layers)
        {
            if (layer.visible)
            {
                for (int y = layer.y; y < layer.height; y++)
                {
                    for (int x = layer.x; x < layer.width; x++)
                    {
                        if (layer.layerMatrix[x, y] == TileMap.TURRET_TOP_TO_GROUND || layer.layerMatrix[x, y] == TileMap.TURRET_HORIZONTAL_LEFT || layer.layerMatrix[x, y] == TileMap.TURRET_HORIZONTAL_RIGHT)
                        {
                            enemiesGizmos.Add(GenerateSingle(x, y, layer.layerMatrix[x, y]));
                        }
                    }
                }
            }
        }

        return enemiesGizmos;
    }

    public List<GameObject> GenerateEnemies()
    {
        List<GameObject> enemies = new List<GameObject>();

        foreach (TileMapLayer layer in layers)
        {
            if (layer.visible)
            {
                for (int y = layer.y; y < layer.height; y++)
                {
                    for (int x = layer.x; x < layer.width; x++)
                    {
                        if (layer.layerMatrix[x, y] == TileMap.SPIKES || layer.layerMatrix[x, y] == TileMap.STV_REACTOR)
                        {
                            enemies.Add(GenerateSingle(x, y, layer.layerMatrix[x, y]));
                        }
                    }
                }
            }
        }

        return enemies;
    }

    public List<GameObject> GenerateBlueSubstance()
    {
        List<GameObject> blueSubstances = new List<GameObject>();

        foreach (TileMapLayer layer in layers)
        {
            if (layer.visible)
            {
                bool[,] used = new bool[layer.width, layer.height];
                int x = 0, y = 0; // Per a navegar per layer

                for (y = layer.y; y < layer.height; y++)
                {
                    for (x = layer.x; x < layer.width; x++)
                    {
                        if (!used[x, y])
                        {
                            if (layer.layerMatrix[x, y] == TileMap.BLUE_SUBSTANCE)
                            {
                                int i = x;
                                int j = y;

                                bool end = false;

                                int counter = 0;
                                while (!end)
                                {
                                    counter++;
                                    for (i = x; i < x + counter; i++)
                                    {
                                        end = end || layer.layerMatrix[i, y + counter] != TileMap.BLUE_SUBSTANCE || used[i, y + counter];
                                    }

                                    for (j = y; j < y + counter; j++)
                                    {
                                        end = end || layer.layerMatrix[x + counter, j] != TileMap.BLUE_SUBSTANCE || used[x + counter, j];
                                    }

                                    // Revisem tambe la cantonada oposada
                                    end = end || layer.layerMatrix[x + counter, y + counter] != TileMap.BLUE_SUBSTANCE || used[x + counter, y + counter];
                                }

                                for (i = x; i < x + counter; i++)
                                {
                                    for (j = y; j < y + counter; j++)
                                    {
                                        used[i, j] = true;
                                    }
                                }

                                counter--;

                                GameObject blueSubstance = null;

                                if (counter == 0)
                                {
                                    blueSubstance = PrefabUtility.InstantiatePrefab(AssetDatabase.LoadAssetAtPath<GameObject>(TiledImporter.BLUE_SUBSTANCE_PATH_1)) as GameObject;
                                }
                                else if (counter == 1 || counter == 2)
                                {
                                    blueSubstance = PrefabUtility.InstantiatePrefab(AssetDatabase.LoadAssetAtPath<GameObject>(TiledImporter.BLUE_SUBSTANCE_PATH_2)) as GameObject;
                                }
                                else
                                {
                                    blueSubstance = PrefabUtility.InstantiatePrefab(AssetDatabase.LoadAssetAtPath<GameObject>(TiledImporter.BLUE_SUBSTANCE_PATH_4)) as GameObject;
                                }
                                

                                blueSubstance.transform.position = new Vector3(x + (float)counter / 2, y + (float)counter / 2);

                                blueSubstance.transform.localScale = new Vector3(counter + 1, counter + 1, counter + 1);

                                blueSubstances.Add(blueSubstance);
                            }
                        }
                    }
                }
            }
        }

        return blueSubstances;
    }

    public List<GameObject> GenerateCranes()
    {
        List<GameObject> cranes = new List<GameObject>();

        foreach (TileMapLayer layer in layers)
        {
            if (layer.visible)
            {
                bool[,] used = new bool[layer.width, layer.height];
                int x = 0, y = 0; // Per a navegar per layer

                for (y = layer.y; y < layer.height; y++)
                {
                    for (x = layer.x; x < layer.width; x++)
                    {
                        if (!used[x, y])
                        {
                            if (layer.layerMatrix[x, y] == TileMap.BASIC_COLLIDER)
                            {
                                int i = x;
                                int j = y;

                                bool end = false;

                                int counter = 0;
                                while (!end)
                                {
                                    counter++;
                                    for (i = x; i < x + counter; i++)
                                    {
                                        if (y + counter >= layer.height)
                                        {
                                            end = true;
                                        }
                                        else
                                        {
                                            end = end || layer.layerMatrix[i, y + counter] != TileMap.BASIC_COLLIDER || used[i, y + counter];
                                        }
                                    }

                                    for (j = y; j < y + counter; j++)
                                    {
                                        if (x + counter >= layer.width)
                                        {
                                            end = true;
                                        }
                                        else
                                        {
                                            end = end || layer.layerMatrix[x + counter, j] != TileMap.BASIC_COLLIDER || used[x + counter, j]; ;
                                        }
                                    }

                                    // Revisem tambe la cantonada oposada
                                    end = end || layer.layerMatrix[x + counter, y + counter] != TileMap.BASIC_COLLIDER || used[x + counter, y + counter];
                                }

                                for (i = x; i < x + counter; i++)
                                {
                                    for (j = y; j < y + counter; j++)
                                    {
                                        used[i, j] = true;
                                    }
                                }

                                counter--;

                                GameObject crane = null;

                                if (counter == 0)
                                {
                                    crane = PrefabUtility.InstantiatePrefab(AssetDatabase.LoadAssetAtPath<GameObject>(TiledImporter.CRANE_PATH_1)) as GameObject;
                                }
                                else if (counter == 1 || counter == 2)
                                {
                                    crane = PrefabUtility.InstantiatePrefab(AssetDatabase.LoadAssetAtPath<GameObject>(TiledImporter.CRANE_PATH_1)) as GameObject;
                                }
                                else
                                {
                                    crane = PrefabUtility.InstantiatePrefab(AssetDatabase.LoadAssetAtPath<GameObject>(TiledImporter.CRANE_PATH_1)) as GameObject;
                                }


                                crane.transform.position = new Vector3(x + (float)counter / 2, y + (float)counter / 2);

                                crane.transform.localScale = new Vector3(counter + 1, counter + 1, 1);

                                cranes.Add(crane);
                            }
                        }
                    }
                }
            }
        }

        return cranes;
    }

	/// <summary>
	/// Crea els game objects dels colliders
	/// </summary>
	public List<GameObject> GenerateColliders() {

        List<GameObject> list = new List<GameObject>();
        
        foreach (TileMapLayer layer in layers) {
			if(layer.visible) {
				bool[,] used = new bool[layer.width, layer.height]; // Per a saber quins tiles ja hem fet servir
				int x = 0, y = 0; // Per a navegar per layer
				int startX = 0, startY = 0, endX = 0, endY = 0; // Inici i final del collider que estem preparant
				
				for(y = layer.y; y < layer.height; y++) {
					for(x = layer.x; x < layer.width; x++) {
						// Debug.Log(string.Format("Layer {0}: x {1} y {2} - used: {3}", layer.name, x, y, used[x,y]));
						if (!used [x, y]) {
							// Debug.Log("Not used. Value: " + layer.layerMatrix [x, y]);
							if (layer.layerMatrix [x, y] != 0) {
								int i = x;
								int j = y;
								startX = x;
								endX = x;
								startY = y;
								endY = y;
								
								// Iteracions sobre la X
								while(i < layer.width && layer.layerMatrix[i,j] == layer.layerMatrix[x,y] ) {
									used[i,j] = true;
									endX = i;
									i++;
								}
								
								// Iteracions sobre la Y
								bool valid = true;
								while(valid && j < layer.height) {
									for(int k = startX; k <= endX; k++) {
										valid = valid && layer.layerMatrix[k,j] == layer.layerMatrix[x,y];
									}
									
									if(valid) {
										endY = j;
										for(int k = startX; k <= endX; k++) {
											used[k,j] = true;
										}
										j++;
										
									}
								}

                                list.Add(GenerateCollider(layer.layerMatrix[x, y], startX, endX, startY, endY));
							}
						}
					}
				}
			}
		}
        
        return list;
	}

	public GameObject GenerateCollider(int tileType, int startX, int endX, int startY, int endY) {
		GameObject collider = null;

        switch (tileType)
        {
            case -1:
                collider = generateJustQuad(startX, endX, startY, endY);
                break;
            case TileMap.BASIC_COLLIDER:
                collider = generateCustomCollider(startX, endX, startY, endY, TiledImporter.BASIC_COLLIDER_PATH);
                break;
            case TileMap.ONE_WAY_PLATFORM:
                collider = generateCustomCollider(startX, endX, startY, endY, TiledImporter.ONE_WAY_COLLIDER_PATH);
                break;
            case TileMap.BLUE_SUBSTANCED_WALL:
                collider = generateCustomCollider(startX, endX, startY, endY, TiledImporter.BLUE_SUBSTANCED_WALL_PATH);
                break;
        }

        return collider;
	}

    public GameObject GenerateSingleWithPrefab(int x, int y, GameObject prefab, float xModifier, float yModifier)
    {
        GameObject gameObject = null;

        gameObject = PrefabUtility.InstantiatePrefab(prefab) as GameObject;

        gameObject.transform.position = new Vector3((float)x + xModifier, (float)y + yModifier);

        return gameObject;
    }

    public GameObject GenerateSingle(int x, int y, int id)
    {
        GameObject gameObject = null;

        float xModifier = 0, yModifier = 0;

        switch(id) {
            case TileMap.DOT:
                gameObject = PrefabUtility.InstantiatePrefab(AssetDatabase.LoadAssetAtPath<GameObject>(TiledImporter.DOT_PATH)) as GameObject;
                break;
            case TileMap.TUTORIAL_DOT:
                gameObject = PrefabUtility.InstantiatePrefab(AssetDatabase.LoadAssetAtPath<GameObject>(TiledImporter.TUTORIAL_DOT_PATH)) as GameObject;
                break;
            case TileMap.SUPER_DOT:
                gameObject = PrefabUtility.InstantiatePrefab(AssetDatabase.LoadAssetAtPath<GameObject>(TiledImporter.SUPER_DOT_PATH)) as GameObject;
                break;
            case TileMap.DENSITY:
                gameObject = PrefabUtility.InstantiatePrefab(AssetDatabase.LoadAssetAtPath<GameObject>(TiledImporter.DENSITY_PATH)) as GameObject;
                break;
            case TileMap.TUTORIAL_DENSITY:
                gameObject = PrefabUtility.InstantiatePrefab(AssetDatabase.LoadAssetAtPath<GameObject>(TiledImporter.TUTORIAL_DENSITY_PATH)) as GameObject;
                break;
            case TileMap.SPIKES:
                gameObject = PrefabUtility.InstantiatePrefab(AssetDatabase.LoadAssetAtPath<GameObject>(TiledImporter.SPIKES_1x1_PATH)) as GameObject;
                yModifier = -0.5f;
                break;
            case TileMap.CELL:
                gameObject = PrefabUtility.InstantiatePrefab(AssetDatabase.LoadAssetAtPath<GameObject>(TiledImporter.CELL_PATH)) as GameObject;
                break;
            case TileMap.BLUE_SUBSTANCE:
                gameObject = PrefabUtility.InstantiatePrefab(AssetDatabase.LoadAssetAtPath<GameObject>(TiledImporter.BLUE_SUBSTANCE_PATH_1)) as GameObject;
                break;
            case TileMap.BOX:
                gameObject = PrefabUtility.InstantiatePrefab(AssetDatabase.LoadAssetAtPath<GameObject>(TiledImporter.BOX_PATH)) as GameObject;
                yModifier = 0.5f;
                xModifier = 0.5f;
                break;
            case TileMap.BLUE_SUBSTANCED_BOX:
                gameObject = PrefabUtility.InstantiatePrefab(AssetDatabase.LoadAssetAtPath<GameObject>(TiledImporter.BLUE_SUBSTANCED_BOX_PATH)) as GameObject;
                yModifier = 0.5f;
                xModifier = 0.5f;
                break;
            case TileMap.TURRET_TOP_TO_GROUND:
                gameObject = PrefabUtility.InstantiatePrefab(AssetDatabase.LoadAssetAtPath<GameObject>(TiledImporter.TURRET_TOP_TO_GROUND_PATH)) as GameObject;
                yModifier = 1.5f;
                xModifier = 0.5f;
                break;
            case TileMap.TURRET_HORIZONTAL_RIGHT:
                gameObject = PrefabUtility.InstantiatePrefab(AssetDatabase.LoadAssetAtPath<GameObject>(TiledImporter.TURRET_HORIZONTAL_RIGHT_PATH)) as GameObject;
                yModifier = 0.5f;
                xModifier = -0.5f;
                break;
            case TileMap.TURRET_HORIZONTAL_LEFT:
                gameObject = PrefabUtility.InstantiatePrefab(AssetDatabase.LoadAssetAtPath<GameObject>(TiledImporter.TURRET_HORIZONTAL_LEFT_PATH)) as GameObject;
                yModifier = 0.5f;
                xModifier = 1.5f;
                break;
            case TileMap.STV_REACTOR:
                gameObject = PrefabUtility.InstantiatePrefab(AssetDatabase.LoadAssetAtPath<GameObject>(TiledImporter.STV_REACTOR_PATH)) as GameObject;
                yModifier = 0f;
                xModifier = 0.5f;
                break;
        }

        gameObject.transform.position = new Vector3((float)x + xModifier, (float)y + yModifier);

        return gameObject;
    }

    public GameObject generateCustomCollider(int startX, int endX, int startY, int endY, string colliderPath)
    {
        GameObject collider = PrefabUtility.InstantiatePrefab(AssetDatabase.LoadAssetAtPath<GameObject>(colliderPath)) as GameObject;
        BoxCollider2D box = collider.GetComponent<BoxCollider2D>();
        collider.transform.position = new Vector3((float)(endX + startX) / 2f, (float)(startY + endY) / 2f, 0);
        box.offset = Vector2.zero;
        // box.size = new Vector2((endX - startX) + 1, (endY - startY) + 1);
        collider.transform.localScale = new Vector2((endX - startX) + 1, (endY - startY) + 1);

        return collider;
    }

    public GameObject generateJustQuad(int startX, int endX, int startY, int endY)
    {
        GameObject collider = GameObject.CreatePrimitive(PrimitiveType.Quad);
        collider.transform.position = new Vector3((float)(endX + startX) / 2f, (float)(startY + endY) / 2f, 0);
        collider.transform.localScale = new Vector2((endX - startX) + 1, (endY - startY) + 1);

        return collider;
    }

	public void PrintQuads() {
		string outText = "";
		
        //GameObject gameObject = new GameObject(gameObjectName);

        string solidPlatform = "Assets/Prefabs/Props/Floors/Metal/Beam.prefab";
        string oneWayPlatform = "Assets/Prefabs/Props/PrefabLab/PrefabPlatform1.prefab";

        GameObject layerObj = new GameObject("Auto Generated Platforms");
        GameObjectUtility.SetParentAndAlign(layerObj, GameObject.Find("Decorations"));

		foreach (TileMapLayer layer in layers) {
			
			// GameObjectUtility.SetParentAndAlign(layerObj, gameObject);

			// Creem els quads
			for (int i = 0; i < layer.height; i++)
			{
				for (int j = 0; j < layer.width; j++)
				{
					outText += " " + layer.layerMatrix[j, i];
					
					if (layer.layerMatrix[j, i] != 0)
					{
                        GameObject quad = null;

                        switch (layer.layerMatrix[j, i])
                        {
                            case 1:
                            case 5:
                                quad = PrefabUtility.InstantiatePrefab(AssetDatabase.LoadAssetAtPath(solidPlatform, typeof(GameObject)) as GameObject) as GameObject;
                                quad.name = "quad_" + i + "_" + j;
                                GameObjectUtility.SetParentAndAlign(quad, layerObj);
                                quad.transform.position = new Vector3(j + layer.x, i + layer.y, 0);
                                break;
                            case 2:
                            case 7:
                                quad = PrefabUtility.InstantiatePrefab(AssetDatabase.LoadAssetAtPath(oneWayPlatform, typeof(GameObject)) as GameObject) as GameObject;
                                quad.name = "quad_" + i + "_" + j;
                                GameObjectUtility.SetParentAndAlign(quad, layerObj);
                                quad.transform.localScale = new Vector3(1.25f, 1, 1);
                                quad.transform.position = new Vector3(j + layer.x, i + layer.y + 0.45f, 0);
                                // quad.GetComponent<MeshRenderer>().material.color = Color.blue;
                                break;
                            case 8:
                                // quad.GetComponent<MeshRenderer>().material.color = Color.green;
                                break;
                        }

                        
                        
						
					}
				}
				
				outText += "\n";
			}
		}
	}
}

public struct TileMapLayer {
	public int height;
	public int width;
	public string name;
	public bool visible;
	public int x;
	public int y;

	public int[,] layerMatrix;
}
