using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;
using System.IO;

public class TiledImporter {

    public static string BASIC_COLLIDER_PATH = "Assets/Prefabs/Editor/Basic.prefab";

    public static string ONE_WAY_COLLIDER_PATH = "Assets/Prefabs/Platforms/OneWayPlatform.prefab";

    public static string BLUE_SUBSTANCE_PATH_1 = "Assets/Prefabs/BlueSubstance/BlueSubstance1x1.prefab";
    
    public static string BLUE_SUBSTANCE_PATH_2 = "Assets/Prefabs/BlueSubstance/BlueSubstance2x2.prefab";
    
    public static string BLUE_SUBSTANCE_PATH_4 = "Assets/Prefabs/BlueSubstance/BlueSubstance4x4.prefab";

    public static string BLUE_SUBSTANCED_WALL_PATH = "Assets/Prefabs/BlueSubstance/BlueSubstancedWall.prefab";

    public static string CRANE_PATH_1 = "Assets/Prefabs/Props/Industrial/CranePlatform1x1_.prefab";

    public static string CRANE_PATH_2 = "Assets/Prefabs/Props/Industrial/CranePlatform2x2Cube_.prefab";

    public static string CRANE_PATH_4 = "Assets/Prefabs/Props/Industrial/CranePlatform3x3_.prefab";

    public static string DOT_PATH = "Assets/Prefabs/VFX/RedHot/RedHotDot.prefab";

    public static string TUTORIAL_DOT_PATH = "Assets/Prefabs/VFX/RedHot/RedHotDotTutorial.prefab";

    public static string SUPER_DOT_PATH = "Assets/Prefabs/VFX/RedHot/RedHotSuperDot.prefab";

    public static string DENSITY_PATH = "Assets/Prefabs/VFX/RedHot/RedHotDensityFactory.prefab";

    public static string TUTORIAL_DENSITY_PATH = "Assets/Prefabs/VFX/RedHot/RedHotDensityTutorial.prefab";

    public static string SPIKES_1x1_PATH = "Assets/Prefabs/Actors/Enemies/Spikes1x1_.prefab";

    public static string STV_REACTOR_PATH = "Assets/Prefabs/Actors/Enemies/StvReactorFactory.prefab";

    public static string TURRET_FACTORY_PATH = "Assets/Prefabs/Actors/Enemies/TurretFactory.prefab";

    public static string CELL_PATH = "Assets/Prefabs/Editor/Pickeable.prefab";

    public static string BOX_PATH = "Assets/Prefabs/Platforms/BreakableWoodBox2x2.prefab";

    public static string BLUE_SUBSTANCED_BOX_PATH = "Assets/Prefabs/Platforms/BlueSubstancedBreakableWoodBox2x2.prefab";

    public static string TURRET_HORIZONTAL_LEFT_PATH = "Assets/Prefabs/Actors/Enemies/EditorGizmos/TurretHorizontalLeft.prefab";

    public static string TURRET_HORIZONTAL_RIGHT_PATH = "Assets/Prefabs/Actors/Enemies/EditorGizmos/TurretHorizontalRight.prefab";

    public static string TURRET_TOP_TO_GROUND_PATH = "Assets/Prefabs/Actors/Enemies/EditorGizmos/TurretTopToGround.prefab";

    public static void Import(string path, bool createCranes)
    {
		StreamReader sr = new StreamReader(path);
		string content = sr.ReadToEnd();
		sr.Close();

		TileMap tilemap = new TileMap ();

		tilemap.ParseFromJSON (content);
        
		List<GameObject> colliders = tilemap.GenerateColliders ();

        GameObject gameplay = GameObject.Find("Gameplay");

        // Posem els colliders on toca
        if (!gameplay)
        {
            gameplay = new GameObject("Gameplay");
        }

		GameObject collidersObj = GameObject.Find("Colliders");

		if(collidersObj == null) {
			collidersObj = new GameObject("Colliders");
			collidersObj.transform.parent = gameplay.transform;
			collidersObj.tag = Tags.EditorColliders;
		}

        // Creem el nou bloc de contingut
        GameObject block = new GameObject(Path.GetFileNameWithoutExtension(path));

		if (collidersObj.transform.Find(block.name))
        {
			GameObject old = collidersObj.transform.Find(block.name).gameObject;
            old.name = "__" + old.name;
            old.SetActive(false);
        }

		block.transform.parent = collidersObj.transform;

		foreach (GameObject col in colliders)
		{
			if (col != null)
			{
				//Debug.Log("Position: " + col.transform.position);
				// GameObject collider = GameObject.Instantiate(col);
				col.transform.parent = block.transform;
			}
		}

        TiledImporter.createAndFillBlock("DotsAndCoins", gameplay, block.name, tilemap.GenerateDots());

        TiledImporter.createAndFillBlock("RedHotDensities", gameplay, block.name, tilemap.GenerateDensities());

        TiledImporter.createAndFillBlock("Enemies", gameplay, block.name, tilemap.GenerateEnemies());

        TiledImporter.createAndFillBlock("BlueSubstances", gameplay, block.name, tilemap.GenerateBlueSubstance());

        createAndFillBlock("Boxes", gameplay, block.name, tilemap.GenerateBoxes());

        createAndFillBlock("EnemiesGizmos", gameplay, block.name, tilemap.GenerateEnemiesGizmos());

        if (createCranes)
        {
            TiledImporter.createAndFillBlock("Decorations", null, block.name, tilemap.GenerateCranes());
        }

        // Generem ara els del Tiled Data
        string dataPath = "Assets/Resources/Data/TiledDataHolder.asset";
        TiledData tiledData = AssetDatabase.LoadAssetAtPath(dataPath, typeof(TiledData)) as TiledData;

        if(tiledData != null)
        {
            foreach(TiledData.TileInfo tileInfo in tiledData.tilesInfo)
            {
                if(tileInfo.fillType == TiledData.TileInfo.FillType.Single)
                {
                    List<GameObject> singlesList = tilemap.GenerateSingles(tileInfo.ID + (int)tileInfo.layer, tileInfo.prefab, tileInfo.xModifier, tileInfo.yModifier);
                    createAndFillBlock(tileInfo.parentGameObject.ToString(), gameplay, block.name, singlesList, false);
                }
            }
        }
    }

    /// <summary>
    /// Crea (si es necessari) i omple un game object amb la llista de game objects que li passem
    /// </summary>
    /// <param name="name">Nom del game object creat que contindra el block que creem</param>
    /// <param name="parent">Parent del game object /name/ (normalment gameplay)</param>
    /// <param name="blockName">Nom del block que creem (per comentar els anteriors si cal)</param>
    /// <param name="list">Llista de GameObjects a crear</param>
    /// <param name="hiddeOld">Si hem d'esborrar, o no, els antics</param>
    private static void createAndFillBlock(string name, GameObject parent, string blockName, List<GameObject> list, bool hiddeOld = true)
    {
        GameObject parentContent = GameObject.Find(name);

        if (parentContent == null)
        {
            parentContent = new GameObject(name);

            if (parent)
            {
                parentContent.transform.parent = parent.transform;
            }
        }

        GameObject block;

        if (!hiddeOld && parentContent.transform.Find(blockName))
        {
            // Ja existia i no l'amaguem
            block = parentContent.transform.Find(blockName).gameObject;
        }
        else
        {
            // Creem el nou bloc de contingut
            block = new GameObject(blockName);

            if (parentContent.transform.Find(blockName))
            {
                GameObject old = parentContent.transform.Find(blockName).gameObject;
                old.name = "__" + old.name;
                old.SetActive(false);
            }

            block.transform.parent = parentContent.transform;
        }

        foreach (GameObject element in list)
        {
            if (element != null)
            {
                element.transform.parent = block.transform;
            }
        }
    }

    public static void RemoveOld(string path)
    {
        string name = Path.GetFileNameWithoutExtension(path);

        GameObject gameplay = GameObject.Find("Gameplay");

        Transform[] olds = gameplay.GetComponentsInChildren<Transform>(true);

        GameObject decorations = GameObject.Find("Decorations");

        if (decorations != null && decorations.transform.Find("__" + name))
        {
            foreach (Transform t in decorations.transform)
            {
                if (t && t.name == "__" + name)
                {
                    GameObject.DestroyImmediate(t.gameObject);
                }
            }
        }

        foreach (Transform old in olds)
        {
            if (old && old.name == "__" + name)
            {
                GameObject.DestroyImmediate(old.gameObject);
            }
        }
    }
}
